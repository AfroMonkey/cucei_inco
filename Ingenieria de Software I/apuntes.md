### 2.1.2 Las Sesiones JAD de trabajo
Usuarios junto con analistas de sistemas. Se realizan borradores para, después,
utilizando herramientas CASE, pasarlo a limpio.

# Requerimientos
Enunciado que describe un proceso o servicio, inicia con un verbo en
infinitivo. Aquí se especifica **todo**, incluso las restricciones.

Para un gran proyecto de desarrollo, se suele ser mas abstractos, para darle
cierta libertad a los desarrolladores con las posibles soluciones.

## Requerimientos de Usuario
Sentencias en lenguaje natural. Escrito para los clientes.
*   Cliente
*   Usuarios finales

## Requerimientos de Sistema
Documento estructurado que define detalladamente las funciones del sistema,
servicios y limitaciones. Define lo que debe ser implementado como parte del
contrato.
*   Desarrolladores

## Requerimientos funcionales
Esencia del sistema. Lo que hace.

## Requerimientos no funcionales
Cómo lo hace.

# Diagrama de Gantt
Modela la planificación de las tareas de un proyecto. Es una herramienta de planeación.  
Sirve para representar y monitorear los avances de las tareas.

## Pasos
1.  Definir el proyecto a diagramar, así como el nivel de detalle de las tareas.
2.  Dividir el proceso en tareas, determinar la duración de las mismas.
3.  Diseñar una tabla colocando de izquierda a derecha, en el eje superior, las unidades de tiempo (días, meses, semanas). Las fases y tareas se colocan de arriba a bajo, en la parte izquierda de la tabla.
4.  Las tareas se representan con barras horizontales de un tamaño que represente la duración de la tarea; cada tarea va ligada a otra.

# Diagrama de Pert
*   Es un grafo.
*   Su objetivo es la planeación.
*   Tiempos máximos y mínimos.
*   Diferentes caminos para lograr el objetivo.
*   Las aristas con actividades (Con un identificador, normalmente letras).
*   Los nodos son uniones o instantes del proyecto (Tienen secuencia).
*   Hay un nodo de inicio y un nodo final.

## Pasos
1.  Crear lista de tareas.
2.  Crear la siguiente tabla y llenarla con las actividades.
TO = Tiempo Optimista  
TMP = Tiempo mas probable
TP = Tiempo probable
TE = Tiempo esperado = (TO+4(TMP)+TP)/6  
4 por la holgura (Puede ser negativa)  
6 para los días laborales por semana  
| Clave | Actividad | Predecesora | TO | TMP | TP | TE |
| ----- | --------- | ----------- | -- | --- | -- | -- |
| A     | Hacer ... | B, C        | 1  | 2   | 4  |    |
3.  Calcular el TE
4.  Hacer una nueva tabla solo con Clave, Actividad, Predecesora y TE.
5.  Grafo. Nodo -> Clave y TE -> Nodo. Ajustar los tiempo con una actividad ficticia (linea punteada).

# Relaciones entre clases
## Asociación
Una asociación es una relación estructural que describe una relación entre objetos. Gráficamente se muestra como una linea continua que une las clases entre si.
Aun que las asociaciones suelen ser bidireccionales, en ocasiones es deseable hacerlas unidireccionales agregando la flecha de navegabilidad.

La multiplicidad de una asociación determina cuantos objetos de cada tipo intervienen en la relación.  
a. El numero de instancias en una clase que se relacionan con una instancia de la otra clase.  
b. Cada asociación tiene 2 multiplicidades.  
c. Para especificar la multiplicidad de una asociación hay que indicar la multiplicidad mínima y la multiplicidad máxima.
<!-- TODO Reparar tablas -->
| Multiplicidad | Significado |
| ------------- | ----------- |
| 1 | Uno y solo uno |
| 0..1 | 0 ó 1 |
| n..m | Desde 'n' hasta 'm' |
| *   | 0 ó varios |
| 0..*   | 0 ó varios |
| 1..*   | 1 ó varios (al menos uno) |
d. Cuando la multiplicidad mínima es '0', la relación es opcional.  
e. Una multiplicidad mínima >= 1 establece una relación obligatoria

### Relaciones involutivas
<!-- TODO Diagramas -->

## Agregación y composición
Agregación: Un elemento se compone de varios opcionales (Rombo vació)  
Composición: Un elemento cuyos componente dependen fuertemente del padre (Rombo relleno)  

## Dependencia
Relación (más débil que una asociación) que se representa con una linea discontinua con terminación en punta de flecha en dirección a la clase que es "usada". Esta relación tabién es conocida como relación de uso.

## Herencia (Generalización y Especialización)
Esta relación se lleva a cabo entre súper clases y subclases. La noción de clase está próxima a la de conjunto: Generalización y especialización expresan relaciones de inclsón entre conjuntos.
<!-- TODO diagrama -->
El nombre de la clase, en UML, se pone en cursiva

# Tarjetas CRC
Instrumento utilizado en la fase inicial del diseño arquitectónico, es utilizado como preámbulo al diagrama de clases de UML; no forma parte de alguna metodología de diseño, pero es considerado como una buena practica en la arquitectura de software.

# Diccionario de Clases
Instrumento utilizado en la definición de entidades, en el cual se describe su naturaleza, características y comportamiento; se elabora como parte del proceso del diseño arquitectónico y es el resultado de las definiciones obtenidas en la etapa de análisis. Se realiza siempre en lenguaje natural. Ejemplo:  
**Cliente**: Persona física o moral de la cual interesa almacenar sus datos fiscales, así como aquellos necesarios para su contacto. A través de este elemento se realizan transacciones comerciales y se generan documentos tales como 'tickets', 'facturas', 'notas de crédito' y 'notas de cargo'. Los datos y documentos que se generan a través de solicitudes de esta entidad o como resultado de las operaciones que se realzaron, deberán estar disponibles; a través de reportes.

# Casos de uso
Un caso de uso es una descripción de un conjunto de secuencias de acciones, incluyendo variantes, que ejecuta un sistema para producir un resultado observable de valor para un actor.

# Diagrama de secuencia
*   Indica el flujo de comunicación.
*   Cada columna representa una instancia.

# Diagrama de colaboración
*   Grafo numerado

# Diagrama de estados
Modela los estados de un objeto o caso de uso.

# Diagrama de actividades
Representa el sistema de acuerdo al modelo de negocios.

# Diagrama de componentes
Normalmente, se realiza uno por sistema.
Utiliza puertos, círculos y semi-círculos.

# Diagrama de despliegue
Modela la arquitectura en tiempo de ejecución de un sistema. Esto muestra la configuración de los elementos de hardware (nodos) y muestra como los elementos y artefactos del software se trazan en esos nodos.
## Nodo
Elemento de hardware o software. Esto se muestra con la forma de una caja tridimensional.
## Instancia de Nodo
El nombre está subrayado y tiene dos puntos antes del tipo de nodo base. Una instancia puede o no tener un nombre antes de os dos puntos.
## Estereotipo de Nodo
Un número de estereotipos estándar se proveen para los nodos. Estos mostrarán un icono apropiado en la esquina superior derecha del símbolo nodo.
## Artefacto
Producto del proceso de desarrollo de software, que puede incluir los modelos del proceso, archivos fuente, ejecutables, documentos de diseño, reportes de prueba, prototipos, manuales de usuario, etc. Se denota por un rectángulo mostrando el nombre del artefacto del estereotipo <<artifact>> y un icono de documento.
## Asociación
Ruta de comunicación entre los nodos. Multiplicidad en los extremos de la asociación.
## Nodo como contenedor
Un nodo puede contener otros elementos.como componentes o artefactos.

# Pruebas Unitarias
Prueba el correcto funcionamiento de una unidad o modulo.
## Características
*   Automatizadas
*   Completas
*   Repetibles
*   Independientes

## Puntos importantes
*   Datos de entrada conocidos
*   Conocer que componentes interactúan
*   Saber el resultado esperado
*   Comparar los resultados obtenidos con los esperados

## Ventajas
*   Fomenta el cambio
*   Simplifica integración
*   Documenta el código

## Puntos a probar
*   Cardinalidad
*   Orden
*   Rango
*   Referencia
*   Existencia
*   Conformidad
*   Tiempo
*   Otras

# Pruebas de intregración
Se realizan cuando todas las pruebas unitarias han sido realizadas.
Se realiza para comprobar la correcta comunicación entre los módulos.
## Prueba de caja negra
Solo revisa que la salida corresponda a la entrada.
## Ventajas
*   Pruebas paralelas

# Prueba de regresión
Se asegura que el nuevo código no tenga efectos secundarios en el código anterior.

# Prueba de Humo
Se asegura que la funcionalidad básica del software sea correcto.
Se origino en la industria ya que si al probar componentes no salia humo se decía que era exitosa.
Se realiza sobre software no estable.
Su objetivo es comprobar que ciertos caminos son correctos mediante el uso de caja negra.
## Ventajas
*   Permite saber si tras agregar un nuevo modulo el sistema sigue funcionando.

# Prueba de desempeño
Mide e desempeño el software en tiempo de ejecución.
Mide tiempos de respuesta.
Utiliza técnicas de caja negra y caja blanca.
Se realizan después de las pruebas de funcionalidad e integración.
## Posibles afectaciones
*   Errores lógicos
*   Cuellos de botella
*   Capacidad de almacenamiento
*   Tasa de entrada/salida de datos

## Relización
Se compara una prueba a su carga normal contra una con una carga total del sistema.

# Prueba de estrés
Encuentra el volumen de datos o de tiempo en el que el sistema comienza a fallar o es incapaz de responder.
Se realizan sobre escenarios atípicos donde la afluencia es mayor al promedio.
Reduce e riesgo de sobre cargar el sistema final.
Provee información de los limites del mismo.


# Pruebas de volumen
Gran cantidad de datos.
Verifica si el sistema es estable en un periodo prolongado y con varias entradas.
Determina cuando el sistema falla.
Gran cantidad de operaciones simultaneas.

# Pruebas de Integridad de Datos y BD
Asegura que los métodos de acceso y procesos no provocan corrupción en los datos.
Garantiza la compatibilidad de los datos entre distintas versiones de software y/o hardware.
Se aconseja hacer modificaciones en las BD y verificar que esto se haya hecho correctamente en la BD.
Intentar hacer movimientos inválidos, para ver el comportamiento del sistema.

# Pruebas de Seguridad de Acceso
Vigila acceso a datos concisos o al propio sistema.
## Nivel Aplicación
*   Identifica grupos de usuarios y las características a las que puede acceder.

## Nivel Sistema
*   Autenticación para acceder al sistema.

# Pruebas del Ciclo de Negocio
Prueban el ciclo "natural" del negocio dentro del nuevo sistema.
La técnica más común es introducir datos validos e inválidos, sobre todo lo referente a fechas o periodos de tiempo.

# Prueba de GUI
Comprueba la interfaz de usuario para que la misma se acople a lo solicitado.
Revisa la funcionalidad y usabilidad de la GUI.

# Prueba de Configuración
Verifica la operación del sistema en sobre distintas configuraciones o escenarios.
