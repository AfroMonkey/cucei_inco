| Actividad | Predecesora | TE |  
|:----------|:-----------:|:--:|
| A         | N/A         | 20 |  
| B         | A           | 10 |  
| C         | B           | 8  |  
| D         | A           | 11 |  
| E         | C,D         | 7  |  
| F         | E           | 6  |  
| G         | D           | 12 |  
| H         | E           | 13 |  
| I         | G,H         | 5  |  

1. Diagrama de Pert
2. Todas las posibles rutas (y cuando dura)
3. Ruta critica
