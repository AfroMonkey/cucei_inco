import java.util.Scanner;


public class Main {
  static Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    Customer[] customers = new Customer[5];
    boolean[] customersB = new boolean[5];
    Supplier[] suppliers = new Supplier[5];
    boolean[] suppliersB = new boolean[5];
    int opt, idx;
    
    for (int i = 0; i < 5; ++i) {
      customers[i] = new Customer();
      suppliers[i] = new Supplier();
      customersB[i] = false;
      suppliersB[i] = false;
    }

    while(true) {
      System.out.println("1) Manage customers");
      System.out.println("2) Manage suppliers");
      System.out.println("0) Exit");
      opt = scanner.nextInt();
      scanner.reset();
      if (opt == 0) {
        return;
      }
      opt *= 10;

      System.out.println("1) Fill");
      System.out.println("2) Show");
      System.out.println("3) Modify");
      System.out.println("4) Delete");
      opt += scanner.nextInt();
      scanner.reset();

      System.out.println("Index (0-4)");
      idx = scanner.nextInt();
      scanner.reset();

      if (0 <= idx && idx <= 4) {
        switch(opt) {
          case 11:
            if (!customersB[idx]) {
              customers[idx].fill();
              customersB[idx] = true;
            } else {
              System.out.println("Already filled");
            }
            break;
          case 12:
            if (customersB[idx]) {
              customers[idx].show();
            } else {
              System.out.println("No filled");
            }
            break;
          case 13:
            if (customersB[idx]) {
              System.out.println("Current data: ");
              customers[idx].show();
              customers[idx].fill();
            } else {
              System.out.println("No filled");
            }
            break;
          case 14:
            if (customersB[idx]) {
              customersB[idx] = false;
            } else {
              System.out.println("No filled");
            }
            break;
          case 21:
            if (!suppliersB[idx]) {
              suppliers[idx].fill();
              suppliersB[idx] = true;
            } else {
              System.out.println("Already filled");
            }
            break;
          case 22:
            if (suppliersB[idx]) {
              suppliers[idx].show();
            } else {
              System.out.println("No filled");
            }
            break;
          case 23:
            if (suppliersB[idx]) {
              System.out.println("Current data: ");
              suppliers[idx].show();
              suppliers[idx].fill();
            } else {
              System.out.println("No filled");
            }
            break;
          case 24:
            if (suppliersB[idx]) {
              suppliersB[idx] = false;
            } else {
              System.out.println("No filled");
            }
            break;
          default:
            System.out.println("Invalid option");
        }
      } else {
        System.out.println("Invalid index");
      }
    }
  }
}


abstract class Person {
  private String name;
  private String address;
  private String tel;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getTel() {
    return tel;
  }

  public void setTel(String tel) {
    this.tel = tel;
  }
  
  public abstract void fill();
  
  public abstract void show();
}


class Customer extends Person {
  static Scanner scanner = new Scanner(System.in);
  
  public Customer() {}
  
  public void fill() {
    System.out.println("**Customer**");
    System.out.print("Name>");
    setName(scanner.next());
    System.out.print("Address>");
    setAddress(scanner.next());
    System.out.print("Tel>");
    setTel(scanner.next());
  }
  
  public void show() {
    System.out.println("**Customer**");
    System.out.println("Name: " + getName());
    System.out.println("Address: " + getAddress());
    System.out.println("Tel: " + getTel());
  }
}


class Supplier extends Person {
  static Scanner scanner = new Scanner(System.in);
  
  private String rfc;
  
  public void setRfc(String rfc) {
    this.rfc = rfc;
  }
  
  public String getRfc() {
    return rfc;
  }
  
  public Supplier() {}
  
  public void fill() {
    System.out.println("**Supplier**");
    System.out.print("Name>");
    setName(scanner.next());
    System.out.print("Address>");
    setAddress(scanner.next());
    System.out.print("Tel>");
    setTel(scanner.next());
    System.out.print("RFC>");
    setRfc(scanner.next());
  }
  
  public void show() {
    System.out.println("**Supplier**");
    System.out.println("Name: " + getName());
    System.out.println("Address: " + getAddress());
    System.out.println("Tel: " + getTel());
    System.out.print("RFC>" + getRfc());
  }  
}
