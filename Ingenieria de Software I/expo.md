# Mantenimiento correctivo
###### Lunes 24

*   ISO y IEEE, bitácoras, instrumentos, etc.


*   Definición
*   Ejemplos
*   Aplicación
*   Evaluación

## Enlaces
*   <https://www.iso.org/obp/ui/#iso:std:iso-iec:14764:ed-2:v1:en>
*   <http://digital.cs.usu.edu/~xqi/Teaching/CS2450F08/Notes/Ch15.Maintenance.pdf>
*   <https://books.google.com.mx/books?id=N69KPjBEWygC&pg=PA267&lpg=PA267&dq=Corrective+maintenance+in+software+engineering&source=bl&ots=8N7mQ3tohj&sig=g4JCbtfabYIzqit6LTISNHnXeb4&hl=es&sa=X&redir_esc=y#v=onepage&q=Corrective%20maintenance%20in%20software%20engineering&f=false>
*   <http://ocw.unican.es/ensenanzas-tecnicas/ingenieria-del-software-ii/materiales/tema8-mantenimientoSistemasSoftware.pdf>
*   <http://informatica.uv.es/iiguia/2000/IPI/material/tema7.pdf>

## Posibles enlaces
*   <http://ecomputernotes.com/software-engineering/tools-for-software-maintenance>
