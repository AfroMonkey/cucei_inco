#!/bin/bash

# Gestor de Copias de Seguridad v1.1

alias=$1
file='archivo.zip'

if [[ ! -n "$alias" ]]; then
  echo "Ingrese su alias"
  exit 1
fi

timestamp=$(date +%Y_%m_%d_%H_%M -u)
cp $file $1_$timestamp
if [ $? -eq 0 ]; then
  echo "Respaldo realizado"
else
  echo "Fallo en el respaldo, notifique al administrador"
fi
