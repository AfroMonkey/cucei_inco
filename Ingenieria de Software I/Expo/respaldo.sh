#!/bin/bash

# Gestor de Copias de Seguridad v1.0

alias=$1
file='archivo.zip'

if [[ ! -n "$alias" ]]; then
  echo "Ingrese su alias"
  exit 1
fi

timestamp=$(date +%Y_%m_%d_%H_%M)
cp $file $1_$timestamp
echo "Respaldo ralizado"
