#include <iostream>
#include <cstdlib>
#include "tombstone.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

enum Options {
    // Main menu
    INSERT = 1,
    SHOW = 2,
    REMOVE = 3,
    EXIT = 0
};

void ShowMenu() {
    cout << INSERT << ") Agregar tumba" << endl;
    cout << SHOW << ") Ver tumbas" << endl;
    cout << REMOVE << ") Eliminar tumba" << endl;
    cout << EXIT << ") Salir" << endl;
    cout << ">";
}

int GetInt(int def) {
    int i;
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore(); //cin.todo
        i = def;
    }
    cin.ignore();
    return i;
}

void Msg(const string &msg) {
    cout << msg;
}

void ClearScreen() {
    system(CLEAR);
}

void Pause() {
    cout << "Presion entrar para continuar . . .";
    cin.ignore();
}

Tombstone ReadTombstone() {
    Tombstone tombstone;

    cout << "Nombre del difunto>";
    getline(cin, tombstone.name);
    cout << "Fecha de nacimiento (aaaammdd)>";
    cin.getline(tombstone.dateBirth, 9);
    cout << "Fecha de fallecimiento (aaaammdd)>";
    cin.getline(tombstone.dateDeath, 9);
    cout << "Numero de tumba>";
    tombstone.number = GetInt(-1);
    
    return tombstone;
}

void ShowTombstone(Tombstone tombstone) {
    cout << "Nombre: " << tombstone.name << endl;
    cout << "Fecha de nacimiento: " << tombstone.dateBirth << endl;
    cout << "Fecha de fallecimiento: " << tombstone.dateDeath << endl;
    cout << "Numero de tumba: " << tombstone.number << endl;
    cout << endl;
}