/*
 * Author: Navarro Presas Moises Alejandro
 * Date: 2016-01-27
 */

#include "main.cli.h"
#include "tombstone.h"
#include "staticList.h"

#define SIZE 10

void Insert(StaticList<Tombstone> &list);
void Show(StaticList<Tombstone> &list);
void Remove(StaticList<Tombstone> &list);

int main() {
    StaticList<Tombstone> list(SIZE);
    int opt;
    do {
        ShowMenu();
        opt = GetInt(-1);
        switch(opt) {
            case INSERT:
                Insert(list);
                break;
            case SHOW:
                Show(list);
                break;
            case REMOVE:
                Remove(list);
                break;
            case EXIT:
                break;
            default:
                Msg("Opcion invalida\n");
                break;
        }
        if(opt != EXIT) {
            Pause();
        }
        ClearScreen();
    } while(opt != EXIT);
    return 0;
}

void Insert(StaticList<Tombstone> &list) {
    Tombstone tombstone = ReadTombstone();
    int position;

    Msg("Posicion>");
    position = GetInt(-1);

    if(list.Insert(position, tombstone)) {
        Msg("Tumba insertada\n");
    } else {
        Msg("Error, compruebe que la lista no este llena o haya seleccionado una posicion fuera del rango\n");
    }
}

void Show(StaticList<Tombstone> &list) {
    int last = list.last();
    for(int i = 0; i <= last; i++) {
        ShowTombstone(list.get(i));
    }
}

void Remove(StaticList<Tombstone> &list) {
    int position;

    Msg("Posicion>");
    position = GetInt(-1);
    if(list.Remove(position)) {
        Msg("Tumba eliminada\n");
    } else {
        Msg("Error, no se pudo eliminar esa tumba\n");
    }
}
