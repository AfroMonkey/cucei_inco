#ifndef STATICLIST_H
#define STATICLIST_H
template <class T>
class StaticList {
private:
    T *list;
    int size;
    int end;
public:
    StaticList(int size);
    ~StaticList();

    bool isEmpty();
    bool isFilled();
    int first();
    int last();
    bool Insert(int position, T data);
    bool Remove(int position);
    T get(int position);
};

template <class T>
StaticList<T>::StaticList(int size) {
    this->size = size;
    list = new T[size];
    end = -1;
}

template <class T>
StaticList<T>::~StaticList() {
    delete[] list;
}

template <class T>
bool StaticList<T>::isEmpty() {
    return end == -1;
}

template <class T>
bool StaticList<T>::isFilled() {
    return end == size-1;
}

template <class T>
int StaticList<T>::first() {
    return isEmpty()? -1 : 0;
}

template <class T>
int StaticList<T>::last() {
    return end;
}

template <class T>
bool StaticList<T>::Insert(int position, T data) {
    if(position >= 0 && position <= end+1 && !isFilled()) {
        for(int i = end; i >= position; i++) {
            list[i+1] = list[i];
        }
        list[position] = data;
        end++;
        return true;
    } else {
        return false;
    }
}

template <class T>
bool StaticList<T>::Remove(int position) {
    if(position >= 0 && position <= end && !isFilled()) {
        for(int i = position; i < end; i++) {
            list[i] = list[i+1];
        }
        end--;
        return true;
    } else {
        return false;
    }   
}

template <class T>
T StaticList<T>::get(int position) {
    return list[position];
}
#endif
