#ifndef ALBUM_H
#define ALBUM_H

#include "song.h"
#include "single_linked_list.h"

class Album
{
private:
    std::string artist_;
    std::string genre_;
    SingleLinkedList<Song> songs_;
public:
    Album();
    Album(std::string artist, std::string genre);

    void set_artist(std::string artist);
    std::string get_artist();
    void set_genre(std::string genre);
    std::string get_genre();

    void add_song(Song song);
    bool remove_song(std::string name);
    void remove_all_songs();

    int get_duration();
};

Album::Album()
{
    artist_ = "";
    genre_ = "";
}

Album::Album(std::string artist, std::string genre)
{
    artist_ = artist;
    genre_ = genre;
}

void Album::set_artist(std::string artist)
{
    artist_ = artist;
}

std::string Album::get_artist()
{
    return artist_;
}

void Album::set_genre(std::string genre)
{
    genre_ = genre;
}

std::string Album::get_genre()
{
    return genre_;
}


void Album::add_song(Song song)
{
    //TODO
}

bool Album::remove_song(std::string name)
{
    //TODO
}

void Album::remove_all_songs()
{
    //TODO
}


int Album::get_duration()
{
    //TODO
}


#endif
