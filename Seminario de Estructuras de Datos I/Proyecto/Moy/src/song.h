#ifndef SONG_H
#define SONG_H

#include <iostream>

class Song
{
private:
    std::string name_;
    int duration_;
public:
    Song();
    Song(std::string name, int duration);

    void set_name(std::string name);
    std::string get_name();
    bool set_duration(int duration);
    int get_duration();
};

Song::Song()
{
    name_ = "";
    duration_ = -1;
}

Song::Song(std::string name, int duration)
{
    name_ = name;
    if (duration > 0)
    {
        duration_ = duration;
    }
}

void Song::set_name(std::string name)
{
    name_ = name;
}

std::string Song::get_name()
{
    return name_;
}

bool Song::set_duration(int duration)
{
    if (duration > 0)
    {
        duration_ = duration;
        return true;
    }
    else
    {
        return false;
    }
}

int Song::get_duration()
{
    return duration_;
}


#endif
