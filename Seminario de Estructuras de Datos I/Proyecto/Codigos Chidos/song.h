#define SONG_H
#ifndef SONG_H

class Song
{
public:
	Song();
	std::string get_title() { return title_; }
	void set_title(std::string title) { title_ = title; }
	std::string get_duration() { return duration_; }
	bool set_duration(std::string duration);
private:
	std::string title_;
	std::string duration_;
};

bool Song::set_duration(std::string duration)
{
	// TODO
}

#endif
