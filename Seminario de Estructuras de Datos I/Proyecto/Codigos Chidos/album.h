#define ALBUM_H
#ifndef ALBUM_H

class Album
{
public:
	Album();
	std::string get_title() { return title_; }
	void set_title(std::string title) { title_ = title; } 
private:
	std::string title_;
	// List
};

#endif
