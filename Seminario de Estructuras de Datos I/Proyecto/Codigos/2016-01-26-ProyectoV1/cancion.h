#ifndef CANCION_H_INCLUDED
#define CANCION_H_INCLUDED

using namespace std;

class Cancion
{
private:
    string nombre;
    int duracion;
public:
    Cancion()
    {
        nombre="Empty";
        duracion=0;
    }
    void fijaNombre (string nombre)
    {
        this->nombre=nombre;
    }
    void fijaDuracion (int duracion)
    {
        this->duracion=duracion;
    }
};

#endif
