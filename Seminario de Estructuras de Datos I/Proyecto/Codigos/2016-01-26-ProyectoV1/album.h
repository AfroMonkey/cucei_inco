#ifndef ALBUM_H_INCLUDED
#define ALBUM_H_INCLUDED

using namespace std;

class Album
{
private:
    string nombre, interprete, genero;
public:
    Album()
    {
        nombre="Empty";
        interprete="Empty";
        genero="Empty";
    }
    void fijaNombre(string nombre)
    {
        this->nombre=nombre;
    }
    void fijaInterprete (string interprete)
    {
        this->interprete=interprete;
    }
    bool fijaGenero (string genero)
    {
        if (genero=="Acid-Jazz" || genero=="Dance" || genero=="Deep" || genero=="House" ||
                genero=="Pop" || genero=="Braindance" || genero=="Experimental")
        {
            this->genero=genero;
            return true;
        }
        else
        {
            return false;
        }
    }
};

#endif
