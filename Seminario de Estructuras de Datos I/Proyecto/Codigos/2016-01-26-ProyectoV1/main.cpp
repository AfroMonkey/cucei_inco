#include <iostream>
#include <cstdlib>
#include "usuario.h"
#include "album.h"
#include "cancion.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif // _WIN32

using namespace std;

void pausa();

int main ()
{
    Usuario u1;
    string password, busqueda, nombre, interprete, genero, album, duracion, ruta;
    int opc, opc2, opc3, opc4;
    char c;
    do
    {
        cout<<"**Rocola v1.0**"<<endl;
        cout<<"Password: ";
        cin>>password;
        if (u1.fijaPassword(password))
        {
            do
            {
                system(CLEAR);
                cout<<"Men\243 principal"<<endl;
                cout<<"1. Mostrar \240lbumes (alfab\202ticamente)."<<endl;
                cout<<"2. Mostrar \240lbumes (g\202nero)."<<endl;
                cout<<"3. Buscar \240lbum."<<endl;
                cout<<"4. Administrar."<<endl;
                cout<<"0. Salir."<<endl;
                cout<<"Opci\242n: ";
                cin>>opc;
                system(CLEAR);
                switch(opc)
                {
                case 1:
                    cout<<"1. Mostrar \240lbumes alfab\202ticamente."<<endl;
                    cout<<"No. \t Nombre \t Int\202rprete \t G\202nero "<<endl;
                    cout<<"N\242mero del \240lbum: ";
                    cin>>opc;
                    break;
                case 2:
                    do
                    {
                        cout<<"2. Mostrar \240lbumes (g\202nero)"<<endl<<endl;
                        cout<<"1. Acid-Jazz."<<endl;
                        cout<<"2. Dance."<<endl;
                        cout<<"3. Deep. "<<endl;
                        cout<<"4. House. "<<endl;
                        cout<<"5. Pop. "<<endl;
                        cout<<"6. Braindance. "<<endl;
                        cout<<"7. Experimental. "<<endl;
                        cout<<"0. Cancelar. "<<endl;
                        cout<<"Opci\242n: ";
                        cin>>opc2;
                        switch(opc2)
                        {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            system(CLEAR);
                            cout<<"1. Mostrar \240lbumes alfab\202ticamente."<<endl;
                            cout<<"No. \t Nombre \t Int\202rprete \t G\202nero "<<endl;
                            cout<<"N\242mero del \240lbum: ";
                            cin>>opc;
                            pausa();
                            system(CLEAR);
                        case 0:
                            break;
                        default:
                            cout<<"Opci\242n inv\240lida.";
                            pausa();
                            system(CLEAR);
                            break;
                        }
                    }
                    while (opc2!=0);
                    break;
                case 3:
                    cout<<"3. Buscar \240lbum. "<<endl;
                    cout<<"Nombre del \240lbum: ";
                    cin>>busqueda;
                    break;
                case 4:
                    cout<<"4. Administrar "<<endl;
                    cout<<"Password: ";
                    cin>>password;
                    system(CLEAR);
                    if (u1.fijaPassword(password))
                    {
                        do
                        {
                            cout<<"1. Administrar \240lbumes. "<<endl;
                            cout<<"2. Administrar canciones. "<<endl;
                            cout<<"3. Almacenamiento secundario. "<<endl;
                            cout<<"4. Limpiar base de datos. "<<endl;
                            cout<<"0. Cancelar. "<<endl;
                            cout<<"Opci\242n: ";
                            cin>>opc3;
                            system(CLEAR);
                            switch (opc3)
                            {
                            case 1:
                                do
                                {
                                    system(CLEAR);
                                    cout<<"4.1. Administrar \240lbumes. "<<endl;
                                    cout<<"1. Agregar \240lbum. "<<endl;
                                    cout<<"2. Modificar \240lbum. "<<endl;
                                    cout<<"3. Eliminar \240lbum. "<<endl;
                                    cout<<"0. Cancelar. "<<endl;
                                    cout<<"Opci\242n: ";
                                    cin>>opc4;
                                    system(CLEAR);
                                    switch(opc4)
                                    {
                                    case 1:
                                        system(CLEAR);
                                        cout<<"4.1.1. Agregar \240lbum. "<<endl;
                                        cout<<"Nombre: ";
                                        cin>>nombre;
                                        cout<<"Int\202rprete: ";
                                        cin>>interprete;
                                        cout<<"G\202nero: ";
                                        cin>>genero;
                                        break;
                                    case 2:
                                        cout<<"Nombre: ";
                                        cin>>nombre;
                                        break;
                                    case 3:
                                        do
                                        {
                                            cout<<"4.1.3. Eliminar \240lbum. "<<endl;
                                            cout<<"Nombre: ";
                                            cin>>nombre;
                                            if (nombre=="valido")
                                            {
                                                cout<<"Seguro que desea eliminar? (s/n)";
                                                cin>>c;
                                            }
                                        }
                                        while (nombre!="valido");
                                        break;
                                    case 0:
                                        break;
                                    default:
                                        cout<<"Opci\242n inv\240lida.";
                                        pausa();
                                        system(CLEAR);
                                        break;
                                    }
                                }
                                while (opc4!=0);
                                break;
                            case 2:
                                do
                                {
                                    system(CLEAR);
                                    cout<<"4.2. Administrar canciones. "<<endl;
                                    cout<<"1. Agregar canci\242n. "<<endl;
                                    cout<<"2. Modificar canci\242n. "<<endl;
                                    cout<<"3. Eliminar canci\242n. "<<endl;
                                    cout<<"0. Cancelar. "<<endl;
                                    cout<<"Opci\242n: ";
                                    cin>>opc;
                                    system(CLEAR);
                                    switch(opc)
                                    {
                                    case 1:
                                        cout<<" 4.2.1. Agregar canci\242n. "<<endl;
                                        cout<<"Album: ";
                                        cin>>album;
                                        cout<<"Nombre: ";
                                        cin>>nombre;
                                        cout<<"Duraci\242n: ";
                                        cin>>duracion;
                                        break;
                                    case 2:
                                        cout<<" 4.2.2. Modificar canci\242n. "<<endl;
                                        cout<<"Album: ";
                                        cin>>album;
                                        cout<<"Nombre: ";
                                        cin>>nombre;
                                        break;
                                    case 3:
                                        cout<<" 4.2.3.Eliminar canci\242n. "<<endl;
                                        cout<<"Album: ";
                                        cin>>album;
                                        cout<<"Nombre: ";
                                        cin>>nombre;
                                        break;
                                    case 0:
                                        break;
                                    default:
                                        cout<<"Opci\242n inv\240lida.";
                                        pausa();
                                        system(CLEAR);
                                        break;
                                    }
                                }
                                while (opc!=0);
                                break;
                            case 3:
                                do
                                {
                                    system(CLEAR);
                                    cout<<"4.3. Almacenamiento secundario. "<<endl;
                                    cout<<"1. Exportar. "<<endl;
                                    cout<<"2. Importar. "<<endl;
                                    cout<<"0. Cancelar. "<<endl;
                                    cout<<"Opci\242n: ";
                                    cin>>opc;
                                    system(CLEAR);
                                    switch(opc)
                                    {
                                    case 1:
                                        cout<<"4.3.1. Exportar "<<endl;
                                        cout<<"Ruta: ";
                                        cin>>ruta;
                                        break;
                                    case 2:
                                        cout<<"4.3.2. Importar "<<endl;
                                        cout<<"Ruta: ";
                                        cin>>ruta;
                                        break;
                                    case 0:
                                        break;
                                    default:
                                        cout<<"Opci\242n inv\240lida.";
                                        pausa();
                                        system(CLEAR);
                                        break;
                                    }
                                }
                                while (opc!=0);
                                break;
                            case 4:
                                cout<<"4.4. Limpiar base de datos. "<<endl;
                                cout<<"Advertencia, esto eliminar\242 todos los registros, ";
                                cout<<"desea continuar (s/n): ";
                                cin>>c;
                                if (c=='s')
                                {
                                    cout<<"Password: ";
                                    cin>>password;
                                    if (u1.fijaPassword(password))
                                    {
                                        cout<<"Operaci\242n realizada exitosamente."<<endl;
                                    }
                                }
                                else
                                {
                                    cout<<"Operaci\242n abortada."<<endl;
                                }
                                pausa();
                                system(CLEAR);
                                break;
                            case 0:
                                break;
                            default:
                                cout<<"Opci\242n inv\240lida.";
                                pausa();
                                system(CLEAR);
                                break;
                            }

                        }
                        while (opc3!=0);
                    }
                    break;
                case 0:
                    break;
                default:
                    cout<<"Opci\242n inv\240lida.";
                    pausa();
                    break;
                }
            }
            while (opc!=0);
        }
        else
        {
            cout<<"Contrase\244a incorrecta"<<endl;
            pausa();
            system(CLEAR);
        }
    }
    while (!u1.fijaPassword(password));

    return 0;
}

void pausa()
{
    cin.get();
    cout<<endl<<"Presione entrar para continuar...";
    cin.get();
}
