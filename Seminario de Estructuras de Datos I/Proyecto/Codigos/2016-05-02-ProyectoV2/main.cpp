#include <iostream>
#include "doubly_linked_list.h"
#include "rockola_cli.h"
#include "album.h"
#include "song.h"

int CmpAlbumTitle(std::string title, Album &album);
int CmpAlbumToAlbumTitle(Album &album, Album &other);
int CmpAlbumToAlbumDuration(Album &album, Album &other);

#define PASSWORD "" //TODO

int main()
{
    int opt;
    bool change_opt = true;
    DoublyLinkedList<Album> albums;

    LoginScreen();
    if(GetStringFromUser() != PASSWORD) return 1;

    do {
        MainMenu();
        Album album;
        Song song;
        opt = change_opt? GetOption() : opt;
        switch(opt)
        {
            case LIST_ALBUMS:
                change_opt = true;
                PrintHeader("VER ALBUMS");
                if (!albums.empty())
                {
                    SortAlbumsMenu();
                    switch(GetOption())
                    {
                        case NAME:
                            PrintHeader("VER ALBUMS (ORD. NOMBRE)");
                            albums.sort(CmpAlbumToAlbumTitle);
                            PrintAlbums(albums);
                            Pause();
                            break;
                        case LENGTH:
                            PrintHeader("VER ALBUMS (ORD. DURACION)");
                            Pause();
                            break;
                        case GO_BACK_SORT:
                            break;
                        default:
                            change_opt = false;
                            OptionNotFound();
                    }
                }
                else
                {
                    std::cout << " No hay ningun album.\n";
                    Pause();
                }
                break;
            case ADD_ALBUM:
                PrintHeader("AGREGAR ALBUM");
                SetAlbum(album);
                if (albums.empty())
                {
                    album.set_id(0);
                }
                else
                {
                    album.set_id(albums.get(albums.end())->get_id() + 1);
                }
                albums.append(album);
                std::cout << "\n Album agregado.\n";
                Pause();
                break;
            case SEARCH_ALBUM:
                PrintHeader("BUSCAR ALBUM");
                if (!albums.empty())
                {
                    int results = 0;
                    std::string title;
                    std::cout << " Ingresa el titulo del album: ";
                    title = GetStringFromUser();
                    for (dll_iterator it = albums.begin(); it; it = it->next_)
                    {
                        Album *holder = albums.get(it);
                        if (CmpAlbumTitle(title, *holder) == 0)
                        {
                            PrintAlbum(*holder);
                            results++;
                        }
                    }
                    if (!results)
                    {
                        std::cout << " No hay ningun album con ese nombre.\n";
                    }
                }
                else
                {
                    std::cout << " No hay ningun album.\n";
                }
                Pause();
                break;
            case EDIT_ALBUM:
                PrintHeader("MODIFICAR ALBUM");
                if (!albums.empty())
                {
                    int results = 0;
                    std::string title;
                    std::cout << " Ingresa el titulo del album: ";
                    title = GetStringFromUser();
                    for (dll_iterator it = albums.begin(); it; it = it->next_)
                    {
                        Album *holder = albums.get(it);
                        if (CmpAlbumTitle(title, *holder) == 0)
                        {
                            results++;
                            PrintAlbum(*holder);
                            std::cout << " Editar este album? (y/n): ";
                            if (YesOrNo())
                            {
                                Album a = *holder;
                                albums.erase(it);
                                EditAlbum(a);
                                albums.append(a);
                                std::cout << " Album modificado.\n";
                                break;
                            }
                            else
                            {
                                std::cout << " Se ha conservado el album.\n";
                            }
                        }
                    }
                    if (!results)
                    {
                        std::cout << " No hay ningun album con ese nombre.\n";
                    }
                }
                else
                {
                    std::cout << " No hay ningun album.\n";
                }
                Pause();
                break;
            case DEL_ALBUM:
                PrintHeader("ELIMINAR ALBUM");
                if (!albums.empty())
                {
                    int results = 0;
                    std::string title;
                    std::cout << " Ingresa el titulo del album: ";
                    title = GetStringFromUser();
                    for (dll_iterator it = albums.begin(); it; it = it->next_)
                    {
                        Album *holder = albums.get(it);
                        if (CmpAlbumTitle(title, *holder) == 0)
                        {
                            results++;
                            PrintAlbum(*holder);
                            std::cout << " Borrar este album? (y/n): ";
                            if (YesOrNo())
                            {
                                albums.erase(it);
                                std::cout << " Album eliminado.\n";
                                break;
                            }
                            else
                            {
                                std::cout << " Se ha conservado el album.\n";
                            }
                        }
                    }
                    if (!results)
                    {
                        std::cout << " No hay ningun album con ese nombre.\n";
                    }
                }
                else
                {
                    std::cout << " No hay ningun album.\n";
                }
                Pause();
                break;
            case DEL_ALL:
                PrintHeader("ELIMINAR TODOS");
                if (!albums.empty())
                {
                    std::cout << " Esto eliminara todos los albumes y canciones del programa.\n";
                    std::cout << " Estas seguro que quieres eliminar todo? (y/n): ";
                    if (YesOrNo())
                    {
                        for (dll_iterator it = albums.begin(); it; it = it->next_)
                        {
                            // clear songs
                        }
                        albums.clear();
                        std::cout << " Toda la informacion ha sido eliminada.\n";
                    }
                }
                else
                {
                    std::cout << " No hay ningun album.\n";
                }
                Pause();
                break;
            case SONGS_MENU:
                if (albums.empty())
                {
                    std::cout << " No hay ningun album.\n";
                    Pause();
                    break;
                }
                do {
                    PrintHeader("ADMINISTRAR CANCIONES");
                    SongsMenu();
                    opt = GetOption();
                    switch(opt)
                    {
                        case SHOW_SONGS: {
                            std::cout << " Ingresa el ID del album: ";
                            int id = GetIntFromUser();
                            int results = 0;
                            for (dll_iterator it = albums.begin(); it; it = it->next_)
                            {
                                Album *holder = albums.get(it);
                                if (id == holder->get_id())
                                {
                                    PrintAlbum(*holder);
                                    holder->print_songs();
                                    results++;
                                    break;
                                }
                            }
                            if (!results)
                            {
                                std::cout << " No hay ningun album con ese ID.\n";
                            }
                            Pause();
                            break;
                        }

                        case ADD_SONG: {
                            std::cout << " Ingresa el ID del album: ";
                            int id = GetIntFromUser();
                            int results = 0;
                            for (dll_iterator it = albums.begin(); it; it = it->next_)
                            {
                                Album *holder = albums.get(it);
                                if (id == holder->get_id())
                                {
                                    results++;
                                    PrintAlbum(*holder);
                                    std::cout << " Seguro que este es el album deseado? (y/n) \n";
                                    if (YesOrNo())
                                    {
                                        SetSong(song);
                                        holder->append(song);
                                        holder->update_duration();
                                        std::cout << "\n Cancion agregada.\n";
                                        break;
                                    }
                                }
                            }
                            if (!results)
                            {
                                std::cout << " No hay ningun album con ese ID.\n";
                            }
                            Pause();
                            break;
                        }
                        case EDIT_SONG: {
                            std::cout << " Ingresa el ID del album: ";
                            int id = GetIntFromUser();
                            int results = 0;
                            for (dll_iterator it = albums.begin(); it; it = it->next_)
                            {
                                Album *holder = albums.get(it);
                                if (id == holder->get_id())
                                {
                                    results++;
                                    PrintAlbum(*holder);
                                    std::cout << " Seguro que este es el album deseado? (y/n) \n";
                                    if (YesOrNo())
                                    {
                                        std::cout << "Ingrese el titulo de la cancion" << "\n";
                                        Song *aux = holder->get_song(GetStringFromUser());
                                        if (aux)
                                        {
                                            PrintSong(*aux);
                                            std::cout << "Seguro que quiere modificar esta cancion? (y/n)" << "\n";
                                            if (YesOrNo())
                                            {
                                                SetSong(*aux);
                                                std::cout << "Cancion modificada\n";
                                                holder->update_duration();
                                            }
                                        }
                                        else
                                        {
                                            std::cout << " No hay ninguna cancion con ese nombre en este album\n";
                                        }
                                        break;
                                    }
                                }
                            }
                            if (!results)
                            {
                                std::cout << " No hay ningun album con ese ID.\n";
                            }
                            Pause();
                            break;
                        }
                        case DEL_SONG: {
                            std::cout << " Ingresa el ID del album: ";
                            int id = GetIntFromUser();
                            int results = 0;
                            for (dll_iterator it = albums.begin(); it; it = it->next_)
                            {
                                Album *holder = albums.get(it);
                                if (id == holder->get_id())
                                {
                                    PrintAlbum(*holder);
                                    std::cout << " Seguro que este es el album deseado? (y/n) \n";
                                    if (YesOrNo())
                                    {
                                        std::cout << "Ingrese el titulo de la cancion" << "\n";
                                        std::string title = GetStringFromUser();
                                        Song *aux = holder->get_song(title);
                                        if (aux)
                                        {
                                            PrintSong(*aux);
                                            std::cout << "Seguro que quiere eliminar esta cancion? (y/n)" << "\n";
                                            if (YesOrNo())
                                            {
                                                holder->remove(title);
                                                std::cout << "Cancion eliminada\n";
                                                holder->update_duration();
                                            }
                                        }
                                        else
                                        {
                                            std::cout << " No hay ninguna cancion con ese nombre en este album\n";
                                        }
                                        results++;
                                        break;
                                    }
                                }
                            }
                            if (!results)
                            {
                                std::cout << " No hay ningun album con ese ID.\n";
                            }
                            Pause();
                            break;
                        }
                        case DEL_ALL_SONG: {
                            std::cout << " Ingresa el ID del album: ";
                            int id = GetIntFromUser();
                            int results = 0;
                            for (dll_iterator it = albums.begin(); it; it = it->next_)
                            {
                                Album *holder = albums.get(it);
                                if (id == holder->get_id())
                                {
                                    PrintAlbum(*holder);
                                    std::cout << " Esto eliminara todas las canciones del album.\n";
                                    std::cout << " Seguro que quieres continuar? (y/n) \n";
                                    if (YesOrNo())
                                    {
                                        holder->clear();
                                        holder->update_duration();
                                        std::cout << "Canciones eliminadas" << "\n";
                                        break;
                                    }
                                }
                            }
                            if (!results)
                            {
                                std::cout << " No hay ningun album con ese ID.\n";
                            }
                            Pause();
                            break;
                        }
                        case GO_BACK_SONG:
                            break;
                        default:
                            OptionNotFound();
                    }
                } while (opt != GO_BACK_SONG);
                break;
            case EXIT:
                break;
            default:
                OptionNotFound();
        }
    } while (opt != EXIT);
}

int CmpAlbumTitle(std::string title, Album &album)
{
    std::string album_title = album.get_title();
    // to lower
    std::transform(title.begin(), title.end(), title.begin(), ::tolower);
    std::transform(album_title.begin(), album_title.end(), album_title.begin(), ::tolower);
    if (title == album_title) return 0;
    return 1;
}

int CmpAlbumToAlbumTitle(Album &album, Album &other)
{
    std::string album_title, other_title;
    album_title = album.get_title();
    other_title = other.get_title();
    std::transform(album_title.begin(), album_title.end(), album_title.begin(), ::tolower);
    std::transform(other_title.begin(), other_title.end(), other_title.begin(), ::tolower);
    return album_title.compare(other_title);
}
