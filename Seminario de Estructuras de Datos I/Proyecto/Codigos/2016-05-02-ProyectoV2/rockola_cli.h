#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <algorithm>
#include "doubly_linked_list.h"
#include "album.h"
#include "song.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

enum Option
{
    // Main menu
    LIST_ALBUMS  = 1,
    ADD_ALBUM    = 2,
    SEARCH_ALBUM = 3,
    EDIT_ALBUM   = 4,
    DEL_ALBUM    = 5,
    DEL_ALL      = 6,
    SONGS_MENU   = 7,
    EXIT         = 0,
    // Sort albums menu
    NAME         = 1,
    LENGTH       = 2,
    GO_BACK_SORT = 3,
    // Open album menu
    OPEN         = 1,
    GO_BACK_OPEN = 2,
    // Songs menu
    SHOW_SONGS   = 1,
    ADD_SONG     = 2,
    EDIT_SONG    = 3,
    DEL_SONG     = 4,
    DEL_ALL_SONG = 5,
    GO_BACK_SONG = 6
};

void LoginScreen()
{
    system(CLEAR);
    for (int i = 0; i < 9; i++) std::cout << "\n";
    std::cout << "                                 R O C K O L A\n\n";
    std::cout << "                              Password: ";
}

// TODO receive cols and fill char in parameters
void PrintHeader(std::string title)
{
    system(CLEAR);
    int space = (80 - 2 - title.length()) / 2;
    std::cout << "+------------------------------------------------------------------------------+\n";
    std::cout << "|";
    for (int i = 0; i < space; i++) std::cout << " ";
    std::cout << title;
    for (int i = 0; i < space; i++) std::cout << " ";
    if (title.length() % 2) std::cout << " ";
    std::cout << "|\n";
    std::cout << "+------------------------------------------------------------------------------+\n";
    std::cout << std::endl;
}

void MainMenu()
{
    PrintHeader("R O C K O L A");
    std::cout << " 1.- Ver albums\n";
    std::cout << " 2.- Agregar album\n";
    std::cout << " 3.- Buscar album\n";
    std::cout << " 4.- Modificar album\n";
    std::cout << " 5.- Eliminar album\n";
    std::cout << " 6.- Eliminar todos\n";
    std::cout << " 7.- Administrar canciones\n";
    std::cout << " 0.- Salir\n";
    std::cout << std::endl;
}

void SortAlbumsMenu()
{
    PrintHeader("VER ALBUMS");
    std::cout << " 1.- Ordenar por nombre\n";
    std::cout << " 2.- Ordenar por duracion\n";
    std::cout << " 3.- Volver al menu principal\n";
    std::cout << std::endl;
}

void OpenAlbumMenu()
{
    std::cout << " 1.- Abrir un album\n";
    std::cout << " 2.- Volver al menu principal\n";
    std::cout << std::endl;
}

void SongsMenu()
{
    std::cout << " 1.- Mostrar canciones\n";
    std::cout << " 2.- Agregar cancion\n";
    std::cout << " 3.- Modificar cancion\n";
    std::cout << " 4.- Eliminar cancion\n";
    std::cout << " 5.- Eliminar todas\n";
    std::cout << " 6.- Volver al menu principal\n";
    std::cout << std::endl;
}

void PrintGenres()
{
    std::cout << "  1.- Pop\n";
    std::cout << "  2.- Rock\n";
    std::cout << "  3.- Jazz\n";
    std::cout << "  4.- Blues\n";
    std::cout << "  5.- Metal\n";
    std::cout << "  6.- Clasica\n";
    std::cout << "  7.- Alternativo\n";
    std::cout << "  8.- Electronica\n";
}

int SetGenre()
{
    std::string opt_string;
    int opt = -1;
    do
    {
        getline(std::cin, opt_string);
        try
        {
            opt = stoi(opt_string);
            if (opt < 1 || opt > 8)
            {
                std::cout << " Error. Ingresa un genero valido: ";
                opt = -1;
            }
        }
        catch (const std::invalid_argument& ia)
        {
            std::cout << " Error. Ingresa un genero valido: ";
        }
    } while(opt == -1);
    return opt;
}

std::string GetGenre(int genre)
{
    switch(genre)
    {
        case 1:
            return "Pop";
        case 2:
            return "Rock";
        case 3:
            return "Jazz";
        case 4:
            return "Blues";
        case 5:
            return "Metal";
        case 6:
            return "Clasica";
        case 7:
            return "Alternativa";
        case 8:
            return "Electronica";
    }
    return "";
}

int GetOption()
{
    std::string opt_string;
    int opt = -1;
    std::cout << " Ingresa una opcion: ";
    getline(std::cin, opt_string);
    do
    {
        try
        {
            opt = stoi(opt_string);
        }
        catch (const std::invalid_argument& ia)
        {
            std::cout << " Opcion no valida. Ingresa una opcion valida: ";
            getline(std::cin, opt_string);
        }
    } while(opt == -1);
    return opt;
}

bool YesOrNo()
{
    std::string opt;
    do
    {
        getline(std::cin, opt);
        std::transform(opt.begin(), opt.end(), opt.begin(), ::tolower);
        if (opt.length() > 1 || (opt.at(0) != 'y' && opt.at(0) != 'n'))
        {
            std::cout << " No es una opcion valida. Ingresa una opcion (y/n): ";
        }
    } while (opt.length() > 1 || (opt.at(0) != 'y' && opt.at(0) != 'n'));
    return opt.at(0) == 'y';
}

std::string GetStringFromUser()
{
    std::string x;
    getline(std::cin, x);
    return x;
}

int GetIntFromUser()
{
    int i;
    cin >> i;
    cin.ignore();
    return i;
}

void Pause()
{
    std::cout << "\n Presiona enter...";
    std::cin.get();
}

void OptionNotFound()
{
    std::cout << "\n 404 Opcion no encontrada. Elige otra opcion\n";
    Pause();
}

void PrintAlbum(Album &album)
{
    std::cout << "\n";
    std::cout << "         ID: " << album.get_id() << endl;
    std::cout << "     Nombre: " << album.get_title() << "\n";
    std::cout << " Interprete: " << album.get_artist() << "\n";
    std::cout << "     Genero: " << GetGenre(album.get_genre()) << "\n";
    std::cout << "   Duracion: " << album.get_duration() << "\n";
}

 void PrintAlbumHeader()
 {
    std::cout << setw(4) << left << "| ID" << setw(25) << left << "| Nombre" << setw(25) << left << "| Interprete" << setw(15) << left << "| Genero" << setw(9) << left << "| Duracion\n";
    std::cout << "+------------------------------------------------------------------------------+\n";
 }

 void PrintAlbumRow(Album &album)
 {
    std::cout << setw(4) << left << ("| " + to_string(album.get_id()));
    std::cout << setw(25) << left << ("| " + album.get_title());
    std::cout << setw(25) << left << ("| " + album.get_artist());
    std::cout << setw(15) << left << ("| " + GetGenre(album.get_genre()));
    std::cout << setw(9) << left << ("| " + album.get_duration()) << "\n";
    std::cout << "+------------------------------------------------------------------------------+\n";
 }

void SetAlbum(Album &album)
{
    std::cout << " Generos\n";
    PrintGenres();
    std::cout << "\n     Nombre: ";
    album.set_title(GetStringFromUser());
    std::cout << " Interprete: ";
    album.set_artist(GetStringFromUser());
    std::cout << "     Genero: ";
    album.set_genre(SetGenre());
}

void EditAlbum(Album &album)
{
    PrintAlbum(album);
    std::string holder;
    std::cout << "\n";
    std::cout << " Ingresa los nuevos datos (deja vacio para conservar el anterior):\n\n";
    PrintGenres();
    std::cout << "\n     Nombre: ";
    holder = GetStringFromUser();
    if (holder != "") album.set_title(holder);
    std::cout << " Interprete: ";
    holder = GetStringFromUser();
    if (holder != "") album.set_artist(holder);
    std::cout << "     Genero: ";
    album.set_genre(SetGenre());
    
}

void SetSong(Song &song)
{
    std::cout << "     Titulo: ";
    song.set_title(GetStringFromUser());
    do {
        std::cout << "   Duracion: ";
    } while(!song.set_duration(GetStringFromUser()));
}

void PrintSong(Song &song)
{
    std::cout << "\n";
    std::cout << "     Titulo: " << song.get_title() << "\n";
    std::cout << "   Duracion: " << song.get_duration() << "\n";
}

void PrintAlbums(DoublyLinkedList<Album> &albums)
{
    int header = 0;
    Album *album;
    dll_iterator it;
    for (it = albums.begin(); it; it = it->next_)
    {
        album = albums.get(it);
        if (album)
        {
            if (!header)
            {
                PrintAlbumHeader();
                header = 1;
            }
            PrintAlbumRow(*album);
        }
    }
}
