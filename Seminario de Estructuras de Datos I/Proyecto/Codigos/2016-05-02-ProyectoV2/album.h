#ifndef ALBUM_H
#define ALBUM_H

#include "singly_linked_list.h"
#include "song.h"

class Album
{
public:
	Album() : id_(0), title_(""), artist_(""), genre_(0), duration_("00:00") {}
	int get_id() { return id_; }
	void set_id(int id) { id_ = id; }
	std::string get_title() { return title_; }
	void set_title(std::string title) { title_ = title; }
    std::string get_artist() { return artist_; }
    void set_artist(std::string artist) { artist_ = artist; }
    int get_genre() { return genre_; }
    void set_genre(int genre) { genre_ = genre; }
    std::string get_duration() { return duration_; }
	void append(Song song) { songs_.insert_ordered(song); }
	void clear() { songs_.clear(); }
	void update_duration();
	void operator=(const Album& other);
	Song* get_song(const std::string title);
	void remove(const std::string title);
	void print_songs();
private:
	int id_;
	std::string title_;
    std::string artist_;
    int genre_;
    std::string duration_;
	SinglyLinkedList<Song> songs_;
};



std::string SecondsToString(int seconds)
{
    if (seconds < 0 || seconds > 60*60) seconds = 0;
    string s;
    s += seconds/60 < 10? "0" : "";
    s += std::to_string((seconds/60));
    s += ":";
    seconds %= 60;
    s += std::to_string(seconds);
    return s;
}

void Album::update_duration()
{
	int seconds = 0;
	for (sll_iterator it = songs_.begin(); it; it = it->next_)
	{
		seconds += songs_.get(it)->get_duration_sec();
	}
	duration_ = SecondsToString(seconds);
}

void Album::operator=(const Album& other)
{
	id_ = other.id_;
    title_ = other.title_;
    artist_ = other.artist_;
    genre_ = other.genre_;
    duration_ = duration_;
    songs_ = other.songs_;
}

Song* Album::get_song(const std::string title)
{
	if (songs_.empty()) return NULL;
	for(sll_iterator it = songs_.begin(); it; it = it->next_)
	{
		if(((Song*)it->data_)->get_title() == title)
		{
			return (Song*)it->data_;
		}
	}
	return NULL;
}

void Album::remove(const std::string title)
{
	if (songs_.empty()) return;
	for(sll_iterator it = songs_.begin(); it; it = it->next_)
	{
		if(((Song*)it->data_)->get_title() == title)
		{
			songs_.erase(it);
			break;
		}
	}
}

void Album::print_songs()
{
	if (songs_.empty()) return;
	for(sll_iterator it = songs_.begin(); it; it = it->next_)
	{
		std::cout << "\n";
	    std::cout << "     Titulo: " << ((Song*)it->data_)->get_title() << "\n";
	    std::cout << "   Duracion: " << ((Song*)it->data_)->get_duration() << "\n";
	}
}

#endif
