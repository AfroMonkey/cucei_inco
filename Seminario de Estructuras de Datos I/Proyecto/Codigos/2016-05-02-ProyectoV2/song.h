#ifndef SONG_H
#define SONG_H

#include <fstream>
#include <cctype>
#include <string>

class Song
{
public:
	Song() : title_(""), duration_("00:00") {}
	std::string get_title() { return title_; }
	void set_title(std::string title) { title_ = title; }
	std::string get_duration() { return duration_; }
    int get_duration_sec();
	bool set_duration(std::string duration);
    void read(std::ifstream file);
    void write(std::ofstream file);
	bool operator<(const Song& song) { return title_ < song.title_; }
private:
	std::string title_;
	std::string duration_;
};

int Song::get_duration_sec()
{
    std::string str_minutes = duration_.substr(0, 2);
    std::string str_seconds = duration_.substr(3, 2);
    int seconds;
    seconds = (stoi(str_minutes) * 60) + (stoi(str_seconds));
    return seconds;
}

bool Song::set_duration(std::string duration)
{
    if (duration.length() != 5) return false;
    if (duration.at(2) != ':') return false;
    if (!isdigit(duration.at(0)) || !isdigit(duration.at(1))) return false;
    if (!isdigit(duration.at(3)) || !isdigit(duration.at(4))) return false;
    if (duration.at(3) > '5') return false;
    duration_ = duration;
    return true;
}

void Song::write(std::ofstream file)
{
    file.write((char*) this, sizeof(Song));
}

void Song::read(std::ifstream file)
{
    file.read((char*) this, sizeof(Song));
}

#endif
