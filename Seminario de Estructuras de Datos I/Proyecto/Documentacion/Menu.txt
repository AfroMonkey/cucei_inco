'@' -> Lista
'I' -> Numero entero
'S' -> Cadena de caracteres
'C' -> Carácter

Rocola
    pass> 'S'
    *valida:
        1. Mostrar álbumes (alfabéticamente)
            @'I'# 'S'
            Numero de álbum> 'I'
            *valido:
                @'I'# 'S'
                Numero de canción> 'I'
                *valido:
                    Reproduciendo
                *invalido:
                    Canción invalida
            *invalido:
                Álbum invalido
        2. Mostrar álbumes (genero)
            1. Acid-jazz
            2. Dance
            3. Deep
            4. House
            5. Pop
            6. Braindance
            7. Experimental
            *valido:
                @'I'# 'S'
                Numero de álbum> 'I'
                *valido:
                    @'I'# 'S'
                    Numero de canción> 'I'
                    *valido:
                        Reproduciendo
                    *invalido:
                        Canción invalida
                *invalido:
                    Álbum invalido
            *invalido:
                Genero invalido
        3. Buscar álbum
            Nombre del álbum> 'S'
            *valido:
                @'I'# 'S'
                Numero de canción> 'I'
                *valido:
                    Reproduciendo
                *invalido:
                    Canción invalida
            *invalido:
                Álbum invalido
        4. Buscar canción
            Nombre de la canción> 'S'
            @'I'# 'S', 'S'
            Numero de canción> 'I'
            *valido:
                Reproduciendo
            *invalido:
                Canción invalida
        5. Administrar
            pass> 'S'
            *valida:
                1. Administrar álbumes
                    1. Agregar álbum
                        Nombre> 'S'
                        *valido:
                            Interprete> 'S'
                            Genero> 'S'
                        *invalido:
                            Nombre de álbum repetido
                    2. Modificar álbum
                        Nombre> 'S'
                        *valido:
                            Nombre: 'S'
                            Interprete: 'S'
                            Genero: 'S'
                            1. Modificar Nombre
                                Nombre> 'S'
                                *invalido:
                                    Álbum repetido
                            2. Modificar Interprete
                                Interprete> 'S'
                            3. Modificar Genero
                                Genero> 'S'
                        *invalido:
                            Álbum no encontrado
                    3. Eliminar álbum
                        Nombre> 'S'
                        *valido:
                            Álbum eliminado
                        *invalido
                            Álbum no encontrado
                2. Administrar canciones
                    1. Agregar canción
                        Álbum> 'S'
                        *valido:
                            Nombre> 'S'
                            Duración> 'S'
                        *invalido:
                            Álbum no encontrado
                    2. Modificar canción
                        Álbum> 'S'
                        *valido:
                            Nombre> 'S'
                            *valido:
                                Nombre> 'S'
                                Duración> 'S'
                                1. Modificar Nombre
                                    Nombre> 'S'
                                    *invalido:
                                        Canción repetida
                                2. Modificar Duración
                                    Duración> 'S'
                            *invalido:
                                Canción no encontrada
                        *invalido:
                            Álbum no encontrado
                    3. Eliminar canción
                        Álbum> 'S'
                        *valido:
                            Nombre> 'S'
                            *valido:
                                Advertencia, se eliminara la canción 'S' del álbum 'S', continuar (s/n)> 'C'
                                *'s':
                                    Canción eliminada
                                *'n':
                                    Operación abortada
                            *invalido:
                                Canción no encontrada
                        *invalido:
                            Álbum no encontrado
                3. Almacenamiento secundario
                    1. Exportar
                        Ruta> 'S'
                            *valida:
                                Datos exportados
                            *invalida:
                                Ruta invalida
                    2. Importar
                        Ruta> 'S'
                            *valida:
                                Datos importados
                            *invalida:
                                Ruta invalida
                3. Limpiar base de datos
                    Advertencia, esto eliminará todos los registros, continuar (s/n)>'C'
                        *'s':
                            Base de datos vacía
                        *'n':
                            Operación abortada
            *invalida:
                Invalida, intente de nuevo.
        *ciclo
    *invalida:
        Invalida, intente de nuevo.
        *ciclo
