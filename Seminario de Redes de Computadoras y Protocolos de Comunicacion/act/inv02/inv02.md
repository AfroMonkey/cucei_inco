# Investigación II
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-01-31

## Cabecera Ethernet
Establece el orden de los elementos de un paquete ethernet utilizando la siguiente estructura:
*   7B| Preámbulo: Sirve paras detectar las nuevas tramas.
*   1B| Delimitador de inicio de trama: Indica que comienza la trama.
*   6B| MAC Destino: Indica el equipo de destino.
*   6B| MAC Origen: Indica el equipo de origen.
*   4B| Etiqueta (Opcional): Campo opcional para VLAN.
*   2B| Protocolo: Indica el encapsulamiento para el Payload.
*   42-1500B| Payload: Datos del mensaje.
*   4B| Secuencia de comprobación: Campo para revisar que el payload sea correcto.
*   12B| Gap: Bytes vacios indicando que ha finalizado la trama.

## Encabezado IPv4
Contiene información acerca del paquete en cuestión, tiene la siguiente estructura:
![](/mnt/afrofiles/Pictures/ip4.png)

## Bibliografía
*   Hernándo, J.M. (1991). Sistemas de telecomunicación (2ª ed. edición). Madrid: Servicio de publicaciones E.T.S.I. Telecomunicación.
*   http://ecovi.uagro.mx/ccna1/course/module6/6.1.3.1/6.1.3.1.html
*   http://neo.lcc.uma.es/evirtual/cdd/tutorial/red/cabipv4.html
