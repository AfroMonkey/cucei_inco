#ifndef MAINWINDOWINTERFACE_HPP
#define MAINWINDOWINTERFACE_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

#include "EthernetController.hpp"

using namespace std;


class MainWIndowInterface
{
public:
    MainWIndowInterface();
    void show();
};

#endif // MAINWINDOWINTERFACE_HPP
