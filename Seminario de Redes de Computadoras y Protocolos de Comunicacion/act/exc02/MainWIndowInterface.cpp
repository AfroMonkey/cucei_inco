#include "MainWIndowInterface.hpp"
#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif
MainWIndowInterface::MainWIndowInterface()
{

}

void MainWIndowInterface::show()
{
    std::string option;
    bool flag = true;

    EthernetController ethernetController;

    do
    {
    	system(CLEAR);
        cout << "[Paquetes]" << '\n' << '\n';
        cout << "1.- Ethernet_1.bin" << '\n';
        cout << "2.- Ethernet_2.bin" << '\n';
        cout << "3.- Ethernet_3.bin" << '\n';
        cout << "4.- [Salir]" << '\n' << '\n';
        cout << "Elija un paquete: ";
        cin >> option;

        switch (atoi(option.c_str())) {
        case 1:
            system(CLEAR);
            ethernetController.analizePacket("ethernet_1.bin");
            cin.ignore();
            cout << '\n' << "Presione cualquier tecla para continuar: ";
            cin.get();
            break;
        case 2:
            system(CLEAR);
            ethernetController.analizePacket("ethernet_2.bin");
            cin.ignore();
            cout << '\n' << "Presione cualquier tecla para continuar: ";
            cin.get();
            break;
        case 3:
            system(CLEAR);
            ethernetController.analizePacket("ethernet_3.bin");
            cin.ignore();
            cout << '\n' << "Presione cualquier tecla para continuar: ";
            cin.get();
            break;
        case 4:
            flag = false;
            break;

        default:
            cout << "Valor incorrecto. Intente de nuevo." << '\n';
            break;
        }

    } while(flag);
}
