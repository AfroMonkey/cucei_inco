#include "Ethernet.hpp"

Ethernet::Ethernet()
{
    MACDestination = std::string();
    MACSource = std::string();
    type = std::string();
    ip = std::string();
    tcp = std::string();
    data = std::string();
}

void Ethernet::setMACDestination(std::string MACDestination)
{
    this->MACDestination = MACDestination;
}

void Ethernet::setMACSource(std::string MACSource)
{
    this->MACSource = MACSource;
}

void Ethernet::setType(std::string type)
{
    this->type = type;
}

void Ethernet::setIP(std::string ip)
{
    this->ip = ip;
}

void Ethernet::setTCP(std::string tcp)
{
    this->tcp = tcp;
}

void Ethernet::setData(std::string data)
{
    this->data = data;
}

std::string Ethernet::getMACDestination()
{
    return MACDestination;
}

std::string Ethernet::getMACSource()
{
    return MACSource;
}

std::string Ethernet::getType()
{
    return type;
}

std::string Ethernet::getIP()
{
    return ip;
}

std::string Ethernet::getTCP()
{
    return tcp;
}

std::string Ethernet::getData()
{
    return data;
}
