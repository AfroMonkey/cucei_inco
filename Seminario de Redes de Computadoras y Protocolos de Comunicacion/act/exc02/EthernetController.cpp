#include "EthernetController.hpp"
#include <cstdlib>

EthernetController::EthernetController()
{

}

void EthernetController::analizePacket(std::string filename)
{
    std::ifstream file;
    std::vector<unsigned char> byteArray;
    unsigned char aux;
    Ethernet ethernet;
    std::string hString;
    std::string auxString;
    int j = 0;

    file.open(filename.c_str(), std::ios::in | std::ios::binary);

    if(file.is_open())
    {
        while(!file.eof())
        {
            file.read(reinterpret_cast<char *>(&aux), sizeof(unsigned char));
            if(file.eof())break;

            byteArray.push_back(aux);
        }

        file.close();

        hString = hexString(byteArray);

        auxString = std::string();
        for(int i = 0; i < 12; i++, j++)
            auxString.push_back(hString[j]);
        ethernet.setMACDestination(auxString);

        auxString = std::string();
        for(int i = 0; i < 12; i++, j++)
            auxString.push_back(hString[j]);
        ethernet.setMACSource(auxString);

        auxString = std::string();
        for(int i = 0; i < 4; i++, j++)
            auxString.push_back(hString[j]);
        ethernet.setType(auxString);

        auxString = std::string();
        for(int i = 0; i < 40; i++, j++)
            auxString.push_back(hString[j]);
        ethernet.setIP(auxString);

        auxString = std::string();
        for(int i = 0; i < 46; i++, j++)
            auxString.push_back(hString[j]);
        ethernet.setTCP(auxString);

        auxString = std::string();
        for(unsigned int i = j; i < hString.size(); i++, j++)
            auxString.push_back(hString[j]);
        ethernet.setData(auxString);

        std::cout << "[Paquete Ethernet]" << '\n' << '\n';
        std::cout << "Direcci\xa2n destino: " << giveFormat(ethernet.getMACDestination()) << '\n';
        std::cout << "Direcci\xa2n origen: " << giveFormat(ethernet.getMACSource()) << '\n';
        std::cout << "Tipo: " << giveFormat(ethernet.getType()) << ' ' << getType(ethernet.getType()) << '\n';
        std::cout << "Direcci\xa2n IP: " << giveFormat(ethernet.getIP()) << '\n';
        std::cout << "TCP: " << giveFormat(ethernet.getTCP()) << '\n';
        std::cout << "Datos: " << giveFormat(ethernet.getData()) << '\n';
    }
    else
    {
        std::cout << "Error al abrir el archivo" << '\n';
        std::cin.get();
    }
}

std::string EthernetController::getType(std::string type)
{
    switch (atoi(type.c_str()))
    {
    case 803:
        return "ECMA Internet";
        break;
    case 800:
        return "Internet Protocol (IP)";
        break;
    case 806:
        return "Address Resolution Protocol (ARP)";
        break;
    case 8035:
        return "RARP";
        break;
    }

    return std::string();
}

std::string EthernetController::hexString(std::vector<unsigned char> data)
{
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (unsigned int i = 0; i < data.size(); i++)
    {
        ss << std::setw(2) << static_cast<int>(data[i]);
    }
    std::string s = ss.str();
    return s;
}


std::string EthernetController::giveFormat(std::string s)
{
    std::string ss;
    for(unsigned int i=0; i<s.length(); i++)
    {
        ss+=s[i];
        if(i%2==1)
        {
            ss+=' ';
        }
    }
    return ss;
}
