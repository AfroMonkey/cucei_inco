#ifndef ETHERNET_HPP
#define ETHERNET_HPP

#include <string>
#include <vector>

class Ethernet
{
public:
    Ethernet();

    void setMACDestination(std::string);
    void setMACSource(std::string);
    void setType(std::string);
    void setIP(std::string);
    void setTCP(std::string);
    void setData(std::string);

    std::string getMACDestination();
    std::string getMACSource();
    std::string getType();
    std::string getIP();
    std::string getTCP();
    std::string getData();

private:
    std::string MACDestination;
    std::string MACSource;
    std::string type;
    std::string ip;
    std::string tcp;
    std::string data;
};

#endif // ETHERNET_HPP
