#ifndef ETHERNETCONTROLLER_HPP
#define ETHERNETCONTROLLER_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <iomanip>

#include "Ethernet.hpp"

class EthernetController
{
public:
    EthernetController();

    void analizePacket(std::string);
    std::string hexString(std::vector<unsigned char>);
    std::string getType(std::string);
    std::string giveFormat(std::string s);
};

#endif // ETHERNETCONTROLLER_HPP
