# Investigación IV
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-02-14


## ARP (Address Resolution Protocol)
Se encarga de indicar al relación de una dirección MAC con una dirección IP dentro dentro de la red. Para lograr esto se enviar un paquete especial (ARP request) en modo broadcast (para todos en la red) y se pregunta por la dirección física (MAC), a ese paquete solo responde (con un paquete ARP reply) aquel dispositivo que tiene esa MAC, y de esta manera se registra que dirección IP le pertenece, esta relación queda almacenada de manera temporal en una tabla.

A continuación una tabla que ejemplifica el contenido de los paquetes ARP.  

Estructura de paquete ARP|Tamaño
:-:|:-
Tipo de hardware|2 bytes
Tipo de protocolo|2 bytes
Longitud MAC (x bytes)|1 byte
Longitud IP (y bytes)|1 byte
Código de operación|2 bytes
Dirección MAC emisor|x bytes
Dirección IP emisor|y bytes
Dirección MAC receptor|x bytes
Dirección IP receptor|y bytes


## RARP (Reverse ARP)
Hace la función contraria a ARP, en lugar de buscar que MAC le pertenece a que IP, busca que IP está relacionada con la MAC buscada.

Estructura de paquete RARP|Tamaño
:-:|:-
Tipo de hardware|2 bytes
Tipo de protocolo|2 bytes
Longitud MAC (x bytes)|1 byte
Longitud IP (y bytes)|1 byte
Código de operación|2 bytes
Dirección MAC emisor|x bytes
Dirección IP emisor|y bytes
Dirección MAC receptor|x bytes
Dirección IP receptor|y bytes

## ICMP (Internet Control Message Protocol)
Es un sub protocolo del IP, se utiliza para enviar mensajes de error y en contadas aplicaciones (ping, traceroute) se utiliza para otro fin.

Estructura de paquete ICMP|Tamaño
:-:|:-
Tipo|1 byte
Código|1 byte
Checksum|2 bytes
Datos (opcionales)|4 bytes

## Bibliografía
*   <http://es.ccm.net/contents/260-el-protocolo-arp>
*   <https://www.rfc-es.org/rfc/rfc0903-es.txt>
*   <https://www.nebrija.es/~cmalagon/seguridad_informatica/Lecturas/TCP-V_ICMP_hxc.pdf>
