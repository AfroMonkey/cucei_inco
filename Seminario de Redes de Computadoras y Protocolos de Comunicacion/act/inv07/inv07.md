# Investigación VII Paquete TCP
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-04-25


## Paquete TCP
El Protocolo para el Control de Transmisiones (Transmission Control Protocol) es uno de los distintos protocoles de Internet. Fue originado en las primeras implementaciones de Networks, se utiliza para complementar el protocolo de Internet (IP), es por esto que normalmente se le conoce como TCP/IP. TCP provee de manera confiable, ordenada y libre de errores un flujo de bytes entre las aplicaciones de los distintos host de la red IP. La mayoría de los servicios que se proveen a través de Internet utilizan TCP para llevar a cabo su cometido.

Debido a que TCP se asegura que los paquetes sean enviados correctamente y sin errores, es una metodología que, en velocidad, se ve superada por otras; pero esto no impide que sea la más utilizada debido a su fiabilidad. Ya que no solo garantiza que los datos serán entregados al destino, si no que además serán dispuestos en el mismo orden que fueron enviados y además proporciona un mecanismo para identificar a las distintas aplicaciones dentro del mimo host utilizando los llamados "puertos".

### Estructura
![](tcp)  

###### Source Port: 16 bits
El número de puerto de origen.

###### Destination Port: 16 bits

El número de puerto de destino.

###### Sequence Number: 32 bits

El número de secuencia del primer octeto de datos en este segmento (excepto cuando SYN está presente). Si SYN está presente el número de secuencia es el el número de secuencia inicial (ISN) y el primer octeto de datos es ISN + 1.

###### Acknowledgment Number: 32 bits

Si el bit de control ACK está configurado, este campo contiene el siguiente número de secuencia que el remitente del segmento espera recibir. Una vez que se establece una conexión, ésta se envía.

###### Data Offset: 4 bits

El número de palabras de 32 bits en el encabezado TCP. Esto indica dónde los datos comienzan. El encabezado TCP (incluso uno incluyendo las opciones) es un integral de 32 bits de longitud.

###### Reserved: 6 bits

Reservado para uso futuro. Debe ser cero.

###### Control Bits: 6 bits (izquierda a derecha)

*   URG: Campo de puntero urgente significativo
*   ACK: Campo de acuse de recibo significativo
*   PSH: Función Push
*   RST: Restablecer la conexión
*   SYN: Sincronizar números de secuencia
*   FIN: No hay más datos del remitente

###### Window: 16 bits

El número de octetos de datos que empiezan con el indicado en el reconocimiento que el remitente de este segmento está dispuesto a
aceptar.

###### Checksum: 16 bits

El campo checksum es el complemento de 16 bits de uno complementa la suma de todas las palabras de 16 bits en el encabezado y el texto. Si un segmento contiene un número impar de octetos de encabezado y texto para ser checksum, el último octeto se rellena a la derecha con ceros para forman una palabra de 16 bits para fines de suma de comprobación. La almohadilla no está transmitida como parte del segmento. Mientras se calcula la suma de comprobación, el campo checksum se reemplaza por ceros.

La suma de comprobación también cubre un pseudo encabezado de 96 bits conceptual-mente prefijado al encabezado TCP. Este pseudo encabezado contiene la Fuente dirección, la dirección de destino, el protocolo y la longitud TCP. esto proporciona la protección TCP contra segmentos mal distribuidos. Esta información se transporta en el Protocolo de Internet y se transfiere a través de la interfaz TCP / Red en los argumentos o resultados de llamadas por el TCP en el IP.

```C
+--------+--------+--------+--------+
|           Source Address          |
+--------+--------+--------+--------+
|         Destination Address       |
+--------+--------+--------+--------+
|  zero  |  PTCL  |    TCP Length   |
+--------+--------+--------+--------+
```

La Longitud TCP es la longitud del encabezado TCP más la longitud de datos en octetos (esto no es una cantidad transmitida explícitamente, pero es computado), y no cuenta los 12 octetos del pseudo encabezamiento.

###### Urgent Pointer: 16 bits
Este campo comunica el valor actual del puntero urgente como Offset positivo del número de secuencia en este segmento. Los punteros urgente apunta al número de secuencia del octeto siguiente Los datos urgentes. Este campo sólo se interpreta en segmentos con el bit de control URG ajustado.

###### Options: variable
Las opciones pueden ocupar espacio al final de la cabecera TCP y su longitud es múltiplo de 8 bits. Todas las opciones están incluidas en el checksum. Una opción puede comenzar en cualquier límite de octeto.

###### Padding: variable
Es utilizado para asegurar que la cabecera finalice y los datos comiencen en un múltiplo de 32 bits; esta compuesto de ceros.

### Bibliogafía
*   <http://freesoft.org/CIE/Course/Section4/8.htm>
*   <https://en.wikipedia.org/wiki/Transmission_Control_Protocol>
*   <https://es.wikipedia.org/wiki/Transmission_Control_Protocol>
