#ifndef IPv6HEADER_HPP
#define IPv6HEADER_HPP

#include <vector>
#include <string>
#include "Functions.hpp"

class IPv6Header
{
public:
	IPv6Header(std::vector<unsigned char>);

	std::string getVersion();
	std::string getTrafficClass();
	std::string getFlowLabel();
	std::string getPayLoadLength();
	std::string getNextHeader();
	std::string getHopLimit();
	std::string getSourceAddress();
	std::string getDestinationAddress();
	std::string getProtocol();

private:
	std::string version;
	std::string trafficClass;
	std::string flowLabel;
	std::string payLoadLength;
	std::string nextHeader;
	std::string hopLimit;
	std::string sourceAddress;
	std::string destinationAddress;
};

#endif