#ifndef ETHERNETHEADER_HPP
#define ETHERNETHEADER_HPP

#include <vector>

#include "Functions.hpp"

class EthernetHeader
{
public:
	EthernetHeader();
	EthernetHeader(std::vector<unsigned char>);
    void setCheckSum(std::string checkSum);
	std::string getDestinationMAC();
	std::string getSourceMAC();
	std::string getEtherType();
	std::string getCheckSum();

private:
	std::string destinationMAC;
	std::string sourceMAC;
	std::string etherType;
	std::string checkSum;
};

#endif
