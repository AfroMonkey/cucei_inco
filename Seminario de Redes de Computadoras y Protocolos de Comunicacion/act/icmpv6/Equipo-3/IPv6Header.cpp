#include "IPv6Header.hpp"
#include <stdlib.h>
IPv6Header::IPv6Header(std::vector<unsigned char> buffer)
{
	std::string binStr = byteArrayToBitsString(buffer);
	version = binarySubstring(binStr, 0, 3);
	trafficClass = binarySubstring(binStr, 4, 11);
	flowLabel = binarySubstring(binStr, 12, 31);
	payLoadLength = binarySubstring(binStr, 32, 47);
	nextHeader = binarySubstring(binStr, 48, 55);
	hopLimit = binarySubstring(binStr, 56, 63);
	sourceAddress = binarySubstring(binStr, 64, 191);
	destinationAddress = binarySubstring(binStr, 192, 319);
}

std::string IPv6Header::getVersion()
{
	return version;
}

std::string IPv6Header::getTrafficClass()
{
	return trafficClass;
}

std::string IPv6Header::getFlowLabel()
{
	return flowLabel;
}

std::string IPv6Header::getPayLoadLength()
{
	return payLoadLength;
}

std::string IPv6Header::getNextHeader()
{
	return nextHeader;
}

std::string IPv6Header::getHopLimit()
{
	return hopLimit;
}

std::string IPv6Header::getSourceAddress()
{
	return sourceAddress;
}

std::string IPv6Header::getDestinationAddress()
{
	return destinationAddress;
}

std::string IPv6Header::getProtocol()
{
	switch(binToDec(atoll(nextHeader.c_str())))
	{
		case 0:
			return "HOPOPT";
			break;

		case 2:
			return "IGMP";
			break;

		case 58:
			return "IPv6-ICMP";
			break;

		default:
			return "Desconocido";
	}
}
