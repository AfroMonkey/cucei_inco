#include "EthernetHeader.hpp"

EthernetHeader::EthernetHeader()
{
	destinationMAC = std::string();
	sourceMAC = std::string();
	etherType = std::string();
}

EthernetHeader::EthernetHeader(std::vector<unsigned char> buffer)
{
	std::string hexStr = hexString(buffer);
	destinationMAC = hexSubStr(hexStr, 0, 11);
	sourceMAC = hexSubStr(hexStr, 12, 23);
	etherType = hexSubStr(hexStr, 24, 27);
}

void EthernetHeader::setCheckSum(std::string checkSumX){
    checkSum=checkSumX;
}

std::string EthernetHeader::getDestinationMAC()
{
	return destinationMAC;
}

std::string EthernetHeader::getSourceMAC()
{
	return sourceMAC;
}

std::string EthernetHeader::getEtherType()
{
	return etherType;
}
