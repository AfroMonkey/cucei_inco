#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>
#include <vector>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <climits>
#include <string>

std::vector<unsigned char> readAllBytesFromFile(std::string);
std::vector<unsigned char> subVector(std::vector<unsigned char>, int, int);
std::string hexString(std::vector<unsigned char>);
std::string hexSubStr(std::string, int, int);
std::string to_bin(unsigned char);
std::string byteArrayToBitsString(std::vector<unsigned char>);
std::string binarySubstring(std::string, int, int);
std::string DecToHex(unsigned long long num);
unsigned long long binToDec(unsigned long long);
std::string strMACFormat(std::string);
std::string strIPFormat(std::vector<unsigned char>, int, int);
std::string strIPv6Format(std::string);

#endif