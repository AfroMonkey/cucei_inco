#include "MainController.hpp"
#include <stdlib.h>
MainController::MainController()
{

}

void MainController::printEthernetHeader(EthernetHeader ethernetHeader)
{
	std::cout << "MAC de destino: " << strMACFormat(ethernetHeader.getDestinationMAC()) << '\n';
	std::cout << "MAC de origen: " << strMACFormat(ethernetHeader.getSourceMAC()) << '\n';
	std::cout << "Tipo de protocolo: " << ethernetHeader.getEtherType() << " - IPv6"<< '\n';
}

void MainController::printIPv6Header(IPv6Header ipv6Header)
{
	std::cout << "Versi""\xa2""n: " << ipv6Header.getVersion() << " | " << binToDec(atoll(ipv6Header.getVersion().c_str())) << '\n';
	std::cout << "Clase de tráfico: " << ipv6Header.getTrafficClass() << '\n';
	std::cout << "Etiqueta de flujo: " << ipv6Header.getFlowLabel() << '\n';
	std::cout << "Longitud del campo de datos: " << ipv6Header.getPayLoadLength() << " | " << binToDec(atoll(ipv6Header.getPayLoadLength().c_str())) << '\n';
	std::cout << "Cabecera siguiente: " << ipv6Header.getNextHeader() << " | " << binToDec(atoll(ipv6Header.getNextHeader().c_str())) << " | " << ipv6Header.getProtocol() << '\n';
	std::cout << "L""\xa1""mite de saltos: " << ipv6Header.getHopLimit() << " | " << binToDec(atoll(ipv6Header.getHopLimit().c_str())) << '\n';
	std::cout << "Direcci""\xa2""n de origen: " << strIPv6Format(ipv6Header.getSourceAddress()) << '\n';
	std::cout << "Direcci""\xa2""n de destino: " << strIPv6Format(ipv6Header.getDestinationAddress()) << '\n';
}

void MainController::printICMPv6(ICMPv6 icmpv6)
{
	std::cout << "Tipo: " << icmpv6.getType() << " | " << binToDec(atoll(icmpv6.getType().c_str())) << " | " << icmpv6.getMeaningType() << '\n';
	std::cout << "C""\xa2""digo: " << icmpv6.getCode() << " | " << binToDec(atoll(icmpv6.getCode().c_str())) << " | " << icmpv6.getMeaningCode() << '\n';
	std::cout << "Checksum: " << icmpv6.getChecksum() << '\n';
	std::cout << "Message body: " << icmpv6.getMessageBody() << '\n';
}

void MainController::readPackage(std::string fileName)
{
	std::vector<unsigned char> buffer = readAllBytesFromFile(fileName);
	if(buffer.size() >= 62)
	{
		EthernetHeader ethernetHeader(subVector(buffer, 0, 13));
		IPv6Header ipv6Header(subVector(buffer, 14, 53));
		ICMPv6 icmpv6(subVector(buffer, 54, 61));
		std::cout << "### Cabecera Ethernet ###" << '\n' << '\n';
		printEthernetHeader(ethernetHeader);
		std::cout << '\n' << "### Cabecera IPv6 ###" << '\n' << '\n';
		printIPv6Header(ipv6Header);
		std::cout << '\n' << "### Cabecera ICMPv6 ###" << '\n' << '\n';
		printICMPv6(icmpv6);
		std::cout << '\n';
	}
	if(byteArrayToBitsString(buffer).size() >  = 496)
	{
		std::string binStr = binarySubstring(byteArrayToBitsString(buffer), 496, byteArrayToBitsString(buffer).size() - 1);
		std::string checkSumEthernet;
		checkSumEthernet=binStr.substr(binStr.length()-32,binStr.length());
		binStr=binStr.substr(0,binStr.length()-32);
		std::cout << "Resto del paquete: " << binStr << '\n';
		std::cout << "Check sum ethernet: " << checkSumEthernet << '\n';
	}
}
