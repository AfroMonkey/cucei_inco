#ifndef MAINCONTROLLER_HPP
#define MAINCONTROLLER_HPP

#include <iostream>
#include <vector>

#include "Functions.hpp"
#include "EthernetHeader.hpp"
#include "IPv6Header.hpp"
#include "ICMPv6.hpp"

class MainController
{
public:
	MainController();

	void printEthernetHeader(EthernetHeader);
	void printIPv6Header(IPv6Header);
	void printICMPv6(ICMPv6);
	void readPackage(std::string);
private:

};

#endif