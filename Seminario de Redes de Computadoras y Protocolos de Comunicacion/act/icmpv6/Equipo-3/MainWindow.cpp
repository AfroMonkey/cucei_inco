#include "MainWindow.hpp"
#include "MainController.hpp"
#include <stdlib.h>
#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

MainWindow::MainWindow()
{

}

void MainWindow::show()
{
	std::string option;
	bool flag = true;
	MainController mainController;

	do
	{
		system(CLEAR);
		cout << "[Men""\xa3"" principal]" << '\n' << '\n';
		cout << "1.- Ethernet IPv6 ND" << '\n';
		cout << "2.- Ethernet IPv6 ICMPv6 Destination Unreachable" << '\n';
		cout << "3.- Ethernet IPv6 ICMPv6 Hop Limit" << '\n';
		cout << "4.- Ethernet IPv6 ICMPv6 IGMP" << '\n';
		cout << "5.- Ethernet IPv6 ICMPv6 Ping" << '\n';
		cout << "6.- Ethernet IPv6 ICMPv6 Pong" << '\n';
		cout << "7.- Ethernet IPv6 ND ADV 1" << '\n';
		cout << "8.- Ethernet IPv6 ND ADV 2" << '\n';
		cout << "9.- Ethernet IPv6 ND Router ADV" << '\n';
		cout << "10.- Ethernet IPv6 ND Router SOL" << '\n';
		cout << "11.- Ethernet IPv6 ND SOL 1" << '\n';
		cout << "12.- Ethernet IPv6 ND SOL 2" << '\n';
		cout << "13.- Salir" << '\n';
		cout << "Elija una opci""\xa2""n: ";
		cin >> option;

		switch(atoi(option.c_str()))
		{
			case 1:
				system(CLEAR);
				mainController.readPackage("Packages/ethernet_ipv6_nd.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 2:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_icmpv6_destination_unreachable.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 3:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_icmpv6_hop_limit.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 4:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_icmpv6_igmp.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 5:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_icmpv6_ping.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 6:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_icmpv6_pong.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 7:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_nd_adv_1.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 8:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_nd_adv_2.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 9:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_nd_router_adv.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 10:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_nd_router_sol.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 11:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_nd_sol_1.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 12:
				system(CLEAR);
				mainController.readPackage("Packages/ipv6_nd_sol_2.bin");
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
				break;

			case 13:
				flag = false;
				break;

			default:
				cout << '\n' << "Error. Intente de nuevo." << '\n';
				cin.ignore();
				cout << '\n' << "Presione cualquier tecla para continuar: ";
				cin.get();
		}
	}while(flag);
}
