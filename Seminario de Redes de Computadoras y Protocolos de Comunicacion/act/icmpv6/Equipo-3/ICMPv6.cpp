#include "ICMPv6.hpp"
#include <stdlib.h>
ICMPv6::ICMPv6(std::vector<unsigned char> buffer)
{
	std::string binStr = byteArrayToBitsString(buffer);
	type = binarySubstring(binStr, 0, 7);
	code = binarySubstring(binStr, 8, 15);
	checksum = binarySubstring(binStr, 16, 31);
	messageBody = binarySubstring(binStr, 32, 63);
}

std::string ICMPv6::getType()
{
	return type;
}

std::string ICMPv6::getCode()
{
	return code;
}

std::string ICMPv6::getChecksum()
{
	return checksum;
}

std::string ICMPv6::getMessageBody()
{
	return messageBody;
}

std::string ICMPv6::getMeaningType()
{
	switch(binToDec(atoll(type.c_str())))
	{
		case 1:
			return "Destination unreachable";
			break;
		case 2:
			return "Packet too big";
			break;
		case 3:
			return "Time exceeded";
			break;
		case 4:
			return "Parameter Problem";
			break;
		case 128:
			return "Echo request";
			break;
		case 129:
			return "Echo reply";
			break;
		case 130:
			return "Multicast Listener Query (MLD)";
			break;
		case 131:
			return "Multicast Listener Report (MLD)";
			break;
		case 132:
			return "Multicast Listener Done (MLD)";
			break;
		case 133:
			return "Router Solicitation (NDP)";
			break;
		case 134:
			return "Router Advertisement (NDP)";
			break;
		case 135:
			return "Neighbor Solicitation (NDP)";
			break;
		case 136:
			return "Neighbor Adverstisement (NDP)";
			break;
		case 137:
			return "Redirect Message (NDP)";
			break;
		case 138:
			return "Router renumbering";
			break;
		case 139:
			return "ICMP Node Information Query";
			break;
		case 140:
			return "ICMP Node Information Response";
			break;
		case 141:
			return "Inverse Neighbor Discovery Solicitation Message";
			break;
		case 142:
			return "Inverse Neighbor Discovery Adverstisement Message";
			break;
		case 143:
			return "Multicast Listener Discovery (MLDv2) reports";
			break;
		case 144:
			return "Home Agent Address Discovery Request Message";
			break;
		case 145:
			return "Home Agent Address Discovery Reply Message";
			break;
		case 146:
			return "Mobile Prefix Solicitation";
			break;
		case 147:
			return "Mobile Prefix Adverstisement";
			break;
		case 148:
			return "Certification Path Solicitation (SEND)";
			break;
		case 149:
			return "Certification Path Adverstisement (SEND)";
			break;
		case 151:
			return "Multicast Router Adverstisement (MRD)";
			break;
		case 152:
			return "Multicast Router Solicitation (MRD)";
			break;
		case 153:
			return "Multicast Router Termination (MRD)";
			break;
		case 155:
			return "RPL Control Message";
			break;
		case 200:
			return "Private experimentation";
			break;
		case 201:
			return "Private experimentation";
			break;
		default:
			return "Unknown";
	}
}

std::string ICMPv6::getMeaningCode()
{
	switch(binToDec(atoll(type.c_str())))
	{
		case 1:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return "No route to destination";
					break;
				case 1:
					return "Communication with destination administratively prohibited";
					break;
				case 2:
					return "Beyond scope of source address";
					break;
				case 3:
					return "Address unreachable";
					break;
				case 4:
					return "Port unreachable";
					break;
				case 5:
					return "Source address failed ingress/egress policy";
					break;
				case 6:
					return "Reject route to destination";
					break;
				case 7:
					return "Error in source routing header";
					break;
				default:
					return "Unknown";
			}
			break;
		case 2:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 3:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return "Hop limit exceeded in transit";
					break;
				case 1:
					return "Fragment reassembly time exceeded";
					break;
				default:
					return "Unknown";
			}
			break;
		case 4:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return "Erroneous header filed encountered";
					break;
				case 1:
					return "Unrecognized Next Header type encountered";
					break;
				case 2:
					return "Unrecognized IPv6 option encountered";
					break;
                case 3:
                    return "IPv6 First Fragment has incomplete IPv6 Header Chain";
					break;
				default:
					return "Unknown";
			}
			break;
		case 128:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 129:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 130:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 131:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 132:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 133:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 134:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 135:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 136:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 137:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 138:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return "Router Renumbering Command";
					break;
				case 1:
					return "Router Renumbering Result";
					break;
				case 255:
					return "Sequence Number Reset";
					break;
				default:
					return "Unknown";
			}
			break;
		case 139:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return "The Data field contains an IPv6 address which is the Subject of this Query";
					break;
				case 1:
					return "The Data field contains a name which is the Subject of this Query, or is empty, as in the case of a NOOP";
					break;
				case 2:
					return "The Data field contains an IPv4 address which is the Subject of this Query";
					break;
				default:
					return "Unknown";
			}
			break;
		case 140:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return "A successful reply. The Reply Data field may or may not be empty";
					break;
				case 1:
					return "The Responder refuses to supply the answer. The Reply Data field will be empty";
					break;
				case 2:
					return "The Qtype of the Query is unknown to the Responder. The Reply Data field will be empty";
					break;
				default:
					return "Unknown";
			}
			break;
		case 141:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 142:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 143:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 144:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 145:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 146:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 147:
			switch(binToDec(atoll(code.c_str())))
			{
				case 0:
					return " ";
					break;
				default:
					return "Unknown";
			}
			break;
		case 148:
			return " ";
			break;
		case 149:
			return " ";
			break;
		case 151:
			return " ";
			break;
		case 152:
			return " ";
			break;
		case 153:
			return " ";
			break;
		case 155:
			return " ";
			break;
		case 200:
			return " ";
			break;
		case 201:
			return " ";
			break;
		default:
			return "Unknown";
	}
}
