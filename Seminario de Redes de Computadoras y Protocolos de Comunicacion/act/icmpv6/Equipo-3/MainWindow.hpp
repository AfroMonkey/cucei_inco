#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <iostream>
#include <cstdlib>

using namespace std;

class MainWindow
{
public:
	MainWindow();
	void show();
};

#endif