#ifndef ICMPV6_HPP
#define ICMPV6_HPP

#include <vector>
#include <string>
#include "Functions.hpp"

class ICMPv6
{
public:
	ICMPv6(std::vector<unsigned char>);

	std::string getType();
	std::string getCode();
	std::string getChecksum();
	std::string getMessageBody();

	std::string getMeaningType();
	std::string getMeaningCode();
	
private:
    std::string type;
    std::string code;
    std::string checksum;
    std::string messageBody;
};

#endif