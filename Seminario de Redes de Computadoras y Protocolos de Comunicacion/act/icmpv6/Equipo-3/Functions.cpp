#include "Functions.hpp"
#include <stdlib.h>
std::vector<unsigned char> readAllBytesFromFile(std::string filename)
{
    std::ifstream file;
    std::vector<unsigned char> byteArray;
    unsigned char aux;

    file.open(filename.c_str(), std::ios::in | std::ios::binary);

    if(!file.is_open())
    {
        std::cout << "This file couldn't opened" << std::endl;
        std::cin.get();
    }
    else
    {
        while(!file.eof())
        {
            file.read(reinterpret_cast<char*>(&aux), sizeof(unsigned char));
            if(file.eof())
                break;
            byteArray.push_back(aux);
        }

        file.close();
    }

    return byteArray;
}

std::vector<unsigned char> subVector(std::vector<unsigned char> v, int from, int to)
{
    std::vector<unsigned char> n;
    for(int i = from; i <= to; ++i)
        n.push_back(v[i]);
    return n;
}

std::string hexString(std::vector<unsigned char> data)
{
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (unsigned int i = 0; i < data.size(); i++)
    {
        ss << std::setw(2) << static_cast<int>(data[i]);
    }
    std::string s = ss.str();
    return s;
}

std::string hexSubStr(std::string inputString, int from, int to)
{
    std::string str;
    for(int i = from; i <= to; i++)
        str += inputString[i];
    return str;
}

std::string to_bin(unsigned char c)
{
	std::string s;
	for(int i = 0; i < 8; i++)
		s += c & (1 << 7 - i) ? '1' : '0';
	return s;
}

std::string byteArrayToBitsString(std::vector<unsigned char> byteArray)
{
	std::string bitsString;
	for(unsigned int i = 0; i < byteArray.size() ; i++)
		bitsString += to_bin(byteArray[i]);
	return bitsString;
}

std::string binarySubstring(std::string inputString, int from, int to)
{
    std::string str;
    for(int i = from; i <= to; i++)
        str += inputString[i];
    return str;
}

std::string DecToHex(unsigned long long num)
{
    std::stringstream ss;
    ss << std::hex << num;
    return ss.str();
}

unsigned long long binToDec(unsigned long long num)
{
    unsigned long long bin, dec = 0, rem, base = 1;

    while (num > 0)
    {
        rem = num % 10;
        dec = dec + rem * base;
        base = base * 2;
        num = num / 10;
    }

    return dec;
}

std::string strMACFormat(std::string inpStr)
{
    std::string mac;
    for(int i = 0; i < inpStr.size(); i++)
    {
        mac += inpStr[i];
        if((i + 1) % 2 == 0 && (i + 1) != inpStr.size())
            mac += ":";
    }
    return mac;
}

std::string strIPFormat(std::vector<unsigned char> data, int from, int to)
{
    std::string str;
    for(int i = from; i <= to; i++)
    {
        std::stringstream ss;
        ss << (static_cast<int>(data[i]));
        str += ss.str();
        if(i < to)
            str += ".";
    }
    return str;
}

std::string strIPv6Format(std::string binStr)
{
    std::stringstream ss;
    int j = 0, b = 4;

    ss << std::hex << std::setfill('0');

    while(j < binStr.size())
    {
        std::string auxStr;
        for(int i = 0; i < 8; ++i)
        {
            auxStr += (binStr[j]);
            j++;
        }
        unsigned long long ull = atoll(auxStr.c_str());
        ss << std::setw(2) << binToDec(ull);
    }

    std::string s = ss.str();

    while(b < s.size())
    {
        s.insert(b, ":");
        b = b + 5;
    }
    return s;
}
