# Investigación VI Paquete IPv6
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-03-07


## Paquete IPv6
El Protocolo de Internet versión 6, en inglés: Internet Protocol version 6 (IPv6), es una versión del Internet Protocol (IP), definida en el RFC 2460 y diseñada para reemplazar a Internet Protocol version 4 (IPv4) RFC 791, que a 2016 se está implementado en la gran mayoría de dispositivos que acceden a Internet.

IPv4 posibilita 4 294 967 296 (2³²) direcciones de host diferentes, mientras que IPv6  admite 340.282.366.920.938.463.463.374.607.431.768.211.456 (2¹²⁸)

La nueva versión del protocolo IP presenta los siguientes cambios principales:
* Encabezado mínimo de 40 Bytes
* Campo de direcciones de 128 bits
* Responsable de la determinación MTU
* Diferentes opciones para la asignación dinámica de direcciones
* No existe el concepto de Broadcast y aparece uno nuevo llamado “Anycast”
* Nueva funcionalidad para determinar la unidad máxima de transmisión de información
* IPSec como funcionalidad nativa de IPv6
* IETF
* Agrega 20 bytes más para QoS (“Flow level”), que se suman a los 8 bits originales de IPv4 que sólo cambian de nombre y ahora se  llaman “Traffic class”.
* Elimina:
  * La capacidad de fragmentación que tenía IPv4, por lo tanto elimina todos esto campos
  * Longitud de cabecera
  * Control de errores de cabecera
  * Opciones

Los primeros 40 bytes (320 bits) son la cabecera del paquete y contiene los siguientes campos:  
![](ipv6)  

### Bibliogafía
* <https://en.wikipedia.org/wiki/IPv6_packet>
* <https://www.google.com/intl/es/ipv6>
* <http://www.ipv6.es/es-ES/introduccion/Paginas/QueesIPv6.aspx>
