#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif
#define CARACTERES_POR_FILA 80
using namespace std;

void imprimirCentrado(string texto,string relleno);
void pausarTerminar();
void pausar();
void liberaScan();
void pausa(string texto);

long int cuentaObjetos;

class Objeto{
public:
    Objeto(){
        cuentaObjetos++;
    }
    virtual ~Objeto(){
        cuentaObjetos--;
    }
    virtual string dameCadena()=0;
    virtual bool esIgualA(Objeto* x)=0;
    virtual bool esMenorA(Objeto* x)=0;
    virtual bool esMayorA(Objeto* x)=0;
    virtual bool esMenorOIgualA(Objeto* x)=0;
    virtual bool esMayorOIgualA(Objeto* x)=0;
};

void liberaScan()
{
    time_t tiempoInicio=time(NULL),tiempoFin;
    char c=cin.get();
    tiempoFin=time(NULL);
    if (difftime(tiempoFin,tiempoInicio)<1)
    {
        if (c!='\n')
        {
            do
            {
                c=cin.get();
            }
            while (c!='\n' && c!=cin.eof());
        }
        cin.get();
    }
}

void pausa(string texto)
{
    cout << texto;
    liberaScan();
}

void pausar()
{
    pausa("Presione entrar para continuar . . .");
}

void pausarTerminar()
{
    pausa("Presione entrar para terminar . . .");
}

void imprimirCentrado(string texto,string relleno)
{
    int longitudTexto=texto.length();
    int espacios=(CARACTERES_POR_FILA-longitudTexto)/2;
    int i;
    for(i=0; i<espacios; i++)
    {
        cout << relleno;
    }
    cout << texto;
    i+=longitudTexto;
    for(; i<CARACTERES_POR_FILA; i++)
    {
        cout << relleno;
    }
}

bool esNumeroEntero(string cadena)
{
    if(cadena.length()<=0){
        return false;
    }
    for(unsigned int i = 0; i < cadena.length(); i++)
    {
        if (!isdigit(cadena[i])) return false;
    }
    return true;
}

void convertirCadenasAMayusculas(char cadena[])
{
    for (unsigned int i=0; i<strlen(cadena); i++)
    {
        cadena[i]=toupper(cadena[i]);
    }
}

void mensajeDeDespedida()
{
    system(CLEAR);
    cout<<" __^__                                      __^__"<<endl;
    cout<<"( ___ )------------------------------------( ___ )"<<endl;
    cout<<" | / |                                      | \\ |"<<endl;
    cout<<" | / |  GRACIAS POR UTILIZAR ESTE SISTEMA   | \\ |"<<endl;
    cout<<" | / |                                      | \\ |"<<endl;
    cout<<" | / |        QUE TENGA UN BUEN D""\xd6""A         | \\ |"<<endl;
    cout<<" |___|                                      |___|"<<endl;
    cout<<"(_____)------------------------------------(_____) "<<endl;
    cout << endl;
    cout<<"                     (0 0)            "<<endl;
    cout<<"            ----oOO-- (_) ----oOO---- "<<endl;
    cout<<"           \xc9\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd";
    cout<<"\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xbb"<<endl;
    cout<<"           \xba   Programa creado por:  \xba"<<endl;
    cout<<"           \xba     Castillo Serrano    \xba"<<endl;
    cout<<"           \xba     Cristian Michell    \xba"<<endl;
    cout <<"           \xc8\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd";
    cout <<"\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xbc\n";
    cout<<"            -------------------------"<<endl;
    cout<<"                    |__|__| "<<endl;
    cout<<"                      || || "<<endl;
    cout<<"                      || || "<<endl;
    cout<<"                      || || "<<endl;
    cout<<"                    ooO Ooo "<<endl;
}

char* pasarDeStringACharArray(char cadenaChar[], string cadenaString){
    int i, j;
    for(i=0,j=cadenaString.length(); i<j; i++){
        cadenaChar[i]=cadenaString[i];
    }
    cadenaChar[i]='\n';
    return cadenaChar;
}

void pasarDeCharArrayAString(string* cadenaString, char cadenaChar[]){
    int i, j;
    for(i=0,j=strlen(cadenaChar); i<j; i++){
        *cadenaString+=cadenaChar[i];
    }
}

#endif // UTIL_H_INCLUDED
