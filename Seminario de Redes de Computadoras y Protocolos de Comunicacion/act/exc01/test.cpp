#include <iostream>

#ifndef LEN
#define LEN 2
#endif

int main(int argc, char const *argv[]) {
    char c[2];
    c[0] = 80;
    c[1] = 35;
    unsigned short t = 0;
    for (int i = 0; i < LEN; ++i)
    {
        t += c[i]/10 << (LEN - 1)*16 - i*4;
        t += c[i]%10 << (LEN - 1)*16 - (i+1)*8;
    }
    std::cout << "type: " << t << '\n';
    return 0;
}
