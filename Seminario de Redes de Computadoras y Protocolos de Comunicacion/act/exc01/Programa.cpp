#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include "Util.h"

using namespace std;

#define TAMANO_DIRECCION_DESTINO 6
#define TAMANO_DIRECCION_ORIGEN 6
#define TAMANO_TIPO 2
#define TAMANO_IP 20
#define TAMANO_TCP 23
#define TAMANO_CODIGO_ASCII 256

void convertirDecimalAHexadecimal(int numeroDecimal);

int main()
{
    bool continuaPrograma=true;
    bool deseaOtroPaquete=true;
    do
    {
        system(CLEAR);
        imprimirCentrado("Practica 1 - Analizador de la estructura de un paquete de Ethernet", "*");
        cout << endl;
        ifstream  archivoDeEntrada;
        string cadena, nombreDeArchivo;
        cout << "Ingrese el nombre del archivo a evaluar con extenci\xa2n:" << endl;
        fflush(stdin);
        getline(cin, nombreDeArchivo);
        cout << endl;
        cout << endl;
        stringstream ss;
        unsigned char caracter;
        archivoDeEntrada.open(nombreDeArchivo.c_str(), ios::in|ios::binary);
        if(archivoDeEntrada.is_open())
        {
            while(!archivoDeEntrada.eof())
            {
                archivoDeEntrada.read((char*)&caracter, sizeof(char));
                if(archivoDeEntrada.eof())break;
                cadena+=(unsigned char)caracter;
            }
            archivoDeEntrada.close();
            imprimirCentrado("*", "*");
            cout << "Contenido de la cadena original:" << endl;
            cout << cadena << endl;
            cout << endl;
            unsigned int i, j=0;
            cout << endl;
            cout << "Separaci\xa2n del paquete y su tranformaci""\xa2""n a su correspondiente hexadecimal." << endl;
            cout << endl;
            cout << "Direcci\xa2n de destino:" << endl;
            for (i=0; i< TAMANO_DIRECCION_DESTINO; i++, j++)
            {
                convertirDecimalAHexadecimal((int)cadena[j]);
            }
            cout << endl << endl;
            cout << "Direcci\xa2n de origen:" << endl;
            for (i=0; i< TAMANO_DIRECCION_ORIGEN; i++, j++)
            {
                convertirDecimalAHexadecimal((int)cadena[j]);
            }
            cout << endl << endl;
            cout << "Tipo:" << endl;
            short type = 0;
            for (i=0; i< TAMANO_TIPO; i++, j++)
            {
                // type += cadena[j]/10 << (TAMANO_TIPO*2 - 1) - i*2;
                // type += cadena[j]%10 << (TAMANO_TIPO*2 - 2) - i*2;
                // std::cout << "cadena:" << (int)cadena[j] << '\n';
                convertirDecimalAHexadecimal((int)cadena[j]);
            }
            std::cout << "type:" << type << '\n';
            cout << endl << endl;;
            cout << "IP:" << endl;
            for (i=0; i< TAMANO_IP; i++, j++)
            {
                convertirDecimalAHexadecimal((int)cadena[j]);
            }
            cout << endl << endl;
            cout << "TCP:" << endl;
            for (i=0; i< TAMANO_TCP; i++, j++)
            {
                convertirDecimalAHexadecimal((int)cadena[j]);
            }
            cout << endl << endl;
            cout << "Datos:" << endl;
            for (; j< cadena.length(); j++)
            {
                convertirDecimalAHexadecimal((int)cadena[j]);
            }
        }
        else
        {
            cout << "El archivo no se pudo abrir correctamente, intentelo de nuevo." << endl;
        }
        cout << endl << endl;
        pausar();
        do
        {
            string opcionCadena;
            int opcionNumero;
            system(CLEAR);
            imprimirCentrado("OTRO INTENTO", "*");
            cout << "Desea evaluar otro paquete." << endl;
            cout << "1. Si." << endl;
            cout << "2. No." << endl;
            cout << "Opci\xa2n: ";
            getline(cin, opcionCadena);
            opcionNumero=esNumeroEntero(opcionCadena)?atoi(opcionCadena.c_str()):-100;
            switch(opcionNumero)
            {
            case 1:
                deseaOtroPaquete=false;
                break;
            case 2:
                continuaPrograma=false;
                deseaOtroPaquete=false;
                break;
            default:
                cout << "Opcion invalida ingrese una opcion valida." << endl;
                deseaOtroPaquete=true;
                pausar();
                break;
            }
        }
        while(deseaOtroPaquete);
    }
    while(continuaPrograma);
    pausarTerminar();
    return 0;
}


void convertirDecimalAHexadecimal(int numeroDecimal)
{
    stringstream ss;
    string cadenaAuxiliar;
    if(numeroDecimal<0){
        numeroDecimal=numeroDecimal+TAMANO_CODIGO_ASCII;
    }
    ss << std::hex << numeroDecimal;
    cadenaAuxiliar=ss.str();
    if(cadenaAuxiliar.length()==1)
    {
        cadenaAuxiliar.insert(cadenaAuxiliar.begin(),'0');
        cout << cadenaAuxiliar << "|";
    }
    else
    {
        cout << ss.str() << "|";
    }

}
