# Investigación III
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-02-07

## Paquetes, tramas y datagramas en modelo OSI
La forma en como se manejan los datos a través del modelo OSI varían dependiendo la capa en la que nos encontremos, a continuación se muestra una imágen donde se representan estas distintas formas.  
![](osi2.PNG)  
En la capa física se manejan las señales eléctricas (los bits), en las capa de red la información es llamada trama, en la de red se le conoce como paquete y en ocasiones en la capa de transporte se le llama segmento.

En la siguiente imagen se muestran además los distintos protocolos que suelen utilizarse en cada capa.  
![](osi.JPG)

## Socket
Punto por el cual se puede enviar y recibir datos desde un solo nodo en una red de computadoras; una de las ventajas que tiene la utilización de sockets es que una vez establecida la conexión entre ambos equipos, no se requiere de la autenticación en cada una de las transmisiones, lo que agiliza el proceso de los envíos.


## Bibliografía
*   <http://belarmino.galeon.com/>
*   Riera García Juan B. Teleinformática y redes de computadores.
*   <http://www.ibm.com/support/knowledgecenter/ssw_ibm_i_71/rzab6/howdosockets.htm>
*   <https://www.cs.uic.edu/~troy/fall04/cs441/drake/sockets.html>
