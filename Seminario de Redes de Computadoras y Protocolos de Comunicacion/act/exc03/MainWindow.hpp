#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <iostream>
#include <cstdlib>
#include <vector> // Borrar

#include "IPv4Packet.hpp"
#include "IPv4Controller.hpp"

using namespace std;

class MainWindow
{
public:
	MainWindow();
	void show();
};

#endif