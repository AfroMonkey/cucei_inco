#include "IPv4Controller.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <sstream>

std::string to_string2(unsigned long long int integer)
{
  std::string ss = "";
  ss = static_cast<std::ostringstream*>(&(std::ostringstream() << integer))->str();
  return ss;
}

IPv4Controller::IPv4Controller()
{

}

std::vector<unsigned char> IPv4Controller::readAllBytesFromFile(std::string filename)
{
	std::ifstream file;
    std::vector<unsigned char> byteArray;
    unsigned char aux;

    file.open(filename.c_str(), std::ios::in | std::ios::binary);

    if(!file.is_open()){
    	std::cout << "This file couldn't opened" << std::endl;
    	std::cin.get();
    }else{

    while(!file.eof())
    {
        file.read(reinterpret_cast<char*>(&aux), sizeof(unsigned char));
        if(file.eof())
            break;
        byteArray.push_back(aux);
    }

    file.close();
    }

    return byteArray;
}

std::string IPv4Controller::to_bin(unsigned char c)
{
	std::string s;
	for(int i = 0; i < 8; i++)
		s += c & (1 << 7 - i) ? '1' : '0';
	return s;
}

std::string IPv4Controller::byteArrayToBitsString(std::vector<unsigned char> byteArray)
{
	std::string bitsString;
	for(unsigned int i = 0; i < byteArray.size() ; i++)
		bitsString += to_bin(byteArray[i]);
	return bitsString;
}

std::string IPv4Controller::binarySubstring(std::string inputString, int from, int to)
{
	std::string str;
	for(int i = from; i <= to; i++)
		str += inputString[i];
	return str;
}

IPv4Packet IPv4Controller::initializeIpv4Packet(std::string bitsString)
{
	IPv4Packet ipv4Packet;
	std::cout << "Total de bytes leidos del archivo: " << bitsString.size() << std::endl;
	std::cout << "-------------------------------------------------------------------------------"<< std::endl;
	std::cout << std::endl;
	ipv4Packet.setVersion(binarySubstring(bitsString, 0, 3));
	ipv4Packet.setIHL(binarySubstring(bitsString, 4, 7));
	ipv4Packet.setServiceType(binarySubstring(bitsString, 8, 15));
	ipv4Packet.setTotalLength(binarySubstring(bitsString, 16, 31));
	ipv4Packet.setIdentification(binarySubstring(bitsString, 32, 47));
	ipv4Packet.setFlags(binarySubstring(bitsString, 48, 50));
	ipv4Packet.setFragmentOffset(binarySubstring(bitsString, 51, 63));
	ipv4Packet.setTTL(binarySubstring(bitsString, 64, 71));
	ipv4Packet.setProtocol(binarySubstring(bitsString, 72, 79));
	ipv4Packet.setHeaderChecksum(binarySubstring(bitsString, 80, 95));
	ipv4Packet.setSourceAddress(binarySubstring(bitsString, 96, 127));
	ipv4Packet.setDestinationAddress(binarySubstring(bitsString, 128, 159));
	ipv4Packet.setOptions(binarySubstring(bitsString, 160, 191));
	ipv4Packet.setData(binarySubstring(bitsString, 192, bitsString.size()- 1));
	return ipv4Packet;
}

std::string IPv4Controller::returnVersion(std::string version)
{
	switch(atoi(version.c_str()))
	{
		case 100:
			return "IPv4";
			break;
		case 110:
			return "IPV6";
			break;
		default:
			return "Desconocida";
	}
}

std::string IPv4Controller::returnTypeOfService(std::string typeOfService)
{
	std::string str;
	std::string fbits;
    str+='\n';
    str+=" Nivel de urgencia: ";
	for(int i = 0; i < 3; i++)
		fbits.push_back(typeOfService[i]);
	if(fbits == "000")
		str += "De rutina";
	else if(fbits == "001")
		str += "Prioritario";
	else if(fbits == "010")
		str += "Inmediato";
	else if(fbits == "011")
		str += "Relámpago";
	else if(fbits == "100")
		str += "Invalidaci""\xa2""n rel\a0mpago";
	else if(fbits == "101")
		str += "Procesando llamada cr\xa1tica y de emergencia";
	else if(fbits == "110")
		str += "Control de trabajo de Internet";
	else if(fbits == "111")
		str += "Control de red";
	else
		str += "Desconocido";
    str+='\n';
    str+=" Retardo: ";
	if(typeOfService[3] == '0')
		str += "normal";
	else
		str += "bajo";
    str+='\n';
    str+=" Rendimiento: ";
	if(typeOfService[4] == '0')
		str += "normal";
	else
		str += "alto";
    str+='\n';
    str+=" Fiabilidad: ";
	if(typeOfService[5] == '0')
		str += "normal";
	else
		str += "alta";
	return str;
}

std::string IPv4Controller::returnFlag(std::string flags)
{
	std::string str;
	if(flags[0] == '0'){
    str+="0 (Reservado)| ";
	}

	if(flags[1] == '0')
		str += "Divisible";
	else
		str += "No divisible";

	if(flags[2] == '0')
		str += " | \xe9ltimo fragmento";
	else
		str += " | Fragmento intermedio";

	return str;
}

unsigned long long IPv4Controller::binToDec(unsigned long long num)
{
	unsigned long long bin, dec = 0, rem, base = 1;

	while (num > 0)
	{
		rem = num % 10;
		dec = dec + rem * base;
		base = base * 2;
		num = num / 10;
	}

	return dec;
}

std::string IPv4Controller::processIPv4(std::string ip)
{
	std::string ipv4;
	std::string aux;

	for(int i = 0; i < 8; i++)
		aux.push_back(ip[i]);
	ipv4.append(to_string2(binToDec(atoll(aux.c_str()))));

	aux = std::string();
	for(int i = 8; i < 16; i++)
		aux.push_back(ip[i]);
	ipv4 += '.';
	ipv4.append(to_string2(binToDec(atoll(aux.c_str()))));

	aux = std::string();
	for(int i = 16; i < 24; i++)
		aux.push_back(ip[i]);
	ipv4 += '.';
	ipv4.append(to_string2(binToDec(atoll(aux.c_str()))));

	aux = std::string();
	for(int i = 24; i < 32; i++)
		aux.push_back(ip[i]);
	ipv4 += '.';
	ipv4.append(to_string2(binToDec(atoll(aux.c_str()))));

	return ipv4;
}

std::string IPv4Controller::sizeOfIHL(std::string ihl)
{
	std::string bits = to_string2(binToDec(atoll(ihl.c_str())) * 32);
	std::string bytes = to_string2((binToDec(atoll(ihl.c_str())) * 32) / 8);
	return bits + " bits | " + bytes + " bytes | ";
}

void IPv4Controller::printIPv4Packet(IPv4Packet ipv4Packet)
{
    std::cout << "-Versi""\xa2""n: " << ipv4Packet.getVersion() << " (Valor leido)" << '\n';
	std::cout << "Versi""\xa2""n: " << returnVersion(ipv4Packet.getVersion()) << '\n';
	std::cout << "-Longitud de cabecera: " << ipv4Packet.getIHL() << " (Valor leido)" << '\n';
	std::cout << "Longitud de cabecera: " << sizeOfIHL(ipv4Packet.getIHL()) << '\n';
	std::cout << "-Tipo de servicio: " << ipv4Packet.getServiceType() << " (Valor leido)" << '\n';
	std::cout << "Tipo de servicio: " << returnTypeOfService(ipv4Packet.getServiceType()) << '\n';
	std::cout << "-Longitud total: " << ipv4Packet.getTotalLength() << " (Valor leido)" << '\n';
	std::cout << "Longitud total: " << binToDec(atoll(ipv4Packet.getTotalLength().c_str())) << '\n';
	std::cout << "Identificador: " << ipv4Packet.getIdentification() << '\n';
	std::cout << "-Banderas: " << ipv4Packet.getFlags() << " (Valor leido)" << '\n';
	std::cout << "Banderas: " << returnFlag(ipv4Packet.getFlags()) << '\n';
	std::cout << "Posici""\xa2""n de fragmento: " << ipv4Packet.getFragmentOffget() << '\n';
	std::cout << "-Tiempo de vida: " << ipv4Packet.getTTL().c_str() << " (Valor leido)" << '\n';
	std::cout << "Tiempo de vida: " << binToDec(atoll(ipv4Packet.getTTL().c_str())) << " enrutadores maximo" << '\n';
	std::cout << "-Protocolo: " << ipv4Packet.getProtocol() << " (Valor leido)" << '\n';
	std::cout << "Protocolo: " << binToDec(atoll(ipv4Packet.getProtocol().c_str())) << '\n';
	std::cout << "-Suma de comprobaci""\xa2""n: " << ipv4Packet.getHeaderChecksum() << " (Valor leido)" << '\n';
	std::cout << "Suma de comprobaci""\xa2""n: " << binToDec(atoll(ipv4Packet.getHeaderChecksum().c_str())) << '\n';
	std::cout << "-Direcci""\xa2""n de origen: " << ipv4Packet.getSourceAddress() << " (Valor leido)" << '\n';
	std::cout << "Direcci""\xa2""n de origen: " << processIPv4(ipv4Packet.getSourceAddress()) << '\n';
	std::cout << "-Direcci""\xa2""n de destino: " << ipv4Packet.getDestinationAddress() << " (Valor leido)" << '\n';
	std::cout << "Direcci""\xa2""n de destino: " << processIPv4(ipv4Packet.getDestinationAddress()) << '\n';
	std::cout << "Opciones: " << ipv4Packet.getOptions() << '\n';
	std::cout << "Datos: " << ipv4Packet.getData() << '\n';
}

std::string IPv4Controller::hexString(std::vector<unsigned char> data)
{
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (unsigned int i = 0; i < data.size(); i++)
    {
        ss << std::setw(2) << static_cast<int>(data[i]);
    }
    std::string s = ss.str();
    return s;
}

void IPv4Controller::processPacket(std::string filename)
{
	std::vector<unsigned char> byteArray = readAllBytesFromFile(filename);
	if(byteArray.size()>0){
	std::string bitsString = byteArrayToBitsString(byteArray);
	IPv4Packet ipv4Packet = initializeIpv4Packet(bitsString);
	printIPv4Packet(ipv4Packet);
	}
}

/*
	std::vector<unsigned char> byteArray = readAllBytesFromFile(filename);
	std::string bitsString = byteArrayToBitsString(byteArray);
	IPv4Packet ipv4Packet = initializeIpv4Packet(bitsString);
	printIPv4Packet(ipv4Packet);
	*/
