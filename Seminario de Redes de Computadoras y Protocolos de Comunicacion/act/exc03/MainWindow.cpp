#include "MainWindow.hpp"
#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif
MainWindow::MainWindow()
{

}

void MainWindow::show()
{
	std::string option;
	bool flag = true;
	IPv4Controller ipv4Controller;

	do
	{
    	system(CLEAR);
		cout << "[Men\xa3 principal]" << '\n' << '\n';
		cout << "1.- Ethernet IPv4 ICMP Host Unreachable" << '\n';
		cout << "2.- Ethernet IPv4 ICMP Network Unreachable" << '\n';
		cout << "3.- Ethernet IPv4 ICMP Ping" << '\n';
		cout << "4.- Ethernet IPv4 ICMP Ping 2" << '\n';
		cout << "5.- Ethernet IPv4 ICMP Pong" << '\n';
		cout << "6.- Ethernet IPv4 ICMP Pong 2" << '\n';
		cout << "7.- Ethernet IPv4 ICMP Redirect" << '\n';
		cout << "8.- Ethernet IPv4 ICMP TTL" << '\n';
		cout << "9.- Ethernet IPv4 ICMP" << '\n';
		cout << "10.- Ethernet IPv4 TCP IRC" << '\n';
		cout << "11.- Ethernet IPv4 TCP SYN (ESTE ARCHIVO SI ES VERSION IPV4)" << '\n';
		cout << "12.- Ethernet IPv4 TCP (ESTE ARCHIVO SI ES VERSION IPV4)" << '\n';
		cout << "13.- Ethernet IPv4 UDP DNS" << '\n';
		cout << "14.- Salir" << '\n' << '\n';
		cout << "Elija una opci""\xa2""n: ";
		cin >> option;

		switch(atoi(option.c_str()))
		{
			case 1:
            	system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_host_unreachable.bin");
	            cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;
			case 2:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_network_unreachable.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 3:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_ping.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 4:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_ping_2.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 5:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_pong.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 6:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_pong_2.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 7:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_redirect.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 8:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp_ttl.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 9:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_icmp.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 10:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_tcp_irc.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 11:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_tcp_syn.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 12:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_tcp.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 13:
				system(CLEAR);
				ipv4Controller.processPacket("ethernet_ipv4_udp_dns.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 14:
				flag = false;
				break;

			default:
				cout << "Error. Intente de nuevo." << '\n';
				std::cin.get();
				std::cin.get();
		}

	} while(flag);

}
