#ifndef IPV4_CONTROLLER_HPP
#define IPV4_CONTROLLER_HPP

#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
#include <string>
#include <bitset>
#include <sstream>
#include <iomanip>

#include "IPv4Packet.hpp"

class IPv4Controller
{
public:
	IPv4Controller();

	std::vector<unsigned char> readAllBytesFromFile(std::string);
	std::string to_bin(unsigned char);
	std::string byteArrayToBitsString(std::vector<unsigned char>);
	std::string binarySubstring(std::string, int, int);
	IPv4Packet initializeIpv4Packet(std::string);
	std::string returnVersion(std::string);
	std::string returnTypeOfService(std::string);
	std::string returnFlag(std::string);
	unsigned long long binToDec(unsigned long long);
	std::string processIPv4(std::string);
	std::string sizeOfIHL(std::string);
	void printIPv4Packet(IPv4Packet);
	std::string hexString(std::vector<unsigned char>);
	void processPacket(std::string);
};

#endif