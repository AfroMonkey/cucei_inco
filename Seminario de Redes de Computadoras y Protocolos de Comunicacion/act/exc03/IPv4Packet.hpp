#ifndef IPV4_PACKET_HPP
#define IPV4_PACKET_HPP

#include <string>

class IPv4Packet
{
public:
    IPv4Packet();
    IPv4Packet(const IPv4Packet&);

    void setVersion(std::string);
    void setIHL(std::string);
    void setServiceType(std::string);
    void setTotalLength(std::string);
    void setIdentification(std::string);
    void setFlags(std::string);
    void setFragmentOffset(std::string);
    void setTTL(std::string);
    void setProtocol(std::string);
    void setHeaderChecksum(std::string);
    void setSourceAddress(std::string);
    void setDestinationAddress(std::string);
    void setOptions(std::string);
    void setData(std::string);

    std::string getVersion();
    std::string getIHL();
    std::string getServiceType();
    std::string getTotalLength();
    std::string getIdentification();
    std::string getFlags();
    std::string getFragmentOffget();
    std::string getTTL();
    std::string getProtocol();
    std::string getHeaderChecksum();
    std::string getSourceAddress();
    std::string getDestinationAddress();
    std::string getOptions();
    std::string getData();

private:
    std::string version;
    std::string ihl;
    std::string serviceType;
    std::string totalLength;
    std::string identification;
    std::string flags;
    std::string fragmentOffset;
    std::string ttl;
    std::string protocol;
    std::string headerChecksum;
    std::string sourceAddress;
    std::string destinationAddress;
    std::string options;
    std::string data;
};

#endif
