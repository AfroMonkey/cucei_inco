#include "IPv4Packet.hpp"

IPv4Packet::IPv4Packet()
{
    version = std::string();
    ihl = std::string();
    serviceType = std::string();
    totalLength = std::string();
    identification = std::string();
    flags = std::string();
    fragmentOffset = std::string();
    ttl = std::string();
    protocol = std::string();
    headerChecksum = std::string();
    sourceAddress = std::string();
    destinationAddress = std::string();
    options = std::string();
    data = std::string();
}

IPv4Packet::IPv4Packet(const IPv4Packet &other)
{
    version = other.version;
    ihl = other.ihl;
    serviceType = other.serviceType;
    totalLength = other.totalLength;
    identification = other.identification;
    flags = other.flags;
    fragmentOffset = other.fragmentOffset;
    ttl = other.ttl;
    protocol = other.protocol;
    headerChecksum = other.headerChecksum;
    sourceAddress = other.sourceAddress;
    destinationAddress = other.destinationAddress;
    options = other.options;
    data = other.data;
}


void IPv4Packet::setVersion(std::string version)
{
	this->version = version;
}

void IPv4Packet::setIHL(std::string ihl)
{
	this->ihl = ihl;
}

void IPv4Packet::setServiceType(std::string serviceType)
{
	this-> serviceType = serviceType;
}

void IPv4Packet::setTotalLength(std::string totalLength)
{
	this->totalLength = totalLength;
}

void IPv4Packet::setIdentification(std::string identification)
{
	this->identification = identification;
}

void IPv4Packet::setFlags(std::string flags)
{
	this->flags = flags;
}

void IPv4Packet::setFragmentOffset(std::string fragmentOffset)
{
	this->fragmentOffset = fragmentOffset;
}

void IPv4Packet::setTTL(std::string ttl)
{
	this->ttl = ttl;
}

void IPv4Packet::setProtocol(std::string protocol)
{
	this->protocol = protocol;
}

void IPv4Packet::setHeaderChecksum(std::string headerChecksum)
{
	this->headerChecksum = headerChecksum;
}

void IPv4Packet::setSourceAddress(std::string sourceAddress)
{
	this->sourceAddress = sourceAddress;
}

void IPv4Packet::setDestinationAddress(std::string destinationAddress)
{
	this->destinationAddress = destinationAddress;
}

void IPv4Packet::setOptions(std::string options)
{
	this->options = options;
}

void IPv4Packet::setData(std::string data)
{
	this->data = data;
}


std::string IPv4Packet::getVersion()
{
	return version;
}

std::string IPv4Packet::getIHL()
{
	return ihl;
}

std::string IPv4Packet::getServiceType()
{
	return serviceType;
}

std::string IPv4Packet::getTotalLength()
{
	return totalLength;
}

std::string IPv4Packet::getIdentification()
{
	return identification;
}

std::string IPv4Packet::getFlags()
{
	return flags;
}

std::string IPv4Packet::getFragmentOffget()
{
	return fragmentOffset;
}

std::string IPv4Packet::getTTL()
{
	return ttl;
}

std::string IPv4Packet::getProtocol()
{
	return protocol;
}

std::string IPv4Packet::getHeaderChecksum()
{
	return headerChecksum;
}

std::string IPv4Packet::getSourceAddress()
{
	return sourceAddress;
}

std::string IPv4Packet::getDestinationAddress()
{
	return destinationAddress;
}

std::string IPv4Packet::getOptions()
{
	return options;
}

std::string IPv4Packet::getData()
{
	return data;
}
