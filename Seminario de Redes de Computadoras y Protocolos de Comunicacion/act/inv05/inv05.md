# Investigación V
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-02-28


## ICMP
Es un protocolo que sirve como apoyo para la suite IP; es usado por dispositivos de red (como routers) para mandar mensajes de error e información operacional. El objetivo de estos mensajes es proporcionar respuestas acerca de temas relacionados con el procesamiento de paquetes IP bajo determinadas condiciones; estos mensajes no son obligatorios y, a menudo, no son permitidos dentro de una red por razones de seguridad.

Los datos que estos mensajes incluye son:

* **Confirmación de Host**  
    Se puede utilizar un mensaje de eco para determinar si un host está o no en funcionamiento. Este uso es la base de la utilidad "ping"

* ** Destino o Servicio Inaccesible**
    Cuando un host o gateway recibe un paquete que no puede entregar, se utiliza este mensaje para notificar al emisor que el destino no se alcanzo.

* **Tiempo Superado**
    Se utiliza para indicar que un paquete no pudo enviarse debido a que se agoto su TTL; en ICMPv6 se utiliza el campo "Límite de Saltos", puesto que no existe el TTL.

* **Re-direccionamiento de Ruta**
    Puede utilizarse para indicar a los host de una red acerca de una mejor ruta disponible para un destino particular.


### Estructura
| Bytes | Campo |
| :-: | |
| 1 | Tipo ICMP |
| 1 | Código ICMP |
| 2 | Checksum ICMP |
| 2 | Identificador (Opcional) |
| 2 | Numero de secuencia (Opcional) |
| 4 | Datos ICMP |



### Bibliogafía
* <http://ecovi.uagro.mx/ccna1/course/module8/8.3.1.1/8.3.1.1.html>
* <http://processors.wiki.ti.com/index.php/CC31xx_%26_CC32xx_Transceiver_Mode>
* <http://www.tcpipguide.com/free/t_ICMPVersion4ICMPv4InformationalMessageTypesandForm.htm>
