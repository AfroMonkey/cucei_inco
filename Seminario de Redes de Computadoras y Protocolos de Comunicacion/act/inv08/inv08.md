# Investigación VIII Paquete UDP
Navarro Presas Moisés Alejandro - 215861509  
SSP de Redes de Computadoras y Protocolos de Internet - D04  
2017-05-02


## Paquete UDP
El protocolo de datagrama de usuario (UDP) es un protocolo no orientado a conexión de la capa de transporte del modelo TCP/IP por lo tanto es muy simple (ya que no proporciona detección de errores).

Los únicos servicios que proporciona a través de IP son checksumming de datos y multiplexación por número de puerto. Por lo tanto, un programa de aplicación que se ejecuta a través de UDP debe tratar directamente con los problemas de comunicación de extremo a extremo que un protocolo orientado a la conexión habría manejado.


### Estructura
![](udp)  

*   **Puerto de origen**: es el número de puerto relacionado con la aplicación del remitente del segmento UDP. Este campo es opcional (si no está especificado, los 16 bits de este campo se pondrán en cero y el destinatario no podrá responder).

*   **Puerto de destino**: este campo contiene el puerto correspondiente a la aplicación del equipo receptor al que se envía.

*   **Longitud**: este campo especifica la longitud total del segmento, con el encabezado incluido. La longitud del campo es necesariamente superior o igual a 8 bytes.

*   **Suma de comprobación**: es el cálculo como complemento de 16 bits del “checksum” de una pseudo encabezado de información de la cabecera IP, el encabezado UDP y los datos, rellenados según sea necesario con cero bytes al final para hacer un múltiplo de dos bytes. Si la suma de comprobación se borra a cero, la suma de comprobación se desactiva. Si la suma de comprobación calculada es cero, entonces este campo debe establecerse en 0xFFFF.



### Fuentes
*   <http://www.it.uc3m.es/lpgonzal/protocolos/transporte.php>
*   <https://www.ietf.org/rfc/rfc768.txt>
*   <https://http://es.ccm.net/contents/284-protocolo-udp>
*   <http://www.networksorcery.com/enp/protocol/udp.htm>
