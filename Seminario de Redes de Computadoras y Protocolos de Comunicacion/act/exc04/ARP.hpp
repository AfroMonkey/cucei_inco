#ifndef ARP_HPP
#define ARP_HPP

#include <string>

class ARP
{
public:
	void setDestinationMAC(std::string);
	void setSourceMAC(std::string);
	void setARPFrameType(std::string);
	void setHardwareType(std::string);
	void setProtocolType(std::string);
	void setHardwareLen(std::string);
	void setProtocolLen(std::string);
	void setARPOperation(std::string);
	void setSenderMACAddress(std::string);
	void setSenderIPAddress(std::string);
	void setDestinationMACAddress(std::string);
	void setDestinationIPAddress(std::string);
	void setEthernetChecksum(std::string);

	std::string getDestinationMAC();
	std::string getSourceMAC();
	std::string getARPFrameType();
	std::string getHardwareType();
	std::string getProtocolType();
	std::string getHardwareLen();
	std::string getProtocolLen();
	std::string getARPOperation();
	std::string getSenderMACAddress();
	std::string getSenderIPAddress();
	std::string getDestinationMACAddress();
	std::string getDestinationIPAddress();
	std::string getEthernetChecksum();

private:
	std::string destinationMAC;
	std::string sourceMAC;
	std::string ARPFrameType;
	std::string hardwareType;
	std::string protocolType;
	std::string hardwareLen;
	std::string protocolLen;
	std::string ARPOperation;
	std::string senderMACAddress;
	std::string senderIPAddress;
	std::string destinationMACAddress;
	std::string destinationIPAddress;
	std::string ethernetChecksum;
};

#endif