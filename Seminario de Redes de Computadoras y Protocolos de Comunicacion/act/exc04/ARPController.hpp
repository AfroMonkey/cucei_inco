#ifndef ARPCONTROLLER_HPP
#define ARPCONTROLLER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "ARP.hpp"

class ARPController
{
public:
	std::vector<unsigned char> readAllBytesFromFile(std::string);
	std::string hexString(std::vector<unsigned char>);
	std::string hexSubStr(std::string, int, int);
	std::string strMACFormat(std::string);
	std::string strIPFormat(std::vector<unsigned char>, int, int);
	std::string protocolType(std::string);
	std::string operationCode(std::string);
	ARP initializeARPPacket(std::string, std::vector<unsigned char>);
	void printARPPacket(ARP);
	void processPacket(std::string);
};

#endif