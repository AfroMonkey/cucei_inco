#include "MainWindow.hpp"
#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif
MainWindow::MainWindow()
{

}

void MainWindow::show()
{
	std::string option;
	bool flag = true;
	ARPController arpController;

	do
	{
    	system(CLEAR);
		cout << "[Menú principal]" << '\n' << '\n';
		cout << "1.- Ethernet ARP Reply" << '\n';
		cout << "2.- Ethernet ARP Request" << '\n';
		cout << "3.- Salir" << '\n' << '\n';
		cout << "Elija una opción: ";
		cin >> option;

		switch(atoi(option.c_str()))
		{
			case 1:
            	system(CLEAR);
				arpController.processPacket("ethernet_arp_reply.bin");
	            cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;
			case 2:
				system(CLEAR);
				arpController.processPacket("ethernet_arp_request.bin");
				cin.ignore();
            	cout << '\n' << "Presione cualquier tecla para continuar: ";
            	cin.get();
				break;

			case 3:
				flag = false;
				break;

			default:
				cout << "Error. Intente de nuevo." << '\n';
				cin.ignore();
				cin.get();
		}

	} while(flag);

}
