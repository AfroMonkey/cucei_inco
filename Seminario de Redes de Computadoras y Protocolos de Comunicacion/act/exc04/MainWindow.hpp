#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <iostream>
#include <cstdlib>

#include "ARPController.hpp"

using namespace std;

class MainWindow
{
public:
	MainWindow();
	void show();
};

#endif