#include "ARPController.hpp"

std::vector<unsigned char> ARPController::readAllBytesFromFile(std::string filename)
{
    std::ifstream file;
    std::vector<unsigned char> byteArray;
    unsigned char aux;

    file.open(filename.c_str(), std::ios::in | std::ios::binary);

    if(!file.is_open())
    {
    	std::cout << "This file couldn't opened" << std::endl;
    	std::cin.get();
    }
    else
    {
	    while(!file.eof())
	    {
	        file.read(reinterpret_cast<char*>(&aux), sizeof(unsigned char));
	        if(file.eof())
	            break;
	        byteArray.push_back(aux);
	    }

    	file.close();
    }

    return byteArray;
}

std::string ARPController::hexString(std::vector<unsigned char> data)
{
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (unsigned int i = 0; i < data.size(); i++)
    {
        ss << std::setw(2) << static_cast<int>(data[i]);
    }
    std::string s = ss.str();
    return s;
}

std::string ARPController::hexSubStr(std::string inputString, int from, int to)
{
	std::string str;
	for(int i = from; i <= to; i++)
		str += inputString[i];
	return str;
}

std::string ARPController::strMACFormat(std::string inpStr)
{
	std::string mac;
	for(int i = 0; i < inpStr.size(); i++)
	{
		mac += inpStr[i];
		if((i + 1) % 2 == 0 && (i + 1) != inpStr.size())
			mac += ":";
	}
	return mac;
}

std::string ARPController::strIPFormat(std::vector<unsigned char> data, int from, int to)
{
	std::string str;
	for(int i = from; i <= to; i++)
	{
		str += std::to_string(static_cast<int>(data[i]));
		if(i < to)
			str += ".";
	}
	return str;
}

std::string ARPController::protocolType(std::string inp)
{
	switch(atoi(inp.c_str()))
	{
		case 800:
			return "IPv4";
			break;
		case 805:
			return "IPv6";
			break;
		default:
			return "Desconocido";
	}
}

std::string ARPController::operationCode(std::string inp)
{
	switch(atoi(inp.c_str()))
	{
		case 1:
			return "Solicitud ARP";
			break;
		case 2:
			return "Respuesta ARP";
			break;
		case 3:
			return "Solicitud RARP";
			break;
		case 4:
			return "Respuesta RARP";
			break;
		default:
			return "Desconocido";
	}
}

ARP ARPController::initializeARPPacket(std::string hexStr, std::vector<unsigned char> data)
{
	ARP arpPacket;
	arpPacket.setDestinationMAC(strMACFormat(hexSubStr(hexStr, 0, 11)));
	arpPacket.setSourceMAC(strMACFormat(hexSubStr(hexStr, 12, 23)));
	arpPacket.setARPFrameType(hexSubStr(hexStr, 24, 27));
	arpPacket.setHardwareType(hexSubStr(hexStr, 28, 31));
	arpPacket.setProtocolType(hexSubStr(hexStr, 32, 35));
	arpPacket.setHardwareLen(hexSubStr(hexStr, 36, 37));
	arpPacket.setProtocolLen(hexSubStr(hexStr, 38, 39));
	arpPacket.setARPOperation(hexSubStr(hexStr, 40, 43));
	arpPacket.setSenderMACAddress(strMACFormat(hexSubStr(hexStr, 44, 55)));
	arpPacket.setSenderIPAddress(strIPFormat(data, 28, 31));
	arpPacket.setDestinationMACAddress(strMACFormat(hexSubStr(hexStr, 64, 75)));
	arpPacket.setDestinationIPAddress(strIPFormat(data, 38, 41));
	arpPacket.setEthernetChecksum(hexSubStr(hexStr, 84, hexStr.size() - 1));
	return arpPacket;
}

void ARPController::printARPPacket(ARP arpPacket)
{
	std::cout << "Ethernet Destination MAC: " << arpPacket.getDestinationMAC() << '\n';
	std::cout << "Ethernet Source MAC: " << arpPacket.getSourceMAC() << '\n';
	std::cout << "ARP Frame Type: " << arpPacket.getARPFrameType() << '\n';
	std::cout << "Hardware Type: " << arpPacket.getHardwareType() << " | Ethernet" << '\n';
	std::cout << "Protocol Type: " << arpPacket.getProtocolType() << " | " << protocolType(arpPacket.getProtocolType()) <<'\n';
	std::cout << "Hardware length: " << arpPacket.getHardwareLen() << " | IEEE 802.3" << '\n';
	std::cout << "Protocol length: " << arpPacket.getProtocolLen() << " | IP" << '\n';
	std::cout << "ARP Operation: " << arpPacket.getARPOperation() << " | " << operationCode(arpPacket.getARPOperation()) << '\n';
	std::cout << "Sender MAC Address: " << arpPacket.getSenderMACAddress() << '\n';
	std::cout << "Sender IP Address: " << arpPacket.getSenderIPAddress() << '\n';
	std::cout << "Destination MAC Address: " << arpPacket.getDestinationMACAddress() << '\n';
	std::cout << "Destination IP Address: " << arpPacket.getDestinationIPAddress() << '\n';
	std::cout << "Ethernet Checksum: " << arpPacket.getEthernetChecksum() << '\n';
}

void ARPController::processPacket(std::string filename)
{
	std::vector<unsigned char> byteArray = readAllBytesFromFile(filename);
	if(byteArray.size() > 0)
	{
		std::string hexStr = hexString(byteArray);
		ARP arpPacket = initializeARPPacket(hexStr, byteArray);
		printARPPacket(arpPacket);
	}
}