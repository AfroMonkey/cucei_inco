#include "ARP.hpp"

void ARP::setDestinationMAC(std::string destinationMAC)
{
	this->destinationMAC = destinationMAC;
}

void ARP::setSourceMAC(std::string sourceMAC)
{
	this->sourceMAC = sourceMAC;
}

void ARP::setARPFrameType(std::string ARPFrameType)
{
	this->ARPFrameType = ARPFrameType;
}

void ARP::setHardwareType(std::string hardwareType)
{
	this->hardwareType = hardwareType;
}

void ARP::setProtocolType(std::string protocolType)
{
	this->protocolType = protocolType;
}

void ARP::setHardwareLen(std::string hardwareLen)
{
	this->hardwareLen = hardwareLen;
}

void ARP::setProtocolLen(std::string protocolLen)
{
	this->protocolLen = protocolLen;
}

void ARP::setARPOperation(std::string ARPOperation)
{
	this->ARPOperation = ARPOperation;
}

void ARP::setSenderMACAddress(std::string senderMACAddress)
{
	this->senderMACAddress = senderMACAddress;
}

void ARP::setSenderIPAddress(std::string senderIPAddress)
{
	this->senderIPAddress = senderIPAddress;
}

void ARP::setDestinationMACAddress(std::string destinationMACAddress)
{
	this->destinationMACAddress = destinationMACAddress;
}

void ARP::setDestinationIPAddress(std::string destinationIPAddress)
{
	this->destinationIPAddress = destinationIPAddress;
}

void ARP::setEthernetChecksum(std::string ethernetChecksum)
{
	this->ethernetChecksum = ethernetChecksum;
}

std::string ARP::getDestinationMAC()
{
	return destinationMAC;
}

std::string ARP::getSourceMAC()
{
	return sourceMAC;
}

std::string ARP::getARPFrameType()
{
	return ARPFrameType;
}

std::string ARP::getHardwareType()
{
	return hardwareType;
}

std::string ARP::getProtocolType()
{
	return protocolType;
}

std::string ARP::getHardwareLen()
{
	return hardwareLen;
}

std::string ARP::getProtocolLen()
{
	return protocolLen;
}

std::string ARP::getARPOperation()
{
	return ARPOperation;
}

std::string ARP::getSenderMACAddress()
{
	return senderMACAddress;
}

std::string ARP::getSenderIPAddress()
{
	return senderIPAddress;
}

std::string ARP::getDestinationMACAddress()
{
	return destinationMACAddress;
}

std::string ARP::getDestinationIPAddress()
{
	return destinationIPAddress;
}

std::string ARP::getEthernetChecksum()
{
	return ethernetChecksum;
}