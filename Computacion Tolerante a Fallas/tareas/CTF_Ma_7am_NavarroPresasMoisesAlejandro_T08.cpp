#include <cstdio>

void rec(int a, int b) {
  printf("%d\n", a);
  rec(a+b, b);
}

int main() {
  int a, b;
  scanf("%d %d", &a, &b);
  rec(a, b);
}
