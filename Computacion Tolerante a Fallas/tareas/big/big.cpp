#include <cstdio>
#include <list>

using namespace std;

list<int> number_a;
list<int> number_b;
list<int>* bigger, *lower;
int global;

void sum() {
  bool carry = false;
  if (number_a.size() > number_b.size()) {
    bigger = &number_a;
    lower = &number_b;
  } else {
    bigger = &number_b;
    lower = &number_a;
  }
  for (auto ib = bigger->end(), il = lower->end(); ib != bigger->begin();) {
    --ib;
    if (carry) {
      ++*ib;
      carry = false;
    }
    if (il != lower->begin()) {
      --il;
      *ib += *il;
      if (*ib > 9) {
        *ib -= 10;
        carry = true;
        if (ib == bigger->begin()) {
          bigger->push_front(0);
        }
      }
    }
  }
}

void rec() {
  ++global;
  sum();
  if (global < 9999) {
    rec();
  }
}

int main() {
  char c;
  while (true) {
    scanf("%c", &c);
    if (c == '\n') {
      break;
    }
    number_a.push_back(c - '0');
  }
  while (true) {
    scanf("%c", &c);
    if (c == '\n') {
      break;
    }
    number_b.push_back(c - '0');
  }
  bigger = &number_a;

  rec();

  for (auto d : *bigger) {
    printf("%d", d);
  }
  printf("\n");

  return 0;
}
