#ifndef CAPTURARETERNIDADUNIVERSO_H_INCLUDED
#define CAPTURARETERNIDADUNIVERSO_H_INCLUDED

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <list>
#include <ctime>
#include <limits>
#include "operarEternidadUniverso.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

using namespace std;

/*
long double superNumero(list<int> eternidadDelUniverso)
{
    long double super = 0;
    long double contar;
    long double indicador = 1;
    long double tamano = eternidadDelUniverso.size()-1;

    for(contar = tamano; contar >= 0; contar--)
    {
        super = super + (eternidadDelUniverso[contar] * indicador);
        indicador = indicador * 10;
    }

    return super;
}
*/

bool captura(list<int> eternidadDelUniverso)
{
    long double recibido;
    //long double recibidoAux;
    int numero;
    bool esNumero;
    bool estado = false;
    int cero = 0;
    //unsigned long long int vueltas = 0;
    //cin.get();
    cout << "Escribe un numero: " << endl;
    system("stty raw");//llamada al sistema para captura de tecla por tecla

    ////////////Inicializar variables locales//////////////////////////
    recibido = 0;
    //recibidoAux = 0;
    numero = 0;
    esNumero = true;
    ///////////////////////////////////////////////////////////////////
    while (numero != 13) //Ciclar mientras no sea entrar (13 acsii de la tecla entrar)
    {
        //vueltas++;
        recibido = getchar(); //Capturar lo recibido por teclado
        numero = recibido - 48;
        if((numero == 0) && (cero == 0))
        {
            numero = 13;
            esNumero = false;
        }
        else
        {

            if(numero > -1 && numero < 10)
            {
                //En caso de que sea un numero agregarlo a la lista
                esNumero = true;
                eternidadDelUniverso.push_back(numero);
                //recibidoAux = recibidoAux * 10 + numero;
            }
            else if(numero == -35)
            {
                numero = 13;
                esNumero = true;
            }
            else
            {
                numero = 13;
                esNumero = false;
            }
        }
        cero = 1;

    }

    system("stty cooked"); // regresnado a la normalidad la captura desde teclado
    int opcion = 0;
    float tiem = 0;
    if(esNumero)
    {
        long double tamano = eternidadDelUniverso.size();
        cout << endl << "El numero ingresado tiene: "<<tamano<<" Digitos totales!" << endl <<endl;
        cout << "Tiempo Estimado en";
        tiem = tiempo(tamano);
        cout << tiem;
        cout << endl <<endl;
        cout << "1. Continuar."<<endl;
        cout << endl;
        cout << "Otra tecla + entrar. Capturar otro numero."<<endl;
        cout << "Opcion: ";
        opcion = 0;
        opcion = getchar();
        opcion = opcion - 48;
        switch(opcion)
        {
        case 1:
        {
            estado = true;
            cout<<endl;
            cin.get();
        }
        break;
        default:
        {
            estado = false;
            cin.get();
        }
        break;
        }
    }
    else
    {
        cout << endl << "Verifique que sean unicamente numeros! o que el numero no inicie con 0!" << endl;
        cout << "------------------------------------------------------------------------" << endl <<endl;
        estado = false;
    }

    /*
    if(estado)
    {
        long double SG = superNumero(eternidadDelUniverso);
        cout << endl << "Numero Ingresado: "<< SG <<endl <<endl;
        long double cuadrado1 = SG * SG;
        cout << endl << "Numero Cuadrado: "<< cuadrado1 <<endl <<endl;

    }
    */

    if(estado && (!eternidadDelUniverso.empty()))
    {
        clock_t t_ini, t_fin;
        float secs;

        eternidadUniverso casiInfinito;
        casiInfinito.n = eternidadDelUniverso;
        int continuar = 1;

        if((casiInfinito.n.size() == 1) && (casiInfinito.n.back() == 1))
        {
            t_ini = clock();
            int uno = 0;
            uno = casiInfinito.n.back();
            cout << endl << "Su cadrado es: " << endl;
            uno = uno * uno;
            cout << uno;
            /*
            cout << endl <<endl << "Calcular felicidad..." << endl;
            //cout << uno;
            cout <<endl <<endl << "La suma de los cuadrados es: " << endl;
            cout << uno;
            cout <<endl <<endl << "El numero: "<<endl;
            cout << uno;
            cout <<endl <<endl << "Es Feliz!"<<endl;
            */
            t_fin = clock();
            secs = (float)(t_fin - t_ini) / CLOCKS_PER_SEC;
            //printf("Tiempo Total en Segundos: %.16g ", secs);
            continuar = 0;
        }
        else if((casiInfinito.n.size() == 1) && (casiInfinito.n.back() == 0))
        {
            t_ini = clock();
            int uno = 0;
            uno = casiInfinito.n.back();
            cout << endl << "Su cadrado es: " << endl;
            uno = uno * uno;
            cout << uno;
            /*
            cout << endl <<endl << "Calcular felicidad... " << endl;
            //cout << uno;
            cout <<endl <<endl << "La suma de los cuadrados es: " << endl;
            cout << uno;
            cout <<endl <<endl << "El numero: "<<endl;
            cout << uno;
            cout <<endl <<endl << "NO es Feliz!"<<endl;
            */
            t_fin = clock();
            secs = (float)(t_fin - t_ini) / CLOCKS_PER_SEC;
            //printf("Tiempo Total en Segundos: %.16g ", secs);
            continuar = 0;
        }
        /*
        else if((casiInfinito.n.size() == 1) && (casiInfinito.n.back() == 2))
        {
            casiInfinito.n.push_front(0);
        }
        else if((casiInfinito.n.size() == 1) && (casiInfinito.n.back() == 3))
        {
            casiInfinito.n.push_front(0);
        }
        else if((casiInfinito.n.size() == 1) && (casiInfinito.n.back() == 4))
        {
            casiInfinito.n.push_front(0);
        }
        */

        if(continuar == 1)
        {
            t_ini = clock();
            eternidadUniverso seVieneElFeliz;
            seVieneElFeliz = cuadrado(casiInfinito);

            //eternidadUniverso temporal;
            //eternidadUniverso auxiliar;

            cout << endl << "Su cadrado es: " << endl;
            imprimir(seVieneElFeliz);

            /*
            cout << endl <<endl << "Calcular felicidad..." << endl;
            //imprimir(seVieneElFeliz);
            //cout <<endl <<endl << "Cuadrados de los digitos: " << endl;
            temporal = haciaElFelizCuadradinesAlterno(seVieneElFeliz); ///Regresa el cuadrado de los digitos.
            //imprimir(temporal);
            cout <<endl <<endl << "La suma de los cuadrados es: " << endl;
            auxiliar = haciaElFelizSumasConAlterno(temporal); ///Regresa la suma de los digitos.
            imprimir(auxiliar);
            //cout << endl;

            int feliz = 0;
            feliz = calcularFelicidad(auxiliar);
            cout << endl;
            if(feliz == 1)
            {
                cout <<endl << endl << "El numero es Feliz!"<<endl;
                t_fin = clock();
                secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
                printf("Tiempo Total en Segundos: %.16g ", secs);
            }
            else
            {
                cout <<endl <<endl << "El numero No es Feliz!"<<endl;
                t_fin = clock();
                secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
                printf("Tiempo Total en Segundos: %.16g ", secs);
            }
            */
            t_fin = clock();
            if(tiem == 0.001)
            {
                secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
                cout<<endl;
                printf("Tiempo Total en Segundos: %.16g ", secs);
            }

        }

    }

    return estado;
}

#endif // CAPTURARETERNIDADUNIVERSO_H_INCLUDED
