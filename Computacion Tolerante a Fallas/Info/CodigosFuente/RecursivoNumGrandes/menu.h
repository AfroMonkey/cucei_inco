#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <list>
#include <ctime>
#include <limits>
#include "capturarEternidadUniverso.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

void ejecutar()
{
    list<int> eternidadDelUniverso;
    bool estado;

    estado = captura(eternidadDelUniverso);

    while(!estado)
    {
        eternidadDelUniverso.clear();
        estado = captura(eternidadDelUniverso);
    }


}

void menuPrincipal()
{
    int opcion = 0;
    list<int> op;
    do
    {
        system(CLEAR);
        opcion = 0;
        cout << "******** Cuadrado de un numero. ********" <<endl <<endl;
        cout << "1. Introducir numero y calcular su cuadrado." <<endl;
        cout << "2. Salir." <<endl;
        cout << "Opcion: ";

        system("stty raw");
        while(opcion != 13)
        {
            opcion = getchar();
            opcion = opcion - 48;
            if(opcion == -35)
            {
                opcion = 13;
            }
            else
            {
                op.push_back(opcion);
            }
        }
        opcion = op.back();
        system("stty cooked");
        //cout<<endl<<op.size()<<endl;

        if((op.size() == 1) && (opcion != 13))
        {

            switch(opcion)
            {

            case 1:
            {
                system(CLEAR);
                cout << "******** Cuadrado de un Numero. ********" <<endl <<endl;
                ejecutar();
                cin.get();
            }
            break;

            case 2:
            {
            }
            break;

            default:
            {
                cout << endl << "Error!, Opcion No correcta!" <<endl <<endl;
                cin.get();
                cin.get();
                op.clear();
            }
            break;

            }
        }
        /*
        else if(op.size() > 1 && opcion == 13)
        {
            cout << endl << "Error!, Opcion No correcta!" <<endl <<endl;
            cin.get();
            opcion = 3;
            op.clear();
        }
        else if((opcion != 1) && (opcion != 2))
        {
            cout << endl << "Error!, Opcion No correcta!" <<endl <<endl;
            //cout << opcion;
            cin.get();
            opcion = 3;
            op.clear();
        }
        */
        else if(op.size() > 1)
        {
            cout << endl << "Error!, Opcion No correcta!" <<endl <<endl;
            //cout << opcion;
            cin.get();
            opcion = 3;
            op.clear();
        }
        op.clear();
    }
    while(opcion != 2);

}


#endif // MENU_H_INCLUDED
