#ifndef OPERARETERNIDADUNIVERSO_H_INCLUDED
#define OPERARETERNIDADUNIVERSO_H_INCLUDED

#include <list>
#include <vector>
#include <iostream>
#include <time.h>
#include <cmath>

#define MAXC 9999999
#define FRECUENCIA 800000
#define MINUTO 60

using namespace std;

class eternidadUniverso
{
public:
    list<int> n;
};

class parcial
{
public:
    eternidadUniverso v;
    eternidadUniverso sums;
};

void imprimir(eternidadUniverso a)
{

    while (a.n.size() > 0)
    {
        cout << a.n.front();
        a.n.pop_front();
    }
}

eternidadUniverso suma(eternidadUniverso a, eternidadUniverso b)
{
    eternidadUniverso c;

    int iA, iB;
    int opA, opB, resC, carryC;
    unsigned int conta = 0;
    unsigned int conta2 = 0;

    iA = a.n.size() - 1;
    iB = b.n.size() - 1;

    resC = 0;
    carryC = 0;

    while (((iA >= 0) || (iB >= 0)) && (conta2 < MAXC))
    {
        while (((iA >= 0) || (iB >= 0)) && (conta < MAXC))
        {
            if (iA >= 0)
            {
                opA = a.n.back();
                a.n.pop_back();
            }
            else
                opA = 0;

            if (iB >= 0)
            {
                opB = b.n.back();
                b.n.pop_back();
            }
            else
                opB = 0;

            resC = opA + opB + carryC;
            if (resC < 0)
            {
                carryC = -1;
                resC =  10+resC;
            }
            else
            {
                carryC = resC / 10;
            }
            resC = resC % 10;

            c.n.push_front(resC);
            iA--;
            iB--;
            conta++;
        }
        conta2++;
        conta = 0;
    }
    if(carryC)
    {
        c.n.push_front(carryC);
    }
    return c;


}

eternidadUniverso negar(eternidadUniverso a)
{
    eternidadUniverso b;
    int conta = 0;
    int conta2 = 0;
    while ((a.n.size() > 0) && (conta2 < MAXC))
    {
        while ((a.n.size() > 0) && (conta < MAXC))
        {
            b.n.push_front(-a.n.back());
            a.n.pop_back();
            conta++;
        }
        conta2++;
        conta = 0;
    }
    return b;
}

eternidadUniverso restar(eternidadUniverso a, eternidadUniverso b)
{
    return suma(a, negar(b));
}

int comparar(eternidadUniverso a, eternidadUniverso b)
{
    eternidadUniverso res;
    int x;
    int conta = 0;
    int conta2 = 0;

    res = restar(a, b);

    while ((res.n.size() > 0) && (conta2 < MAXC))
    {
        while ((res.n.size() > 0) && (conta < MAXC))
        {
            x = res.n.front();
            res.n.pop_front();
            if (x > 0)
            {
                return 1; // a > b
            }
            else if (x < 0)
            {
                return -1; // b > a
            }
            conta++;
        }
        conta2++;
        conta = 0;
    }
    return 0; // b == a.0
}

eternidadUniverso cuadrado(eternidadUniverso a)
{
    eternidadUniverso salida;
    eternidadUniverso i;
    eternidadUniverso topeSumasMaximas;
    eternidadUniverso i1;
    list<parcial> sumasParciales;
    parcial b, c;
    bool esEficiente = true;
    int comp;
    unsigned int digitos = 0;

    i.n.push_back(1);
    i1.n.push_back(1);

    b.v = a;
    b.sums = i;

    sumasParciales.push_front(b);
    int contadorCuadrado = 0;
    int contadorCuadrado2 = 0;
    int cicloFin = 0;


    salida = a;
    int compAux = 0;

    while ((compAux >= 0) && (contadorCuadrado < MAXC) && (cicloFin != 1))
    {
        while ((compAux >= 0) && (contadorCuadrado < MAXC) && (cicloFin != 1))
        {

            compAux = comparar(a, i);

            if (i.n.size() > digitos)
            {
                digitos++;
            }

            c = sumasParciales.back();

            salida = suma(b.v, c.v);
            topeSumasMaximas = suma(b.sums, c.sums);

            comp = comparar(a, topeSumasMaximas);

            if (comp < 0)   // Hay más sumas potenciales que máximas
            {
                sumasParciales.pop_back();
                esEficiente = false;
            }
            else if (comp > 0)   // Hay menos sumas potenciales que máximas
            {
                b.sums = topeSumasMaximas;
                b.v = salida;
                if(esEficiente)
                {
                    sumasParciales.push_back(b);
                }
                i = suma(i, c.sums);
            }
            else   // Son exactamente iguales
            {
                //cout<<endl<<"Contador Cuadrado: "<<contadorCuadrado<<endl;
                cicloFin = 1;
            }
            contadorCuadrado++;

        }
        //cout<<endl<<"Contador Cuadrado: "<<contadorCuadrado<<endl;
        contadorCuadrado2++;
        contadorCuadrado = 0;
    }

    return salida;
}

eternidadUniverso cuadradoSumasSucesivas(eternidadUniverso a)
{
    eternidadUniverso sumasParciales;
    eternidadUniverso aAux;
    eternidadUniverso salida;
    eternidadUniverso topeSumasMaximas; //es el tope o numero de repeticiones
    eternidadUniverso topeAux1;
    eternidadUniverso topeAux2;
    eternidadUniverso topeAux3;
    int comparacion = 1;

    topeAux1.n.push_back(1);
    topeAux2.n.push_back(1);
    topeAux3.n.push_back(1);
    aAux = a;

    while(comparacion != 0)
    {

        ///Calcular tope
        topeSumasMaximas = suma(topeAux1, topeAux2);
        comparacion = comparar(topeSumasMaximas, a);
        ///Sumas sucesivas
        sumasParciales = suma(a,aAux);
        aAux = sumasParciales;
        if(comparacion != 0)
        {
            topeAux2 = suma(topeAux2, topeAux3);
        }
    }
    salida = sumasParciales;
    return salida;

}

eternidadUniverso haciaElFelizCuadradinesAlterno(eternidadUniverso a)
{
    int temporal = 0;

    list<int> felicesTemporal;

    while (a.n.size() > 0)
    {
        temporal = a.n.front();
        temporal = temporal * temporal;

        felicesTemporal.push_back(temporal);


        //cout << endl << "temporal: " << temporal <<endl;
        if(!a.n.empty())
        {
            a.n.pop_front();
        }
    }

    eternidadUniverso nuevoF;
    nuevoF.n = felicesTemporal;

    return nuevoF;
}

eternidadUniverso haciaElFelizCuadradines(eternidadUniverso a)
{
    int temporal = 0;
    int aux = 0;
    int carry = 0;

    list<int> felicesTemporal;

    while (a.n.size() > 0)
    {
        temporal = a.n.front();
        temporal = temporal * temporal;
        aux = temporal % 10;
        carry = temporal / 10;

        if(carry > 0)
        {
            felicesTemporal.push_back(carry);
        }

        felicesTemporal.push_back(aux);


        //cout << endl << "temporal: " << temporal <<endl;

        a.n.pop_front();
    }

    eternidadUniverso nuevoF;
    nuevoF.n = felicesTemporal;

    return nuevoF;
}

eternidadUniverso haciaElFelizSumasConAlterno(eternidadUniverso a)
{
    int temporal = 0;
    int aux = 0;
    int carry = 0;
    bool inicio = true;

    list<int> felicesTemporal;
    list<int> felicesTemporal2;
    list<int> resultadoTemporal;
    eternidadUniverso A;
    eternidadUniverso B;
    eternidadUniverso R;

    while (a.n.size() > 0)
    {
        if(inicio)
        {
            ///Separar en una lista temporar el numero por digitos
            temporal = a.n.front();
            //cout<<endl<<"Temporal: "<<temporal<<endl;
            aux = temporal % 10;
            carry = temporal / 10;
            //cout<<endl<<"Carry: "<<carry<<endl;

            if(carry > 0)
            {
                felicesTemporal.push_back(carry);
            }

            felicesTemporal.push_back(aux); ///En este momento ya hay un numero completo separado en digitos

            a.n.pop_front();
            inicio = false;
            A.n = felicesTemporal;
            R = A;
            //cout<<endl<<"Aux: "<<aux<<endl;

            if(!a.n.empty())
            {
                ///Repetir con una segunda lista temporal
                temporal = a.n.front();
                aux = temporal % 10;
                carry = temporal / 10;

                if(carry > 0)
                {
                    felicesTemporal2.push_back(carry);
                }

                felicesTemporal2.push_back(aux);

                if(!a.n.empty())
                {
                    a.n.pop_front();
                    B.n = felicesTemporal2;

                    ///Se llama a la suma
                    R = suma(A, B);
                    resultadoTemporal = R.n;
                }
            }

        }
        else
        {
            //cout<<endl<<"Entre"<<endl;
            felicesTemporal2.clear();
            felicesTemporal.clear();
            temporal = a.n.front();
            aux = temporal % 10;
            carry = temporal / 10;

            if(carry > 0)
            {
                felicesTemporal2.push_back(carry);
            }

            felicesTemporal2.push_back(aux);

            a.n.pop_front();
            B.n = felicesTemporal2;

            ///Se llama a la suma
            A.n = R.n;
            R = suma(A, B);
        }



    }

    return R;
}

int calcularFelicidad(eternidadUniverso a)
{
    eternidadUniverso felicidad;
    felicidad.n = a.n;
    int feliz = 0;
    int parar = 0;
    vector<int> repetidos;
    repetidos.push_back(0);

    if((a.n.size() == 1) && a.n.back() == 0)
    {
        parar = 1;
        feliz = 0;
        //cout <<"entre";
    }
    else
    {
        while((parar != 1))
        {
            felicidad = haciaElFelizCuadradinesAlterno(felicidad);
            felicidad = haciaElFelizSumasConAlterno(felicidad);
            if(!felicidad.n.empty())
            {
                //cout <<endl <<endl << "La suma de los cuadrados es: " << endl;
                cout<<endl;
                imprimir(felicidad);

                if(felicidad.n.size() == 1)
                {
                    repetidos.push_back(felicidad.n.back());
                }

                //si se repite un numero 3 veces se cumple la condicion de paro.
                for(unsigned int i = 0; i < repetidos.size(); i++)
                {
                    if(i > 2)
                    {
                        int bus = 0;
                        bus = repetidos[i];
                        repetidos[i] = 0;
                        for(int unsigned j = 0; j < repetidos.size(); j++)
                        {
                            if((bus == repetidos[j]) && (bus != 0))
                            {
                                parar = 1;
                            }
                        }
                    }
                }

                /*if((felicidad.n.size() == 1) && (felicidad.n.back() == 4))
                {
                    parar = 1;
                    //cout <<endl << "No es Feliz!"<<endl;
                }
                else */if((felicidad.n.size() == 1) && (felicidad.n.back() == 1))
                {
                    parar = 1;
                    feliz = 1;
                    //cout <<endl << "Es Feliz!"<<endl;
                }
            }
            else
            {
                parar = 1;
                feliz = 1;
            }
        }
    }

    return feliz;
}

float tiempo(int tamano)
{
    float tiempoR = 0;
    if(tamano == 1)
    {
        tiempoR = 0.001;
        cout << " segundos: ";
    }
    else
    {
        tiempoR = ((pow(tamano,2))* 50) / FRECUENCIA;

        if(tiempoR > MINUTO)
        {
            tiempoR = tiempoR / MINUTO;
            cout << " minutos: ";
        }
        else if(tiempoR > MINUTO)
        {
            tiempoR = tiempoR / MINUTO;
            cout << " horas: ";
        }
        else
        {
            cout << " segundos: ";
        }
    }
    return tiempoR;
}

#endif // OPERARETERNIDADUNIVERSO_H_INCLUDED
