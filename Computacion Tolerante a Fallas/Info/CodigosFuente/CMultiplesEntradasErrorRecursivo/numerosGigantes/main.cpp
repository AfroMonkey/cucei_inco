///------------- Universidad de Guadalajara ----------------
///Centro Universitario de Ciencias Exactas e Ingenierías---
///Materia: Computación Tolerante a Fallas
///Ciclo: 2017-B
///Alumno: Andrea Judit Parkhurst Casas
///Codigo: 210073707
///Forma de Compilación: g++ main.cpp -o main
///---------------------------------------------------------

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <ctime>
#include <limits>
#define MAX 256

#ifdef _WIN32   _// para usar el sistem cls si estasa en windows
#define CLEAR "cls"
#else
#define CLEAR "clear"  // para usar el sistem clear si estas en linux
#endif

using namespace std;



void limpiarPantalla()
{
    system(CLEAR);
}

void pausa()
{
    cout<<endl<<"Presione Entrar para continuar...";
cin.ignore(numeric_limits<streamsize>::max(),'\n');
}



int main()
{
    unsigned long long int i=0;
    vector<int> Vectorzaso;   // capturar el numero
    long double num, num2;
    long double recibidos, recibidos2;
    int numero, numero2;
    char menu[MAX], menu2[MAX];
    bool esNumero, esNumero2;
    do
    {
        limpiarPantalla();
        esNumero=true;
        esNumero2=true;
        numero=0;
        numero2=0;
        recibidos=0;
        recibidos2=0;

        cout<<"------------------------------------------------------------------"<<endl;
        cout<<endl<<"1. Ingresar un Numero"<<endl;
        cout<<"2. Salir"<<endl;
        cout<<endl<<"Ingrese su Opcion: ";
        cin.getline(menu,256);
        if(strcmp(menu,"1")!=0 and strcmp(menu,"2")!=0)    // compara el string menu , con el uno , y con el dos
        {
            esNumero2=false;

        }

        if(esNumero2)
        {
          // cout<<"menu 1:"<<menu[1]<<"."<<endl;

            switch(menu[0])
            {


            case '1':
            {
                cout<<"------------------------------------------------------------------"<<endl;
                cout<<"Escribe un numero: ";
                 // cin.ignore();
                 // fflush( stdout ); //Recoger toda la entrada en buffer
                 // cout<<endl<<"Numero: "<<numDigitos(numero)<<endl;
                system("stty raw");   // llamada al sistema para captura de caracter por caracter
                num = 0;
                i=0;

                while (num!=13)
                    // con getchar almacenar lo escrito con el teclado
                {
                    i++;
                    num=getchar();
                    numero=num-48;

                    // cout<<"I: "<<num;
                    // cout<<"Numero: "<<numero<<endl;

                    if(numero>-1 and numero<10)
                    {
                        esNumero=true;
                        Vectorzaso.push_back(numero);
                        recibidos=recibidos*10+numero;
                        // cout<<numero;
                        // cout<<"recibidos: "<<recibidos<<endl;
                    }
                    else if(numero==-35)
                    {
                        esNumero=true;
                    }
                    else
                    {
                        esNumero=false;
                        num=13;
                    }
                    if(Vectorzaso.front()==0)
                    {
                    esNumero=false;
                    }


                }

                system("stty cooked"); // regresnado a la normalidad la llamada de caracteres

                // cout<<"I: "<<num;
                if(esNumero)
                {

                    cout<<endl<<"El numero ingresado tiene: "<<i-1<<" de Digitos!"<<endl;
                    // cout<<"recibidos "<<recibidos<<endl;

                    if(recibidos<100000000)
                    {
                        cout<<endl<<"ERROR: el numero ingresado es menor a 100,000,000 (cien millones)"<<endl;
                        pausa();
                    }
                    else
                    {

                        do
                        {
                        regresar:

                            int k=0;
                            char yes[MAX];
                            cout<<endl<<endl<<"Ingrese Numero a buscar: ";
                            system("stty raw");
                            num2= 0;

                     recibidos2=0;
                            while (num2!=13)

                    // con getchar almacenar lo escrito con el teclado
                {
                    num2=getchar();
                    numero2=num2-48;

                    // cout<<"I: "<<num;
                    // cout<<"Numero: "<<numero<<endl;

                    if(numero2>-1 and numero2<10)
                    {
                        esNumero=true;
                        recibidos2=recibidos2*10+numero2;
                        // cout<<numero;
                        // cout<<"recibidos: "<<recibidos<<endl;
                    }
                    else if(numero2==-35)
                    {
                        esNumero=true;
                    }
                    else
                    {
                        esNumero=false;
                        num2=13;
                    }


                }

                system("stty cooked");

                if(esNumero)
                {


                            float calculo;
                            calculo=(3*(float)i-1)/CLOCKS_PER_SEC;
                            cout<<endl<<"Tiempo restante aproximado: "<<calculo<<" segundos!"<<endl;
                            cout<<endl<<"Seguro desea hacer la busqueda? "<<endl;
                            cout<<"Si: 1"<<endl<<"No: Cualquier otra tecla"<<endl;
                            cout<<endl<<"Su opcion: ";
                            k=recibidos2;
                                  // cin.ignore();
                            cin.getline(yes,256);

                            if(strcmp(yes,"1")==0)
                            {
                                int z=0;
                                int tamano=Vectorzaso.size();
                                for(int i=0; i<tamano; i++)
                                {
                                    if(k==Vectorzaso[i])
                                    {
                                        z++;
                                    }
                                }

                                if(z>0)
                                {
                                    cout<<endl<<"El digito "<<k<<" se repite "<<z<<" veces!"<<endl;
                                }
                                else
                                {
                                    cout<<endl<<"El digito "<<k<<" No existe!"<<endl;
                                }

                                cout<<"------------------------------------------------------------------"<<endl;
                                cout<<endl<<"Desea Buscar otro digito?"<<endl;
                                cout<<"Si: 1"<<endl<<"No: Cualquier otra tecla"<<endl;
                                cout<<"Su Opcion: ";
                                cin.getline(menu2,256);
                                cout<<endl;
                                 if(strcmp(menu2,"1")!=0)
                            {
                                menu2[0]='n';
                            }

                            }
                            else
                            {
                                menu2[0]='n';
                            }
                            }
                            else{
                            cout<<endl<<"ERROR: Al parecer ingreso un numero NO entero, letra u otro simbolo distinto!"<<endl;
                            cout<<"       Solo se pueden Ingresar Numeros Enteros!"<<endl;
                            pausa();
                            goto regresar;
                            }
                        }
                        while(menu2[0]=='1');
                    }
                }
                else
                {
                    cout<<endl<<"ERROR: Al parecer ingreso un numero NO entero, letra u otro simbolo distinto!"<<endl;
                    cout<<"       o ingreso un numero con 0 inicial (Procure no iniciar con 0)!"<<endl;
                    cout<<"       Solo se pueden Ingresar Numeros Enteros!"<<endl;
                    pausa();
                }
                Vectorzaso.clear();
                break;
            }
            case '2':
            {
                cout<<endl<<"Presione Entrar para Salir...";
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                return 0;
                break;
            }
            default:
                cout<<endl<<"ERROR: la opcion elegida no existe!"<<endl;
                pausa();
                break;
            }
        }
        else
        {
            cout<<endl<<"ERROR: la opcion elegida no existe!"<<endl;
            pausa();
                // cin.get();
            menu[0]='3';
        }
    }
    while(menu[0]!='2');

    return 0;
}
