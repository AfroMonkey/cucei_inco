///------------- Universidad de Guadalajara ----------------
///Centro Universitario de Ciencias Exactas e Ingenierías---
///Materia: Computación Tolerante a Fallas
///Ciclo: 2017-B

///Alumno: Andrea Judit Parkhurst Casas
///Codigo: 210073707
///Practica: 3 Sumas Recursivas

///Forma de Compilación: g++ main.cpp -o main
///desbloquear limite de pila:  ulimit -s unlimited
///---------------------------------------------------------

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <ctime>
#include <limits>
#include <ctype.h>
#include <cmath>
#include <inttypes.h>
#include <cstdint>
#define MAX 256

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

using namespace std;

typedef unsigned int uint128_t __attribute__((mode(TI)));
 uint128_t f=1,b=0,cc=0,d=0, ban=0;      /// Define una variable de 128 bits
 uint128_t dfg, recovery;                /// Define una variable de 128 bits
long double a=-1;

void limpiarPantalla()
{
    system(CLEAR);
}

void pausa()
{
    cout<<endl<<"Presione Entrar para continuar...";
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
}


uint128_t ssum(uint128_t x)
{
    return x;
}

long double yey(long double x)
{
    return x;
}

uint128_t impare(uint128_t x)   /// Estas cosas son gets de toda la vida
{
    return x;
}

uint128_t recuperacion(uint128_t x)
{
    return x;
}



uint128_t imparesRecursivo(long long int y, uint128_t c,uint128_t numImpar, uint128_t sumas)
{
    uint128_t impar, suma;
    impar=numImpar;
    impar--;
    suma=sumas;
    c=c+1;
    y=y+2;
    a=y;
    cc=impar;
    suma=suma+y;
    d=suma;
    //cout<<"Suma: "<<suma<<"  Y: "<<y<<"  impar: "<<impar<<endl; son para debuguear
    // cout<<"iteracion: "<<c<<endl;
    if(c<9999999)   /// Calcular mientras sea menor a las maximas iteraciones que permite el sistema operativo
    {
        if(impar!=0)
        {
            imparesRecursivo(y,c,impar,suma);
        }
        else
        {
            return suma;
        }
    }
    else
    {
        //cout<<"entre"<<endl;
        a=yey(y);
        cc=impare(impar);
        d=ssum(suma);
    }

}



uint128_t recursivas()
{

    if(f!=0)
    {

        dfg=0;
        dfg=imparesRecursivo(a,b,cc,d);

        f=cc;
        b=0;

        recursivas();
        recovery=recuperacion(dfg);
    }
    else
    {
        return recovery;
    }
}

typedef unsigned __int128 uint128_t;

static int print_u128_u(uint128_t u128)
{
    int rc;
    if (u128 > 18446744073709551615)
    {
        uint128_t leading  = u128 / 10000000000000000000;
        uint64_t  trailing = u128 % 10000000000000000000;
        rc = print_u128_u(leading);
        rc += printf("%.19"  PRIu64, trailing);
    }
    else
    {
        uint64_t u64 = u128;
        rc = printf("%" PRIu64, u64);
    }
    return rc;
}

int main()
{
    unsigned long long int i=0;
    vector<int> Vectorzaso;
    long double num;
    uint128_t recibidos;
    int numero;
    char menu[MAX];
    bool esNumero, esNumero2;

    dfg=0, recovery=0;
    b=0,cc=0,d=0;
    a=-1;
    f=1;

    do
    {
        limpiarPantalla();
        esNumero=true;
        esNumero2=true;
        numero=0;
        recibidos=0;


        cout<<"------------------------------------------------------------------"<<endl;
        cout<<endl<<"1. Ingresar un Numero"<<endl;
        cout<<"2. Salir"<<endl;
        cout<<endl<<"Ingrese su Opcion: ";
        cin.getline(menu,256);
        if(strcmp(menu,"1")!=0 and strcmp(menu,"2")!=0)
        {
            esNumero2=false;

        }

        if(esNumero2)
        {


            switch(menu[0])
            {


            case '1':
            {
                cout<<"------------------------------------------------------------------"<<endl;
                cout<<"Escribe un numero: ";
                system("stty raw");//llamada al sistema para captura de caracter por caracter
                num = 0;
                i=0;

                while (num!=13)
                    //con getchar almacenar lo escrito con el teclado
                {
                    i++;
                    num=getchar();
                    numero=num-48;


                    if(numero>-1 and numero<10)
                    {
                        esNumero=true;
                        Vectorzaso.push_back(numero);
                        recibidos= ((uint128_t) recibidos * 10 + numero);
                    }
                    else if(numero==-35)
                    {
                        esNumero=true;
                    }
                    else
                    {
                        esNumero=false;
                        num=13;
                    }
                    if(Vectorzaso.front()==0)
                    {
                        esNumero=false;
                    }


                }

                system("stty cooked"); // regresnado a la normalidad la llamada de caracteres

                //cout<<"I: "<<num;
                if(esNumero)
                {

                    cout<<endl<<"El numero ingresado tiene: "<<i-1<<" de Digitos!"<<endl;
                    //cout<<"recibidos "<<recibidos<<endl;


                    if(recibidos>0)
                    {
                        //2466 digitos vs 4932
                        vector<int> finall;
                        cout<<endl<<"La suma de los primeros numeros impares es: ";
                        b=0,cc=0,d=0, f=1;
                        a=-1;
                        cc=recibidos;
                        dfg=0, recovery=0;

                        clock_t t_ini, t_fin;
                        double secs;

                        t_ini = clock();



                            print_u128_u(recursivas());
                            cout<<endl;


                        t_fin = clock();

                        secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
                        printf("Tiempo Total en Segundos: %.16g ", secs);

                        //cout<<(unsigned long long int)(recibidos*recibidos);

                        cout<<endl;
                        pausa();

                    }
                }
                else
                {
                    cout<<endl<<"ERROR: Al parecer ingreso un numero NO entero, letra u otro simbolo distinto!"<<endl;
                    cout<<"       o ingreso un numero con 0 inicial (Procure no iniciar con 0)!"<<endl;
                    cout<<"       Solo se pueden Ingresar Numeros Enteros!"<<endl;
                    pausa();
                }
                Vectorzaso.clear();
                break;
            }
            case '2':
            {
                cout<<endl<<"Presione Entrar para Salir...";
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                return 0;
                break;
            }
            default:
                cout<<endl<<"ERROR: la opcion elegida no existe!"<<endl;
                pausa();
                break;
            }
        }
        else
        {
            cout<<endl<<"ERROR: la opcion elegida no existe!"<<endl;
            pausa();
            menu[0]='3';
        }
    }
    while(menu[0]!='2');

    return 0;
}




