/// Libreria genial para manejar enteros hasta 128 bits

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cinttypes>

#define UINT64_MAX 18446744073709551615

using namespace std;


typedef unsigned int uint128_t __attribute__((mode(TI))); /// definimos el tipo de dato en "mode(TI)" lo que genera que podamos operarlo,
                                                            /// ejemplo: hacer sumas, multiplicaciones o comparaciones.

///Algunos compiladores (como GNU-GCC) admiten el atributo mode ( __attribute__((mode(X))) ).
///El atributo mode y los diversos parámetros del modo máquina se pueden utilizar para crear varios tipos de datos.
///Haciendo que en un sistema x86-64, pueden crearse enteros de 128 bits y Decimal-Flotantes.


///La siguiente lista describe todos los posibles parámetros utilizados por el atributo mode:

///BI - 1 Bit
///QI - Número entero de trimestre; 1 byte
///HI - Media entero; 2 bytes
///PSI - Entero parcial parcial; 4 bytes; No todos los bits utilizados
///SI - Entero único; 4 bytes
///PDI - Número entero doble parcial; 8 bytes; No todos los bits utilizados
///DI - Número entero doble; 8 bytes (64 bits)
///TI -Tetra Integer; 16 bytes (128 bits)       - ESTE ES EL QUE NOS INTERSA ;) -
///OI - Octa Entero; 32 bytes (256 bits)
///XI - Hexadeca Entero; 64 bytes (512 bits)
///QF - Cuarto Flotante; 1 byte cuarta precisión flotador
///HF - Media flotante; Punto de flotación de media precisión de 2 bytes
///TQF - Flotante de tres cuartos; Punto de flotación de tres bytes de tres cuartos de precisión
///SF - flotante simple; Punto de flotación de precisión de 4 bytes
///DF - Doble Flotante; Punto de flotación de doble precisión de 8 bytes
///XF - Flotación extendida; Punto flotante de precisión extendida de 12 bytes
///TF - Tetra flotante; Punto de flotador de tetra-precisión de 16 bytes
///SD - Decimal flotante simple; Punto de flotación decimal de 4 bytes (32 bits)
///DD - Flotación Decimal Doble; 8 bytes (64 bits) decimal float-point
///TD - Tetra Decimal Flotante; 4 bytes (128 bits) decimal float-point
///CQI - Complejo Trimestre Entero; 1 byte
///CHI - Complejo medio entero; 2 bytes
///CSI - Complejo entero único; 4 bytes
///CDI - Complejo doble entero; 8 bytes
///CTI - Complejo Tetra Entero; 16 bytes
///COI - Complejo Octa Entero; 32 bytes
///QC - Cuarto Complejo; 1 byte cuarto de precisión complejo punto flotante
///HC - Half Complex; 2 byte, semi-precisión, complejo, punto flotante
///SC - Complejo único; Punto de flotación complejo de precisión de 4 bytes
///DC - Complejo doble; 8 bytes de doble precisión compleja float-point
///XC - Complejo extendido; Punto flotante complejo de precisión extendida de 12 bytes
///TC - Complejo Tetra; 16 byte tetra-precisión compleja float-point
///QQ - Cuarto-Fraccional; Número fraccionario firmado de 1 byte
///HQ - Semi-fraccional; Número fraccionario firmado de 2 bytes
///SQ - Un solo fraccional; Número fraccionario firmado de 4 bytes (32 bits)
///DQ - Doble-fraccional; Número fraccionario firmado de 8 bytes (64 bits)
///TQ - Tetra-Fraccional; Número de fracción firmada de 16 bytes (128 bits)
///UQQ - Unsigned Cuarto-Fraccional; Número fraccionario sin signo de 1 byte
///UHQ - Sin signo medio-fraccional; Número fraccionario sin signo de 2 bytes
///USQ - Unsigned Single-Fractional; Número fraccionario sin signo de 4 bytes (32 bits)
///UDQ - Sin signo doble-fraccional; Número fraccionario sin signo de 8 bytes (64 bits)
///UTQ - Sin señal Tetra-Fraccional; Número fraccionario sin signo de 16 bytes (128 bits)
///HA - Medio acumulador; Acumulador firmado de 2 bytes (16 bits)
///SA - Acumulador único; Acumulador firmado de 4 bytes (32 bits)
///DA - Acumulador doble; 8-byte (64-bit) acumulador firmado
///TA - Tetra-acumulador; 16-byte (128-bit) acumulador firmado
///UHA - Semi-acumulador no firmado; Acumulador sin signo de 2 bytes (16 bits)
///EE.UU. - Unsigned Single-Accumulator; Acumulador sin signo de 4 bytes (32 bits)
///UDA - Unsigned Doble-Acumulador; 8-byte (64-bit) sin signo de acumulador
///UTA - Tetraacumulador no firmado; Acumulador sin signo de 16 bytes (128 bits)
///CC - Código de Condición
///BLK - Bloque
///VOID - Nulo
///P - Modo de dirección
///V4SI - Vector; 4 enteros individuales
///V8QI - Vector; 8 enteros de un solo byte
///BND32 - Puntero de 32 bits vinculado
///BND64 - Puntero de 32 bits vinculado


typedef unsigned __int128 uint128_t;

static int print_u128_u(uint128_t u128)                 ///Metodo especial para imprimir los 128 bits en pantalla
{
    int rc;
    if (u128 > UINT64_MAX)
    {
        uint128_t leading  = u128 / 10000000000000000000;  ///Se divide entre la base del entero de 64 bits (20 digitos)
        uint64_t  trailing = u128 % 10000000000000000000; ///se le saca el modulo para optener el numero individual,
                                                           ///Como cuando haciamos lo del entero para obtener digito por digito XD
        rc = print_u128_u(leading);
        rc += printf("%.19"  PRIu64, trailing);            ///PRIu64 es un especificador de formato, introducido en C99, para imprimir uint64_t,
                                                           /// donde uint64_t es: Tipo entero sin signo con ancho de 64 bits respectivamente
                                                           ///(proporcionado sólo si la implementación soporta directamente el tipo).
    }
    else
    {
        uint64_t u64 = u128;
        rc = printf("%" PRIu64, u64);
    }
    return rc;
}


///Para imprimir en decimal, lo mejor es ver si el valor es mayor que UINT64_MAX (numero maximo alcansable entero en 64 bits);
///Si es así, entonces dividir por la mayor potencia de 10 que es menor que UINT64_MAX,
///imprimir ese número (y es posible que tenga que repetir el proceso una segunda vez).
///Si no lo es, quiere decir que es un entero que puede representarse sin problemas con los 64 bits.


int main ()

{
uint128_t resultado, numero1, numero2;

numero1=99999999999; /// manejaremos el caso de los 11 9's
numero2=99999999999;

resultado=(numero1*numero2);     ///multiplicaremos los 11 9's por si mismos (bueno un cuadrado XD)
                                 ///necesario señalar que pa' que funcione los numeros a manejar sean
                                 ///de variables uint128_t, sino no podra operarlos correctamente

cout<<"Resultado: ";        ///seran 22 digitos (9999999999800000000001)
print_u128_u(resultado); ///mandamos a imprimir los datos procesados de la variable resultados
                         ///con el metodo para imprimir los de 128 bits.
cout<<endl;
return 0;
}
