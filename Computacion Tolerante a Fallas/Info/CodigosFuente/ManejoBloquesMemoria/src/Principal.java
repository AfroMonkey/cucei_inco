import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
/// andrea este es el 7
public class Principal extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DefaultTableModel modeloLoteActual = new DefaultTableModel();
	private DefaultTableModel modeloBloqueados = new DefaultTableModel();
	private DefaultTableModel modeloNuevos = new DefaultTableModel();
	private JTable tablaTerminados = new JTable();
	private JTable tablaBloqueados = new JTable();
	private JTable tablaNuevos = new JTable();
	private JTable tablaLoteActual = new JTable();
	private DefaultTableModel modeloTerminados = new DefaultTableModel();
	private JTextField textFieldContador;
	final JLabel label = new JLabel();
	protected static final JFrame SeleccionarProcesos = null;
	protected static final JFrame SeleccionarQuantum = null;
	private int numProcesos;
	Proceso proceso = new Proceso();
	private JTextField textFieldProcesosPendientes;
	private int duracionTotal;
	private ArrayList<Proceso> listaTotalProcesos = new ArrayList<Proceso>(); //Lista que tiene todos los procesos agregados
	private ArrayList<Proceso> listaBloqueados = new ArrayList<Proceso>(); //lista de elementos bloqueados
	private JTextField textFieldId;
	private JTextField textFieldOpe;
	private JTextField textFieldTme;
	private JTextField textFieldTt;
	private JTextField textFieldTr;
	JButton btnAgregarProcesos = new JButton("Agregar Procesos");
	AdministradorEjecucion administradorEjecucion;
	private int totalProcesos;
	private JTextField textFieldQuantum;
	private int quantum;
	private JTextField textFieldProcesosListos;
	JProgressBar[] progressBar = new JProgressBar[28];
	JLabel[] numMarco = new JLabel[28];
	private JTextField textFieldTamanio;
	
	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception w) {
					w.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		totalProcesos=0;
		setTitle("Algoritmo de planificaci�n RR");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1180, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		//Tabla de Listos
		modeloLoteActual.addColumn("ID");
		modeloLoteActual.addColumn("TME");
		modeloLoteActual.addColumn("TR");
		contentPane.setLayout(null);

		//Tabla de procesos
		tablaLoteActual.setModel(modeloLoteActual);
		JScrollPane scrollLoteActual = new JScrollPane(tablaLoteActual);
		scrollLoteActual.setBounds(25, 54, 247, 157);
		contentPane.add(scrollLoteActual);
		
		//Tabla de procesos bloqueados
		tablaBloqueados.setModel(modeloBloqueados);
		JScrollPane scrollBloqueados = new JScrollPane(tablaBloqueados);
		scrollBloqueados.setBounds(601, 54, 247, 157);
		contentPane.add(scrollBloqueados);
		modeloBloqueados.addColumn("ID");
		modeloBloqueados.addColumn("TT");

		//Tabla de procesos terminados
		tablaTerminados.setModel(modeloTerminados);
		modeloTerminados.addColumn("ID");
		modeloTerminados.addColumn("OPE");
		modeloTerminados.addColumn("Resultado");
		JScrollPane scrollPane_1 = new JScrollPane(tablaTerminados);
		scrollPane_1.setBounds(895, 54, 247, 157);
		contentPane.add(scrollPane_1);
		
		//Tabla de procesos nuevos
		tablaNuevos.setModel(modeloNuevos);
		modeloNuevos.addColumn("ID");
		modeloNuevos.addColumn("Tama�o");
		JScrollPane scrollNuevos = new JScrollPane(tablaNuevos);
		scrollNuevos.setBounds(25, 259, 247, 157);
		contentPane.add(scrollNuevos);

		btnAgregarProcesos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limpiar();
				btnAgregarProcesos.setVisible(false);
				agregarProcesos();
			}
		});
		btnAgregarProcesos.setBounds(972, 435, 192, 25);
		contentPane.add(btnAgregarProcesos);

		JLabel lblContador = new JLabel("Contador:");
		lblContador.setBounds(25, 440, 111, 20);
		contentPane.add(lblContador);

		textFieldContador = new JTextField();
		textFieldContador.setEditable(false);
		textFieldContador.setBounds(97, 440, 82, 19);
		contentPane.add(textFieldContador);
		textFieldContador.setColumns(10);

		JLabel lblLotesPendientes = new JLabel("Procesos Pendientes:");
		lblLotesPendientes.setBounds(303, 10, 159, 15);
		contentPane.add(lblLotesPendientes);

		textFieldProcesosPendientes = new JTextField();
		textFieldProcesosPendientes.setEditable(false);
		textFieldProcesosPendientes.setBounds(437, 7, 86, 19);
		contentPane.add(textFieldProcesosPendientes);
		textFieldProcesosPendientes.setColumns(10);

		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(304, 60, 70, 15);
		contentPane.add(lblId);

		JLabel lblNewLabel = new JLabel("OPE:");
		lblNewLabel.setBounds(304, 87, 70, 15);
		contentPane.add(lblNewLabel);

		JLabel lblTme = new JLabel("TME:");
		lblTme.setBounds(304, 114, 70, 15);
		contentPane.add(lblTme);

		JLabel lblTt = new JLabel("TT:");
		lblTt.setBounds(304, 141, 24, 15);
		contentPane.add(lblTt);

		JLabel lblNewLabel_1 = new JLabel("TR:");
		lblNewLabel_1.setBounds(304, 168, 70, 15);
		contentPane.add(lblNewLabel_1);

		textFieldId = new JTextField();
		textFieldId.setEditable(false);
		textFieldId.setColumns(10);
		textFieldId.setBounds(373, 60, 192, 19);
		contentPane.add(textFieldId);

		textFieldOpe = new JTextField();
		textFieldOpe.setEditable(false);
		textFieldOpe.setColumns(10);
		textFieldOpe.setBounds(373, 87, 192, 19);
		contentPane.add(textFieldOpe);

		textFieldTme = new JTextField();
		textFieldTme.setEditable(false);
		textFieldTme.setColumns(10);
		textFieldTme.setBounds(373, 112, 192, 19);
		contentPane.add(textFieldTme);

		textFieldTt = new JTextField();
		textFieldTt.setEditable(false);
		textFieldTt.setColumns(10);
		textFieldTt.setBounds(373, 139, 192, 19);
		contentPane.add(textFieldTt);

		textFieldTr = new JTextField();
		textFieldTr.setEditable(false);
		textFieldTr.setColumns(10);
		textFieldTr.setBounds(373, 166, 192, 19);
		contentPane.add(textFieldTr);
		
		//Invocamos el m�todo, ahora si funcionara
		contentPane.setFocusable(true);
		
		JLabel lblListos = new JLabel("Procesos Listos");
		lblListos.setBounds(103, 39, 98, 14);
		contentPane.add(lblListos);
		
		JLabel lblProcesoEnEjecucin = new JLabel("Proceso en ejecuci\u00F3n");
		lblProcesoEnEjecucin.setBounds(375, 39, 159, 14);
		contentPane.add(lblProcesoEnEjecucin);
		
		JLabel lblProcesosBloqueados = new JLabel("Procesos bloqueados");
		lblProcesosBloqueados.setBounds(665, 39, 132, 14);
		contentPane.add(lblProcesosBloqueados);
		
		JLabel lblProcesosTerminados = new JLabel("Procesos terminados");
		lblProcesosTerminados.setBounds(960, 39, 124, 14);
		contentPane.add(lblProcesosTerminados);
		
		JLabel lblQuantum = new JLabel("Quantum:");
		lblQuantum.setBounds(581, 10, 70, 14);
		contentPane.add(lblQuantum);
		
		textFieldQuantum = new JTextField();
		textFieldQuantum.setEditable(false);
		textFieldQuantum.setBounds(650, 7, 86, 20);
		contentPane.add(textFieldQuantum);
		textFieldQuantum.setColumns(10);
		
		JLabel lblProcesosPendientes = new JLabel("Procesos Pendientes");
		lblProcesosPendientes.setBounds(97, 243, 124, 14);
		contentPane.add(lblProcesosPendientes);
		
		JLabel lblProcesosListos = new JLabel("Procesos Listos:");
		lblProcesosListos.setBounds(25, 12, 111, 14);
		contentPane.add(lblProcesosListos);
		
		textFieldProcesosListos = new JTextField();
		textFieldProcesosListos.setEditable(false);
		textFieldProcesosListos.setBounds(127, 9, 86, 20);
		contentPane.add(textFieldProcesosListos);
		textFieldProcesosListos.setColumns(10);
		
		JLabel lblMemoria = new JLabel("Memoria");
		lblMemoria.setBounds(642, 243, 61, 14);
		contentPane.add(lblMemoria);
		
		JLabel lblTamao = new JLabel("Tama\u00F1o:");
		lblTamao.setBounds(303, 194, 71, 14);
		contentPane.add(lblTamao);
		
		textFieldTamanio = new JTextField();
		textFieldTamanio.setEditable(false);
		textFieldTamanio.setBounds(373, 191, 192, 20);
		contentPane.add(textFieldTamanio);
		textFieldTamanio.setColumns(10);
		
		generarMemoria(); /// para ver los procesos de memoria
		
		contentPane.addKeyListener(new KeyListener(){
			public void keyTyped(KeyEvent e){
				;
			}
			@SuppressWarnings("deprecation")
			public void keyPressed(KeyEvent w){
				if(w.getKeyCode()==KeyEvent.VK_P){
					administradorEjecucion.suspend();
				}
				if(w.getKeyCode()==KeyEvent.VK_C){
					administradorEjecucion.resume();
				}
				if(w.getKeyCode()==KeyEvent.VK_E){
					administradorEjecucion.setInterrupcion(true);
				}	
				if(w.getKeyCode()==KeyEvent.VK_W){
					administradorEjecucion.setError(true);
				}
				if(w.getKeyCode()==KeyEvent.VK_U){
					Proceso proceso = new Proceso();
					proceso.setId(totalProcesos++);
					proceso.setOperacion();
					proceso.setTme();
					proceso.setResultado(proceso.getOperacion());
					proceso.setTr(proceso.getTme());
					proceso.setTt(0);
					proceso.setTamanio();
					listaTotalProcesos.add(proceso);
					administradorEjecucion.setHayNuevoProceso(true);
				}
				if(w.getKeyCode()==KeyEvent.VK_B){
					administradorEjecucion.setMostrarTabla(true);
				}
				if(w.getKeyCode()==KeyEvent.VK_P){
					administradorEjecucion.suspend();
				}
			}
			public void keyReleased(KeyEvent w){
				;
			}
		});
	}

	public static void delaySegundo(){ 
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	public void agregarProcesos(){
		Seleccionar seleccionar = new Seleccionar((SeleccionarProcesos));
		seleccionar.setVisible(true);
		numProcesos=seleccionar.retorna(); //Guarda el numero de procesos a agregar escrito en la ventana de SeleccionarNumProcesos
		//Ciclo que agrega todos los procesos registrados en una sola lista (listaTotalProcesos)
		duracionTotal=0;
		for(int i=0; i<numProcesos; i++){
			Proceso proceso = new Proceso();
			proceso.setId(totalProcesos++);
			proceso.setOperacion();
			proceso.setTme();
			proceso.setResultado(proceso.getOperacion());
			proceso.setTr(proceso.getTme());
			proceso.setTt(0);
			proceso.setTamanio();
			duracionTotal+=proceso.getTme(); //Voy calculando la duracion (suma de todos los procesos
			listaTotalProcesos.add(proceso); //agrego a la lista
		}	

		administradorEjecucion = new AdministradorEjecucion(listaTotalProcesos,
				textFieldId,  textFieldOpe,  textFieldTme, textFieldTt,  textFieldTr, modeloTerminados,
				btnAgregarProcesos, modeloLoteActual, textFieldProcesosPendientes,
				textFieldContador, tablaLoteActual, duracionTotal, modeloBloqueados, listaBloqueados, textFieldProcesosListos, modeloNuevos, progressBar, numMarco, textFieldTamanio);
		administradorEjecucion.start();
	}

	public void limpiar(){
		if (modeloTerminados.getRowCount() > 0) {
			for (int i = modeloTerminados.getRowCount() - 1; i > -1; i--) {
				modeloTerminados.removeRow(i);
			}
		}
		listaTotalProcesos.clear();
		duracionTotal=0;
	}
	
	public void generarMemoria(){
		int x;
		x=327;
		for(int i=0; i<14; i++){
			numMarco[i*2] = new JLabel();
			numMarco[i*2].setText(Integer.toString(i*2));
			numMarco[i*2].setBounds(x, 260, 46, 14);
			contentPane.add(numMarco[i*2]);
			
			numMarco[(i*2)+1] = new JLabel();
			numMarco[(i*2)+1].setText(Integer.toString((i*2)+1));
			numMarco[(i*2)+1].setBounds(x, 332, 46, 14);
			contentPane.add(numMarco[(i*2)+1]);
			
			progressBar[i*2] = new JProgressBar();
			progressBar[i*2].setBounds(x, 283, 46, 14);
			progressBar[i*2].setMaximum(5);
			progressBar[i*2].setMinimum(0);
			progressBar[i*2].setToolTipText("Libre");
			contentPane.add(progressBar[i*2]);
			
			progressBar[(i*2)+1] = new JProgressBar();
			progressBar[(i*2)+1].setBounds(x,356, 46, 14);
			progressBar[(i*2)+1].setMaximum(5);
			progressBar[(i*2)+1].setMinimum(0);
			progressBar[(i*2)+1].setToolTipText("Libre");
			contentPane.add(progressBar[(i*2)+1]);
			
			x+=55;
		}
	}
}