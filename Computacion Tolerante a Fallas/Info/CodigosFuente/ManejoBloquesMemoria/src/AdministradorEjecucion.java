import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class AdministradorEjecucion extends Thread{
	ArrayList<Proceso> listaTotalProcesos;
	ArrayList<Proceso> listaBloqueados;
	JTextField textFieldId; 
	JTextField textFieldOpe; 
	JTextField textFieldTme;
	JTextField textFieldTt; 
	JTextField textFieldTr;
	JTextField textFieldProcesosListos;
	DefaultTableModel modeloLoteActual;
	DefaultTableModel modeloNuevos;
	JTextField textFieldProcesosPendientes;
	int contadorLocal=0;
	DefaultTableModel modeloTerminados;
	DefaultTableModel modeloBloqueados;
	String[] informacion = new String[3];
	String[] informacionBloqueados = new String[2];
	JButton btnAgregarProcesos;
	boolean pausa;
	boolean continuar;
	boolean interrupcion;
	boolean error;
	boolean pendiente;
	boolean hayNuevoProceso;
	boolean mostrarTabla;
	int j;
	int tTTemporal;
	int memoriaDisponible=130;
	JTextField textFieldContador;
	JTextField textFieldTamanio;
	JTable tablaLoteActual;
	int duracionTotal;
	int contadorGlobal;
	int numProcesosListos; //Variable que indica el numero de procesos que hay en la primera tabla
	ArrayList<Proceso> listaProcesosListos = new ArrayList<Proceso>();
	int tiempoRespuestaAux; //variable que guarda el valor temporal del tiempo de respuesta de un proceso por si se interrumpe
	int marcosOcupados=2;
	int procesosAgregados=0;
	JProgressBar[] progressBar = new JProgressBar[28];
	JLabel[] numMarco = new JLabel[28];
	
	public AdministradorEjecucion(ArrayList<Proceso> listaTotalProcesos,
			JTextField textFieldId, JTextField textFieldOpe, JTextField textFieldTme,
		JTextField textFieldTt, JTextField textFieldTr, DefaultTableModel modeloTerminados, JButton btnAgregarProcesos, 
		DefaultTableModel modeloLoteActual, JTextField textFieldProcesosPendientes,
		JTextField textFieldContador, JTable tablaLoteActual, int duracionTotal, DefaultTableModel modeloBloqueados, 
		ArrayList<Proceso> listaBloqueados, JTextField textFieldProcesosListos,
		DefaultTableModel modeloNuevos, JProgressBar[] progressBar, JLabel[] numMarco, JTextField textFieldTamanio){
		contadorGlobal=0;			
		this.listaTotalProcesos=listaTotalProcesos;
		this.textFieldId=textFieldId;
		this.textFieldOpe=textFieldOpe;
		this.textFieldTme=textFieldTme;
		this.textFieldTt=textFieldTt;
		this.textFieldTr=textFieldTr;
		this.modeloTerminados=modeloTerminados;
		this.btnAgregarProcesos=btnAgregarProcesos;
		this.modeloLoteActual=modeloLoteActual;
		this.textFieldProcesosPendientes=textFieldProcesosPendientes;
		this.textFieldContador=textFieldContador;
		this.tablaLoteActual=tablaLoteActual;
		this.duracionTotal=duracionTotal;
		this.modeloBloqueados=modeloBloqueados;
		this.listaBloqueados=listaBloqueados;
		this.textFieldProcesosListos=textFieldProcesosListos;
		this.modeloNuevos=modeloNuevos;
		this.progressBar=progressBar;
		this.numMarco=numMarco;
		this.textFieldTamanio=textFieldTamanio;
		pausa=false;
		continuar=false;
		interrupcion=false;
		error=false;
	}
	public void run(){
		
		//Se asigna los marcos del sistema operativo
				progressBar[0].setForeground(Color.black);
				progressBar[0].setValue(5);
				progressBar[0].setToolTipText("S.O");
				progressBar[1].setForeground(Color.black);
				progressBar[1].setValue(5);
				progressBar[1].setToolTipText("S.O");
				
		//Se agregan los primeros procesos a la cola de listos
		boolean memoriaLlena=false;
		for(int i=0; i<listaTotalProcesos.size(); i++){
		if(memoriaDisponible>=listaTotalProcesos.get(i).getTamanio() && !memoriaLlena){
		if(listaTotalProcesos.get(i).getTamanio()%5==0){
			memoriaDisponible-=listaTotalProcesos.get(i).getTamanio();
		}
		else{
			memoriaDisponible-=(listaTotalProcesos.get(i).getTamanio()+(5-listaTotalProcesos.get(i).getTamanio()%5));
		}
		String[] informacion={(Integer.toString(listaTotalProcesos.get(i).getId())), Integer.toString(listaTotalProcesos.get(i).getTme()), Integer.toString(listaTotalProcesos.get(i).getTr())};
		modeloLoteActual.addRow(informacion);
		listaProcesosListos.add(listaTotalProcesos.get(i));
		listaTotalProcesos.get(i).setTiempoLlegada(Integer.toString(0));
		listaTotalProcesos.get(i).setEstado("Listo");
		asignarMemoria(listaTotalProcesos.get(i).getTamanio(), listaTotalProcesos.get(i).getId());
		numProcesosListos++;
		procesosAgregados++;
	}
	else{
		memoriaLlena=true;
		String[] informacion={(Integer.toString(listaTotalProcesos.get(i).getId())), Integer.toString(listaTotalProcesos.get(i).getTamanio())};
		modeloNuevos.addRow(informacion);
	}
}
		//Comienza el tiempo
				while(listaProcesosListos.size()>0 || listaBloqueados.size()>0){
					//Se agrega proceso nulo cuando no haya datos
					if(listaProcesosListos.size()==0 && listaBloqueados.size()>0){
						Proceso proceso = new Proceso();
						proceso.setId(1000);
						proceso.setOperacion();
						proceso.setTme();
						proceso.setResultado(proceso.getOperacion());
						proceso.setTr(proceso.getTme());
						proceso.setTt(0);
						listaProcesosListos.add(proceso);
						String[] informacion={"1000", Integer.toString(listaProcesosListos.get(0).getTme()), Integer.toString(listaProcesosListos.get(0).getTr())};
						modeloLoteActual.addRow(informacion);
					}
					else{
						//Cambia de color el proceso que est� trabajando
						for(int i=2; i<28; i++){
							if(progressBar[i].getToolTipText().equals(Integer.toString(listaProcesosListos.get(0).getId()))){
								progressBar[i].setForeground(Color.red);
							}
						}
					}
		
					for(int i=0; i<listaProcesosListos.size(); i++){
						for(int j=0; j<modeloTerminados.getRowCount(); j++){
							if(modeloTerminados.getValueAt(j, 0).equals(Integer.toString(listaProcesosListos.get(i).getId()))){
								listaProcesosListos.remove(i);
							}
						}
					}
					
					textFieldId.setText(Integer.toString(listaProcesosListos.get(0).getId()));
					textFieldOpe.setText(listaProcesosListos.get(0).getOperacion());
					textFieldTme.setText(Integer.toString(listaProcesosListos.get(0).getTme()));
					textFieldTamanio.setText(Integer.toString(listaProcesosListos.get(0).getTamanio()));
					if(!listaProcesosListos.get(0).estaPendiente() && listaProcesosListos.get(0).getId()!=1000 && listaProcesosListos.get(0).isNuevo()){
						listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoRespuesta(Integer.toString(contadorGlobal-Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoLlegada())));
						listaProcesosListos.get(0).setNuevo(false);
					}
					contadorLocal=0;
					while(listaProcesosListos.get(0).getTt()<listaProcesosListos.get(0).getTme()){
						
						for(int i=0; i<listaProcesosListos.size(); i++){
							for(int j=0; j<listaProcesosListos.size(); j++){
								if(listaProcesosListos.get(i).getId()==listaProcesosListos.get(j).getId() && i!=j){
									listaProcesosListos.remove(j);
								}
							}
						}
						
						textFieldProcesosListos.setText(Integer.toString(numProcesosListos));
						textFieldProcesosPendientes.setText(Integer.toString(modeloNuevos.getRowCount()));
		//Cuando se presiona B
		if(mostrarTabla){
		for(int i=0; i<procesosAgregados; i++){
				if(listaTotalProcesos.get(i).getEstado().equals("Listo")){
					listaTotalProcesos.get(i).setTiempoFinalizacion("N/A");
					listaTotalProcesos.get(i).setTiempoRetorno("N/A");
					listaTotalProcesos.get(i).setTiempoServicio(Integer.toString(listaTotalProcesos.get(i).getTt()));
					listaTotalProcesos.get(i).setTiempoEspera(Integer.toString(contadorGlobal-listaTotalProcesos.get(i).getTt()));
								}
					}
					TiemposProcesos tiempos = new TiemposProcesos(listaTotalProcesos);
					tiempos.setVisible(true);
					while(tiempos.isDisplayable()){
						delaySegundo();
					}
					mostrarTabla=false;
					}
		//Cuando se presiona U
		if(hayNuevoProceso){
			if(memoriaDisponible>=listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio() && modeloNuevos.getRowCount()==0){
				if(listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio()%5==0){
					memoriaDisponible-=listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio();
				}
				else{
					memoriaDisponible-=(listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio()+(5-listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio()%5));
				}
				String[] informacion={(Integer.toString(listaTotalProcesos.get(listaTotalProcesos.size()-1).getId())), Integer.toString(listaTotalProcesos.get(listaTotalProcesos.size()-1).getTme()), Integer.toString(listaTotalProcesos.get(listaTotalProcesos.size()-1).getTr())};
				modeloLoteActual.addRow(informacion);
				listaProcesosListos.add(listaTotalProcesos.get(listaTotalProcesos.size()-1));
				listaTotalProcesos.get(listaTotalProcesos.size()-1).setTiempoLlegada(Integer.toString(contadorGlobal));
				listaTotalProcesos.get(listaTotalProcesos.size()-1).setEstado("Listo");
				asignarMemoria(listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio(), listaTotalProcesos.get(listaTotalProcesos.size()-1).getId());
				numProcesosListos++;
				procesosAgregados++;
			}
			else{
				String[] informacion={(Integer.toString(listaTotalProcesos.get(listaTotalProcesos.size()-1).getId())), Integer.toString(listaTotalProcesos.get(listaTotalProcesos.size()-1).getTamanio())};
				modeloNuevos.addRow(informacion);
			}
			hayNuevoProceso=false;
		}
		
		//Se elimina el proceso nulo
		if(listaProcesosListos.get(0).getId()==1000 && listaProcesosListos.size()>1){
			break;
		}
		
		//Se asigna el tiempo que le restaba al proceso que vuelve a la cola de listos
		if(listaProcesosListos.get(0).estaPendiente()){ ///
			contadorLocal=listaProcesosListos.get(0).getTt();
			listaProcesosListos.get(0).setPendiente(false);
			listaProcesosListos.get(0).setTiempoBloqueado(Integer.toString(0));
		}
		
		//Se cuenta el tiempo de los procesos bloqueados
		if(listaBloqueados.size()>0){
			for(int k=0; k<modeloBloqueados.getRowCount(); k++){
				listaBloqueados.get(k).setTiempoBloqueado(Integer.toString(Integer.parseInt(listaBloqueados.get(k).getTiempoBloqueado())+1));
				modeloBloqueados.setValueAt(listaBloqueados.get(k).getTiempoBloqueado(), k, 1);
				if(modeloBloqueados.getValueAt(k, 1).equals("8")){
					
					//Se cambia de color a azul en la memoria en caso de que termine
					for(int i=2; i<28; i++){
						if(progressBar[i].getToolTipText().equals(Integer.toString(listaBloqueados.get(k).getId()))){
							progressBar[i].setForeground(Color.blue);
						}
					}
					
					modeloBloqueados.removeRow(k);
					listaBloqueados.get(k).setTiempoBloqueado(Integer.toString(0));
					listaProcesosListos.add(listaBloqueados.get(k));
					String[] informacion={(Integer.toString(listaBloqueados.get(k).getId())), Integer.toString(listaBloqueados.get(k).getTme()), Integer.toString(listaBloqueados.get(k).getTr())};
					modeloLoteActual.addRow(informacion);
					listaBloqueados.remove(k);
				}
			}
		}		
		//Aumento del tiempo
		listaProcesosListos.get(0).setTr(listaProcesosListos.get(0).getTme()-contadorLocal);
		textFieldTt.setText(Integer.toString(contadorLocal));
		textFieldTr.setText(Integer.toString(listaProcesosListos.get(0).getTr()));
		textFieldContador.setText(Integer.toString(contadorGlobal++));
		delaySegundo();
		contadorLocal++;
		listaProcesosListos.get(0).setTt(contadorLocal);
		listaProcesosListos.get(0).setTr(listaProcesosListos.get(0).getTme()-contadorLocal);
		textFieldTt.setText(Integer.toString(listaProcesosListos.get(0).getTt()));
		textFieldTr.setText(Integer.toString(listaProcesosListos.get(0).getTr()));
		
		//Cambio de proceso
		if( listaProcesosListos.get(0).getTt()<listaProcesosListos.get(0).getTme() && listaProcesosListos.get(0).getId()!=1000){
			listaProcesosListos.get(0).setPendiente(true);
			listaProcesosListos.add(listaProcesosListos.get(0));
			String[] informacion={(Integer.toString(listaProcesosListos.get(0).getId())), Integer.toString(listaProcesosListos.get(0).getTme()), Integer.toString(listaProcesosListos.get(0).getTr())};
			modeloLoteActual.addRow(informacion);
			
			//se cambia de color a azul
			for(int i=2; i<28; i++){
				if(progressBar[i].getToolTipText().equals(Integer.toString(listaProcesosListos.get(0).getId()))){
					progressBar[i].setForeground(Color.blue);
				}
			}
			break;
		}
		if(error){
			break;
		}
		if(interrupcion){
			break;
		}
	}
	//Cuando se presiona W
			if(interrupcion){
			interrupcion=false;
				if(listaProcesosListos.get(0).getId()!=1000){
				for(int i=2; i<28; i++){
					if(progressBar[i].getToolTipText().equals(Integer.toString(listaProcesosListos.get(0).getId()))){
					progressBar[i].setForeground(Color.MAGENTA);
								}
							}
							listaBloqueados.add(listaProcesosListos.get(0));
							listaBloqueados.get(listaBloqueados.size()-1).setPendiente(true);
							informacionBloqueados[0]=Integer.toString(listaBloqueados.get(listaBloqueados.size()-1).getId());
							informacionBloqueados[1]=listaBloqueados.get(listaBloqueados.size()-1).getTiempoBloqueado();
							modeloBloqueados.addRow(informacionBloqueados);
						}
						modeloLoteActual.removeRow(0);
						listaProcesosListos.remove(0);
					}
		//Cuando se presiona E
			else if(error){
				if(listaProcesosListos.get(0).getId()!=1000){
					informacion[0]=Integer.toString(listaProcesosListos.get(0).getId());
					informacion[1]=listaProcesosListos.get(0).getOperacion();
					informacion[2]="Error";
					modeloTerminados.addRow(informacion);
					j=0;
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoFinalizacion(Integer.toString(contadorGlobal));
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoRetorno(Integer.toString(Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoFinalizacion())-Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoLlegada()))); ///);
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoServicio(Integer.toString(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTt()));
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoEspera(Integer.toString(contadorGlobal-listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTt()));
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setEstado("Terminado /Error");
					numProcesosListos--;
					memoriaDisponible+=(listaProcesosListos.get(0).getTamanio()+(5-(listaProcesosListos.get(0).getTamanio()%5)));
					liberarMemoria(listaProcesosListos.get(0).getId());
				}
				error=false;
				modeloLoteActual.removeRow(0);
				listaProcesosListos.remove(0);
			}		
			//Termina su tme sin error
			else{
				if(listaProcesosListos.get(0).getId()!=1000 && listaProcesosListos.get(0).getTt()==listaProcesosListos.get(0).getTme()){
					informacion[0]=Integer.toString(listaProcesosListos.get(0).getId());
					informacion[1]=listaProcesosListos.get(0).getOperacion();
					informacion[2]=Double.toString(listaProcesosListos.get(0).getResultado());
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoFinalizacion(Integer.toString(contadorGlobal));
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoServicio(Integer.toString(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTme()));
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setEstado("Terminado");
					modeloTerminados.addRow(informacion);
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoRetorno(Integer.toString(Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoFinalizacion())-Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoLlegada())));
					listaTotalProcesos.get(listaProcesosListos.get(0).getId()).setTiempoEspera(Integer.toString(Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoRetorno())-Integer.parseInt(listaTotalProcesos.get(listaProcesosListos.get(0).getId()).getTiempoServicio())));
					memoriaDisponible+=(listaProcesosListos.get(0).getTamanio()+(5-(listaProcesosListos.get(0).getTamanio()%5)));
					liberarMemoria(listaProcesosListos.get(0).getId());
					numProcesosListos--;
				}
				modeloLoteActual.removeRow(0);
				listaProcesosListos.remove(0);
			}
			
			//Se verifica si cabe un nuevo proceso en memoria
			if(procesosAgregados<listaTotalProcesos.size()){
				int l=procesosAgregados;
				for(int k=l; k<listaTotalProcesos.size(); k++){
					if(memoriaDisponible>=listaTotalProcesos.get(procesosAgregados).getTamanio()){
						if(listaTotalProcesos.get(procesosAgregados).getTamanio()%5==0){
							memoriaDisponible-=listaTotalProcesos.get(procesosAgregados).getTamanio();
						}
						else{
							memoriaDisponible-=(listaTotalProcesos.get(procesosAgregados).getTamanio()+(5-listaTotalProcesos.get(procesosAgregados).getTamanio()%5));
						}
						String[] informacion={(Integer.toString(listaTotalProcesos.get(procesosAgregados).getId())), Integer.toString(listaTotalProcesos.get(procesosAgregados).getTme()), Integer.toString(listaTotalProcesos.get(procesosAgregados).getTr())};
						modeloLoteActual.addRow(informacion);
						listaProcesosListos.add(listaTotalProcesos.get(procesosAgregados));
						listaTotalProcesos.get(procesosAgregados).setTiempoLlegada(Integer.toString(contadorGlobal));
						listaTotalProcesos.get(procesosAgregados).setEstado("Listo");
						asignarMemoria(listaTotalProcesos.get(procesosAgregados).getTamanio(), listaTotalProcesos.get(procesosAgregados).getId());
						if(modeloNuevos.getRowCount()>0){
							modeloNuevos.removeRow(0);
						}
						numProcesosListos++;
						procesosAgregados++;
					}
					else{
						break;
					}
				}
			}
			System.out.println("Memoria Disponible: "+memoriaDisponible);
				}
				TiemposProcesos tiempos = new TiemposProcesos(listaTotalProcesos);
				tiempos.setVisible(true);
				btnAgregarProcesos.setVisible(true);
			}
	public static void delaySegundo(){ 
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
	
	public void setInterrupcion(boolean e){
		interrupcion=e;
	}
	
	public void setError(boolean w){
		error=w;
	}
	
	public void setHayNuevoProceso(boolean u){
		hayNuevoProceso=u;
	}
	
	public void setMostrarTabla(boolean b){
		mostrarTabla=b;
	}
	public void asignarMemoria(int tamanio, int id){
		int j=0;
		for(int i=2; i<56; i++){
			if(progressBar[j].getToolTipText().equals("Libre")){
				if(tamanio>=5){
					progressBar[j].setValue(5);
					tamanio-=5;
					progressBar[j].setForeground(Color.BLUE);
					progressBar[j].setToolTipText(Integer.toString(id));
				}
				else if(tamanio==0){
					break;
				}
				else{
					progressBar[j].setValue(tamanio);
					progressBar[j].setForeground(Color.BLUE);
					progressBar[j].setToolTipText(Integer.toString(id));
					break;
				}
			}
			j++;
			if(j==28){
				j=0;
			}
		}
	}
	
	public void liberarMemoria(int id){
		int j=0;
		for(int i=2; i<56; i++){
			if(progressBar[j].getToolTipText().equals(Integer.toString(id))){
				progressBar[j].setToolTipText("Libre");
				progressBar[j].setValue(0);
			}
			j++;
			if(j==28){
				j=0;
			}
		}
	}
}
				

