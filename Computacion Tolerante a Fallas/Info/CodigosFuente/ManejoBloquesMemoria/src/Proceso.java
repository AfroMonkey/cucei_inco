
public class Proceso {
	private String operacion, tipoOperacion;
	private String[] operaciones = new String[6];
	private int id, tme, tt, tr;
	private String tiempoBloqueado, tiempoLlegada, tiempoFinalizacion, tiempoRetorno, tiempoRespuesta, tiempoServicio, estado;
	String tiempoEspera;
	private double resultado;
	private int[] operandos = new int[2];
	private boolean bloqueado, pendiente, nuevo;
	private int tamanio;
	
	Proceso(){
		bloqueado=false;
		tiempoBloqueado=Integer.toString(0);
		pendiente=false;
		tiempoEspera="N/A";
		tiempoLlegada="N/A";
		tiempoFinalizacion="N/A";
		tiempoRetorno="N/A";
		tiempoRespuesta="N/A";
		tiempoServicio="N/A";
		estado="Nuevo";
		nuevo=true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion() {
		operaciones[0]="+";
		operaciones[1]="-";
		operaciones[2]="*";
		operaciones[3]="/";
		operaciones[4]="residuo";
		operaciones[5]="potencia";
		tipoOperacion=operaciones[(int) (Math.random()*6)];
		operandos[0]=(int) (Math.random()*100)+1;
		operandos[1]=(int) (Math.random()*100)+1;
		if(tipoOperacion.equals("potencia")){
			operacion="pow("+Integer.toString(operandos[0])+","+Integer.toString(operandos[0])+")";
		}
		else if(tipoOperacion.equals("residuo")){
			operacion=Integer.toString(operandos[0])+"%"+Integer.toString(operandos[0]);
		}
		else{
			operacion=Integer.toString(operandos[0])+tipoOperacion+Integer.toString(operandos[1]);
		}
		
	}
	public int getTme() {
		return tme;
	}
	public void setTme() {
		tme=(int) (Math.random()*20)+1;
		//tme=10;
	}
	public double getResultado() {
		return resultado;
	}
	public void setResultado(String operacion) {
		if(tipoOperacion.equals("+")){
			resultado=operandos[0]+operandos[1];
		}
		if(tipoOperacion.equals("-")){
			resultado=operandos[0]-operandos[1];
		}
		if(tipoOperacion.equals("*")){
			resultado=operandos[0]*operandos[1];
		}
		if(tipoOperacion.equals("/")){
			resultado=operandos[0]/operandos[1];
		}
		if(tipoOperacion.equals("residuo")){
			resultado=operandos[0]%operandos[1];
		}
		if(tipoOperacion.equals("potencia")){
			resultado=Math.pow(operandos[0],operandos[1]);
		}
	}
	public int getTt() {
		return tt;
	}
	public void setTt(int tt) {
		this.tt = tt;
	}
	public int getTr() {
		return tr;
	}
	public void setTr(int tr) {
		this.tr = tr;
	}
	public boolean estaBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(boolean pendiente) {
		this.bloqueado = pendiente;
	}
	public boolean estaPendiente() {
		return pendiente;
	}
	public void setPendiente(boolean pendiente) {
		this.pendiente = pendiente;
	}
	public String getTiempoBloqueado() {
		return tiempoBloqueado;
	}
	public void setTiempoBloqueado(String tiempoBloqueado) {
		this.tiempoBloqueado = tiempoBloqueado;
	}
	public String getTiempoLlegada() {
		return tiempoLlegada;
	}
	public void setTiempoLlegada(String tiempoLlegada) {
		this.tiempoLlegada = tiempoLlegada;
	}
	public String getTiempoFinalizacion() {
		return tiempoFinalizacion;
	}
	public void setTiempoFinalizacion(String tiempoFinalizacion) {
		this.tiempoFinalizacion = tiempoFinalizacion;
	}
	public String getTiempoRetorno() {
		return tiempoRetorno;
	}
	public void setTiempoRetorno(String tiempoRetorno) {
		this.tiempoRetorno = tiempoRetorno;
	}
	public String getTiempoRespuesta() {
		return tiempoRespuesta;
	}
	public void setTiempoRespuesta(String tiempoRespuesta) {
		this.tiempoRespuesta = tiempoRespuesta;
	}
	public String getTiempoEspera() {
		return tiempoEspera;
	}
	public void setTiempoEspera(String tiempoEspera) {
		this.tiempoEspera = tiempoEspera;
	}
	public String getTiempoServicio() {
		return tiempoServicio;
	}
	public void setTiempoServicio(String tiempoServicio) {
		this.tiempoServicio = tiempoServicio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public boolean isNuevo() {
		return nuevo;
	}
	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}
	public int getTamanio() {
		return tamanio;
	}
	public void setTamanio() {
		tamanio=(int) (Math.random()*23)+7;
	}	
	
}
