import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Seleccionar extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private int numProcesos;

	/**
	 * Create the dialog.
	 */
	public Seleccionar(JFrame padre) {
		super(padre, true);
		setBounds(100, 100, 200, 200);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel label = new JLabel("�Cu�ntos procesos");
			label.setBounds(25, 12, 160, 25);
			contentPanel.add(label);
		}
		{
			JLabel label = new JLabel("deseas agregar?");
			label.setBounds(28, 33, 160, 25);
			contentPanel.add(label);
		}
		{
			textField = new JTextField();
			textField.setColumns(10);
			textField.setBounds(37, 88, 114, 19);
			contentPanel.add(textField);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if(esNumerico(textField.getText())){
							numProcesos=Integer.parseInt(textField.getText());
							if(numProcesos<0){
								JOptionPane.showMessageDialog(null, "Introduce un n�mero v�lido");
							}
							else{					
								setVisible(false);
							}
						}
						else{
							JOptionPane.showMessageDialog(null, "Introduce un n�mero v�lido");
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	//Funcion que valida si una cadena es un numero
		public boolean esNumerico(String caracter){
			try{
				Long.parseLong(caracter);
			}catch(Exception e){
				return false;
			}
			return true;
		}
		
		public void cargar(){
			
		}
		
		public int retorna(){
			return numProcesos;
		}

}
