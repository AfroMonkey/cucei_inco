import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class TiemposProcesos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DefaultTableModel modeloTiempos = new DefaultTableModel();
	private JTable tablaTiempos = new JTable();
	private String resultado;
	
	public TiemposProcesos(ArrayList<Proceso> listaTotalProcesos) {
		setTitle("Tiempos de los Procesos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		tablaTiempos.addKeyListener(new KeyListener(){
			public void keyTyped(KeyEvent e){
				;
			}
			public void keyPressed(KeyEvent e){
				if(e.getKeyCode()==KeyEvent.VK_C){
					dispose();
				}
			}
			public void keyReleased(KeyEvent e){
				;
			}
		});
		
		modeloTiempos.addColumn("ID");
		modeloTiempos.addColumn("Estado");
		modeloTiempos.addColumn("Operacion");
		modeloTiempos.addColumn("Resultado");
		modeloTiempos.addColumn("Tiempo Bloqueado");
		modeloTiempos.addColumn("Llegada");
		modeloTiempos.addColumn("Finalizacion");
		modeloTiempos.addColumn("Retorno");
		modeloTiempos.addColumn("Respuesta");
		modeloTiempos.addColumn("Espera");
		modeloTiempos.addColumn("Servicio");
		
		tablaTiempos.setModel(modeloTiempos);
		JScrollPane scrollTiempos = new JScrollPane(tablaTiempos);
		contentPane.add(scrollTiempos);
		

		for(int i=0; i<listaTotalProcesos.size(); i++){
			if(listaTotalProcesos.get(i).getEstado().equals("Terminado /Error")){
				resultado="Error";
			}
			else if(listaTotalProcesos.get(i).getEstado().equals("Nuevo") || listaTotalProcesos.get(i).getEstado().equals("Listo")){
				resultado="Sin calcular";
			}
			else{
				resultado=Double.toString(listaTotalProcesos.get(i).getResultado());
			}
			String[] informacion={Integer.toString(listaTotalProcesos.get(i).getId()), listaTotalProcesos.get(i).getEstado(), 
					listaTotalProcesos.get(i).getOperacion(), resultado,
					listaTotalProcesos.get(i).getTiempoBloqueado(), listaTotalProcesos.get(i).getTiempoLlegada(),
					listaTotalProcesos.get(i).getTiempoFinalizacion(), listaTotalProcesos.get(i).getTiempoRetorno(),
					listaTotalProcesos.get(i).getTiempoRespuesta(), listaTotalProcesos.get(i).getTiempoEspera(),
					listaTotalProcesos.get(i).getTiempoServicio()};
			modeloTiempos.addRow(informacion);
		}
		
	}

}
