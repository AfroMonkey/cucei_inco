#include <iostream>
#include "Circulo.h"

using namespace std;

void imprimeCirculo(Circulo& c);
void modificaCirculo(Circulo& c);
//void modificaCirculo(Circulo c);//que pasa si comentas la anterior y descomentas esta?

int main()
{
    Circulo c1,c2;

    cout << "Propiedades del circulo c1:" << endl;
    imprimeCirculo(c1);
    cout << "Propiedades del circulo c2:" << endl;
    imprimeCirculo(c2);
    cout << "Modificando c2:" << endl;
    modificaCirculo(c2);
    imprimeCirculo(c2);
    return 0;
}

void imprimeCirculo(Circulo& c){//paso de parametro por referencia
    cout << "radio=" << c.dameRadio() << endl;
    cout << "colorBorde=" << c.dameColorBorde() << endl;
    cout << "colorFondo=" << c.dameColorFondo() << endl << endl;
}

//paso de parametros por valor "Circulo c" vs por referencia "Circulo& c"
void modificaCirculo(Circulo& c){
//void modificaCirculo(Circulo c){//que pasa si comentas la anterior y descomentas esta?
    c.fijaRadio(100);
}
