#include <stdio.h>
#include <stdlib.h>

#define MAX_INGRESOS 3

//Variables globales, representan el estado del programa
float ingresos[MAX_INGRESOS];//por defecto se incializan en ceros
int cuentaIngresos;

void capturarIngreso();
void listarIngresos();
void pausar();

int main()
{
    listarIngresos();
    capturarIngreso();
    listarIngresos();
    capturarIngreso();
    listarIngresos();
    capturarIngreso();
    listarIngresos();
    return 0;
}

void capturarIngreso(){
    printf("Dame un ingreso mas: ");
    scanf("%f",&ingresos[cuentaIngresos]);
    cuentaIngresos++;
    getchar();
    pausar();
    printf("\n");
}

void listarIngresos(){
    printf("ingresos[0]=%f\n",ingresos[0]);
    printf("ingresos[1]=%f\n",ingresos[1]);
    printf("ingresos[2]=%f\n",ingresos[2]);
    pausar();
}

void pausar(){
    printf("Presione entrar para continuar . . .");
    getchar();
}

