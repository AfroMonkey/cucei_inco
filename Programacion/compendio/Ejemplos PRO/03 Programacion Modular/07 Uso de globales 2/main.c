#include <stdio.h>
#include <stdlib.h>

#define MAX_INGRESOS 3

float ingresos[MAX_INGRESOS];
int cuentaIngresos; //no requere inicializacion pues toda global se inicia en cero

void capturarIngreso();
void listarIngresos();
void pausar();

int main()
{
    listarIngresos();
    capturarIngreso();
    listarIngresos();
    capturarIngreso();
    listarIngresos();
    capturarIngreso();
    listarIngresos();
    return 0;
}

void capturarIngreso(){
    printf("Dame un ingreso mas: ");
    scanf("%f",&ingresos[cuentaIngresos++]);
    getchar();
    pausar();
    printf("\n");
}

void listarIngresos(){
    int i=0; //contador
    printf("Total de ingresos capturados: %d\n",cuentaIngresos);
    printf("ingresos[%d]=%f\n",i,ingresos[i]);
    i++;
    printf("ingresos[%d]=%f\n",i,ingresos[i]);
    i++;
    printf("ingresos[%d]=%f\n",i,ingresos[i]);
    i++;//esta ultima instruccion servira para un ejemplo posterior
    pausar();
}

void pausar(){
    printf("Presione entrar para continuar . . .");
    getchar();
}
