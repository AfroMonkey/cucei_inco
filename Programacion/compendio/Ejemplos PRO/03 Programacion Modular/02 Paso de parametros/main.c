#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define TOTAL_PRACTICAS 7

int practicas[TOTAL_PRACTICAS];//variable global

void imprimirTitulo(char* titulo){//titulo es un parametro
    system(CLEAR);
    printf("**********ADMINISTRACION DE PRACTICAS**********\n");
    printf("%s\n",titulo);
}

void capturarPracticas(){//esta es una subrutina
    int calificacion;//variable local
    imprimirTitulo("CAPTURA DE PRACTICAS");//la cadena aqui es un argumento
    printf("Dame la calificacion de la practica 1: ");
    scanf("%d",&calificacion);
    practicas[0]=calificacion;
    printf("Dame la calificacion de la practica 2: ");
    scanf("%d",&calificacion);
    practicas[1]=calificacion;
    printf("Dame la calificacion de la practica 3: ");
    scanf("%d",&calificacion);
    practicas[2]=calificacion;
    printf("Dame la calificacion de la practica 4: ");
    scanf("%d",&calificacion);
    practicas[3]=calificacion;
    printf("Dame la calificacion de la practica 5: ");
    scanf("%d",&calificacion);
    practicas[4]=calificacion;
    printf("Dame la calificacion de la practica 6: ");
    scanf("%d",&calificacion);
    practicas[5]=calificacion;
    printf("Dame la calificacion de la practica 7: ");
    scanf("%d",&calificacion);
    practicas[6]=calificacion;
    getchar();
    printf("Presione entrar para continuar . . .");
    getchar();
}

void listarPracticas(){
    imprimirTitulo("LISTADO DE PRACTICAS");
    printf("practica 1 = %d\n",practicas[0]);
    printf("practica 2 = %d\n",practicas[1]);
    printf("practica 3 = %d\n",practicas[2]);
    printf("practica 4 = %d\n",practicas[3]);
    printf("practica 5 = %d\n",practicas[4]);
    printf("practica 6 = %d\n",practicas[5]);
    printf("practica 7 = %d\n",practicas[6]);
    printf("Presione entrar para continuar . . .");
    getchar();
}

int main()
{
    capturarPracticas();
    listarPracticas();
    return 0;
}
