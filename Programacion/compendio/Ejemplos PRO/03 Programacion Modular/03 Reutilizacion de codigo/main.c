#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define TOTAL_PRACTICAS 7

int practicas[TOTAL_PRACTICAS];//variable global

void imprimirTitulo(char* titulo){
    system(CLEAR);
    printf("**********ADMINISTRACION DE PRACTICAS**********\n");
    printf("%s\n",titulo);
}

void capturarPractica(int numPractica){//numPractica es un parametro
    int calificacion;//variable local
    printf("Dame la calificacion de la practica %d: ",numPractica);
    scanf("%d",&calificacion);
    practicas[numPractica-1]=calificacion;
    getchar();
}

void pausar(){
    printf("Presione entrar para continuar . . .");
    getchar();
}

void capturarPracticas(){
    imprimirTitulo("CAPTURA DE PRACTICAS");
    capturarPractica(1);//el 1 es un argumento
    capturarPractica(2);
    capturarPractica(3);
    capturarPractica(4);
    capturarPractica(5);
    capturarPractica(6);
    capturarPractica(7);
    pausar();
}

void listarPracticas(){
    imprimirTitulo("LISTADO DE PRACTICAS");
    printf("practica 1 = %d\n",practicas[0]);
    printf("practica 2 = %d\n",practicas[1]);
    printf("practica 3 = %d\n",practicas[2]);
    printf("practica 4 = %d\n",practicas[3]);
    printf("practica 5 = %d\n",practicas[4]);
    printf("practica 6 = %d\n",practicas[5]);
    printf("practica 7 = %d\n",practicas[6]);
    pausar();
}

int main()
{
    capturarPracticas();
    listarPracticas();
    return 0;
}
