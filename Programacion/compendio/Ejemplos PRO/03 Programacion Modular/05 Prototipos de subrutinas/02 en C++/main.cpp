#include <iostream>
#include <cstdlib>

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define TOTAL_PRACTICAS 7

using namespace std;

int practicas[TOTAL_PRACTICAS];//variable global

//Prototipos
void imprimirTitulo(string titulo);
void capturarPractica(int numPractica);
void capturarPracticas();
void listarPracticas();
void pausar();

int main()
{
    capturarPracticas();
    listarPracticas();
    return 0;
}

void imprimirTitulo(string titulo){
    system(CLEAR);
    cout << "**********ADMINISTRACION DE PRACTICAS**********" << endl;
    cout << titulo << endl;
}

void capturarPractica(int numPractica){
    int calificacion;//variable local
    cout << "Dame la calificacion de la practica " << numPractica << ": ";
    cin >> calificacion;
    practicas[numPractica-1]=calificacion;
    cin.ignore();
}

void capturarPracticas(){
    imprimirTitulo("CAPTURA DE PRACTICAS");
    capturarPractica(1);
    capturarPractica(2);
    capturarPractica(3);
    capturarPractica(4);
    capturarPractica(5);
    capturarPractica(6);
    capturarPractica(7);
    pausar();
}

void listarPracticas(){
    imprimirTitulo("LISTADO DE PRACTICAS");
    cout << "practica 1 = " << practicas[0] << endl;
    cout << "practica 2 = " << practicas[1] << endl;
    cout << "practica 3 = " << practicas[2] << endl;
    cout << "practica 4 = " << practicas[3] << endl;
    cout << "practica 5 = " << practicas[4] << endl;
    cout << "practica 6 = " << practicas[5] << endl;
    cout << "practica 7 = " << practicas[6] << endl;
    pausar();
}

void pausar(){
    cout << "Presione entrar para continuar . . .";
    cin.get();
}
