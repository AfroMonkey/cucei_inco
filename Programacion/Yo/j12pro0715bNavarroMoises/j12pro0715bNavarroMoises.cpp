/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: J12
 * Nombre de la practica: J12
 * Fecha: 2015/09/08
 * Versión 1
 * Tiempo: 00:04
 */
#include <iostream>

#define MAXIMO_ASISTENCIAS 34

using namespace std;

int main() {
	int faltas;
	float porcentajeDeAsistencias;

	cout << "Faltas >";
	cin >> faltas;

	porcentajeDeAsistencias = (float)(MAXIMO_ASISTENCIAS - faltas) / MAXIMO_ASISTENCIAS * 100;
	cout << "Porcentaje de asistencias = " << porcentajeDeAsistencias << "\x25" << endl;

	return 0;
}