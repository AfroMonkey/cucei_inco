/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J20
 * Nombre de practica: J20
 * Fecha: 2015/10/16
 * Version 1
 * Tiempo 00:07
 */

#include "stdio.h"

#define SALARIO_NVL_1 18000
#define INCREMENTO_NVL_1 12
#define SALARIO_NVL_2 30000
#define INCREMENTO_NVL_2 8
#define SALARIO_NVL_3 50000
#define INCREMENTO_NVL_3 7

int main() {
    float salario;
    printf("Salario>");
    scanf("%f", &salario);
    if(salario < SALARIO_NVL_1) {
        salario += salario * INCREMENTO_NVL_1 / 100;
    } else if(salario <= SALARIO_NVL_2) {
        salario += salario * INCREMENTO_NVL_2 / 100;
    } else if(salario <= SALARIO_NVL_3) {
        salario += salario * INCREMENTO_NVL_3 / 100;
    }
    printf("Salario: %.2f\n", salario);
    return 0;
}
