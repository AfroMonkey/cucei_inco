/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J18
 * Nombre de practica: J18
 * Fecha: 2015/10/14
 * Version 1
 * Tiempo: 00:03
 */

#include "stdio.h"

#define PORC_DESCUENTO 20.0

int main() {
    float importe;
    printf("Importe>");
    scanf("%f", &importe);
    if(importe >= 50.0) {
        importe -= importe * PORC_DESCUENTO / 100;
    }
    printf("Total a pagar: %.2f\n", importe);
    return 0;
}
