/*NOTA: Los comentarios no contienen acentos ya que pueden provocar conflictos, y al no ser impresos
 * en la terminal no coloque su codigo hexadecimal
 */
/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: A4
 * Nombre de practica: 
 * Fecha: 2015/10/01
 * Version 1
 * Tiempo: 01:00
 */

#include "stdio.h"
#include "stdlib.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define CARACTERES_NOMBRE_MATERIA 25
#define CARACTERES_ACRONIMO_MATERIA 5

//Los porcentajes a considerar para la evaluacion del curso
#define PORCENTAJE_TAREAS (float) 7 / 100
#define PORCENTAJE_ACTIVIDADES (float) 63 / 100
#define PORCENTAJE_EXAMENES_PARCIALES (float) 30 / 100
//Declaracion y definicion de constantes
#define TOTAL_ASISTENCIAS 34
#define RETARDOS_POR_FALTA 3
#define TOTAL_TAREAS 10
#define TOTAL_ACTIVIDADES 9
#define TOTAL_EXAMENES_PARCIALES 3

typedef struct {
    //Declaracion y definicion por defecto de variables (a usar para entrada de datos) 
    char nombre[CARACTERES_NOMBRE_MATERIA + 1];
    char acronimo[CARACTERES_ACRONIMO_MATERIA + 1];
    char diasSemana[7];
    int horaInicio;
    int horaFinalizacion;

    int cantidadFaltas, cantidadRetardos, cantidadTareasRealizadas;
    int calificacionesActividades[TOTAL_ACTIVIDADES];
    int calificacionesExamenesParciales[TOTAL_EXAMENES_PARCIALES];
}Materia;

float puntosFinalesTareas, puntosFinalesActividades, puntosFinalesExamenes;
float calificacionFinal, asistenciasTotales, porcentajeAsistencia;
Materia materia;

void pausar();
void capturarMateria();
void capturarAsistencias();
void capturarTareas();
void capturarActividad(int actividad);
void capturarActividades();
void capturarExamen(int examen);
void capturarExamenes();
void computarAsistencias();
void computarPuntosTareas();
void computarPuntosActividades();
void computarPuntosExamenes();
void computarCalificacionFinal();
void mostrarMateria();
void mostrarAsistencias();
void mostrarTareas();
void mostrarActividad(int actividad);
void mostrarActividades();
void mostrarExamen(int examen);
void mostrarExamenes();
void mostrarPuntosTareas();
void mostrarPuntosActividades();
void mostrarPuntosExamenes();
void mostrarPorcentajeAsistencia();
void mostrarCalificacionFinal();

int main() {
    printf("Evaluador del Curso de Programaci%cn v1.0\n\n", 0xA2);
    //Capturar datos
    capturarMateria();
    capturarAsistencias();
    capturarTareas();
    capturarActividades();
    capturarExamenes();

    //Computar datos
    computarPuntosTareas();
    computarPuntosActividades();
    computarPuntosExamenes();
    computarAsistencias();
    computarCalificacionFinal();
    
    system(CLEAR);
    printf("Evaluador del Curso de Programaci%cn v1.0\n\n", 0xA2);
    //Mostrar datos
    mostrarMateria();
    mostrarPuntosTareas();
    mostrarPuntosActividades();
    mostrarPuntosExamenes();
    mostrarPorcentajeAsistencia();
    mostrarCalificacionFinal();
    pausar();
    return 0;
}

void pausar() {
    printf("Presione entrar para terminar . . .\n");
    getchar();
}

void capturarMateria() {
    printf("Nombre de la materia >");
    scanf("%s", materia.nombre);
    printf("Acr%cnimo de la materia >", 0xA2);
    scanf("%s", materia.acronimo);
    printf("D%cas de la semana >", 0xA1);
    scanf("%s", materia.diasSemana);
    printf("Hora de inicio >");
    scanf("%d", &materia.horaInicio);
    printf("Hora de finalizaci%cn >", 0xA2);
    scanf("%d", &materia.horaFinalizacion);
    getchar();
    pausar();
    system(CLEAR);
}

void capturarAsistencias() {
    printf("Cu%cntas faltas acumulaste? ", 0xA0);
    scanf("%d", &materia.cantidadFaltas);
    printf("Cu%cntos retardos acumulaste? ", 0xA0);
    scanf("%d", &materia.cantidadRetardos);
    getchar();
}

void capturarTareas() {
    printf("Cu%cntas tareas realizaste? ", 0xA0);
    scanf("%d", &materia.cantidadTareasRealizadas);
    getchar();
}

void capturarActividad(int actividad) {
    printf("Cu%cnto obtuviste en la actividad #%d? ", 0xA0, actividad);
    scanf("%d", &materia.calificacionesActividades[actividad - 1]);
    getchar();
}

void capturarActividades() {
    capturarActividad(1);
    capturarActividad(2);
    capturarActividad(3);
    capturarActividad(4);
    capturarActividad(5);
    capturarActividad(6);
    capturarActividad(7);
    capturarActividad(8);
}

void capturarExamen(int examen) {
    printf("Cu%cnto obtuviste en el examen parcial #%d? ", 0xA0, examen);
    scanf("%d", &materia.calificacionesExamenesParciales[examen - 1]);
    getchar();
}

void capturarExamenes() {
    capturarExamen(1);
    capturarExamen(2);
    capturarExamen(3);
}

void computarAsistencias() {
    asistenciasTotales = TOTAL_ASISTENCIAS;
    asistenciasTotales -= materia.cantidadFaltas;
    asistenciasTotales -= (float)materia.cantidadRetardos / 3;
    porcentajeAsistencia = asistenciasTotales / TOTAL_ASISTENCIAS * 100;
}

void computarPuntosTareas() {
    puntosFinalesTareas = (float)materia.cantidadTareasRealizadas * 100 / TOTAL_TAREAS;
    puntosFinalesTareas *= PORCENTAJE_TAREAS;
}

void computarPuntosActividades() {
    puntosFinalesActividades = 0;
    puntosFinalesActividades += materia.calificacionesActividades[0];
    puntosFinalesActividades += materia.calificacionesActividades[1];
    puntosFinalesActividades += materia.calificacionesActividades[2];
    puntosFinalesActividades += materia.calificacionesActividades[3];
    puntosFinalesActividades += materia.calificacionesActividades[4];
    puntosFinalesActividades += materia.calificacionesActividades[5];
    puntosFinalesActividades += materia.calificacionesActividades[6];
    puntosFinalesActividades += materia.calificacionesActividades[7];
    puntosFinalesActividades += materia.calificacionesActividades[8];
    puntosFinalesActividades /= TOTAL_ACTIVIDADES;
    puntosFinalesActividades *= PORCENTAJE_ACTIVIDADES;
}

void computarPuntosExamenes() {
    puntosFinalesExamenes = 0;
    puntosFinalesExamenes += materia.calificacionesExamenesParciales[0];
    puntosFinalesExamenes += materia.calificacionesExamenesParciales[1];
    puntosFinalesExamenes += materia.calificacionesExamenesParciales[2];
    puntosFinalesExamenes /= TOTAL_EXAMENES_PARCIALES;
    puntosFinalesExamenes *= PORCENTAJE_EXAMENES_PARCIALES;
}

void computarCalificacionFinal() {
    calificacionFinal = puntosFinalesTareas + puntosFinalesActividades + puntosFinalesExamenes;
}

void mostrarMateria() {
    printf("Nombre de la materia= %s\n", materia.nombre);
    printf("Acr%cnimo de la materia= %s\n", 0xA2, materia.acronimo);
    printf("D%cas de la semana= %s\n", 0xA1, materia.diasSemana);
    printf("Hora de inicio= %d\n", materia.horaInicio);
    printf("Hora de finalizaci%cn= %d\n", 0xA2, materia.horaFinalizacion);
}

void mostrarAsistencias() {
    printf("Faltas acumuladas= %d\n", materia.cantidadFaltas);
    printf("Retardos acumulado= %d\n", materia.cantidadRetardos);
}

void mostrarTareas() {
    printf("Tareas realizadas= %d\n", materia.cantidadTareasRealizadas);
}

void mostrarActividad(int actividad) {
    printf("Calificaci%cn obtenida en la actividad #%d=%d\n", 0xA2, 
        actividad, materia.calificacionesActividades[actividad - 1]);
}

void mostrarActividades() {
    mostrarActividad(1);
    mostrarActividad(2);
    mostrarActividad(3);
    mostrarActividad(4);
    mostrarActividad(5);
    mostrarActividad(6);
    mostrarActividad(7);
    mostrarActividad(8);
}

void mostrarExamen(int examen) {
    printf("Calificaci%cn obtenida en el examen #%d=%d\n", 0xA2, 
        examen, materia.calificacionesExamenesParciales[examen - 1]);
}

void mostrarExamenes() {
    mostrarExamen(1);
    mostrarExamen(2);
    mostrarExamen(3);
}

void mostrarPuntosTareas() {
    printf("Tareas= %.2f\n", puntosFinalesTareas);
}

void mostrarPuntosActividades() {
    printf("Actividades= %.2f\n", puntosFinalesActividades);
}

void mostrarPuntosExamenes() {
    printf("Ex%cmenes= %.2f\n", 0xA0, puntosFinalesExamenes);
}

void mostrarPorcentajeAsistencia() {
    printf("Total de asistencias= %.2f\n", asistenciasTotales);
    printf("Porcentaje de asistencias= %.2f\n", porcentajeAsistencia);
}

void mostrarCalificacionFinal() {
    printf("Calificaci%cn Final= %.2f\n", 0xA2, calificacionFinal);
}
