#include <iostream>

using namespace std;

class Note {
private:
    string description;
public:
    Note();
    ~Note();
    void setDescription(string description);
    string getDescription();
};

Note::Note() {
    description = "Staff";
}

Note::~Note() {
}

void Note::setDescription(string description) {
    this->description = description;
}

string Note::getDescription() {
    return description;
}
