/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: 
 * Nombre de practica: 
 * Fecha: 
 * Version 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdlib>
#include "administradorNotas.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

using namespace std;
//------------------------------------------------------------------------------------------------//
NotesManager notesManager;
//------------------------------------------------------------------------------------------------//
void pause();
void manageNotes();
void addNote();
void modiffyNote();
void showDetailsOfNote();
void listNotes();
void removeNote();

int main() {
    int opc;
    do {
        cout << "***Asistente Escolar***" << endl;
        cout << "Menú principal: " << endl;
        cout << "1. Administrar Notas" << endl;
        cout << "2. Administrar Materias" << endl;
        cout << "3. Administrar Tareas" << endl;
        cout << "4. Administrar Recordatorios" << endl;
        cout << "0. Salir" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();
        system(CLEAR);
        switch(opc) {
            case 1: {
                manageNotes();
                break;    
            }
            case 2: {
                //TODO
                break;    
            }
            case 3: {
                //TODO
                break;    
            }
            case 4: {
                //TODO
                break;    
            }
            case 0: {
                cout << "Cerrando . . ." << endl;
                break;    
            }
            default: {
                cout << "Opción invalida" << endl;
                pause();
                break;    
            }
        }
    } while(opc != 0);
    return 0;
}
//------------------------------------------------------------------------------------------------//
void pause() {
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
}
//------------------------------------------------------------------------------------------------//
void manageNotes() {
    int opc;
    do {
        cout << "**Administrar Notas**" << endl;
        cout << "1. Agregar nota" << endl;
        cout << "2. Modificar nota" << endl;
        cout << "3. Mostrar detalles de una nota" << endl;
        cout << "4. Listar notas" << endl;
        cout << "5. Eliminar nota" << endl;
        cout << "0. Regresar al menú anterior" << endl;
        cin >> opc;
        cin.ignore();
        system(CLEAR);
        switch(opc) {
            case 1: {
                addNote();
                break;
            }
            case 2: {
                modiffyNote();
                break;
            }
            case 3: {
                showDetailsOfNote();
                break;
            }
            case 4: {
                listNotes();
                break;
            }
            case 5: {
                removeNote();
                break;
            }
            case 0: {
                //Regresar al menú anterior
                break;
            }
            default: {
                cout << "Opción invalida" << endl;
                pause();
                break;    
            }
        }
    } while(opc != 0);
}
//------------------------------------------------------------------------------------------------//
void addNote() {
    string description;
    cout << "*Agregar nota*" << endl;
    cout << "Descripción>";
    getline(cin, description);
    try {
        Note note;
        note.setDescription(description);
        notesManager.add(note);
        cout << "Nota agregada con éxito" << endl;
    } catch(ListException &e) {
        e.what();
    }
    pause();
    system(CLEAR);
}

void modiffyNote() {
    int position;
    cout << "*Modificar nota*" << endl;
    cout << "Número de nota>";
    cin >> position;
    cin.ignore();
    position--;
    try {
        string description;
        Note note = notesManager.get(position);
        cout << "Vieja descripción: "<< note.getDescription() << endl;
        cout << "Nueva descripción>";
        getline(cin, description);
        notesManager.modiffy(position, note);
        cout << "Nota modificada con éxito" << endl;
    } catch(ListException &e) {
        e.what();
    }
    pause();
    system(CLEAR);
}

void showDetailsOfNote() {
    int position;
    cout << "*Mostrar detalles de una nota*" << endl;
    cout << "Número de nota>";
    cin >> position;
    cin.ignore();
    position--;
    try {
        cout << "Descripción: " << notesManager.get(position).getDescription() << endl;
    } catch(ListException &e) {
        e.what();
    }
    pause();
    system(CLEAR);
}

void listNotes() {
    try {
        //TODO Queue
    } catch(ListException &e) {
        e.what();
    }
    pause();
    system(CLEAR);
}

void removeNote() {

}
