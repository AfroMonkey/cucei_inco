#include "nota.h"
#include "staticList.h"

class NotesManager {
private:
    StaticList<Note> notes;
public:
    NotesManager();
    NotesManager(int max);
    ~NotesManager();
    void add(Note note);
    void modiffy(int position, Note note);
    Note get(int position);
    StaticList<Note> getList();
    void remove(int position);
};

NotesManager::NotesManager(){
}

NotesManager::NotesManager(int max){
    notes = StaticList<Note>(max);
}

NotesManager::~NotesManager(){
}

void NotesManager::add(Note note) {
    try {
        notes.insert(notes.last() + 1, note);
    } catch(ListException &e) {
        throw e;
    }
}

void NotesManager::modiffy(int position, Note note) {
    try {
        notes.remove(position);
        notes.insert(position, note);
    } catch(ListException &e) {
        throw e;
    }
}

Note NotesManager::get(int position) {
    try {
        return notes.get(position);
    } catch(ListException &e) {
        throw e;
    }
}

StaticList<Note> NotesManager::getList() {
    return notes;
}

void NotesManager::remove(int position) {
    try {
        notes.remove(position);
    } catch(ListException &e) {
        throw e;
    }
}
