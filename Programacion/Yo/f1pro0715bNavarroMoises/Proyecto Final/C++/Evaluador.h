#ifndef EVALUADOR_INCLUDED
#define EVALUADOR_INCLUDED

#include "constantes.h"

class Evaluador {
    int cantidadTareasRealizadas;
    int calificacionesActividades[CANTIDAD_ACTIVIDADES];
    int calificacionesExamenes[CANTIDAD_EXAMENES];
    float puntosFinalesTareas;
    float puntosFinalesActividades;
    float puntosFinalesExamenes;
    float calificacionFinal;
public:
    Evaluador() {
        int i;

        cantidadTareasRealizadas = -1;
        for(i = 0; i < CANTIDAD_ACTIVIDADES; i++) {
            calificacionesActividades[i] = -1;
        }
        for(i = 0; i < CANTIDAD_EXAMENES; i++) {
            calificacionesExamenes[i] = -1;
        }
        puntosFinalesTareas = -1;
        puntosFinalesActividades = -1;
        puntosFinalesExamenes = -1;
        calificacionFinal = -1;
    }

    bool fijaCantidadTareasRealizadas(int cantidadTareasRealizadas) {
        if(cantidadTareasRealizadas >= 0 && cantidadTareasRealizadas <= CANTIDAD_TAREAS) {
            this->cantidadTareasRealizadas = cantidadTareasRealizadas;
            return true;
        } else {
            return false;
        }
    }

    int dameCantidadTareasRealizadas() {
        return cantidadTareasRealizadas;
    }

    bool fijaCalificacionActividad(int i, int calificacion) {
        if(calificacion >= 0 && calificacion <= 100) {
            if(i >= 0 && i < CANTIDAD_ACTIVIDADES) {
                calificacionesActividades[i] = calificacion;
            }
            return true;
        } else {
            return false;
        }
    }

    int dameCalificacionActividad(int i) {
        if(i >= 0 && i < CANTIDAD_ACTIVIDADES) {
            return calificacionesActividades[i];
        } else {
            return -1;
        }
    }

    bool fijaCalificacionExamen(int i, int calificacion) {
        if(calificacion >= 0 && calificacion <= 100) {
            if(i >= 0 && i < CANTIDAD_EXAMENES) {
                calificacionesExamenes[i] = calificacion;
            }
            return true;
        } else {
            return false;
        }
    }

    int dameCalificacionExamen(int i) {
        if(i >= 0 && i < CANTIDAD_EXAMENES) {
            return calificacionesExamenes[i];
        } else {
            return -1;
        }
    }

    float damePuntosFinalesTareas() {
        puntosFinalesTareas = cantidadTareasRealizadas * 100.0 / CANTIDAD_TAREAS;
        puntosFinalesTareas *= PORCENTAJE_TAREAS / 100.0;

        return puntosFinalesTareas;
    }

    float damePuntosFinalesActividades() {
        int i;

        for(puntosFinalesActividades = i = 0; i < CANTIDAD_ACTIVIDADES; i++) {
            puntosFinalesActividades += calificacionesActividades[i];
        }
        puntosFinalesActividades /= CANTIDAD_ACTIVIDADES;
        puntosFinalesActividades *= PORCENTAJE_ACTIVIDADES / 100.0;

        return puntosFinalesActividades;
    }

    float damePuntosFinalesExamenes() {
        int i;

        for(puntosFinalesExamenes = i = 0; i < CANTIDAD_EXAMENES; i++) {
            puntosFinalesExamenes += calificacionesExamenes[i];
        }
        puntosFinalesExamenes /= CANTIDAD_EXAMENES;
        puntosFinalesExamenes *= PORCENTAJE_EXAMENES / 100.0;

        return puntosFinalesExamenes;
    }

    float dameCalificacionFinal() {
        calificacionFinal = puntosFinalesTareas + puntosFinalesActividades + puntosFinalesExamenes;

        return calificacionFinal;
    }

    bool tieneCalificacionAprobatoria() {
        return (calificacionFinal >= CALIFICACION_MINIMA_APROBATORIA && calificacionFinal <= 
          CALIFICACION_MAXIMA);
    }
};

#endif
