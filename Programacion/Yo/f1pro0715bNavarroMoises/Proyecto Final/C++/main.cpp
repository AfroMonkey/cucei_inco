/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: F1
 * Nombre de practica: Proyeto Final
 * Fecha: 2015/11/30
 * Version: 1
 * Tiempo: 00:07
 */

#include <iostream>
#include <cstdlib>

#include "constantes.h"
#include "utilidades.h"
#include "Materia.h"
#include "ControlAsistencia.h"
#include "Evaluador.h"
#include "Tarea.h"
#include "Recordatorio.h"
#include "Nota.h"

using namespace std;

void pausarLimpiar();
void capturarMateria(Materia &materia);
void capturarAsistencias(ControlAsistencia &controlAsistencia);
void capturarEvaluandos(Evaluador &evaluador);
void capturarTareas(Evaluador &evaluador);
void capturarActividades(Evaluador &evaluador);
void capturarExamenes(Evaluador &evaluador);
void computarEImprimir(ControlAsistencia &controlAsistencia, Evaluador &evaluador);
void capturarTarea(Tarea &tarea);
void imprimirTarea(Tarea &tarea);
void capturarRecordatorio(Recordatorio &recordatorio);
void imprimirRecordatorio(Recordatorio &recordatorio);
void capturarNota(Nota &nota);
void imprimirNota(Nota &nota);
void evaluarCurso();
void gestionMateria();
void gestionTareas();
void gestionAgenda();
void gestionNotas();

int main() {
    int opc;
    bool repetirMenu;

    repetirMenu = true;

    do{
        cout << "1. Evaluaci" << (char)0xA2 << "n de un Curso" << endl;
        cout << "2. Gesti" << (char)0xA2 << "n de Materias" << endl;
        cout << "3. Gesti" << (char)0xA2 << "n de Tareas" << endl;
        cout << "4. Agenda" << endl;
        cout << "5. Notas" << endl;
        cout << "6. Salir" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();
        system(CLEAR);

        switch(opc) {
            case OPC_EVALUACION_CURSO:
                evaluarCurso();
                break;
            case OPC_GESTION_MATERIAS:
                gestionMateria();
                break;
            case OPC_GESTION_TAREAS:
                gestionTareas();
                break;
            case OPC_GESTION_AGENDA:
                gestionAgenda();
                break;
            case OPC_GESTION_NOTAS:
                gestionNotas();
                break;
            case OPC_SALIR:
                repetirMenu = false;
                break;
            default:
                cout << "Opci" << (char)0xA2 << "n no valida" << endl;
                break;
        }
        if(repetirMenu) {
            pausarLimpiar();
        }
    } while(repetirMenu);
    
    return 0;
}

void pausarLimpiar() {
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
    system(CLEAR);
}

void capturarMateria(Materia &materia) {
    string acronimo, nombre, profesor, diasSemana;
    int horaInicio, horaFin;

    cout << "*Captura de materia*" << endl;
    cout << "Acr" << (char)0xA2 << "nimo>";
    cin >> acronimo;
    cin.ignore();
    cout << "Nombre>";
    getline(cin, nombre);
    cout << "Profesor>";
    getline(cin, profesor);
    cout << "D" << (char)0xA1 << "as de la semana>";
    getline(cin, diasSemana);
    cout << "Hora inicio>";
    cin >> horaInicio;
    cout << "Hora de fin>";
    cin >> horaFin;
    cin.ignore();

    materia.fijaNombre(nombre);
    materia.fijaAcronimo(acronimo);
    materia.fijaDiasSemana(diasSemana);
    materia.fijaHoraInicio(horaInicio);
    materia.fijaHoraFin(horaFin);
}

void capturarAsistencias(ControlAsistencia &controlAsistencia) {
    int cantidadFaltas, cantidadRetardos;

    do {
        cout << "Cu" << (char)0xA0 << "ntas faltas acumulaste? ";
        cin >> cantidadFaltas;
    } while(!controlAsistencia.fijaCantidadFaltas(cantidadFaltas));
    do {
        cout << "Cu" << (char)0xA0 << "ntos retardos acumulaste? ";
        cin >> cantidadRetardos;
    } while(!controlAsistencia.fijaCantidadRetardos(cantidadRetardos));
    cin.ignore();
}

void capturarEvaluandos(Evaluador &evaluador) {
    capturarTareas(evaluador);
    capturarActividades(evaluador);
    capturarExamenes(evaluador);
}

void capturarTareas(Evaluador &evaluador) {
    int cantidadTareasRealizadas;

    do {
        cout << "Cu" << (char)0xA0 << "ntas tareas realizaste? ";
        cin >> cantidadTareasRealizadas;
    } while(!evaluador.fijaCantidadTareasRealizadas(cantidadTareasRealizadas));
    cin.ignore();
}

void capturarActividades(Evaluador &evaluador) {
    int calificacion;
    int i;

    i = 0;
    while(i < CANTIDAD_ACTIVIDADES) {
        do {
            cout << "Cu" << (char)0xA0 << "nto obtuviste en la actividad #" << i+1 <<"? ";
            cin >> calificacion;
        } while(!evaluador.fijaCalificacionActividad(i, calificacion));
        i++;
    }
    cin.ignore();
}

void capturarExamenes(Evaluador &evaluador) {
    int calificacion;
    int i;

    i = 0;
    while(i < CANTIDAD_EXAMENES) {
        do {
            cout << "Cu" << (char)0xA0 << "nto obtuviste en el " << i+1 << (char)0xA7 << 
              " examen parcial? ";
            cin >> calificacion;
        } while(!evaluador.fijaCalificacionExamen(i, calificacion));
        i++;
    }
    cin.ignore();
}

void computarEImprimir(ControlAsistencia &controlAsistencia, Evaluador &evaluador) {
    float asistenciasTotales, porcentajeAsistencia;
    bool tieneDerecho;

    float puntosFinalesTareas, puntosFinalesActividades, puntosFinalesExamenes, calificacionFinal;
    bool tieneCalificacionAprobatoria;

    string strDerecho, strCalApro, strAprobado;

    /* Computo de asistencias --------------------------------------------------------------------*/
    asistenciasTotales = controlAsistencia.dameAsistenciasTotales();
    porcentajeAsistencia = controlAsistencia.damePorcentajeAsistencia();
    tieneDerecho = controlAsistencia.tieneDerecho();
    /* Computo de tareas -------------------------------------------------------------------------*/
    puntosFinalesTareas = evaluador.damePuntosFinalesTareas();
    /* Computo de actividades --------------------------------------------------------------------*/
    puntosFinalesActividades = evaluador.damePuntosFinalesActividades();
    /* Computo de examenes parciales -------------------------------------------------------------*/
    puntosFinalesExamenes = evaluador.damePuntosFinalesExamenes();
    /* Computo de puntos finales -----------------------------------------------------------------*/
    calificacionFinal = evaluador.dameCalificacionFinal();
    tieneCalificacionAprobatoria = evaluador.tieneCalificacionAprobatoria();

    strDerecho = tieneDerecho?"Tiene":"No tiene";
    strCalApro = tieneCalificacionAprobatoria?"":"no";
    strAprobado = tieneDerecho && tieneCalificacionAprobatoria?"":"no";


    /* Salida de datos ---------------------------------------------------------------------------*/
    cout << "Tareas\tActividades\tEx" << (char)0xA0 << "menes" << endl;
    cout << puntosFinalesTareas << "\t" << puntosFinalesActividades << "\t\t" << 
      puntosFinalesExamenes << endl;
    cout << "Total de asistencias= \t\t" << asistenciasTotales << endl;
    cout << "Porcentaje de asistencias= \t" << porcentajeAsistencia;
    cout << "\t" << strDerecho << " derecho" << endl;
    cout << "Calificaci" << (char)0xA2 << "n Final= \t\t" << calificacionFinal; 
    cout << "\tCalificaci" << (char)0xA2 << "n " << strCalApro << " aprobatoria" << endl;
    cout << "Alumn@ " << strAprobado << " aprobado" << endl;
}

void capturarTarea(Tarea &tarea) {
    string asunto;
    string descripcion;
    string materia;

    cout << "*Capturar tarea*" << endl;
    cout << "Asunto>";
    getline(cin, asunto);
    cout << "Descripci" << (char)0xA2 << "n>";
    getline(cin, descripcion);
    cout << "Materia>";
    cin >> materia;
    cin.ignore();

    tarea.fijaAsunto(asunto);
    tarea.fijaDescripcion(descripcion);
    tarea.fijaMateria(materia);
}

void imprimirTarea(Tarea &tarea) {
    cout << "*Imprimir tarea*" << endl;
    cout << "Asunto: " << tarea.dameAsunto() << endl;
    cout << "Descripci" << (char)0xA2 << "n: " << tarea.dameDescripcion() << endl; 
    cout << "Materia: " << tarea.dameMateria() << endl; 
}

void capturarRecordatorio(Recordatorio &recordatorio) {
    string fecha;
    string hora;
    string asunto;
    char tipo;

    cout << "*Captura recordatorio*" << endl;
    cout << "Fecha>";
    cin >> fecha;
    cout << "Hora>";
    cin >> hora;
    cin.ignore();
    cout << "Asunto>";
    getline(cin, asunto);
    cout << "Tipo>";
    cin >> tipo;
    cin.ignore();

    recordatorio.fijaFecha(fecha);
    recordatorio.fijaHora(hora);
    recordatorio.fijaAsunto(asunto);
    recordatorio.fijaTipo(tipo);
}

void imprimirRecordatorio(Recordatorio &recordatorio) {
    cout << "*Imprimir recordatorio*" << endl;
    cout << "Fecha: " << recordatorio.dameFecha() << endl;
    cout << "Hora: " << recordatorio.dameHora() << endl;
    cout << "Asunto: " << recordatorio.dameAsunto() << endl;
    cout << "Tipo: " << recordatorio.dameTipo() << endl;
}

void capturarNota(Nota &nota) {
    string descripcion;

    cout << "*Captura nota*" << endl;
    cout << "Descripci" << (char)0xA2 << "n>";
    getline(cin, descripcion);

    nota.fijaDescripcion(descripcion);
}

void imprimirNota(Nota &nota) {
    cout << "*Imprimir nota*" << endl;
    cout << "Descripci" << (char)0xA2 << "n: " << nota.dameDescripcion() << endl;
}


void evaluarCurso() {
    ControlAsistencia controlAsistencia;
    Evaluador evaluador;

    capturarAsistencias(controlAsistencia);
    capturarEvaluandos(evaluador);

    computarEImprimir(controlAsistencia, evaluador);   
}

void gestionMateria() {
    Materia materia;

    capturarMateria(materia);

    cout << "Evaluador del Curso v1.0 - " << materia.dameNombre() << " (" << materia.dameAcronimo() 
      << ") " << materia.dameDiasSemana() << " " << materia.dameHoraInicio() << "a" << 
      materia.dameHoraFin() << endl << endl; 
}

void gestionTareas() {
    Tarea tarea;

    capturarTarea(tarea);

    imprimirTarea(tarea);
}

void gestionAgenda() {
    Recordatorio recordatorio;

    capturarRecordatorio(recordatorio);

    imprimirRecordatorio(recordatorio);
}

void gestionNotas() {
    Nota nota;

    capturarNota(nota);

    imprimirNota(nota);
}