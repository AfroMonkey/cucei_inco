/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 6
 * Name of practice: Area of triangle (scanf)
 * Date: 2015/09/02
 * Version 1
 * Tiempo: 00:02
 */

#include "stdio.h"

int main() {
	printf("/*Calcular el area de un triangulo*/\n");

	int areaTriangulo, base, altura;

	printf("Base >");
	scanf("%d", &base);

	printf("Altura >");
	scanf("%d", &altura);

	areaTriangulo = base * altura / 2;

	printf("Area del triangulo: %d\n", areaTriangulo);
	
	return 0;
}