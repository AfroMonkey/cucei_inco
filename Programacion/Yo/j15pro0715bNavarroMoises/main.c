/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J15
 * Nombre de practica: J15
 * Fecha: 2015/10/10
 * Version 1
 * Tiempo= 00:15
 */

#include "stdio.h"

void imprimirGrafico();
void pausar();

int main() {
    imprimirGrafico();
    pausar();
    return 0;
}

void imprimirGrafico() {
    /*Las lineas acontinuacion rebasan las 100 columnas; pero de otra manera no sertia entendible
     *para el programador
     */
    printf("***********************************************************************************************************************\n");
    printf("***************************            _          __  __             _                     ****************************\n");
    printf("***************************    /\\    / _|         |  \\/  |           | |                   ****************************\n");
    printf("***************************   /  \\  | |_ _ __ ___ | \\  / | ___  _ __ | | _____ _   _       ****************************\n");
    printf("***************************  / /\\ \\ |  _| '__/ _ \\| |\\/| |/ _ \\| '_ \\| |/ / _ \\ | | |      ****************************\n");
    printf("*************************** / ____ \\| | | | | (_) | |  | | (_) | | | |   <  __/ |_| |      ****************************\n");
    printf("***************************/_/    \\_\\_| |_|  \\___/|_|  |_|\\___/|_| |_|_|\\_\\___|\\__, |      ****************************\n");
    printf("***************************                                                     __/ |_____ ****************************\n");
    printf("***************************                                                    |___/______|****************************\n");
    printf("***********************************************************************************************************************\n");
}

void pausar() {
    printf("Presione entrar para continuar . . .");
    getchar();
}