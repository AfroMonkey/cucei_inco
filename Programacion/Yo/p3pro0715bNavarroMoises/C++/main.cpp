/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: P3
 * Nombre de practica: Variables Globales y Variables Locales
 * Fecha: 2015/11/24
 * Version 1
 * Tiempo: 00:10
 */

#include <iostream>
#include <cstdlib>

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

/* Declaracion y definicion de constantes --------------------------------------------------------*/
#define PORCENTAJE_IVA 16
#define CANTIDAD_PORCENTAJES_ISR 3
#define PORCENTAJE_RETENCION_ISR 10
#define PORCENTAJE_RETENCION_IVA 10
#define CANTIDAD_MESES 12
#define OPC_ESTABLECER_MES 1
#define OPC_CAPTURAR_INGRESOS 2
#define OPC_CAPTURAR_GASTOS 3
#define OPC_LISTAR_INGRESOS 4
#define OPC_LISTAR_GASTOS 5
#define OPC_CALCULAR_IMPUESTOS 6
#define OPC_SALIR 7
/* Variables globales de uso interno del programa ------------------------------------------------*/
float ingresos[CANTIDAD_MESES];
float gastos[CANTIDAD_MESES];
string meses[CANTIDAD_MESES];
int mesActual;
int porcentajesIsr[CANTIDAD_PORCENTAJES_ISR];
float limitesInferioresIsr[CANTIDAD_PORCENTAJES_ISR];

void inicializarVariables();
void imprimirMenu();
void establecerMes();
void capturarIngresos();
void capturarGastos();
void listarIngresos();
void listarGastos();
void calcularImpuestos();
int porcentajeIsrAplicable(float gananciaBruta);

int main() {
    int continuarPrograma;
    /* Variables de programa modificables por el usuario -----------------------------------------*/
    int opcion;

    inicializarVariables();
    do {
        imprimirMenu();
        cin >> opcion;
        cin.ignore();

        switch(opcion) {
            case OPC_ESTABLECER_MES:
                establecerMes();
                break;
            case OPC_CAPTURAR_INGRESOS:
                capturarIngresos();
                break;
            case OPC_CAPTURAR_GASTOS:
                capturarGastos();
                break;
            case OPC_LISTAR_INGRESOS:
                listarIngresos();
                break;
            case OPC_LISTAR_GASTOS:
                listarGastos();
                break;
            case OPC_CALCULAR_IMPUESTOS:
                calcularImpuestos();
                break;
            case OPC_SALIR:
                break;
            default:
                cout << "Opci" << (char)0xA2 << "n invalida" << endl;
                break;
        }
        continuarPrograma = opcion != OPC_SALIR;
        if(continuarPrograma) {
            cout << "Presione entrar para continuar . . .";
            cin.ignore();
            system(CLEAR);
        }
    } while(continuarPrograma);

    return 0;
}

void inicializarVariables() {
    int i;

    for(i = 0; i < CANTIDAD_MESES; i++) {
        ingresos[i] = gastos[i] = 0;
    }
    meses[0] = "Enero";
    meses[1] = "Febrero";
    meses[2] = "Marzo";
    meses[3] = "Abril";
    meses[4] = "Mayo";
    meses[5] = "Junio";
    meses[6] = "Julio";
    meses[7] = "Agosto";
    meses[8] = "Septiembre";
    meses[9] = "Octubre";
    meses[10] = "Noviembre";
    meses[11] = "Diciembre";
    mesActual = 0;
    limitesInferioresIsr[0] = 0.00;
    limitesInferioresIsr[1] = 10000.01;
    limitesInferioresIsr[2] = 20000.01;
    porcentajesIsr[0] = 11;
    porcentajesIsr[1] = 15;
    porcentajesIsr[2] = 20;
}

void imprimirMenu() {
    cout << "C" << (char)0x8F << "LCULO DE IMPUESTOS ANUAL" << endl << endl;
    cout << "Men" << (char)0xA3 << " principal:" << endl;
    cout << "1. Establecer mes para captura (mes actual es "<< meses[mesActual] <<")" << endl;
    cout << "2. Captura de ingresos" << endl;
    cout << "3. Captura de gastos" << endl;
    cout << "4. Mostrar lista de ingresos anual" << endl;
    cout << "5. Mostrar lista de gastos anual" << endl;
    cout << "6. C" << (char)0xA0 << "lculo de impuestos anual" << endl;
    cout << "7. Salir\n" << endl;
    cout << "Opci" << (char)0xA2 << "n: ";
}

void establecerMes() {
    int mesSolicitado, i;

    system(CLEAR);
    cout << "Establecer mes para captura" << endl;
    for(i = 0; i < CANTIDAD_MESES; i++) {
        cout << i+1 << ")" << meses[i] << endl;
    }
    cout << "Elige el mes (1-12): ";
    cin>> mesSolicitado;
    cin.ignore();
    if(mesSolicitado >= 1 && mesSolicitado <= CANTIDAD_MESES) {
        mesActual = mesSolicitado-1;
        cout << "Se ha establecido el mes de captura en " << meses[mesActual] << endl;
    } else {
        cout << "Mes invalido, no se efectu" << (char)0xA2 << " cambios" << endl;
    }
}

void capturarIngresos() {
    cout << "Captura de ingresos" << endl;
    do {
        cout << "Dame el ingreso del mes de " << meses[mesActual] << ": ";
        cin>> ingresos[mesActual];
    } while(ingresos[mesActual] < 0);
    cin.ignore();
}

void capturarGastos() {
    cout << "Captura de gastos" << endl;
    do {
        cout << "Dame el gasto del mes de " << meses[mesActual] <<": ";
        cin>> gastos[mesActual];
    } while(gastos[mesActual] < 0);
    cin.ignore();
}

void listarIngresos() {
    int i;

    system(CLEAR);
    cout << "Mostrar lista de ingresos anual" << endl;
    for(i = 0; i < CANTIDAD_MESES; i++) {
        cout << meses[i] << " = " << ingresos[i] << endl;
    }
}

void listarGastos() {
    int i;

    system(CLEAR);
    cout << "Mostrar lista de gastos anual" << endl;
    for(i = 0; i < CANTIDAD_MESES; i++) {
        cout << meses[i] << " = " << gastos[i] << endl;
    }
}

void calcularImpuestos() {
    /* Declaracion de variables ------------------------------------------------------------------*/
    int porcentajeIsr, i;
    float ingresosTotales, iva, subtotal, retencionIsr, retencionIva, total;
    float gastosTotales, gananciaBruta, isr, gananciaNeta, pagoIsr, gastosIva, pagoIva;
    /* Calculo de impuestos ----------------------------------------------------------------------*/
    for(ingresosTotales = gastosTotales = i = 0; i < CANTIDAD_MESES; i++) {
        ingresosTotales += ingresos[i];
        gastosTotales += gastos[i];
    }
    iva = ingresosTotales * PORCENTAJE_IVA / 100;
    subtotal = ingresosTotales + iva;
    retencionIsr = ingresosTotales * PORCENTAJE_RETENCION_ISR / 100;
    retencionIva = ingresosTotales * PORCENTAJE_RETENCION_IVA / 100;
    total = subtotal - retencionIsr - retencionIva;
    gananciaBruta = ingresosTotales - gastosTotales;
    porcentajeIsr = porcentajeIsrAplicable(gananciaBruta);
    isr = gananciaBruta * porcentajeIsr / 100;
    gananciaNeta = gananciaBruta - isr;
    pagoIsr = isr - retencionIsr;
    gastosIva = gastosTotales * PORCENTAJE_IVA / 100;
    pagoIva = iva - gastosIva - retencionIva;
    /* Salida de datos ---------------------------------------------------------------------------*/
    system(CLEAR);
    cout << "C" << (char)0xB5 << "LCULO DE IMPUESTOS" << endl;
    cout << "***Tabla Recibo de Honorarios*** " << endl;
    cout << "Ingresos " << ingresosTotales << endl;
    cout << "(+) IVA " << iva << endl;
    cout << "(=) Subtotal " << subtotal << endl;
    cout << "(-) Retenci" << (char)0xA2 << "n ISR " << retencionIsr << endl;
    cout << "(-) Retenci" << (char)0xA2 << "n IVA " << retencionIva << endl;
    cout << "(=) Total  " << total <<endl;
    cout << "***Tabla Ganancias*** " << endl;
    cout << "Ingresos " << ingresosTotales << endl;
    cout << "(-) Gastos  " << gastosTotales <<endl;
    cout << "(=) Ganancia Bruta " << gananciaBruta << endl;
    cout << "(-) ISR " << porcentajeIsr << "%% " << isr << endl;
    cout << "(=) Ganancia Neta " << gananciaNeta << endl;
    cout << "***Tabla ISR*** " << endl;
    cout << "ISR " << porcentajeIsr << "%% " << isr << endl;
    cout << "(-) ISR Retenido " << retencionIsr << endl;
    cout << "(=) ISR a Pagar " << pagoIsr << endl;
    cout << "***Tabla IVA*** " << endl;
    cout << "IVA " << isr << endl;
    cout << "(-) Gastos IVA " << gastosIva << endl;
    cout << "(-) Retenci" << (char)0xA2 << "n IVA " << retencionIva << endl;
    cout << "(=) IVA a Pagar " << pagoIva << endl;
}


int porcentajeIsrAplicable(float gananciaBruta) {
    int i;

    if(gananciaBruta >= 0) {
        i = 0;
        while(i < CANTIDAD_PORCENTAJES_ISR && gananciaBruta >= limitesInferioresIsr[i]) {
            i++;
        }
        return porcentajesIsr[--i];
    } else {
        return 0;
    }
}
