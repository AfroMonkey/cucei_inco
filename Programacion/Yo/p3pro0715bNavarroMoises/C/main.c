/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: P3
 * Nombre de practica: Variables Globales y Variables Locales
 * Fecha: 2015/11/24
 * Version 1
 * Tiempo: 01:00
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

/* Declaracion y definicion de constantes --------------------------------------------------------*/
#define PORCENTAJE_IVA 16
#define CANTIDAD_PORCENTAJES_ISR 3
#define PORCENTAJE_RETENCION_ISR 10
#define PORCENTAJE_RETENCION_IVA 10
#define CANTIDAD_MESES 12
#define OPC_ESTABLECER_MES 1
#define OPC_CAPTURAR_INGRESOS 2
#define OPC_CAPTURAR_GASTOS 3
#define OPC_LISTAR_INGRESOS 4
#define OPC_LISTAR_GASTOS 5
#define OPC_CALCULAR_IMPUESTOS 6
#define OPC_SALIR 7
/* Variables globales de uso interno del programa ------------------------------------------------*/
float ingresos[CANTIDAD_MESES];
float gastos[CANTIDAD_MESES];
char* meses[];
int mesActual;
int porcentajesIsr[CANTIDAD_PORCENTAJES_ISR];
float limitesInferioresIsr[CANTIDAD_PORCENTAJES_ISR];

void inicializarVariables();
void imprimirMenu();
void establecerMes();
void capturarIngresos();
void capturarGastos();
void listarIngresos();
void listarGastos();
void calcularImpuestos();
int porcentajeIsrAplicable(float gananciaBruta);

int main() {
    int continuarPrograma;
    /* Variables de programa modificables por el usuario -----------------------------------------*/
    int opcion;

    inicializarVariables();
    do {
        imprimirMenu();
        scanf("%d", &opcion);
        getchar();

        switch(opcion) {
            case OPC_ESTABLECER_MES:
                establecerMes();
                break;
            case OPC_CAPTURAR_INGRESOS:
                capturarIngresos();
                break;
            case OPC_CAPTURAR_GASTOS:
                capturarGastos();
                break;
            case OPC_LISTAR_INGRESOS:
                listarIngresos();
                break;
            case OPC_LISTAR_GASTOS:
                listarGastos();
                break;
            case OPC_CALCULAR_IMPUESTOS:
                calcularImpuestos();
                break;
            case OPC_SALIR:
                break;
            default:
                printf("Opci\xA2n invalida\n");
                break;
        }
        continuarPrograma = opcion != OPC_SALIR;
        if(continuarPrograma) {
            printf("Presione entrar para continuar . . .");
            getchar();
            system(CLEAR);
        }
    } while(continuarPrograma);

    return 0;
}

void inicializarVariables() {
    ingresos = {};
    gastos = {};
    meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", 
      "Septiembre", "Octubre", "Noviembre", "Diciembre"}
    mesActual = 0;
    limitesInferioresIsr[0] = 0.00;
    limitesInferioresIsr[1] = 10000.01;
    limitesInferioresIsr[2] = 20000.01;
    porcentajesIsr[0] = 11;
    porcentajesIsr[1] = 15;
    porcentajesIsr[2] = 20;
}

void imprimirMenu() {
    printf("C\x8FLCULO DE IMPUESTOS ANUAL\n\n");
    printf("Men\xA3 principal:\n");
    printf("1. Establecer mes para captura (mes actual es %s)\n", meses[mesActual]);
    printf("2. Captura de ingresos\n");
    printf("3. Captura de gastos\n");
    printf("4. Mostrar lista de ingresos anual\n");
    printf("5. Mostrar lista de gastos anual\n");
    printf("6. C\xA0lculo de impuestos anual\n");
    printf("7. Salir\n\n");
    printf("Opci\xA2n: ");
}

void establecerMes() {
    int mesSolicitado, i;

    system(CLEAR);
    printf("Establecer mes para captura\n");
    for(i = 0; i < CANTIDAD_MESES; i++) {
        printf("%d) %s\n", i+1, meses[i]);
    }
    printf("Elige el mes (1-12): ");
    scanf("%d", &mesSolicitado);
    getchar();
    if(mesSolicitado >= 1 && mesSolicitado <= CANTIDAD_MESES) {
        mesActual = mesSolicitado-1;
        printf("Se ha establecido el mes de captura en %s\n", meses[mesActual]);
    } else {
        printf("Mes invalido, no se efectu\xA2 cambios\n");
    }
}

void capturarIngresos() {
    printf("Captura de ingresos\n");
    do {
        printf("Dame el ingreso (mayor o igual que 0) del mes de %s: ", meses[mesActual]);
        scanf("%f", &ingresos[mesActual]);
    } while(ingresos[mesActual] < 0);
    getchar();
}

void capturarGastos() {
    printf("Captura de gastos\n");
    do {
        printf("Dame el gasto (mayor o igual que 0) del mes de %s: ", meses[mesActual]);
        scanf("%f", &gastos[mesActual]);
    } while(gastos[mesActual] < 0);
    getchar();
}

void listarIngresos() {
    int i;

    system(CLEAR);
    printf("Mostrar lista de ingresos anual\n");
    for(i = 0; i < CANTIDAD_MESES; i++) {
        printf("%s = %.2f\n", meses[i], ingresos[i]);
    }
}

void listarGastos() {
    int i;

    system(CLEAR);
    printf("Mostrar lista de gastos anual\n");
    for(i = 0; i < CANTIDAD_MESES; i++) {
        printf("%s = %.2f\n", meses[i], gastos[i]);
    }
}

void calcularImpuestos() {
    /* Declaracion de variables ------------------------------------------------------------------*/
    int porcentajeIsr, i;
    float ingresosTotales, iva, subtotal, retencionIsr, retencionIva, total;
    float gastosTotales, gananciaBruta, isr, gananciaNeta, pagoIsr, gastosIva, pagoIva;
    /* Calculo de impuestos ----------------------------------------------------------------------*/
    for(ingresosTotales = gastosTotales = i = 0; i < CANTIDAD_MESES; i++) {
        ingresosTotales += ingresos[i];
        gastosTotales += gastos[i];
    }
    iva = ingresosTotales * PORCENTAJE_IVA / 100;
    subtotal = ingresosTotales + iva;
    retencionIsr = ingresosTotales * PORCENTAJE_RETENCION_ISR / 100;
    retencionIva = ingresosTotales * PORCENTAJE_RETENCION_IVA / 100;
    total = subtotal - retencionIsr - retencionIva;
    gananciaBruta = ingresosTotales - gastosTotales;
    porcentajeIsr = porcentajeIsrAplicable(gananciaBruta);
    isr = gananciaBruta * porcentajeIsr / 100;
    gananciaNeta = gananciaBruta - isr;
    pagoIsr = isr - retencionIsr;
    gastosIva = gastosTotales * PORCENTAJE_IVA / 100;
    pagoIva = iva - gastosIva - retencionIva;
    /* Salida de datos ---------------------------------------------------------------------------*/
    system(CLEAR);
    printf("C\xA0lculo de impuestos anual\n\n");
    printf("***Tabla Recibo de Honorarios***\n");
    printf("Ingresos \t\t %10.2f\n", ingresosTotales);
    printf("(+) IVA \t\t %10.2f\n", iva);
    printf("(=) Subtotal \t\t %10.2f\n", subtotal);
    printf("(-) Retenci\xA2n ISR \t %10.2f\n", retencionIsr);
    printf("(-) Retenci\xA2n IVA \t %10.2f\n", retencionIva);
    printf("(=) Total  \t\t %10.2f\n", total);
    printf("***Tabla Ganancias*** \n");
    printf("Ingresos \t\t %10.2f\n", ingresosTotales);
    printf("(-) Gastos  \t\t %10.2f\n", gastosTotales);
    printf("(=) Ganancia Bruta \t %10.2f\n", gananciaBruta);
    printf("(-) ISR %d%% \t\t %10.2f\n", porcentajeIsr, isr);
    printf("(=) Ganancia Neta  \t %10.2f\n", gananciaNeta);
    printf("***Tabla ISR*** \n");
    printf("ISR %d%%\t\t\t %10.2f\n", porcentajeIsr, isr);
    printf("(-) ISR Retenido \t %10.2f\n", retencionIsr);
    printf("(=) ISR a Pagar \t %10.2f\n", pagoIsr);
    printf("***Tabla IVA*** \n");
    printf("IVA \t\t\t %10.2f\n", isr);
    printf("(-) Gastos IVA \t\t %10.2f\n", gastosIva);
    printf("(-) Retenci\xA2n IVA \t %10.2f\n", retencionIva);
    printf("(=) IVA a Pagar \t %10.2f\n", pagoIva);
}


int porcentajeIsrAplicable(float gananciaBruta) {
    int i;

    if(gananciaBruta >= 0) {
        i = 0;
        while(i < CANTIDAD_PORCENTAJES_ISR && gananciaBruta >= limitesInferioresIsr[i]) {
            i++;
        }
        return porcentajesIsr[--i];
    } else {
        return 0;
    }
}
