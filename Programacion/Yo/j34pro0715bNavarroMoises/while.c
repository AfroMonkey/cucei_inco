/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J34
 * Nombre de practica: J34
 * Fecha: 2015/11/07
 * Version 1
 * Tiempo: 00:25
 */

#include "stdio.h"

#define EDAD_MAX_NINYO 12
#define EDAD_MAX_ADOLESCENTE 17
#define EDAD_MAX_JOVEN 29
#define EDAD_MAX_ADULTO_J 59

int main() {
    float pesoPromNinyos, pesoPromAdolescentes, pesoPromJovenes, pesoPromAdultosJ, pesoPromAdultosM;
    int numNinyos, numAdolescentes, numJovenes, numAdultosJ, numAdultosM;
    int numPacientes;
    float peso;
    int edad;
    int i;

    printf("Numero de pacientes>");
    scanf("%d", &numPacientes);

    if(numPacientes > 0) {
        pesoPromNinyos = pesoPromAdolescentes = pesoPromJovenes = pesoPromAdultosJ = 
          pesoPromAdultosM = 0;
        numNinyos = numAdolescentes = numJovenes = numAdultosJ = numAdultosM = 0;
        i = 0;
        while(i < numPacientes) {
            printf("Edad del paciente #%d>", ++i);
            scanf("%d", &edad);
            
            if(edad > 0) {
                printf("Peso del paciente %d>", i);
                scanf("%f", &peso);
                
                if(edad <= EDAD_MAX_NINYO) {
                    pesoPromNinyos += peso;
                    numNinyos++;
                } else if(edad <= EDAD_MAX_ADOLESCENTE) {
                    pesoPromAdolescentes += peso;
                    numAdolescentes++;
                } else if(edad <= EDAD_MAX_JOVEN) {
                    pesoPromJovenes += peso;
                    numJovenes++;
                } else if(edad <= EDAD_MAX_ADULTO_J) {
                    pesoPromAdultosJ += peso;
                    numAdultosJ++;
                } else {
                    pesoPromAdultosM += peso;
                    numAdultosM++;
                }
            } else {
                printf("Error, edad invalida\n");
            }
        }
        pesoPromNinyos = (numNinyos!=0) ? pesoPromNinyos/numNinyos : -1;
        pesoPromAdolescentes = (numAdolescentes!=0) ? pesoPromAdolescentes/numAdolescentes : -1;
        pesoPromJovenes = (numJovenes!=0) ? pesoPromJovenes/numJovenes : -1;
        pesoPromAdultosJ = (numAdultosJ!=0) ? pesoPromAdultosJ/numAdultosJ : -1;
        pesoPromAdultosM = (numAdultosM!=0) ? pesoPromAdultosM/numAdultosM : -1;

        printf("\n");
        printf("NOTA: Si el peso promedio es -1, significa que no hubo pacientes en ese rango\n");
        printf("Peso promedio de ninyos: %.2f\n", pesoPromNinyos);
        printf("Peso promedio de adolescentes: %.2f\n", pesoPromAdolescentes);
        printf("Peso promedio de jovenes: %.2f\n", pesoPromJovenes);
        printf("Peso promedio de adultos jovenes: %.2f\n", pesoPromAdultosJ);
        printf("Peso promedio de adultos mayores: %.2f\n", pesoPromAdultosM);
    } else {
        printf("No hay nadie\n");
    }

    return 0;
}
