/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: T3
 * Nombre de practica: Estructuras de Control Iterativas y Anidadas 
 * Fecha: 2015/11/11
 * Version 2
 * Tiempo: 00:10
 */

#include <iostream>
#include <cstdlib>

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_ARITMETICA_SIMPLE 1
#define OPC_ARITMETICA_EXTENDIDA 2
#define OPC_FACTORIAL 3
#define OPC_TABLA_MULTIPLICAR 4
#define OPC_SALIR 5

int main() {
    /*Variables de uso interno del programa*/
    int i;
    int opc;
    bool finPrograma;
    int factorial;
    /*Variables para entrada de datos*/
    int a, b, c;
    char operador;
    int n;
    int multiplicando, maxMultiplicador;

    finPrograma = false;
    opc = 0;

    do {
        /*Menu principal*/
        cout << "CALCULADORA v1.0" << endl;
        cout << "Elige una de las siguientes opciones:" << endl;
        cout << "1. Aritmetica simple con suma de enteros a+b" << endl;
        cout << "2. Aritmetica extendida binaria (+,-,*,/,%% modulo)" << endl;
        cout << "3. Factorial de n (n!)" << endl;
        cout << "4. Detallar una tabla de multiplicar" << endl;
        cout << "5. Salir" << endl;
        cout << "Opcion a elegir? ";
        cin >> opc;
        cin.ignore();

        /*Realizacion de operaciones*/
        switch(opc) {
            case OPC_ARITMETICA_SIMPLE:
                /*Aritmetica simple con suma de enteros a+b*/
                cout << "Dame el valor de a=";
                cin >> a;
                cout << "Dame el valor de b=";
                cin >> b;
                cin.ignore();
                c = a+b;
                cout << a << " + " << b << " = " << c << endl;
                break;
            case OPC_ARITMETICA_EXTENDIDA:
                /*Aritmetica extendida binaria (+,-,*,/,%% modulo)*/
                cout << "Escribe el operador (+,-,*,/,%): ";
                cin >> operador;
                cout << "Dame el valor de a=";
                cin >> a;
                cout << "Dame el valor de b=";
                cin >> b;
                cin.ignore();
                switch(operador) {
                    case '+':
                        c = a+b;
                        break;
                    case '-':
                        c = a-b;
                        break;
                    case '*':
                        c = a*b;
                        break;
                    case '/':
                        if(b != 0) {
                            c = a/b;
                        } else {
                            c = 0;
                            cout << "No se permite que el divisor sea 0" << endl;
                        }
                        break;
                    case '%':
                        c = a%b;
                        break;
                    default:
                        c = 0;
                        cout << "No hay un resultado calculable" << endl;
                        break;
                }
                cout << a << " " << operador << " " << b << " = " << c << endl;
                break;
            case OPC_FACTORIAL:
                /*Factorial de n (n!) */
                cout << "Dame el valor de n=";
                cin >> n;
                cin.ignore();
                if(n >= 0) {
                    for(factorial = 1, i = n; i > 0; i--) {
                        factorial *= i;
                    }
                    cout << "El factorial de "<< n << " es " << factorial << endl;
                } else {
                    cout << "El factorial de " << n << " no esta definido." << endl;
                }
                break;
            case OPC_TABLA_MULTIPLICAR:
                /*Detallar una tabla de multiplicar*/
                cout << "Dame el multiplicando: ";
                cin >> multiplicando;
                cout << "Dame hasta cual multiplicador: ";
                cin >> maxMultiplicador;
                cin.ignore();
                if(maxMultiplicador >= 1) {
                    cout << "Tabla de multiplicar del " << multiplicando << "..." << endl;
                    i = 1;
                    while(i <= maxMultiplicador) {
                        cout << multiplicando << "\t X \t " << i << "\t = \t" << multiplicando*i << endl;
                        i++;
                    }
                } else {
                    cout << "Limite invalido" << endl;
                }
                break;
            case OPC_SALIR:
                /*Salida del programa*/
                finPrograma = true;
                break;
            default:
                cout << endl;
                cout << "Opcion no valida" << endl;
                break;
        }

        if(!finPrograma) {
            cout << endl;
            cout << "Presione entrar para continuar . . .";
            cin.ignore();
            system(CLEAR);
        }
    } while(!finPrograma);
    return 0;
}
