/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: T3
 * Nombre de practica: Estructuras de Control Iterativas y Anidadas 
 * Fecha: 2015/11/11
 * Version 1
 * Tiempo: 00:42
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_ARITMETICA_SIMPLE 1
#define OPC_ARITMETICA_EXTENDIDA 2
#define OPC_FACTORIAL 3
#define OPC_TABLA_MULTIPLICAR 4
#define OPC_SALIR 5

int main() {
    /*Variables de uso interno del programa*/
    int i;
    int opc;
    int finPrograma;
    int factorial;
    /*Variables para entrada de datos*/
    int a, b, c;
    char operador;
    int n;
    int multiplicando, maxMultiplicador;

    finPrograma = opc = 0;

    do {
        /*Menu principal*/
        printf("CALCULADORA v1.0\n");
        printf("Elige una de las siguientes opciones:\n");
        printf("1. Aritmetica simple con suma de enteros a+b\n");
        printf("2. Aritmetica extendida binaria (+,-,*,/,%% modulo)\n");
        printf("3. Factorial de n (n!)\n");
        printf("4. Detallar una tabla de multiplicar\n");
        printf("5. Salir\n");
        printf("Opcion a elegir? ");
        scanf("%d", &opc);
        getchar();

        /*Realizacion de operaciones*/
        switch(opc) {
            case OPC_ARITMETICA_SIMPLE:
                /*Aritmetica simple con suma de enteros a+b*/
                printf("Dame el valor de a=");
                scanf("%d", &a);
                printf("Dame el valor de b=");
                scanf("%d", &b);
                getchar();
                c = a+b;
                printf("%d + %d = %d\n", a, b, c);
                break;
            case OPC_ARITMETICA_EXTENDIDA:
                /*Aritmetica extendida binaria (+,-,*,/,%% modulo)*/
                printf("Escribe el operador (+,-,*,/,%%): ");
                scanf("%c", &operador);
                printf("Dame el valor de a=");
                scanf("%d", &a);
                printf("Dame el valor de b=");
                scanf("%d", &b);
                getchar();
                switch(operador) {
                    case '+':
                        c = a+b;
                        break;
                    case '-':
                        c = a-b;
                        break;
                    case '*':
                        c = a*b;
                        break;
                    case '/':
                        if(b != 0) {
                            c = a/b;
                        } else {
                            c = 0;
                            printf("No se permite que el divisor sea 0\n");
                        }
                        break;
                    case '%':
                        c = a%b;
                        break;
                    default:
                        c = 0;
                        printf("No hay un resultado calculable\n");
                        break;
                }
                printf("%d %c %d = %d\n", a, operador, b, c);
                break;
            case OPC_FACTORIAL:
                /*Factorial de n (n!) */
                printf("Dame el valor de n=");
                scanf("%d", &n);
                getchar();
                if(n >= 0) {
                    for(factorial = 1, i = n; i > 0; i--) {
                        factorial *= i;
                    }
                    printf("El factorial de %d es %d\n", n, factorial);
                } else {
                    printf("El factorial de %d no esta definido.\n", n);
                }
                break;
            case OPC_TABLA_MULTIPLICAR:
                /*Detallar una tabla de multiplicar*/
                printf("Dame el multiplicando: ");
                scanf("%d", &multiplicando);
                printf("Dame hasta cual multiplicador: ");
                scanf("%d", &maxMultiplicador);
                getchar();
                if(maxMultiplicador >= 1) {
                    printf("Tabla de multiplicar del %d...\n", multiplicando);
                    i = 1;
                    while(i <= maxMultiplicador) {
                        printf("%d \t X \t %d \t = \t %d \n", multiplicando, i, multiplicando*i);
                        i++;
                    }
                } else {
                    printf("Limite invalido\n");
                }
                break;
            case OPC_SALIR:
                /*Salida del programa*/
                finPrograma = 1;
                break;
            default:
                printf("\n");
                printf("Opcion no valida\n");
                break;
        }

        if(!finPrograma) {
            printf("\n");
            printf("Presione entrar para continuar . . .");
            getchar();
            system(CLEAR);
        }
    } while(!finPrograma);
    return 0;
}
