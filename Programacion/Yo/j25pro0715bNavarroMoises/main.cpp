/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J25
 * Nombre de practica: J25
 * Fecha: 2015/10/26
 * Version: 1
 * Tiempo: 00:10
 */

#include <iostream>

using namespace std;

#define PORC_IVA 16.0
#define BASE_MOTOCICLETA 35.0
#define BASE_AUTOMOVIL 48.50
#define BASE_CAMION 57.0
#define INC_POR_TONELADA 18.0
#define OPC_MOTOCICLETA 1
#define OPC_AUTOMOVIL 2
#define OPC_CAMION 3

int main() {
    float importe;
    int opc;

    opc = -1;

    cout << "1) Motocicleta" << endl;
    cout << "2) Autom\xA2vil" << endl;
    cout << "3) Cami\xA2n" << endl;
    cout << ">";
    cin >> opc;

    switch(opc) {
        case OPC_MOTOCICLETA: {
            importe = BASE_MOTOCICLETA + BASE_MOTOCICLETA*PORC_IVA/100;
            break;
        }
        case OPC_AUTOMOVIL: {
            importe = BASE_AUTOMOVIL + BASE_AUTOMOVIL*PORC_IVA/100;
            break;
        }
        case OPC_CAMION: {
            int toneladas;
            cout << "Toneladas>";
            cin >> toneladas;
            importe = BASE_CAMION + toneladas*INC_POR_TONELADA;
            break;
        }
        default: {
            cout << "Opci\xA2n invalida" << endl;
            importe = -1;
            break;
        }
    }

    cout << "Importe: $" << importe << endl;

    return 0;
}
