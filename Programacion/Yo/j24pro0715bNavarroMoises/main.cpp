/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J24
 * Nombre de practica: J24
 * Fecha: 2015/10/23
 * Version: 1
 * Tiempo 00:10
 */

#include <iostream>
#include <cmath>

using namespace std;

#define CIRCULO 1
#define CUADRADO 2
#define TRIANGULO 3
#define RECTANGULO 4

int main() {
    int opc;
    float area;

    opc = -1;

    cout << "**CALCULO DE AREA**" << endl;
    cout << "1) Circulo" << endl;
    cout << "2) Cuadrado" << endl;
    cout << "3) Triangulo" << endl;
    cout << "4) Rectangulo" << endl;
    cout << ">";
    cin >> opc;

    switch(opc) {
        case CIRCULO: {
            float radio;
            radio = -1;
            cout << "Radio>";
            cin>>radio;
            area = M_PI*radio*radio;
            break;
        }
        case CUADRADO: {
            float lado;
            lado = -1;
            cout << "Lado>";
            cin >> lado;
            area = lado*lado;
            break;
        }
        case TRIANGULO: {
            float base, altura;
            base = altura = -1;
            cout << "Base>";
            cin >> base;
            cout << "Altura>";
            cin >> altura;
            area = base*altura/2;
            break;
        }
        case RECTANGULO: {
            float lado1, lado2;
            lado1 = lado2 = -1;
            cout << "Lado 1>";
            cin >> lado1;
            cout << "Lado 2>";
            cin >> lado2;
            area = lado1*lado2;
            break;
        }
        default: {
            area = -1;
            cout << "Opcion invalida" << endl;
            break;
        }
    }

    cout << "Area = " << area;

    return 0;
}
