/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 8
 * Name of practice: Operations
 * Date: 2015/09/02
 * Version 1
 * Timepo: 00:02
 */
 
#include "stdio.h"

int main() {
	printf("/*Operaciones con enteros*/\n");

	int a, b;

	printf("a> ");
	scanf("%d", &a);

	printf("b> ");
	scanf("%d", &b);

	printf("%d + %d = %d\n", a, b, a+b);
	printf("%d - %d = %d\n", a, b, a-b);
	printf("%d - %d = %d\n", b, a, b-a);
	printf("%d * %d = %d\n", a, b, a*b);
	printf("%d / %d = %d\n", a, b, a/b);
	printf("%d / %d = %d\n", b, a, b/a);
	printf("%d %% %d = %d\n", a, b, a%b);
	printf("%d %% %d = %d\n", b, a, b%a);

	return 0;
}