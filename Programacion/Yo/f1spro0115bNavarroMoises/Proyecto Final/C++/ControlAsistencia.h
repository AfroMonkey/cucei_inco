#ifndef CONTROL_ASISTENCIA_INCLUDED
#define CONTROL_ASISTENCIA_INCLUDED

#include "constantes.h"

class ControlAsistencia {
    int cantidadFaltas;
    int cantidadRetardos;
    float asistenciasTotales;
    float porcentajeAsistencia;
public:
    ControlAsistencia() {
        cantidadFaltas = -1;
        cantidadRetardos = -1;
        asistenciasTotales = 0;
        porcentajeAsistencia = 0;
    }

    bool fijaCantidadFaltas(int cantidadFaltas) {
        if(cantidadFaltas>= 0 && cantidadFaltas <= CANTIDAD_ASISTENCIAS_MAXIMA) {
            this->cantidadFaltas = cantidadFaltas;
            return true;
        } else {
            return false;
        }
    }

    int dameCantidadFaltas() {
        return cantidadFaltas;
    }

    bool fijaCantidadRetardos(int cantidadRetardos) {
        if(cantidadFaltas>= 0 && cantidadFaltas <= CANTIDAD_ASISTENCIAS_MAXIMA) {
            this->cantidadRetardos = cantidadRetardos;
            return true;
        } else {
            return false;
        }
    }

    int dameCantidadRetardos() {
        return cantidadRetardos;
    }

    float dameAsistenciasTotales() {
        asistenciasTotales = CANTIDAD_ASISTENCIAS_MAXIMA;
        asistenciasTotales -= cantidadFaltas;
        asistenciasTotales -= (float)cantidadRetardos / RETARDOS_POR_FALTA;

        return asistenciasTotales;
    }

    float damePorcentajeAsistencia() {
        porcentajeAsistencia = asistenciasTotales / CANTIDAD_ASISTENCIAS_MAXIMA * 100;

        return porcentajeAsistencia;
    }

    bool tieneDerecho() {
        return porcentajeAsistencia >= PORCENTAJE_ASISTENCIA_MINIMA_PARA_DERECHO;
    }
};

#endif
