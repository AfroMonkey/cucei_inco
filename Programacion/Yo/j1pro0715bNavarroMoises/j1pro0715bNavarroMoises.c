/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 1
 * Name of practice: Area of a triangle
 * Date: 2015/08/20
 * Version 1
 */
 
#include "stdio.h"

int main() {
	int a, b, h;

	/* Default values */
	b = 3;
	h = 4;

	printf("base >");
	scanf("%d", &b);

	printf("height >");	
	scanf("%d", &h);

	a = (b * h) / 2;
	printf("area: %d.%d", a, (b%2 && h%2)?5:0);

	return 0;
}