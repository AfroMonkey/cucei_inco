/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J28
 * Nombre de practica: J28
 * Fecha: 2015/10/29
 * Version 1
 * Tiempo: 00:12
 */

#include "stdio.h"

#define NUM_PERSONAS 100

int main() {
    char opc;
    int a, b, c;
    int i;

    a = b = c = i = 0;

    do {
        printf("Persona %d\n", i+1);
        printf("a)   Los pol\xA1ticos deben bajarse el sueldo y dejar de robar.\n");
        printf("b)   Los pol\xA1ticos deben conservar su sueldo y dejar de robar.\n");
        printf("c)   Es indistinto, ya que los pol\xA1ticos siempre roban\n");
        printf(">");
        scanf("%c", &opc);
        getchar();
        if(opc <= 'Z') {
            opc += ' ';
        }
        switch(opc) {
            case 'a': {
                a++;
                break;
            }
            case 'b': {
                b++;
                break;
            }
            case 'c': {
                c++;
                break;
            }
            default: {
                printf("Opci\xA2n invalida\n");
                break;
            }
        }
        i++;
    } while(i < NUM_PERSONAS);

    printf("Cantidad de votantes por la opci\xA2n a: %d\n", a);
    printf("Cantidad de votantes por la opci\xA2n b: %d\n", b);
    printf("Cantidad de votantes por la opci\xA2n c: %d\n", c);

    return 0;
}
