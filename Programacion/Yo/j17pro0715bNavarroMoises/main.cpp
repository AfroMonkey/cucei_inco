/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J17
 * Nombre de practica: J17
 * Fecha: 2015/10/12
 * Version: 1
 * Tiempo: 00:15
 */

#include <iostream>

using namespace std;

class Arbol {
    string especie;
    float altura;
    int edad;
public:
    void fijaEspecie(string especieX) {
        especie = especieX;
    }

    string dameEspecie() {
        return especie;
    }

    void fijaAltura(float alturaX) {
        altura = alturaX;
    }

    float dameAltura() {
        return altura;
    }

    void fijaEdad(int edadX) {
        edad = edadX;
    }

    int dameEdad() {
        return edad;
    }
};

class Libro {
    string nombre;
    char clasificacion;
    int codigo;
public:
    void fijaNombre(string nombreX) {
        nombre = nombreX;
    }

    string dameNombre() {
        return nombre;
    }

    void fijaClasificacion(char clasificacionX) {
        clasificacion = clasificacionX;
    }

    char dameClasificacion() {
        return clasificacion;
    }

    void fijaCodigo(int codigoX) {
        codigo = codigoX;
    }

    int dameCodigo() {
        return codigo;
    }
};

int main() {
    Arbol arbol;
    Libro libro;
    string auxString;
    float auxFloat;
    int auxInt;
    char auxChar;

    cout << "**Lectura de datos de arbol**" << endl;
    cout << "Especie>";
    getline(cin,auxString);
    arbol.fijaEspecie(auxString);
    cout << "Altura>";
    cin >> auxFloat;
    arbol.fijaAltura(auxFloat);
    cout << "Edad>";
    cin >> auxInt;
    arbol.fijaEdad(auxInt);
    cout << "**Lectura de datos de libro**" << endl;
    cout << "Nombre>";
    cin.get();
    getline(cin,auxString);
    libro.fijaNombre(auxString);
    cout << "Clasificacion>";
    cin >> auxChar;
    libro.fijaClasificacion(auxChar);
    cout << "Codigo>";
    cin >> auxInt;
    libro.fijaCodigo(auxInt);

    cout << "**Impresion de datos arbol**" << endl;
    cout << arbol.dameEspecie() << endl;
    cout << arbol.dameAltura() << endl;
    cout << arbol.dameEdad() << endl;
    cout << "**Impresion de datos libro**" << endl;
    cout << libro.dameNombre() << endl;
    cout << libro.dameClasificacion() << endl;
    cout << libro.dameCodigo() << endl;

    return 0;
}
