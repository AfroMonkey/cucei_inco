/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: T2
 * Nombre de practica: Tarea 2
 * Fecha: 2015/10/22
 * Version 3
 * Tiempo: 00:04
 */

#include <iostream>

using namespace std;

#define ENERO 1
#define FEBRERO 2
#define MARZO 3
#define ABRIL 4
#define MAYO 5
#define JUNIO 6
#define JULIO 7
#define AGOSTO 8
#define SEPTIEMBRE 9
#define OCTUBRE 10
#define NOVIEMBRE 11
#define DICIEMBRE 12

int esBisiesto(int anio);

int main() {
    int dia, mes, anio;
    string salida;
    dia = mes = anio = 0;

    cout << "VERIFICADOR DE FECHA v1.0" << endl;

    cout << "Dame el dia: ";
    cin >>dia;
    cout << "Dame el mes: ";
    cin >>mes;
    cout << "Dame el anio: ";
    cin >>anio;

    switch(mes) {
        case ENERO: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case FEBRERO: {
            if((dia >= 1 && dia <= 28) || (dia == 29 && esBisiesto(anio))) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case MARZO: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case ABRIL: {
            if(dia >= 1 && dia <= 30) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case MAYO: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case JUNIO: {
            if(dia >= 1 && dia <= 30) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case JULIO: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case AGOSTO: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case SEPTIEMBRE: {
            if(dia >= 1 && dia <= 30) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case OCTUBRE: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case NOVIEMBRE: {
            if(dia >= 1 && dia <= 30) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        case DICIEMBRE: {
            if(dia >= 1 && dia <= 31) {
                salida = "Fecha valida!";
            } else {
                salida = "Dia no valido";
            }
            break;
        }
        default: {
            salida = "Mes no valido!";  
            break;
        }
    }

    cout << salida << endl;
    
    return 0;
}

int esBisiesto(int anio) {
    return ((anio%4 == 0 && anio%100 != 0) || (anio%400 == 0));
}
