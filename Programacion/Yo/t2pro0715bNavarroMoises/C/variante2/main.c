/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: T2
 * Nombre de practica: Tarea 2
 * Fecha: 2015/10/22
 * Version 2
 * Tiempo: 00:04
 */

#include "stdio.h"

#define ENERO 1
#define FEBRERO 2
#define MARZO 3
#define ABRIL 4
#define MAYO 5
#define JUNIO 6
#define JULIO 7
#define AGOSTO 8
#define SEPTIEMBRE 9
#define OCTUBRE 10
#define NOVIEMBRE 11
#define DICIEMBRE 12

int esBisiesto(int anio);

int main() {
    int dia, mes, anio;
    char *salida;
    dia = mes = anio = 0;

    printf("VERIFICADOR DE FECHA v1.0\n");

    printf("Dame el dia: ");
    scanf("%d", &dia);
    printf("Dame el mes: ");
    scanf("%d", &mes);
    printf("Dame el anio: ");
    scanf("%d", &anio);

    if(mes==ENERO||mes==MARZO||mes==MAYO||mes==JULIO||mes==AGOSTO||mes==OCTUBRE||mes==DICIEMBRE) {
        if(dia>=1&&dia<=31) {
            salida = "Fecha valida!";
        } else {
            salida = "Dia no valido!";
        }
    } else if(mes == FEBRERO) {
        if((dia >= 1 && dia <= 28) || (dia == 29 && esBisiesto(anio))) {
            salida = "Fecha valida!";
        } else {
            salida = "Dia no valido!";
        }
    } else if(mes == ABRIL || mes == JUNIO || mes == SEPTIEMBRE || mes == NOVIEMBRE) {
        if(dia >= 1 && dia <= 30) {
            salida = "Fecha valida!";
        } else {
            salida = "Dia no valido!";            
        }
    } else {
        salida = "Mes no valido!";   
    }

    printf("%s\n", salida);
    
    return 0;
}

int esBisiesto(int anio) {
    return ((anio%4 == 0 && anio%100 != 0) || (anio%400 == 0));
}
