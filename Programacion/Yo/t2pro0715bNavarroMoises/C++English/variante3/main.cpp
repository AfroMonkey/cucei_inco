/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: T2
 * Nombre de practica: Tarea 2
 * Fecha: 2015/10/22
 * Version 3
 * Tiempo: 00:05
 */

#include <iostream>

using namespace std;

#define JANUARY 1
#define FEBRUARY 2
#define MARCH 3
#define APRIL 4
#define MAY 5
#define JUNE 6
#define JULY 7
#define AGUGUST 8
#define SEPTEMBER 9
#define OCTOBER 10
#define NOVEMBER 11
#define DECEMBER 12

int isLeapYear(int year);

int main() {
    int day, month, year;
    string output;
    day = month = year = 0;

    cout << "DATE VALIDATOR v1.0" << endl;

    cout << "Give me the day: ";
    cin >>day;
    cout << "Give me the month: ";
    cin >>month;
    cout << "Give me the year: ";
    cin >>year;

    switch(month) {
        case JANUARY: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case FEBRUARY: {
            if((day >= 1 && day <= 28) || (day == 29 && isLeapYear(year))) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case MARCH: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case APRIL: {
            if(day >= 1 && day <= 30) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case MAY: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case JUNE: {
            if(day >= 1 && day <= 30) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case JULY: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case AGUGUST: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case SEPTEMBER: {
            if(day >= 1 && day <= 30) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case OCTOBER: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case NOVEMBER: {
            if(day >= 1 && day <= 30) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        case DECEMBER: {
            if(day >= 1 && day <= 31) {
                output = "Valid date!";
            } else {
                output = "Invalid day!";
            }
            break;
        }
        default: {
            output = "Invalid month!!";  
            break;
        }
    }

    cout << output << endl;
    
    return 0;
}

int isLeapYear(int year) {
    return ((year%4 == 0 && year%100 != 0) || (year%400 == 0));
}
