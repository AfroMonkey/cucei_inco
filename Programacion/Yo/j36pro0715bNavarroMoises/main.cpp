/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J36
 * Nombre de practica: J36
 * Fecha: 2015/11/10
 * Version: 1
 * Tiempo: 00:05
 */

#include <iostream>

using namespace std;

#define CANTIDAD_ENTEROS 8

int main() {
    int enteros[CANTIDAD_ENTEROS];
    int acumulador;
    int i;

    i = 0;
    while(i < CANTIDAD_ENTEROS) {
        cout << "Entero #" << i+1 << ">";
        cin >> enteros[i++];
    }

    for(acumulador = i = 0; i < CANTIDAD_ENTEROS; i++) {
        acumulador += enteros[i];
    }

    cout << "Acumulador: " << acumulador << endl;
    return 0;
}
