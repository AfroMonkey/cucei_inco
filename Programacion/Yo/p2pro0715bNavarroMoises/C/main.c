/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: P2
 * Nombre de practica: Arreglos Vectores
 * Fecha: 2015/11/12
 * Version 1
 * Tiempo: 00:52
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

/*Declaracion y definicion de constates ----------------------------------------------------------*/
#define PORCENTAJE_IVA 16
#define PORCENTAJE_ISR 11
#define PORCENTAJE_RETENCION_ISR 10
#define PORCENTAJE_RETENCION_IVA 10
#define NUM_MESES 12
#define OPC_ESTABLECER_MES 1
#define OPC_CAPTURAR_INGRESOS 2
#define OPC_CAPTURAR_GASTOS 3
#define OPC_LISTAR_INGRESOS 4
#define OPC_LISTAR_GASTOS 5
#define OPC_CALCULAR_IMPUESTOS 6
#define OPC_SALIR 7

int main() {
    /*Declaracion de variables para entrada de datos ---------------------------------------------*/
    float ingresos[NUM_MESES];
    float gastos[NUM_MESES];
    int mesActual, mesSolicitado;
    int opcion;
    /*Declaracion de variables para computo ------------------------------------------------------*/
    float ingresosTotales, iva, subtotal, retencionIsr, retencionIva, total;
    float gastosTotales, gananciaBruta, isr, gananciaNeta;
    float pagoIsr;
    float gastosIva, pagoIva;
    int i;
    char* meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", 
      "Septiembre", "Octubre", "Noviembre", "Diciembre"};

    for(i = 0; i < NUM_MESES; i++) {
        ingresos[i] = gastos[i] = 0;
    }

    mesActual = 0;
    do {
        printf("C\x8FLCULO DE IMPUESTOS ANUAL\n\n");
        printf("Men\xA3 principal:\n");
        printf("1. Establecer mes para captura (mes actual es %s)\n", meses[mesActual]);
        printf("2. Captura de ingresos\n");
        printf("3. Captura de gastos\n");
        printf("4. Mostrar lista de ingresos anual\n");
        printf("5. Mostrar lista de gastos anual\n");
        printf("6. C\xA0lculo de impuestos anual\n");
        printf("7. Salir\n\n");
        printf("Opci\xA2n: ");
        scanf("%d", &opcion);
        getchar();

        switch(opcion) {
            /* Entrada de datos ------------------------------------------------------------------*/
            case OPC_ESTABLECER_MES:
                system(CLEAR);
                printf("Establecer mes para captura\n");
                for(i = 0; i < NUM_MESES; i++) {
                    printf("%d) %s\n", i+1, meses[i]);
                }
                printf("Elige el mes (1-12): ");
                scanf("%d", &mesSolicitado);
                getchar();
                if(mesSolicitado >= 1 && mesSolicitado <= NUM_MESES) {
                    mesActual = mesSolicitado-1;
                    printf("Se ha establecido el mes de captura en %s\n", meses[mesActual]);
                } else {
                    printf("Mes invalido, no se efectu\xA2 cambios\n");
                }
                break;
            case OPC_CAPTURAR_INGRESOS:
                printf("Captura de ingresos\n");
                printf("Dame el ingreso del mes de %s: ", meses[mesActual]);
                scanf("%f", &ingresos[mesActual]);
                getchar();
                break;
            case OPC_CAPTURAR_GASTOS:
                printf("Captura de gastos\n");
                printf("Dame el gasto del mes de %s: ", meses[mesActual]);
                scanf("%f", &gastos[mesActual]);
                getchar();
                break;
            case OPC_LISTAR_INGRESOS:
                system(CLEAR);
                printf("Mostrar lista de ingresos anual\n");
                for(i = 0; i < NUM_MESES; i++) {
                    printf("%s = %.2f\n", meses[i], ingresos[i]);
                }
                break;
            case OPC_LISTAR_GASTOS:
                system(CLEAR);
                printf("Mostrar lista de gastos anual\n");
                for(i = 0; i < NUM_MESES; i++) {
                    printf("%s = %.2f\n", meses[i], gastos[i]);
                }
                break;
            case OPC_CALCULAR_IMPUESTOS:
            /*Calculo de impuestos ---------------------------------------------------------------*/
                for(ingresosTotales = gastosTotales = i = 0; i < NUM_MESES; i++) {
                    ingresosTotales += ingresos[i];
                    gastosTotales += gastos[i];
                }

                iva = ingresosTotales * PORCENTAJE_IVA / 100;
                subtotal = ingresosTotales + iva;
                retencionIsr = ingresosTotales * PORCENTAJE_RETENCION_ISR / 100;
                retencionIva = ingresosTotales * PORCENTAJE_RETENCION_IVA / 100;
                total = subtotal - retencionIsr - retencionIva;
                gananciaBruta = ingresosTotales - gastosTotales;
                isr = gananciaBruta * PORCENTAJE_ISR / 100;
                gananciaNeta = gananciaBruta - isr;
                pagoIsr = isr - retencionIsr;
                gastosIva = gastosTotales * PORCENTAJE_IVA / 100;
                pagoIva = iva - gastosIva - retencionIva;

            /*Salida de resultados ---------------------------------------------------------------*/
                system(CLEAR);
                printf("C\xA0lculo de impuestos anual\n\n");
                printf("***Tabla Recibo de Honorarios***\n");
                printf("Ingresos \t\t %10.2f\n", ingresosTotales);
                printf("(+) IVA \t\t %10.2f\n", iva);
                printf("(=) Subtotal \t\t %10.2f\n", subtotal);
                printf("(-) Retenci\xA2n ISR \t %10.2f\n", retencionIsr);
                printf("(-) Retenci\xA2n IVA \t %10.2f\n", retencionIva);
                printf("(=) Total  \t\t %10.2f\n", total);

                printf("***Tabla Ganancias*** \n");
                printf("Ingresos \t\t %10.2f\n", ingresosTotales);
                printf("(-) Gastos  \t\t %10.2f\n", gastosTotales);
                printf("(=) Ganancia Bruta \t %10.2f\n", gananciaBruta);
                printf("(-) ISR  \t\t %10.2f\n", isr);
                printf("(=) Ganancia Neta  \t %10.2f\n", gananciaNeta);

                printf("***Tabla ISR*** \n");
                printf("ISR \t\t\t %10.2f\n", isr);
                printf("(-) ISR Retenido \t %10.2f\n", retencionIsr);
                printf("(=) ISR a Pagar \t %10.2f\n", pagoIsr);

                printf("***Tabla IVA*** \n");
                printf("IVA \t\t\t %10.2f\n", isr);
                printf("(-) Gastos IVA \t\t %10.2f\n", gastosIva);
                printf("(-) Retenci\xA2n IVA \t %10.2f\n", retencionIva);
                printf("(=) IVA a Pagar \t %10.2f\n", pagoIva);
                break;
            case OPC_SALIR:
                break;
            default:
                printf("Opci\xA2n invalida\n");
                break;
        }

        if(opcion != OPC_SALIR) {
            printf("Presione entrar para continuar . . .");
            getchar();
            system(CLEAR);
        }
    } while(opcion != OPC_SALIR);

    return 0;
}
