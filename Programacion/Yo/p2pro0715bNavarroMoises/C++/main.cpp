/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: P2
 * Nombre de practica: Arreglos Vectores
 * Fecha: 2015/11/12
 * Version 1
 * Tiempo: 00:10
 */

#include <iostream>
#include <cstdlib>

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

/*Declaracion y definicion de constates ----------------------------------------------------------*/
#define PORCENTAJE_IVA 16
#define PORCENTAJE_ISR 11
#define PORCENTAJE_RETENCION_ISR 10
#define PORCENTAJE_RETENCION_IVA 10
#define NUM_MESES 12
#define OPC_ESTABLECER_MES 1
#define OPC_CAPTURAR_INGRESOS 2
#define OPC_CAPTURAR_GASTOS 3
#define OPC_LISTAR_INGRESOS 4
#define OPC_LISTAR_GASTOS 5
#define OPC_CALCULAR_IMPUESTOS 6
#define OPC_SALIR 7

int main() {
    /*Declaracion de variables para entrada de datos ---------------------------------------------*/
    float ingresos[NUM_MESES];
    float gastos[NUM_MESES];
    int mesActual, mesSolicitado;
    int opcion;
    /*Declaracion de variables para computo ------------------------------------------------------*/
    float ingresosTotales, iva, subtotal, retencionIsr, retencionIva, total;
    float gastosTotales, gananciaBruta, isr, gananciaNeta;
    float pagoIsr;
    float gastosIva, pagoIva;
    int i;
    string meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", 
      "Septiembre", "Octubre", "Noviembre", "Diciembre"};

    for(i = 0; i < NUM_MESES; i++) {
        ingresos[i] = gastos[i] = 0;
    }

    mesActual = 0;
    do {
        cout << "C" << (char)0x8F << "LCULO DE IMPUESTOS ANUAL" << endl << endl;
        cout << "Men" << (char)0xA3 << " principal:" << endl;
        cout << "1. Establecer mes para captura (mes actual es "<< meses[mesActual] <<")" << endl;
        cout << "2. Captura de ingresos" << endl;
        cout << "3. Captura de gastos" << endl;
        cout << "4. Mostrar lista de ingresos anual" << endl;
        cout << "5. Mostrar lista de gastos anual" << endl;
        cout << "6. C" << (char)0xA0 << "lculo de impuestos anual" << endl;
        cout << "7. Salir\n" << endl;
        cout << "Opci" << (char)0xA2 << "n: ";
        cin >> opcion;
        cin.ignore();

        switch(opcion) {
            /* Entrada de datos ------------------------------------------------------------------*/
            case OPC_ESTABLECER_MES:
                system(CLEAR);
                cout << "Establecer mes para captura" << endl;
                for(i = 0; i < NUM_MESES; i++) {
                    cout << i+1 << ")" << meses[i] << endl;
                }
                cout << "Elige el mes (1-12): ";
                cin >> mesSolicitado;
                cin.ignore();
                if(mesSolicitado >= 1 && mesSolicitado <= NUM_MESES) {
                    mesActual = mesSolicitado-1;
                    cout << "Se ha establecido el mes de captura en " << meses[mesActual] << endl;
                } else {
                    cout << "Mes invalido, no se efectu" << (char)0xA2 << " cambios" << endl;
                }
                break;
            case OPC_CAPTURAR_INGRESOS:
                cout << "Captura de ingresos" << endl;
                cout << "Dame el ingreso del mes de " << meses[mesActual] << ": ";
                cin >> ingresos[mesActual];
                cin.ignore();
                break;
            case OPC_CAPTURAR_GASTOS:
                cout << "Captura de gastos" << endl;
                cout << "Dame el gasto del mes de " << meses[mesActual] <<": ";
                cin >> gastos[mesActual];
                cin.ignore();
                break;
            case OPC_LISTAR_INGRESOS:
                system(CLEAR);
                cout << "Mostrar lista de ingresos anual" << endl;
                for(i = 0; i < NUM_MESES; i++) {
                    cout << meses[i] << " = " << ingresos[i] << endl;
                }
                break;
            case OPC_LISTAR_GASTOS:
                system(CLEAR);
                cout << "Mostrar lista de gastos anual" << endl;
                for(i = 0; i < NUM_MESES; i++) {
                    cout << meses[i] << " = " << gastos[i] << endl;
                }
                break;
            case OPC_CALCULAR_IMPUESTOS:
            /*Calculo de impuestos ---------------------------------------------------------------*/
                for(ingresosTotales = gastosTotales = i = 0; i < NUM_MESES; i++) {
                    ingresosTotales += ingresos[i];
                    gastosTotales += gastos[i];
                }

                iva = ingresosTotales * PORCENTAJE_IVA / 100;
                subtotal = ingresosTotales + iva;
                retencionIsr = ingresosTotales * PORCENTAJE_RETENCION_ISR / 100;
                retencionIva = ingresosTotales * PORCENTAJE_RETENCION_IVA / 100;
                total = subtotal - retencionIsr - retencionIva;
                gananciaBruta = ingresosTotales - gastosTotales;
                isr = gananciaBruta * PORCENTAJE_ISR / 100;
                gananciaNeta = gananciaBruta - isr;
                pagoIsr = isr - retencionIsr;
                gastosIva = gastosTotales * PORCENTAJE_IVA / 100;
                pagoIva = iva - gastosIva - retencionIva;

            /*Salida de resultados ---------------------------------------------------------------*/
                system(CLEAR);
                cout << "C" << (char)0xB5 << "LCULO DE IMPUESTOS" << endl;

                cout << "***Tabla Recibo de Honorarios*** " << endl;
                cout << "Ingresos " << ingresosTotales << endl;
                cout << "(+) IVA " << iva << endl;
                cout << "(=) Subtotal " << subtotal << endl;
                cout << "(-) Retenci" << (char)0xA2 << "n ISR " << retencionIsr << endl;
                cout << "(-) Retenci" << (char)0xA2 << "n IVA " << retencionIva << endl;
                cout << "(=) Total  " << total <<endl;

                cout << "***Tabla Ganancias*** " << endl;
                cout << "Ingresos " << ingresosTotales << endl;
                cout << "(-) Gastos  " << gastosTotales <<endl;
                cout << "(=) Ganancia Bruta " << gananciaBruta << endl;
                cout << "(-) ISR  " << isr << endl;
                cout << "(=) Ganancia Neta " << gananciaNeta << endl;

                cout << "***Tabla ISR*** " << endl;
                cout << "ISR " << isr << endl;
                cout << "(-) ISR Retenido " << retencionIsr << endl;
                cout << "(=) ISR a Pagar " << pagoIsr << endl;

                cout << "***Tabla IVA*** " << endl;
                cout << "IVA " << isr << endl;
                cout << "(-) Gastos IVA " << gastosIva << endl;
                cout << "(-) Retenci" << (char)0xA2 << "n IVA " << retencionIva << endl;
                cout << "(=) IVA a Pagar " << pagoIva << endl;
                break;
            case OPC_SALIR:
                break;
            default:
                cout << "Opci" << (char)0xA2 << "n invalida" << endl;
                break;
        }

        if(opcion != OPC_SALIR) {
            cout << "Presione entrar para continuar . . .";
            cin.ignore();
            system(CLEAR);
        }
    } while(opcion != OPC_SALIR);

    return 0;
}
