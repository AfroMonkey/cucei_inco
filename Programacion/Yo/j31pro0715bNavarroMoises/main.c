/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J31
 * Nombre de practica: J31
 * Fecha: 2015/11/04
 * Version 1
 * Tiempo: 00:05
 */

#include "stdio.h"

int main() {
    int a, b;
    int c;
    char opc;

    printf("Escribe el operador (+,-,*,/,%%): ");
    scanf("%c", &opc);
    printf("Dame el valor de a=");
    scanf("%d", &a);
    printf("Dame el valor de b=");
    scanf("%d", &b);

    switch(opc) {
        case '+':
            c = a+b;
            break;
        case '-':
            c = a-b;
            break;
        case '*':
            c = a*b;
            break;
        case '/':
            c = a/b;
            break;
        case '%':
            c = a%b;
            break;
        default:
            c = -1;
            printf("Operador invalido\n");
            break;
    }
    
    printf("%d %c %d = %d\n", a, opc, b, c);

    return 0;
}
