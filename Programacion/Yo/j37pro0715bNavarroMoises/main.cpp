/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J37
 * Nombre de practica: J37
 * Fecha: 2015/11/11
 * Version: 1
 * Tiempo: 00:05
 */

#include <iostream>

using namespace std;

#define NUM_ENTEROS 10

int main() {
    int enteros[NUM_ENTEROS];
    int multiplicador;
    int i;

    i = 0;
    do {
        cout << "Valor de casilla #" << i+1 <<">";
        cin >> enteros[i];
        i++;
    } while(i < NUM_ENTEROS);

    cout << "Multiplicador>";
    cin >> multiplicador;

    i = 0;
    while(i < NUM_ENTEROS) {
        enteros[i] *= multiplicador;
        i++;
    }

    for(i = 0; i < NUM_ENTEROS; i++) {
        cout << "Valor de casilla #" << i+1 << " = " << enteros[i] << endl;
    }

    return 0;
}
