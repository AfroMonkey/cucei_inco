/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J22
 * Nombre de practica: J22
 * Fecha: 2015/10/21
 * Version: 1
 * Tiempo: 00:03
 */

#include <iostream>

using namespace std;

int main() {
    int anio;
    string esBisiesto;

    cout << "A\xA4o >";
    cin >> anio;

    if((anio%4 == 0 && anio%100 != 0) || (anio%400 == 0)) {
        esBisiesto = "si";
    } else {
        esBisiesto = "no";
    }
    
    cout << "El a\xA4o " << anio << " " << esBisiesto << " es bisiesto." << endl;

    return 0;
}
