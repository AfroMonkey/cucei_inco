/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J13
 * Nombre de practica: J13
 * Fecha: 2015/10/08
 * Version 1
 * Tiempo: 00:05
 */

#include "stdio.h"

#define ALUMNO_N_CARACTERES_NOMBRE 50

typedef struct {
    char nombre[ALUMNO_N_CARACTERES_NOMBRE];
    int codigo;
    float calificacion;
}Alumno;

int main() {
    Alumno alumno;

    printf("**Captura de datos**\n");
    printf("Nombre >");
    gets(alumno.nombre);
    printf("C%cdigo >", 0xA2);
    scanf("%d", &alumno.codigo);
    printf("Calificaci%cn >", 0xA2);
    scanf("%f", &alumno.calificacion);

    printf("\n**Muestreo de datos**\n");
    printf("Nombre: %s\n", alumno.nombre);
    printf("C%cdigo: %d\n", 0xA2, alumno.codigo);
    printf("Calificaci%cn: %.2f\n", 0xA2, alumno.calificacion);

    return 0;
}
