/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J32
 * Nombre de practica: J32
 * Fecha: 2015/11/05
 * Version 1
 * Tiempo: 00:03
 */

#include "stdio.h"

int main() {
    int numPersonas;
    int i;

    printf("Numero de personas>");
    scanf("%d", &numPersonas);

    if(numPersonas <= 0) {
        printf("no hay nadie\n");
    } else {
        i = 0;
        while(i < numPersonas) {
            printf("Hola persona #%d, te saludo\n", ++i);
        }
    }
    
    return 0;
}
