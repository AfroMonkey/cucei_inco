/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J32
 * Nombre de practica: J32
 * Fecha: 2015/11/05
 * Version 1
 * Tiempo: 00:02
 */

#include "stdio.h"

int main() {
    int numPersonas;
    int i;

    printf("Numero de personas>");
    scanf("%d", &numPersonas);

    if(numPersonas <= 0) {
        printf("no hay nadie\n");
    } else {
        for(i = 1; i <= numPersonas; i++) {
            printf("Hola perona #%d, te saludo\n", i);
        }
    }

    return 0;
}
