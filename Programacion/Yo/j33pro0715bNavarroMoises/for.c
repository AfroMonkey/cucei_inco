/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J33
 * Nombre de practica: J33
 * Fecha: 2015/11/06
 * Version 2
 * Tiempo 00:02
 */

#include "stdio.h"

int main() {
    int numPersonas;
    float estatura, edad;
    float estaturaPromedio, edadPromedio;
    int i;

    printf("Numero de peronas>");
    scanf("%d", &numPersonas);

    if(numPersonas > 0) {
        for(estaturaPromedio = edadPromedio = i = 0; i < numPersonas; i++) {
            printf("Estatura de persona #%d>", i+1);
            scanf("%f", &estatura);
            printf("Edad de persona #%d>", i+1);
            scanf("%f", &edad);

            estaturaPromedio += estatura;
            edadPromedio += edad;
        }
        estaturaPromedio /= numPersonas;
        edadPromedio /= numPersonas;

        printf("Estatura promedio %.2f\n", estaturaPromedio);
        printf("Edad promedio %.2f\n", edadPromedio);
    } else {
        printf("Numero de personas invalido\n");
    }

    return 0;
}
