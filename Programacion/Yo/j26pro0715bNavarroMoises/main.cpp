/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J26
 * Nombre de practica: J26
 * Fecha: 2015/10/27
 * Version: 1
 * Tiempo: 00:06
 */

#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int x;

    x = -1;

    cout << "X>";
    cin >> x;

    if(x >= 0) {
        x *= x;
        x += (x%2 == 0)?5:-5;
    } else {
        x *= x*x;
        x = abs(x);
        x += (x > 100)?100:-100;
    }

    cout << "X: " << x << endl;
    return 0;
}
