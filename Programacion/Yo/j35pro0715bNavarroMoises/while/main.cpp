/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J35
 * Nombre de practica: J35
 * Fecha: 2015/11/08
 * Version: 2 - While
 * Tiempo: 00:01
 */

#include <iostream>

using namespace std;

int main() {
    int multiplicando, maxMultiplicador;
    int i;

    cout << "Dame el multiplicando = ";
    cin >> multiplicando;
    cout << "Dame hasta cual multiplicador = ";
    cin >>maxMultiplicador;
    cin.ignore();

    cout << "Tabla de multiplicar del " << multiplicando << "..." << endl;

    if(maxMultiplicador >= 1) {
        i = 1;
        while(i <= maxMultiplicador) {
            cout<<multiplicando<<"\t"<<"X"<<"\t"<<i<<"\t"<<"="<<"\t"<<multiplicando*i<<endl;
            i++;
        }
        cout << "Presione entrar para continuar . . . ";
        cin.ignore();
    } else {
        cout << "Limite invalido" << endl;
    }
    return 0;
}
