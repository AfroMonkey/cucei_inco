/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: A7
 * Nombre de practica: Actividad VII. Control de Flujo del Programa - Seleccion y Validacion
 * Fecha: 2015/11/10
 * Version: 1
 * Tiempo: 01:25
 */

#include <iostream>
#include <cstdlib>

#include "constantes.h"
#include "utilidades.h"
#include "Materia.h"
#include "ControlAsistencia.h"
#include "Evaluador.h"

using namespace std;

void pausarLimpiar();
void capturarMateria(Materia &materia);
void capturarAsistencias(ControlAsistencia &controlAsistencia);
void capturarEvaluandos(Evaluador &evaluador);
void capturarTareas(Evaluador &evaluador);
void capturarActividades(Evaluador &evaluador);
void capturarExamenes(Evaluador &evaluador);
void computarEImprimir(ControlAsistencia &controlAsistencia, Evaluador &evaluador);

int main() {
    Materia materia;
    ControlAsistencia controlAsistencia;
    Evaluador evaluador;

    /* Entrada de datos --------------------------------------------------------------------------*/
    capturarMateria(materia);
    pausarLimpiar();

    cout << "Evaluador del Curso v1.0 - " << materia.dameNombre() << " (" << materia.dameAcronimo() 
      << ") " << materia.dameDiasSemana() << " " << materia.dameHoraInicio() << "a" << 
      materia.dameHoraFin() << endl << endl; 
    capturarAsistencias(controlAsistencia);
    capturarEvaluandos(evaluador);

    computarEImprimir(controlAsistencia, evaluador);
    pausarLimpiar();
    
    return 0;
}

void pausarLimpiar() {
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
    system(CLEAR);
}

void capturarMateria(Materia &materia) {
    string acronimo, nombre, profesor, diasSemana;
    int horaInicio, horaFin;

    cout << "*Captura de materia*" << endl;
    cout << "Acr" << (char)0xA2 << "nimo>";
    cin >> acronimo;
    cin.ignore();
    cout << "Nombre>";
    getline(cin, nombre);
    cout << "Profesor>";
    getline(cin, profesor);
    cout << "D" << (char)0xA1 << "as de la semana>";
    getline(cin, diasSemana);
    cout << "Hora inicio>";
    cin >> horaInicio;
    cout << "Hora de fin>";
    cin >> horaFin;
    cin.ignore();

    materia.fijaNombre(nombre);
    materia.fijaAcronimo(acronimo);
    materia.fijaDiasSemana(diasSemana);
    materia.fijaHoraInicio(horaInicio);
    materia.fijaHoraFin(horaFin);
}

void capturarAsistencias(ControlAsistencia &controlAsistencia) {
    int cantidadFaltas, cantidadRetardos;

    do {
        cout << "Cu" << (char)0xA0 << "ntas faltas acumulaste? ";
        cin >> cantidadFaltas;
    } while(!controlAsistencia.fijaCantidadFaltas(cantidadFaltas));
    do {
        cout << "Cu" << (char)0xA0 << "ntos retardos acumulaste? ";
        cin >> cantidadRetardos;
    } while(!controlAsistencia.fijaCantidadRetardos(cantidadRetardos));
    cin.ignore();
}

void capturarEvaluandos(Evaluador &evaluador) {
    capturarTareas(evaluador);
    capturarActividades(evaluador);
    capturarExamenes(evaluador);
}

void capturarTareas(Evaluador &evaluador) {
    int cantidadTareasRealizadas;

    do {
        cout << "Cu" << (char)0xA0 << "ntas tareas realizaste? ";
        cin >> cantidadTareasRealizadas;
    } while(!evaluador.fijaCantidadTareasRealizadas(cantidadTareasRealizadas));
    cin.ignore();
}

void capturarActividades(Evaluador &evaluador) {
    int calificacion;
    int i;

    for(i = 0; i < CANTIDAD_ACTIVIDADES; i++) {
        do {
            cout << "Cu" << (char)0xA0 << "nto obtuviste en la actividad #" << i+1 <<"? ";
            cin >> calificacion;
        } while(!evaluador.fijaCalificacionActividad(i, calificacion));
    }
    cin.ignore();
}

void capturarExamenes(Evaluador &evaluador) {
    int calificacion;
    int i;

    for(i = 0; i < CANTIDAD_EXAMENES; i++) {
        do {
            cout << "Cu" << (char)0xA0 << "nto obtuviste en el " << i+1 << (char)0xA7 << 
              " examen parcial? ";
            cin >> calificacion;
        } while(!evaluador.fijaCalificacionExamen(i, calificacion));
    }
    cin.ignore();
}

void computarEImprimir(ControlAsistencia &controlAsistencia, Evaluador &evaluador) {
    float asistenciasTotales, porcentajeAsistencia;
    bool tieneDerecho;

    float puntosFinalesTareas, puntosFinalesActividades, puntosFinalesExamenes, calificacionFinal;
    bool tieneCalificacionAprobatoria;

    string strDerecho, strCalApro, strAprobado;

    /* Computo de asistencias --------------------------------------------------------------------*/
    asistenciasTotales = controlAsistencia.dameAsistenciasTotales();
    porcentajeAsistencia = controlAsistencia.damePorcentajeAsistencia();
    tieneDerecho = controlAsistencia.tieneDerecho();
    /* Computo de tareas -------------------------------------------------------------------------*/
    puntosFinalesTareas = evaluador.damePuntosFinalesTareas();
    /* Computo de actividades --------------------------------------------------------------------*/
    puntosFinalesActividades = evaluador.damePuntosFinalesActividades();
    /* Computo de examenes parciales -------------------------------------------------------------*/
    puntosFinalesExamenes = evaluador.damePuntosFinalesExamenes();
    /* Computo de puntos finales -----------------------------------------------------------------*/
    calificacionFinal = evaluador.dameCalificacionFinal();
    tieneCalificacionAprobatoria = evaluador.tieneCalificacionAprobatoria();

    strDerecho = tieneDerecho?"Tiene":"No tiene";
    strCalApro = tieneCalificacionAprobatoria?"":"no";
    strAprobado = tieneDerecho && tieneCalificacionAprobatoria?"":"no";


    /* Salida de datos ---------------------------------------------------------------------------*/
    cout << "Tareas\tActividades\tEx" << (char)0xA0 << "menes" << endl;
    cout << puntosFinalesTareas << "\t" << puntosFinalesActividades << "\t\t" << 
      puntosFinalesExamenes << endl;
    cout << "Total de asistencias= \t\t" << asistenciasTotales << endl;
    cout << "Porcentaje de asistencias= \t" << porcentajeAsistencia;
    cout << "\t" << strDerecho << " derecho" << endl;
    cout << "Calificaci" << (char)0xA2 << "n Final= \t\t" << calificacionFinal; 
    cout << "\tCalificaci" << (char)0xA2 << "n " << strCalApro << " aprobatoria" << endl;
    cout << "Alumn@ " << strAprobado << " aprobado" << endl;
}
