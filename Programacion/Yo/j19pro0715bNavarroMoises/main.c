/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J19
 * Nombre de practica: J19
 * Fecha: 2015/10/15
 * Version 1
 * Tiempo: 00:03
 */

#include "stdio.h"

int main() {
    int a, b, c;

    printf("Primer numero>");
    scanf("%d", &a);
    printf("Segundo numero>");
    scanf("%d", &b);
    printf("Tercer numero>");
    scanf("%d", &c);

    if(a < b && b < c) {
        printf("est%cn en orden creciente\n", 0xA0);
    } else {
        printf("no est%cn en orden creciente\n", 0xA0);
    }
    return 0;
}
