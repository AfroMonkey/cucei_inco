/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J16
 * Nombre de practica: J16
 * Fecha: 2015/10/11
 * Version: 1
 * Tiempo: 00:10
 */

#include <iostream>

using namespace std;

class Alumno {
    string nombre;
    int codigo;
    float calificacion;
public:
    void fijaNombre(string nombreX) {
        nombre = nombreX;
    }

    string dameNombre() {
        return nombre;
    }

    void fijaCodigo(int codigoX) {
        codigo = codigoX;
    }

    int dameCodigo() {
        return codigo;
    }

    void fijaCalificacion(float calificacionX) {
        calificacion = calificacionX;
    }

    float dameCalificacion() {
        return calificacion;
    }

};

int main() {
    Alumno alumno;
    string nombreX;
    int codigoX;
    float calificacionX;

    cout << "**Captura de datos**" << endl;
    cout << "Nombre>";
    getline(cin, nombreX);
    alumno.fijaNombre(nombreX);
    cout << "Codigo>";
    cin >> codigoX;
    alumno.fijaCodigo(codigoX);
    cout << "Calificacion>";
    cin >> calificacionX;
    alumno.fijaCalificacion(calificacionX);

    cout << "**Salida de datos**" << endl;
    cout << "Nombre:" << alumno.dameNombre() << endl;
    cout << "Codigo:" << alumno.dameCodigo() << endl;
    cout << "Calificacion:" << alumno.dameCalificacion() << endl;

    return 0;
}
