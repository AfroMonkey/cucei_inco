/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: J11
 * Nombre de la practica: J11
 * Fecha: 2015/09/08
 * Versión 1
 * Tiempo: 00:05
 */
#include <iostream>

using namespace std;

int main() {
	int a, b, c, aux;

	cout << "a >";	
	cin >> a;

	cout << "b >";	
	cin >> b;

	cout << "c >";	
	cin >> c;

	cout << "Originales :" << endl;
	cout << "a: " << a << endl;
	cout << "b: " << b << endl;
	cout << "c: " << c << endl;

	cout << "Intercambiando" << endl;
	aux = a;
	a = c;
	c = b;
	b = aux;

	cout << "a: " << a << endl;
	cout << "b: " << b << endl;
	cout << "c: " << c << endl;

	return 0;
}