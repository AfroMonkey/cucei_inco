/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J29
 * Nombre de practica: J29
 * Fecha: 2015/10/30
 * Version 1
 * Tiempo: 00:02
 */

#include "stdio.h"

int main() {
    char c;

    c = 'a';

    do {
        printf("%c%c\n", c-32, c);
        c++;
    } while (c <= 'z');

    return 0;
}
