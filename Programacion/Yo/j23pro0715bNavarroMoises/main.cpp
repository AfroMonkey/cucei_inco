/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J23
 * Nombre de practica: J23
 * Fecha: 2015/10/23
 * Version: 1
 * Tiempo 00:05
 */

#include <iostream>

using namespace std;

#define SUMA 1
#define RESTA 2
#define MULTIPLICACION 3
#define DIVISION 4

int main() {
    float a, b, c;
    int opc;

    opc = -1;
    cout << "1) Suma a+b" << endl;
    cout << "2) Resta a-b" << endl;
    cout << "3) Multiplicacion a*b" << endl;
    cout << "4) Division a/b" << endl;
    cout << ">";
    cin >> opc;

    if(opc >= SUMA && opc <= DIVISION) {
        cout << "a>";
        cin>>a;
        cout << "b>";
        cin>>b;
    }

    switch(opc) {
        case SUMA: {
            c = a+b;
            break;
        }
        case RESTA: {
            c = a-b;
            break;
        }
        case MULTIPLICACION: {
            c = a*b;
            break;
        }
        case DIVISION: {
            c = a/b;
            break;
        }
        default: {
            c = -1;
            cout << "Opcion invalida" << endl;
            break;
        }
    }

    cout << "C = " << c << endl;

    return 0;
}
