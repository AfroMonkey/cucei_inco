#ifndef TAREA_INCLUDED
#define TAREA_INCLUDED

class Tarea {
    string asunto;
    string descripcion;
    string materia;
public:
    Tarea() {
        asunto = "Staff";
        descripcion = "Staff";
        materia = "Staff";
    }

    void fijaAsunto(string asuntoX) {
        asunto = asuntoX;
    }

    string dameAsunto() {
        return asunto;
    }

    void fijaDescripcion(string descripcionX) {
        descripcion = descripcionX;
    }

    string dameDescripcion() {
        return descripcion;
    }

    void fijaMateria(string materiaX) {
        materia = materiaX;
    }

    string dameMateria() {
        return materia;
    }
};

#endif
