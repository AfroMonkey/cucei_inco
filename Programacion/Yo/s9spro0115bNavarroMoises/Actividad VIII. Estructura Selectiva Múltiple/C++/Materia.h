#ifndef MATERIA_INCLUDED
#define MATERIA_INCLUDED

using namespace std;

class Materia {
    string acronimo;
    string nombre;
    string profesor;
    string diasSemana;
    int horaInicio;
    int horaFin;
public:
    Materia() {
        nombre = "Staff";
        acronimo = "Staff";
        diasSemana = "Staff";
        horaInicio = -1;
        horaFin = -1;
    }

    void fijaAcronimo(string acronimoX) {
        acronimo = acronimoX;
    }

    string dameAcronimo() {
        return acronimo;
    }

    void fijaNombre(string nombreX) {
        nombre = nombreX;
    }

    string dameNombre() {
        return nombre;
    }

    void fijaProfesor(string profesorX) {
        profesor = profesorX;
    }

    string dameProfesor() {
        return profesor;
    }

    void fijaDiasSemana(string diasSemanaX) {
        diasSemana = diasSemanaX;
    }

    string dameDiasSemana() {
        return diasSemana;
    }

    void fijaHoraInicio(int horaInicioX) {
        horaInicio = horaInicioX;
    }

    int dameHoraInicio() {
        return horaInicio;
    }

    void fijaHoraFin(int horaFinX) {
        horaFin = horaFinX;
    }

    int dameHoraFin() {
        return horaFin;
    }
};

#endif
