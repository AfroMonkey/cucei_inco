#ifndef NOTA_INCLUDED
#define NOTA_INCLUDED

class Nota {
    string descripcion;
public:
    Nota() {
        descripcion = "Staff";
    }

    void fijaDescripcion(string descripcionX) {
        descripcion = descripcionX;
    }

    string dameDescripcion() {
        return descripcion;
    }
};

#endif