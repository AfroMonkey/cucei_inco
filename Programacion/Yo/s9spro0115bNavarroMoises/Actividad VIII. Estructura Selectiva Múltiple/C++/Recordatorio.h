#ifndef RECORDATORIO_INCLUDED
#define RECORDATORIO_INCLUDED

class Recordatorio {
    string fecha;
    string hora;
    string asunto;
    char tipo;
public:
    Recordatorio() {
        fecha = "Staff";
        hora = "Staff";
        asunto = "Staff";
        tipo = 'X';
    }

    void fijaFecha(string fechaX) {
        fecha = fechaX;
    }

    string dameFecha() {
        return fecha;
    }

    void fijaHora(string horaX) {
        hora = horaX;
    }

    string dameHora() {
        return hora;
    }

    void fijaAsunto(string asuntoX) {
        asunto = asuntoX;
    }

    string dameAsunto() {
        return asunto;
    }

    void fijaTipo(char tipoX) {
        tipo = tipoX;
    }

    char dameTipo() {
        return tipo;
    }
};

#endif
