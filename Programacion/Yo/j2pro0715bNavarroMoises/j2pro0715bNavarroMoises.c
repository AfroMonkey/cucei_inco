/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 2
 * Name of practice: Area of a triangle
 * Date: 2015/08/26
 * Version 1
 */
 
#include "stdio.h"

int main() {
	int areaTriangulo, base, altura;

	/* Default values */
	base = 3;
	altura = 4;

	printf("base >");
	scanf("%d", &base);

	printf("height >");	
	scanf("%d", &altura);

	areaTriangulo = (base * altura) / 2;
	printf("area: %d.%d", areaTriangulo, (base%2 && altura%2)?5:0);

	return 0;
}