/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 4
 * Name of practice: Area of Circle
 * Date: 2015/08/28
 * Version 1
 */
 
#include "stdio.h"

int main() {
	float areaCirculo, radio, pi;
	pi = 3.1416;
	radio = 10.5;
	areaCirculo = pi * (radio*radio);	
	printf("Radio : %f\n", radio);
	printf("Area: %f\n", areaCirculo);
	return 0;
}