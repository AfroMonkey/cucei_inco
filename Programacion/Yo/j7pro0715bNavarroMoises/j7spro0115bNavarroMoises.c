/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 7
 * Name of practice: Area of Circle (scanf)
 * Date: 2015/09/02
 * Version 1
 * Timepo: 00:02
 */

#include "stdio.h"

#define PI 3.1416

int main() {
	printf("/*Calcular el area de un circulo*/\n");

	float areaCirculo, radio;

	printf("Radio >");
	scanf("%f", &radio);

	areaCirculo = PI * radio * radio;

	printf("Area del circulo: %f\n", areaCirculo);
	
	return 0;
}