/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: T1
 * Nombre de la practica: Tarea 1
 * Fecha: 2015/09/03
 * Versión 1
 * Tiempo: 00:13
 */
#include "stdio.h"

 #define PORCENTAJE_IVA 16
 #define PORCENTAJE_RETENCION_ISR 10
 #define PORCENTAJE_RETENCION_IVA 10

int main() {
	int ingreso, iva, subtotal, retencionIsr, retencionIva, total;

	printf("CALCULO DE IMPUESTOS\n\n");
	
	printf("Dame el ingreso: ");
	scanf("%d", &ingreso);

	printf("CALCULO DE IMPUESTOS\n\n");
	printf("***Tabla Recibo de Honorarios***\n");

	printf("Ingresos \t\t %d\n", ingreso);

	iva = ingreso * PORCENTAJE_IVA / 100;
	printf("(+) IVA \t\t %d\n", iva);

	subtotal = ingreso + iva;
	printf("(=) Subtotal \t\t %d\n", subtotal);

	retencionIsr = ingreso * PORCENTAJE_RETENCION_ISR / 100;
	printf("(-) Retenci\xA2n ISR \t %d\n", retencionIsr);

	retencionIva = ingreso * PORCENTAJE_RETENCION_IVA / 100;
	printf("(-) Retenci\xA2n IVA \t %d\n", retencionIva);

	total = subtotal - retencionIsr - retencionIva;
	printf("(=) Total \t\t %d\n", total);

	return 0;
}