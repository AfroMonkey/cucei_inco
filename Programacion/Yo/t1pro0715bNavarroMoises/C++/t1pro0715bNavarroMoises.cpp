/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: T1
 * Nombre de la practica: Tarea 1
 * Fecha: 2015/09/03
 * Versión 1
 * Tiempo: 00:04
 */
#include <iostream>

 using namespace std;

 #define PORCENTAJE_IVA 16
 #define PORCENTAJE_RETENCION_ISR 10
 #define PORCENTAJE_RETENCION_IVA 10

int main() {
	int ingreso, iva, subtotal, retencionIsr, retencionIva, total;

	cout<<"CALCULO DE IMPUESTOS"<<endl<<endl;
	
	cout<<"Dame el ingreso: ";
	cin>>ingreso;

	cout<<"CALCULO DE IMPUESTOS"<<endl<<endl;
	cout<<"***Tabla Recibo de Honorarios***"<<endl;

	cout<<"Ingresos \t\t "<<ingreso<<endl;

	iva = ingreso * PORCENTAJE_IVA / 100;
	cout<<"(+) IVA \t\t "<<iva<<endl;

	subtotal = ingreso + iva;
	cout<<"(=) Subtotal \t\t "<<subtotal<<endl;

	retencionIsr = ingreso * PORCENTAJE_RETENCION_ISR / 100;
	cout<<"(-) Retenci\xA2n ISR \t "<<retencionIsr<<endl;

	retencionIva = ingreso * PORCENTAJE_RETENCION_IVA / 100;
	cout<<"(-) Retenci\xA2n IVA \t "<<retencionIva<<endl;

	total = subtotal - retencionIsr - retencionIva;
	cout<<"(=) Total \t\t "<<total<<endl;

	return 0;
}