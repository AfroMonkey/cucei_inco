/*
 * Student: Navarro Presas Moisés Alejandro
 * Number of practice: T1
 * Name of practice: Tarea 1
 * Date: 2015/09/03
 * Version 1
 * Tiempo: 00:05
 */
#include "stdio.h"

 #define TAX_PERCENT 16
 #define RETENTION_ISR_PERCENT 10
 #define RETENTION_TAX_PERCENT 10

int main() {
	int income, tax, subtotal, isrRetention, taxRetention, total;

	printf("TAX CALCULATION\n\n");
	
	printf("Give me the income: ");
	scanf("%d", &income);

	printf("TAX CALCULATION\n\n");
	printf("***Bill table***\n");

	printf("Incomes \t\t %d\n", income);

	tax = income * TAX_PERCENT / 100;
	printf("(+) TAX \t\t %d\n", tax);

	subtotal = income + tax;
	printf("(=) Subtotal \t\t %d\n", subtotal);

	isrRetention = income * RETENTION_ISR_PERCENT / 100;
	printf("(-) Retention ISR \t %d\n", isrRetention);

	taxRetention = income * RETENTION_TAX_PERCENT / 100;
	printf("(-) Retention TAX \t %d\n", taxRetention);

	total = subtotal - isrRetention - taxRetention;
	printf("(=) Total \t\t %d\n", total);

	return 0;
}