/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: Q2
 * Nombre de practica: Actividad II. Variables, Constantes, Entrada y Salida de Datos 
 * Fecha: 2015/09/24
 * Version 1
 * Tiempo: 01:07
 */

#include "stdio.h"

#define CARACTERES_NOMBRE_MATERIA 25
#define CARACTERES_ACRONIMO_MATERIA 5

//Los porcentajes a considerar para la evaluacion del curso 
#define PORCENTAJE_TAREAS (float) 7 / 100
#define PORCENTAJE_ACTIVIDADES (float) 63 / 100
#define PORCENTAJE_EXAMENES_PARCIALES (float) 30 / 100
//Declaracion y definicion de constantes
#define TOTAL_ASISTENCIAS 34
#define RETARDOS_POR_FALTA 3
#define TOTAL_TAREAS 10
#define TOTAL_ACTIVIDADES 9
#define TOTAL_EXAMENES_PARCIALES 3

typedef struct {
    //Declaracion y definicion por defecto de variables (a usar para entrada de datos) 
    char nombre[CARACTERES_NOMBRE_MATERIA + 1];
    char acronimo[CARACTERES_ACRONIMO_MATERIA + 1];
    char diasSemana[7];
    int horaInicio;
    int horaFinalizacion;

    int cantidadFaltas, cantidadRetardos, cantidadTareasRealizadas;
    int calificacionesActividades[TOTAL_ACTIVIDADES];
    int calificacionesExamenesParciales[TOTAL_EXAMENES_PARCIALES];
}Materia;

int main() {
    //Declaracion de variables (para computo) 
    float puntosFinalesTareas, puntosFinalesActividades, puntosFinalesExamenes;
    float calificacionFinal, asistenciasTotales, porcentajeAsistencia;
    Materia materia;

    //Entrada de datos
    printf("Nombre de la materia >");
    scanf("%s", materia.nombre);
    printf("Acronimo de la materia >");
    scanf("%s", materia.acronimo);
    printf("Dias de la semana >");
    scanf("%s", materia.diasSemana);
    printf("Hora de inicio >");
    scanf("%d", &materia.horaInicio);
    printf("Hora de finalizacion >");
    scanf("%d", &materia.horaFinalizacion);

    printf("Evaluador del Curso de Programaci\xA2n v1.0\n\n");
    printf("Cu\xA0ntas faltas acumulaste? ");
    scanf("%d", &materia.cantidadFaltas);
    printf("Cu\xA0ntos retardos acumulaste? ");
    scanf("%d", &materia.cantidadRetardos);
    printf("Cu\xA0ntas tareas realizaste? ");
    scanf("%d", &materia.cantidadTareasRealizadas);
    printf("Cu\xA0nto obtuviste en la actividad #1? ");
    scanf("%d", &materia.calificacionesActividades[0]);
    printf("Cu\xA0nto obtuviste en la actividad #2? ");
    scanf("%d", &materia.calificacionesActividades[1]);
    printf("Cu\xA0nto obtuviste en la actividad #3? ");
    scanf("%d", &materia.calificacionesActividades[2]);
    printf("Cu\xA0nto obtuviste en la actividad #4? ");
    scanf("%d", &materia.calificacionesActividades[3]);
    printf("Cu\xA0nto obtuviste en la actividad #5? ");
    scanf("%d", &materia.calificacionesActividades[4]);
    printf("Cu\xA0nto obtuviste en la actividad #6? ");
    scanf("%d", &materia.calificacionesActividades[5]);
    printf("Cu\xA0nto obtuviste en la actividad #7? ");
    scanf("%d", &materia.calificacionesActividades[6]);
    printf("Cu\xA0nto obtuviste en la actividad #8? ");
    scanf("%d", &materia.calificacionesActividades[7]);
    printf("Cu\xA0nto obtuviste en la actividad #9? ");
    scanf("%d", &materia.calificacionesActividades[8]);
    printf("Cu\xA0nto obtuviste en el primer examen parcial? ");
    scanf("%d", &materia.calificacionesExamenesParciales[0]);
    printf("Cu\xA0nto obtuviste en el segundo examen parcial? ");
    scanf("%d", &materia.calificacionesExamenesParciales[1]);
    printf("Cu\xA0nto obtuviste en el tercer examen parcial? ");
    scanf("%d", &materia.calificacionesExamenesParciales[2]);

    //Computo de Tareas 
    puntosFinalesTareas = (float)materia.cantidadTareasRealizadas * 100 / TOTAL_TAREAS;
    puntosFinalesTareas *= PORCENTAJE_TAREAS;
    //Computo de Actividades
    puntosFinalesActividades = 0;
    puntosFinalesActividades += materia.calificacionesActividades[0];
    puntosFinalesActividades += materia.calificacionesActividades[1];
    puntosFinalesActividades += materia.calificacionesActividades[2];
    puntosFinalesActividades += materia.calificacionesActividades[3];
    puntosFinalesActividades += materia.calificacionesActividades[4];
    puntosFinalesActividades += materia.calificacionesActividades[5];
    puntosFinalesActividades += materia.calificacionesActividades[6];
    puntosFinalesActividades += materia.calificacionesActividades[7];
    puntosFinalesActividades += materia.calificacionesActividades[8];
    puntosFinalesActividades /= TOTAL_ACTIVIDADES;
    puntosFinalesActividades *= PORCENTAJE_ACTIVIDADES;
    //Computo de Examenes Parciales
    puntosFinalesExamenes = 0;
    puntosFinalesExamenes += materia.calificacionesExamenesParciales[0];
    puntosFinalesExamenes += materia.calificacionesExamenesParciales[1];
    puntosFinalesExamenes += materia.calificacionesExamenesParciales[2];
    puntosFinalesExamenes /= TOTAL_EXAMENES_PARCIALES;
    puntosFinalesExamenes *= PORCENTAJE_EXAMENES_PARCIALES;
    //Computo de Asistencias
    asistenciasTotales = TOTAL_ASISTENCIAS;
    asistenciasTotales -= materia.cantidadFaltas;
    asistenciasTotales -= (float)materia.cantidadRetardos / 3;
    porcentajeAsistencia = asistenciasTotales / TOTAL_ASISTENCIAS * 100;
    //Computo de Puntos Finales 
    calificacionFinal = puntosFinalesTareas + puntosFinalesActividades + puntosFinalesExamenes;

    //Salida de datos
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf("Evaluador del Curso de Programaci\xA2n v1.0\n\n");
    printf("Nombre de la materia= %s\n", materia.nombre);
    printf("Acronimo de la materia= %s\n", materia.acronimo);
    printf("Dias de la semana= %s\n", materia.diasSemana);
    printf("Hora de inicio= %d\n", materia.horaInicio);
    printf("Hora de finalizacion= %d\n", materia.horaFinalizacion);
    printf("Tareas\t Actividades\t Ex\xA0menes\n");
    printf("%6.2f\t %11.2f\t %8.2f\n", 
        puntosFinalesTareas, puntosFinalesActividades, puntosFinalesExamenes);
    printf("Total de asistencias=\t\t %5.2f\n", asistenciasTotales);
    printf("Porcentaje de asistencias=\t %5.2f\n", porcentajeAsistencia);
    printf("Calificaci\xA2n Final=\t\t %5.2f\n", calificacionFinal);
    printf("\n");
    getchar();
    printf("Presione entrar para terminar . . .\n");
    getchar();
    return 0;
}
