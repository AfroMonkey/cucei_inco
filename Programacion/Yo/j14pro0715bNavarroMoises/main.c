/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J14
 * Nombre de practica: J14
 * Fecha: 2015/10/09
 * Version 1
 * Tiempo 00:10
 */

#include "stdio.h"

#define ARBOL_N_C_ESPECIE 50
#define LIBRO_N_C_NOMBRE 50

typedef struct {
    char especie[ARBOL_N_C_ESPECIE+1];
    float altura;
    int edad;
}Arbol;

typedef struct {
    char nombre[LIBRO_N_C_NOMBRE+1];
    char clasificacion;
    int codigo;
}Libro;

int main() {
    Arbol arbol;
    Libro libro;

    printf("**Lectura de datos de %crbol**\n", 0xB5);
    printf("Especie >");
    gets(arbol.especie);
    printf("Altura >");
    scanf("%f", &arbol.altura);
    printf("Edad >");
    scanf("%d", &arbol.edad);
    printf("**Lectura de datos de Libro**\n");
    printf("Nombre >");
    getchar();
    gets(libro.nombre);
    printf("Clasificaci%cn (J/T/A) >", 0xA2); //Jovenes, Todos, Adultos
    scanf("%c", &libro.clasificacion);
    printf("C%cdigo >", 0xA2);
    scanf("%d", &libro.codigo);

    printf("**Impresion de datos de %crbol**\n", 0xB5);
    printf("Especie: %s\n", arbol.especie);
    printf("Altura: %.2f\n", arbol.altura);
    printf("Edad: %d\n", arbol.edad);
    printf("**Impresi%cn de datos de Libro**\n", 0xA2);
    printf("Nombre: %s\n", libro.nombre);
    printf("Clasificaci%cn: %c\n", 0xA2, libro.clasificacion);
    printf("C%cdigo: %d\n", 0xA2, libro.codigo);

    return 0;
}
