/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J21
 * Nombre de practica: J21
 * Fecha: 2015/10/16
 * Version 1
 * Tiempo 00:30
 */

#include "stdio.h"

int main() {
    int mes, dia;

    printf("Mes>");
    scanf("%d", &mes);
    printf("D\xA1\x61>");
    scanf("%d", &dia);

    printf("El mes es ");
    if((mes > 3 && mes < 6) || (mes == 3 && dia >= 21) || (mes == 6 && dia <= 21)) {
        if(mes == 3) {
            printf("marzo");
        } else if(mes == 4) {
            printf("abril");
        } else if(mes == 5) {
            printf("mayo");
        } else {
            printf("junio");
        }
        printf(" y la estaci\xA2n es primavera\n");
    } else if((mes > 6 && mes < 9) || (mes == 6 && dia >= 22) || (mes == 9 && dia <= 21)) {
        if(mes == 6) {
            printf("junio");
        } else if(mes == 7) {
            printf("julio");
        } else if(mes == 8) {
            printf("agosto");
        } else {
            printf("septiembre");
        }
        printf(" y la estaci\xA2n es verano\n");
    } else if((mes > 9 && mes < 12) || (mes == 9 && dia >= 22) || (mes == 12 && dia <= 21)) {
        if(mes == 9) {
            printf("septiembre");
        } else if(mes == 10) {
            printf("octubre");
        } else if(mes == 11) {
            printf("noviembre");
        } else {
            printf("diciembre");
        }
        printf(" y la estaci\xA2n es oto\xA4o\n");
    } else if((mes >= 1 && mes < 3) || (mes == 12 && dia > 21) || (mes == 3 && dia <= 20)) {
        if(mes == 12) {
            printf("diciembre");
        } else if(mes == 1) {
            printf("enero");
        } else if(mes == 2) {
            printf("febrero");
        } else {
            printf("marzo");
        }
        printf(" y la estaci\xA2n es invierno\n");
    } else {
        //Error de usuario
    }

    return 0;
}
