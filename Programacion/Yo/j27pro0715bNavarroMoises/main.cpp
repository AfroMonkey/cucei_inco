/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J27
 * Nombre de practica: J27
 * Fecha: 2015/10/28
 * Version: 1
 * Tiempo: 00:15
 */

#include <iostream>

using namespace std;

string convertirA2Digitos(int valor);

int main() {
    int horasIniciales, minutosIniciales, segundosIniciales;
    int horasFinales, minutosFinales, segundosFinales;
    int diferenciaHoras, diferenciaMinutos, direfenciaSegundos;
    string salida;

    cout << "Horas iniciales>";
    cin >> horasIniciales;
    cout << "Minutos iniciales>";
    cin >> minutosIniciales;
    cout << "Segundos iniciales>";
    cin >> segundosIniciales;
    cout << "Horas finales>";
    cin >> horasFinales;
    cout << "Minutos finales>";
    cin >> minutosFinales;
    cout << "Segundos finales>";
    cin >> segundosFinales;

    direfenciaSegundos = horasFinales*60*60 + minutosFinales*60 + segundosFinales;
    direfenciaSegundos -= (horasIniciales*60*60 + minutosIniciales*60 + segundosIniciales);

    if(direfenciaSegundos >= 0) {
        diferenciaHoras = direfenciaSegundos/(60*60);
        direfenciaSegundos %= 60*60;
        diferenciaMinutos = direfenciaSegundos/60;
        direfenciaSegundos %= 60;
        salida = "Tiempo transcurrido: ";
        salida += convertirA2Digitos(diferenciaHoras);
        salida += ":";
        salida += convertirA2Digitos(diferenciaMinutos);
        salida += ":";
        salida += convertirA2Digitos(direfenciaSegundos);
    } else {
        salida = "Error, tiempo final inferior al tiempo inicial";
    }

    cout << salida << endl;

    return 0;
}


string convertirA2Digitos(int valor) {
    string salida;
    salida = valor/10+'0';
    salida += valor%10+'0';
    return salida;
}
