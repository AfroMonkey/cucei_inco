/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: J30
 * Nombre de practica: J30
 * Fecha: 2015/10/31
 * Version 1
 * Tiempo: 00:11
 */

#include "stdio.h"

#define NUM_DATOS 50

int main() {
    int numPositivos, numPares;
    float sumPositivos, sumNegativos, sumaPares, sumaImpares, promedio;
    float dato;
    int i;

    numPositivos = numPares = sumPositivos = sumNegativos = sumaPares = sumaImpares = promedio = 0;
    i = 0;

    do {
        printf("Dato %d>", i+1);
        scanf("%f", &dato);
        if(dato >= 0) {
            numPositivos++;
            sumPositivos += dato;
        } else {
            sumNegativos += dato;
        }
        if((int)dato%2 == 0) {
            numPares++;
            sumaPares += dato;
        } else {
            sumaImpares += dato;
        }
        promedio += dato;
        i++;
    } while(i < NUM_DATOS);
    promedio /= NUM_DATOS;

    printf("Cantidad de positivos: %d\n", numPositivos);
    printf("Cantidad de negativos: %d\n", NUM_DATOS-numPositivos);
    printf("Cantidad de pares: %d\n", numPares);
    printf("Cantidad de impares: %d\n", NUM_DATOS-numPares);
    printf("Suma de positivos: %f\n", sumPositivos);
    printf("Suma de negativos: %f\n", sumNegativos);
    printf("Suma de pares: %f\n", sumaPares);
    printf("Suma de impares: %f\n", sumaImpares);
    printf("Promedio: %f\n", promedio);
    return 0;
}
