/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 3
 * Name of practice: Area of Circle
 * Date: 2015/08/28
 * Version 1
 */
 
#include "stdio.h"

int main() {
	float a, r, pi;
	pi = 3.1416;
	r = 10.5;
	a = pi * (r*r);	
	printf("Radio : %f\n", r);
	printf("Area: %f\n", a);
	return 0;
}