/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: P1
 * Nombre de la practica: P1
 * Fecha: 2015/09/08
 * Versi\xA2n 1
 * Tiempo: 00:07
 */
#include <iostream>

using namespace std;

/*Declaración y definición de constates ----------------------------------------------------------*/
#define PORCENTAJE_IVA 16
#define PORCENTAJE_ISR 11
#define PORCENTAJE_RETENCION_ISR 10
#define PORCENTAJE_RETENCION_IVA 10

int main() {
	/*Declaración de variables -------------------------------------------------------------------*/
	float ingreso, iva, subtotal, retencionIsr, retencionIva, total;
	float gasto, gananciaBruta, isr, gananciaNeta;
	float pagoIsr;
	float gastosIva, pagoIva;

	cout << "C\xB5LCULO DE IMPUESTOS" << endl;

	/*Entrada de datos ---------------------------------------------------------------------------*/
	cout << "Dame el ingreso: ";
	cin >> ingreso;

	cout << "Dame el gasto: ";
	cin >> gasto;

	/*Cálculo de impuestos -----------------------------------------------------------------------*/
	iva = ingreso * PORCENTAJE_IVA / 100;
	subtotal = ingreso + iva;
	retencionIsr = ingreso * PORCENTAJE_RETENCION_ISR / 100;
	retencionIva = ingreso * PORCENTAJE_RETENCION_IVA / 100;
	total = subtotal - retencionIsr - retencionIva;
	gananciaBruta = ingreso - gasto;
	isr = gananciaBruta * PORCENTAJE_ISR / 100;
	gananciaNeta = gananciaBruta - isr;
	pagoIsr = isr - retencionIsr;
	gastosIva = gasto * PORCENTAJE_IVA / 100;
	pagoIva = iva - gastosIva - retencionIva;

	/*Salida de datos ----------------------------------------------------------------------------*/
	cout << "C\xB5LCULO DE IMPUESTOS" << endl;

	cout << "***Tabla Recibo de Honorarios*** " << endl;
	cout << "Ingresos " << ingreso << endl;
	cout << "(+) IVA " << iva << endl;
	cout << "(=) Subtotal " << subtotal << endl;
	cout << "(-) Retenci\xA2n ISR " << retencionIsr << endl;
	cout << "(-) Retenci\xA2n IVA " << retencionIva << endl;
	cout << "(=) Total  " << total <<endl;

	cout << "***Tabla Ganancias*** " << endl;
	cout << "Ingresos " << ingreso << endl;
	cout << "(-) Gastos  " << gasto <<endl;
	cout << "(=) Ganancia Bruta " << gananciaBruta << endl;
	cout << "(-) ISR  " << isr << endl;
	cout << "(=) Ganancia Neta " << gananciaNeta << endl;

	cout << "***Tabla ISR*** " << endl;
	cout << "ISR " << isr << endl;
	cout << "(-) ISR Retenido " << retencionIsr << endl;
	cout << "(=) ISR a Pagar " << pagoIsr << endl;

	cout << "***Tabla IVA*** " << endl;
	cout << "IVA " << isr << endl;
	cout << "(-) Gastos IVA " << gastosIva << endl;
	cout << "(-) Retenci\xA2n IVA " << retencionIva << endl;
	cout << "(=) IVA a Pagar " << pagoIva << endl;

	cin.get();
	cout << "Presione entrar para terminar . . ." << endl;
	cin.get();

	return 0;
}