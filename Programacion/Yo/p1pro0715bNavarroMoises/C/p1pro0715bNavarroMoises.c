/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: P1
 * Nombre de la practica: P1
 * Fecha: 2015/09/08
 * Versi\xA2n 1
 * Tiempo: 00:22
 */
#include "stdio.h"

/*Declaración y definición de constates ----------------------------------------------------------*/
#define PORCENTAJE_IVA 16
#define PORCENTAJE_ISR 11
#define PORCENTAJE_RETENCION_ISR 10
#define PORCENTAJE_RETENCION_IVA 10

int main() {
    /*Declaración de variables -------------------------------------------------------------------*/
    float ingreso, iva, subtotal, retencionIsr, retencionIva, total;
    float gasto, gananciaBruta, isr, gananciaNeta;
    float pagoIsr;
    float gastosIva, pagoIva;

    printf("C\xB5LCULO DE IMPUESTOS\n");

    /*Entrada de datos ---------------------------------------------------------------------------*/
    printf("Dame el ingreso: ");
    scanf("%f", &ingreso);

    printf("Dame el gasto: ");
    scanf("%f", &gasto);

    /*Cálculo de impuestos -----------------------------------------------------------------------*/
    iva = ingreso * PORCENTAJE_IVA / 100;
    subtotal = ingreso + iva;
    retencionIsr = ingreso * PORCENTAJE_RETENCION_ISR / 100;
    retencionIva = ingreso * PORCENTAJE_RETENCION_IVA / 100;
    total = subtotal - retencionIsr - retencionIva;
    gananciaBruta = ingreso - gasto;
    isr = gananciaBruta * PORCENTAJE_ISR / 100;
    gananciaNeta = gananciaBruta - isr;
    pagoIsr = isr - retencionIsr;
    gastosIva = gasto * PORCENTAJE_IVA / 100;
    pagoIva = iva - gastosIva - retencionIva;

    /*Salida de datos ----------------------------------------------------------------------------*/
    printf("C\xB5LCULO DE IMPUESTOS\n");
    
    printf("***Tabla Recibo de Honorarios*** \n");
    printf("Ingresos \t\t %10.2f\n", ingreso);
    printf("(+) IVA \t\t %10.2f\n", iva);
    printf("(=) Subtotal \t\t %10.2f\n", subtotal);
    printf("(-) Retenci\xA2n ISR \t %10.2f\n", retencionIsr);
    printf("(-) Retenci\xA2n IVA \t %10.2f\n", retencionIva);
    printf("(=) Total  \t\t %10.2f\n", total);

    printf("***Tabla Ganancias*** \n");
    printf("Ingresos \t\t %10.2f\n", ingreso);
    printf("(-) Gastos  \t\t %10.2f\n", gasto);
    printf("(=) Ganancia Bruta \t %10.2f\n", gananciaBruta);
    printf("(-) ISR  \t\t %10.2f\n", isr);
    printf("(=) Ganancia Neta  \t %10.2f\n", gananciaNeta);

    printf("***Tabla ISR*** \n");
    printf("ISR \t\t\t %10.2f\n", isr);
    printf("(-) ISR Retenido \t %10.2f\n", retencionIsr);
    printf("(=) ISR a Pagar \t %10.2f\n", pagoIsr);

    printf("***Tabla IVA*** \n");
    printf("IVA \t\t\t %10.2f\n", isr);
    printf("(-) Gastos IVA \t\t %10.2f\n", gastosIva);
    printf("(-) Retenci\xA2n IVA \t %10.2f\n", retencionIva);
    printf("(=) IVA a Pagar \t %10.2f\n", pagoIva);

    getchar();
    printf("Presione entrar para terminar . . .\n");
    getchar();

    return 0;
}