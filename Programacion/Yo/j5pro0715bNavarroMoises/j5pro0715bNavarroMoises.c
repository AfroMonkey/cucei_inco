/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 5
 * Name of practice: Area of Circle
 * Date: 2015/08/28
 * Version 1
 */
 
#include "stdio.h"

#define PI 3.1416

int main() {
	float areaCirculo, radio;
	radio = 10.5;
	areaCirculo = PI * (radio*radio);	
	printf("Radio : %f\n", radio);
	printf("Area: %f\n", areaCirculo);
	return 0;
}