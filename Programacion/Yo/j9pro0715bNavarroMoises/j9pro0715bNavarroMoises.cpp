/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: J9
 * Nombre de la practica: J9
 * Fecha: 2015/09/08
 * Versión 1
 * Tiempo: 00:05
 */
#include <iostream>

using namespace std;

int main() {
	float celsius, fahrenheit;

	cout << "Celsius >";
	cin >> celsius;

	fahrenheit = 1.8 * celsius + 32;
	cout << celsius << "\xF8\x43 = " <<fahrenheit << endl;
	
	return 0;
}