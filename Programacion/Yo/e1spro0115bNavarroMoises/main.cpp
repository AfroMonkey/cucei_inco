/* Autor: Navarro Presas Moises Alejandro
 * Codigo: 215861509
 * Fecha: 2015/10/30
 * Nombre: Examen 1
 */

#include <iostream>

using namespace std;

#define OPC_DESCUENTO 1
#define OPC_AREA 2
#define OPC_LLENAR_VECTOR 3
#define OPC_MOSTRAR_VECTOR 4
#define DISP_DESCUENTO 100
#define PORC_DESCUENTO 10.0
#define PI 3.1416
#define MIN_RADIO 0
#define MAX_RADIO 80
#define NUM_ACTIVDADES 3

class Circulo {
    int radio;
public:
    Circulo() {
        radio = 0;
    }

    bool fijaRadio(int radioX) {
        if(radioX > MIN_RADIO && radioX <= MAX_RADIO) {
            radio = radioX;
            return true;
        } else {
            return false;
        }
    }

    int dameRadio() {
        return radio;
    }

    float dameArea() {
        return PI*radio*radio;
    }
};

int actividades[NUM_ACTIVDADES];

void descuento();
void area();
void llenarVector();
void almacenarEnVector(int indice);
void mostrarVector();

int main() {
    int opc;

    cout << "Primer Examen SPRO" << endl;

    cout << "1. Calculo de descuento" << endl;
    cout << "2. Area circulo" << endl;
    cout << "3. Llenar el vector" << endl;
    cout << "4. Mostrar vector" << endl;
    cout << "Elija la opcion: ";
    cin >> opc;

    switch(opc) {
        case OPC_DESCUENTO: {
            descuento();
            break;
        }
        case OPC_AREA: {
            area();
            break;
        }
        case OPC_LLENAR_VECTOR: {
            llenarVector();
            break;
        }
        case OPC_MOSTRAR_VECTOR: {
            mostrarVector();
            break;
        }
        default: {
            cout << "Opcion No Valida" << endl;
            break;
        }
    }
    return 0;
}

void descuento() {
    int articulo1;
    float articulo2;
    float montoAPagar;

    cout << "Dame el precio del Primer Articulo ";
    cin >> articulo1;
    cout << "Dame el precio del Segundo Articulo ";
    cin >> articulo2;
    
    montoAPagar = articulo1+articulo2;

    cout << "Monto inicial a pagar: " << montoAPagar << endl;

    if(montoAPagar > DISP_DESCUENTO) {
        montoAPagar -= montoAPagar*PORC_DESCUENTO/100;
    }
    
    cout << "Precio original del Primer Articulo: " << articulo1 << endl;
    cout << "Precio original del Segundo Articulo: " << articulo2 << endl;
    cout << "Monto final a pagar: " << montoAPagar << endl;
}

void area() {
    Circulo circulo;
    int radio;
    cout << "Dame el radio ";
    cin >> radio;
    if(circulo.fijaRadio(radio)) {
        cout << circulo.dameArea();
    } else {
        cout << "El Radio es Invalido" << endl;
    }
}

void llenarVector() {
    almacenarEnVector(0);
    almacenarEnVector(1);
    almacenarEnVector(2);
}

void almacenarEnVector(int indice) {
    int calificacion;
    cout << "Dame la calificacion del examen #" << indice+1 << " ";
    cin >> calificacion;
    actividades[indice] = calificacion;
}

void mostrarVector() {
    cout << "Calificacion del examen #1: " << actividades[0] << endl;
    cout << "Calificacion del examen #2: " << actividades[1] << endl;
    cout << "Calificacion del examen #3: " << actividades[2] << endl; 
}
