/*
 * Alumno: Navarro Presas Moisés Alejandro
 * Numero de practica: J10
 * Nombre de la practica: J10
 * Fecha: 2015/09/08
 * Versión 1
 * Tiempo: 00:06
 */
#include <iostream>

using namespace std;

int main() {
	float fahrenheit, celsius1, celsius2;

	cout << "Fahrenheit >";
	cin >> fahrenheit;

	celsius1 = (fahrenheit - 32) / 2;
	celsius1 += celsius1 / 10;
	cout << fahrenheit << "\xF8\x46 = " << celsius1 << "\xF8\x43" <<endl;

	celsius2 = (fahrenheit - 32) / 1.8;
	cout << fahrenheit << "\xF8\x46 = " << celsius2 << "\xF8\x43" <<endl;

	cout << celsius1 << " - " << celsius2 << " = " << celsius1 - celsius2 << endl;
	
	return 0;
}