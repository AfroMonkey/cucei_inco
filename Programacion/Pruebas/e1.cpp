/* Autor: Navarro Presas Moises Alejandro
 * Fecha: 2015/10/23
 * Tiempo: 00:51
 */

#include <iostream>

using namespace std;

#define NUM_EMPLEADOS 5
#define PORC_INC_HORAS_EXTRA 10

class Empleado {
    string nombre;
    float sueldoPorHora;
    int horasMinimas;
    int horasMaximas;
    int horasTrabajadas;
public:
    Empleado() {
        nombre = "Staff";
        horasTrabajadas = -1;
        sueldoPorHora = -1;
        horasMinimas = -1;
        horasMaximas = -1;
    }

    void fijaNombre(string nombreX) {
        nombre = nombreX;
    }

    string dameNombre() {
        return nombre;
    }

    bool fijaSueldoPorHora(float sueldoPorHoraX) {
        if(sueldoPorHoraX > 0) {
            sueldoPorHora = sueldoPorHoraX;
            return true;
        } else {
            return false;
        }
    }

    float dameSueldoPorHora() {
        return sueldoPorHora;
    }

    bool fijaHorasMinimas(int horasMinimasX) {
        if(horasMinimasX > 0) {
            horasMinimas = horasMinimasX;
            return true;
        } else {
            return false;
        }
    }

    int dameHorasMinimas() {
        return horasMinimas;
    }

    int fijaHorasMaximas(int horasMaximasX) {
        if(horasMaximasX > 0) {
            if(horasMaximasX >= horasMinimas) {
                horasMaximas = horasMaximasX;
                return 1;
            } else {
                return -2;
            }
        } else {
            return -1;
        }
    }

    int dameHorasMaximas() {
        return horasMaximas;
    }

    int fijaHorasTrabajadas(int horasTrabajadasX) {
        if(horasTrabajadasX >= 0) {
            horasTrabajadas = horasTrabajadasX;
            if(horasTrabajadas <= horasMaximas) {
                return 1;
            } else {
                return -2;
            }
        } else {
            return -1;
        }
    }

    int dameHorasTrabajadas() {
        return horasTrabajadas;
    }

    float damePagaFinal() {
        float pagaFinal;
        pagaFinal = horasTrabajadas*sueldoPorHora;
        if(horasTrabajadas < horasMinimas) {
            pagaFinal -= 500;
            pagaFinal = (pagaFinal<0)?0:pagaFinal;
        } else {
            int horasExtra;
            horasExtra = horasTrabajadas-horasMinimas;
            if(horasExtra > 0) {
                pagaFinal += horasExtra*sueldoPorHora*PORC_INC_HORAS_EXTRA/100;
            }
        }
        return pagaFinal;
    }

    bool seExcedio() {
        return horasTrabajadas > horasMaximas;
    }
};

Empleado empleados[NUM_EMPLEADOS];

bool llenarEmpleado(int indice);
void mostrarEmpleado(int indice);
void alerta(string mensaje);

int main() {
    if(llenarEmpleado(0)&&llenarEmpleado(1)&&llenarEmpleado(2)&&llenarEmpleado(3)&&llenarEmpleado(4)) {
        mostrarEmpleado(0);
        mostrarEmpleado(1);
        mostrarEmpleado(2);
        mostrarEmpleado(3);
        mostrarEmpleado(4);
        mostrarEmpleadoMasTrabajador();
    } else {
        alerta("Ingreso un campo invalido");
    }

    return 0;
}

bool llenarEmpleado(int indice) {
    bool estatus;
    string nombre;
    float sueldoPorHora;
    int horasMinimas;
    int horasMaximas;
    int horasTrabajadas;

    estatus = true;
    cout << "Empleado numero " << indice+1 << endl;;
    cout << "Ingrese el nombre>";
    getline(cin, nombre);
    empleados[indice].fijaNombre(nombre);
    cout << "Ingrese el sueldo por hora>";
    cin >> sueldoPorHora;
    if(empleados[indice].fijaSueldoPorHora(sueldoPorHora)) {
        cout << "Ingrese las horas minimas>";
        cin >> horasMinimas;
        if(empleados[indice].fijaHorasMinimas(horasMinimas)) {
            cout << "Ingrese las horas maximas>";
            cin >> horasMaximas;
            switch(empleados[indice].fijaHorasMaximas(horasMaximas)) {
                case 1: {
                    cout << "Ingrese las horas trabajadas>";
                    cin >> horasTrabajadas;
                    switch(empleados[indice].fijaHorasTrabajadas(horasTrabajadas)) {
                        case 1: {
                            break;
                        }
                        case -1: {
                            estatus = false;
                            break;
                        }
                        case -2: {
                            alerta("Sobrecarga de trabajo");
                            break;
                        }
                        default: {
                            alerta("Error sin identificar");
                            estatus = false;
                            break;
                        }
                    }
                    break;
                }
                case -1: {
                    estatus = false;
                    break;
                }
                case -2: {
                    alerta("Horas maximas menor que hormas minimas");
                    estatus = false;
                    break;
                }
                default: {
                    alerta("Error sin identificar");
                    estatus = false;
                    break;
                }
            }
        } else {
            estatus = false;
        }
    } else {
        estatus = false;
    }

    return estatus;
}

void mostrarEmpleado(int indice) {
    cout << "Informacion del empleado " << indice+1 << endl;
    cout << "Nombre: " << empleados[indice].dameNombre() << endl;
    cout << "Sueldo por hora: " << empleados[indice].dameSueldoPorHora() << endl;
    cout << "Horas minimas: " << empleados[indice].dameHorasMinimas() << endl;
    cout << "Horas maximas: " << empleados[indice].dameHorasMaximas() << endl;
    cout << "Horas trabajadas: " << empleados[indice].dameHorasTrabajadas() << endl;
    cout << "Paga final: " << empleados[indice].damePagaFinal() << endl;
}

void alerta(string mensaje) {
    cout << mensaje << endl;
}
