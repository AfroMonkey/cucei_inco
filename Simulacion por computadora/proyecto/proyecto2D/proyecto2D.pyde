stage = f = c = rr = None
tr = rr = 0

BACKGROUND = [38, 50, 56]
ROTATION = PI/4
SCALE = 2.0
sr = 1/SCALE

def drawPolygon(pr):
    polygon = pr[:]
    polygon.append(polygon[0])
    last = polygon[0]
    for p in polygon[1:]:
        diferencial(last, p)
        last = p

def centroid(polygon):
    minX = maxX = polygon[0][0]
    minY = maxY = polygon[0][1]
    for p in polygon[1:]:
        if p[0] < minX:
            minX = p[0]
        if p[0] > maxX:
            maxX = p[0]
        if p[1] < minY:
            minY = p[1]
        if p[1] > maxY:
            maxY = p[1]
    return [minX+(maxX-minX)/2, minY+(maxY-minY)/2]

def transl(polygon, o):
    nf = []
    for p in polygon:
        nf.append([p[0]+o[0], p[1]+o[1]])
    return nf

def transform(polygon, m):
    nf = []
    for p in polygon:
        nf.append(matrixMul([p + [1]], m)[0])
    return nf
    
def matrixMul(M1, M2):
    result = [[0, 0, 0]]
    for i in range(len(M1)):
        for j in range(len(M2[0])):
            for k in range(len(M2)):
                result[i][j] += M1[i][k] * M2[k][j]
    return result

def matrix(t, r, s):
    return [
        [s[0]*cos(r), s[1]*sin(r), 0],
        [-(s[0]*sin(r)), s[1]*cos(r), 0],
        [s[0]*(t[0]*cos(r)-t[1]*sin(r)), s[1]*(t[1]*cos(r)+t[0]*sin(r)), 1]
    ]


def diferencial(p1o, p2o):
    p1 = p1o[:]
    p2 = p2o[:]
    m = 'nan'
    if p2[0] - p1[0]:
        m = (float(p2[1]) - p1[1])/(p2[0] - p1[0])
        b = p2[1] - m*p2[0]
        if  abs(m) > 1:
            if p2[1] > p1[1]:
                dir = 1
            else:
                dir = -1
        else:
            if p2[0] > p1[0]:
                dir = 1
            else:
                dir = -1
    else:
        if p2[1] > p1[1]:
            dir = 1
        else:
            dir = -1
    while True:
        if p1[0] < 1 and p1[1] < 0:
            break
        dx = p1[0] - p2[0]
        dx *= dx
        dy = p1[1] - p2[1]
        dy *= dy
        point(p1[0], p1[1])
        if abs(dx + dy) <= 1:
            break
        if m == 'nan':
            p1[1] += dir
        elif abs(m) > 1:
            p1[1] += int(dir)
            p1[0] = int(p1[1] - b)/m
        else:
            p1[0] += int(dir)
            p1[1] = int(m*p1[0] + b)

def setup():
    global f, stage, c
    size(150, 150)
    background(*BACKGROUND)
    strokeWeight(1)
    frameRate(60)
    
    l = 7
    p = [20, 8]
    f = [
         [p[0]+0, p[1]-0],
         [p[0]+l, p[1]-0],
         [p[0]+l, p[1]-l],
         [p[0]+0, p[1]-l],
    ]
    c = centroid(f)
    stage = 1

def draw():
    global stage, tr, f, c, rr, sr
    if stage == 1:
        stroke(250, 250, 250)
        background(*BACKGROUND)
        nf = f
        stage = 2
    if int(stage) == 2:
        if stage == 2:
            stroke(221, 44, 0)
            ca = [int(-tr*coor) for coor in c]
            tr += 0.01
            nf = transl(f, ca)
            if tr >= 1:
                f = nf
                stage = 2.1
        if stage == 2.1:
            stroke(0, 200, 83)
            m = matrix([0, 0], ROTATION*rr, [SCALE*sr, SCALE*sr])
            rr += 0.01
            sr += 0.005
            nf = transform(f, m)
            if rr >= 1:
                f = nf
                stage = 2.2
                tr = 0
        if stage == 2.2:
            stroke(0, 145, 234)
            ca = [int(tr*coor) for coor in c]
            tr += 0.01
            nf = transl(f, ca)
            if tr >= 1:
                f = nf
                stage = 3
    if stage == 3:
        return
    background(*BACKGROUND)
    drawPolygon(nf)