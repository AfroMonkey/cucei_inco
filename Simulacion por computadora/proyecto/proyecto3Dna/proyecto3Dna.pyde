cent = l = cent2 =None
tr = rr = 1
figs = []
cams = []

BACKGROUND = [38, 50, 56]
ROTATION = PI/4
SCALE = 1.35
SCALE2 = 0.5
sr = 1/SCALE
sr2 = 1/SCALE2
ss = (1-sr)/100
ss2 = (1-sr2)/100

colors = [
    [250, 250, 250],
    [221, 44, 0],
    [0, 200, 83],
    [0, 145, 234],
    [255, 87, 34],
    [9, 145, 234],
    [225, 190, 231],
]

def drawFigure(edges):
    for edge in edges:
        edge = edge[0] + edge[1]
        if len(edge) == 8:
            edge.pop(7)
            edge.pop(3)
        line(*edge)

def centroid3D(vertices):
    minX = maxX = vertices[0][0]
    minY = maxY = vertices[0][1]
    minZ = maxZ = vertices[0][2]
    for v in vertices[1:]:
        if v[0] < minX:
            minX = v[0]
        if v[0] > maxX:
            maxX = v[0]
        if v[1] < minY:
            minY = v[1]
        if v[1] > maxY:
            maxY = v[1]
        if v[2] < minZ:
            minZ = v[2]
        if v[2] > maxZ:
            maxZ = v[2]
    return [minX+(maxX-minX)/2, minY+(maxY-minY)/2, minZ+(maxZ-minZ)/2]

def transl(edges, o):
    ne = []
    for e in edges:
        ne.append([
            [e[0][0]+o[0], e[0][1]+o[1], e[0][2]+o[2]],
            [e[1][0]+o[0], e[1][1]+o[1], e[1][2]+o[2]],
        ])
    return ne

def transform(edges, m):
    ne = []
    for e in edges:
        ne.append([
            matrixMul([e[0] + [1]], m)[0],
            matrixMul([e[1] + [1]], m)[0],
        ])
    return ne

def matrixMul(M1, M2):
    result = 4*[[0, 0, 0, 0]]
    for i in range(len(M1)):
        for j in range(len(M2[0])):
            for k in range(len(M2)):
                result[i][j] += M1[i][k] * M2[k][j]
    return result

def matrix(t, r, s):
    tx, ty, tz = t
    rx, ry, rz = r
    sx, sy, sz = s
    return [
        [sx*cos(ry)*cos(rz), sy*sin(rz)*cos(ry), -sz*sin(ry), 0],
        [sx*(sin(rx)*sin(ry)*cos(rz) - sin(rz)*cos(rx)), sy*(sin(rx)*sin(ry)*sin(rz) + cos(rx)*cos(rz)), sz*sin(rx)*cos(ry), 0],
        [sx*(sin(rx)*sin(rz) + sin(ry)*cos(rx)*cos(rz)), sy*(-sin(rx)*cos(rz) + sin(ry)*sin(rz)*cos(rx)), sz*cos(rx)*cos(ry), 0],
        [sx*((tx*cos(ry) + (ty*sin(rx) + tz*cos(rx))*sin(ry))*cos(rz) - (ty*cos(rx) - tz*sin(rx))*sin(rz)), sy*((tx*cos(ry) + (ty*sin(rx) + tz*cos(rx))*sin(ry))*sin(rz) + (ty*cos(rx) - tz*sin(rx))*cos(rz)),  sz*(-tx*sin(ry) + (ty*sin(rx) + tz*cos(rx))*cos(ry)), 1],
    ]

def mousePressed():
    cams.append(cams[0])
    cams.pop(0)
    camera(*cams[0])

def setup():
    global c1, cent, stage, l, cams
    size(300, 300, P3D)
    background(*BACKGROUND)
    strokeWeight(1)
    frameRate(60)
    cams.append([
        width/10.0, -10, 40,
        width/15.0, 10, 0,
        0, 1, 0
    ])
    cams.append([
        80, -10, 10,
        width/15.0, 10, 0,
        0, 1, 0
    ])
    cams.append([
        -20, -30, 30,
        width/15.0, 10, 0,
        0, 1, 0
    ])
    camera(*cams[0])

    l = 10
    p = [10, 7, 3]
    c1 = [
        [[p[0]+0, p[1]-0, p[2]-0], [p[0]+l, p[1]-0, p[2]-0]],
        [[p[0]+l, p[1]-0, p[2]-0], [p[0]+l, p[1]-l, p[2]-0]],
        [[p[0]+l, p[1]-l, p[2]-0], [p[0]+0, p[1]-l, p[2]-0]],
        [[p[0]+0, p[1]-l, p[2]-0], [p[0]+0, p[1]-0, p[2]-0]],
        [[p[0]+0, p[1]-0, p[2]-l], [p[0]+l, p[1]-0, p[2]-l]],
        [[p[0]+l, p[1]-0, p[2]-l], [p[0]+l, p[1]-l, p[2]-l]],
        [[p[0]+l, p[1]-l, p[2]-l], [p[0]+0, p[1]-l, p[2]-l]],
        [[p[0]+0, p[1]-l, p[2]-l], [p[0]+0, p[1]-0, p[2]-l]],
        [[p[0]+0, p[1]-0, p[2]-0], [p[0]+0, p[1]-0, p[2]-l]],
        [[p[0]+l, p[1]-0, p[2]-0], [p[0]+l, p[1]-0, p[2]-l]],
        [[p[0]+l, p[1]-l, p[2]-0], [p[0]+l, p[1]-l, p[2]-l]],
        [[p[0]+0, p[1]-l, p[2]-0], [p[0]+0, p[1]-l, p[2]-l]],
    ]
    cent = centroid3D([
        [p[0]+0, p[1]-0, p[2]-0],
        [p[0]+l, p[1]-0, p[2]-0],
        [p[0]+l, p[1]-l, p[2]-0],
        [p[0]+0, p[1]-l, p[2]-0],
        [p[0]+0, p[1]-0, p[2]-l],
        [p[0]+l, p[1]-0, p[2]-l],
        [p[0]+l, p[1]-l, p[2]-l],
        [p[0]+0, p[1]-l, p[2]-l],
    ])
    figs.append(c1)
    c2 = transl(c1, [-coor for coor in cent])
    figs.append(c2)
    m = matrix([0, 0, 0], [ROTATION, ROTATION, ROTATION], [SCALE, SCALE, SCALE])
    c2 = transform(c2, m)
    figs.append(c2)
    c2 = transl(c2, [coor for coor in cent])
    figs.append(c2)
    cent2 = c2[0][0][:]
    cent2[0] += l/4
    cent2[1] -= l/4
    cent2[2] -= l/4
    c3 = transl(c1, [-coor for coor in cent])
    figs.append(c3)
    m = matrix([0, 0, 0], [0, 0, 0], [SCALE2, SCALE2, SCALE2])
    c3 = transform(c3, m)
    figs.append(c3)
    c3 = transl(c3, [int(tr*coor) for coor in cent2])
    figs.append(c3)

def draw():
    background(*BACKGROUND)
    for f, c in zip(figs, colors):
        stroke(*c)
        drawFigure(f)