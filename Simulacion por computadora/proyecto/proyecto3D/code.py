cent = stage = l = cent2 =None
tr = rr = 0
figs = []
cams = []

BACKGROUND = [38, 50, 56]
ROTATION = PI/4
SCALE = 1.35
SCALE2 = 0.5
sr = 1/SCALE
sr2 = 1/SCALE2
ss = (1-sr)/100
ss2 = (1-sr2)/100

colors = [
    [250, 250, 250],
    [221, 44, 0],
    [0, 200, 83],
    [0, 145, 234],
    [221, 44, 0],
    [0, 200, 83],
    [0, 145, 234],
]

def drawFigure(edges):
    for edge in edges:
        edge = edge[0] + edge[1]
        if len(edge) == 8:
            edge.pop(7)
            edge.pop(3)
        line(*edge)

def centroid3D(vertices):
    minX = maxX = vertices[0][0]
    minY = maxY = vertices[0][1]
    minZ = maxZ = vertices[0][2]
    for v in vertices[1:]:
        if v[0] < minX:
            minX = v[0]
        if v[0] > maxX:
            maxX = v[0]
        if v[1] < minY:
            minY = v[1]
        if v[1] > maxY:
            maxY = v[1]
        if v[2] < minZ:
            minZ = v[2]
        if v[2] > maxZ:
            maxZ = v[2]
    return [minX+(maxX-minX)/2, minY+(maxY-minY)/2, minZ+(maxZ-minZ)/2]

def transl(edges, o):
    ne = []
    for e in edges:
        ne.append([
            [e[0][0]+o[0], e[0][1]+o[1], e[0][2]+o[2]],
            [e[1][0]+o[0], e[1][1]+o[1], e[1][2]+o[2]],
        ])
    return ne

def transform(edges, m):
    ne = []
    for e in edges:
        ne.append([
            matrixMul([e[0] + [1]], m)[0],
            matrixMul([e[1] + [1]], m)[0],
        ])
    return ne

def matrixMul(M1, M2):
    result = 4*[[0, 0, 0, 0]]
    for i in range(len(M1)):
        for j in range(len(M2[0])):
            for k in range(len(M2)):
                result[i][j] += M1[i][k] * M2[k][j]
    return result

def matrix(t, r, s):
    tx, ty, tz = t
    rx, ry, rz = r
    sx, sy, sz = s
    return [
        [sx*cos(ry)*cos(rz), sy*sin(rz)*cos(ry), -sz*sin(ry), 0],
        [sx*(sin(rx)*sin(ry)*cos(rz) - sin(rz)*cos(rx)), sy*(sin(rx)*sin(ry)*sin(rz) + cos(rx)*cos(rz)), sz*sin(rx)*cos(ry), 0],
        [sx*(sin(rx)*sin(rz) + sin(ry)*cos(rx)*cos(rz)), sy*(-sin(rx)*cos(rz) + sin(ry)*sin(rz)*cos(rx)), sz*cos(rx)*cos(ry), 0],
        [sx*((tx*cos(ry) + (ty*sin(rx) + tz*cos(rx))*sin(ry))*cos(rz) - (ty*cos(rx) - tz*sin(rx))*sin(rz)), sy*((tx*cos(ry) + (ty*sin(rx) + tz*cos(rx))*sin(ry))*sin(rz) + (ty*cos(rx) - tz*sin(rx))*cos(rz)),  sz*(-tx*sin(ry) + (ty*sin(rx) + tz*cos(rx))*cos(ry)), 1],
    ]

def mousePressed():
    cams.append(cams[0])
    cams.pop(0)
    camera(*cams[0])

def setup():
    global c1, cent, stage, l, cams
    size(300, 300, P3D)
    background(*BACKGROUND)
    strokeWeight(1)
    frameRate(60)
    cams.append([
        width/10.0, -10, 40,
        width/15.0, 10, 0,
        0, 1, 0
    ])
    cams.append([
        50, -10, 30,
        width/15.0, 10, 0,
        0, 1, 0
    ])
    cams.append([
        -20, -30, 30,
        width/15.0, 10, 0,
        0, 1, 0
    ])
    camera(*cams[0])

    l = 10
    p = [10, 7, 3]
    c1 = [
        [[p[0]+0, p[1]-0, p[2]-0], [p[0]+l, p[1]-0, p[2]-0]],
        [[p[0]+l, p[1]-0, p[2]-0], [p[0]+l, p[1]-l, p[2]-0]],
        [[p[0]+l, p[1]-l, p[2]-0], [p[0]+0, p[1]-l, p[2]-0]],
        [[p[0]+0, p[1]-l, p[2]-0], [p[0]+0, p[1]-0, p[2]-0]],
        [[p[0]+0, p[1]-0, p[2]-l], [p[0]+l, p[1]-0, p[2]-l]],
        [[p[0]+l, p[1]-0, p[2]-l], [p[0]+l, p[1]-l, p[2]-l]],
        [[p[0]+l, p[1]-l, p[2]-l], [p[0]+0, p[1]-l, p[2]-l]],
        [[p[0]+0, p[1]-l, p[2]-l], [p[0]+0, p[1]-0, p[2]-l]],
        [[p[0]+0, p[1]-0, p[2]-0], [p[0]+0, p[1]-0, p[2]-l]],
        [[p[0]+l, p[1]-0, p[2]-0], [p[0]+l, p[1]-0, p[2]-l]],
        [[p[0]+l, p[1]-l, p[2]-0], [p[0]+l, p[1]-l, p[2]-l]],
        [[p[0]+0, p[1]-l, p[2]-0], [p[0]+0, p[1]-l, p[2]-l]],
    ]
    cent = centroid3D([
        [p[0]+0, p[1]-0, p[2]-0],
        [p[0]+l, p[1]-0, p[2]-0],
        [p[0]+l, p[1]-l, p[2]-0],
        [p[0]+0, p[1]-l, p[2]-0],
        [p[0]+0, p[1]-0, p[2]-l],
        [p[0]+l, p[1]-0, p[2]-l],
        [p[0]+l, p[1]-l, p[2]-l],
        [p[0]+0, p[1]-l, p[2]-l],
    ])
    stage = 1

def draw():
    global c1, cent, stage, tr, sr, rr, figs, l, sr2, cent2
    cl = None
    if stage == 1:
        stroke(250, 250, 250)
        background(*BACKGROUND)
        figs.append(c1)
        stage = 2
    if int(stage) == 2:
        if stage == 2:
            cl = [221, 44, 0]
            ca = [int(-tr*coor) for coor in cent]
            tr += 0.01
            nc = transl(c1, ca)
            if tr >= 1:
                c1 = nc
                stage = 2.1
        if stage == 2.1:
            cl = [0, 200, 83]
            m = matrix([0, 0, 0], [ROTATION*rr, ROTATION*rr, ROTATION*rr], [SCALE*sr, SCALE*sr, SCALE*sr])
            rr += 0.01
            sr += ss
            nc = transform(c1, m)
            if rr >= 1:
                c1 = nc
                stage = 2.2
                tr = 0
        if stage == 2.2:
            cl = [0, 145, 234]
            ca = [int(tr*coor) for coor in cent]
            tr += 0.01
            nc = transl(c1, ca)
            if tr >= 1:
                figs.append(nc)
                stage = 3
                tr = rr = 0
                c1 = figs[0]
                cent2 = nc[0][0][:]
                cent2[0] -= l/4
                cent2[0] -= l/4
                cent2[0] -= l/4

    if int(stage) == 3:
        if stage == 3:
            cl = [221, 44, 0]
            ca = [int(-tr*coor) for coor in cent]
            tr += 0.01
            nc = transl(c1, ca)
            if tr >= 1:
                c1 = nc
                stage = 3.1
        if stage == 3.1:
            cl = [0, 200, 83]
            m = matrix([0, 0, 0], [0, 0, 0], [SCALE2*sr2, SCALE2*sr2, SCALE2*sr2])
            rr += 0.01
            sr2 += ss2
            nc = transform(c1, m)
            if rr >= 1:
                c1 = nc
                stage = 3.2
                tr = 0
        if stage == 3.2:
            cl = [0, 145, 234]
            ca = [int(tr*coor) for coor in cent2]
            tr += 0.01
            nc = transl(c1, ca)
            if tr >= 1:
                figs.append(nc)
                stage = 4

    background(*BACKGROUND)
    for f, c in zip(figs, colors):
        stroke(*c)
        drawFigure(f)
    if stage == 4:
        return
    stroke(*cl)
    drawFigure(nc)
