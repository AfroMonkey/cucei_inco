from math import ceil

center = False
circles = []
sqrt_2 = sqrt(2)
mode = False

block_size = 1
width = 500
height = 500
fps = 60*2

def init():
    background(0)
    fill('#FFFFFF')
    noStroke()

def setup():
    size(width*block_size, height*block_size)
    frameRate(fps)
    init()

def draw():
    center = [width/2, height/2]
    rx = 200
    ry = 120
    set_pixel(center)
    # draw_ellipse(center, rx, ry)
    draw_ellipse_b(center[0], center[1], rx, ry)

def set_pixel(p):
    rect(p[0]*block_size, p[1]*block_size, block_size, block_size)

def draw_ellipse(center, rx, ry):
    for i in range(center[0], center[0] + rx + 1):
        y_aux = center[1] - ((ry*sqrt(rx**2 - i**2 + 2*i*center[0] - center[0]**2))/rx)
        set_pixel([i, y_aux])
        set_pixel([i, 2*center[1] - y_aux])
        set_pixel([2*center[0] - i, y_aux])
        set_pixel([2*center[0] - i, 2*center[1] - y_aux])
    for i in range(center[1], center[1] + ry + 1):
        x_aux = center[0] - ((rx*sqrt(ry**2 - i**2 + 2*i*center[1] - center[1]**2))/ry)
        set_pixel([x_aux, i])
        set_pixel([2*center[1] - x_aux, i])
        set_pixel([x_aux, 2*center[0] - i])
        set_pixel([2*center[1] - x_aux, 2*center[0] - i])

def draw_ellipse_b(cx, cy, rx, ry):
    pass
        