import math
import time
points = []
ncp = 4

second_grade_matrix = [
    [1, -2, 1],
    [-2, 2, 0],
    [1, 0, 0]
]
third_grade_matrix = [
    [-1, 3, -3, 1],
    [3, -6, 3, 0],
    [-3, 3, 0, 0],
    [1, 0, 0, 0]
]
fourth_grade_matrix = [
    [1, -4, 6, -4, 1],
    [-4, 12, -12, 4, 0],
    [6, -12, 6, 0, 0],
    [-4, 4, 0, 0, 0],
    [1, 0, 0, 0, 0]
]


def setup():
    size(600, 600)
    stroke(100)
    fill(255, 153)
    background(0)
    strokeWeight(5)
    frameRate(144)


def draw():
    pass


def mousePressed():
    global points
    if len(points) < ncp:
        posX = mouseX
        posY = mouseY
        points.append([posX, posY])
        point(posX, posY)
    if ncp == len(points) and len(points) == 3:
        current_matrix = second_grade_matrix
        draw_curve(points, current_matrix)
    elif ncp == len(points) and len(points) == 4:
        current_matrix = third_grade_matrix
        draw_curve(points, current_matrix)
    elif ncp == len(points) and len(points) == 5:
        current_matrix = fourth_grade_matrix
        draw_curve(points, current_matrix)


def draw_lines(points):
    for i in range(len(points) - 1):
        line(points[i][0], points[i][1], points[i + 1][0], points[i + 1][1])


def draw_interpolations(points):
    global t
    if len(points) == 1:
        stroke(100)
        return
    new_points = []
    for i in range(len(points) - 1):
        actual_color = get_color(i)
        stroke(actual_color[0], actual_color[1], actual_color[2])
        new_pointX = points[i][0] + (points[i + 1][0] - points[i][0]) * t
        new_pointY = points[i][1] + (points[i + 1][1] - points[i][1]) * t
        new_points.append([new_pointX, new_pointY])
    draw_lines(new_points)
    draw_interpolations(new_points)


def get_color(value):
    value %= 6
    switcher = {
        0: [255, 0, 0],
        1: [255, 255, 0],
        2: [0, 255, 0],
        3: [0, 255, 255],
        4: [0, 0, 255],
        5: [255, 0, 255],
    }
    return switcher.get(value, [255, 255, 255])


def calculate_current_point(points, current_matrix, t):
    t_matrix = []
    pre_result_matrixX = []
    pre_result_matrixY = []
    resultX = 0
    resultY = 0
    for i in range(len(points)):
        pre_result_matrixX.append(0)
        pre_result_matrixY.append(0)

    for i in range(len(points) - 1, -1, -1):
        t_matrix.append(t**i)

    for i in range(len(points)):
        for j in range(len(points)):
            pre_result_matrixX[i] += points[j][0] * current_matrix[i][j]
            pre_result_matrixY[i] += points[j][1] * current_matrix[i][j]
        resultX += pre_result_matrixX[i] * t_matrix[i]
        resultY += pre_result_matrixY[i] * t_matrix[i]
    return [resultX, resultY]


def draw_curve_points(curve_points):
    stroke(51, 51, 255)
    for actualPoint in curve_points:
        point(actualPoint[0], actualPoint[1])
    stroke(170)


def draw_curve(points, current_matrix):
    curve_points = []
    t = 0
    step = 0.001
    actual_time = time.time()
    while (t <= 1):
        actualPoint = calculate_current_point(points, current_matrix, t)
        curve_points.append(actualPoint)
        t += step
    final_time = time.time() - actual_time
    print("Tiempo=" + str(final_time))
    draw_curve_points(curve_points)
