cp = [
    [100, 100],
    [250, 200],
    [400, 100],
]

vp = []

def vm(v, e):
    r = []
    for i in map(lambda x:x*e,v):
        r.append(i)
    return r

def vs(v1, v2):
    r = []
    for i in range(len(v1)):
        r.append(v1[i] + v2[i])                       
    return r

u = 0.0

block_size = 1
width = 500
height = 500
fps = 60*2

def init():
    strokeWeight(4)
    background(255, 255, 255)
    stroke(0)
    noSmooth()

def setup():
    size(width*block_size, height*block_size)
    frameRate(fps)
    init()
    skull()

def line2(p1, p2):
    # stroke(p1[2])
    line(p1[0], p1[1], p2[0], p2[1])

def point2(p):
    # stroke(0)
    point(p[0], p[1])
    
def skull():
    for i, p in enumerate(cp[:-1]):
        line2(cp[i], cp[i+1])

def draw():
    global u
    q1 = vs(vm(cp[0], (1.0 - u)), u*cp[1])
    q1 = vs(vm(cp[1], (1.0 - u)), u*cp[2])
    print(q1)
    u += 0.00001