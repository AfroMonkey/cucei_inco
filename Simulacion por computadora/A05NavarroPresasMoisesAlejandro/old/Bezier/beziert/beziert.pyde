import math, time
points = []
ncp = 3

def setup():
    size(600, 600)
    stroke(100)
    fill(255, 153)
    background(51)
    strokeWeight(1)
    frameRate(144)

def draw():
    pass

def mousePressed():
    global points
    if len(points) < ncp:
        posX = mouseX
        posY = mouseY
        points.append([posX, posY])
    if len(points) == ncp:
            drawCurve(points)

def drawLines(points):
    for i in range(len(points)-1):
        line(points[i][0], points[i][1], points[i+1][0], points[i+1][1])

def drawInterpolations(points):
    global t
    if len(points) == 1:
        stroke(100)
        return
    newPoints = []
    for i in range(len(points)-1):
        actualColor = getColor(i)
        stroke(actualColor[0], actualColor[1], actualColor[2])
        newPointX = points[i][0] + (points[i+1][0] - points[i][0]) * t
        newPointY = points[i][1] + (points[i+1][1] - points[i][1]) * t
        newPoints.append([newPointX, newPointY])
    drawLines(newPoints)
    drawInterpolations(newPoints)

def getColor(value):
    value %= 6
    switcher = {
        0: [255, 0, 0],
        1: [255, 255, 0],
        2: [0, 255, 0],
        3: [0, 255, 255],
        4: [0, 0, 255],
        5: [255, 0, 255],
    }
    return switcher.get(value, [255, 255, 255])

def calculateCurrentPoint(points, t):
    n = len(points) - 1
    nFact = math.factorial(n)
    totalX = 0
    totalY = 0
    for i in range(n+1):
        coef = nFact / (math.factorial(i) * math.factorial(n - i))
        totalX += coef*points[i][0]*((1-t)**(n-i))*(t**i)
        totalY += coef*points[i][1]*((1-t)**(n-i))*(t**i)
    return [totalX, totalY]

def drawCurvePoints(curvePoints):
    stroke(51,51,255)
    for actualPoint in curvePoints:
        point(actualPoint[0], actualPoint[1])
    stroke(170)


def drawCurve(points):
    curvePoints = []
    thisPoints = len(points)
    t = 0
    step = 0.001
    actual_time = time.time()
    while(t <= 1):
        actualPoint = calculateCurrentPoint(points, t)
        curvePoints.append(actualPoint)
        t += step
    final_time = time.time() - actual_time
    print("Tiempo=" + str(final_time))
    drawCurvePoints(curvePoints)
