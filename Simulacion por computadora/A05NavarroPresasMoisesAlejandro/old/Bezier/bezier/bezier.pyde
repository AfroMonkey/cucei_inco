import math
points = []
t = 0.0
step = 0.001
curve_points = []
ncp = 3


def setup():
    global t, curve_points
    size(600, 600)
    stroke(100)
    fill(255, 153)
    background(0)
    strokeWeight(5)
    frameRate(144)
    t = 0
    curve_points = []


def draw():
    global points, t, step, reset, curve_points
    if len(points) < 3:
        return
    if (t <= 1):
        background(0)
        stroke(51, 102, 0)
        draw_lines(points)
        stroke(170)
        draw_curve_points()
        draw_interpolations(points)
        actual_point = get_current_point()
        curve_points.append(actual_point)
        t += step

def mousePressed():
    global points, reset
    if len(points) < ncp:
        posX = mouseX
        posY = mouseY
        points.append([posX, posY])
        point(posX, posY)


def draw_lines(points):
    for i in range(len(points)-1):
        line(points[i][0], points[i][1], points[i+1][0], points[i+1][1])

def draw_interpolations(points):
    if len(points) == 1:
        stroke(100)
        return
    newPoints = []
    for i in range(len(points)-1):
        color = get_color(i)
        stroke(color[0], color[1], color[2])
        newPointX = points[i][0] + (points[i+1][0] - points[i][0]) * t
        newPointY = points[i][1] + (points[i+1][1] - points[i][1]) * t
        newPoints.append([newPointX, newPointY])
    draw_lines(newPoints)
    draw_interpolations(newPoints)

def get_color(value):
    value %= 6
    switcher = {
        0: [255, 0, 0],
        1: [255, 255, 0],
        2: [0, 255, 0],
        3: [0, 255, 255],
        4: [0, 0, 255],
        5: [255, 0, 255],
    }
    return switcher.get(value, [255, 255, 255])


def get_current_point():
    global points
    n = len(points) - 1
    nFact = math.factorial(n)
    totalX = 0
    totalY = 0
    for i in range(n+1):
        coef = nFact / (math.factorial(i) * math.factorial(n - i))
        totalX += coef*points[i][0]*((1-t)**(n-i))*(t**i)
        totalY += coef*points[i][1]*((1-t)**(n-i))*(t**i)
    return [totalX, totalY]


def draw_curve_points():
    global curve_points
    stroke(51,51,255)
    for actual_point in curve_points:
        point(actual_point[0], actual_point[1])
    stroke(170)
