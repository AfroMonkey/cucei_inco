import math
points = []
t = 0.0
step = 0.001
reset = False
curvePoints = []
ncp = 3

secondGradeMatrix = [[1, -2, 1], [-2, 2, 0], [1, 0, 0]]
thirdGradeMatrix = [[-1, 3, -3, 1], [3, -6, 3, 0], [-3, 3, 0, 0], [1, 0, 0, 0]]
fourthGradeMatrix = [[1, -4, 6, -4, 1], [-4,12, -12, 4, 0], [6, -12, 6, 0, 0], [-4, 4, 0, 0, 0], [1, 0, 0, 0, 0]]
currentMatrix = []

def setup():
    size(600, 600)
    stroke(100)
    fill(255, 153)
    background(51)
    strokeWeight(1)
    frameRate(144)

def draw():
    global points, t, step, reset, curvePoints
    if reset:
        background(51)
        drawLines(points)
        reset = False
        t = 0
        curvePoints = []
    if len(points) < 3:
        return
    if (t <= 1):
        background(51)
        stroke(51, 102, 0)
        drawLines(points)
        stroke(170)
        drawCurvePoints()
        drawInterpolations(points)
        actualPoint = calculateCurrentPoint()
        curvePoints.append(actualPoint)
        t += step

def mousePressed():
    global points, reset, currentMatrix
    if len(points) < ncp:
          posX = mouseX
          posY = mouseY
          points.append([posX, posY])
          reset = True
          point(posX, posY)
      if ncp == len(points) and len(points) == 3:
          currentMatrix = secondGradeMatrix
      elif ncp == len(points) and len(points) == 4:
          currentMatrix = thirdGradeMatrix
      elif ncp == len(points) and len(points) == 5:
          currentMatrix = fourthGradeMatrix


def drawLines(points):
    for i in range(len(points)-1):
        line(points[i][0], points[i][1], points[i+1][0], points[i+1][1])

def drawInterpolations(points):
    global t
    if len(points) == 1:
        stroke(100)
        return
    newPoints = []
    for i in range(len(points)-1):
        actualColor = getColor(i)
        stroke(actualColor[0], actualColor[1], actualColor[2])
        newPointX = points[i][0] + (points[i+1][0] - points[i][0]) * t
        newPointY = points[i][1] + (points[i+1][1] - points[i][1]) * t
        newPoints.append([newPointX, newPointY])
    drawLines(newPoints)
    drawInterpolations(newPoints)

def getColor(value):
    value %= 6
    switcher = {
        0: [255, 0, 0],
        1: [255, 255, 0],
        2: [0, 255, 0],
        3: [0, 255, 255],
        4: [0, 0, 255],
        5: [255, 0, 255],
    }
    return switcher.get(value, [255, 255, 255])

def calculateCurrentPoint():
    global points, t, currentMatrix
    tMatrix = []
    preResultMatrixX = []
    preResultMatrixY = []
    resultX = 0
    resultY = 0
    for i in range(len(points)):
        preResultMatrixX.append(0)
        preResultMatrixY.append(0)

    for i in range(len(points)-1, -1, -1):
        tMatrix.append(t**i)

    for i in range(len(points)):
        for j in range(len(points)):
            preResultMatrixX[i] += points[j][0] * currentMatrix[i][j]
            preResultMatrixY[i] += points[j][1] * currentMatrix[i][j]
        resultX += preResultMatrixX[i] * tMatrix[i]
        resultY += preResultMatrixY[i] * tMatrix[i]
    return [resultX, resultY]



def drawCurvePoints():
    global curvePoints
    stroke(51,51,255)
    for actualPoint in curvePoints:
        point(actualPoint[0], actualPoint[1])
    stroke(170)
