import math
points = []
t = 0.0
step = 0.001
reset = False
curve_points = []
ncp = 4

secondGradeMatrix = [
    [1, -2, 1],
    [-2, 2, 0],
    [1, 0, 0]
]
thirdGradeMatrix = [
    [-1, 3, -3, 1],
    [3, -6, 3, 0],
    [-3, 3, 0, 0],
    [1, 0, 0, 0]
]
fourthGradeMatrix = [
    [1, -4, 6, -4, 1],
    [-4, 12, -12, 4, 0],
    [6, -12, 6, 0, 0],
    [-4, 4, 0, 0, 0],
    [1, 0, 0, 0, 0]
]
current_matrix = []


def setup():
    global t, curve_points
    size(600, 600)
    stroke(100)
    fill(255, 153)
    background(0)
    strokeWeight(5)
    frameRate(144)
    t = 0
    curve_points = []


def draw():
    global points, t, step, reset, curve_points
    if len(points) < ncp:
        return
    if (t <= 1):
        background(0)
        stroke(51, 102, 0)
        draw_lines(points)
        stroke(170)
        draw_curve_points()
        draw_interpolations(points)
        actual_point = calculate_current_point()
        curve_points.append(actual_point)
        t += step


def mousePressed():
    global points, reset, current_matrix
    if len(points) < ncp:
        posX = mouseX
        posY = mouseY
        points.append([posX, posY])
        reset = True
        point(posX, posY)
        if ncp == len(points) and len(points) == 3:
            current_matrix = secondGradeMatrix
        elif ncp == len(points) and len(points) == 4:
            current_matrix = thirdGradeMatrix
        elif ncp == len(points) and len(points) == 5:
            current_matrix = fourthGradeMatrix


def draw_lines(points):
    for i in range(len(points) - 1):
        line(points[i][0], points[i][1], points[i + 1][0], points[i + 1][1])


def draw_interpolations(points):
    global t
    if len(points) == 1:
        stroke(100)
        return
    new_points = []
    for i in range(len(points) - 1):
        actual_color = getColor(i)
        stroke(actual_color[0], actual_color[1], actual_color[2])
        new_pointX = points[i][0] + (points[i + 1][0] - points[i][0]) * t
        new_pointY = points[i][1] + (points[i + 1][1] - points[i][1]) * t
        new_points.append([new_pointX, new_pointY])
    draw_lines(new_points)
    draw_interpolations(new_points)


def getColor(value):
    value %= 6
    switcher = {
        0: [255, 0, 0],
        1: [255, 255, 0],
        2: [0, 255, 0],
        3: [0, 255, 255],
        4: [0, 0, 255],
        5: [255, 0, 255],
    }
    return switcher.get(value, [255, 255, 255])


def calculate_current_point():
    global points, t, current_matrix
    tMatrix = []
    pre_result_matrixX = []
    pre_result_matrixY = []
    resultX = 0
    resultY = 0
    for i in range(len(points)):
        pre_result_matrixX.append(0)
        pre_result_matrixY.append(0)

    for i in range(len(points) - 1, -1, -1):
        tMatrix.append(t**i)

    for i in range(len(points)):
        for j in range(len(points)):
            pre_result_matrixX[i] += points[j][0] * current_matrix[i][j]
            pre_result_matrixY[i] += points[j][1] * current_matrix[i][j]
        resultX += pre_result_matrixX[i] * tMatrix[i]
        resultY += pre_result_matrixY[i] * tMatrix[i]
    return [resultX, resultY]


def draw_curve_points():
    global curve_points
    stroke(51, 51, 255)
    for actual_point in curve_points:
        point(actual_point[0], actual_point[1])
    stroke(170)
