from math import ceil, pi, atan2

center = False
circles = []
sqrt_2 = sqrt(2)
mode = False

block_size = 1
width = 500
height = 500
fps = 60*2

def init():
    global circles, center, mode
    center = False
    mode = False
    circles = []
    background(0)    
    fill('#FFFFFF')
    stroke(1)
    rect(0, 0, width*block_size, height*block_size/2)
    rect(0, height*block_size/2, width*block_size, height*block_size/2)
    fill(0)
    text('Bresenham', 20, 0 + 20)
    text('Punto medio', 20, height*block_size/2 + 20)
    fill('#FFFFFF')
    noStroke()

def setup():
    size(width*block_size, height*block_size)
    frameRate(fps)
    init()

def draw():
    if circles:
        background(0)
        for i in range(len(circles)):
            new_r = int(circles[i][1]/2)
            if new_r:
                collition = False
                if circles[i][0][0] + circles[i][1] > width:
                    new_a = pi - circles[i][2]
                    new_a2 = pi + circles[i][2]
                    new_c = [width-new_r, circles[i][0][1]]
                    collition = True
                elif circles[i][0][1] - circles[i][1] < 0:
                    new_a = -circles[i][2]
                    new_a2 = pi + circles[i][2]
                    new_c = [circles[i][0][0], new_r]
                    collition = True
                elif circles[i][0][0] - circles[i][1] < 0:
                    new_a = pi - circles[i][2]
                    new_a2 = pi + circles[i][2]
                    new_c = [new_r, circles[i][0][1]]
                    collition = True
                elif circles[i][0][1] + circles[i][1] > height:
                    new_a = -circles[i][2]
                    new_a2 = pi + circles[i][2]
                    new_c = [circles[i][0][0], height-new_r]
                    collition = True

                if collition:   
                    circles.append((new_c[:], new_r, new_a))
                    circles.append((new_c[:], new_r, pi+circles[i][2]))
                    circles.pop(i)
                else:
                    circles[i][0][0] = circles[i][0][0] + cos(circles[i][2])
                    circles[i][0][1] = circles[i][0][1] + sin(circles[i][2])
                    if mode == 'mid':
                        draw_circle(circles[i][0], circles[i][1])
                    else:
                        draw_circle2(circles[i][0], circles[i][1])
            
def mousePressed():
    global center, circles, mode
    if not mode:
        if mouseY < height*block_size/2:
            mode = 'bresenham'
        else:
            mode = 'mid'
        background(0)
    else:
        if not center:
            background(0)
            set_pixel([mouseX/block_size, mouseY/block_size])
            center = [mouseX/block_size, mouseY/block_size]
        else:
            dx = mouseX/block_size-center[0]
            dy = mouseY/block_size-center[1]
            r = sqrt(dx**2 + dy**2)
            a = atan2(dy, dx)
            circles.append((center, r, a))
            center = False

def set_pixel(p):
    rect(p[0]*block_size, p[1]*block_size, block_size, block_size)

def draw_circle(c, r):
    limit_x = int(r+1/sqrt_2)
    for x in range(limit_x):
        y = round(sqrt(r*r - x*x))
        set_pixel([c[0]+y, c[1]-x])  # 1
        set_pixel([c[0]+x, c[1]-y])  # 2
        set_pixel([c[0]-x, c[1]-y])  # 3
        set_pixel([c[0]-y, c[1]-x])  # 4
        set_pixel([c[0]-y, c[1]+x])  # 5
        set_pixel([c[0]-x, c[1]+y])  # 6
        set_pixel([c[0]+x, c[1]+y])  # 7
        set_pixel([c[0]+y, c[1]+x])  # 8

def draw_circle2(c, r):
    x = r - 1
    y = 0
    dx = 1
    dy = 1
    err = dx - r*2    
    while (x >= y):
        set_pixel([c[0]+x, c[1]+y])
        set_pixel([c[0]+y, c[1]+x])
        set_pixel([c[0]-y, c[1]+x])
        set_pixel([c[0]-x, c[1]+y])
        set_pixel([c[0]-x, c[1]-y])
        set_pixel([c[0]-y, c[1]-x])
        set_pixel([c[0]+y, c[1]-x])
        set_pixel([c[0]+x, c[1]-y])
        if (err <= 0):
            y += 1
            err += dy
            dy += 2
        if (err > 0):
            x -= 1
            dx += 2
            err += (-1*r*2) + dx