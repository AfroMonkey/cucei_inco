e = mode = dir = m = b = start_point = end_point = False
block_size = 10
width = 100
height = 80
fps = 60

def init():
    stroke(1)
    global mode, dir, m, b, start_point, end_point
    e = mode = dx = dy = dir = m = b = start_point = end_point = False
    dx = dy = False
    background(0)
    fill('#FFFFFF')
    rect(0, 0, width*block_size, height*block_size/2)
    rect(0, height*block_size/2, width*block_size, height*block_size/2)
    fill(0)
    text('Bresenham', 20, 0 + 20)
    text('Diferencial', 20, height*block_size/2 + 20)
    fill('#FFFFFF')
    noStroke()

def setup():
    size(width*block_size, height*block_size)
    frameRate(fps)
    init()

def draw():
    if end_point:
        if mode == 'diferencial':
            diferencial()
        else:
            bresenham()
        set_pixel(start_point)

def mousePressed():
    global dx, dy, e, dir, mode, m, b, start_point, end_point
    if not mode:
        if mouseY < height*block_size/2:
            mode = 'bresenham'
        else:
            mode = 'diferencial'
        background(0)
    elif not start_point:
        start_point = [mouseX/block_size, mouseY/block_size]
        set_pixel(start_point)
    elif not end_point:
        if start_point[0] == mouseX/block_size and start_point[1] == mouseY/block_size:
            return

        end_point = [mouseX/block_size, mouseY/block_size]
        set_pixel(end_point)

        m = 'nan'
        if end_point[0] - start_point[0]:
            m = (float(end_point[1]) - start_point[1])/(end_point[0] - start_point[0])
            if mode == 'diferencial':
                b = end_point[1] - m*end_point[0]
                if  abs(m) > 1:
                    if end_point[1] > start_point[1]:
                        dir = 1
                    else:
                        dir = -1
                else:
                    if end_point[0] > start_point[0]:
                        dir = 1
                    else:
                        dir = -1
            else:
                e = 0
                if end_point[0] > start_point[0]:
                    dir = 1
                else:
                    dir = -1
                if end_point[1] > start_point[1]:
                    b = 1
                else:
                    b = -1
        else:
            if end_point[1] > start_point[1]:
                dir = 1
            else:
                dir = -1
    else:
        init()

def set_pixel(point):
    rect(int(point[0])%width * block_size, point[1]%height * block_size, block_size, block_size)

def diferencial():
    global start_point
    if m == 'nan':
         start_point[1] += dir
    elif abs(m) > 1:
        start_point[1] += dir
        start_point[0] = (int(start_point[1] - b)/m)
    else:
        start_point[0] += dir
        start_point[1] = int(m*start_point[0] + b)

def bresenham():
    global e, start_point
    if m == 'nan':
         start_point[1] += dir
    else:
        if abs(m) <= 1:
            e += abs(m)
            if e > 0.5:
                start_point[1] += b
                e -= 1
            start_point[0] += dir
        else:
            e += abs(1/m)
            if e > 0.5:
                start_point[0] += dir
                e -= 1
            start_point[1] += b
    