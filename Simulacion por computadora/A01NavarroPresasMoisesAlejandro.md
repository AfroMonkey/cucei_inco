# Gráficas por computadora
## Linea del tiempo
**Navarro Presas Moisés Alejandro - 11300693**

*   **1947** Surgimiento de las computadoras digitales.
    *   Se construyó  en  la  Universidad  de  Pennsylvania  la  ENIAC  (Electronic Numerical  Integrator  And  Calculator)  que  fue  la  primera  computadora  electrónica.


*   **1959** Sistema de dibujo por computadora, la DAC-1(Design Augmented by Computers).
    *   Fue creado por General Motors e IBM. Permitía al usuario describir un automóvil en 3D con la capacidad de rotar y cambiar el ángulo de la imagen.


*   **1960** Sistema gráfico SAGE (Semi Automatic Ground Environment).
    *   Era un sistema de varias computadoras con características de red que recopilaba los datos de varios radares para procesarla y obtener una sola imagen del espacio aéreo sobre una gran área.


*   **1962** Primer juego de video, llamado “Spacewar”.
    *   Desarrollado por Steve Russell, instalado sobre el DEC PDP-1, fue el primer videojuego que pudo ser jugado en multiples computadoras.


*   **1963** Sketchpad y la pluma de luz.
    *   Sketchpad fue uno de los programas pioneros para la interación humano-computadora, considerado como el antecesor de los programas CAD. Ademas de que impulso el uso de las GUI. Este programa se utilizaba para la creación de trazos utilizando una pluma de luz para la entrada de datos.


*   **1978** "Mathematical elements for computer graphics“.
    *   Rogers David F y Adams Jack A. publican este libro, el cual incluye una introducción a la tecnología de los gráficos por computadora (puntos, lineas, proyecciones, transformaciones, curvas, etc.)


*   **1980** Aportación magistral de Turner Whitted.
    *   Publicó un articulo sobre un nuevo método para representar superficies altamente reflexivas (Ray Tracing).


*   **1993** Jurassic Park.
    *   Revoluciona los efectos por computadora al generar dinosaurios de manera digital.


*   **1999** Autodesk tiene 1.000.000 usuarios de AutoCAD LT.
    *   Autodesk es una compañia multinacional de software para uso de ingeniería, arquitectura, construcción, etc. AutoCAD es un programa de diseño asistido por computadora ampliamente utilizado en áreas de ingeniería para representar modelos físicos.


### Referencias
*   <https://www.uv.mx/personal/gerhernandez/files/2011/04/historia-compuesta.pdf>
*   <https://en.wikipedia.org/wiki/DAC-1>
*   <https://en.wikipedia.org/wiki/Semi-Automatic_Ground_Environment>
*   <https://en.wikipedia.org/wiki/Spacewar!>
*   <https://en.wikipedia.org/wiki/Sketchpad>
*   <https://bibliotecas.ort.edu.uy/bibid/22030>
*   <https://en.wikipedia.org/wiki/Ray_tracing_(graphics)>
*   <https://en.wikipedia.org/wiki/Jurassic_Park>
*   <http://www.asociacionceat.org/aw/5/historia.htm>
