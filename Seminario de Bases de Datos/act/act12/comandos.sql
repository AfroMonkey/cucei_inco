SELECT *
FROM registro
WHERE categoria_id
IN (1, 2);

SELECT *
FROM gasto
WHERE dest_pago
IN (1, 3, 4);

SELECT *
FROM ingreso
WHERE origen_id
IN (2, 5, 8);


SELECT SUM(registro_id)
FROM gasto
GROUP BY dest_pago;

SELECT SUM(monto)
FROM registro
GROUP BY usuario_id;

SELECT SUM(usuario_id)
FROM usuario
GROUP BY alias;


SELECT MAX(monto)
FROM registro
GROUP BY usuario_id;

SELECT MAX(fecha)
FROM registro
GROUP BY usuario_id;

SELECT MAX(usuario_id)
FROM usuario
GROUP BY correo;


SELECT MIN(monto)
FROM registro
GROUP BY usuario_id;

SELECT MIN(fecha)
FROM registro
GROUP BY usuario_id;

SELECT MIN(usuario_id)
FROM usuario
GROUP BY correo;


SELECT AVG(monto)
FROM registro
GROUP BY usuario_id;

SELECT AVG(monto)
FROM registro
GROUP BY categoria_id;

SELECT AVG(monto)
FROM registro
GROUP BY concepto;


SELECT contrasena, COUNT(usuario_id)
FROM usuario
GROUP BY contrasena;

SELECT fecha, COUNT(usuario_id)
FROM registro
GROUP BY fecha;

SELECT concepto, COUNT(usuario_id)
FROM registro
GROUP BY concepto;


SELECT usuario_id, AVG(monto)
FROM registro
GROUP BY usuario_id
HAVING monto > AVG(monto);

SELECT categoria_id, AVG(monto)
FROM registro
GROUP BY categoria_id
HAVING monto > AVG(monto);

SELECT concepto, AVG(monto)
FROM registro
GROUP BY concepto
HAVING monto > AVG(monto);
