PRAGMA foreign_keys = ON;

CREATE TABLE categoria(
  categoria_id INTEGER PRIMARY KEY,
  nombre TEXT,
  inicio NUMERIC,
  fin NUMERIC
);

CREATE TABLE gasto(
  registro_id INTEGER NOT NULL,
  proveedor_id INTEGER NOT NULL,
  dest_pago INTEGER,
  FOREIGN KEY(registro_id) REFERENCES registro(registro_id),
  FOREIGN KEY(proveedor_id) REFERENCES persona(persona_id),
  FOREIGN KEY(dest_pago) REFERENCES persona(persona_id)
);

CREATE TABLE ingreso(
  registro_id INTEGER NOT NULL,
  origen_id INTEGER NOT NULL,
  FOREIGN KEY(registro_id) REFERENCES registro(registro_id),
  FOREIGN KEY(origen_id) REFERENCES persona(persona_id)
);

CREATE TABLE persona(
  persona_id INTEGER PRIMARY KEY,
  r_social TEXT,
  telefono NUMERIC
);

CREATE TABLE registro(
  registro_id INTEGER PRIMARY KEY,
  categoria_id INTEGER NOT NULL,
  usuario_id INTEGER NOT NULL,
  fecha DATE,
  concepto TEXT,
  monto REAL,
  referencia TEXT,
  descripcion TEXT,
  FOREIGN KEY(categoria_id) REFERENCES categoria(categoria_id),
  FOREIGN KEY(usuario_id) REFERENCES usuario(usuario_id)
);

CREATE TABLE usuario(
  usuario_id INTEGER NOT NULL,
  alias TEXT,
  contrasena TEXT,
  correo TEXT,
  FOREIGN KEY(usuario_id) REFERENCES persona(persona_id)
);
