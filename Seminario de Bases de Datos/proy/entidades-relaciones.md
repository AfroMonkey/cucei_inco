
## Entidades

* Alumno
    * carrera, varchar(60)

* Entregable
    * código, int
    * fecha, date
    * calificación, smallint
    * nombre, varchar(45)

* Evaluando
    * valor, smallint
    * nombre, varchar(45)

* Materia
    * código, int [PK]
    * nombre, varchar(75)

* Persona
    * código, int [PK]
    * nombre, varchar(30)
    * apellido_p, varchar(30)
    * apellido_m, varchar(30)

* Profesor
    * departamento, varchar(60)

* Sección
    * código, varchar(3) [PK]
    * nrc, int
    * horario, varchar(8)
    * calendario, varchar(5)
    * cupos, smallint


## Relaciones

* asiste_a       (Alumno   -> Sección)
    * fecha, date
    * tipo, varchar(20)
    
* dividida_en    (Materia  -> Secciones)
* hace           (Alumno   -> Entregable)
* imparte        (Profesor -> Sección)
* parte_de       (Entregable -> Evaluandos)
* pertenece_a    (Entregable -> Sección)
* puede_impartir (Profesor -> Materia)
* tiene          (Sección  -> Evaluandos)
