
## Diccionario de datos

Entidad o relación | Concepto | Tipo de concepto | Tipo de dato | Tamaño | Dominio | Descripción | Ejemplo
-------------------|----------|------------------|--------------|--------|---------|-------------|--------
 | Alumno | Entidad | | | | Las personas que toman una sección de una materia | N/A
Alumno | carrera | atributo | string | 60 | a-z, A-Z, ñ, Ñ | Carrera en la que está enrolado el alumno | Licenciatura en Ingeniería en Computación
 | Entregable | Entidad | | | | Cualquier actividad que realiza y entrega un alumno en una sección | N/A
Entregable | calificación | atributo | smallint | N/A | 0-200 [?] | Valor númerico que determina el porcentaje del puntaje obtenido de la actividad | 100
Entregable | código | atributo | smallint | N/A | 0-2000 [?] | Valor que identifica a una actividad dentro de una sección, cada entregable de una sección debe tener un código único, pero el código puede repetirse en diferentes secciones | 13
Entregable | fecha | atributo | date | N/A | 01/01/1970 al 31/12/2030 | Fecha en que se presentó el entregable | 18/02/2017
Entregable | nombre | atributo | string | 45 | a-z, A-Z, ñ, Ñ, 0-9, #, - | Nombre del entregable | Actividad #2 - Delimitadores
 | Evaluando | Entidad | | | | Apecto de evaluación de una sección | N/A
Evaluando | nombre | atributo | string | 45 | a-z, A-Z, ñ, Ñ | Nombre que identifica el aspecto de evaluación | Prácticas
Evaluando | código | atributo | int | N/A | 0-200 [?] | Valor que indica el puntaje que tendrá ese aspecto de evaluación | 20
 | Materia | Entidad | | | | Un curso que engloba temas de un área específica | N/A
Materia | código | atributo | int | N/A | 0-[?] | Valor númerico que identifica la materia | 235 [?]
Materia | nombre | atributo | string | 75 | a-z, A-Z, ñ, Ñ | Nombre de la materia | Minería de datos
 | Persona | Entidad | | | | Los miembros que participan en la universidad como alumnos o profesores | N/A
Persona | apellido_m | atributo | string | 30 | a-z, A-Z, ñ, Ñ | Apellido materno de la persona | Presas
Persona | apellido_p | atributo | string | 30 | a-z, A-Z, ñ, Ñ | Apellido paterno de la persona | Navarro
Persona | código | atributo | int | N/A | 0-999999999 | Código único que la universidad asigna a cada alumno o profesor | 216661713
Persona | nombre | atributo | string | 30 | a-z, A-Z, ñ, Ñ | Nombre de pila de la persona | Moisés
 | Profesor | Entidad | | | | Las personas que imparten una sección a los alumnos | N/A
Profesor | departamento | atributo | string | 60 | a-z, A-Z, ñ, Ñ | Área de la organización a la que pertenece el profesor | Departamento de computación
 | Sección | Entidad | | | | Instancias específicas de una materia | N/A
Sección | calendario | atributo | string | 5 | A, B, 0-9 | Año y semestre en que se imparte la sección | 2017A
Sección | código | atributo | string | 3 | D, 0-9 | Código que identifica a una sección de otras secciones de la misma materia impartidas en el mismo calendario | D03
Sección | cupos | atributo | smallint | N/A | 0-42 [?] | Cantidad de alumnos que pueden registrarse en la sección | 28
Sección | horario | atributo | string | 8 | L,M,,I,J,V,S,0-9 | Días y horas en que se imparte la sección | LI,92
Sección | nrc | atributo | int | N/A | 0-99999 [?] | Valor único que identifica a una sección particular | 23560
 | asiste_a | Relación | | | | Relación N a M entre Alumno y Sección | N/A
asiste_a | fecha | atributo | date | N/A | 01/01/1970 al 31/12/2030 | Fecha en que se presentó el alumno a la sección | 18/02/2017
asiste_a | tipo | atributo | string | 30 | a-z, A-Z, ñ, Ñ | Tipo de asistencia que tuvo que el alumno en la sección | Asistencia
| dividida_en | Relación | | | | Relación 1 a N entre Materia y Sección | N/A
| hace | Relación | | | | Relación 1 a N entre Alumno y Entregable | N/A
| imparte | Relación | | | | Relación 1 a N entre Profesor y Sección | N/A
| parte_de | Relación | | | | Relación N a 1 entre Entregable y Evaluando | N/A
| pertenece_a | Relación | | | | Relación N a 1 entre Entregable y Sección | N/A
| puede_impartir | Relación | | | | Relación N a M entre Profesor y Materia | N/A
| tiene | Relación | | | | Relación 1 a N entre Sección y Evaluando | N/A