#include <stdio.h>
#include <stdlib.h>

int factorial(unsigned int $a) {
	unsigned int $f = 1;
	__asm__ __volatile(
		"mov $0x0001, %%eax;"
		"mov %1, %%ecx;"
		"c:"
		"mul %%ecx;"
		"dec %%ecx;"
		"jnz c;"
		"movl %%eax, %0;"
		:"=g"($f)
		:"g"($a)
	);
	return $f;
}

int main() {
	unsigned int n;
	scanf("%d", &n);
	printf("%d\n", factorial(n));
	return 0;
}
