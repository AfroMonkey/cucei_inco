org 100h
main:
    mov ax, 0005h       ; Multiplicando
    mov cx, 0004h       ; Multiplicador
    call multiplica     ; Llamada a la funcion multiplica
    ret

multiplica proc
    push bx             ; Almacena el valor de BX
    mov bx, ax          ; BX = AX
    mov ax, 0000h       ; AX = 0 = Producto
    cmp cx, 0000h       ; Si la mupltiplicacion es por 0, finaliza
    jz end_multiplica   ;
suma:
    add ax, bx          ; AX += BX
    dec cx              ; CX--
    jnz suma            ; Si CX > 0: suma
end_multiplica:
    pop bx              ; Recupera el valor de BX inicial
    ret
endp
