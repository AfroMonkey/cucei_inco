org 100h
main:
    mov ax, 0015h   ; Dividendo
    mov dx, 0004h   ; Divisor
    call divide     ; Llamada a la funcion
    ret

divide proc         ; Almacena el valor previo de CX
    push cx         ; Inicializa CX = 0
    mov cx, 0000h   ; Evita division entre 0
    cmp dx, 0000h   ;
    jz end_divide   ;
    add ax, dx      ; Suma un DX para el caso AX == 0
resta:
    sub ax, dx      ; AX -= DX
    inc cx          ; CX++
    cmp dx, ax      ; Si DX <= AX, aun se puede dividir
    jle resta
end_divide:
    dec cx          ; Se contraresta la primer suma
    mov dx, ax      ; DX = AX = Residuo
    mov ax, cx      ; AX = CX = Cociente
    pop cx
    ret
endp
