org 0x100

    mov cx, 0x0001  ; Mueve a CX un 1 (CX indica la columna a manipular)
    mov al, 0x13    ; Modo grafico. 320x200 pixeles con 256 colores
    mov ah, 0x00    ; Para indicar qel modo de video
    int 10h         ; Llama a ala interrupcion 10 (encargada de video)

bucle1:
    mov dx, cx      ; Mueve a DX el valor de CX (DX indica la fila a manipular)
    mov al, color   ; Mueve a AL el valor del color para el pixel
    mov ah, 0x0C    ; Indica que se cambiar� el valor a un pixel
    int 10h         ; Llama a la interrupcion 10

    cmp cx, 101     ; Llegar hasta 100 x 100 (va en diagonal)
    jz fin          ; Brinca a <fin>

    inc cx          ; Incrementa CX
    add color, 0x02 ; Incrementa <color> en 2 unidades (variar el color)
    jmp bucle1      ; Brinca a <bucle1>
fin:
    ret             ; Devuelve el control al SO

color db 0x01       ; Declara e inicializa la variable <color> con 1
