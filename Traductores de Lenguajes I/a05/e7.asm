; Programa que calcula el MCD y MCM de dos numeros decimales (soporta numeros de 8 bits o 1 byte)
org 0x100
    mov cont, 0x01  ; Inicializa <cont> con 1
bucle:
    mov ah, 0x00    ; Inicializa (y limpia) la parte alta del registro A
    mov al, cont    ; Mueve a AL el valor de la variable <cont>
    mov bl, nro1    ; Mueve a BL el valor de la variable <nro1>
    div bl          ; AL = AX/BL (El residuo queda en AH)
    cmp ah, 0x00    ; Compara si el resultado es 0 y sobran 0
    je parte1       ; De ser as�, brinca a <parte1>
bc:                 ; En caso de no ser 0 y sobra 0
    inc cont        ; Incrementa <cont>
    jmp bucle       ; Repite este bloque

parte1:             ; En caso de que la division de 0 y sobren 0
    mov ah, 0x00    ; Se hace practicamente lo mismo que en el bloque anterior
    mov al, cont    ; pero con <nro2>
    mov bl, nro2    ;
    div bl          ;
    cmp ah, 0x00    ;
    je parte2       ;
    jmp bc          ;
parte2:
    mov al, cont    ; Mueve a AL el valor contenido en <cont>
    mov MCM, al     ; Mueve a <MCM>  el valor de AL (que es el de <cont>)
    jmp parte3      ; Brinca a la siguiente parte (innecesario)

parte3:
    mov al, nro1    ; Mueve a AL el valor de <nro1>
    mov bl, nro2    ; Mueve a BL el valor de <nro2>
    mul bl          ; AX = AL*BL
    mov bl, MCM     ; Mueve a BL el valor de <MCM>
    div bl          ; AL = AX/BL (El residuo queda en AH)
    mov MCD, al     ; Mueve a la variable <MCD> el resultado de la division
    ret             ; Devuelve el control al SO

cont db 0x00        ; Cont = contador 
MCM db 0x00         ; En esta variable se guarda el MCM
MCD db 0x00         ; En esta variable se guarda el MCD
nro1 db 48          ; numero1 decimal
nro2 db 60          ; numero2 decimal