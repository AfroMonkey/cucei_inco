#include <stdio.h>

int suma(int $a, int $b);

int main() {<
	printf("%d", suma(2, 5));
	return 0;
}

int suma(int $a, int $b) {
	int $result;
	__asm__ __volatile__(
		"movl %1, %%eax;"
		"movl %2, %%ebx;"
		"addl %%ebx, %%eax;"
		"movl %%eax, %0;" : "=g" ($result):"g"($a),"g"($b)
	);
	return $result;
}
