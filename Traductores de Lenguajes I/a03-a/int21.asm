#fasm#

org  100h

lectura:
    mov ah, 0Ah             ; Modo de entrada de teclado
    mov dx, str_ptr         ; El inicio de la cadena es str_ptr + 1
    int 21h                 ; Llamada a la interrupcion 21

impresion:
    mov bl, [str_ptr + 1]   ; BX = numero de caracteres en leidos
    add bx, str_ptr + 1     ; BX = Direccion del ultimo caracter en str_ptr
    mov [bx + 1], BYTE "$"  ; Establece fin de cadena despues del ultimo leido
    mov ah, 09h             ; Modo de impresion en pantalla
    mov dx, str_ptr + 2     ; Imprime datos leidos desde el primer caracter
    int 21h

    ret

; Variables
str_ptr db 81               ; 80 + enter
