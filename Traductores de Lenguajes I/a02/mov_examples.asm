org 100h
.data
a   db 3 dup (?)            ; Declaracion de arreglo con basura
.code
main:
    mov ax, 3AH             ; Inmediato                 AX = 34h
    mov si, 21h             ; Inmediato                 SI = 21h
    mov bx, ax              ; Registro                  BX = AX
    mov 15h, ax             ; Directo                   Memoria[15h] = AX
    mov [bx], ax            ; Indirecto por registro    Memoria[BX] = AX
    mov [bx + si], ax       ; Base mas indice           Memoria[BX+SI] = AX
    mov a[bx + si], al      ; Relativo base mas indice  Memoria[DireccionInicio(a)+BX+SI] = AL
    mov [bx + 04h], ax      ; Relativo por registro     Memoria[BX+04h] = AX
    mov [bx + 06h + si], ax ; Indice escalado           Memoria[BX+SI+06h] = AX

ret
