org 100h

main:
    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    rol ax, 03h                 ; ax = 0110101010001110b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    rcl ax, 03h                 ; ax = 0110101010001011b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    ror ax, 03h                 ; ax = 0011100110101010b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    rcr ax, 03h                 ; ax = 0101100110101010b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    shl ax, 03h                 ; ax = 0110101010001000b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    sal ax, 03h                 ; ax = 0110101010001000b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    shr ax, 03h                 ; ax = 0001100110101010b & C = 0

    clc                         ; C = 0
    mov ax, 1100110101010001b   ; ax = 1100110101010001b
    sar ax, 03h                 ; ax = 1111100110101010b & C = 0
    ret
