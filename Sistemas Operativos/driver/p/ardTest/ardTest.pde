import processing.serial.*;

Serial port;

int WIDTH = 500;
int HEIGHT = 500;

int old_x = -1;
int old_y = -1;
int x;
int y;
int r;
String s;
String[] vals;

void setup() {
  size(500, 500);
  port = new Serial(this, Serial.list()[0], 9600);
  background(255, 255, 255);
}

void draw() {
  if (port.available() > 0) {
    s = port.readStringUntil('\n');
    if (s != null && s.length() > 0) {
      vals = s.split(",");
      if (vals.length == 4) {
        x = Integer.parseInt(vals[0]);
        y = Integer.parseInt(vals[1]);
        r = Integer.parseInt(vals[2]);
        if (r > 500) {
          background(255, 255, 255);
          old_x = -1;
          delay(500);
        }
        x /= 10;
        x *= 10;
        y /= 10;
        y *= 10;
        x = (int)((x/1023.0)*WIDTH);
        y = (int)((y/1023.0)*HEIGHT);
        println(x, y);
        if (old_x != -1) {
          line(old_x, old_y, x, y);
        }
        old_x = x;
        old_y = y;
      }
    }
  }
}
