import re
import csv

pr = {}

for row in csv.reader(open('pr.csv')):
    pr[row[0]] = row[1]

while (True):
    p = input('>')
    if p == 'exit':
        break
    if p in pr:
        print(pr[p])
    elif re.compile(r"^([a-zA-Z_]+[\w_]*)$").search(p):
        print('Identificador')
    else:
        print('Invalido')
