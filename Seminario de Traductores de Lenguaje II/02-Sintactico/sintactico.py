import re
import csv

pr = {}
pr['\n'] = 'DELIMITER'

for row in csv.reader(open('pr.csv')):
    pr[row[1]] = row[0]

id_searcher = re.compile(r"^([a-zA-Z_]+[\w_]*)$")
int_searcher = re.compile(r"^(\d+)$")
float_searcher = re.compile(r"^(\d+\.\d+)$")

declaration_g = {
    0: [('TYPE', 1)],
    1: [('ID', 2)],
    2: [('\n', 3), (',', 1), ('[', 4)],
    3: [],
    4: [('INT', 5)],
    5: [(']', 6)],
    6: [('\n', 3), (',', 1)],
}
declaration_f = [3, ]
declaration_a = [declaration_g, declaration_f]

input_output_g = {
    0: [('INPUT', 1), ('OUTPUT', 1)],
    1: [('ID', 2)],
    2: [('\n', 3), ('[', 4)],
    3: [],
    4: [('INT', 5)],
    5: [(']', 3)],
}
input_output_f = [3, ]
input_output_a = [input_output_g, input_output_f]

asignation_g = {
    0: [('ID', 1)],
    1: [('EQUAL', 2)],
    2: [('INT', 28), ('FLOAT', 28), ('ID', 28), ('(', 33)],
    28: [('\n', 29), ('OPERATOR_ARITHMETIC', 30)],
    29: [],
    30: [('INT', 31), ('FLOAT', 31), ('ID', 31), ('(', 33)],
    31: [('OPERATOR_ARITHMETIC', 30), ('\n', 29)],
    33: [('INT', 34), ('FLOAT', 34), ('ID', 34)],
    34: [('OPERATOR_ARITHMETIC', 35)],
    35: [('INT', 36), ('FLOAT', 36), ('ID', 36)],
    36: [('OPERATOR_ARITHMETIC', 35), (')', 37)],
    37: [('OPERATOR_ARITHMETIC', 39), ('\n', 29)],
    39: [('(', 33), ('INT', 31), ('FLOAT', 31), ('ID', 31)],
}
asignation_f = [29, ]
asignation_a = [asignation_g, asignation_f]

for_g = {
    0: [('FOR', 1)],
    1: [('(', 2)],
    2: [('ID', 3)],
    3: [('EQUAL', 4)],
    4: [('INT', 28), ('FLOAT', 28), ('ID', 28), ('(', 33)],
    28: [(';', 29), ('OPERATOR_ARITHMETIC', 30)],
    29: [('INT', 45), ('FLOAT', 45), ('ID', 45), ('(', 46)],
    40: [('ID', 41), ('INT', 41), ('FLOAT', 41)],
    41: [('OPERATOR_LOGICAL', 51), (';', 100)],
    43: [('(', 46)],
    44: [('ID', 45), ('INT', 45), ('FLOAT', 45)],
    45: [('OPERATOR_RELATIONAL', 40)],
    46: [('ID', 47), ('INT', 47), ('FLOAT', 47)],
    47: [('OPERATOR_RELATIONAL', 48)],
    48: [('ID', 49), ('INT', 49), ('FLOAT', 49)],
    49: [(')', 50), ('OPERATOR_LOGICAL', 46)],
    50: [(';', 100), ('OPERATOR_LOGICAL', 51)],
    51: [('ID', 45), ('INT', 45), ('FLOAT', 45), ('(', 46)],
    30: [('INT', 31), ('FLOAT', 31), ('ID', 31), ('(', 33)],
    31: [('OPERATOR_ARITHMETIC', 30), (';', 29)],
    33: [('INT', 34), ('FLOAT', 34), ('ID', 34)],
    34: [('OPERATOR_ARITHMETIC', 35)],
    35: [('INT', 36), ('FLOAT', 36), ('ID', 36)],
    36: [('OPERATOR_ARITHMETIC', 35), (')', 37)],
    37: [('OPERATOR_ARITHMETIC', 39), (';', 29)],
    39: [('(', 33), ('INT', 31), ('FLOAT', 31), ('ID', 31)],
    100: [('ID', 101)],
    101: [('EQUAL', 102)],
    102: [('INT', 1028), ('FLOAT', 1028), ('ID', 1028), ('(', 33)],
    1028: [(')', 74), ('OPERATOR_ARITHMETIC', 1030)],
    1030: [('INT', 1031), ('FLOAT', 1031), ('ID', 1031), ('(', 1033)],
    1031: [('OPERATOR_ARITHMETIC', 1030), (')', 74)],
    1033: [('INT', 1034), ('FLOAT', 1034), ('ID', 1034)],
    1034: [('OPERATOR_ARITHMETIC', 1035)],
    1035: [('INT', 1036), ('FLOAT', 1036), ('ID', 1036)],
    1036: [('OPERATOR_ARITHMETIC', 1035), (')', 1037)],
    1037: [('OPERATOR_ARITHMETIC', 1039), (')', 74)],
    1039: [('(', 1033), ('INT', 1031), ('FLOAT', 1031), ('ID', 1031)],
    74: [('{', 75)],
    75: [('\n', 76)],
    76: [],
}
for_f = [76, ]
for_a = [for_g, for_f]

function_g = {
    0: [('TYPE', 1)],
    1: [('ID', 2)],
    2: [('(', 3)],
    3: [(')', 4), ('TYPE', 7)],
    4: [('{', 5)],
    5: [('\n', 6)],
    6: [],
    7: [('ID', 8)],
    8: [(')', 4), (',', 9)],
    9: [('TYPE', 7)],
}
function_f = [6, ]
function_a = [function_g, function_f]

if_g = {
    0: [('IF', 1)],
    1: [('(', 2)],
    2: [('ID', 45), ('INT', 45), ('FLOAT', 45), ('(', 43), ],  # LOGIC_CONDITION
    3: [(')', 4)],
    4: [('{', 5)],
    5: [('\n', 6)],
    6: [],
    40: [('ID', 41), ('INT', 41), ('FLOAT', 41)],
    41: [('OPERATOR_LOGICAL', 51), (')', 4)],
    43: [('(', 46)],
    44: [('ID', 45), ('INT', 45), ('FLOAT', 45)],
    45: [('OPERATOR_RELATIONAL', 40)],
    46: [('ID', 47), ('INT', 47), ('FLOAT', 47)],
    47: [('OPERATOR_RELATIONAL', 48)],
    48: [('ID', 49), ('INT', 49), ('FLOAT', 49)],
    49: [(')', 50), ('OPERATOR_LOGICAL', 46)],
    50: [(')', 4), ('OPERATOR_LOGICAL', 51)],
    51: [('ID', 45), ('INT', 45), ('FLOAT', 45), ('(', 46)],
}
if_f = [6, ]
if_a = [if_g, if_f]

else_g = {
    0: [('}', 1)],
    1: [('ELSE', 2)],
    2: [('{', 3)],
    3: [('\n', 4)],
    4: [],
}
else_f = [4, ]
else_a = [else_g, else_f]

while_g = {
    0: [('WHILE', 1)],
    1: [('(', 2)],
    2: [('ID', 45), ('INT', 45), ('FLOAT', 45), ('(', 43), ],  # LOGIC_CONDITION
    3: [(')', 4)],
    4: [('{', 5)],
    5: [('\n', 6)],
    6: [],
    40: [('ID', 41), ('INT', 41), ('FLOAT', 41)],
    41: [('OPERATOR_LOGICAL', 51), (')', 4)],
    43: [('(', 46)],
    44: [('ID', 45), ('INT', 45), ('FLOAT', 45)],
    45: [('OPERATOR_RELATIONAL', 40)],
    46: [('ID', 47), ('INT', 47), ('FLOAT', 47)],
    47: [('OPERATOR_RELATIONAL', 48)],
    48: [('ID', 49), ('INT', 49), ('FLOAT', 49)],
    49: [(')', 50), ('OPERATOR_LOGICAL', 46)],
    50: [(')', 4), ('OPERATOR_LOGICAL', 51)],
    51: [('ID', 45), ('INT', 45), ('FLOAT', 45), ('(', 46)],
}
while_f = [6, ]
while_a = [while_g, while_f]

return_g = {
    0: [('RETURN', 1)],
    1: [('ID', 2)],
    2: [('\n', 3)],
    3: [],
}
return_f = [3, ]
return_a = [return_g, return_f]

scope_close_g = {
    0: [('}', 1)],
    1: [('\n', 2)],
    2: []
}
scope_close_f = [2, ]
scope_close_a = [scope_close_g, scope_close_f]


automatons = [
    asignation_a,
    declaration_a,
    input_output_a,
    return_a
]


def tokenize(line):
    tokens = []
    token = ''
    # in_logic_term = False
    # in_arithmetic_term = False
    error = False
    for c in line:
        if c in pr and pr[c] in ['DELIMITER_CHAR', 'DELIMITER_INDEX', 'DELIMITER_SCOPE', 'DELIMITER_TERM', 'DELIMITER']:
            if token:
                if token in pr:
                    token = pr[token]
                    # if token in ['OPERATOR_COMPARATIONAL', 'OPERATOR_LOGICAL', 'OPERATOR_RELATIONAL'] and not in_logic_term:
                    #     if tokens[-1] in ['INT', 'FLOAT', 'ID', ')']:
                    #         tokens.pop()
                    #         in_logic_term = True
                    #     else:
                    #         error = True
                elif id_searcher.search(token):
                    token = 'ID'
                elif int_searcher.search(token):
                    token = 'INT'
                elif float_searcher.search(token):
                    token = 'FLOAT'
                else:
                    print(token, 'ERROR', line)
                    error = True
                if not error:
                    tokens.append(token)
            if c != ' ':
                tokens.append(c)
            token = ''
        else:
            token += c
    return tokens


def is_valid(automaton, tokens):
    actual = 0
    t = 0
    for token in tokens:
        for path in automaton[0][actual]:
            if token == path[0]:
                actual = path[1]
                # print(token, actual)
                break
        else:
            return False
        t += 1
    return actual in automaton[1]


to_analyze = open('test.nr', 'r')

stack = []
for l in to_analyze.readlines():
    if len(l.split()):
        tokens = tokenize(l)
        # if tokens[0] == 'FOR':
        #     print(tokens)
        #     is_valid(for_a, tokens)
        # continue
        for automaton in automatons:
            if is_valid(automaton, tokens):
                print("OK   ", l[: -1])
                break
        else:
            error = False
            if is_valid(function_a, tokens):
                stack.append('FUNCTION')
            elif is_valid(for_a, tokens):
                stack.append('FOR')
            elif is_valid(while_a, tokens):
                stack.append('WHILE')
            elif is_valid(if_a, tokens):
                stack.append('IF')
            elif is_valid(else_a, tokens):
                if stack[-1] == 'IF':
                    stack.pop()
                    stack.append('ELSE')
                else:
                    error = True
            elif is_valid(scope_close_a, tokens):
                if len(stack):
                    stack.pop()
                else:
                    error = True
            else:
                error = True
            if error:
                print("ERROR", l[: -1], tokens)
            else:
                print("OK   ", l[: -1])
