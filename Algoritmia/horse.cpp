#include <iostream>

#define WIDTH 8
#define LENGHT 8

char table[LENGHT][WIDTH];

void init();
void print();
void move(int y, int x);
int gen;

int main() {
  init();
  move(0, 0);
  return 0;
}

void init() {
  gen = 0;
  for (int i = 0; i < LENGHT; ++i) {
    for (int j = 0; j < WIDTH; ++j) {
      table[i][j] = '*';
    }
  }
}

void print() {
  std::cout << "gen = " << gen++ << '\n';
  for (int i = 0; i < LENGHT; ++i) {
    for (int j = 0; j < WIDTH; ++j) {
      std::cout << table[i][j];
    }
    std::cout << '\n';
  }
  std::cout << '\n';
}

void move(int y, int x) {
  if (y < 0 || y >= LENGHT || x < 0 || x >= WIDTH)
    return;
  if (table[y][x] == 'V')
    return;
  table[y][x] = 'V';
  print();
  move(y - 2, x - 1); // UL
  move(y - 2, x + 1); // UR
  move(y + 2, x - 1); // DL
  move(y + 2, x + 1); // DR
  move(y - 1, x - 2); // LU
  move(y + 1, x - 2); // LD
  move(y - 1, x + 2); // RU
  move(y + 1, x + 2); // RD
}
