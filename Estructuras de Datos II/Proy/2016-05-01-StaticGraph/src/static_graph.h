#ifndef STATIC_GRAPH_H
#define STATIC_GRAPH_H

#include <iostream>
#include <vector>

#include "static_queue.h"
#include "static_stack.h"

#define NO_CONNECTION -1

class Path
{
public:
    Path(int from, int to, int maxSize);
    ~Path() {}
    void add(pair<int, int> pair);
    void pairsToPath();
    StaticStack<int> *path_;
private:
    int from_;
    int to_;
    StaticStack<pair<int, int>> *pairs_;
};

Path::Path(int from, int to, int maxSize)
{
    from_ = from;
    to_ = to;
    pairs_ = new StaticStack<pair<int, int>>(maxSize*maxSize);
    path_ = new StaticStack<int>(maxSize*maxSize);
}

void Path::add(pair<int, int> pair)
{
    pairs_->push(pair);
}

void Path::pairsToPath()
{
    int v;
    pair<int, int> pair;
    v = to_;
    while(!pairs_->is_empty())
    {
        pair = pairs_->get_top();
        pairs_->pop();
        if (pair.second == v)
        {
            path_->push(v);
            v = pair.first;
            if (v == from_)
            {
                path_->push(v);
                break;
            }
        }
    }
}

class StaticGraph {
private:
    int size_;
    bool weighted_;
    bool directed_;
    int **weights_;
public:
    std::string *ids_;

    StaticGraph(const bool weighted, const bool directed, const int size = 50);
    ~StaticGraph();

    bool set_link(const int start, const int end, int weight = 0);
    int get_vertex(const std::string id);
    std::string get_id(const int pos);
    void depth_first_search(int start);
    void breadth_first_search(int start);
    void depth_first_search(int start, int end);
    void breadth_first_search(int start, int end);
    int  dijkstra(int start, int end);
    void print();
    void printPath(Path &path);
};

StaticGraph::StaticGraph(const bool weighted, const bool directed, const int size) {
    size_ = size;
    weighted_ = weighted;
    directed_ = directed;
    ids_ = new std::string[size_];
    weights_ = new int*[size_];
    for(int i = 0; i < size_; i++) {
        weights_[i] = new int[size_];
        for(int j = 0; j < size_; j++) {
            weights_[i][j] = NO_CONNECTION;
        }
    }
}

StaticGraph::~StaticGraph() {
    delete [] ids_;
    for(int i = 0; i < size_; i++) {
        delete [] weights_[i];
    }
    delete [] weights_;
}


bool StaticGraph::set_link(const int start, const int end, int weight) {
    if(start >= 0 && start < size_ && end >= 0 && end < size_ && (weight >= 0 || weight == NO_CONNECTION)) {
        weight *= weighted_;
        weights_[start][end] = weight;
        if(!directed_) {
            weights_[end][start] = weight;
        }
        return true;
    } else {
        return false;
    }
}

int StaticGraph::get_vertex(const std::string id) {
    for(int i = 0; i < size_; i++) {
        if(ids_[i] == id) {
            return i;
        }
    }
    return -1;
}

std::string StaticGraph::get_id(const int pos) {
    if(pos >= 0 && pos < size_) {
        return ids_[pos];
    } else {
        return "null";
    }
}

void StaticGraph::depth_first_search(int start) {
	bool visited[size_]={false};
	StaticStack<int> stack(size_);
    int vertex, i;

    std::cout << get_id(start) << " ";
    stack.push(start);
    while(!stack.is_empty()) {
        vertex = stack.get_top();
        visited[vertex] = true;
        for(i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                std::cout << get_id(i) << " ";
                stack.push(i);
                visited[i] = true;
                break;
            }
        }
        if(i == size_) {
            stack.pop();
        }
    }
    std::cout << std::endl;
}

void StaticGraph::breadth_first_search(int start) {
	bool visited[size_]={false};
	StaticQueue<int> queue(size_);
    int vertex;

    queue.enqueue(start);
    while(!queue.is_empty()) {
        vertex = queue.deque();
        visited[vertex] = true;
        std::cout << get_id(vertex) << " ";
        for(int i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                queue.enqueue(i);
                visited[i] = true;
            }
        }
    }
    std::cout << std::endl;
}

void StaticGraph::depth_first_search(int start, int end) {
	bool visited[size_]={false};
	StaticStack<int> stack(size_);
    int vertex, i;

    stack.push(start);
    while(!stack.is_empty()) {
        vertex = stack.get_top();
        visited[vertex] = true;
        for(i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                stack.push(i);
                visited[i] = true;
                break;
            }
        }
        if(i == end) {
            break;
        }
        if(i == size_) {
            stack.pop();
        }
    }

    if(stack.is_empty()) {
        std::cout << "No hay ruta" << std::endl;
    } else {
        StaticStack<int> aux(size_);
        while(!stack.is_empty()) {
            aux.push(stack.pop());
        }
        int prev = -1;
        int weight = 0;
        while(!aux.is_empty()) {
            vertex = aux.pop();
            if(prev != -1) {
                weight += weights_[prev][vertex];
                std::cout << "-" << weights_[prev][vertex] << "->";
            }
            std::cout << "(" << get_id(vertex) << ")";
            prev = vertex;
        }
        std::cout << std::endl;
        std::cout << "Peso total = " << weight << std::endl;
    }
}

void StaticGraph::breadth_first_search(int start, int end) {
	bool visited[size_]={false};
	StaticQueue<int> queue(size_);
    int vertex;
    int weights2_[size_][size_];
    bool founded = false;

    for(int i = 0; i < size_; i++) {
        for(int j = 0; j < size_; j++) {
            weights2_[i][j] = -1;
        }
    }

    queue.enqueue(start);
    while(!queue.is_empty()) {
        vertex = queue.deque();
        visited[vertex] = true;
        for(int i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                queue.enqueue(i);
                visited[i] = true;
                weights2_[vertex][i] = weights_[vertex][i];
                if(i == end) {
                    founded = true;
                    break;
                }
            }
        }
        if(founded) {
            break;
        }
    }
    if(founded) {
    	bool visited[size_]={false};
    	StaticStack<int> stack(size_);
        int vertex, i;

        stack.push(start);
        while(!stack.is_empty()) {
            vertex = stack.get_top();
            visited[vertex] = true;
            for(i = 0; i < size_; i++) {
                if(!visited[i] && weights2_[vertex][i] != NO_CONNECTION) {
                    stack.push(i);
                    visited[i] = true;
                    break;
                }
            }
            if(i == end) {
                break;
            }
            if(i == size_) {
                stack.pop();
            }
        }

        if(stack.is_empty()) {
            std::cout << "No hay ruta" << std::endl;
        } else {
            StaticStack<int> aux(size_);
            while(!stack.is_empty()) {
                aux.push(stack.pop());
            }
            int prev = -1;
            int weight = 0;
            while(!aux.is_empty()) {
                vertex = aux.pop();
                if(prev != -1) {
                    weight += weights2_[prev][vertex];
                    std::cout << "-" << weights2_[prev][vertex] << "->";
                }
                std::cout << "(" << get_id(vertex) << ")";
                prev = vertex;
            }
            std::cout << std::endl;
            std::cout << "Peso total = " << weight << std::endl;
        }
    } else {
        std::cout << "No hay ruta" << std::endl;
    }
}

int StaticGraph::dijkstra(int v, int u) {
    std::vector<int> list(size_);
    std::vector<int> dist(size_);
    std::vector<bool> visited(size_);
    Path path(v, u, size_);
    pair<int, int> fromTo;

    for (int i = 0; i < size_; i++)
    {
        dist[i] = 9999999;
    }

    dist[v] = 0;
    list.insert(list.begin(), v);

    while (!list.empty())
    {
        int cur = list[0], inList = 0;
        list.erase(list.begin());

        visited[cur] = true;

        if (cur == u)
        {
            path.pairsToPath();
            printPath(path);
            return dist[u];
        }

        for (int i = 0; i < size_; i++)
        {
            if (weights_[cur][i] != -1 && !visited[i])
            {
                if ((dist[cur] + weights_[cur][i]) < dist[i])
                {
                    dist[i] = dist[cur] + weights_[cur][i];
                    fromTo.first = cur;
                    fromTo.second = i;
                    path.add(fromTo);
                    for (int j = 0; j < size_; j++)
                    {
                        if (list[j] == i)
                        {
                            list.erase(list.begin() + j);
                            break;
                        }
                    }
                    for (int j = 0; j < size_; j++)
                    {
                        if (dist[list[j]] > dist[i])
                        {
                            list.insert(list.begin() + j, i);
                            inList = 1;
                            break;
                        }
                    }
                    if (!inList)
                    {
                        list.push_back(i);
                    }
                }
            }
        }
    }

    std::cout << " No hay ruta." << std::endl;
    return -1;
}

void StaticGraph::print() {
    std::cout << "\t";
    for(int i = 0; i < size_; i++) {
        std::cout << get_id(i) << "\t";
    }
    std::cout << std::endl;
    for(int i = 0; i < size_; i++) {
        std::cout << get_id(i) << "\t";
        for(int j = 0; j < size_; j++) {
            std::cout << weights_[i][j] << "\t";
        }
        std::cout << std::endl;
    }
}

void StaticGraph::printPath(Path &path)
{
    if (path.path_->is_empty())
    {
        std::cout << "No hay ruta" << std::endl;
        return;
    }
    int v = path.path_->get_top();
    path.path_->pop();
    int cost = 0;
    while (!path.path_->is_empty())
    {
        int x = path.path_->get_top();
        path.path_->pop();
        cost += weights_[v][x];
        std::cout << "(" << get_id(v) << ")" << "-" << weights_[v][x] << "->";
        v = x;
    }
    std::cout << get_id (v) << endl;
    std::cout << "Peso total = " << cost     << std::endl;
}

#endif
