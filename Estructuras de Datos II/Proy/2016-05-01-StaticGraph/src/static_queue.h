#ifndef STATIC_QUEUE_H
#define STATIC_QUEUE_H

template <class T>
class StaticQueue {
private:
    T* queue_;
    int front_;
    int back_;
    int size_;
public:
    StaticQueue(int size = 50);
    StaticQueue(StaticQueue<T> &copy);
    ~StaticQueue();

    bool is_empty();
    bool enqueue(T data);
    bool is_full();
    T deque();
    T get_front();
    T get_back();
};

template <class T>
StaticQueue<T>::StaticQueue(int size) {
    size_ = size;
    queue_ = new T[size_];
    front_ = 0;
    back_ = -1;
}

template <class T>
StaticQueue<T>::StaticQueue(StaticQueue<T> &copy) {
    size_ = copy.size_;
    queue_ = new T[size_];
    front_ = copy.front_;
    back_ = copy.back_;
    for(int i = front_; i < back_; i++) {
        queue_[i] = copy.queue_[i];
    }
}

template <class T>
StaticQueue<T>::~StaticQueue() {
    delete [] queue_;
}

template <class T>
bool StaticQueue<T>::is_empty() {
    return front_ > back_;
}

template <class T>
bool StaticQueue<T>::is_full() {
    return back_ - front_ == size_ - 1;
}

template <class T>
bool StaticQueue<T>::enqueue(T data) {
    if(!is_full()) {
        queue_[++back_ % size_] = data;
        return true;
    } else {
      return false;
    }
}

template <class T>
T StaticQueue<T>::deque() {
    if(!is_empty()) {
        return queue_[front_++ % size_];
    }
    return -1;
}

template <class T>
T StaticQueue<T>::get_front() {
    if(!is_empty()) {
        return queue_[front_ % size_];
    }
    return -1;
}

template <class T>
T StaticQueue<T>::get_back() {
    if(!is_empty()) {
        return queue_[back_ % size_];
    }
    return -1;
}

#endif
