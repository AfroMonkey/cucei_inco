#ifndef INDEX_H
#define INDEX_H

#include <fstream>

template <class T>
class Index {
public:
    T key;
    long address;

    Index();
    Index(T key, long address);
    void read(std::ifstream &database);
    void write(std::ofstream &database);
    bool operator<(Index<T> &index);
    bool operator>(Index<T> &index);
};

template <class T>
Index<T>::Index() {
    key = address = -1;
}

template <class T>
Index<T>::Index(T  key, long address) {
    this->key = key;
    this->address = address;
}

template <class T>
void Index<T>::read(std::ifstream &database) {
    database.read((char*)this, sizeof(Index<T>));
}

template <class T>
void Index<T>::write(std::ofstream &database) {
    database.write((char*)this, sizeof(Index<T>));
}

template <class T>
bool Index<T>::operator<(Index<T> &index) {
    return key < index.key;
}

template <class T>
bool Index<T>::operator>(Index<T> &index) {
    return key > index.key;
}

#endif
