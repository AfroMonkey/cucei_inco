#ifndef DOUBLY_LINKED_LIST_H
#define DOUBLY_LINKED_LIST_H

template <class T>
class Node {
public:
    T data;
    Node *next;
    Node *prev;

    Node();
    Node(T &data);
};

template <class T>
Node<T>::Node() {
    next = prev = nullptr;
}

template <class T>
Node<T>::Node(T &data) {
    next = prev = nullptr;
    this->data = data;
}

template <class T>
class DoublyLinkedList {
private:
    Node<T> *header;
public:
    DoublyLinkedList();
    ~DoublyLinkedList();
    /*Capacity*/
    bool isEmpty();
    int size();
    /*Element access*/
    T front();
    T back();
    /*Node<T> access*/
    Node<T>* begin();
    Node<T>* end();
    Node<T>* get(int position);
    Node<T>* get_by_data(T &data);
    /*Modifiers*/
    void insert(T &data);
    void insert2(T &data);
    void pop_front();
    void pop_back();
    void erase(Node<T> *position);
    void clear();
    /*Operations*/
    void remove(const T &data);
    /*Miscellaneous*/
    void for_each(void (*function)(T));
};

template <class T>
DoublyLinkedList<T>::DoublyLinkedList() {
    header = nullptr;
}

template <class T>
DoublyLinkedList<T>::~DoublyLinkedList() {
    clear();
}

/*Capacity*/
template <class T>
bool DoublyLinkedList<T>::isEmpty() {
    return header == nullptr;
}

template <class T>
int DoublyLinkedList<T>::size() {
    int s = 0;
    if(!isEmpty()) {
        Node<T> *aux = header;
        while(aux->next != header) {
            s++;
        }
    }
    return s;
}

/*Element access*/
template <class T>
T DoublyLinkedList<T>::front() {
    T data;
    if(!isEmpty()) {
        data = header->data;
    }
    return data;
}

template <class T>
T DoublyLinkedList<T>::back() {
    T data;
    if(!isEmpty()) {
        data = header->prev->data;
    }
    return data;
}

/*Node<T> access*/
template <class T>
Node<T>* DoublyLinkedList<T>::begin() {
    return header;
}

template <class T>
Node<T>* DoublyLinkedList<T>::end() {
    return header->prev;
}

template <class T>
Node<T>* DoublyLinkedList<T>::get(int position) {
    if(position >= 0 && position < size()) {
        Node<T> *aux;
        aux = header;
        for(int i = 0; i < position; i++) {
            aux = aux->next;
        }
        return aux;
    } else {
        return nullptr;
    }
}

template <class T>
Node<T>* DoublyLinkedList<T>::get_by_data(T &data) {
    if(!isEmpty()) {
        Node<T> *aux = header;
        while(aux->data < data) {
            aux = aux->next;
        }
        if(aux->data == data) {
            return aux;
        } else {
            return nullptr;
        }
    } else {
        return nullptr;
    }
}

/*Modifiers*/
template <class T>
void DoublyLinkedList<T>::insert(T &data) {
    Node<T> *node = new Node<T>(data);
    if(isEmpty()) {
        header = node;
        header->prev = header;
        header->next = header;
    } else if(node->data > header->data){
        node->next = header;
        header->prev->next = node;
        node->prev = header->prev;
        header->prev = node;
        header = node;
    } else {
        Node<T> *aux = header->next;
        while(aux != header && aux->data > node->data) {
            aux = aux->next;
        }
        aux->prev->next = node;
        node->prev = aux->prev;
        node->next = aux;
        aux->prev = node;
    }
}

template <class T>
void DoublyLinkedList<T>::insert2(T &data) {
    Node<T> *node = new Node<T>(data);
    if(isEmpty()) {
        header = node;
        header->prev = header;
        header->next = header;
    } else if(node->data < header->data){
        node->next = header;
        header->prev->next = node;
        node->prev = header->prev;
        header->prev = node;
        header = node;
    } else {
        Node<T> *aux = header->next;
        while(aux != header && aux->data < node->data) {
            aux = aux->next;
        }
        aux->prev->next = node;
        node->prev = aux->prev;
        node->next = aux;
        aux->prev = node;
    }
}

template <class T>
void DoublyLinkedList<T>::pop_front() {
    if(!isEmpty()) {
        if(header->next == header) {
            delete header;
            header = nullptr;
        } else {
            Node<T> *aux = header;
            header = header->next;
            header->prev = aux->prev;
            aux->prev->next = header;
            delete aux;
        }
    }
}

template <class T>
void DoublyLinkedList<T>::pop_back() {
    if(!isEmpty()) {
        if(header->prev == header) {
            delete header;
            header = nullptr;
        } else {
            Node<T> *aux = header->prev;
            header->prev = aux->prev;
            aux->prev->next = header;
            delete aux;
        }
    }
}

template <class T>
void DoublyLinkedList<T>::erase(Node<T> *position) {
    if(!isEmpty()) {
        if(position == header) {
            pop_front();
        } else {
            Node<T> *aux = header;
            while(aux != position) {
                aux = aux->next;
            }
            aux->prev->next = aux->next;
            aux->next->prev = aux->prev;
            delete aux;
        }
    }
}

template <class T>
void DoublyLinkedList<T>::clear() {
    if(!isEmpty()) {
        Node<T> *aux;
        while(header->next != header) {
            aux = header;
            header = header->next;
            erase(aux);
        }
        delete header;
        header = nullptr;
    }
}

/*Operations*/
template <class T>
void DoublyLinkedList<T>::remove(const T &data) {
    if(!isEmpty()) {
        Node<T> *aux = header;
        while(aux->data < data) {
            aux = aux->next;
        }
        if(aux->data == data) {
            erase(aux);
        }
    }
}
/*Miscellaneous*/
template <class T>
void DoublyLinkedList<T>::for_each(void (*function)(T)) {
    if(!isEmpty()) {
        Node<T> *aux = header;
        while(aux->next != header) {
            (*function)(aux->data);
            aux = aux->next;
        }
        (*function)(aux->data);
    }
}

#endif
