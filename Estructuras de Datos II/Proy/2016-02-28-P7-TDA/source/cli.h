#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <cstdlib>

#include "article.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_SORT_BY_PRICE 3
#define OPT_MODIFY 4
#define OPT_REMOVE 5
#define OPT_SEARCH 6
#define OPT_LOAD_TDA 7
#define OPT_ASC 8
#define OPT_DESC 9
#define OPT_EXIT 0

#define ERROR_FILE "Error, no se pudo abrir el archivo\n"
#define ERROR_MISSGIN_ARTICLE "Error, no se pudo encontrar el articulo\n"
#define INVALID_OPTION "Opcion invalida\n"

void show_menu() {
    cout << OPT_ADD << ") Agregar" << endl;
    cout << OPT_SHOW << ") Mostrar" << endl;
    cout << OPT_SORT_BY_PRICE << ") Ordenar por precio" << endl;
    cout << OPT_MODIFY << ") Modificar" << endl;
    cout << OPT_REMOVE << ") Eliminar" << endl;
    cout << OPT_SEARCH << ") Buscar" << endl;
    cout << OPT_LOAD_TDA << ") Cargar TDA" << endl;
    cout << OPT_ASC << ") Ascendente" << endl;
    cout << OPT_DESC << ") Descendente" << endl;
    cout << OPT_EXIT << ") Salir" << endl;
}

void clear_screen() {
    system(CLEAR);
}

void pause() {
    cout << "Presion enetrar para continuar . . . ";
    cin.ignore();
}

int getInt(string msg = ">", int def = -1) {
    int i;
    cout << msg;
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i = def;
    }
    cin.ignore();
    return i;
}

void fill_article(Article &article, int code) {
    article.code = code;
    cout << "Nombre>";
    cin.getline(article.name, 35);
    cout << "Autor>";
    cin.getline(article.author, 35);
    cout << "Categoria>";
    cin.getline(article.categoy, 35);
    cout << "Genero>";
    cin.getline(article.gender, 35);
    cout << "Precio>";
    cin >> article.price;
    cin.ignore();
}

void print_article(const Article &article) {
    cout << "Codigo:" << article.code << endl;
    cout << "Nombre:" << article.name << endl;
    cout << "Autor:" << article.author << endl;
    cout << "Categoria:" << article.categoy << endl;
    cout << "Genero:" << article.gender << endl;
    cout << "Precio:" << article.price << endl;
    cout << endl;
}

void msg(const string m) {
    cout << m;
}

#endif
