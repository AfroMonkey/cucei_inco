/*
 *         Author: Navarro Presas Moises Alejandro
 *           Code: 215861509
 * Subject-Matter: Estructuras de Datos II
 *      Professor: Macias Brambilia Hassem Ruben
 *       Compiler: gcc version 5.3.0 (GCC)
 */
#include "student.h"
#include "cli.h"
#include "db_controller.h"

void opt_add();
void opt_show();
void opt_modify();
void opt_delete();

int main()
{
    create_database();

    int opt;
    do {
        clear_screen();
        print_menu();
        opt = get_int();
        switch (opt)
        {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_MODIFY:
                opt_modify();
                break;
            case OPT_DELETE:
                opt_delete();
                break;
            case OPT_EXIT:
                break;
            default:
                msg(ERR_INVALID);
        }
        pause();
    } while (opt != OPT_EXIT);
    return 0;
}

void opt_add()
{
    msg(HEADER_ADD);
    Student student;
    fill_student(student);
    if (add_student(student))
    {
        msg(MSG_DONE);
    }
    else
    {
        msg(ERR_FULL);
    }
}

void opt_show()
{
    msg(HEADER_SHOW);
    show_students();
}

void opt_modify()
{
    msg(HEADER_MODIFY);
    char* name = get_string("    Nombre>");
    int location = get_student_location(name);
    if (location != -1)
    {
        print_student(read_student(location));
        Student student;
        fill_student(student);
        if (strcmp(student.name_, name) == 0)
        {
            write_student(student, location);
        }
        else
        {
            delete_student(name, location);
            add_student(student);
        }
    }
    else
    {
        msg(ERR_MISSING);
    }
}

void opt_delete()
{
    msg(HEADER_DELETE);
    char* name = get_string("    Nombre>");
    if (delete_student(name))
    {
        msg(MSG_DONE);
    }
    else
    {
        msg(ERR_MISSING);
    }
    delete name;
}
