#ifndef STUDENT_H
#define STUDENT_H

class Student
{
public:
    char name_[30];
    char address_[30];
    char city_[30];
    char tel_[10];
    char email_[30];

    Student();
};

Student::Student()
{
    name_[0] = '\x0';
    address_[0] = '\x0';
    city_[0] = '\x0';
    tel_[0] = '\x0';
    email_[0] = '\x0';
}

#endif
