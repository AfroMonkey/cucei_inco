#ifndef DISPERSSION_H
#define DISPERSSION_H

#include <iostream>

#include "student.h"
#include "config.h"

int get_base(char *name)
{
    int base = 0;

    for (int i = 0; name[i] != '\x0'; i++)
    {
        base += name[i];
    }
    base %= ROWS;
    base *= (sizeof(int) + COLUMNS * sizeof(Student));

    return base;
}

#endif
