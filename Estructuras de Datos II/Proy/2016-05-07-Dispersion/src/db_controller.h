#ifndef DB_CONTROLLER_H
#define DB_CONTROLLER_H

#include <fstream>
#include <cstring>

#include "config.h"
#include "student.h"
#include "cli.h"
#include "disperssion.h"


void create_database()
{
    {
        std::ifstream database(DB_LOCATION);
        if (database)
            return;
    }
    std::ofstream database(DB_LOCATION, std::ios::app);
    Student student;
    int z = 0;
    for (int i = 0; i < ROWS; i++)
    {
        database.write((char*)&z, sizeof(int));
        for (int j = 0; j < COLUMNS; j++)
        {
            database.write((char*)&student, sizeof(student));
        }
    }
}

int get_student_location(char *name)
{
    int base = get_base(name);
    std::ifstream database(DB_LOCATION);
    Student aux;
    long int i;

    database.seekg(base);
    database.read((char*)&i, sizeof(int));
    if (i != 0)
    {
        for (int j = 0; j < i; j++)
        {
            database.read((char*)&aux, sizeof(Student));
            if (database.eof())
            {
                database.close();
                return -1;
            }
            if (strcmp(aux.name_, name) == 0)
            {
                print_student(aux);
                if (get_int(0, "\tEs este (1/0)?"))
                {
                    j = database.tellg();
                    j -= sizeof(Student);
                    database.close();
                    return j;
                }
            }
        }
    }
    database.close();
    return -1;
}

Student read_student(int location)
{
    Student student;
    std::ifstream database(DB_LOCATION);

    database.seekg(location);
    database.read((char*)&student, sizeof(student));

    database.close();

    return student;
}

void write_student(const Student &student, int location)
{
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);

    database.seekp(location);
    database.write((char*)&student, sizeof(student));

    database.close();
}

bool add_student(Student &student)
{
    int base = get_base(student.name_);
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);
    Student aux;
    int i;

    database.seekp(base);
    database.read((char*)&i, sizeof(int));
    if (i < COLUMNS)
    {
        database.seekp(base + sizeof(int) + i * sizeof(student));
        i++;
        database.write((char*)&student, sizeof(student));
        database.seekp(base);
        database.write((char*)&i, sizeof(int));
        database.close();

        return true;
    }
    else
    {

        database.close();
        return false;
    }
}

void show_students()
{
    Student student;
    int j;
    bool empty = true;
    std::ifstream database(DB_LOCATION);

    for (int i = 0; i < ROWS; i++)
    {
        database.seekg(i*(sizeof(student) * COLUMNS + sizeof(int)));
        for (database.read((char*)&j, sizeof(int)); j > 0; j--)
        {
            empty = false;
            database.read((char*)&student, sizeof(student));
            print_student(student);
        }
    }
    if (empty)
    {
        msg(MSG_EMPTY);
    }
}

bool delete_student(char* name, int location = -1)
{
    if (location == -1) location = get_student_location(name);
    if (location != -1)
    {
        fstream database(DB_LOCATION, ios::in | ios::out);
        int base = get_base(name);
        location -= base;
        location -= sizeof(int);
        location /= sizeof(Student);
        int i;
        Student aux;

        database.seekg(base);
        database.read((char*)&i, sizeof(int));
        for (int j = location + 1; j < i; j++)
        {
            database.seekg(base + sizeof(int) + j * sizeof(Student));
            database.read((char*)&aux, sizeof(Student));
            database.seekp(base + sizeof(int) + (j-1) * sizeof(Student));
            database.write((char*)&aux, sizeof(Student));
        }
        i--;
        database.seekp(base);
        database.write((char*)&i, sizeof(int));
        database.close();
        return true;
    }
    else
    {
        return false;
    }
}

#endif
