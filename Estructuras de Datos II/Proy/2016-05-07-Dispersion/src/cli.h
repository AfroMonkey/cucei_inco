#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <cstdlib>

#include "student.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_MODIFY 3
#define OPT_DELETE 4
#define OPT_EXIT 0

#define ERR_DUPLICATED " Error, clave duplicada\n"
#define ERR_FULL " Error, base de datos llena\n"
#define ERR_MISSING " Error, no se encontro el producto\n"
#define ERR_INVALID " Opcion invalida\n"
#define MSG_DONE " Listo\n"
#define HEADER_ADD "\tAgregar\n"
#define HEADER_SHOW "\tMostrar\n\n"
#define HEADER_MODIFY "\tModificar\n\n"
#define HEADER_DELETE "\tEliminar\n\n"
#define MSG_EMPTY " Archivo vacio\n\n"

void print_menu()
{
    cout << " " << OPT_ADD << ")" << "Agregar" << endl;
    cout << " " << OPT_SHOW << ")" << "Mostrar" << endl;
    cout << " " << OPT_MODIFY << ")" << "Modificar" << endl;
    cout << " " << OPT_DELETE << ")" << "Eliminar" << endl;
    cout << " " << OPT_EXIT << ")" << "Salir" << endl;
}

int get_int(int d = -1, string msg = " >")
{
    int i;
    cout << msg;
    if (!(cin >> i))
    {
        i = d;
        cin.clear();
        cin.ignore();
    }
    cin.ignore();
    return i;
}

char* get_string(string msg = " >")
{
    char *s = new char[30];
    cout << msg;
    cin.getline(s, 30);
    return s;
}

void fill_student(Student &student)
{
    cout << endl;
    cout << "    Nombre> ";
    cin.getline(student.name_, 30);
    cout << " Direccion> ";
    cin.getline(student.address_, 30);
    cout << "    Ciudad> ";
    cin.getline(student.city_, 30);
    cout << "  Telefono> ";
    cin.getline(student.tel_, 10);
    cout << "     Email> ";
    cin.getline(student.email_, 30);
}

void print_student(const Student &student)
{
    cout << endl;
    cout << "    Nombre: " << student.name_ << endl;
    cout << " Direccion: " << student.address_ << endl;
    cout << "    Ciudad: " << student.city_ << endl;
    cout << "  Telefono: " << student.tel_ << endl;
    cout << "     Email: " << student.email_ << endl;
}

void msg(const string s)
{
    cout << s;
}

void pause()
{
    cout << endl;
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
}

void clear_screen()
{
    system(CLEAR);
}

#endif
