/*
 * Author: Navarro Presas Moises Alejandro
 * Reg: 215861509
 * Subject: Estructuras de Datos II, D07
 * Professor: Macias Brambilia Hassem Ruben
 * Compiler: mingw (gcc version 5.3.0 (GCC))
 * Date: 2016-01-28
 */

#include "employee.h"
#include "data_base_controller.h"
#include "command_line_interface.h"

void menu_add();
void menu_show();
void menu_modiffy();
void menu_delete();
void menu_search();

int main() {
    int opt;
    do {
        show_menu();
        opt = get_int(-1);
        switch(opt) {
            case OPT_ADD:
                menu_add();
                break;
            case OPT_SHOW:
                menu_show();
                break;
            case OPT_MODIFY:
                menu_modiffy();
                break;
            case OPT_DELETE:
                menu_delete();
                break;
            case OPT_SEARCH:
                menu_search();
                break;
            case OPT_EXIT:
                break;
            default:
                msg("Opcion invalida\n");
                break;
        }
        pause();
        clear_screen();
    } while(opt != OPT_EXIT);
    return 0;    
}

void menu_add() {
    Employee *employee = new Employee;
    string data_base_location = get_data_base_location();

    fill_employee(employee);
    employee->code = to_string(get_last_code(data_base_location)+1);
    if(unique_rfc(employee->rfc, data_base_location)) {
        if(write_employee(employee, data_base_location)) {
            msg("Empleado añadido al archivo\n");
        } else {
            msg("Error al escribir en el archivo\n");
        }
    } else {
        msg("Error, RFC repetido\n");
    }
    delete employee;
}

void menu_show() {
    show_employees(get_data_base_location());
}

void menu_modiffy() {
    string data_base_location = get_data_base_location();
    Employee *employee = search_employee(get_code(), data_base_location);
    Employee *new_employee = new Employee;
    if(employee != nullptr) {
        show_employee(employee);
        msg("Introduzca los nuevos datos\n");
        fill_employee(new_employee);
        new_employee->code = employee->code;
        if(unique_rfc(new_employee->rfc, data_base_location)) {
            if(replace_employee(stoi(employee->code, nullptr, 10), new_employee, data_base_location)) {
                msg("Empleado modificado\n");
            } else {
                msg("Error, no se pudo modificar el empleado\n");
            }
        } else {
            msg("Error, RFC repetido\n");
        }
    } else {
        msg("Error, no se pudo encontrar al empleado\n");
    }
    delete new_employee;
}

void menu_delete() {
    string data_base_location = get_data_base_location();
    Employee *employee = search_employee(get_code(), data_base_location);
    if(employee != nullptr) {
        show_employee(employee);
        msg("Seguro que desea eliminar este emeplado (1/0)");
        if(get_int(0)) {
            if(remove_employee(stoi(employee->code, nullptr, 10), data_base_location)) {
                msg("Empleado eliminado\n");
            } else {
                msg("Error, no se pudo modificar el empleado\n");
            }
        } else {
            msg("Operacion abortada\n");
        }
    } else {
        msg("Error, no se pudo encontrar al empleado\n");
    }
}

void menu_search() {
    Employee *employee = search_employee(get_code(), get_data_base_location());
    if(employee != nullptr) {
        show_employee(employee);
    } else {
        msg("Error, no se pudo encontrar al empleado\n");
    }
}
