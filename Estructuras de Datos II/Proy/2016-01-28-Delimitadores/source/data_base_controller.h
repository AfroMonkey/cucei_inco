#ifndef DATA_BASE_CONTROLLER
#define DATA_BASE_CONTROLLER

#include <fstream>
#include "employee.h"
#include "command_line_interface.h" //show_employe(Employe *);

using namespace std;

#define CHAR_DELIMITER '|'

Employee* get_employe(ifstream &data_base) {
    Employee *employee = new Employee;
    char buffer[100];

    data_base.getline(buffer, 100, CHAR_DELIMITER);
    employee->code = buffer;
    data_base.getline(buffer, 100, CHAR_DELIMITER);
    employee->name = buffer;
    data_base.getline(buffer, 100, CHAR_DELIMITER);
    employee->date_admission = buffer;
    data_base.getline(buffer, 100, CHAR_DELIMITER);
    employee->rfc = buffer;
    data_base.getline(buffer, 100, CHAR_DELIMITER);
    employee->job = buffer;
    data_base.getline(buffer, 100, CHAR_DELIMITER);
    employee->observations = buffer;

    if(data_base.eof()) {
        return nullptr;
    } else {
        return employee;
    }
}

int get_last_code(const string &data_base_location) {
    int code = -1;
    ifstream data_base(data_base_location);

    if(data_base) {
        Employee *employee;
        while(true) {
            employee = get_employe(data_base);
            if(employee != nullptr) {
                code = stoi(employee->code, nullptr, 10);
            } else {
                break;
            }
        }
        data_base.close();
    }
    if(code == -1) {
        if(data_base_location == "Directivos.db") {
            code = 199;
        } else {
            code = 99;
        }
    }
    return code;
}

bool unique_rfc(const string &rfc, const string &data_base_location) {
    ifstream data_base(data_base_location);

    if(data_base) {
        Employee *employee;
        while(true) {
            employee = get_employe(data_base);
            if(employee != nullptr) {
                if(employee->rfc == rfc) {
                    break;
                }
            } else {
                break;
            }
        }
        if(employee != nullptr && employee->rfc == rfc) {
            return false;
        }
    }
    return true;

}

bool write_employee(Employee *employee, const string &data_base_location) {
    ofstream data_base(data_base_location, ios::app);
    if(data_base) {
        data_base   << employee->code << CHAR_DELIMITER
                    << employee->name << CHAR_DELIMITER
                    << employee->date_admission << CHAR_DELIMITER
                    << employee->rfc << CHAR_DELIMITER
                    << employee->job << CHAR_DELIMITER
                    << employee->observations << CHAR_DELIMITER;
        data_base.close();
        return true;
    } else {
        return false;
    }
}

Employee* search_employee(int code, const string &data_base_location) {
    ifstream data_base(data_base_location);

    if(code != -1 && data_base) {
        Employee *employee;
        while(true) {
            employee = get_employe(data_base);
            if(employee != nullptr) {
                if(stoi(employee->code, nullptr, 10) >= code) {
                    break;
                }
            } else {
                break;
            }
        }
        data_base.close();
        if(employee != nullptr && employee->code == to_string(code)) {
            return employee;
        }
    }
    return nullptr;
}

bool remove_employee(int code, const string &data_base_location) {
    ifstream data_base(data_base_location);

    if(data_base) {
        string s_code = to_string(code);
        const string temp_data_base_location = "temp.db";
        Employee *employee;

        while(true) {
            employee = get_employe(data_base);
            if(employee != nullptr) {
                if(employee->code != s_code) {
                    write_employee(employee, temp_data_base_location);
                }
            } else {
                break;
            }
        }
        data_base.close();
        remove(data_base_location.c_str());
        rename(temp_data_base_location.c_str(), data_base_location.c_str());
        return true;
    }
    return false;
}

bool replace_employee(int code, Employee *new_employee, const string &data_base_location) {
    ifstream data_base(data_base_location);

    if(data_base) {
        string s_code = to_string(code);
        const string temp_data_base_location = "temp.db";
        Employee *employee;

        while(true) {
            employee = get_employe(data_base);
            if(employee != nullptr) {
                if(employee->code == s_code) {
                    write_employee(new_employee, temp_data_base_location);
                } else {
                    write_employee(employee, temp_data_base_location);
                }
            } else {
                break;
            }
        }
        data_base.close();
        remove(data_base_location.c_str());
        rename(temp_data_base_location.c_str(), data_base_location.c_str());
        return true;
    }
    return false;
}

void show_employees(const string &data_base_location) { //Search how to remove this
    ifstream data_base(data_base_location);

    if(data_base) {
        Employee *employee;

        while(true) {
            employee = get_employe(data_base);
            if(employee != nullptr) {
                show_employee(employee);
            } else {
                break;
            }
        }
        data_base.close();
    }
}

#endif
