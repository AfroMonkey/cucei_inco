#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <iostream>

using namespace std;

class Employee {
public:
    Employee();

    string code;
    string name;
    string date_admission;
    string rfc;
    string job;
    string observations;
};

Employee::Employee() {
    code = -1;
    name = "Staff";
    date_admission = "1900-01-01";
    rfc = "XXXXIIIIXXX";
    job = "Foo";
    observations = "Foo";
}
#endif
