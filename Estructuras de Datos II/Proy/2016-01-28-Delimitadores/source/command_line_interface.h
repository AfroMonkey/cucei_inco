#ifndef COMMAND_LINE_INTERFACE_H
#define COMMAND_LINE_INTERFACE_H

#include <iostream>
#include <cstdlib>
#include "employee.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_MODIFY 3
#define OPT_DELETE 4
#define OPT_SEARCH 5
#define OPT_EXIT 0

void pause() {
    cout << "Presione entrar para continuar . . ." << endl;
    cin.ignore();
}

void clear_screen() {
    system(CLEAR);
}

void show_menu() {
    cout << OPT_ADD << ") Agregar" << endl;
    cout << OPT_SHOW << ") Mostrar" << endl;
    cout << OPT_MODIFY << ") Modificar" << endl;
    cout << OPT_DELETE << ") Eliminar" << endl;
    cout << OPT_SEARCH << ") Buscar" << endl;
    cout << OPT_EXIT << ") Salir" << endl;
}

int get_int(int def) {
    int i;
    cout << ">";
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i = def;
    }
    cin.ignore();
    return i;
}

void msg(const string &m) {
    cout << m;
}

void fill_employee(Employee *employee) {
    cout << "Nombre>";
    getline(cin, employee->name);
    cout << "Fecha de admision (YYYY-MM-DD)>";
    cin >> employee->date_admission;
    cout << "RFC (XXXXIIIIIIXXX)>";
    cin >> employee->rfc;
    cout << "Puesto>";
    getline(cin, employee->job);
    cout << "Observaciones>";
    getline(cin, employee->observations);
}

void show_employee(Employee *employee) {
    cout << "Codigo de empleado: " << employee->code << endl;
    cout << "Nombre: " << employee->name << endl;
    cout << "Fecha de admision: " << employee->date_admission << endl;
    cout << "RFC: " << employee->rfc << endl;
    cout << "Puesto: " << employee->job << endl;
    cout << "Observaciones: " << employee->observations << endl;
    cout << endl;
}

string get_data_base_location() {
    string data_base_location;

    cout << "1) Operativo" << endl;
    cout << "2) Directivo" << endl;
    switch(get_int(-1)) {
        case 2:
            data_base_location = "Directivos.db";
            break;
        default:
            cout <<"Opcion invalida, base de datos por defecto: Operativos.db" << endl;
        case 1:
            data_base_location = "Operativos.db";
            break;
    }
    return data_base_location;
}

int get_code() {
    cout << "Codigo";
    return get_int(-1);
}
#endif
