/*
 *         Author: Navarro Presas Moises Alejandro
 *           Code: 215861509
 * Subject-Matter: Estructuras de Datos II
 *      Professor: Macias Brambilia Hassem Ruben
 *       Compiler: gcc version 5.3.0 (GCC)
 */

#include <iostream>
#include <fstream>
#include <algorithm> /*std::sort*/
#include <list>
#include <map>
#include <cstdlib>

#include "huffman_node.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

void pause_clear();

int main()
{
    int opt;
    std::cout << "1) Encriptar" << "\n";
    std::cout << "2) Desencriptar" << "\n";
    std::cin >>opt;
    switch (opt)
    {
    case 1:
        {
            std::string file_path;

            std::cout << "Ingrese el archivo de entrada>";
            std::cin >> file_path;
            std::ifstream input(file_path);
            if (!input)
            {
                std::cout << "No se pudo abrir el archivo" << "\n";
                return 1;
            }

            std::cout << "Ingrese el archivo donde se guardara el diccionario>";
            std::cin >> file_path;
            std::ofstream keys_file(file_path, std::ios::trunc);
            if (!keys_file)
            {
                std::cout << "No se pudo abrir el archivo" << "\n";
                return 2;
            }

            std::cout << "Ingrese el archivo donde se guardara la cadena binaria>";
            std::cin >> file_path;
            std::ofstream output(file_path, std::ios::trunc | std::ios::binary);
            if (!output)
            {
                std::cout << "No se pudo abrir el archivo" << "\n";
                return 3;
            }
            std::cin.ignore();
            pause_clear();

            std::string content((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>()); //Read all the file
            input.close();
            std::string sorted(content);
            std::sort(sorted.begin(), sorted.end()); //Sort a copy of the file

            std::list<HuffmanNode*> tree;
            for (int i = 0, reps = 1; sorted[i]; i++, reps++)
            {
                if (sorted[i] != sorted[i+1]) //I have a change, is a new node
                {
                    HuffmanNode *node = new HuffmanNode(reps, sorted[i]);
                    tree.push_back(node);
                    reps = 0;
                }
            }

            /* Mostrar Lista Sin Ordenar */
            std::cout << "***Lista sin ordenar***" << "\n";
            for (std::list<HuffmanNode*>::iterator it = tree.begin(); it != tree.end(); ++it)
            {
                std::cout << ((HuffmanNode*)*it)->c_ << "(" << ((HuffmanNode*)*it)->weight_ << ")->";
            }
            std::cout << "\n";
            pause_clear();

            tree.sort(HuffmanNode::cmp); //Sort the nodes
            /* Mostrar Lista Ordenada */
            std::cout << "***Lista ordenada***" << "\n";
            for (std::list<HuffmanNode*>::iterator it = tree.begin(); it != tree.end(); ++it)
            {
                std::cout << ((HuffmanNode*)*it)->c_ << "(" << ((HuffmanNode*)*it)->weight_ << ")->";
            }
            std::cout << "\n";
            pause_clear();


            while (tree.size() > 1) //We must have only one node, the root
            {
                HuffmanNode *node = new HuffmanNode();

                node->left_ = *tree.begin(); //Get the first node of the list
                tree.pop_front();
                node->right_ = *tree.begin();
                tree.pop_front();
                node->weight_ = node->left_->weight_ + node->right_->weight_;

                tree.push_front(node);
                tree.sort(HuffmanNode::cmp);
            }

            std::map<char, std::string> keys;

            /* Mostrar diccionario */
            std::cout << "***Diccionario de datos***" << "\n";
            (*tree.begin())->get_dictionary(keys);
            pause_clear();
            (*tree.begin())->free(); //We dont need more the tree, only the keys map
            delete *tree.begin();

            for (std::map<char, std::string>::iterator it = keys.begin(); it != keys.end(); ++it)
            {
                keys_file << it->first << it->second << std::endl; //Write the keys in the key_file
            }
            keys_file.close();

            /* Compress */
            std::string bits; //codes generated with the keys and input_file
            for (int i = 0; content[i]; i++)
            {
                bits += keys[content[i]];
            }

            while (bits.length() % 8) //To complete the byte
            {
                bits += '0';
            }
            /* Cadena de bits */
            std::cout << "***Cadena binaria***" << "\n";
            std::cout << bits << "\n";
            pause_clear();

            /* Output */
            output << bits;
            output.close();
            std::cout << "Archivo guardado" << "\n";
            break;
        }
    case 2:
        {
            std::string file_path;

            std::cout << "Ingrese el archivo de entrada>";
            std::cin >> file_path;
            std::ifstream input(file_path);
            if (!input)
            {
                std::cout << "No se pudo abrir el archivo" << "\n";
                return 1;
            }

            std::cout << "Ingrese el archivo diccionario>";
            std::cin >> file_path;
            std::ifstream keys_file(file_path);
            if (!keys_file)
            {
                std::cout << "No se pudo abrir el archivo" << "\n";
                return 2;
            }

            std::cout << "Ingrese el archivo donde se guardara la informacion descomprimida>";
            std::cin >> file_path;
            std::ofstream output(file_path, std::ios::trunc | std::ios::binary);
            if (!output)
            {
                std::cout << "No se pudo abrir el archivo" << "\n";
                return 3;
            }
            std::cin.ignore();

            /* Dictionary */
            std::map<std::string, char> keys;
            char c;
            std::string k;
            while (true)
            {
                keys_file.read((char*)&c, sizeof(char));
                if (keys_file.eof()) break;
                getline(keys_file, k);
                keys[k] = c;
            }
            keys_file.close();

            /* Input */
            std::string bits;
            while (true)
            {
                input.read((char*)&c, sizeof(char));
                if (input.eof()) break;
                bits += c;
            }
            input.close();

            /* Output */
            int i;
            std::string may_c;
            for (i = 0; i <= (int)bits.length(); i++)
            {
                may_c += bits[i];
                if (keys.find(may_c) != keys.end())
                {
                    output.write((char*)&keys[may_c], sizeof(char));
                    may_c = "";
                }
            }

            if (keys.find(may_c) == keys.end())
            {
                while (keys.find(may_c) == keys.end() && may_c != "")
                {
                    may_c = may_c.substr(0, may_c.size()-1);
                }
                if (may_c != "") output.write((char*)&keys[may_c], sizeof(char));
            }

            output.close();
            pause_clear();
        }
    }

    return 0;
}

void pause_clear()
{
    std::cout << "Presione entrar para continuar . . .";
    std::cin.ignore();
    system(CLEAR);
}
