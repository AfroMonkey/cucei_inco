#ifndef COMMAND_LINE_INTERFACE_H
#define COMMAND_LINE_INTERFACE_H

#include <iostream>
#include <cstdlib>
#include "client.h"
#include "rental.h"
#include "vehicle.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADMIN_CLIENTS 1
#define OPT_ADMIN_RENTALS 2
#define OPT_ADMIN_VEHICLES 3
#define OPT_EXIT 0
#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_MODIFY 3
#define OPT_REMOVE 4

#define ERROR_INVALID_OPTION "Error, opcion invalida\n"
#define ERROR_FILE "Error, no se puedo abrir el archivo\n"
#define ERROR_CLIENT_NOT_FOUND "Error, no se encontro el cliente\n"
#define ERROR_VEHICLE_NOT_FOUND "Error, no se encontro el vehiculo\n"
#define HEADER_MAIN "** RentaCar **\n"
#define HEADER_CLIENTS "** Administrar clientes **\n"
#define HEADER_RENTALS "** Administrar alquileres **\n"
#define HEADER_VEHICLES "** Administrar vehiculos **\n"
#define MSG_ABORTED "Operacion cancelada\n"

void pause() {
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
}

void clear_screen() {
    system(CLEAR);
}

void print_main_menu() {
    cout <<  OPT_ADMIN_CLIENTS << ") Administrar clientes" << endl;
    cout <<  OPT_ADMIN_RENTALS << ") Administrar alquileres" << endl;
    cout <<  OPT_ADMIN_VEHICLES << ") Administrar vehiculos" << endl;
    cout <<  OPT_EXIT << ") Salir" << endl;
}

void print_second_menu() {
    cout <<  OPT_ADD << ") Agregar" << endl;
    cout <<  OPT_SHOW << ") Mostrar" << endl;
    cout <<  OPT_MODIFY << ") Modificar" << endl;
    cout <<  OPT_REMOVE << ") Eliminar" << endl;
    cout <<  OPT_EXIT << ") Salir" << endl;
}

int get_int(int def) {
    int i;
    cout << ">";
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i= def;
    }
    cin.ignore();
    return i;
}

string get_string() {
    cout << ">";
    string s;
    cin >> s;
    return s;
}

bool get_confirmation() {
    string c;
    cout << "Seguro (s/n)?";
    cin >> c;
    cin.ignore();
    return c == "s";
}

void msg(const string &m) {
    cout << m;
}

void fill_client(const string &id, Client *client) {
    if(id == "") {
        cout << "Codigo>";
        cin >> client->id;
    } else {
        client->id = id;
    }
    cout << "RFC>";
    cin >> client->rfc;
    cout << "Domicilio>";
    getline(cin, client->adress);
    cout << "Telefono>";
    cin >> client->tel;
    cin.ignore();
}

void print_client(Client &client) {
    cout << "Codigo:" << client.id << endl;
    cout << "RFC:" << client.rfc << endl;
    cout << "Domicilio:" << client.adress << endl;
    cout << "Telefono:" << client.tel << endl;
    cout << endl;
}

void fill_rental(const string &folio, Rental *rental) {
    if(folio == "") {
        cout << "Folio>";
        cin >> rental->folio;
    } else {
        rental->folio = folio;
    }
    cout << "Codigo de cliente>";
    cin >> rental->id_client;
    cout << "Fecha>";
    cin >> rental->date;
    cout << "Hora>";
    cin >> rental->hour;
    cout << "Numero de vehiculo>";
    cin >> rental->vehicle;
    cin.ignore();
}

void print_rental(Rental &rental) {
    cout << "Folio:" << rental.folio << endl;
    cout << "Codigo de cliente:" << rental.id_client << endl;
    cout << "Fecha:" << rental.date << endl;
    cout << "Hora:" << rental.hour << endl;
    cout << "Numero de vehiculo:" << rental.vehicle << endl;
}

void fill_vehicle(const int code, Vehicle &vehicle) {
    if(code == -1) {
        cout << "Codigo";
        vehicle.code = get_int(-1);
    } else {
        vehicle.code = code;
    }
    cout << "Placas>";
    cin.getline(vehicle.license_plate, 12);
    cout << "Modelo>";
    cin.getline(vehicle.model, 30);
    cout << "Anyo";
    vehicle.year = get_int(-1);
    cout << "Buen estado? (s/n)>";
    vehicle.state = cin.get() == 's'? true : false;
}

void print_vehicle(const Vehicle &vehicle) {
    cout << "Codigo:" << vehicle.code << endl;
    cout << "Placas:" << vehicle.license_plate << endl;
    cout << "Modelo:" << vehicle.model << endl;
    cout << "Anyo:" << vehicle.year << endl;
    cout << "Buen estado?:" << (vehicle.state? "si" : "no") << endl;
    cout << endl;
}

#endif
