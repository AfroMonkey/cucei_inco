/*
 * Author: Navarro Presas Moises Alejandro
 * Reg: 215861509
 * Subject: Estructuras de Datos II, D07
 * Professor: Macias Brambilia Hassem Ruben
 * Compiler: gcc version 4.9.3 (GCC) (cygwin)
 * Date: 2016-02-14
 */

#include "command_line_interface.h"
#include "database_controller.h"
#include "client.h"
#include "vehicle.h"

#define CLIENTS_DATABASE_LOCATION "Clients.db"
#define RENTALS_DATABASE_LOCATION "Rentals.db"
#define VEHICLES_DATABASE_LOCATION "Vehicles.db"

void admin_clients();
void admin_rentals();
void admin_vehicles();

int main() {
    int opt;
    do {
        clear_screen();
        msg(HEADER_MAIN);
        print_main_menu();
        opt = get_int(-1);
        switch(opt) {
            case OPT_ADMIN_CLIENTS:
                admin_clients();
                break;
            case OPT_ADMIN_RENTALS:
                admin_rentals();
                break;
            case OPT_ADMIN_VEHICLES:
                admin_vehicles();
                break;
            case OPT_EXIT:
                return 0;
                break;
            default:
                msg(ERROR_INVALID_OPTION);
                pause();
        }
    }while(opt != OPT_EXIT);
    return 0;
}
/* Admin Clients */
void add_client() {
    Client client;
    fill_client("", &client);
    if(!write_client(CLIENTS_DATABASE_LOCATION, client)) {
        msg(ERROR_FILE);
    }
}

void show_clients() {
    if(!print_clients(CLIENTS_DATABASE_LOCATION)) {
        msg(ERROR_FILE);
    }
}

void modify_client() {
    Client *client;
    msg("ID");
    client = get_client(CLIENTS_DATABASE_LOCATION, get_string());
    if(client != nullptr) {
        print_client(*client);
        if(get_confirmation()) {
            Client new_client;
            fill_client(client->id, &new_client);
            replace_client(CLIENTS_DATABASE_LOCATION, client->id, new_client);
        } else {
            msg(MSG_ABORTED);
        }
    }
}

void remove_client() {
    Client *client;
    msg("ID");
    client = get_client(CLIENTS_DATABASE_LOCATION, get_string());
    if(client != nullptr) {
        print_client(*client);
        if(get_confirmation()) {
            remove_client(CLIENTS_DATABASE_LOCATION, client->id);
        } else {
            msg(MSG_ABORTED);
        }
    }
}


void admin_clients() {
    int opt;
    do {
        clear_screen();
        msg(HEADER_CLIENTS);
        print_second_menu();
        opt = get_int(-1);
        switch(opt) {
            case OPT_ADD:
                add_client();
                break;
            case OPT_SHOW:
                show_clients();
                break;
            case OPT_MODIFY:
                modify_client();
                break;
            case OPT_REMOVE:
                remove_client();
                break;
            case OPT_EXIT:
                return;
                break;
            default:
                msg(ERROR_INVALID_OPTION);
        }
        pause();
    }while(opt != OPT_EXIT);
}
/* Admin Rentals */
void add_rental() {
    Rental rental;
    fill_rental("", &rental);
    if(get_client(CLIENTS_DATABASE_LOCATION, rental.id_client) != nullptr) {
        if(search_vehicle(VEHICLES_DATABASE_LOCATION, atoi(rental.vehicle.c_str())).code != -1) {
            if(!write_rental(RENTALS_DATABASE_LOCATION, rental)) {
                msg(ERROR_FILE);
            }
        } else {
            msg(ERROR_VEHICLE_NOT_FOUND);
        }
    } else {
        msg(ERROR_CLIENT_NOT_FOUND);
    }
}

void show_rentals() {
    if(!print_rentals(RENTALS_DATABASE_LOCATION)) {
        msg(ERROR_FILE);
    }
}

void modify_rental() {
    Rental *rental;
    msg("ID");
    rental = get_rental(CLIENTS_DATABASE_LOCATION, get_string());
    if(rental != nullptr) {
        print_rental(*rental);
        if(get_confirmation()) {
            Rental new_rental;
            fill_rental(rental->folio, &new_rental);
            if(get_client(CLIENTS_DATABASE_LOCATION, new_rental.id_client) != nullptr) {
                if(search_vehicle(VEHICLES_DATABASE_LOCATION, atoi(new_rental.vehicle.c_str())).code != -1) {
                    replace_rental(CLIENTS_DATABASE_LOCATION, rental->folio, new_rental);
                } else {
                    msg(ERROR_VEHICLE_NOT_FOUND);
                }
            } else {
                msg(ERROR_CLIENT_NOT_FOUND);
            }
        } else {
            msg(MSG_ABORTED);
        }
    }
}

void remove_rental() {
    Rental *rental;
    msg("ID");
    rental = get_rental(CLIENTS_DATABASE_LOCATION, get_string());
    if(rental != nullptr) {
        print_rental(*rental);
        if(get_confirmation()) {
            remove_rental(CLIENTS_DATABASE_LOCATION, rental->folio);
        } else {
            msg(MSG_ABORTED);
        }
    }
}

void admin_rentals() {
    int opt;
    do {
        clear_screen();
        msg(HEADER_RENTALS);
        print_second_menu();
        opt = get_int(-1);
        switch(opt) {
            case OPT_ADD:
                add_rental();
                break;
            case OPT_SHOW:
                show_rentals();
                break;
            case OPT_MODIFY:
                modify_rental();
                break;
            case OPT_REMOVE:
                remove_rental();
                break;
            case OPT_EXIT:
                return;
                break;
            default:
                msg(ERROR_INVALID_OPTION);
        }
        pause();
    }while(opt != OPT_EXIT);
}
/* Admin Vehicles */
void add_vehicle() {
    Vehicle vehicle;
    fill_vehicle(-1, vehicle);
    if(!write_vehicle(VEHICLES_DATABASE_LOCATION, vehicle)) {
        msg(ERROR_FILE);
    }
}

void show_vehicles() {
    if(!print_vehicles(VEHICLES_DATABASE_LOCATION)) {
        msg(ERROR_FILE);
    }
}

void modify_vehicle() {
    Vehicle vehicle;
    msg("Codigo");
    vehicle = search_vehicle(VEHICLES_DATABASE_LOCATION, get_int(-1));
    if(vehicle.code != -1) {
        print_vehicle(vehicle);
        if(get_confirmation()) {
            Vehicle new_vehicle;
            fill_vehicle(vehicle.code, new_vehicle);
            replace_vehicle(VEHICLES_DATABASE_LOCATION, vehicle.code, new_vehicle);
        } else {
            msg(MSG_ABORTED);
        }
    }
}

void remove_vehicle() {
    Vehicle vehicle;
    msg("Codigo");
    vehicle = search_vehicle(VEHICLES_DATABASE_LOCATION, get_int(-1));
    if(vehicle.code != -1) {
        print_vehicle(vehicle);
        if(get_confirmation()) {
            remove_vehicle(VEHICLES_DATABASE_LOCATION, vehicle.code);
        } else {
            msg(MSG_ABORTED);
        }
    }
}

void admin_vehicles() {
    int opt;
    do {
        clear_screen();
        msg(HEADER_VEHICLES);
        print_second_menu();
        opt = get_int(-1);
        switch(opt) {
            case OPT_ADD:
                add_vehicle();
                break;
            case OPT_SHOW:
                show_vehicles();
                break;
            case OPT_MODIFY:
                modify_vehicle();
                break;
            case OPT_REMOVE:
                remove_vehicle();
                break;
            case OPT_EXIT:
                return;
                break;
            default:
                msg(ERROR_INVALID_OPTION);
        }
        pause();
    }while(opt != OPT_EXIT);
}
