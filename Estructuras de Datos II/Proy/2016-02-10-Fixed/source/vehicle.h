#ifndef VEHICLE_H
#define VEHICLE_H

class Vehicle {
public:
    int code;
    char license_plate[12];
    char model[30];
    int year;
    bool state;

    Vehicle();
};

Vehicle::Vehicle() {
    code = -1;
}

#endif
