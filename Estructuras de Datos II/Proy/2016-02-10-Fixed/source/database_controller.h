#ifndef DATABASE_CONTROLLER
#define DATABASE_CONTROLLER

#include <iostream>
#include <fstream>
#include "client.h"
#include "command_line_interface.h"

using namespace std;

#define TEMP_DATABASE_LOCATION "Temp.db"

bool write_client(const string &DATABASE_LOCATION, Client &client) {
    ofstream database(DATABASE_LOCATION, ios::app);
    if(database) {
        int length;
        length = client.id.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)client.id.c_str(), length);
        length = client.rfc.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)client.rfc.c_str(), length);
        length = client.adress.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)client.adress.c_str(), length);
        length = client.tel.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)client.tel.c_str(), length);
        database.close();
        return true;
    } else {
        return false;
    }
}

Client* read_client(ifstream &database) {
    Client *client = new Client;
    int length;
    char *buffer;

    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    client->id = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    client->rfc = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    client->adress = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    client->tel = buffer;
    delete[] buffer;

    if(database.eof()) {
        return nullptr;
    } else {
        return client;
    }
}

bool print_clients(const string &DATABASE_LOCATION) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Client *client;
        while(true) {
            client = read_client(database);
            if(client != nullptr) {
                print_client(*client);
                delete client;
            } else {
                break;
            }
        }
        database.close();
        return true;
    } else {
        return false;
    }
}

Client* get_client(const string &DATABASE_LOCATION, const string &id) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Client *client;
        bool founded = false;
        while(true) {
            client = read_client(database);
            if(client != nullptr) {
                if(client->id == id) {
                    founded = true;
                    break;
                }
                delete client;
            } else {
                break;
            }
        }
        database.close();
        if(founded) {
            return client;
        }
    }
    return nullptr;
}

void replace_client(const string &DATABASE_LOCATION, const string &id, Client &new_client) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Client *client;
        while(true) {
            client = read_client(database);
            if(client != nullptr) {
                if(client->id == id) {
                    write_client(TEMP_DATABASE_LOCATION, new_client);
                } else {
                    write_client(TEMP_DATABASE_LOCATION, *client);
                }
                delete client;
            } else {
                break;
            }
        }
        database.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE_LOCATION, DATABASE_LOCATION.c_str());
    }
}

void remove_client(const string &DATABASE_LOCATION, const string &id) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Client *client;
        while(true) {
            client = read_client(database);
            if(client != nullptr) {
                if(client->id != id) {
                    write_client(TEMP_DATABASE_LOCATION, *client);
                }
                delete client;
            } else {
                break;
            }
        }
        database.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE_LOCATION, DATABASE_LOCATION.c_str());
    }
}

bool write_rental(const string &DATABASE_LOCATION, Rental &rental) {
    ofstream database(DATABASE_LOCATION, ios::app);
    if(database) {
        int length;
        length = rental.folio.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)rental.folio.c_str(), length);
        length = rental.id_client.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)rental.id_client.c_str(), length);
        length = rental.date.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)rental.date.c_str(), length);
        length = rental.hour.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)rental.hour.c_str(), length);
        length = rental.vehicle.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)rental.vehicle.c_str(), length);
        database.close();
        return true;
    } else {
        return false;
    }
}

Rental* read_rental(ifstream &database) {
    Rental *rental = new Rental;
    int length;
    char *buffer;

    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    rental->folio = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    rental->id_client = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    rental->date = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    rental->hour = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    rental->vehicle = buffer;
    delete[] buffer;

    if(database.eof()) {
        return nullptr;
    } else {
        return rental;
    }
}

bool print_rentals(const string &DATABASE_LOCATION) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Rental *rental;
        while(true) {
            rental = read_rental(database);
            if(rental != nullptr) {
                print_rental(*rental);
                delete rental;
            } else {
                break;
            }
        }
        database.close();
        return true;
    } else {
        return false;
    }
}

Rental* get_rental(const string &DATABASE_LOCATION, const string &folio) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Rental *rental;
        bool founded = false;
        while(true) {
            rental = read_rental(database);
            if(rental != nullptr) {
                if(rental->folio == folio) {
                    founded = true;
                    break;
                }
                delete rental;
            } else {
                break;
            }
        }
        database.close();
        if(founded) {
            return rental;
        }
    }
    return nullptr;
}

void replace_rental(const string &DATABASE_LOCATION, const string &folio, Rental &new_rental) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Rental *rental;
        while(true) {
            rental = read_rental(database);
            if(rental != nullptr) {
                if(rental->folio == folio) {
                    write_rental(TEMP_DATABASE_LOCATION, new_rental);
                } else {
                    write_rental(TEMP_DATABASE_LOCATION, *rental);
                }
                delete rental;
            } else {
                break;
            }
        }
        database.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE_LOCATION, DATABASE_LOCATION.c_str());
    }
}

void remove_rental(const string &DATABASE_LOCATION, const string &folio) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Rental *rental;
        while(true) {
            rental = read_rental(database);
            if(rental != nullptr) {
                if(rental->folio != folio) {
                    write_rental(TEMP_DATABASE_LOCATION, *rental);
                }
                delete rental;
            } else {
                break;
            }
        }
        database.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE_LOCATION, DATABASE_LOCATION.c_str());
    }
}

bool write_vehicle(const string &DATABASE_LOCATION, const Vehicle &vehicle) {
    ofstream database(DATABASE_LOCATION, ios::app);
    if(database) {
        database.write((char*)&vehicle, sizeof(Vehicle));
        database.close();
        return true;
    }
    return false;
}

Vehicle search_vehicle(const string &DATABASE_LOCATION, const int code) {
    Vehicle vehicle;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        while(true) {
            database.read((char*)&vehicle, sizeof(Vehicle));
            if(database.eof()) {
                break;
            }
            if(vehicle.code == code) {
                return vehicle;
            }
        }
    }
    vehicle.code = -1;
    return vehicle;
}

bool print_vehicles(const string &DATABASE_LOCATION) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Vehicle vehicle;
        while(true) {
            database.read((char*)&vehicle, sizeof(Vehicle));
            if(database.eof()) {
                break;
            }
            print_vehicle(vehicle);
        }
        database.close();
        return true;
    } else {
        return false;
    }
}

void replace_vehicle(const string &DATABASE_LOCATION, const int code, const Vehicle &new_vehicle) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Vehicle vehicle;
        while(true) {
            database.read((char*)&vehicle, sizeof(Vehicle));
            if(database.eof()) {
                break;
            }
            if(vehicle.code == code) {
                write_vehicle(TEMP_DATABASE_LOCATION, new_vehicle);
            } else {
                write_vehicle(TEMP_DATABASE_LOCATION, vehicle);
            }
        }
        database.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE_LOCATION, DATABASE_LOCATION.c_str());
    }
}

void remove_vehicle(const string &DATABASE_LOCATION, const int code) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Vehicle vehicle;
        while(true) {
            database.read((char*)&vehicle, sizeof(Vehicle));
            if(database.eof()) {
                break;
            }
            if(vehicle.code != code) {
                write_vehicle(TEMP_DATABASE_LOCATION, vehicle);
            }
        }
        database.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE_LOCATION, DATABASE_LOCATION.c_str());
    }
}

#endif
