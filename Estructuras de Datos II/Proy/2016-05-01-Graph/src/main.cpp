/*
 * Author: Navarro Presas Moises Alejandro
 * Code: 215861509
 * Subject-Matter: Estructuras de Datos II
 * Professor: Macias Brambilia Hassem Ruben
 * Compiler: gcc version 5.3.0 (GCC) -std=c++11
 */
#include <iostream>
#include <fstream>

#include "cli.h"
#include "config.h"
#include "graph.h"

void opt_breadth_route(Graph &graph);
void opt_best_route(Graph &graph);

int main() {
    Graph graph(true, false);
    std::ifstream cities(DB_CITIES);
    std::ifstream matrix(DB_MATRIX);
    if (!cities || !matrix) return -1;
    string city;
    for(int i = 0; i < NUM_CITIES; i++)
    {
        getline(cities, city);
        graph.add_vertex(city);
    }
    cities.close();
    string start, end, weight;
    while (getline(matrix, start))
    {
        getline(matrix, end);
        getline(matrix, weight);
        if (!graph.set_link(graph.get_vertex(start), graph.get_vertex(end), (int)std::stoi(weight))) return -2;
    }
    matrix.close();

    int opt;
    do {
        show_menu();
        opt = get_int();
        switch(opt) {
        case OPT_BREADTH_ROUTE:
                opt_breadth_route(graph);
                break;
            case OPT_BEST_ROUTE:
                opt_best_route(graph);
                break;
            case OPT_EXIT:
                return 0;
                break;
            default:
                msg(INVALID_OPTION);
                break;
        }
        pause();
        clear_screen();
    } while(opt != OPT_EXIT);
    return 0;
}

void opt_breadth_route(Graph &graph) {
    Vertex* start = graph.get_vertex(get_string("Inicio>"));
    Vertex* end = graph.get_vertex(get_string("Final>"));
    if(start != NULL && end != NULL) {
        graph.breadth_first_search(start, end);
    } else {
        msg(ERROR);
    }
}

void opt_best_route(Graph &graph) {
    Vertex* start = graph.get_vertex(get_string("Inicio>"));
    Vertex* end = graph.get_vertex(get_string("Final>"));
    if(start != NULL && end != NULL) {
        graph.dijkstra(start, end);
    } else {
        msg(ERROR);
    }
}
