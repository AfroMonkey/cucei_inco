#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <queue>
#include <stack>

#include "vertex.h"

class Edge
{
public:
    Edge(Vertex *from, Vertex *to, int weight): from_(from), to_(to), weight_(weight) {}
    ~Edge() {}
    Vertex *from_;
    Vertex *to_;
    int weight_;
};


class Graph
{
public:
    bool weighted_;
    bool directed_;
    std::list<Vertex> vertices_;

    Graph(bool weighted, bool directed) : weighted_(weighted), directed_(directed) {}

    bool add_vertex(const std::string name);
    bool add_vertex(Vertex &vertex);
    bool set_link(Vertex *start, Vertex *end, int weight);
    Vertex* get_vertex(const std::string name);

    void depth_first_search(Vertex *start);
    void breadth_first_search(Vertex *start);
    void depth_first_search(Vertex *start, Vertex *end);
    void breadth_first_search(Vertex *start, Vertex *end);
    void dijkstra(Vertex *start, Vertex *end);
private:
    void unvisit();
    Link* GetEdge(Vertex* from, Vertex* to);
};

Link* Graph::GetEdge(Vertex* from, Vertex* to)
{
    if (!(from && to)) return 0;
    for (auto e = from->links_.begin(); e != from->links_.end(); ++e)
    {
        if (e->to_ == to)
        {
            return &(*e); // *v returns a reference, & on a reference provides an address
        }
    }
    return 0;
}

bool Graph::add_vertex(const std::string name)
{
    for (std::list<Vertex>::iterator i = vertices_.begin(); i != vertices_.end(); i++)
    {
        if (i->name_ == name) return false;
    }
    vertices_.push_back(Vertex(name));
    return true;
}

bool Graph::add_vertex(Vertex &vertex)
{
    vertices_.push_back(vertex);
    return true;
}

bool Graph::set_link(Vertex *start, Vertex *end, int weight)
{
    if (!(start && end)) return false;

    start->set_link(end, weight);
    if (!directed_)
    {
        end->set_link(start, weight);
    }
    return true;
}

Vertex* Graph::get_vertex(const std::string name)
{
    for (std::list<Vertex>::iterator i = vertices_.begin(); i != vertices_.end(); i++)
    {
        if (i->name_ == name) return &*i;
    }
    return NULL;
}

void Graph::depth_first_search(Vertex *start)
{
	std::stack<Vertex*> s;
    Vertex *vertex;
    std::list<Link>::iterator i;

    std::cout << start->name_ << " ";
    s.push(start);
    while (!s.empty())
    {
        vertex = s.top();
        vertex->visited_ = true;
        for (i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_) {
                std::cout << i->to_->name_ << " ";
                i->to_->visited_ = true;
                s.push(i->to_);
                break;
            }
        }
        if(i == vertex->links_.end()) {
            s.pop();
        }
    }
    unvisit();
    std::cout << std::endl;
}

void Graph::breadth_first_search(Vertex *start)
{
	std::queue<Vertex*> q;
    Vertex *vertex;

    start->visited_ = true;
    q.push(start);
    while (!q.empty()) {
        vertex = q.front();
        q.pop();
        vertex->visited_ = true;
        std::cout << vertex->name_ << " ";
        for (std::list<Link>::iterator i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_)
            {
                i->to_->visited_ = true;
                q.push(i->to_);
            }
        }
    }
    unvisit();
    std::cout << std::endl;
}

void Graph::depth_first_search(Vertex *start, Vertex *end) {
	std::stack<Vertex*> s;
    std::stack<int> weights;
    Vertex *vertex;
    std::list<Link>::iterator i;

    start->visited_ = true;
    s.push(start);
    while (!s.empty())
    {
        vertex = s.top();
        for (i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_)
            {
                i->to_->visited_ = true;
                s.push(i->to_);
                weights.push(i->weight_);
                break;
            }
        }
        if (i->to_ == end) break;
        if (i == vertex->links_.end())
        {
            s.pop();
            weights.pop();
        }
    }

    if (s.empty()) std::cout << "No hay ruta" << std::endl;
    else {
        std::stack<Vertex*> aux;
        std::stack<int> aux2;
        while (!s.empty())
        {
            aux.push(s.top());
            s.pop();
        }
        s = aux;
        while (!weights.empty())
        {
            aux2.push(weights.top());
            weights.pop();
        }
        weights = aux2;
        Vertex *prev = NULL;
        int weight = 0;
        while (!s.empty())
        {
            vertex = s.top();
            s.pop();
            if(prev) {
                std::cout << "-" << weights.top() << "->";
                weight += weights.top();
                weights.pop();
            }
            std::cout << "(" << vertex->name_ << ")";
            prev = vertex;
        }
        std::cout << std::endl;
        std::cout << "Peso total = " << weight << std::endl;
    }
    unvisit();
}

void Graph::breadth_first_search(Vertex *start, Vertex *end) {
	std::queue<Vertex> queue;
    std::stack<Link> path;
    for (auto v = vertices_.begin(); v != vertices_.end(); ++v)
    {
        v->visited_ = 0;
    }
    queue.push(*start);
    start->visited_ = 1;
    while (!queue.empty())
    {
        Vertex cur = queue.front();
        queue.pop();
        if (cur.name_ == end->name_)
        {
            // print path
            int cost = 0;
            std::stack<Vertex> clean_path;
            Vertex v = *end;
            while (!path.empty())
            {
                Link edge = path.top();
                path.pop();
                if (edge.to_->name_ == v.name_)
                {
                    cost += edge.weight_;
                    clean_path.push(*edge.to_);
                    v = *edge.from_;
                    if (v.name_ == start->name_)
                    {
                        clean_path.push(v);
                        break;
                    }
                }
            }
            if (clean_path.empty())
            {
                std::cout << " No hay ruta." << std::endl;
            }
            else
            {
                v = clean_path.top();
                clean_path.pop();
                while (!clean_path.empty())
                {
                    std::cout << "(" << v.name_ << ")->";
                    v = clean_path.top();
                    clean_path.pop();
                }
                std::cout << "(" << v.name_ << ")" << endl;
                std::cout << "Peso total = " << cost << std::endl;
            }
            return;
        }
        for (auto v = vertices_.begin(); v != vertices_.end(); ++v)
        {
            if (GetEdge(&cur, &(*v)) && !v->visited_)
            {
                v->visited_ = 1;
                queue.push(*v);
                path.push(*GetEdge(&cur, &(*v)));
            }
        }
    }
}

void Graph::dijkstra(Vertex *start, Vertex *end)
{
    std::list<Vertex*> list;
    std::stack<Link> path;
    for (auto v = vertices_.begin(); v != vertices_.end(); ++v)
    {
        v->visited_ = 0;
        v->cost_ = 9999999;
    }
    start->visited_ = 1;
    start->cost_ = 0;
    list.push_back(start);
    while (!list.empty())
    {
        Vertex* cur = list.front();
        list.pop_front();
        cur->visited_ = 1;
        if (cur == end)
        {
            std::stack<Vertex> clean_path;
            Vertex v = *end;
            while (!path.empty())
            {
                Link edge = path.top();
                path.pop();
                if (edge.to_->name_ == v.name_)
                {
                    clean_path.push(*edge.to_);
                    v = *edge.from_;
                    if (v.name_ == start->name_)
                    {
                        clean_path.push(v);
                        break;
                    }
                }
            }
            if (clean_path.empty())
            {
                std::cout << " No hay ruta." << std::endl;
            }
            else
            {
                v = clean_path.top();
                clean_path.pop();
                while (!clean_path.empty())
                {
                    std::cout << "(" << v.name_ << ")" << "->";
                    v = clean_path.top();
                    clean_path.pop();
                }
                std::cout << "(" <<  v.name_ << ")" << endl;
                std::cout << "Peso total = " << v.cost_ << std::endl;
            }
            return;
        }

        for (auto v = vertices_.begin(); v != vertices_.end(); ++v)
        {
            Link* edge = GetEdge(cur, &(*v));
            if (edge && !v->visited_)
            {
                int in_list = 0;
                if ((cur->cost_ + edge->weight_) < v->cost_)
                {
                    v->cost_ = cur->cost_ + edge->weight_;
                    path.push(*edge);
                    list.remove(&(*v));
                }

                for (std::list<Vertex*>::iterator it = list.begin(); it != list.end(); ++it)
                {
                    if ((*it)->cost_ > v->cost_)
                    {
                        list.insert(it, &(*v));
                        in_list = 1;
                        break;
                    }
                }
                if (!in_list)
                {
                    list.push_back(&(*v));
                }
            }
        }
    }
    unvisit();
}

void Graph::unvisit()
{
    for (std::list<Vertex>::iterator i = vertices_.begin(); i != vertices_.end(); i++)
    {
        i->visited_ = false;
    }
}

#endif
