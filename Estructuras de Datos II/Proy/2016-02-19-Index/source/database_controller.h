#ifndef DATABASE_CONTROLLER_H
#define DATABASE_CONTROLLER_H

#include <fstream>

#include "index.h"
#include "article.h"
#include "cli.h"
#include "doublyLinkedList.h"
#include "conf.h"

using namespace std;

Index<int> get_last_index(const string &index_db_location) {
    Index<int> index;
    ifstream index_database(index_db_location);
    if(index_database) {
        index_database.seekg(0, ios::end);
        index_database.seekg((long)index_database.tellg() - sizeof(Index<int>));
        index.read(index_database);
        index_database.close();
    }
    return index;
}

Index<int> get_index(const string &index_db_location, const int code) {
    Index<int> index;
    ifstream index_database(index_db_location);
    if(index_database) {
        while(true) {
            index.read(index_database);
            if(index_database.eof()) {
                index.key = index.address = -1;
                break;
            }
            if(index.key == code) {
                break;
            }
        }
    }
    return index;
}

bool write_article(const string &db_location, const string &index_db_location, Article article) {
    ofstream database(db_location, ios::app);
    ofstream index_database(index_db_location, ios::app);
    if(database && index_database) {
        Index<int> index;
        index.key = article.code;
        index.address = database.tellp();
        article.write(database);
        index.write(index_database);
        database.close();
        index_database.close();
        return true;
    } else {
        return false;
    }
}

bool write_article(const string &db_location, Article article, const long address) {
    fstream database(db_location, ios::in | ios::out);
    if(database) {
        database.seekp(address);
        article.write(database);
        database.close();
        return true;
    } else {
        return false;
    }
}

Article get_article(const string &db_location, long address) {
    Article article;
    ifstream database(db_location);
    if(database) {
        database.seekg(address);
        article.read(database);
    }
    return article;
}

bool print_articles(const string &db_location, const string &index_db_location) {
    ifstream index_database(index_db_location);
    if(index_database) {
        Index<int> index;
        while(true) {
            index.read(index_database);
            if(index_database.eof()) {
                break;
            }
            print_article(get_article(db_location, index.address));
        }
        index_database.close();
        return true;
    } else {
        return false;
    }
}

DoublyLinkedList<Index<float>> get_list(const string &db_location, const string &index_db_location) {
    DoublyLinkedList<Index<float>> list;
    ifstream index_database(index_db_location);
    if(index_database) {
        Index<int> index;
        Article article;
        while(true) {
            index.read(index_database);
            if(index_database.eof()) {
                break;
            }
            list.insert(*(new Index<float>(get_article(db_location, index.address).price, index.address)));
        }
    }
    return list;
}

bool remove_article(const string &db_location, const string &index_db_location, Index<int> index) {
    ifstream index_database(index_db_location);
    ofstream temp_index_database(TEMP_INDEX_DB_LOCATION, ios::app);
    if(index_database) {
        Index<int> aux;
        while(true) {
            aux.read(index_database);
            if(index_database.eof()) {
                break;
            }
            if(index.key != aux.key) {
                write_article(TEMP_ARTICLE_DB_LOCATION, TEMP_INDEX_DB_LOCATION, get_article(db_location, aux.address));
            }
        }
        index_database.close();
        temp_index_database.close();
        remove(db_location.c_str());
        remove(index_db_location.c_str());
        rename(TEMP_ARTICLE_DB_LOCATION, db_location.c_str());
        rename(TEMP_INDEX_DB_LOCATION, index_db_location.c_str());
        return true;
    } else {
        return false;
    }
}

#endif
