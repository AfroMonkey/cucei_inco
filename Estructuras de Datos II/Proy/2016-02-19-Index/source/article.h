#ifndef ARTICLE_H
#define ARTICLE_H

#include <fstream>

class Article {
public:
    int code;
    char name[30];
    char author[30];
    char categoy [30];
    char gender[30];
    float price;

    Article();
    void read(std::ifstream &database);
    void write(std::ofstream &database);
    void write(std::fstream &database);
};

Article::Article() {
    code = -1;
}

void Article::read(std::ifstream &database) {
    database.read((char*)this, sizeof(Article));
}

void Article::write(std::ofstream &database) {
    database.write((char*)this, sizeof(Article));
}

void Article::write(std::fstream &database) {
    database.write((char*)this, sizeof(Article));
}

#endif
