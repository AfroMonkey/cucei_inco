/*
 * Author: Navarro Presas Moises Alejandro
 * Reg: 215861509
 * Subject: Estructuras de Datos II, D07
 * Professor: Macias Brambilia Hassem Ruben
 * Compiler: gcc version 4.9.3 (GCC)
 * Date: 2016-02-19
 */

 void opt_add();
 void opt_show();
 void opt_sort();
 void opt_modify();
 void opt_remove();
 void opt_search();

#include "article.h"
#include "index.h"
#include "conf.h"
#include "database_controller.h"
#include "cli.h"
#include "doublyLinkedList.h"

 int main()  {
     do {
        clear_screen();
        show_menu();
        switch (getInt()) {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_SORT:
                opt_sort();
                break;
            case OPT_MODIFY:
                opt_modify();
                break;
            case OPT_REMOVE:
                opt_remove();
                break;
            case OPT_SEARCH:
                opt_search();
                break;
            case OPT_EXIT:
                return 0;
                break;
            default:
                msg(INVALID_OPTION);
                break;
        }
        pause();
     } while(true);
 }

 void opt_add() {
     Article article;
     fill_article(article, get_last_index(INDEX_DB_LOCATION).key+1);
     if(!write_article(ARTICLES_DB_LOCATION, INDEX_DB_LOCATION, article)) {
         msg(ERROR_FILE);
     }
 }

 void opt_show() {
     if(!print_articles(ARTICLES_DB_LOCATION, INDEX_DB_LOCATION)) {
         msg(ERROR_FILE);
     }
 }

 void opt_sort() {
     DoublyLinkedList<Index<float>> list = get_list(ARTICLES_DB_LOCATION, INDEX_DB_LOCATION);
     if(!list.isEmpty()) {
         Node<Index<float>> *it = list.begin();
         Node<Index<float>> *end = list.end();
         while(it != end) {
             print_article(get_article(ARTICLES_DB_LOCATION, it->data.address));
             it = it->next;
         }
         print_article(get_article(ARTICLES_DB_LOCATION, it->data.address));
     }
     list.clear();
 }

 void opt_modify() {
     Index<int> index = get_index(INDEX_DB_LOCATION, getInt("Codigo>"));
     if(index.key != -1) {
         print_article(get_article(ARTICLES_DB_LOCATION, index.address));
         if(getInt("Seguro?(1/0)")) {
             Article article;
             fill_article(article, index.key);
             if(!write_article(ARTICLES_DB_LOCATION, article, index.address)) {
                 msg(ERROR_FILE);
             }
         }
     } else {
         msg(ERROR_MISSGIN_ARTICLE);
     }
 }

 void opt_remove() {
     Index<int> index = get_index(INDEX_DB_LOCATION, getInt("Codigo>"));
     if(index.key != -1) {
         print_article(get_article(ARTICLES_DB_LOCATION, index.address));
         if(getInt("Seguro?(1/0)")) {
             if(!remove_article(ARTICLES_DB_LOCATION, INDEX_DB_LOCATION, index)) {
                 msg(ERROR_FILE);
             }
         }
     } else {
         msg(ERROR_MISSGIN_ARTICLE);
     }
 }

 void opt_search() {
     Index<int> index = get_index(INDEX_DB_LOCATION, getInt("Codigo>"));
     if(index.key != -1) {
         print_article(get_article(ARTICLES_DB_LOCATION, index.address));
     } else {
         msg(ERROR_MISSGIN_ARTICLE);
     }
 }
