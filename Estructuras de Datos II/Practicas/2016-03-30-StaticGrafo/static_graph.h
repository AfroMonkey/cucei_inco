#ifndef STATIC_GRAPH_H
#define STATIC_GRAPH_H

#include <iostream>
#include <vector>

#include "static_queue.h"
#include "static_stack.h"

#define NO_CONNECTION -1

class StaticGraph {
private:
    int size_;
    bool weighted_;
    bool directed_;
    int **weights_;
public:
    std::string *ids_;

    StaticGraph(const bool weighted, const bool directed, const int size = 50);
    ~StaticGraph();

    bool set_link(const int start, const int end, int weight = 0);
    int get_vertex(const std::string id);
    std::string get_id(const int pos);
    void depth_first_search(int start);
    void breadth_first_search(int start);
    void depth_first_search(int start, int end);
    void breadth_first_search(int start, int end);
    void dijkstra(int start, int end);
    void print();
};

StaticGraph::StaticGraph(const bool weighted, const bool directed, const int size) {
    size_ = size;
    weighted_ = weighted;
    directed_ = directed;
    ids_ = new std::string[size_];
    weights_ = new int*[size_];
    for(int i, j = 0; i < size_; i++) {
        weights_[i] = new int[size_];
        for(j = 0; j < size_; j++) {
            weights_[i][j] = NO_CONNECTION;
        }
    }
}

StaticGraph::~StaticGraph() {
    delete [] ids_;
    for(int i = 0; i < size_; i++) {
        delete [] weights_[i];
    }
    delete [] weights_;
}


bool StaticGraph::set_link(const int start, const int end, int weight) {
    if(start >= 0 && start < size_ && end >= 0 && end < size_ && (weight >= 0 || weight == NO_CONNECTION)) {
        weight *= weighted_;
        weights_[start][end] = weight;
        if(!directed_) {
            weights_[end][start] = weight;
        }
        return true;
    } else {
        return false;
    }
}

int StaticGraph::get_vertex(const std::string id) {
    for(int i = 0; i < size_; i++) {
        if(ids_[i] == id) {
            return i;
        }
    }
    return -1;
}

std::string StaticGraph::get_id(const int pos) {
    if(pos >= 0 && pos < size_) {
        return ids_[pos];
    } else {
        return "null";
    }
}

void StaticGraph::depth_first_search(int start) {
	bool visited[size_]={false};
	StaticStack<int> stack(size_);
    int vertex, i;

    std::cout << get_id(start) << " ";
    stack.push(start);
    while(!stack.is_empty()) {
        vertex = stack.get_top();
        visited[vertex] = true;
        for(i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                std::cout << get_id(i) << " ";
                stack.push(i);
                visited[i] = true;
                break;
            }
        }
        if(i == size_) {
            stack.pop();
        }
    }
    std::cout << std::endl;
}

void StaticGraph::breadth_first_search(int start) {
	bool visited[size_]={false};
	StaticQueue<int> queue(size_);
    int vertex;

    queue.enqueue(start);
    while(!queue.is_empty()) {
        vertex = queue.deque();
        visited[vertex] = true;
        std::cout << get_id(vertex) << " ";
        for(int i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                queue.enqueue(i);
                visited[i] = true;
            }
        }
    }
    std::cout << std::endl;
}

void StaticGraph::depth_first_search(int start, int end) {
	bool visited[size_]={false};
	StaticStack<int> stack(size_);
    int vertex, i;

    stack.push(start);
    while(!stack.is_empty()) {
        vertex = stack.get_top();
        visited[vertex] = true;
        for(i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                stack.push(i);
                visited[i] = true;
                break;
            }
        }
        if(i == end) {
            break;
        }
        if(i == size_) {
            stack.pop();
        }
    }

    if(stack.is_empty()) {
        std::cout << "No hay ruta" << std::endl;
    } else {
        StaticStack<int> aux(size_);
        while(!stack.is_empty()) {
            aux.push(stack.pop());
        }
        int prev = -1;
        int weight = 0;
        while(!aux.is_empty()) {
            vertex = aux.pop();
            if(prev != -1) {
                weight += weights_[prev][vertex];
                std::cout << "-" << weights_[prev][vertex] << "->";
            }
            std::cout << "(" << get_id(vertex) << ")";
            prev = vertex;
        }
        std::cout << std::endl;
        std::cout << "Peso total = " << weight << std::endl;
    }
}

void StaticGraph::breadth_first_search(int start, int end) {
	bool visited[size_]={false};
	StaticQueue<int> queue(size_);
    int vertex;
    int weights2_[size_][size_];
    bool founded = false;

    for(int i = 0; i < size_; i++) {
        for(int j = 0; j < size_; j++) {
            weights2_[i][j] = -1;
        }
    }

    queue.enqueue(start);
    while(!queue.is_empty()) {
        vertex = queue.deque();
        visited[vertex] = true;
        for(int i = 0; i < size_; i++) {
            if(!visited[i] && weights_[vertex][i] != NO_CONNECTION) {
                queue.enqueue(i);
                visited[i] = true;
                weights2_[vertex][i] = weights_[vertex][i];
                if(i == end) {
                    founded = true;
                    break;
                }
            }
        }
        if(founded) {
            break;
        }
    }
    if(founded) {
    	bool visited[size_]={false};
    	StaticStack<int> stack(size_);
        int vertex, i;

        stack.push(start);
        while(!stack.is_empty()) {
            vertex = stack.get_top();
            visited[vertex] = true;
            for(i = 0; i < size_; i++) {
                if(!visited[i] && weights2_[vertex][i] != NO_CONNECTION) {
                    stack.push(i);
                    visited[i] = true;
                    break;
                }
            }
            if(i == end) {
                break;
            }
            if(i == size_) {
                stack.pop();
            }
        }

        if(stack.is_empty()) {
            std::cout << "No hay ruta" << std::endl;
        } else {
            StaticStack<int> aux(size_);
            while(!stack.is_empty()) {
                aux.push(stack.pop());
            }
            int prev = -1;
            int weight = 0;
            while(!aux.is_empty()) {
                vertex = aux.pop();
                if(prev != -1) {
                    weight += weights2_[prev][vertex];
                    std::cout << "-" << weights2_[prev][vertex] << "->";
                }
                std::cout << "(" << get_id(vertex) << ")";
                prev = vertex;
            }
            std::cout << std::endl;
            std::cout << "Peso total = " << weight << std::endl;
        }
    } else {
        std::cout << "No hay ruta" << std::endl;
    }
}

void StaticGraph::dijkstra(int v, int u) {
    std::vector<int> dist(size_);
    std::vector<bool> visited(size_);

    for (int i = 0; i < size_; i++)
    {
        dist[i] = 999999;
    }

    dist[v] = 0;
    for(int i = 0; i < size_; ++i)
    {
        int cur = -1;
        for(int j = 0; j < size_; ++j)
        {
            if (visited[j]) continue;
            if (cur == -1 || dist[j] < dist[cur])
            {
                cur = j;
            }
        }

        visited[cur] = true;

        for (int j = 0; j < size_; j++)
        {
            int d;
            if (weights_[cur][j] != -1)
            {
                d = dist[cur] + weights_[cur][j];
                if (d < dist[j])
                {
                    dist[j] = d;
                }
            }
        }
    }
    if(dist[u] == 999999) {
        std::cout << "No hay ruta" << std::endl;
    } else {
        std::cout << "Peso total = " << dist[u] << std::endl;
    }
}

void StaticGraph::print() {
    std::cout << "\t";
    for(int i = 0; i < size_; i++) {
        std::cout << get_id(i) << "\t";
    }
    std::cout << std::endl;
    for(int i = 0; i < size_; i++) {
        std::cout << get_id(i) << "\t";
        for(int j = 0; j < size_; j++) {
            std::cout << weights_[i][j] << "\t";
        }
        std::cout << std::endl;
    }
}
#endif
