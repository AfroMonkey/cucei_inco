#ifndef STATIC_STACK_H
#define STATIC_STACK_H

#include <cstdlib>

template <class T>
class StaticStack {
private:
    int size_;
    T *stack_;
    int top_;
public:
    StaticStack(int size = 50);
    ~StaticStack();

    bool is_empty();
    bool is_full();
    bool push(T data);
    void clear();
    T pop();
    T get_top();
};

template <class T>
StaticStack<T>::StaticStack(int size) {
    size_ = size;
    stack_ = new T[size_];
    top_ = -1;
}

template <class T>
StaticStack<T>::~StaticStack() {
    delete [] stack_;
}

template <class T>
bool StaticStack<T>::is_empty() {
    return top_ == -1;
}

template <class T>
bool StaticStack<T>::is_full() {
    return top_ == size_ - 1;
}

template <class T>
bool StaticStack<T>::push(T data) {
    if(!is_full()) {
        stack_[++top_] = data;
        return true;
    } else {
      return false;
    }
}

template <class T>
void StaticStack<T>::clear() {
    while(!is_empty()) {
        pop();
    }
}

template <class T>
T StaticStack<T>::pop() {
    if(!is_empty()) {
        return stack_[top_--];
    }
}

template <class T>
T StaticStack<T>::get_top() {
    if(!is_empty()) {
        return stack_[top_];
    }
}

#endif
