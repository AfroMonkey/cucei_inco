#include <iostream>
#include <string>

#include "cli.h"
#include "static_graph.h"

void opt_add_connection(StaticGraph &graph);
void opt_depth_first_search(StaticGraph &graph);
void opt_breadth_first_search(StaticGraph &graph);
void opt_depth_route(StaticGraph &graph);
void opt_breadth_route(StaticGraph &graph);
void opt_best_route(StaticGraph &graph);
void opt_print(StaticGraph &graph);

int main() {
    int size = get_int("Numero de nodos>");
    if(size < 1) {
        return -1;
    }
    StaticGraph graph(true, true, size);
    for(int i = 0; i < size; i++) {
        graph.ids_[i] = get_string("ID #" + to_string(i) + ">");
    }
    pause();
    clear_screen();

    int opt;
    do {
        show_menu();
        opt = get_int();
        switch(opt) {
            case OPT_ADD_CONNECTION:
                opt_add_connection(graph);
                break;
            case OPT_DEPTH_FIRST_SEARCH:
                opt_depth_first_search(graph);
                break;
            case OPT_BREADTH_FIRST_SEARCH:
                opt_breadth_first_search(graph);
                break;
            case OPT_DEPTH_ROUTE:
                opt_depth_route(graph);
                break;
            case OPT_BREADTH_ROUTE:
                opt_breadth_route(graph);
                break;
            case OPT_BEST_ROUTE:
                opt_best_route(graph);
                break;
            case OPT_PRINT:
                opt_print(graph);
                break;
            case OPT_EXIT:
                return 0;
                break;
            default:
                msg(INVALID_OPTION);
                break;
        }
        pause();
        clear_screen();
    } while(opt != OPT_EXIT);
    return 0;
}

void opt_add_connection(StaticGraph &graph) {
    string start = get_string("Inicio>");
    string end = get_string("Final>");
    int weight = get_int ("Peso>");
    if(graph.set_link(graph.get_vertex(start), graph.get_vertex(end), weight)) {
        msg(DONE);
    } else {
        msg(ERROR);
    }
}

void opt_depth_first_search(StaticGraph &graph) {
    int start = graph.get_vertex(get_string("Inicio>"));
    if(start != -1) {
        graph.depth_first_search(start);
    } else {
        msg(ERROR);
    }
}

void opt_breadth_first_search(StaticGraph &graph) {
    int start = graph.get_vertex(get_string("Inicio>"));
    if(start != -1) {
        graph.breadth_first_search(start);
    } else {
        msg(ERROR);
    }
}

void opt_depth_route(StaticGraph &graph) {
    int start = graph.get_vertex(get_string("Inicio>"));
    int end = graph.get_vertex(get_string("Final>"));
    if(start != -1 && end != -1) {
        graph.depth_first_search(start, end);
    } else {
        msg(ERROR);
    }
}

void opt_breadth_route(StaticGraph &graph) {
    int start = graph.get_vertex(get_string("Inicio>"));
    int end = graph.get_vertex(get_string("Final>"));
    if(start != -1 && end != -1) {
        graph.breadth_first_search(start, end);
    } else {
        msg(ERROR);
    }
}

void opt_best_route(StaticGraph &graph) {
    int start = graph.get_vertex(get_string("Inicio>"));
    int end = graph.get_vertex(get_string("Final>"));
    if(start != -1 && end != -1) {
        graph.dijkstra(start, end);
    } else {
        msg(ERROR);
    }
}

void opt_print(StaticGraph &graph) {
    graph.print();
}
