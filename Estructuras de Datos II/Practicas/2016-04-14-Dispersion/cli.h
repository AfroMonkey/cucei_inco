#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <cstdlib>

#include "item.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_MODIFY 3
#define OPT_DELETE 4
#define OPT_EXIT 0

#define ERR_DUPLICATED "Error, clave duplicada\n"
#define ERR_FULL "Error, base de datos llena\n"
#define ERR_MISSING "Error, no se encontro el producto\n"
#define MSG_DONE "Listo\n"

void print_menu()
{
    cout << OPT_ADD << ")" << "Agregar" << endl;
    cout << OPT_SHOW << ")" << "Mostrar" << endl;
    cout << OPT_MODIFY << ")" << "Modificar" << endl;
    cout << OPT_DELETE << ")" << "Eliminar" << endl;
    cout << OPT_EXIT << ")" << "Salir" << endl;
}

int get_int(int d = -1, string msg = ">")
{
    int i;
    cout << msg;
    if (!(cin >> i))
    {
        i = d;
        cin.clear();
        cin.ignore();
    }
    cin.ignore();
    return i;
}

char* get_string(string msg = ">")
{
    char *s = new char[30];
    cout << msg;
    cin.getline(s, 30);
    return s;
}

void fill_item(Item &item)
{
    cout << "Nombre>";
    cin.getline(item.name_, 30);
    cout << "Precio>";
    cin >> item.price_;
    cout << "Existencias>";
    cin >> item.quantity_;
    cin.ignore();
}

void print_item(const Item &item)
{
    cout << "Nombre>" << item.name_ << endl;
    cout << "Precio>" << item.price_ << endl;
    cout << "Existencias" << item.quantity_ << endl;
    std::cout << endl;
}

void msg(const string s)
{
    cout << s;
}

void pause()
{
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
}

void clear_screen()
{
    system(CLEAR);
}

#endif
