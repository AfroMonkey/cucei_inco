#ifndef DB_CONTROLLER_H
#define DB_CONTROLLER_H

#include <fstream>
#include <cstring>

#include "config.h"
#include "item.h"
#include "cli.h"
#include "disperssion.h"


void create_database()
{
    {
        std::ifstream database(DB_LOCATION);
        if (database)
            return;
    }
    std::ofstream database(DB_LOCATION, std::ios::app);
    Item item;
    unsigned int size = sizeof(Item);
    for (unsigned int i = 0; i < MAX; i++)
    {
        database.write((char*)&item, size);
    }
}

int get_item_location(char *name)
{
    unsigned int base = get_base(name);
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);
    Item aux;
    unsigned int i;
    bool rev;



    database.seekp(base);
    rev = false;
    for (i = base; i != base || !rev; i += sizeof(Item))
    {
        database.read((char*)&aux, sizeof(Item));
        if (database.eof())
        {
            i = 0;
            rev = true;
            continue;
        }
        if (strcmp(aux.name_, "") == 0)
        {
            break;
        }
        if (strcmp(aux.name_, name) == 0)
        {
            database.close();
            return i;
        }
    }
    return -1;
}

Item read_item(unsigned int location)
{
    Item item;
    std::ifstream database(DB_LOCATION);

    database.seekg(location);
    database.read((char*)&item, sizeof(Item));

    database.close();

    return item;
}

void write_item(const Item &item, unsigned int location)
{
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);

    database.seekp(location);
    database.write((char*)&item, sizeof(Item));

    database.close();
}

bool add_item(Item &item)
{
    unsigned int base = get_base(item.name_);
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);
    Item aux;
    unsigned int i;
    bool rev;

    database.seekp(base);
    rev = false;
    for (i = base; i != base || !rev; i += sizeof(Item))
    {
        database.read((char*)&aux, sizeof(Item));
        if (database.eof())
        {
            i = 0;
            rev = true;
            continue;
        }
        if (strcmp(aux.name_, "\x0") == 0)
        {
            break;
        }
    }

    if (i == base + sizeof(Item) && rev)
    {
        database.close();
        return false;
    }

    database.seekp(i);
    database.write((char*)&item, sizeof(Item));
    database.close();

    return true;
}

void show_items()
{
    Item item;
    std::ifstream database(DB_LOCATION);
    while (true)
    {
        database.read((char*)&item, sizeof(Item));
        if (database.eof())
        {
            break;
        }
        if(strcmp(item.name_, "") != 0)
        {
            print_item(item);
        }
    }
    database.close();
}

bool delete_item(char* name)
{
    unsigned int base = get_base(name);
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);
    bool rev;
    unsigned int i;
    Item aux;

    database.seekp(base);
    rev = false;
    for (i = base; i != (unsigned int)base || !rev; i += sizeof(Item))
    {
        database.read((char*)&aux, sizeof(Item));
        if (database.eof())
        {
            i = 0;
            rev = true;
            continue;
        }
        if (strcmp(aux.name_, "") == 0)
        {
            database.close();
            return false;
        }
        if (strcmp(aux.name_, name) == 0)
        {
            break;
        }
    }

    if (i == base + sizeof(Item) && rev)
    {
        database.close();
        return false;
    }

    Item aux2;

    database.seekp(i);
    database.write((char*)&aux2, sizeof(Item));

    rev = false;
    base = i;
    for (i = base; i != (unsigned int)base || !rev; i += sizeof(Item))
    {
        database.read((char*)&aux, sizeof(Item));
        if (database.eof())
        {
            i = 0;
            rev = true;
            continue;
        }
        if (strcmp(aux.name_, "") == 0)
        {
            break;
        }
        if (get_base(aux.name_) <= (int)i)
        {
            database.seekp(i);
            database.write((char*)&aux, sizeof(Item));
            database.write((char*)&aux2, sizeof(Item));
        }
        else
        {
            break;
        }
    }

    database.close();

    return true;
}

#endif
