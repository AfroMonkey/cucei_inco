#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include "pet.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_ADD 1
#define OPC_SHOW 2
#define OPC_MODIFY 3
#define OPC_DELETE 4
#define OPC_SEARCH 5
#define OPC_EXIT 6

#define CHAR_DELIMITER '|'

void add();
void show();
void modify();
void deleteFromFile();
void search();

int main() {
	int opc;

	do {
		cout << OPC_ADD << ") Agregar" << endl;
		cout << OPC_SHOW << ") Mostrar" << endl;
		cout << OPC_MODIFY << ") Modificar" << endl;
		cout << OPC_DELETE << ") Eliminar" << endl;
		cout << OPC_SEARCH << ") Buscar" << endl;
		cout << OPC_EXIT << ") Salir" << endl;
		cout << ">";
		cin >> opc;
		cin.ignore();

		switch(opc) {
			case OPC_ADD: {
				add();
				break;
			}
			case OPC_SHOW: {
				show();
				break;
			}
			case OPC_MODIFY: {
				modify();
				break;
			}
			case OPC_DELETE: {
				deleteFromFile();
				break;
			}
			case OPC_SEARCH: {
				search();
				break;
			}
			case OPC_EXIT: {
				cout << "Cerrando" << endl;
				break;
			}
			default: {
				cout << "Opcion invalida" << endl;
				break;
			}
		}
		cout << "Presione entrar para continuar . . .";
		cin.ignore();
		system(CLEAR);
	} while(opc != OPC_EXIT);

	return 0;
}

Pet* fillPet() {
	Pet *pet = new Pet;

	cout << "Nombre>";
	cin.getline(pet->name, 35);
	cout << "Raza>";
	cin.getline(pet->type, 35);
	cout << "Color>";
	cin.getline(pet->color, 20);
	cout << "Tamanyo>";
	cin.getline(pet->size, 4);

	return pet;
}

Pet* getPet(ifstream &database) {
	Pet *pet = new Pet;
	int i = -1;
	do {
		i++;
		database.read((char*)&pet->name[i], 1);
		if(database.eof()) { //Without this, "infinte loop"
			return nullptr;
		}
	} while(pet->name[i] != CHAR_DELIMITER);
	pet->name[i] = '\x0';
	
	i = -1;
	do {
		i++;
		database.read((char*)&pet->type[i], 1);
	} while(pet->type[i] != CHAR_DELIMITER);
	pet->type[i] = '\x0';
	
	i = -1;
	do {
		i++;
		database.read((char*)&pet->color[i], 1);
	} while(pet->color[i] != CHAR_DELIMITER);
	pet->color[i] = '\x0';
	
	i = -1;
	do {
		i++;
		database.read((char*)&pet->size[i], 1);
	} while(pet->size[i] != CHAR_DELIMITER);
	pet->size[i] = '\x0';

	return pet;
}

void showPet(Pet &pet) {
	cout << "Nombre:" << pet.name << endl;
	cout << "Raza:" << pet.type << endl;
	cout << "Color:" << pet.color << endl;
	cout << "Tamanyo:" << pet.size << endl;
	cout << endl;
}

void add() {
	ofstream database("Database.txt", ios::app);
	if(database.good()) { 
		Pet *pet = fillPet();
		database << pet->name << CHAR_DELIMITER << pet->type << CHAR_DELIMITER << pet->color << CHAR_DELIMITER << pet->size << CHAR_DELIMITER;
		delete pet;
		database.close();
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void show() {
	ifstream database("Database.txt");
	if(database.good()) {
		Pet *pet;
		while(true) {
			pet = getPet(database);
			if(pet != nullptr) {
				showPet(*pet);
				delete pet;
			} else {
				break;	
			}
		}
		database.close();
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void modify() {
	ifstream original("Database.txt");
	if(original.good()) {
		char name[35];
		Pet *pet, *newPet;

		cout << "Nombre>";
		cin.getline(name, 35);

		while(true) {
			pet = getPet(original);
			if(pet != nullptr) {
				if(strcmp(pet->name, name) == 0) {
					break;
				}
			} else {
				break;	
			}
		}
		original.close();
		original.open("Database.txt");
		if(pet != nullptr) {
			if(original.good()) {
				showPet(*pet);
				cout << "Ingrese los nuevos datos" << endl;
				newPet = fillPet();

				ofstream temp("Temp.txt", ios::app);
				if(temp.good()) {
					while(true) {
						pet = getPet(original);
						if(pet != nullptr) {
							if(strcmp(pet->name, name) == 0) {
								temp << newPet->name << CHAR_DELIMITER << newPet->type << CHAR_DELIMITER << newPet->color << CHAR_DELIMITER << newPet->size << CHAR_DELIMITER;
							} else {
								temp << pet->name << CHAR_DELIMITER << pet->type << CHAR_DELIMITER << pet->color << CHAR_DELIMITER << pet->size << CHAR_DELIMITER;
							}
						} else {
							break;	
						}
					}
					temp.close();
					original.close();
					remove("Database.txt");
					rename("Temp.txt", "Database.txt");
				} else {
					cout << "Error al abrir el archivo" << endl;
				}
				original.close();
			} else {
				cout << "Error al abrir el archivo" << endl;
			}
		} else {
			cout << "No se econtro la mascota" << endl;
		}
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void deleteFromFile() {
	ifstream original("Database.txt");
	if(original.good()) {
		char name[35];
		Pet *pet, *newPet;

		cout << "Nombre>";
		cin.getline(name, 35);

		while(true) {
			pet = getPet(original);
			if(pet != nullptr) {
				if(strcmp(pet->name, name) == 0) {
					break;
				}
			} else {
				break;	
			}
		}
		original.close();
		original.open("Database.txt");
		if(pet != nullptr) {
			if(original.good()) {
				char opc;
				showPet(*pet);
				cout << "Seguro que desea eliminar esta mascota(y/n)>";
				cin >> opc;
				cin.ignore();
				if(opc == 'y') {
					ofstream temp("Temp.txt", ios::app);
					if(temp.good()) {
						while(true) {
							pet = getPet(original);
							if(pet != nullptr) {
								if(strcmp(pet->name, name) != 0) {
									temp << pet->name << CHAR_DELIMITER << pet->type << CHAR_DELIMITER << pet->color << CHAR_DELIMITER << pet->size << CHAR_DELIMITER;
								}
							} else {
								break;	
							}
						}
						temp.close();
						original.close();
						remove("Database.txt");
						rename("Temp.txt", "Database.txt");
					} else {
						cout << "Error al abrir el archivo" << endl;
					}
				}
				original.close();
			} else {
				cout << "Error al abrir el archivo" << endl;
			}
		} else {
			cout << "No se econtro la mascota" << endl;
		}
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void search() {
	ifstream database("Database.txt");
	if(database.good()) {
		char name[35];
		Pet *pet, *newPet;

		cout << "Nombre>";
		cin.getline(name, 35);

		while(true) {
			pet = getPet(database);
			if(pet != nullptr) {
				if(strcmp(pet->name, name) == 0) {
					break;
				}
			} else {
				break;	
			}
		}
		
		if(pet != nullptr) {
			showPet(*pet);
		} else {
			cout << "No se econtro a la mascota" << endl;
		}
		database.close();
	} else {
		cout << "Error al abrir el archivo" << endl;
	}	
}
