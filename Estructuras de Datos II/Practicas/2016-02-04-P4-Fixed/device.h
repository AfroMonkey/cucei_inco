#ifndef DEVICE_H
#define DEVICE_H

class Device
{
public:
    int code;
    char description[30];
    char in_date[9];
    float price;
};

#endif
