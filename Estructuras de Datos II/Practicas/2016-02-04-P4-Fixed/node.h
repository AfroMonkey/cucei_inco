#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED
template <class TYPE>
class Node {
public:
    TYPE data;
    Node<TYPE> *prev;
    Node<TYPE> *next;

    Node();
    Node(TYPE data);
};

template <class TYPE>
Node<TYPE>::Node() {
    prev = nullptr;
    next = nullptr;
}

template <class TYPE>
Node<TYPE>::Node(TYPE data) {
    prev = nullptr;
    next = nullptr;
    this->data = data;
}
#endif
