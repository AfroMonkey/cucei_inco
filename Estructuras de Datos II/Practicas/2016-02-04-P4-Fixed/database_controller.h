#ifndef DATABASE_CONTROLLER
#define DATABASE_CONTROLLER

#include <iostream>
#include <fstream>
#include <cstring>

#include "device.h"
#include "main_cli.h"
#include "list.h"

using namespace std;

bool duplicated_description(const char *DATABASE_LOCATION, Device &device) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Device aux;
        bool duplicated = false;
        while(true) {
            database.read((char*)&aux, sizeof(Device));
            if(database.eof()) {
                break;
            }
            if(strcmp(aux.description, device.description) == 0) {
                duplicated = true;
                break;
            }
        }
        database.close();
        if(duplicated) {
            return true;
        }
    }
    return false;
}

void show_devices(const char *DATABASE_LOCATION) {
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Device device;
        while(true) {
            database.read((char*)&device, sizeof(Device));
            if(database.eof()) {
                break;
            }
            print_device(device);
        }
        database.close();
    }
}

void write_device(const char *DATABASE_LOCATION, Device &device) {
    ofstream database(DATABASE_LOCATION, ios::app);
    database.write((char*)&device, sizeof(Device));
    database.close();
}

void replace_device(const char *DATABASE_LOCATION, Device &device, Device &new_device) {
    ifstream database(DATABASE_LOCATION);
    ofstream temp_database("Temp.db", ios::app);
    Device aux;
    while(true) {
        database.read((char*)&aux, sizeof(Device));
        if(database.eof()) {
            break;
        }
        if(aux.code == device.code) {
            temp_database.write((char*)&new_device, sizeof(Device));
        } else {
            temp_database.write((char*)&aux, sizeof(Device));
        }
    }
    database.close();
    temp_database.close();
    remove(DATABASE_LOCATION);
    rename("Temp.db", DATABASE_LOCATION);
}

void remove_device(const char *DATABASE_LOCATION, Device &device) {
    ifstream database(DATABASE_LOCATION);
    ofstream temp_database("Temp.db", ios::app);
    Device aux;
    while(true) {
        database.read((char*)&aux, sizeof(Device));
        if(database.eof()) {
            break;
        }
        if(aux.code != device.code) {
            temp_database.write((char*)&aux, sizeof(Device));
        }
    }
    database.close();
    temp_database.close();
    remove(DATABASE_LOCATION);
    rename("Temp.db", DATABASE_LOCATION);
}

Device search_device(const char *DATABASE_LOCATION, int code) {
    ifstream database(DATABASE_LOCATION);
    Device device;
    if(database) {
        while(true) {
            database.read((char*)&device, sizeof(Device));
            if(database.eof()) {
                break;
            }
            if(device.code == code) {
                return device;
            }
        }
    }
    device.code = -1;
    return device;
}

List<Device>* get_list_devices(const char *DATABASE_LOCATION) {
    List<Device> *list = new List<Device>;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        Device device;
        while(true) {
            database.read((char*)&device, sizeof(Device));
            if(database.eof()) {
                break;
            }
            list->push_back(device);
        }
        database.close();
    }
    return list;
}

#endif
