#include <fstream>

#include "main_cli.h"
#include "device.h"
#include "database_controller.h"
#include "list.h"

#define DATABASE_LOCATION "Devices.db"

void opt_add();
void opt_show();
void opt_modify();
void opt_delete();
void opt_sort();

int main() {
    do {
        print_menu();
        switch(get_int(-1)) {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_MODIFY:
                opt_modify();
                break;
            case OPT_DELETE:
                opt_delete();
                break;
            case OPT_SORT:
                opt_sort();
                break;
            case OPT_EXIT:
                return 0;
                break;
            default:
                msg("Opcion invalida\n");
        }
    }while(true);
}

void opt_add() {
    Device device;
    fill_device(&device);
    if(!duplicated_description(DATABASE_LOCATION, device)) {
        write_device(DATABASE_LOCATION, device);
    } else {
        msg(MSG_ERROR_DUPLICATED_DESCRIPTION);
    }
}

void opt_show() {
    show_devices(DATABASE_LOCATION);
}

void opt_modify() {
    Device device;
    msg("Codigo");
    device = search_device(DATABASE_LOCATION, get_int(-1));
    if(device.code != -1) {
        print_device(device);
        msg("Seguro que desea modificarlo (1/0)");
        if(get_int(0)) {
            Device new_device;
            fill_device(&new_device);    
            if(!duplicated_description(DATABASE_LOCATION, new_device)) {
                replace_device(DATABASE_LOCATION, device, new_device);
            } else {
                msg(MSG_ERROR_DUPLICATED_DESCRIPTION);
            }
        }
    } else {
        msg(MSG_ERROR_NOT_FOUND);
    }
}

void opt_delete() {Device device;
    msg("Codigo");
    device = search_device(DATABASE_LOCATION, get_int(-1));
    if(device.code != -1) {
        print_device(device);
        msg("Seguro que desea eliminarlo (1/0)");
        if(get_int(0)) {
            remove_device(DATABASE_LOCATION, device);
        }
    } else {
        msg(MSG_ERROR_NOT_FOUND);
    }
}

int relation(Device a, Device b) {
    return a.price < b.price;
}

void opt_sort() {
    List<Device> *list;
    list = get_list_devices(DATABASE_LOCATION);
    list->sort(relation);
    list->forEach(print_device);
    delete list;
}
