#ifndef MAIN_CLI_H
#define MAIN_CLI_H

#include <iostream>

#include "device.h"

using namespace std;

#define MSG_ERROR_DUPLICATED_DESCRIPTION "Error, esa descripcion ya existe\n"
#define MSG_ERROR_NOT_FOUND "Error, no se encontro ese dispositivo\n"

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_MODIFY 3
#define OPT_DELETE 4
#define OPT_SORT 5
#define OPT_EXIT 0

void print_menu() {
    cout << OPT_ADD << ") Agregar" << endl;
    cout << OPT_SHOW << ") Mostrar" << endl;
    cout << OPT_MODIFY << ") Modificar" << endl;
    cout << OPT_DELETE << ") Eliminar" << endl;
    cout << OPT_SORT << ") Ordenar" << endl;
    cout << OPT_EXIT << ") Salir" << endl;
}

int get_int(int def) {
    int i;
    cout << ">";
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i = def;
    }
    cin.ignore();
    return i;
}

void msg(const string &m) {
    cout << m;
}

void fill_device(Device *device) {
    cout << "Codigo";
    device->code = get_int(-1);
    cout << "Descripcion>";
    cin >> device->description;
    cout << "Fecha de ingreso>";
    cin >> device->in_date;
    cout << "Precio>";
    cin >> device->price;
    cin.ignore();
}

void print_device(Device device) {
    cout << "Codigo: " << device.code << endl;
    cout << "Descripcion: " << device.description << endl;
    cout << "Fecha de ingreso: " << device.in_date << endl;
    cout << "Precio: " << device.price << endl;
    cout << endl;
}

#endif
