#include <algorithm> /*std::sort*/
#include <fstream>
#include <iostream>
#include <list>
#include <map>

#include "huffman_node.h"

int main(int argc, char *argv[]) {
  /* Input args*/
  if (argc != 4) {
    std::cout << "Usage:\n";
    std::cout << argv[0] << " input_file keys_file output_file"
              << "\n";
    return 1;
  }

  /* Prepare the input files */
  std::ifstream input(argv[1]);
  if (!input) {
    std::cout << "Cant open the file \"" << argv[1] << "\""
              << "\n";
    return 2;
  }

  std::ofstream keys_file(argv[2], std::ios::trunc);
  if (!keys_file) {
    std::cout << "Cant create the file \"" << argv[2] << "\""
              << "\n";
    return 3;
  }

  std::ofstream output(argv[3], std::ios::trunc | std::ios::binary);
  if (!output) {
    std::cout << "Cant create the file \"" << argv[3] << "\""
              << "\n";
    return 4;
  }

  std::string content((std::istreambuf_iterator<char>(input)),
                      std::istreambuf_iterator<char>()); // Read all the file
  input.close();
  std::string sorted(content);
  std::sort(sorted.begin(), sorted.end()); // Sort a copy of the file

  std::list<HuffmanNode *> tree;
  for (int i = 0, reps = 1; sorted[i]; i++, reps++) {
    if (sorted[i] != sorted[i + 1]) // I have a change, is a new node
    {
      HuffmanNode *node = new HuffmanNode(reps, sorted[i]);
      tree.push_back(node);
      reps = 0;
    }
  }

  tree.sort(HuffmanNode::cmp); // Sort the nodes

  while (tree.size() > 1) // We must have only one node, the root
  {
    HuffmanNode *node = new HuffmanNode();

    node->left_ = *tree.begin(); // Get the first node of the list
    tree.pop_front();
    node->right_ = *tree.begin();
    tree.pop_front();
    node->weight_ = node->left_->weight_ + node->right_->weight_;

    tree.push_front(node);
    tree.sort(HuffmanNode::cmp);
  }

  std::map<char, std::string> keys;

  (*tree.begin())->get_dictionary(keys);
  (*tree.begin())->free(); // We dont need more the tree, only the keys map
  delete *tree.begin();

  for (std::map<char, std::string>::iterator it = keys.begin();
       it != keys.end(); ++it) {
    keys_file << it->first << it->second
              << std::endl; // Write the keys in the key_file
  }
  keys_file.close();

  /* Compress */
  std::string bits; // codes generated with the keys and input_file
  char c;
  for (int i = 0; content[i]; i++) {
    bits += keys[content[i]];
  }

  while (bits.length() % 8) // To complete the byte
  {
    bits += '0';
  }

  /* Output */
  int bit;
  c = '\x0';
  for (int i = 0; i <= (int)bits.length(); i++) {
    if (i != 0 && i % 8 == 0) // Each byte is written
    {
      output.write((char *)&c, sizeof(char));
      c = '\x0';
    }
    bit = (bits[i] == '1') ? 1 : 0;
    c |= bit << (7 - i % 8); // Assign the "n" (7 - i%8) bit to the char "c"
  }
  output.close();

  return 0;
}
