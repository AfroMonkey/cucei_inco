#ifndef INDEX_H
#define INDEX_H

template <class T>
class Index {
public:
    T key;
    long int location;
};
#endif
