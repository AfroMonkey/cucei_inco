#ifndef DATABASE_CONTROLLER_H
#define DATABASE_CONTROLLER_H

#include <iostream>
#include <fstream>

#include "index.h"
#include "student.h"
#include "cli.h"
#include "conf.h"

using namespace std;

Index<int>* get_index(const string DATABASE_LOCATION, int code) {
    Index<int> *index = nullptr;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        index = new Index<int>;
        while(true) {
            database.read((char*)&(*index), sizeof(Index<int>));
            if(database.eof()) {
                delete index;
                index = nullptr;
                break;
            }
            if(index->key == code) {
                break;
            }
        }
        database.close();
    }
    return index;
}

bool write_student(const string DATABASE_LOCATION, const string DATABASE_INDEX,  Student &student) {
    ofstream database(DATABASE_LOCATION, ios::app);
    ofstream database_index(DATABASE_INDEX, ios::app);
    if(database && database_index) {
        Index<int> index;
        index.location = database.tellp();
        index.key = student.code;

        int length = student.to_string().length();
        database.write((char*)student.to_string().c_str(), length);

        database_index.write((char*)&index, sizeof(Index<int>));

        database.close();
        database_index.close();
        return true;
    } else {
        return false;
    }
}

Student* get_student(const string DATABASE_LOCATION, Index<int> index) {
    Student *student = nullptr;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        string buffer;
        database.seekg(index.location);
        getline(database, buffer);
        student = new Student(buffer);
    }
    return student;
}

bool print_students(const string DATABASE_LOCATION, const string DATABASE_INDEX) {
    ifstream database_index(DATABASE_INDEX);
    if(database_index) {
        Index<int> index;
        Student *student;
        while(true) {
            database_index.read((char*)&index, sizeof(Index<int>));
            if(database_index.eof()) {
                break;
            }
            student = get_student(DATABASE_LOCATION, index);
            if(student != nullptr) {
                print_student(*student);
            } else {
                //ERROR
            }
        }
        return true;
    } else {
        return false;
    }
}
#endif
