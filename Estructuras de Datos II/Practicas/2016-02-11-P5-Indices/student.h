#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>

#include "conf.h"

using namespace std;

class Student {
public:
    int code;
    string name;
    string career;
    int credits;

    Student();
    Student(const string s);
    string to_string();
};

Student::Student() {
    code = -1;
    credits = -1;
}

Student::Student(const string s) {
    int i, aux = 0;
    for(i=0; s.at(i) != CHAR_DELIMITER_FIELD; i++);
    code = stoi(s.substr(0, i));
    aux += ++i;
    for(i=0; s.at(i) != CHAR_DELIMITER_FIELD; i++);
    name = s.substr(aux, i);
    aux += ++i;
    for(i=0; s.at(i) != CHAR_DELIMITER_FIELD; i++);
    career = s.substr(aux, i);
    aux += ++i;
    for(i=0; s.at(i) != CHAR_DELIMITER_FIELD; i++);
    credits = stoi(s.substr(aux, i));
    aux += ++i;
}

string Student::to_string() {
    string s;
    s = std::to_string(code) + CHAR_DELIMITER_FIELD;
    s += name + CHAR_DELIMITER_FIELD;
    s += career + CHAR_DELIMITER_FIELD;
    s += std::to_string(credits) + CHAR_DELIMITER_FIELD + "\n";
    return s;
}

#endif
