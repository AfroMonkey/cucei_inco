#include "cli.h"
#include "student.h"
#include "index.h"
#include "database_controller.h"

void opt_add();
void opt_show();
void opt_search();
void opt_modify();
void opt_remove();

int main() {
    do {
        clear_screen();
        print_menu();
        switch(get_int()) {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_SEARCH:
                opt_search();
            case OPT_MODIFY:
                opt_modify();
            case OPT_REMOVE:
                opt_remove();
                break;
            case OPT_EXIT:
                return 0;
                break;
            default:
                msg(INVALID_OPTION);
                break;
        }
        pause();
    }while (true);
}

void opt_add() {
    Student student;
    fill_student(student);
    if(get_index(DATABASE_LOCATION_INDEX, student.code) == nullptr) {
        if(write_student(DATABASE_LOCATION_STUDENTS, DATABASE_LOCATION_INDEX, student)) {
            msg(MSG_DONE);
        } else {
            msg(ERROR_FILE);
        }
    } else {
        msg(ERROR_DUPLICATED_STUDENT);
    }
}

void opt_show() {
    if(!print_students(DATABASE_LOCATION_STUDENTS, DATABASE_LOCATION_INDEX)) {
        msg(ERROR_FILE);
    }
}

void opt_search() {
    Index<int> *index = get_index(DATABASE_LOCATION_INDEX, get_int("Registro>"));
    if(index != nullptr) {
        print_student(*get_student(DATABASE_LOCATION_STUDENTS, *index));
    } else {
        msg(ERROR_NOT_FOUDN_STUDENT);
    }
}

void opt_modify() {
    //TODO
}

void opt_remove() {
    //TODO
}
