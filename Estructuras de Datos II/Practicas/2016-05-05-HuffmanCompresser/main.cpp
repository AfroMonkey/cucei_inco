#include <iostream>
#include <fstream>
#include <map>

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        std::cout << "Usage:\n";
        std::cout << argv[0] << " input_file key_file output_file" << "\n";
        return 1;
    }

    std::ifstream input(argv[1]);
    if (!input)
    {
        std::cout << "Cant open the file \"" << argv[1] << "\"" << "\n";
        return 2;
    }
    std::ifstream keys_file(argv[2]);
    if (!keys_file)
    {
        std::cout << "Cant open the file \"" << argv[2] << "\"" << "\n";
        return 3;
    }
    std::ofstream output(argv[3], std::ios::trunc | std::ios::binary);
    if (!output)
    {
        std::cout << "Cant open the file \"" << argv[3] << "\"" << "\n";
        return 4;
    }

    /* Dictionary */
    std::map<char, std::string> keys;
    char c;
    std::string k;
    while (true)
    {
        keys_file.read((char*)&c, sizeof(char));
        if (keys_file.eof()) break;
        getline(keys_file, k);
        keys[c] = k;
    }
    keys_file.close();

    /* Input */
    std::string bits;
    while (true)
    {
        input.read((char*)&c, sizeof(char));
        if (input.eof()) break;
        bits += keys[c];
    }
    input.close();

    while (bits.length() % 8)
    {
        bits += '0';
    }

    /* Output */
    int bit;
    c = '\x0';
    for (int i = 0; i <= (int)bits.length(); i++)
    {
        if (i != 0 && i%8 == 0)
        {
            output.write((char*)&c, sizeof(char));
            c = '\x0';
        }
        bit = (bits[i] == '1') ? 1 : 0;
        c |= bit << (7 - i%8);
    }
    output.close();

    return 0;
}
