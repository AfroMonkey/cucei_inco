#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        std::cout << "Uso: " << argv[0] << " input_file output_file key" << std::endl;
        return 1;
    }
    if (argv[3][1] != '\x0')
    {
        std::cout << "Formato de llave invalido, use un caracter" << std::endl;
        return 2;
    }
    std::ifstream input(argv[1]);
    if (!input)
    {
        std::cout << "No se pudo abrir el archivo " << argv[1] << std::endl;
        return 3;
    }
    std::ofstream output(argv[2], std::ios::trunc);
    if (!output)
    {
        std::cout << "No se pudo crear el archivo " << argv[2] << std::endl;
        return 4;
    }
    char c;
    while (input.read((char*)&c, sizeof(char)))
    {
        c ^= argv[3][0];
        output.write((char*)&c, sizeof(char));
    }
    output.close();
    input.close();

    return 0;
}
