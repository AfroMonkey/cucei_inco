#include <iostream>
#include <fstream>

#define KEY 3

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        std::cout << "Uso: " << argv[0] << " c/d input_file output_file" << std::endl;
        return 1;
    }
    if (argv[1][1] != '\x0' || (argv[1][0] != 'c' && argv[1][0] != 'd'))
    {
        std::cout << "Comando desconocido, utilice 'c' para cifrar y 'd' para descifrar" << std::endl;
        return 2;
    }
    std::ifstream input(argv[2]);
    if (!input)
    {
        std::cout << "No se pudo abrir el archivo " << argv[1] << std::endl;
        return 4;
    }
    std::ofstream output(argv[3], std::ios::trunc);
    if (!output)
    {
        std::cout << "No se pudo crear el archivo " << argv[2] << std::endl;
        return 5;
    }
    char c;
    while (input.read((char*)&c, sizeof(char)))
    {
        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
        {
            c += argv[1][0] == 'c'? KEY : -KEY;
            if (c > 'Z' && c < 'a') c-= 'Z';
            if (c < 'A') c+= 'A';
            if (c > 'z') c-= 'z';
            if (c < 'a' && c > 'Z') c+= 'a';
        }
        output.write((char*)&c, sizeof(char));
    }
    output.close();
    input.close();

    return 0;
}
