#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <cstdlib>

#include "movie.h"
#include "index_entry.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_SHOW_TDA 3
#define OPT_SHOW_GENRES_TDA 4
#define OPT_SHOW_INDEX_GENRE_TDA 5
#define OPT_SHOW_INDEX 6
#define OPT_MODIFY 7
#define OPT_DELETE 8
#define OPT_EXIT 0

#define INVALID_OPTION "Opcion invalida\n"
#define ERROR_FILE "Error, no se pudo abrir el archivo\n"
#define ERROR_DUPLICATED_MOVIE "Error, id duplicado\n"
#define ERROR_MISSING_MOVIE "Error, no se encontro la pelicula\n"
#define MSG_DONE "Listo\n"

class NodeLink {
public:
    int key;
    NodeLink *next;

    NodeLink() {next = nullptr;}
    bool operator==(const NodeLink &node) {return key == node.key;}
    bool operator<(const NodeLink &node) {return key < node.key;}
};

class NodeGenre {
public:
    char genre[35];
    NodeLink *start;

    NodeGenre() {start = nullptr;}
    bool operator==(const NodeGenre &node) {string g = genre, g2 = node.genre; return g == g2;}
    bool operator<(const NodeGenre &node) {string g = genre, g2 = node.genre; return g < g2;}
    void operator=(const char g[35]) {
        for(int i = 0; i < 35; i++) {
            genre[i] = g[i];
        }
    }
};

void print_menu() {
    cout << OPT_ADD << ") Agregar" << endl;
    cout << OPT_SHOW << ") Mostrar" << endl;
    cout << OPT_SHOW_TDA << ") Mostrar TDA" << endl;
    cout << OPT_SHOW_GENRES_TDA << ") Mostrar TDA Generos" << endl;
    cout << OPT_SHOW_INDEX_GENRE_TDA << ") Mostrar TDA Index Generos" << endl;
    cout << OPT_SHOW_INDEX << ") Mostrar con index" << endl;
    cout << OPT_MODIFY << ") Modificar" << endl;
    cout << OPT_DELETE << ") Eliminar" << endl;
    cout << OPT_EXIT << ") Salir" << endl;
}

void print_index_entry(IndexEntry<int> index) {
    cout << "Key: " << index.key << endl;
    cout << "Address: " << index.address << endl;
    cout << endl;
}

int get_int(string msg = ">", int def = -1) {
    int i;
    cout << msg;
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i = def;
    }
    cin.ignore();
    return i;
}

void clear_screen() {
    system(CLEAR);
}

void msg(string m) {
    cout << m;
}

void pause() {
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
}

/*--*/
void fill_movie(Movie &movie, int id = -1) {
    if(id == -1) {
        movie.id = get_int("            ID>");
    } else {
        movie.id = id;
    }
    cout << "        Nombre>";
    cin.getline(movie.name, 35);
    cout << "      Director>";
    cin.getline(movie.director, 35);
    cout << "        Genero>";
    cin.getline(movie.genre, 35);
    cout << "  Protagonista>";
    cin.getline(movie.protagonist, 35);
}

void print_movie(const Movie &movie) {
    cout << "            ID:" << movie.id << endl;
    cout << "        Nombre:" << movie.name << endl;
    cout << "      Director:" << movie.director << endl;
    cout << "        Genero:" << movie.genre << endl;
    cout << "  Protagonista:" << movie.protagonist << endl;
    cout << endl;
}

void print_node_genre(NodeGenre node) {
    cout << "Genero: " << node.genre << endl;
    cout << "Inicia: " << node.start << endl;
    cout << endl;
}

void print_node_link(NodeLink node) {
    cout << "ID: " << node.key << endl;
    cout << "Siguiente: " << node.next << endl;
    cout << endl;
}

#endif
