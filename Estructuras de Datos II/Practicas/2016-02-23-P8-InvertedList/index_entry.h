#ifndef INDEX_H
#define INDEX_H

template <class T>
class IndexEntry {
public:
    T key;
    long int address;

    IndexEntry();
    IndexEntry(T key, long address = -1);
    bool operator==(const IndexEntry<T> &index);
    bool operator<(const IndexEntry<T> &index);
};

template <class T>
IndexEntry<T>::IndexEntry() {
    key = address = 0;
}

template <class T>
IndexEntry<T>::IndexEntry(T key, long address) {
    this->key = key;
    this->address = address;
}

template <class T>
bool IndexEntry<T>::operator==(const IndexEntry<T> &index) {
    return this->key == index.key;
}

template <class T>
bool IndexEntry<T>::operator<(const IndexEntry<T> &index) {
    return this->key < index.key;
}

#endif
