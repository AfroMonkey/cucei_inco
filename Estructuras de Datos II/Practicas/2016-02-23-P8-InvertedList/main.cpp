#include "cli.h"
#include "index_entry.h"
#include "database_controller.h"
#include "singly_linked_list.h"
#include "movie.h"
#include "conf.h"

void opt_add();
void opt_show();
void opt_show_tda();
void opt_show_genres_tda();
void opt_show_index_genre_tda();
void opt_show_index();
void opt_modify();
void opt_delete();

void reload_genres();
void reload_index_genre();

SinglyLinkedList<IndexEntry<int>> index;
SinglyLinkedList<NodeLink> index_genre;
SinglyLinkedList<NodeGenre> genres;

int main() {
    load_index(index);
    load_genres(genres);
    load_index_genre(index_genre);
    reload_genres();
    reload_index_genre();
    do {
        clear_screen();
        print_menu();
        switch(get_int()) {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_SHOW_TDA:
                opt_show_tda();
                break;
            case OPT_SHOW_GENRES_TDA:
                opt_show_genres_tda();
                break;
            case OPT_SHOW_INDEX_GENRE_TDA:
                opt_show_index_genre_tda();
                break;
            case OPT_SHOW_INDEX:
                opt_show_index();
                break;
            case OPT_MODIFY:
                opt_modify();
                break;
            case OPT_DELETE:
                opt_delete();
                break;
            case OPT_EXIT:
                save_index(index);
                save_genres(genres, index_genre);
                save_index_genre(index_genre, genres);
                return 0;
            default:
                msg(INVALID_OPTION);
                break;
        }
        pause();
    }while (true);
}

NodeLink* get_last_index_genre(Node<NodeGenre> *node) {
    NodeLink *aux = node->data_.start;
    while(aux != nullptr && aux->next != nullptr) {
        aux = aux->next;
    }
    return aux;
}

void opt_add() {
    Movie movie;
    IndexEntry<int> indexEntry;
    fill_movie(movie);
    indexEntry.address = get_end(MOVIES_DBL);
    indexEntry.key = movie.id;
    if(index.get(movie.id) == nullptr) {
        if(write_movie(movie, indexEntry.address)) {
            index.insert(indexEntry);
            NodeLink newIndexGenre;
            newIndexGenre.key = movie.id;
            newIndexGenre.next = nullptr;
            index_genre.insert(newIndexGenre);


            NodeGenre aux;
            aux = movie.genre;
            Node<NodeGenre> *node = (genres.get(aux));

            if(node != nullptr) {
                NodeLink *last_index_genre_entry = get_last_index_genre(node);
                last_index_genre_entry->next = &index_genre.get(newIndexGenre)->data_;
            } else {
                NodeGenre newGenre;
                newGenre = movie.genre;
                newGenre.start = &(index_genre.get(newIndexGenre)->data_);
                genres.insert(newGenre);
            }
            msg(MSG_DONE);
        } else {
            msg(ERROR_FILE);
        }
    } else {
        msg(ERROR_DUPLICATED_MOVIE);
    }
}

void print_genre(NodeGenre currentGenre) {
    NodeLink *aux = currentGenre.start;
    cout << "Genero:" << currentGenre.genre << endl;
    Node<IndexEntry<int>> *node;
    while(aux != nullptr) {
        node = index.get(aux->key);
        if(node != nullptr) {
            print_movie(read_movie(node->data_.address));
        }
        aux = aux->next;
    }
}

void opt_show() {
    genres.for_each(print_genre);
}

void opt_show_tda() {
    index.for_each(print_index_entry);
}

void opt_show_genres_tda() {
    genres.for_each(print_node_genre);
}
void opt_show_index_genre_tda() {
    Node<NodeLink> *it = index_genre.begin();
    for(; it; it = it->next_) {
        cout << "Direccion: " << it << endl;
        print_node_link(it->data_);
    }
}

void opt_show_index() {
    index.for_each(print_movie_by_index);
}

NodeLink* prev_node_genre(const NodeGenre genre, const NodeLink *node) {
    if(genre.start == node) {
        return nullptr;
    } else {
        NodeLink *aux = genre.start;
        while(aux != nullptr && aux->next != node) {
            aux = aux->next;
        }
        return aux;
    }
}

void opt_modify() {
    Node<IndexEntry<int>> *node;
    node = index.get(get_int("Codigo>"));
    if(node) {
        Movie movie = read_movie(node->data_.address);
        print_movie(movie);
        if(get_int("Seguro? (1/0)")) {
            string prev_genre = movie.genre;
            fill_movie(movie, movie.id);
            if(movie.genre != prev_genre) {
                NodeGenre genre;
                genre = prev_genre.c_str();
                NodeLink link;
                link.key = movie.id;
                Node<NodeLink> *node_link = index_genre.get(link);
                Node<NodeGenre> *node_genre = genres.get(genre);
                NodeLink *prev = prev_node_genre(node_genre->data_, &node_link->data_);
                if(prev != nullptr) {
                    prev->next = node_link->data_.next;
                    node_link->data_.next = nullptr;
                    NodeGenre newGenre;
                    newGenre = movie.genre;
                    if(node_genre == nullptr) {
                        NodeLink *last_index_genre_entry = get_last_index_genre(node_genre);
                        last_index_genre_entry->next = &index_genre.get(node_link->data_)->data_;
                    } else {
                        Node<NodeGenre> *node_newGenre = (genres.get(newGenre));
                        newGenre.start = &(index_genre.get(node_link->data_)->data_);
                        genres.insert(newGenre);
                    }
                } else {
                    genres.release(node_genre);
                    NodeGenre newGenreX;
                    newGenreX = movie.genre;
                    newGenreX.start = &node_link->data_;
                    genres.insert(newGenreX);
                }
            }
            write_movie(movie, node->data_.address);
        }
    } else {
        msg(ERROR_MISSING_MOVIE);
    }
}

void opt_delete() {
    Node<IndexEntry<int>> *node;
    node = index.get(get_int("Codigo>"));
    if(node) {
        Movie movie = read_movie(node->data_.address);
        print_movie(movie);
        if(get_int("Seguro? (1/0)")) {
            index.remove(movie.id);
            save_index(index);
            if(remove_movie(movie.id)) {
                index.clear();
                load_index(index);

                NodeGenre genre;
                genre = movie.genre;
                NodeLink link;
                link.key = movie.id;
                Node<NodeLink> *node_link = index_genre.get(link);
                Node<NodeGenre> *node_genre = genres.get(genre);
                NodeLink *prev = prev_node_genre(node_genre->data_, &node_link->data_);
                index_genre.release(node_link);
                if(prev != nullptr) {
                    prev->next = node_link->data_.next;
                } else {
                    genres.release(node_genre);
                }

                msg(MSG_DONE);
            } else {
                msg(ERROR_FILE);
            }
        }
    } else {
        msg(ERROR_MISSING_MOVIE);
    }
}

void reload_genres() {
    int start;
    for(Node<NodeGenre> *it = genres.begin(); it ; it = it->next_) {
        start = it->data_.start->key;
        delete it->data_.start;
        it->data_.start = &(index_genre.in(start)->data_);
    }
}

void reload_index_genre() {
    int next;
    for(Node<NodeLink> *it = index_genre.begin(); it ; it = it->next_) {
        next = it->data_.next->key;
        delete it->data_.next;
        it->data_.next = &(index_genre.in(next)->data_);
    }
}
