#include <cstdlib> //NULL

/* Single Node */
#ifndef SINGLE_NODE
#define SINGLE_NODE

template <class T>
class Node {
public:
    T data_;
    Node *next_;

    Node(T data = NULL, Node *next = NULL) {next_ = next; data_ = data;};
};

#endif

/* Singly Linked SinglyLinkedList */
/* Warning: This list is autmatically sorted using the operator "<" */
#ifndef SINGLY_LINKED_LIST_H
#define SINGLY_LINKED_LIST_H

template <class T>
class SinglyLinkedList {
private:
    Node<T> *list_;
    unsigned int size_;
public:
    SinglyLinkedList();
    ~SinglyLinkedList();
    /*Capacity*/
    bool isEmpty();
    int size();
    /*Node<T> access*/
    Node<T>* begin();
    Node<T>* end();
    Node<T>* prev(const Node<T> *node);
    Node<T>* get(const T &data);
    Node<T>* in(int position);
    /*Modifiers*/
    bool pop_front();
    bool pop_back();
    void insert(const T data);
    bool release(Node<T> *position);
    void clear();
    /*Operations*/
    void remove(const T &data, int times = 1);
    /*Miscellaneous*/
    void for_each(void (*function)(T));
    void for_each_equal(T &data, void (*funcion)(T));
    int position(const Node<T> *node);
};

template <class T>
SinglyLinkedList<T>::SinglyLinkedList() {
    list_ = NULL;
    size_ = 0;
}

template <class T>
SinglyLinkedList<T>::~SinglyLinkedList() {
    clear();
}

template <class T>
bool SinglyLinkedList<T>::isEmpty() {
    return list_ == NULL;
}

template <class T>
int SinglyLinkedList<T>::size() {
    return size_;
}

template <class T>
Node<T>* SinglyLinkedList<T>::begin() {
    return list_;
}

template <class T>
Node<T>* SinglyLinkedList<T>::end() {
    if(!isEmpty()) {
        Node<T> *aux;
        for(aux = list_; aux->next_ != NULL; aux = aux->next_);
        return aux;
    } else {
        return NULL;
    }
}

template <class T>
Node<T>* SinglyLinkedList<T>::prev(const Node<T> *node) {
    if(node != NULL && node != list_) {
        Node<T> *aux;
        for(aux = list_; aux != NULL && aux->next_ != node; aux = aux->next_);
        return aux;
    } else {
        return NULL;
    }
}

template <class T>
Node<T>* SinglyLinkedList<T>::get(const T &data) {
    for(Node<T> *aux = list_; aux != NULL; aux = aux->next_) {
        if(aux->data_ == data) {
            return aux;
        }
    }
    return NULL;
}

template <class T>
Node<T>* SinglyLinkedList<T>::in(int position) {
    Node<T> *aux;
    for(aux = list_; aux != NULL && position != 0; aux = aux->next_, position--);
    return aux;
}

template <class T>
bool SinglyLinkedList<T>::pop_front() {
    if(!isEmpty()) {
        Node<T> *aux;
        aux = list_;
        list_ = list_->next_;
        delete aux;
        size_--;
        return true;
    } else {
        return false;
    }
}

template <class T>
bool SinglyLinkedList<T>::pop_back() {
    if(!isEmpty()) {
        if(list_ == end()) {
            delete list_;
            list_ = NULL;
        } else {
            Node<T> *aux = list_;
            for(aux = list_; aux->next_->next_ != NULL; aux = aux->next_);
            delete aux->next_;
            aux->next_ = NULL;
        }
        return true;
    } else {
        return false;
    }
}

template <class T>
void SinglyLinkedList<T>::insert(const T data) {
    Node<T> *node = new Node<T>(data);
    if(isEmpty()) {
        list_ = node;
    } else {
        if(node->data_ < list_->data_) {
            node->next_ = list_;
            list_ = node;
        } else {
            Node<T> *aux = list_;
            while(true) {
                if(aux->next_ != NULL && aux->next_->data_ < node->data_) {
                    aux = aux->next_;
                } else {
                    break;
                }
            }
            node->next_ = aux->next_;
            aux->next_ = node;
        }
    }
}

template <class T>
bool SinglyLinkedList<T>::release(Node<T> *position) {
    if(position != NULL) {
        if(position == list_) {
            list_ = list_->next_;
            delete position;
            return true;
        } else {
            Node<T> *aux = prev(position);
            if(aux != NULL) {
                aux->next_ = position->next_;
                delete position;
                size_--;
                return true;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}

template <class T>
void SinglyLinkedList<T>::clear() {
    for(Node<T> *aux = list_; aux != NULL; aux = list_) {
        list_ = list_->next_;
        delete aux;
    }
    size_ = 0;
}

template <class T>
void SinglyLinkedList<T>::remove(const T &data, int times) {
    if(!isEmpty()) {
        Node<T> *aux2;

        for(Node<T> *aux = list_; aux->next_ != NULL && times != 0;) {
            if(aux->next_->data_ == data) {
                aux2 = aux->next_;
                aux->next_ = aux2->next_;
                delete aux2;
                times--;
                size_--;
            } else {
                aux = aux->next_;
            }
        }
        if(list_->data_ == data) {
            pop_front();
        }
    }
}

template <class T>
void SinglyLinkedList<T>::for_each(void (*function)(T)) {
    for(Node<T> *aux = list_; aux != NULL; aux = aux->next_) {
        (*function)(aux->data_);
    }
}

template <class T>
void SinglyLinkedList<T>::for_each_equal(T &data, void (*function)(T)) {
    for(Node<T> *aux = list_; aux != NULL; aux = aux->next_) {
        if(aux->data_ == data) {
            (*function)(aux->data_);
        }
    }
}

template <class T>
int SinglyLinkedList<T>::position(const Node<T> *node) {
    int position = -1;
    Node<T> *aux;
    for(aux = list_; aux != NULL; aux = aux->next_) {
        position++;
        if(aux == node) {
            break;
        }
    }
    if(aux != NULL) {
        return position;
    } else {
        return -1;
    }
}

#endif
