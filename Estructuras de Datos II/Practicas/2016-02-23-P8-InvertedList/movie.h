#ifndef MOVIE_H
#define MOVIE_H

#include "conf.h"

class Movie {
public:
    int id;
    char name[MAX_SIZE_NAME];
    char director[MAX_SIZE_DIRECTOR];
    char genre[MAX_SIZE_GENRE];
    char protagonist[MAX_SIZE_STELLAR];
};

#endif
