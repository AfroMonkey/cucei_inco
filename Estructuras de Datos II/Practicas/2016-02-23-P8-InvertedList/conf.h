/* Files */
#define MOVIES_DBL "movies.db"
#define INDEX_DBL "index.db"
#define GENRES_DBL "genres.db"
#define INDEX_GENRE_DBL "index_genres.db"

/* Movie */
#define MAX_SIZE_NAME 35
#define MAX_SIZE_DIRECTOR 35
#define MAX_SIZE_GENRE 35
#define MAX_SIZE_STELLAR 35
