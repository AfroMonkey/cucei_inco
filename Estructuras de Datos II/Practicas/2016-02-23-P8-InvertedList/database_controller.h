#ifndef DATABASE_CONTROLLER_H
#define DATABASE_CONTROLLER_H

#include <iostream>
#include <fstream>

using namespace std;

#include "conf.h"
#include "singly_linked_list.h"
#include "index_entry.h"
#include "movie.h"
#include "cli.h"

void load_index(SinglyLinkedList<IndexEntry<int>> &index) {
    ifstream database(INDEX_DBL);
    if(database) {
        IndexEntry<int> indexEntry;
        while(true) {
            database.read((char*)&indexEntry, sizeof(IndexEntry<int>));
            if(database.eof()) {
                break;
            }
            index.insert(indexEntry);
        }
        database.close();
    }
}

void load_genres(SinglyLinkedList<NodeGenre> &genres) {
    ifstream database(GENRES_DBL);
    if(database) {
        NodeGenre node_genre;
        NodeLink *node_next;
        char genre[35];
        int next;
        while(true) {
            database.read((char*)genre, 35);
            database.read((char*)&next, sizeof(int));
            if(database.eof()) {
                break;
            }
            node_next = new NodeLink;
            node_next->key = next;
            node_genre = genre;
            node_genre.start = node_next;
            genres.insert(node_genre);
        }
        database.close();
    }
}

void load_index_genre(SinglyLinkedList<NodeLink> &index_genre) {
    ifstream database(INDEX_GENRE_DBL);
    if(database) {
        NodeLink node_link;
        NodeLink *node_next;
        int key;
        int next;
        while(true) {
            database.read((char*)&key, sizeof(int));
            database.read((char*)&next, sizeof(int));
            if(database.eof()) {
                break;
            }
            node_next = new NodeLink;
            node_next->key = next;
            node_link.key = key;
            node_link.next = node_next;
            index_genre.insert(node_link);
        }
        database.close();
    }
}

void save_index(SinglyLinkedList<IndexEntry<int>> &index) {
    remove(INDEX_DBL);
    if(!index.isEmpty()) {
        ofstream database(INDEX_DBL, ios::app);
        if(database) {
            Node<IndexEntry<int>> *it;
            for(it = index.begin(); it; it = it->next_) {
                database.write((char*)&(it->data_), sizeof(IndexEntry<int>));
            }
            database.close();
        }
    }
}

void save_genres(SinglyLinkedList<NodeGenre> &genres, SinglyLinkedList<NodeLink> &index_genre) {
    remove(GENRES_DBL);
    if(!genres.isEmpty()) {
        ofstream database(GENRES_DBL, ios::app);
        int start;
        if(database) {
            Node<NodeGenre> *it;
            for(it = genres.begin(); it; it = it->next_) {
                database.write((char*)it->data_.genre, sizeof(char[35]));
                start = index_genre.position((Node<NodeLink>*)it->data_.start);
                database.write((char*)&start, sizeof(int));
            }
            database.close();
        }
    }
}

void save_index_genre(SinglyLinkedList<NodeLink> &index_genre, SinglyLinkedList<NodeGenre> &genres) {
    remove(INDEX_GENRE_DBL);
    if(!genres.isEmpty()) {
        ofstream database(INDEX_GENRE_DBL, ios::app);
        int next;
        if(database) {
            Node<NodeLink> *it;
            for(it = index_genre.begin(); it; it = it->next_) {
                database.write((char*)&(it->data_.key), sizeof(int));
                next = index_genre.position((Node<NodeLink>*)it->data_.next);
                database.write((char*)&next, sizeof(int));
            }
            database.close();
        }
    }
}

long get_end(const string &dbl) {
    long address = 0;
    ifstream database(dbl);
    if(database) {
        database.seekg(0, ios::end);
        address = database.tellg();
        database.close();
    }
    return address;
}

bool write_movie(const Movie &movie, const long address) {
    fstream database(MOVIES_DBL, ios::in | ios::out);
    if(!database) {
        database.open(MOVIES_DBL, ios::app);
    }
    if(database) {
        database.seekp(address);
        database.write((char*)&movie, sizeof(Movie));
        database.close();
        return true;
    } else {
        return false;
    }
}

bool remove_movie(const int id) {
    ifstream database(MOVIES_DBL);
    ofstream temp("temp.db", ios::app);
    if(database && temp) {
        Movie movie;
        while(true) {
            database.read((char*)&movie, sizeof(Movie));
            if(database.eof()) {
                break;
            }
            if(movie.id != id) {
                temp.write((char*)&movie, sizeof(Movie));
            }
        }
        database.close();
        temp.close();
        remove(MOVIES_DBL);
        rename("temp.db", MOVIES_DBL);
        return true;
    } else {
        return false;
    }
}

Movie read_movie(const long address) {
    Movie movie;
    ifstream database(MOVIES_DBL);
    if(database) {
        database.seekg(address);
        database.read((char*)&movie, sizeof(Movie));
        database.close();
    }
    return movie;
}

void print_movie_by_index(IndexEntry<int> index) {
    print_movie(read_movie(index.address));
}

#endif
