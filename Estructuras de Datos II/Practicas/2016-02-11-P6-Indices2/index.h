#ifndef INDEX_H
#define INDEX_H

template <class T>
class Index {
public:
    T key;
    long int address;

    Index();
    Index(T key, long address);
    bool operator==(const Index<T> &index);
    bool operator<(const Index<T> &index);
};

template <class T>
Index<T>::Index() {
    key = address = 0;
}

template <class T>
Index<T>::Index(T key, long address) {
    this->key = key;
    this->address = address;
}

template <class T>
bool Index<T>::operator==(const Index<T> &index) {
    return this->key == index.key;
}

template <class T>
bool Index<T>::operator<(const Index<T> &index) {
    return this->key < index.key;
}
#endif
