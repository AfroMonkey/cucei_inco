#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <cstdlib>

#include "student.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_SEARCH 3
#define OPT_MODIFY 4
#define OPT_REMOVE 5
#define OPT_SHOW_INDEX 6
#define OPT_EXIT_AND_SAVE 0

#define INVALID_OPTION "Opcion invalida\n"
#define ERROR_DUPLICATED_STUDENT "Error, ese alumno ya existe\n"
#define ERROR_FILE "Error, no se pudo abrir el archivo\n"
#define ERROR_MISSING_STUDENT "Error, no se encontro al estudiante\n"
#define MSG_DONE "Listo\n"

void print_menu() {
    cout << OPT_ADD << ") Agregar " << endl;
    cout << OPT_SHOW << ") Mostrar " << endl;
    cout << OPT_SEARCH << ") Buscar " << endl;
    cout << OPT_MODIFY << ") Modificar " << endl;
    cout << OPT_REMOVE << ") Eliminar " << endl;
    cout << OPT_SHOW_INDEX << ") Mostrar archivo indices " << endl;
    cout << OPT_EXIT_AND_SAVE << ") Guardar y Salir " << endl;
}

int get_int(string msg = ">", int def = -1) {
    int i;
    cout << msg;
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i = def;
    }
    cin.ignore();
    return i;
}

void clear_screen() {
    system(CLEAR);
}

void msg(string m) {
    cout << m;
}

void pause() {
    cout << "Presione entrar para continuar . . .";
    cin.ignore();
}

/*--*/
void fill_student(Student &student, int code = -1) {
    if(code != -1) {
        student.code = code;
    } else {
        student.code = get_int("Registro>");
    }
    cout << "Nombre>";
    cin >> student.name;
    cout << "Carrera>";
    cin >> student.career;
    student.credits = get_int("Creditos>");
}

void print_student(Student &student) {
    cout << "Registro:" << student.code << endl;
    cout << "Nombre:" << student.name << endl;
    cout << "Carrera:" << student.career << endl;
    cout << "Creditos:" << student.credits << endl;
    cout << endl;
}

#endif
