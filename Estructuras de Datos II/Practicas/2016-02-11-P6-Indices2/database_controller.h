#ifndef DATABASE_CONTROLLER_H
#define DATABASE_CONTROLLER_H

#include <iostream>
#include <fstream>

#include "index.h"
#include "student.h"
#include "cli.h"
#include "conf.h"
#include "doublyLinkedList.h"

using namespace std;

Index<int>* get_index(const string DATABASE_LOCATION, int code) {
    Index<int> *index = nullptr;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        index = new Index<int>;
        while(true) {
            database.read((char*)&(*index), sizeof(Index<int>));
            if(database.eof()) {
                delete index;
                index = nullptr;
                break;
            }
            if(index->key == code) {
                break;
            }
        }
        database.close();
    }
    return index;
}

bool write_student(const string DATABASE_LOCATION, const string DATABASE_INDEX,  Student &student) {
    ofstream database(DATABASE_LOCATION, ios::app);
    ofstream database_index(DATABASE_INDEX, ios::app);
    if(database && database_index) {
        Index<int> index;
        index.address = database.tellp();
        index.key = student.code;


        student.write(database);

        database_index.write((char*)&index, sizeof(Index<int>));

        database.close();
        database_index.close();
        return true;
    } else {
        return false;
    }
}

Student* get_student(const string DATABASE_LOCATION, Index<int> index) {
    Student *student = nullptr;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        student = new Student();
        string buffer;
        database.seekg(index.address);
        student->read(database);
        database.close();
    }
    return student;
}

bool print_students(const string DATABASE_LOCATION, const string DATABASE_INDEX) {
    ifstream database_index(DATABASE_INDEX);
    if(database_index) {
        Student student;
        Index<int> index;
        while(true) {
            database_index.read((char*)&index, sizeof(Index<int>));
            if(database_index.eof()) {
                break;
            }
            ifstream database(DATABASE_LOCATION);
            database.seekg(index.address);
            student.read(database);
            print_student(student);
        }
        database_index.close();
        return true;
    } else {
        return false;
    }
}

bool write_student(const string &DATABASE_LOCATION, const Index<int> &index, const Student &student) {
    fstream database(DATABASE_LOCATION, ios::in | ios::out);
    if(!database) {
        database.open(DATABASE_LOCATION, ios::app);
    }
    if(database) {
        database.seekp(index.address);
        database.write((char*)&student, sizeof(Student));
        database.close();
        return true;
    } else {
        return false;
    }
}

bool remove_student(const string &DATABASE_LOCATION, const string &DATABASE_INDEX, Index<int> &index) {
    ofstream tmp_database1(TEMP_DATABASE, ios::app);
    ofstream tmp_database2(TEMP_DATABASE2, ios::app);
    ifstream database_index(DATABASE_INDEX);
    if(tmp_database1 && database_index) {
        Index<int> *aux = new Index<int>;
        Student *student;
        while(true) {
            database_index.read((char*)aux, sizeof(Index<int>));
            if(database_index.eof()) {
                break;
            }
            student = get_student(DATABASE_LOCATION, *aux);
            if(student->code != index.key) {
                aux->address = tmp_database1.tellp();
                student->write(tmp_database1);
                tmp_database2.write((char*)aux, sizeof(Index<int>));
            }
        }
        tmp_database1.close();
        tmp_database2.close();
        database_index.close();
        remove(DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE, DATABASE_LOCATION.c_str());
        rename(TEMP_DATABASE2, DATABASE_INDEX.c_str());
        return true;
    } else {
        return false;
    }
}

bool read_index(const string &DATABASE_INDEX, DoublyLinkedList<Index<int>> *list) {
    ifstream database_index(DATABASE_INDEX);
    if(database_index) {
        Index<int> *index;
        while(true) {
            index = new Index<int>;
            database_index.read((char*)index, sizeof(Index<int>));
            if(database_index.eof()) {
                break;
            }
            list->insert(*index);
        }
        database_index.close();
        return true;
    } else {
        return false;
    }
}

long get_EOF(const string &DATABASE_LOCATION) {
    long address = 0;
    ifstream database(DATABASE_LOCATION);
    if(database) {
        database.seekg(0, ios::end);
        address = database.tellg();
        database.close();
    }
    return address;
}

#endif
