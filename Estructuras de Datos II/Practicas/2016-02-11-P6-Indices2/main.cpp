#include "cli.h"
#include "student.h"
#include "index.h"
#include "database_controller.h"
#include "doublyLinkedList.h"

void opt_add();
void opt_show();
void opt_search();
void opt_modify();
void opt_remove();
void opt_show_index();
void opt_exit_and_save();

bool write_index(const string &DATABASE_INDEX);

DoublyLinkedList<Index<int>> *list;

int main() {
    list = new DoublyLinkedList<Index<int>>;
    read_index(DATABASE_LOCATION_INDEX, list);
    do {
        clear_screen();
        print_menu();
        switch(get_int()) {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_SEARCH:
                opt_search();
                break;
            case OPT_MODIFY:
                opt_modify();
                break;
            case OPT_REMOVE:
                opt_remove();
                break;
            case OPT_SHOW_INDEX:
                opt_show_index();
                break;
            case OPT_EXIT_AND_SAVE:
                opt_exit_and_save();
                return 0;
            default:
                msg(INVALID_OPTION);
                break;
        }
        pause();
    }while (true);
}

void opt_add() {
    Student student;
    fill_student(student);
    Index<int> *index = new Index<int>(student.code, 0);
    if(list->get_by_data(*index) == nullptr) {
        index->address = get_EOF(DATABASE_LOCATION_STUDENTS);
        list->insert(*index);
        if(write_student(DATABASE_LOCATION_STUDENTS, *index, student)) {
            msg(MSG_DONE);
        } else {
            msg(ERROR_FILE);
        }
    } else {
        msg(ERROR_DUPLICATED_STUDENT);
    }
}

void opt_show() {
    if(!list->isEmpty()) {
        Node<Index<int>> *it = list->begin();
        Node<Index<int>> *end = list->end();
        while(it != end) {
            print_student(*get_student(DATABASE_LOCATION_STUDENTS, it->data));
            it = it->next;
        }
        print_student(*get_student(DATABASE_LOCATION_STUDENTS, it->data));
    }
}

void opt_search() {
    Node<Index<int>> *node = list->get_by_data(*new Index<int>(get_int("Registro>"), 0));
    if(node != nullptr) {
        print_student(*get_student(DATABASE_LOCATION_STUDENTS, node->data));
    } else {
        msg(ERROR_MISSING_STUDENT);
    }
}

void opt_modify() {
    int code = get_int("Registro>");
    Node<Index<int>> *node = list->get_by_data(*new Index<int>(code, 0));
    if(node != nullptr) {
        print_student(*get_student(DATABASE_LOCATION_STUDENTS, node->data));
        if(get_int("Seguro (1/0)?")) {
            Student student;
            fill_student(student, code);
            if(write_student(DATABASE_LOCATION_STUDENTS, node->data, student)) {
                msg(MSG_DONE);
            } else {
                msg(ERROR_FILE);
            }
        }
    } else {
        msg(ERROR_MISSING_STUDENT);
    }
}

void opt_remove() {
    int code = get_int("Registro>");
    Node<Index<int>> *node = list->get_by_data(*new Index<int>(code, 0));
    if(node != nullptr) {
        print_student(*get_student(DATABASE_LOCATION_STUDENTS, node->data));
        if(get_int("Seguro (1/0)?")) {
            write_index(DATABASE_LOCATION_INDEX);
            if(remove_student(DATABASE_LOCATION_STUDENTS, DATABASE_LOCATION_INDEX, node->data)) {
                msg(MSG_DONE);
            } else {
                msg(ERROR_FILE);
            }
            delete list;
            list = new DoublyLinkedList<Index<int>>;
            read_index(DATABASE_LOCATION_INDEX, list);
        }
    } else {
        msg(ERROR_MISSING_STUDENT);
    }
}

void opt_show_index() {
    ifstream database_index(DATABASE_LOCATION_INDEX);
    if(database_index) {
        Index<int> index;
        while(true) {
            database_index.read((char*)&index, sizeof(Index<int>));
            if(database_index.eof()) {
                break;
            }
            cout << "Key:" << index.key << endl;
            cout << "Address:" << index.address << endl;
        }
    }
}

void opt_exit_and_save() {
    if(write_index(DATABASE_LOCATION_INDEX)) {
        msg(MSG_DONE);
    } else {
        msg(ERROR_FILE);
    }
}

bool write_index(const string &DATABASE_INDEX) {
    remove(DATABASE_INDEX.c_str());
    ofstream database(DATABASE_LOCATION_INDEX, ios::app);
    if(database) {
        if(!list->isEmpty()) {
            Node<Index<int>> *it = list->begin();
            Node<Index<int>> *end = list->end();
            while(it != end) {
                database.write((char*)&it->data, sizeof(Index<int>));
                it = it->next;
            }
            database.write((char*)&it->data, sizeof(Index<int>));
        }
        database.close();
        return true;
    } else {
        return false;
    }
}
