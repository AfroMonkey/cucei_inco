#ifndef STUDENT_H
#define STUDENT_H

#include <fstream>
#include "conf.h"

class Student {
public:
    int code;
    char name[30];
    char career[30];
    int credits;

    Student();
    void write(std::ofstream &database);
    void read(std::ifstream &database);
};

Student::Student() {
    code = -1;
    credits = -1;
}

void Student::write(std::ofstream &database) {
    database.write((char*)this, sizeof(Student));
}

void Student::read(std::ifstream &database) {
    database.read((char*)this, sizeof(Student));
}

#endif
