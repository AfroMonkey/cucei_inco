#ifndef PATIENT_H
#define PATIENT_H

#include <iostream>

class Patient {
public:
    std::string name;
    std::string age;
    std::string illness;
    std::string treatment;
};

#endif
