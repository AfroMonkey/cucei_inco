/*
 * Author: Navarro Presas Moises Alejandro
 * Reg: 215861509
 * Subject: Estructuras de Datos II, D07
 * Professor: Macias Brambilia Hassem Ruben
 * Compiler: mingw (gcc version 5.3.0 (GCC))
 * Date: 2016-01-28
 */

#include "patient.h"
#include "database_controller.h"
#include "command_line_interface.h"

#define DATABASE_LOCATION "Patients.db"

void menu_add();
void menu_show();
void menu_modiffy();
void menu_delete();
void menu_search_name();
void menu_search_treatment();

int main() {
    int opt;
    do {
        show_menu();
        opt = get_int(-1);
        switch(opt) {
            case OPT_ADD:
                menu_add();
                break;
            case OPT_SHOW:
                menu_show();
                break;
            case OPT_MODIFY:
                menu_modiffy();
                break;
            case OPT_DELETE:
                menu_delete();
                break;
            case OPT_SEARCH_NAME:
                menu_search_name();
                break;
            case OPT_SEARCH_TREATMENT:
                menu_search_treatment();
                break;
            case OPT_EXIT:
                break;
            default:
                msg("Opcion invalida\n");
                break;
        }
        pause();
        clear_screen();
    } while(opt != OPT_EXIT);
    return 0;
}

void menu_add() {
    Patient *patient = new Patient;

    fill_patient(patient);
    if(write_patient(patient, DATABASE_LOCATION)) {
        msg("Paciente añadido al archivo\n");
    } else {
        msg("Error al escribir en el archivo\n");
    }
    delete patient;
}

void menu_show() {
    show_patients(DATABASE_LOCATION);
}

void menu_modiffy() {
    string name;
    cout << "Nombre>";
    getline(cin, name);
    Patient *patient = search_patient(name, DATABASE_LOCATION);
    Patient *new_patient = new Patient;
    if(patient != nullptr) {
        show_patient(patient);
        msg("Introduzca los nuevos datos\n");
        fill_patient(new_patient);
        if(replace_patient(name, new_patient, DATABASE_LOCATION)) {
            msg("Paciente modificado\n");
        } else {
            msg("Error, no se pudo modificar el paciente\n");
        }
    } else {
        msg("Error, no se pudo encontrar al paciente\n");
    }
    delete new_patient;
}

void menu_delete() {
    string name;
    cout << "Nombre>";
    getline(cin, name);
    Patient *patient = search_patient(name, DATABASE_LOCATION);
    if(patient != nullptr) {
        show_patient(patient);
        msg("Seguro que desea eliminar este emeplado (1/0)");
        if(get_int(0)) {
            if(remove_patient(name, DATABASE_LOCATION)) {
                msg("Paciente eliminado\n");
            } else {
                msg("Error, no se pudo modificar el paciente\n");
            }
        } else {
            msg("Operacion abortada\n");
        }
    } else {
        msg("Error, no se pudo encontrar al paciente\n");
    }
}

void menu_search_name() {
    string name;
    cout << "Nombre>";
    getline(cin, name);
    Patient *patient = search_patient(name, DATABASE_LOCATION);
    if(patient != nullptr) {
        show_patient(patient);
    } else {
        msg("Error, no se pudo encontrar al paciente\n");
    }
}

void menu_search_treatment() {
    string treatment;
    cout << "Tratamiento>";
    getline(cin, treatment);
    show_patients_treatment(treatment, DATABASE_LOCATION);
}
