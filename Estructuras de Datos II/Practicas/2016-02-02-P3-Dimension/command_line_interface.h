#ifndef COMMAND_LINE_INTERFACE_H
#define COMMAND_LINE_INTERFACE_H

#include <iostream>
#include <cstdlib>
#include "patient.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_ADD 1
#define OPT_SHOW 2
#define OPT_MODIFY 3
#define OPT_DELETE 4
#define OPT_SEARCH_NAME 5
#define OPT_SEARCH_TREATMENT 6
#define OPT_EXIT 0

#define ERROR_FILE "No se pudo abrir el arcvhivo"
#define ERROR_PATIENT_NOT_FOUND "No hay pacientes con esas caracteristicas"
#define MSG_ABORTED "Operacion cancelada"

void pause() {
    cout << "Presione entrar para continuar . . ." << endl;
    cin.ignore();
}

void clear_screen() {
    system(CLEAR);
}

void show_menu() {
    cout << OPT_ADD << ") Agregar" << endl;
    cout << OPT_SHOW << ") Mostrar" << endl;
    cout << OPT_MODIFY << ") Modificar" << endl;
    cout << OPT_DELETE << ") Eliminar" << endl;
    cout << OPT_SEARCH_NAME << ") Buscar por nombre" << endl;
    cout << OPT_SEARCH_TREATMENT << ") Buscar por tratamiento" << endl;
    cout << OPT_EXIT << ") Salir" << endl;
}

int get_int(int def) {
    int i;
    cout << ">";
    if(!(cin >> i)) {
        cin.clear();
        cin.ignore();
        i = def;
    }
    cin.ignore();
    return i;
}

void msg(const string &m) {
    cout << m;
}

void fill_patient(Patient *patient) {
    cout << "Nombre>";
    getline(cin, patient->name);
    cout << "Edad>";
    cin >> patient->age;
    cin.ignore();
    cout << "Padecimineto>";
    getline(cin, patient->illness);
    cout << "Tratamiento>";
    getline(cin, patient->treatment);
}

void show_patient(Patient *patient) {
    cout << "Nombre:";
    cout <<  patient->name << endl;
    cout << "Edad:";
    cout << patient->age << endl;
    cout << "Padecimiento:";
    cout <<  patient->illness << endl;
    cout << "Tratamiento:";
    cout <<  patient->treatment << endl;
    cout << endl;
}

string get_database_location() {
    string database_location;

    cout << "1) Operativo" << endl;
    cout << "2) Directivo" << endl;
    switch(get_int(-1)) {
        case 2:
            database_location = "Directivos.db";
            break;
        default:
            cout <<"Opcion invalida, base de datos por defecto: Operativos.db" << endl;
        case 1:
            database_location = "Operativos.db";
            break;
    }
    return database_location;
}

int get_code() {
    cout << "Codigo";
    return get_int(-1);
}
#endif
