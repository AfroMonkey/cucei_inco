#ifndef DATA_BASE_CONTROLLER
#define DATA_BASE_CONTROLLER

#include <fstream>
#include "patient.h"
#include "command_line_interface.h" //show_employe(Employe *);

using namespace std;

#define CHAR_DELIMITER '|'

Patient* get_employe(ifstream &database) {
    Patient *patient = new Patient;
    int length;
    char *buffer;

    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    patient->name = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    patient->age = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    patient->illness = buffer;
    delete[] buffer;
    database.read((char*)&length, sizeof(int));
    buffer = new char[length+1];
    buffer[length] = '\x0';
    database.read((char*)buffer, length);
    patient->treatment = buffer;
    delete[] buffer;

    if(database.eof()) {
        return nullptr;
    } else {
        return patient;
    }
}

bool write_patient(Patient *patient, const string &database_location) {
    ofstream database(database_location, ios::app);
    if(database) {
        int length;
        length = patient->name.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)patient->name.c_str(), length);
        length = patient->age.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)patient->age.c_str(), length);
        length = patient->illness.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)patient->illness.c_str(), length);
        length = patient->treatment.length();
        database.write((char*)&length, sizeof(int));
        database.write((char*)patient->treatment.c_str(), length);
        database.close();
        database.close();
        return true;
    } else {
        return false;
    }
}

Patient* search_patient(const string &name, const string &database_location) {
    ifstream database(database_location);

    if(database) {
        Patient *patient;
        while(true) {
            patient = get_employe(database);
            if(patient != nullptr) {
                if(patient->name == name) {
                    break;
                }
            } else {
                break;
            }
        }
        database.close();
        if(patient != nullptr && patient->name == name) {
            return patient;
        }
    }
    return nullptr;
}

bool remove_patient(const string &name, const string &database_location) {
    ifstream database(database_location);

    if(database) {
        const string temp_database_location = "temp.db";
        Patient *patient;

        while(true) {
            patient = get_employe(database);
            if(patient != nullptr) {
                if(patient->name != name) {
                    write_patient(patient, temp_database_location);
                }
            } else {
                break;
            }
        }
        database.close();
        remove(database_location.c_str());
        rename(temp_database_location.c_str(), database_location.c_str());
        return true;
    }
    return false;
}

bool replace_patient(const string &name, Patient *new_patient, const string &database_location) {
    ifstream database(database_location);

    if(database) {
        const string temp_database_location = "temp.db";
        Patient *patient;

        while(true) {
            patient = get_employe(database);
            if(patient != nullptr) {
                if(patient->name == name) {
                    write_patient(new_patient, temp_database_location);
                } else {
                    write_patient(patient, temp_database_location);
                }
            } else {
                break;
            }
        }
        database.close();
        remove(database_location.c_str());
        rename(temp_database_location.c_str(), database_location.c_str());
        return true;
    }
    return false;
}

void show_patients(const string &database_location) { //Search how to remove this
    ifstream database(database_location);

    if(database) {
        Patient *patient;

        while(true) {
            patient = get_employe(database);
            if(patient != nullptr) {
                show_patient(patient);
            } else {
                break;
            }
        }
        database.close();
    }
}

void show_patients_treatment(const string &treatment, const string &database_location) { //Search how to remove this
    ifstream database(database_location);

    if(database) {
        Patient *patient;

        while(true) {
            patient = get_employe(database);
            if(patient != nullptr ) {
                if(patient->treatment == treatment) {
                    show_patient(patient);
                }
            } else {
                break;
            }
        }
        database.close();
    }
}

#endif
