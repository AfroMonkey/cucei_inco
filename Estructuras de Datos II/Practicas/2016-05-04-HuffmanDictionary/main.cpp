#include <iostream>
#include <fstream>
#include <algorithm> /*std::sort*/
#include <list>

#include "huffman_node.h"

int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::cout << "Usage:\n";
        std::cout << argv[0] << " input_file output_file" << "\n";
        return 1;
    }

    std::ifstream input(argv[1]);
    if (!input)
    {
        std::cout << "Cant open the file \"" << argv[1] << "\"" << "\n";
        return 2;
    }
    std::ofstream output(argv[2], std::ios::trunc);
    if (!input)
    {
        std::cout << "Cant open the file \"" << argv[2] << "\"" << "\n";
        return 3;
    }

    std::string content((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
    input.close();
    std::sort(content.begin(), content.end());

    std::list<HuffmanNode*> tree;
    for (int i = 0, j = 1; content[i]; i++, j++)
    {
        if (content[i] != content[i+1])
        {
            HuffmanNode *node = new HuffmanNode(j, content[i]);
            tree.push_back(node);
            j = 0;
        }
    }

    tree.sort(HuffmanNode::cmp);

    while (tree.size() > 1)
    {
        HuffmanNode *node = new HuffmanNode();

        node->left_ = *tree.begin();
        tree.pop_front();
        node->right_ = *tree.begin();
        tree.pop_front();
        node->weight_ = node->left_->weight_ + node->right_->weight_;

        tree.push_front(node);
        tree.sort(HuffmanNode::cmp);
    }

    HuffmanNode* root = *tree.begin();
    root->get_dictionary(output);
    output.close();
    root->free();
    delete root;

    return 0;
}
