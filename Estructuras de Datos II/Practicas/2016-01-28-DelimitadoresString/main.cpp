#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include "pet.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_ADD 1
#define OPC_SHOW 2
#define OPC_MODIFY 3
#define OPC_DELETE 4
#define OPC_SEARCH 5
#define OPC_EXIT 6

#define CHAR_DELIMITER '|'

void add();
void show();
void modify();
void deleteFromFile();
void search();

int main() {
	int opc;

	do {
		cout << OPC_ADD << ") Agregar" << endl;
		cout << OPC_SHOW << ") Mostrar" << endl;
		cout << OPC_MODIFY << ") Modificar" << endl;
		cout << OPC_DELETE << ") Eliminar" << endl;
		cout << OPC_SEARCH << ") Buscar" << endl;
		cout << OPC_EXIT << ") Salir" << endl;
		cout << ">";
		cin >> opc;
		cin.ignore();

		switch(opc) {
			case OPC_ADD: {
				add();
				break;
			}
			case OPC_SHOW: {
				show();
				break;
			}
			case OPC_MODIFY: {
				modify();
				break;
			}
			case OPC_DELETE: {
				deleteFromFile();
				break;
			}
			case OPC_SEARCH: {
				search();
				break;
			}
			case OPC_EXIT: {
				cout << "Cerrando" << endl;
				break;
			}
			default: {
				cout << "Opcion invalida" << endl;
				break;
			}
		}
		cout << "Presione entrar para continuar . . .";
		cin.ignore();
		system(CLEAR);
	} while(opc != OPC_EXIT);

	return 0;
}

Pet* fillPet() {
	Pet *pet = new Pet;

	cout << "Nombre>";
	getline(cin, pet->name);
	cout << "Raza>";
	getline(cin, pet->type);
	cout << "Color>";
	getline(cin, pet->color);
	cout << "Tamanyo>";
	getline(cin, pet->size);

	return pet;
}

Pet* getPet(ifstream &database) {
	Pet *pet = new Pet;
	char buffer[35];
	database.getline(buffer, 35, '|');
	pet->name = buffer;
	database.getline(buffer, 35, '|');
	pet->type = buffer;
	database.getline(buffer, 35, '|');
	pet->color = buffer;
	database.getline(buffer, 35, '|');
	pet->size = buffer;
	if(database.eof()) {
		return nullptr;
	}
	
	return pet;
}

void showPet(Pet &pet) {
	cout << "Nombre:" << pet.name << endl;
	cout << "Raza:" << pet.type << endl;
	cout << "Color:" << pet.color << endl;
	cout << "Tamanyo:" << pet.size << endl;
	cout << endl;
}

void add() {
	ofstream database("Database.txt", ios::app);
	if(database.good()) { 
		Pet *pet = fillPet();
		database << pet->name << CHAR_DELIMITER << pet->type << CHAR_DELIMITER << pet->color << CHAR_DELIMITER << pet->size << CHAR_DELIMITER;
		delete pet;
		database.close();
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void show() {
	ifstream database("Database.txt");
	if(database.good()) {
		Pet *pet;
		while(true) {
			pet = getPet(database);
			if(pet != nullptr) {
				showPet(*pet);
				delete pet;
			} else {
				break;	
			}
		}
		database.close();
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void modify() {
	ifstream original("Database.txt");
	if(original.good()) {
		string name;
		Pet *pet, *newPet;

		cout << "Nombre>";
		getline(cin, name);

		while(true) {
			pet = getPet(original);
			if(pet != nullptr) {
				if(pet->name == name) {
					break;
				}
			} else {
				break;	
			}
		}
		original.close();
		original.open("Database.txt");
		if(pet != nullptr) {
			if(original.good()) {
				showPet(*pet);
				cout << "Ingrese los nuevos datos" << endl;
				newPet = fillPet();

				ofstream temp("Temp.txt", ios::app);
				if(temp.good()) {
					while(true) {
						pet = getPet(original);
						if(pet != nullptr) {
							if(pet->name == name) {
								temp << newPet->name << CHAR_DELIMITER << newPet->type << CHAR_DELIMITER << newPet->color << CHAR_DELIMITER << newPet->size << CHAR_DELIMITER;
							} else {
								temp << pet->name << CHAR_DELIMITER << pet->type << CHAR_DELIMITER << pet->color << CHAR_DELIMITER << pet->size << CHAR_DELIMITER;
							}
						} else {
							break;	
						}
					}
					temp.close();
					original.close();
					remove("Database.txt");
					rename("Temp.txt", "Database.txt");
				} else {
					cout << "Error al abrir el archivo" << endl;
				}
				original.close();
			} else {
				cout << "Error al abrir el archivo" << endl;
			}
		} else {
			cout << "No se econtro la mascota" << endl;
		}
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void deleteFromFile() {
	ifstream original("Database.txt");
	if(original.good()) {
		char name[35];
		Pet *pet, *newPet;

		cout << "Nombre>";
		cin.getline(name, 35);

		while(true) {
			pet = getPet(original);
			if(pet != nullptr) {
				if(pet->name == name) {
					break;
				}
			} else {
				break;	
			}
		}
		original.close();
		original.open("Database.txt");
		if(pet != nullptr) {
			if(original.good()) {
				char opc;
				showPet(*pet);
				cout << "Seguro que desea eliminar esta mascota(y/n)>";
				cin >> opc;
				cin.ignore();
				if(opc == 'y') {
					ofstream temp("Temp.txt", ios::app);
					if(temp.good()) {
						while(true) {
							pet = getPet(original);
							if(pet != nullptr) {
								if(pet->name != name) {
									temp << pet->name << CHAR_DELIMITER << pet->type << CHAR_DELIMITER << pet->color << CHAR_DELIMITER << pet->size << CHAR_DELIMITER;
								}
							} else {
								break;	
							}
						}
						temp.close();
						original.close();
						remove("Database.txt");
						rename("Temp.txt", "Database.txt");
					} else {
						cout << "Error al abrir el archivo" << endl;
					}
				}
				original.close();
			} else {
				cout << "Error al abrir el archivo" << endl;
			}
		} else {
			cout << "No se econtro la mascota" << endl;
		}
	} else {
		cout << "Error al abrir el archivo" << endl;
	}
}

void search() {
	ifstream database("Database.txt");
	if(database.good()) {
		char name[35];
		Pet *pet, *newPet;

		cout << "Nombre>";
		cin.getline(name, 35);

		while(true) {
			pet = getPet(database);
			if(pet != nullptr) {
				if(pet->name == name) {
					break;
				}
			} else {
				break;	
			}
		}
		
		if(pet != nullptr) {
			showPet(*pet);
		} else {
			cout << "No se econtro a la mascota" << endl;
		}
		database.close();
	} else {
		cout << "Error al abrir el archivo" << endl;
	}	
}
