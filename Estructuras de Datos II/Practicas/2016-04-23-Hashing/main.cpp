#include "item.h"
#include "cli.h"
#include "db_controller.h"

void opt_add();
void opt_show();
void opt_modify();
void opt_delete();

int main()
{
    create_database();

    int opt;
    do {
        print_menu();
        opt = get_int();
        switch (opt)
        {
            case OPT_ADD:
                opt_add();
                break;
            case OPT_SHOW:
                opt_show();
                break;
            case OPT_MODIFY:
                opt_modify();
                break;
            case OPT_DELETE:
                opt_delete();
                break;
            case OPT_EXIT:
                break;
            case 5:
                char* name = get_string("Nombre>");
                cout << get_base(name) << endl;
                break;
        }
        pause();
        clear_screen();
    } while (opt != OPT_EXIT);
    return 0;
}

void opt_add()
{
    Item item;
    fill_item(item);
    if (get_item_location(item.name_) == -1)
    {
        if (add_item(item))
        {
            msg(MSG_DONE);
        }
        else
        {
            msg(ERR_FULL);
        }
    }
    else
    {
        msg(ERR_DUPLICATED);
    }
}

void opt_show()
{
    show_items();
}

void opt_modify()
{
    char* name = get_string("Nombre>");
    int location = get_item_location(name);
    if (location != -1)
    {
        print_item(read_item(location));
        if (get_int(0, "Seguro (1/0)?"))
        {
            Item item;
            fill_item(item);
            if (strcmp(item.name_, name) == 0)
            {
                write_item(item, location);
            }
            else
            {
                delete_item(name);
                add_item(item);
            }
        }
    }
    else
    {
        msg(ERR_MISSING);
    }
}

void opt_delete()
{
    char* name = get_string("Nombre>");
    if (delete_item(name))
    {
        msg(MSG_DONE);
    }
    else
    {
        msg(ERR_MISSING);
    }
    delete name;
}
