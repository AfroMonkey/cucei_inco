#ifndef DISPERSSION_H
#define DISPERSSION_H

#include <cmath>
#include <iostream>

#include "item.h"
#include "config.h"

long int get_base(char *name)
{
    int base = 0;

    for (int i = 0; name[i] != '\x0'; i++)
    {
        base += name[i] * pow(26, i);
    }
    base %= ROWS;
    base *= (sizeof(int) + COLUMNS * sizeof(Item));

    return base;
}

#endif
