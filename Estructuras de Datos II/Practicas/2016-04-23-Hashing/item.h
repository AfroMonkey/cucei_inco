#ifndef ITEM_H
#define ITEM_H

#include <cmath>

class Item
{
public:
    char name_[30];
    float price_;
    int quantity_;

    Item();
};

Item::Item()
{
    name_[0] = '\x0';
    price_ = 0;
    quantity_ = 0;
}

#endif
