#ifndef DB_CONTROLLER_H
#define DB_CONTROLLER_H

#include <fstream>
#include <cstring>

#include "config.h"
#include "item.h"
#include "cli.h"
#include "disperssion.h"


void create_database()
{
    {
        std::ifstream database(DB_LOCATION);
        if (database)
            return;
    }
    std::ofstream database(DB_LOCATION, std::ios::app);
    Item item;
    int z = 0;
    for (int i = 0; i < ROWS; i++)
    {
        database.write((char*)&z, sizeof(int));
        for (int j = 0; j < COLUMNS; j++)
        {
            database.write((char*)&item, sizeof(Item));
        }
    }
}

int get_item_location(char *name)
{
    long int base = get_base(name);
    std::ifstream database(DB_LOCATION);
    Item aux;
    long int i;

    database.seekg(base);
    database.read((char*)&i, sizeof(int));
    if (i != 0)
    {
        for (int j = 0; j < i; j++)
        {
            database.read((char*)&aux, sizeof(Item));
            if (database.eof())
            {
                database.close();
                return -1;
            }
            if (strcmp(aux.name_, name) == 0)
            {
                j = database.tellg();
                j -= sizeof(Item);
                database.close();
                return j;
            }
        }
    }
    database.close();
    return -1;
}

Item read_item(int location)
{
    Item item;
    std::ifstream database(DB_LOCATION);

    database.seekg(location);
    database.read((char*)&item, sizeof(Item));

    database.close();

    return item;
}

void write_item(const Item &item, int location)
{
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);

    database.seekp(location);
    database.write((char*)&item, sizeof(Item));

    database.close();
}

bool add_item(Item &item)
{
    long int base = get_base(item.name_);
    std::fstream database(DB_LOCATION, std::ios::in | std::ios::out);
    Item aux;
    int i;

    database.seekp(base);
    database.read((char*)&i, sizeof(int));
    if (i < COLUMNS)
    {
        database.seekp(base + sizeof(int) + i * sizeof(Item));
        i++;
        database.write((char*)&item, sizeof(Item));
        database.seekp(base);
        database.write((char*)&i, sizeof(int));
        database.close();

        return true;
    }
    else
    {

        database.close();
        return false;
    }
}

void show_items()
{
    Item item;
    int j;
    std::ifstream database(DB_LOCATION);

    for (int i = 0; i < ROWS; i++)
    {
        database.seekg(i*(sizeof(Item) * COLUMNS + sizeof(int)));
        for (database.read((char*)&j, sizeof(int)); j > 0; j--)
        {
            database.read((char*)&item, sizeof(Item));
            print_item(item);
        }
    }
}

bool delete_item(char* name)
{
    int location = get_item_location(name);
    if (location != -1)
    {
        fstream database(DB_LOCATION, ios::in | ios::out);
        long long int base = get_base(name);
        location -= base;
        location -= sizeof(int);
        location /= sizeof(Item);
        int i;
        Item aux;

        database.seekg(base);
        database.read((char*)&i, sizeof(int));
        for (int j = location + 1; j < i; j++)
        {
            database.seekg(base + sizeof(int) + j * sizeof(Item));
            database.read((char*)&aux, sizeof(Item));
            database.seekp(base + sizeof(int) + (j-1) * sizeof(Item));
            database.write((char*)&aux, sizeof(Item));
        }
        i--;
        database.seekp(base);
        database.write((char*)&i, sizeof(int));
        database.close();
        return true;
    }
    else
    {
        return false;
    }
}

#endif
