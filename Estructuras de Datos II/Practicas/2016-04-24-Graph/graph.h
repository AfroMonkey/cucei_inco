#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <queue>
#include <stack>

#include "vertex.h"

class Graph
{
public:
    bool directed_;
    bool weighted_;
    std::list<Vertex> vertices_;

    Graph(bool directed, bool weighted) : directed_(directed), weighted_(weighted) {}

    bool add_vertex(const std::string name);
    bool add_vertex(Vertex &vertex);
    bool set_link(Vertex *start, Vertex *end, int weight);
    Vertex* get_vertex(const std::string name);

    void depth_first_search(Vertex *start);
    void breadth_first_search(Vertex *start);
    void depth_first_search(Vertex *start, Vertex *end);
    void breadth_first_search(Vertex *start, Vertex *end);
    void dijkstra(Vertex *start, Vertex *end);
private:
    void unvisit();
};

bool Graph::add_vertex(const std::string name)
{
    for (std::list<Vertex>::iterator i = vertices_.begin(); i != vertices_.end(); i++)
    {
        if (i->name_ == name) return false;
    }
    vertices_.push_back(Vertex(name));
    return true;
}

bool Graph::add_vertex(Vertex &vertex)
{
    vertices_.push_back(vertex);
    return true;
}

bool Graph::set_link(Vertex *start, Vertex *end, int weight)
{
    if (!(start && end)) return false;

    start->set_link(end, weight);
    return true;
}

Vertex* Graph::get_vertex(const std::string name)
{
    for (std::list<Vertex>::iterator i = vertices_.begin(); i != vertices_.end(); i++)
    {
        if (i->name_ == name) return &*i;
    }
    return NULL;
}

void Graph::depth_first_search(Vertex *start)
{
	std::stack<Vertex*> s;
    Vertex *vertex;
    std::list<Link>::iterator i;

    std::cout << start->name_ << " ";
    s.push(start);
    while (!s.empty())
    {
        vertex = s.top();
        vertex->visited_ = true;
        for (i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_) {
                std::cout << i->to_->name_ << " ";
                i->to_->visited_ = true;
                s.push(i->to_);
                break;
            }
        }
        if(i == vertex->links_.end()) {
            s.pop();
        }
    }
    unvisit();
    std::cout << std::endl;
}

void Graph::breadth_first_search(Vertex *start)
{
	std::queue<Vertex*> q;
    Vertex *vertex;

    start->visited_ = true;
    q.push(start);
    while (!q.empty()) {
        vertex = q.front();
        q.pop();
        vertex->visited_ = true;
        std::cout << vertex->name_ << " ";
        for (std::list<Link>::iterator i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_)
            {
                i->to_->visited_ = true;
                q.push(i->to_);
            }
        }
    }
    unvisit();
    std::cout << std::endl;
}

void Graph::depth_first_search(Vertex *start, Vertex *end) {
	std::stack<Vertex*> s;
    std::stack<int> weights;
    Vertex *vertex;
    std::list<Link>::iterator i;

    start->visited_ = true;
    s.push(start);
    while (!s.empty())
    {
        vertex = s.top();
        for (i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_)
            {
                i->to_->visited_ = true;
                s.push(i->to_);
                weights.push(i->weight_);
                break;
            }
        }
        if (i->to_ == end) break;
        if (i == vertex->links_.end())
        {
            s.pop();
            weights.pop();
        }
    }

    if (s.empty()) std::cout << "No hay ruta" << std::endl;
    else {
        std::stack<Vertex*> aux;
        std::stack<int> aux2;
        while (!s.empty())
        {
            aux.push(s.top());
            s.pop();
        }
        s = aux;
        while (!weights.empty())
        {
            aux2.push(weights.top());
            weights.pop();
        }
        weights = aux2;
        Vertex *prev = NULL;
        int weight = 0;
        while (!s.empty())
        {
            vertex = s.top();
            s.pop();
            if(prev) {
                std::cout << "-" << weights.top() << "->";
                weight += weights.top();
                weights.pop();
            }
            std::cout << "(" << vertex->name_ << ")";
            prev = vertex;
        }
        std::cout << std::endl;
        std::cout << "Peso total = " << weight << std::endl;
    }
    unvisit();
}

void Graph::breadth_first_search(Vertex *start, Vertex *end) {
	std::queue<Vertex*> queue;
    Vertex *vertex;
    bool founded = false;
    std::list<Link>::iterator i;
    Graph second_graph(directed_, weighted_);

    queue.push(start);
    while (!queue.empty())
    {
        vertex = queue.front();
        queue.pop();
        vertex->visited_ = true;
        for (i = vertex->links_.begin(); i != vertex->links_.end(); i++)
        {
            if (!i->to_->visited_)
            {
                queue.push(i->to_);
                i->to_->visited_ = true;
                second_graph.add_vertex(*i->to_);
                if (i->to_ == end)
                {
                    founded = true;
                    break;
                }
            }
        }
        if(founded) break;
    }
    unvisit();
    if(founded)
    {
        second_graph.depth_first_search(start, end);
    }
    else std::cout << "No hay ruta" << std::endl;
}

void Graph::dijkstra(Vertex *start, Vertex *end)
{
    std::cout << "nope" << "\n";
}

void Graph::unvisit()
{
    for (std::list<Vertex>::iterator i = vertices_.begin(); i != vertices_.end(); i++)
    {
        i->visited_ = false;
    }
}

#endif
