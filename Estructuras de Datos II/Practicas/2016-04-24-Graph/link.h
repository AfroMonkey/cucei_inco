#ifndef LINK_H
#define LINK_H

class Vertex;

class Link
{
public:
    Vertex *to_;
    int weight_;

    Link(Vertex *to, const int weight = 0) : to_(to), weight_(weight) {}
};

#endif
