    //revisar si es triangulo

    CargaAc '0
    GuardAc 1232    //equilatero
    GuardAc 1249    //isosceles
    GuardAc 1302    //escaleno

    CargaAc 1013
    CmpAcVs 1022
    GuarCmp 1203
    CargaAc 1203
    NOTAc
    GuardAc 1203
    CmpAcVs 1058
    GuarCmp 1217
    CargaAc 1217
    NOTAc
    GuardAc 1217
    CargaAc 1022
    CmpAcVs 1058
    GuarCmp 1223
    CargaAc 1223
    NOTAc
    GuardAc 1223
//tipo de triangulo
    CargaAc 1203
    ANDAc   1217
    ANDAc   1223    //Equilatero
    GuardAc 1232
    CargaAc 1203
    ORAc    1217
    ORAc    1223    //Isosceles
    GuardAc 1249
    CargaAc 1232
    NOTAc
    ANDAc   1249
    GuardAc 1249
    ORAc    1232
    NOTAc           //Escaleno
    GuardAc 1302
