#include <stdio.h>

#define MAX 5

int X, Ac, i, j, aux, aux2;
int arr[MAX];

int main() {
    for(i = 0; i < MAX; i++) {
        scanf("%d", &arr[i]);
    }

    Ac = 0;
    i = Ac;
    do {
        Ac = 0;
        X = 0;
        j = Ac;
        do {
            Ac = arr[X];
            aux = Ac;
            Ac = j;
            Ac += 1;
            j = Ac;
            X = j;
            Ac = arr[X];
            if(Ac < aux) {
                aux2 = arr[X-1];
                arr[X-1] = arr[X];
                arr[X] = aux2;
            } 
            Ac = j;
            Ac += 1;
        } while(Ac < MAX);
        Ac = i;
        Ac += 1;
        i = Ac;
    } while(Ac < MAX);

    for(i = 0; i < MAX; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}