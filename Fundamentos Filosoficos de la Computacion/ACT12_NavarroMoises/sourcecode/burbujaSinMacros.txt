/*Main*/
//inicio->1000
//tam->2000
//aux->3000
//i->500
//j->600
//auxI1->400
//auxI2->300
    Lee 2000

    CargaX '0
    CargaAc '0
cicloLeerVector:
    GuardaAc 3000
    CargaX 3000
    Lee #1000
    SumaAc '1
    CmpAcVs 2000
    SaltaMen cicloLeerVector

    CargaAc '0
    GuardaAc 500
cicloB1:
    CargaAc '0
    CargaX '0
    GuardaAc 600
cicloB2:
    CargaAc #1000
    GuardaAc 3000
    CargaAc 600
    SumaAc '1
    GuardaAc 600
    CargaX 600
    CargaAc #1000
    CmpAcVs 3000
    SaltaMen hacerIntercambio
    Salta cicloB2Comparar
hacerIntercambio:
    CargaAc '1000
    SumaAc 600
    GuardaAc 400
    RestaAc'1
    GuardaAc 300
    intercambiar(400, 300, 3000)
cicloB2Comparar:
    CargaAc 600
    SumaAc '1
    CmpAcVs 2000
    SaltaMen cicloB2
cicloB1Comparar:
    CargaAc 500
    CmpAcVs 2000
    SumaAc '1
    GuardaAc 500
    SaltaMen cicloB1

    CargaX '0
    CargaAc '0
cicloEscribirVector:
    GuardaAc 3000
    CargaX 3000
    Escribe#1000
    SumaAc '1
    CmpAcVs 2000
    SaltaMen cicloEscribirVector

    Alto
/*FinMain*/