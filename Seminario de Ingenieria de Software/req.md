# Funcionales
* Administrar asistencias.
    * Establecer el tipo de asistencia (asistencia, falta, retardo, etc) de un día para un alumno en una sección.
* Administrar calificaciones.
    * Establecer una calificación de algún evaluando (actividad, examen, tarea, etc) para un alumno en alguna sección.
* Administrar Sección.
    * Establecer la ponderación de los evaluandos.
    * Establecer los horarios de la sección.
    * Establecer que alumnos pertenecen a la sección.
    * Establecer los temas de la materia.
        * Indicar el método para impartir el tema.
        * Indicar las herramientas, medios e instrumentos para el tema.
    * Establecer la lista de actividades de la sección.
* Obtener estadísticas.
    * Mostrar estadísticas grupales para las asistencias.
    * Mostrar estadísticas individuales para las asistencias.
* Generar reportes.
    * Generar un formato estándar para la entrega de reportes al final de semestre (ver documento ZXC).
* Mostrar estatus del estudiante.
    * Indicar si tiene derecho a ordinario.
    * Indicar si no tiene derecho a ordinario, por faltas.

# No Funcionales
* Ser ejecutable desde un iPad Pro
* Ser no dependiente de conexión a Internet
