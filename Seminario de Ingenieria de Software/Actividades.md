## Lista de actividades

Actividad |  TO  | TMP  |  TP  | TE
:--       | :--: | :--: | :--: | :--:
Obtener requerimientos | 15 | 21 | 30 | 21.5
Elaborar SRS | 10 | 14 | 18 | 14
Establecer arquitectura | 5 | 8 | 14 | 8.5
Planear la GUI | 1 | 2 | 3 | 2
Presentar GUI al usuario | 0.5 | 1 | 2 | 1
Mejorar GUI | 1 | 2 | 3 | 2
Probar GUI en iPad | 0.5 | 1 | 3 | 1.25
Planear DB | 2 | 3 | 5 | 3
Generar DB | 0.5 | 1 | 3 | 1.25
Establecer conexión entre aplicación y DB | 0.5 | 1 | 4.5 | 1.5
Codificar módulo puente entre aplicación y DB | 2 | 3 | 5 | 3
Codificar módulo para ABC de secciones | 2 | 3 | 5 | 3
Codificar módulo para ABC de alumnos | 2 | 3 | 5 | 3
Codificar módulo para registrar asistencias | 1 | 2 | 3 | 2
Codificar módulo para registrar evaluaciones | 1 | 2 | 3 | 2
Codificar módulo de reportes | 5 | 7 | 10 | 7
Codificar módulo de estadísticas | 2 | 3 | 5 | 3
Codificar módulo para generar respaldos | 0.5 | 1 | 3 | 1.25
Pruebas a nivel módulo | 2 | 3 | 5 | 3
Probar prototipo en iPad | 3 | 5 | 7 | 5

* TO = Tiempo optimista
* TMP = Tiempo más probable
* TP = Tiempo pesimista
* TE = Tiempo esperado
