## Lista de actividades

Clave | Actividad | Predecesora(s) | Duración esperada (días)
:--: | :-- | :-- | :--:
A | Obtener requerimientos | N/A | 21.5
B | Elaborar SRS | A | 14
C | Establecer arquitectura | B | 8.5
D | Planear la GUI | A | 2
E | Presentar GUI al usuario | D | 1
F | Mejorar GUI | E | 2
G | Probar GUI en iPad | F | 1.25
H | Planear DB | C | 3
I | Generar DB | H | 1.25
J | Establecer conexión entre aplicación y DB | I | 1.5
K | Codificar módulo puente entre aplicación y DB | J | 3
L | Codificar módulo para ABC de secciones | K | 3
M | Codificar módulo para ABC de alumnos | L | 3
N | Codificar módulo para registrar asistencias | M | 2
Ñ | Codificar módulo para registrar evaluaciones | M | 2
O | Codificar módulo de reportes | N, Ñ | 7
P | Codificar módulo de estadísticas | N, Ñ | 3
Q | Codificar módulo para generar respaldos | N, Ñ | 1.25
R | Pruebas a nivel módulo | O, P, Q | 3
S | Probar prototipo en iPad | G, R | 5
