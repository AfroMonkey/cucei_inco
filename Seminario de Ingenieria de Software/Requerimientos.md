# Funcionales

*   Administración de materias
	*   El sistema debe ser capaz de dar de alta materias, registrando su nombre y su clave.
	*   El sistema permitirá modificar los datos de una materia registrada.
	*   El sistema permitirá eliminar una materia registrada.
	*   El sistema deberá ser capaz de registrar secciones dentro de una materia.

*   Administración de secciones
	*   Cada sección debe ser registrada con su identificador, NRC, calendario, horario, fecha de inicio y fecha de fin.
	*   El sistema identificará a cada sección en base a su calendario, identificador y a la materia a la que está asignada.
	*   El sistema permitirá la elección de los formatos de horario más comunes mediante opciones predeterminadas:
    	*   Lunes y Miércoles
    	*   Martes y Jueves
    	*   Sólo los viernes
    	*   Sólo los sábados
    	*   Personalizada
	*   El sistema permitirá modificar los datos de una sección.
	*   El sistema permitirá eliminar una sección.

*   Gestión de fechas y temas
	*   El sistema permitirá registrar el tema visto en cada día de clase, especificando el método e instrumento utilizados.
	*   El sistema deberá registrar una lista de los días de hábiles de clases que tendrá el semestre en curso, indicando también vacaciones y asuetos.

*   Administración y estadísticas de asistencias
	*   El sistema permitirá tomar y registrar la asistencia (asistencia, falta, retardo, etc).
	*   El sistema deberá ser capaz de calcular el porcentaje de asistencia general por día.
	*   El sistema deberá ser capaz de calcular el porcentaje de asistencia general por semestre.
	*   El sistema deberá ser capaz de calcular el porcentaje de asistencia acumulado por cada alumno.
	*   El sistema alertará al profesor cuando un alumno esté a <<2>> faltas de perder calificación ordinaria.
	*   El sistema indicará cuando un alumno haya pérdido derecho a calificación ordinaria por faltas.
	*   El sistema indicará cuando un alumno haya pérdido derecho a calificación extraordinaria por faltas.

*   Administración de evaluandos
	*   El sistema permitirá definir las rúbricas de evaluación para cada sección, asignándoles un nombre y un porcentaje del puntaje final de la calificación.
	*   El sistema permitirá la creación de evaluandos dentro de cada sección, el evaluando debe tener un nombre, un tema, fecha de entrega y pertecener a una de las rúbricas de evaluación.
	*   El sistema permitirá modificar las rúbricas de evaluación, al cambiar la rúbrica, los evaluandos que pertenezcan a dicha rúbrica serán actualizados también.
	*   El sistema deberá ser capaz de modificar los evaluandos ya registrados, en caso de que haya existido un error durante su captura.

*   Administración y evaluación de alumnos
	*   El sistema permitirá asignar alumnos a una sección, registrando su código y su nombre.
	*   El sistema permitirá importar una lista de alumnos a una sección utilizando un archivo CSV.
	*   El sistema deberá ser capaz de cambiar de sección a un alumno.	
	*   El sistema deberá ser capaz de gestionar el registro de los evaluandos de cada alumno; registrando su nombre y su calificación.
	*   El sistema deberá ser capaz de calcular automáticamente la calificación ordinaria de cada alumno cuando éste cuente con un mínimo de 80% de asistencias.

*   Generar reportes
	*   El sistema deberá ser capaz de generar un reporte con las estadísticas de asistencia por sección.
	*   El sistema deberá ser capaz de generar un reporte con la evaluación de los alumnos en una sección.
	*   El sistema deberá ser capaz de generar un reporte con los temas vistos durante el curso.

# No Funcionales

*   El sistema podrá ser ejecutado desde un iPad Pro.
*   El sistema no debe depender de conexión a Internet.
*   El sistema contará con un diseño basado en los colores institucionales de UdG.
*   El formato del reporte de asistencia por sección debe ser el que se utiliza en el documento <<doc>> que se encuentra en el apéndice de este documento.
*   El formato del reporte de evaluación por sección debe ser el que se utiliza en el documento <<doc>> que se encuentra en el apéndice de este documento.
*   El formato del reporte de la planificación del curso debe ser el que se utiliza en el documento <<doc>> que se encuentra en el apéndice de este documento.

# ¿Dónde van? :V

*   El sistema será ejecutado por un solo usuario
*   El cálculo de porcentajes de asistencia se debe calcular en base a los días hábiles del curso. (ya que anteriormente se menciona que también se deben indicar los asuetos y vacaciones).
*   El sistema deberá ser capaz de generar respaldos cuando el usuario lo desee. (Esto estaba en una de las imágenes de todos los docs que nos envió pero no sé si es necesaria).

# Requisitos que pusimos en el otro doc y en este no (?)

*   Escribir en la Base de Datos rápidamente.
*   El usuario no desea autenticarse en el sistema. (aunque supongo que es lo mismo del solo usuario de ¿Dónde van?).
*   Asegurar el buen funcionamiento del sistema.
*   Uso de licencias
*   Futuras ampliaciones para el software.