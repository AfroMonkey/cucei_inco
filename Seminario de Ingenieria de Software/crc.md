# CRC

## Materia
| Responsabilidades | Colaboradores |
| ----------------- | ------------- |
| Administrar la inforamción de las materias | Seccion: Contener secciones |

## Seccion
| Responsabilidades | Colaboradores |
| ----------------- | ------------- |
| Administrar la información de las secciones | Alumno: Contener alumnos |
| Gestionar los temas por sección | RubricaEvaluacion: Contiene rubricas para sus evaluandos |
| | Alumno: Las secciones contienen un conjunto de alumnos |

## RubricaEvaluacion
| Responsabilidades | Colaboradores |
| ----------------- | ------------- |
| Establecer el valor por tipo de evaluando | Seccion: Cada rubrica pertenece a una sección |

## Asistencia
| Responsabilidades | Colaboradores |
| ----------------- | ------------- |
| Indica la presencia o ausencia de un alumno en una secion un dia en especifico | Seccion y Alumno: Relaciona un dia y un alumno con una seccion para indicar el tipo de asistencia |

## Evaluando
| Responsabilidades | Colaboradores |
| ----------------- | ------------- |
| Administrar el puntaje de un evaluando para un alumno en una seccion | Alumno: Cada evaluando le pertenece a un alumno |
| | Seccion: Cada evaluando pertenece a una seccion |

## Alumno
| Responsabilidades | Colaboradores |
| ----------------- | ------------- |
| Administra la información de un alumno | Asistencia: Posee una asistencia de un dia en una sección. |
| | Evaluando: Entrega un evaluando y recibe una calificacion en el mismo |
