% from rad/s to RPM
K_RDPS2RPM = 60 / ( 2 * pi );
% from RPM to rad/s
K_RPM2RDPS = 1 / K_RDPS2RPM;
% from radians to degrees
K_R2D = 180 / pi;
% from degrees to radians
K_D2R = 1 / K_R2D;
% from Inch to Meter
K_IN2M = 0.0254;
% from Meter to Inch
K_M2IN = 1 / K_IN2M;
% from rad/s to RPM
K_RADPS2RPM = 60 / ( 2 * pi );
% from RPM to rad/s
K_RPM2RADPS = 1 / K_RADPS2RPM;
% from oz-force to N
K_OZ2N = 0.2780139;
% from N to oz-force
K_N2OZ = 1 / K_OZ2N;
% Motor Armature Resistance (Ohm)
Rm = 2.6;
% Motor Armature Inductance (H)
Lm = 180e-6;
% Motor Torque Constant (N.m/A)
Kt = 1.088 * K_OZ2N * K_IN2M; % = .00767
% Motor ElectroMechanical Efficiency [ = Tm * w / ( Vm * Im ) ]
Eff_m = 1;
% Motor Back-EMF Constant (V.s/rad)
Km = 0.804e-3 * K_RDPS2RPM; % = .00767
% Rotor Inertia (kg.m^2)
Jm = 5.523e-5 * K_OZ2N * K_IN2M; % = 3.9e-7
% IP02 Cart Mass, with 3 cable connectors (kg)
Mc2 = 0.57;
% Cart Weight Mass (kg)
Mw = 0.37;
% Planetary Gearbox (a.k.a. Internal) Gear Ratio
Kg = 3.71;
% Planetary Gearbox Efficiency
Eff_g = 1;
% Cart Motor Pinion number of teeth
N_mp = 24;
% Motor Pinion Radius (m)
r_mp = 0.5 / 2 * K_IN2M;  %  = 6.35e-3
% Cart Position Pinion number of teeth
N_pp = 56;
% Position Pinion Radius (m)
r_pp = 1.167 /2 * K_IN2M; %  = 14.8e-3
% Rack Pitch (m/teeth)
Pr = 1e-2 / 6.01; % = 0.0017
% Cart Travel (m)
Tc = 0.814;
%
K_EC = Pr * N_pp / ( 4 * 1024 );
% K_EP is positive, since CCW is the positive sense of rotation
K_EP = 2 * pi / ( 4 * 1024 );

K_PC = 0; K_PP = 0;

% Pendulum Mass (with T-fitting)
Mp = 0.127;
% Pendulum Full Length (with T-fitting, from axis of rotation to tip)
Lp = ( 13 + 1 / 4 ) * K_IN2M;  % = 0.3365
% Distance from Pivot to Centre Of Gravity
lp = 7 * K_IN2M;  % = 0.1778
% Pendulum Moment of Inertia (kg.m^2) - approximation
Ip = Mp * Lp^2 / 12;  % = 1.1987 e-3
% Equivalent Viscous Damping Coefficient (N.m.s/rad)
Bp = 0.0024;

Mc=1.0731;

Beq = 5.4;

g=9.81;

% 
z = (Mc+Mp)*Ip+Mc*Mp*(lp)^2;
A = [0, 1, 0, 0; ((Mp^2)*(lp^2))/z, - (Mp*lp*Bp)/z, 0, ((Ip+Mp*lp^2)*Beq)/z; 0, 0, 0, 1; ((Mc+Mp)*(Mp*g*lp))/z, - ((Mc+Mp)*Bp)/z, 0, - (Mp*lp*Beq)/z];
B = [0; (Ip+Mp*lp^2)/z; 0; (Mc*lp)/z];
C = eye(4);
D = [0; 0; 0; 0];
Q = eye(4) * 50;
R = .01;
k = lqr(A, B, Q, R);
alpha_0 = K_D2R * 5;


