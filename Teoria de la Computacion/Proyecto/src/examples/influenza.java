// cellular automaton size
int cols = 100;
int rows = 60;
int cellSize = 10;

int neighborhoodRadius = 1;
int neighborhoodSize = (int) pow((2 * neighborhoodRadius + 1), 2) - 1;

boolean sirModel = true;

int t = 0;
int generationsPerSecond = 50;
int stopAtT = -1;

// initial state probabilities
float initInfected = 0.00;
float initDead = 0.00;

// poblational and virus parameters 
float birthRate = 0.00;
float deathRate = 0.00;

float contactInfectionProb = 0.06;
float virusMorbidity = 0.008;
float spontaneousInfectionProb = 0.0001;

float recoveryProb = 0.2;
float incrementRecoveryProb = 0.001;
float reSusceptibleProb = 0.1;
float incrementReSusceptibleProb = 0.001;