// cellular automaton size
int cols = 100;
int rows = 60;
int cellSize = 10;

int neighborhoodRadius = 1;
int neighborhoodSize = (int) pow((2 * neighborhoodRadius + 1), 2) - 1;

boolean sirModel = false;

int t = 0;
int generationsPerSecond = 30;
int stopAtT = -1;

// initial state probabilities
float initInfected = 0.00;
float initDead = 0.00;

// poblational and virus parameters 
float birthRate = 0.02;
float deathRate = 0.008;

float contactInfectionProb = 0.1;
float virusMorbidity = 0.0001;
float spontaneousInfectionProb = 0.0001;

float recoveryProb = 0.6;
float incrementRecoveryProb = 0.00;
float reSusceptibleProb = 0.00;
float incrementReSusceptibleProb = 0.00;