import de.bezier.math.combinatorics.*;

// cellular automaton size
int cols = 100;
int rows = 60;
int cellSize = 10;

int neighborhoodRadius = 1;
int neighborhoodSize = (int) pow((2 * neighborhoodRadius + 1), 2) - 1;

boolean sirModel = true;

int t = 0;
int generationsPerSecond = 10;
int stopAtT = -1;

// initial state probabilities
float initInfected = 0.00;
float initDead = 0.00;

// poblational and virus parameters 
float birthRate = 0.00;
float deathRate = 0.00;

float contactInfectionProb = 0.15;
float virusMorbidity = 0.005;
float spontaneousInfectionProb = 0.00;

float recoveryProb = 0.1;
float incrementRecoveryProb = 0.001;
float reSusceptibleProb = 0.00005;
float incrementReSusceptibleProb = 0.00;

// posible states of a cell
enum State
{
  SUSCEPTIBLE, INFECTED, RECOVERED, DEAD, NULL;
}

class Cell
{
  State state;
  int updated;
}

// cellular automaton as a bidimensional array of cells
Cell[][] cells;
State[] neighborhood;

boolean loop = false;
State mouseState = State.INFECTED;

void settings()
{
  //+ 1 to compensate for the borderline
  size(cols * cellSize + 1, rows * cellSize + 1);
}

void setup()
{
  frameRate(generationsPerSecond);
  cells = new Cell[cols][rows];
  initCells();
  drawGeneration();
}

void draw()
{
  //randomSeed(0);
  if (loop)
  {
    println("t = " + t);
    ++t;
    update();
    drawGeneration();
  }
  if (t == stopAtT)
  {
    noLoop();
  }
}

void initCells()
{
  for (int i = 0; i < cols; ++i)
  {
    for (int j = 0; j < rows; ++j)
    {
      cells[i][j] = new Cell();
      if (random(0, 1) < initDead)
      {
        cells[i][j].state = State.DEAD;
      } else if (random(0, 1) < initInfected)
      {
        cells[i][j].state = State.INFECTED;
      } else
      {
        cells[i][j].state = State.SUSCEPTIBLE;
      }
    }
  }
}

void update()
{
  Cell[][] newGen;
  newGen = new Cell[cols][rows];
  for (int i = 0; i < cols; ++i)
  {
    for (int j = 0; j < rows; ++j)
    {
      newGen[i][j] = new Cell();
      newGen[i][j].state = computeNextState(i, j);
    }
  }
  for (int i = 0; i < cols; ++i)
  {
    for (int j = 0; j < rows; ++j)
    {
      if (cells[i][j].state != newGen[i][j].state)
      {
        cells[i][j].state = newGen[i][j].state;
        cells[i][j].updated = t;
      }
    }
  }
}

State[] getNeighborhood(int i, int j)
{
  int idx = 0;
  State[] neighborhood = new State[neighborhoodSize];
  for (int k = i - neighborhoodRadius; k <= i + neighborhoodRadius; ++k)
  {
    for (int l = j - neighborhoodRadius; l <= j + neighborhoodRadius; ++l)
    {
      if (k != i || l != j)
      {
        neighborhood[idx] = validCell(k, l) ? cells[k][l].state : State.NULL;
        ++idx;
      }
    }
  }
  return neighborhood;
}

State computeNextState(int i, int j)
{
  // null cells can't change its state
  if (cells[i][j].state == State.NULL) return State.NULL;

  int alive, dead = 0, infected = 0, recovered = 0, susceptible = 0;
  State[] neighborhood = getNeighborhood(i, j);
  for (int k = 0; k < neighborhoodSize; ++k)
  {
    switch(neighborhood[k])
    {
    case DEAD:
      ++dead;
      break;
    case SUSCEPTIBLE:
      ++susceptible;
      break;
    case INFECTED:
      ++infected;
      break;
    case RECOVERED:
      ++recovered;
    default:
    }
  }
  alive = susceptible + infected + recovered;

  // current state is dead. Posible next state: dead, susceptible.
  if (cells[i][j].state == State.DEAD)
  {
    if (alive >= 2)
    {
      Combination comb = new Combination(alive, 2);
      if (random(0, 1) < binomial(comb.totalAsInt(), 1, birthRate))
      {
        return State.SUSCEPTIBLE;
      }
    }
  } else
  {
    // cell is alive, it may be dead in next generation
    if (random(0, 1) < deathRate)
    {
      return State.DEAD;
    }
    // cell is susceptible, it may get infected
    if (cells[i][j].state == State.SUSCEPTIBLE)
    {
      if (infected >= 1)
      {
        if (random(0, 1) < binomial(infected, 1, contactInfectionProb))
        {
          return State.INFECTED;
        }
      }
      if (random(0, 1) < spontaneousInfectionProb)
      {
        return State.INFECTED;
      }
    }
    // cell is infected, it may recover or die due to the infection
    else if (cells[i][j].state == State.INFECTED)
    {
      if (random(0, 1) < virusMorbidity)
      {
        return State.DEAD;
      }
      if (random(0, 1) < (recoveryProb + (t - cells[i][j].updated) * incrementRecoveryProb))
      {
        return sirModel ? State.RECOVERED : State.SUSCEPTIBLE;
      }
    }
    
    else if (cells[i][j].state == State.RECOVERED)
    {
      if (random(0, 1) < (reSusceptibleProb + (t - cells[i][j].updated) * incrementReSusceptibleProb))
      {
        return State.SUSCEPTIBLE;
      }
    }
  }

  return cells[i][j].state;
}

void mutateVirus()
{
  for (int i = 0; i < cols; ++i)
  {
    for (int j = 0; j < rows; ++j)
    {
      if (cells[i][j].state == State.RECOVERED)
      {
        cells[i][j].state = State.SUSCEPTIBLE;
        cells[i][j].updated = t;
      }
    }
  }
  drawGeneration();
}

// Miscellaneous
boolean validCell(int i, int j)
{
  if (i < 0 || i >= cols) return false;
  if (j < 0 || j >= rows) return false;
  return true;
}

float binomial(int n, int x, float p)
{
  float prob = 0;
  if (n < 0 || n < x) return -1;
  for (int i = x; i <= n; ++i)
  {
    Combination comb = new Combination(n, i);
    prob += comb.totalAsInt() * pow(p, i) * pow(1 - p, n - i);
  }
  return prob;
}

// Input handling
void keyPressed()
{
  if (sirModel && (key == 'm' || key == 'M'))
  {
    mutateVirus();
  }

  if (key == ' ')
  {
    loop = !loop;
  }
  
  if (key == 'i' || key == 'I')
  {
    mouseState = State.INFECTED;
  }
  
  if (key == 'n' || key == 'N')
  {
    mouseState = State.NULL;
  }
}

void mousePressed()
{
  int i = mouseX / cellSize;
  int j = mouseY / cellSize;
  if (validCell(i, j))
  {
    cells[i][j].state = mouseState;
    cells[i][j].updated = t;
    drawCell(i, j);
  }
}

void mouseDragged()
{
  int i = mouseX / cellSize;
  int j = mouseY / cellSize;
  if (validCell(i, j))
  {
    cells[i][j].state = mouseState;
    cells[i][j].updated = t;
    drawCell(i, j);
  }
}

// UI methods
void drawGeneration()
{
  for (int i = 0; i < cols; ++i)
  {
    for (int j = 0; j < rows; ++j)
    {
      drawCell(i, j);
    }
  }
}

void drawCell(int i, int j)
{
  stroke(50);
  fill(getColor(cells[i][j].state));
  rect(i * cellSize, j * cellSize, cellSize, cellSize);
}

int getColor(State state)
{
  switch (state)
  {
  case SUSCEPTIBLE:
    return #005fb4;
  case INFECTED:
    return #ff0000;
  case RECOVERED:
    return #7800b4;//#006666;
  case DEAD:
    return #FFFFFF;
  default: // NULL
    return #C0C0C0;
  }
}
