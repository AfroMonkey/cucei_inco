/*
 * Author: Navarro Presas Moisés Alejandro
 * Number of practice: AA2
 * Name of practice: Registros con Arreglos, Arreglos de Registros y Arreglo de Objetos
 * Date: 2015/09/02
 * Version 1
 */
#include <iostream>
#include <cstdio>
#include "product.h"

#define PRODUCTS_MAX 500

using namespace std;

void addProduct(Product products[]);
void listProducts(Product products[]);
void updateStock(Product products[]);

int main() {
	int opc;
	Product products[PRODUCTS_MAX];
	for(int i = 0; i < PRODUCTS_MAX; i++) {
		products[i].clear();
	}
	do {
		cout<<"1) Add product"<<endl;
		cout<<"2) List products"<<endl;
		cout<<"3) Update stock"<<endl;
		cout<<"4) Exit"<<endl;
		cin>>opc;

		switch(opc) {
			case 1: {
				addProduct(products);
				cout<<"Press enter to continue ..."<<endl;
				getchar();
				getchar();
				break;
			}
			case 2: {
				listProducts(products);
				cout<<"Press enter to continue ..."<<endl;
				getchar();
				getchar();
				break;
			}
			case 3: {
				updateStock(products);
				cout<<"Press enter to continue ..."<<endl;
				getchar();
				getchar();
				break;
			}
			case 4: {
				//TODO
				break;
			}
			default: {
				cout<<"Invalid option"<<endl;
			}
		}
	}while(opc != 4);	
	return 0;
}

void addProduct(Product products[]) {
	string barcode;
	cout<<"Barcode >";
	cin>>barcode;
	int i;
	for(i = 0; products[i].isFilled() && i != PRODUCTS_MAX; i++);
	if(i != PRODUCTS_MAX) {
		products[i].fill(barcode);
	} else {
		cout<<"Not enough space"<<endl;
	}
}

void listProducts(Product products[]) {
	for(int i = 0; i < PRODUCTS_MAX; i++) {
		if(products[i].isFilled()) {
			products[i].print();
			cout<<"--------"<<endl;
		}
	}
}

void updateStock(Product products[]) {
	string barcode;
	cout<<"Barcode >";
	cin>>barcode;
	int i;
	for(i = 0; i < PRODUCTS_MAX; i++) {
		if(products[i].isFilled() && (products[i].getBarcode() == barcode)) {
			products[i].print();
			int quantity;
			cout<<"Quantity (+/-) >";
			cin>>quantity;
			if(products[i].updateStock(quantity)) {
				cout<<"New stock: "<<products[i].getStock()<<endl;
			} else {
				cout<<"Not enough inventory"<<endl;
			}
			break;
		}
	}
	if(i == PRODUCTS_MAX) {
		cout<<"Product not found"<<endl;
	}
}