#include "date.h"
#include <sstream>

void Date::setYear(short int year) {
	this->year = year;
}

short int Date::getYear() {
	return year;
}

void Date::setMonth(short int month) {
	this->month = month;
}

short int Date::getMonth() {
	return month;
}

void Date::setDay(short int day) {
	this->day = day;
}

short int Date::getDay() {
	return day;
}

void Date::fill() {
	cout<<"Year >";
	cin>>year;

	cout<<"Month >";
	cin>>month;

	cout<<"Day >";
	cin>>day;
}

string Date::toString() {
	string aux;
	ostringstream tempY, tempM, tempD;
	tempY<<year;
	aux = tempY.str();
	tempM<<month;
	aux += "/" + tempM.str();
	tempD<<day;
	aux += "/" + tempD.str();
	return aux;
}