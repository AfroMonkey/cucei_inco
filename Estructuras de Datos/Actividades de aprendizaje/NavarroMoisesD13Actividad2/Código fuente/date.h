#include <iostream>

using namespace std;

class Date {
private:
	short int year;
	short int month;
	short int day;
public:
	void setYear(short int year);
	short int getYear();
	void setMonth(short int month);
	short int getMonth();
	void setDay(short int day);
	short int getDay();
	void fill();
	string toString();
};