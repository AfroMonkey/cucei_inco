#include <iostream>
#include "date.h"

using namespace std;

class Product {
private:
	string barcode_;
	string name_;
	string weight_;
	Date entrance_;
	float wholesalePrice_;
	float retailPrice_;
	unsigned short int stock_;
	bool isFilled_;
public:
	string getBarcode();
	bool isFilled();
	void clear();
	void fill(string barcode);
	void print();
	bool updateStock(int quantity);
	unsigned short int getStock();
};