#include "product.h"

string Product::getBarcode() {
	return barcode_;
}

void Product::fill(string barcode) {
	barcode_ = barcode;
	cout<<"Name >";
	cin>>name_;
	cout<<"Weight >";
	cin>>weight_;
	cout<<"Entrance >"<<endl;
	entrance_.fill();
	cout<<"wholesale price >";
	cin>>wholesalePrice_;
	cout<<"Retail price >";
	cin>>retailPrice_;
	cout<<"Stock >";
	cin>>stock_;
	isFilled_ = true;
}

void Product::clear() {
	isFilled_ = false;
}

bool Product::isFilled() {
	return isFilled_;
}

void Product::print() {
	cout<<"Barcode: "<<barcode_<<endl;
	cout<<"Name: "<<name_<<endl;
	cout<<"Weight: "<<weight_<<endl;
	cout<<"Entrance: "<<entrance_.toString()<<endl;
	cout<<"Wholesale price: "<<wholesalePrice_<<endl;
	cout<<"Retail Price: "<<retailPrice_<<endl;
	cout<<"Stock: "<<stock_<<endl;
}

bool Product::updateStock(int quantity) {
	if(stock_ + quantity < 0) {
		return false;
}
	else {
		stock_ += quantity;
		return true;
	}
}

unsigned short int Product::getStock() {
	return stock_;
}