#include <iostream>

using namespace std;

class Name {
private:
	string lastName1;
	string lastName2;
	string names;
public:
	void setLastName1(string lastName1);
	string getLastName1();
	void setLastName2(string lastName2);
	string getLastName2();
	void setNames(string names);
	string getNames();
	void fill();
	string toString();
};