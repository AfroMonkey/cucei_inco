#include "student.h"

string Student::getName() {
	return name.getLastName1() + " " + name.getLastName2() + " " + name.getNames();
}

void Student::setBirthdate(short int year, short int month, short int day) {
	birthdate.setYear(year);
	birthdate.setMonth(month);
	birthdate.setDay(day);
}

string Student::getBirthdate() {
	return birthdate.toString();
}

void Student::setTel(long int tel) {
	this->tel = tel;
}

long int Student::getTel() {
	return tel;
}

void Student::fill() {
	cout<<"Name ---------"<<endl;
	name.fill();
	cout<<"Birthdate ----"<<endl;
	birthdate.fill();
	cout<<"Address ------"<<endl;
	cout<<"Address >";
	cin>>address;
	cout<<"Tel ----------"<<endl;
	cout<<"Tel >";
	cin>>tel;
}

void Student::print() {
	cout<<"Student ------"<<endl;
	cout<<"Name: "<<name.toString()<<endl;
	cout<<"Birthdate: "<<birthdate.toString()<<endl;
	cout<<"Address: "<<address<<endl;
	cout<<"Tel: "<<tel<<endl;
}
