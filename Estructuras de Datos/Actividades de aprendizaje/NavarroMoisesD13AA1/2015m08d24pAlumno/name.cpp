#include "name.h"

void Name::setLastName1(string lastName1) {
	this->lastName1 = lastName1;
}

string Name::getLastName1() {
	return lastName1;
}

void Name::setLastName2(string lastName2) {
	this->lastName2 = lastName2;
}

string Name::getLastName2() {
	return lastName2;
}

void Name::setNames(string names) {
	this->names = names;
}

string Name::getNames() {
	return names;
}

string Name::toString() {
	return lastName1 + " " + lastName2 + " " + names;
}

void Name::fill() {
	cout<<"Last Name 1 >";
	cin>>lastName1;


	cout<<"Last Name 2 >";
	cin>>lastName2;

	cout<<"Name >";
	cin>>names;
}
