#include <iostream>
#include "date.h"
#include "name.h"

using namespace std;

class Student {
private:
	Name name;
	Date birthdate;
	string address;
	long int tel;
public:
	void setName(string lastName1, string lastName2, string names);
	string getName();
	void setBirthdate(short int year, short int month, short int day);
	string getBirthdate();
	void setTel(long int tel);
	long int getTel();
	void fill();
	void print();
};