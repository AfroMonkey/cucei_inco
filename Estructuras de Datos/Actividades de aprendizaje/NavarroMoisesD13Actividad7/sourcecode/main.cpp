#include <iostream>
#include <cstdlib>
#include "staticList.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define MAX 1000

int comparation(int a, int b);
void printList(StaticList<int> list);

int main() {
    int numElements, print, opc;

    StaticList<int> *originalList;
    StaticList<int> copyList;

    cout << "**Metodos de ordenamiento**" << endl;
    cout << "Numero de elementos >";
    cin >> numElements;
    cout << "Impresion de elementos (1/0)>";
    cin >> print;
    system(CLEAR);

    originalList = new StaticList<int>(numElements + 5);

    cout << "1) Generar lista aleatoria" << endl;
    cout << "2) Generar lista ascendente" << endl;
    cout << "3) Generar lista descendente" << endl;
    cout << "4) Ingresar lista" << endl;
    cout << ">";
    cin >> opc;
    switch(opc) {
        case 1: {
            for(int i = 0;  i < numElements; i++) {
                originalList->insert(originalList->last() + 1, rand() % MAX);
            }
            break;
        }
        case 2: {
            for(int i = 0;  i < numElements; i++) {
                originalList->insert(originalList->last() + 1, i);
            }
            break;
        }
        case 3: {
            for(int i = 0;  i < numElements; i++) {
                originalList->insert(originalList->last() + 1, numElements - i - 1);
            }
            break;
        }
        case 4: {
            int e;
            for(int i = 0;  i < numElements; i++) {
                cout << "Elemento numero " << i + 1 << ">";
                cin >> e;
                originalList->insert(originalList->last() + 1, e);
            }
            break;
        }
        default: {
            cout << "Opcion invalida" << endl;
            exit(0);
            break;
        }
    }
    system(CLEAR);

    do {
        copyList = *originalList;

        if(print) {
            cout << "Lista original: " << endl;
            printList(copyList);
        }

        copyList = *originalList;

        cout << "1) Burbuja" << endl;
        cout << "2) Burbuja mejorado" << endl;
        cout << "3) Insercion" << endl;
        cout << "4) Seleccion" << endl;
        cout << "5) Quick" << endl;
        cout << "6) Todos" << endl;
        cout << "0) Salir" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();
        switch(opc) {
            case 1: {
                cout << "Burbuja" << endl;
                copyList.bubbleSort(comparation);
                if(print) {
                    printList(copyList);
                }
                break;
            }
            case 2: {
                cout << "Burbuja mejorado" << endl;
                copyList.bubbleSortImproved(comparation);
                if(print) {
                    printList(copyList);
                }
                break;
            }
            case 3: {
                cout << "Insercion" << endl;
                copyList.insertionSort(comparation);
                if(print) {
                    printList(copyList);
                }
                break;
            }
            case 4: {
                cout << "Seleccion" << endl;
                copyList.selectionSort(comparation);
                if(print) {
                    printList(copyList);
                }
                break;
            }
            case 5: {
                cout << "Quick" << endl;
                copyList.quickSort(comparation);
                if(print) {
                    printList(copyList);
                }
                break;
            }
            case 6: {
                cout << "Burbuja" << endl;
                copyList.bubbleSort(comparation);
                if(print) {
                    printList(copyList);
                }
                cout << endl;
                copyList = *originalList;
                cout << "Burbuja mejorado" << endl;
                copyList.bubbleSortImproved(comparation);
                if(print) {
                    printList(copyList);
                }
                cout << endl;
                copyList = *originalList;
                cout << "Insercion" << endl;
                copyList.insertionSort(comparation);
                if(print) {
                    printList(copyList);
                }
                cout << endl;
                copyList = *originalList;
                cout << "Seleccion" << endl;
                copyList.selectionSort(comparation);
                if(print) {
                    printList(copyList);
                }
                cout << endl;
                copyList = *originalList;
                cout << "Quick" << endl;
                copyList.quickSort(comparation);
                if(print) {
                    printList(copyList);
                }
                cout << endl;
                break;
            }
            case 0: {
                cout << "Cerrando . . ." << endl;
                break;
            }
            default : {
                cout << "Opcion invalida . . ." << endl;
                break;
            }
        }
        cout << "Presione entrar para continuar . . ." << endl;
        cin.ignore();
        system(CLEAR);
    } while(opc != 0);

    delete originalList;

    return 0;
}

int comparation(int a, int b) {
    return a - b;
}

void printList(StaticList<int> list) {
    for(int i = 0; i <= list.last(); i++) {
        cout << list.get(i) << " ";
    }
    cout << endl;
}