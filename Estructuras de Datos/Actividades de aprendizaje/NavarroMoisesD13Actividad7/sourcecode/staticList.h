#include <iostream>
#include <sys/time.h> //Quitar

using namespace std;

#define DEFAULT_SIZE 50

long long int comparationsQ, swapsQ; //Quitar

struct timeval timeStart,timeEnd; //Quitar

template <class TYPE>
void swap(TYPE *a, TYPE *b);

class ListException{
public:
    ListException(const string& msg) : msg_(msg) {}
    ~ListException() {}

    string what() {return msg_;}
private:
    string msg_;
};

template <class TYPE>
class StaticList {
private:
    int max_;
    TYPE* list_;
    int end_;
    int divide(int start, int end, int (*relation)(TYPE, TYPE));
    void quick(int start, int end, int (*relation)(TYPE, TYPE));
public:
    StaticList();
    StaticList(int max);
    ~StaticList();

    bool isEmpty();
    bool isFilled();
    int first();
    int last();
    int previous(int position);
    int next(int position);
    void insert(int position, TYPE data);
    void remove(int position);
    TYPE get(int position);
    void bubbleSort(int (*relation)(TYPE, TYPE));
    void bubbleSortImproved(int (*relation)(TYPE, TYPE));
    void insertionSort(int (*relation)(TYPE, TYPE));
    void selectionSort(int (*relation)(TYPE, TYPE));
    void quickSort(int (*relation)(TYPE, TYPE));
    int linearSearch(int (*relation)(TYPE, TYPE), TYPE searched);
    int binarySearch(int (*relation)(TYPE, TYPE), TYPE searched);
    void clear();
    StaticList<TYPE>& operator=(StaticList& toCopy);
};

template <class TYPE>
StaticList<TYPE>::StaticList() {
    max_ = DEFAULT_SIZE;
    list_ = new TYPE[max_];
    end_ = -1;
}

template <class TYPE>
StaticList<TYPE>::StaticList(int max) {
    max_ = max;
    list_ = new TYPE[max_];
    end_ = -1;
}

template <class TYPE>
StaticList<TYPE>::~StaticList() {
    delete[] list_;
}

template <class TYPE>
bool StaticList<TYPE>::isEmpty() {
    return end_ == -1;
}

template <class TYPE>
bool StaticList<TYPE>::isFilled() {
    return end_ == max_ - 1;
}

template <class TYPE>
int StaticList<TYPE>::first() {
    if(!isEmpty()) {
        return 0;
    } else {
        return -1;
    }
}

template <class TYPE>
int StaticList<TYPE>::last() {
    return end_;
}

template <class TYPE>
int StaticList<TYPE>::previous(int position) {
    if(!isEmpty()) {
        if(position > 0 && position <= end_) {
            return position - 1;
        } else {
            throw ListException("Out of bounds");
        }
    } else {
        throw ListException("List empty");
    }    
}

template <class TYPE>
int StaticList<TYPE>::next(int position) {
    if(!isEmpty()) {
        if(position >= 0 && position < end_) {
            return position + 1;
        } else {
            throw ListException("Out of bounds");
        }
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
void StaticList<TYPE>::insert(int position, TYPE data) {
    if(!isFilled()) {
        if(position >= 0 && position <= end_ + 1) {
            for(int i = end_; i >= position; i--) {
                list_[i + 1] = list_[i];
            }
            list_[position] = data;
            end_++;
        } else {
            throw ListException("Out of bounds");            
        }
    } else {
        throw ListException("List filled");
    }
}

template <class TYPE>
void StaticList<TYPE>::remove(int position) {
    if(!isFilled()) {
        if(position >= 0 && position <= end_ + 1) {
            for(int i = position; i < end_; i++) {
                list_[i] = list_[i + 1];
            }
            end_--;
        } else {
            throw ListException("Out of bounds");            
        }
    } else {
        throw ListException("List filled");
    }
}

template <class TYPE>
TYPE StaticList<TYPE>::get(int position) {
    if(!isEmpty()) {
        if(position >= 0 && position <= end_) {
            return list_[position];
        } else {
            throw ListException("Out of bounds");            
        }
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
void StaticList<TYPE>::bubbleSort(int (*relation)(TYPE, TYPE)) {
    int comparations = 0; //Quitar
    int swaps = 0; //Quitar

    if(isEmpty()) {
        return;
    }
    gettimeofday(&timeStart,NULL); //Quitar
    for(int i = 1; i <= end_; i++) {
        for(int j = 0; j <= end_ - i; j++) {
            comparations++; //Quitar
            if((*relation)(list_[j+1], list_[j]) < 0) {
                swaps++; //Quitar
                swap(&list_[j], &list_[j+1]);
            }
        }
    }

    gettimeofday(&timeEnd,NULL); //Quitar
    long time; //Quitar
    time = (timeEnd.tv_sec*1000000+timeEnd.tv_usec)-(timeStart.tv_sec*1000000+timeStart.tv_usec); //Quitar
    cout<< "Tiempo: "<< time <<" microsegundos" << endl; //Quitar
    cout << "Comparaciones: " << comparations << endl; //Quitar
    cout << "Intercambios: " << swaps << endl; //Quitar
}

template <class TYPE>
void StaticList<TYPE>::bubbleSortImproved(int (*relation)(TYPE, TYPE)) {
    int comparations = 0;
    int swaps = 0;

    if(isEmpty()) {
        return;
    }
    bool swapped = true;
    gettimeofday(&timeStart,NULL); //Quitar
    for(int i = 1; i <= end_; i++) {
        swapped = false;
        for(int j = 0; j <= end_ - i; j++) {
            comparations++;
            if((*relation)(list_[j+1], list_[j]) < 0) {
                swaps++;
                swap(&list_[j], &list_[j+1]);
                swapped = true;
            }
        }
        if(!swapped) {
            break;
        }
    }

    gettimeofday(&timeEnd,NULL); //Quitar
    long time; //Quitar
    time = (timeEnd.tv_sec*1000000+timeEnd.tv_usec)-(timeStart.tv_sec*1000000+timeStart.tv_usec); //Quitar
    cout<< "Tiempo: "<< time <<" microsegundos" << endl; //Quitar
    cout << "Comparaciones: " << comparations << endl;
    cout << "Intercambios: " << swaps << endl;
}

template <class TYPE>
void StaticList<TYPE>::insertionSort(int (*relation)(TYPE, TYPE)) {
    int comparations = 0;
    int swaps = 0;

    if(isEmpty()) {
        return;
    }
    gettimeofday(&timeStart,NULL); //Quitar
    for(int i = 1; i <= end_; i++) {
        for(int j = i; j > 0; j--) {
            comparations++;
            if((*relation)(list_[j], list_[j-1]) < 0) {
                swaps++;
                swap(&list_[j], &list_[j-1]);
            } else {
                break;
            }
        }
    }

    gettimeofday(&timeEnd,NULL); //Quitar
    long time; //Quitar
    time = (timeEnd.tv_sec*1000000+timeEnd.tv_usec)-(timeStart.tv_sec*1000000+timeStart.tv_usec); //Quitar
    cout<< "Tiempo: "<< time <<" microsegundos" << endl; //Quitar
    cout << "Comparaciones: " << comparations << endl;
    cout << "Intercambios: " << swaps << endl;
}

template <class TYPE>
void StaticList<TYPE>::selectionSort(int (*relation)(TYPE, TYPE)) {
    int comparations = 0;
    int swaps = 0;

    if(isEmpty()) {
        return;
    }
    int min;
    gettimeofday(&timeStart,NULL); //Quitar
    for(int i = 0; i <= end_ - 1; i++) {
        min = i;
        for(int j = i+1; j <= end_; j++) {
            comparations++;
            if((*relation)(list_[j], list_[min]) < 0) {
                min = j;   
            }        
        }
        if(min != i) {
            swaps++;
            swap(&list_[i], &list_[min]);
        }
    }

    if(isEmpty()) {
        return;
    }

    gettimeofday(&timeEnd,NULL); //Quitar
    long time; //Quitar
    time = (timeEnd.tv_sec*1000000+timeEnd.tv_usec)-(timeStart.tv_sec*1000000+timeStart.tv_usec); //Quitar
    cout<< "Tiempo: "<< time <<" microsegundos" << endl; //Quitar
    cout << "Comparaciones: " << comparations << endl;
    cout << "Intercambios: " << swaps << endl;
}

template <class TYPE>
int StaticList<TYPE>::divide(int start, int end, int (*relation)(TYPE, TYPE)) {
    int wall;
    TYPE pivot;

    wall = start;
    pivot = list_[start];
    for(int i = start + 1; i < end; i++) {
        comparationsQ++;
        if((*relation)(list_[i],pivot) < 0) {
            swapsQ++;
            wall++;
            swap(&list_[i], &list_[wall]);
        }
    }
    if(start != wall) {
        swapsQ++;
        swap(&list_[start], &list_[wall]);
    }
    
    return wall;
}

template <class TYPE>
void StaticList<TYPE>::quick(int start, int end, int (*relation)(TYPE, TYPE)) {
    if(start < end) {
        int pivotPosition = divide(start, end, *relation);
        quick(start, pivotPosition-1, *relation);
        quick(pivotPosition+1, end, *relation);
    }
}

template <class TYPE>
void StaticList<TYPE>::quickSort(int (*relation)(TYPE , TYPE)) {
    comparationsQ = 0; //Quitar
    swapsQ = 0; //Quitar

    if(isEmpty()) {
        return;
    }
    gettimeofday(&timeStart,NULL); //Quitar
    quick(0, end_, *relation);
    gettimeofday(&timeEnd,NULL); //Quitar
    long time; //Quitar
    time = (timeEnd.tv_sec*1000000+timeEnd.tv_usec)-(timeStart.tv_sec*1000000+timeStart.tv_usec); //Quitar
    cout<< "Tiempo: "<< time <<" microsegundos" << endl; //Quitar
    cout << "Comparaciones: " << comparationsQ << endl;
    cout << "Intercambios: " << swapsQ << endl;
}

template <class TYPE>
int StaticList<TYPE>::linearSearch(int (*relation)(TYPE, TYPE), TYPE searched) {
    if(!isEmpty()) {
        for(int position = 0; position <= end_; position++) {
            if((*relation)(searched, list_[position]) == 0) {
                return position;
            }
        }
        return -1;
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
int StaticList<TYPE>::binarySearch(int (*relation)(TYPE, TYPE), TYPE searched) {
    if(!isEmpty()) {
        int start, end, middle;
        start = 0;
        end = last();
        do {
            middle = (start + end) / 2;
            if((*relation)(list_[middle], searched) == 0) {
                return middle;
            } else if((*relation)(list_[middle], searched) > 0) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
        } while(start <= end);
        return -1;
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
void StaticList<TYPE>::clear() {
    end_ = -1;
}

template <class TYPE>
StaticList<TYPE>& StaticList<TYPE>::operator=(StaticList<TYPE>& toCopy) {
    if(this != &toCopy) {
        clear();
        delete[] list_;
        max_ = (int)((StaticList<TYPE>*)(&toCopy)->last())+1;
        list_ = new TYPE[max_];
        for(int i = 0; i < max_; i++) {
            insert(last() + 1, (TYPE)((StaticList<TYPE>*)(&toCopy)->get(i)));
        }
    }
    return *this;
}


template <class TYPE>
void swap(TYPE *a, TYPE *b) {
    TYPE aux = *a;
    *a = *b;
    *b = aux;
}