#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <cmath>
#include "staticStack.h"
#include "staticQueue.h"

using namespace std;

bool isOperator(string s);

#ifndef Posfix_h
#define Posfix_h
class Posfix {
private:
    string expression_;
public:
    Posfix();
    Posfix(string& expression);
    ~Posfix();

    float solve();
};

Posfix::Posfix() {
}

Posfix::Posfix(string& expression) {
    Posfix();
    expression_ = expression;
}

Posfix::~Posfix() {
}

float Posfix::solve() {
    StaticQueue<string> input;
    StaticStack<string> stack;
    string aux;
    float operating1, operating2;

    for(unsigned int i = 0; i < expression_.length(); i++) {
        if(expression_.at(i) != ',') {
            aux.push_back(expression_.at(i));
        } else {
            input.enqueue(aux);
            aux.clear();
        }
    }
    input.enqueue(aux);

    while(!input.isEmpty()) {
        aux = input.deque();
        if(isOperator(aux)) {
            operating2 = atof(stack.pop().c_str());
            operating1 = atof(stack.pop().c_str());

            if(aux.compare("+") == 0) {
                operating1 += operating2;
            } else if(aux.compare("-") == 0) {
                operating1 -= operating2;
            } else if(aux.compare("*") == 0) {
                operating1 *= operating2;
            } else if(aux.compare("/") == 0) {
                operating1 /= operating2;
            } else if(aux.compare("^") == 0) {
                operating1 = pow(operating1, operating2);
            }
            
            ostringstream auxStream;
            auxStream << operating1;
            stack.push(auxStream.str());
        } else {
            stack.push(aux);
        }
    }

    return atof(stack.pop().c_str());
}
#endif

bool isOperator(string s) {
    bool b = false;
    b += s == "+";
    b += s == "-";
    b += s == "*";
    b += s == "/";
    b += s == "^";
    return b;
}
