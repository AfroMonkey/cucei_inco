#define DEFAULT_SIZE 50

#ifndef QueueException_h
#define QueueException_h
class QueueException {
public:
    QueueException(const std::string& msg) : msg_(msg) {}
    ~QueueException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};
#endif

#ifndef StaticQueue_h
#define StaticQueue_h
template <class TYPE>
class StaticQueue {
private:
    TYPE* queue_;
    int front_;
    int back_;
    int max_;
public:
    StaticQueue();
    StaticQueue(int max);
    ~StaticQueue();

    bool isEmpty();
    void enqueue(TYPE data);
    bool isFull();
    TYPE deque();
    TYPE getFront();
    TYPE getBack();
};

template <class TYPE>
StaticQueue<TYPE>::StaticQueue() {
    max_ = DEFAULT_SIZE;
    queue_ = new TYPE[max_];
    front_ = 0;
    back_ = -1;
}

template <class TYPE>
StaticQueue<TYPE>::StaticQueue(int max) {
    max_ = max;
    queue_ = new TYPE[max_];
    front_ = 0;
    back_ = -1;
}

template <class TYPE>
StaticQueue<TYPE>::~StaticQueue() {
}

template <class TYPE>
bool StaticQueue<TYPE>::isEmpty() {
    return front_ > back_;
}

template <class TYPE>
bool StaticQueue<TYPE>::isFull() {
    return back_ - front_ == max_ - 1;
}

template <class TYPE>
void StaticQueue<TYPE>::enqueue(TYPE data) {
    if(!isFull()) {
        queue_[++back_ % max_] = data;
    } else {
        throw QueueException("Queue full");
    }
}

template <class TYPE>
TYPE StaticQueue<TYPE>::deque() {
    if(!isEmpty()) {
        return queue_[front_++ % max_];
    } else {
        throw QueueException("Queue empty");
    }
}

template <class TYPE>
TYPE StaticQueue<TYPE>::getFront() {
    if(!isEmpty()) {
        return queue_[front_ % max_];
    } else {
        throw QueueException("Queue empty");
    }
}

template <class TYPE>
TYPE StaticQueue<TYPE>::getBack() {
    if(!isEmpty()) {
        return queue_[back_ % max_];
    } else {
        throw QueueException("Queue empty");
    }
}
#endif
