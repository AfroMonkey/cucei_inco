/*
 * Autor: Navarro Presas Moisés Alejandro
 * Número de práctica: AA5
 * Nombre de práctica: Notacion Infija y Posfija
 * Fecha: 2015/10/05
 * Versión: 1
 * Tiempo: 01:30
 */

#include <iostream>
#include "infix.h"
#include "posfix.h"

using namespace std;

int main() {
    Infix infix;
    Posfix posfix;
    StaticQueue<char> infixOutput;
    string infixExpression;
    string posfixExpression;

    cout << "Expression >";
    cin >> infixExpression;

    infix = Infix(infixExpression);
    try {
        infixOutput = infix.toPosfix();

        while(!infixOutput.isEmpty()) {
            posfixExpression.push_back(infixOutput.getFront());
            cout << infixOutput.deque();
        }
        cout << " = ";
    } catch(StackException &e) {
        cout << e.what() << endl;
    } catch(QueueException &e) {
        cout << e.what() << endl;
    }

    posfix = Posfix(posfixExpression);
    try {
        cout << posfix.solve() << endl;
    } catch(StackException &e) {
        cout << e.what() << endl;
    } catch(QueueException &e) {
        cout << e.what() << endl;
    }

    return 0;
}
