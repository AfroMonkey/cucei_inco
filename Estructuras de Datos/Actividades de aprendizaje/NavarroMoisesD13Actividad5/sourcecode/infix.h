#include <iostream>
#include <map>
#include "staticStack.h"
#include "staticQueue.h"

using namespace std;

map<char, int> priorities;

bool isOperator(char c);

#ifndef Infix_h
#define Infix_h
class Infix {
private:
    string expression_;
public:
    Infix();
    Infix(string& expression);
    ~Infix();

    StaticQueue<char> toPosfix();
};

Infix::Infix() {
    priorities['('] = 0;
    priorities['+'] = 1;
    priorities['-'] = 1;
    priorities['*'] = 2;
    priorities['/'] = 2;
    priorities['^'] = 3;
    priorities[')'] = 4;
}

Infix::Infix(string& expression) {
    Infix();
    expression_ = expression;
}

Infix::~Infix() {
}

StaticQueue<char> Infix::toPosfix() {
    StaticQueue<char> input;
    StaticQueue<char> output;
    StaticStack<char> stack;

    for(unsigned int i = 0; i < expression_.length(); i++) {
        input.enqueue(expression_.at(i));
    }
    try {
        bool prevIsNumber = true;
        while(!input.isEmpty()) {
            char c = input.deque();
            if(isOperator(c)) {

                prevIsNumber = false;

                if(stack.isEmpty()) {
                    stack.push(c);
                } else {
                    if(c == ')') {
                        while(stack.getTop() != '(') {
                            if(!output.isEmpty()) {
                                output.enqueue(',');
                            }
                            output.enqueue(stack.pop());
                        }
                        stack.pop();
                    } else {
                        if(c != '(') {
                            while(!stack.isEmpty() && priorities[c] <= priorities[stack.getTop()]) {
                                if(!output.isEmpty()) {
                                    output.enqueue(',');
                                }
                                output.enqueue(stack.pop());
                            }
                        }
                        stack.push(c);
                    }
                }
            } else {
                if(!prevIsNumber) {
                    if(!output.isEmpty()) {
                        output.enqueue(',');
                    }
                }
                prevIsNumber = true;
                output.enqueue(c);
            }
        }
        while(!stack.isEmpty()) {
            output.enqueue(',');
            output.enqueue(stack.pop());
        }
    } catch(StackException &e) {
        throw StackException(e);
    } catch (QueueException &e) {
        throw QueueException(e);
    }

    return output;
}
#endif

bool isOperator(char c) {
    bool b = false;
    b += c == '(';
    b += c == '+';
    b += c == '-';
    b += c == '*';
    b += c == '/';
    b += c == '^';
    b += c == ')';
    return b;
}