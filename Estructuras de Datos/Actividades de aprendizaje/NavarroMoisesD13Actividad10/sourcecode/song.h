#include <iomanip>

using namespace std;

class Song {
private:
    string author;
    string artist;
    string name;
    int ranking;
    string mp3;
public:
    Song();
    Song(string atribs);
    ~Song();

    void setAuthor(string author);
    string getAuthor();
    void setArtist(string artist);
    string getArtist();
    void setName(string name);
    string getName();
    void setRanking(int ranking);
    int getRanking();
    void setMp3(string mp3);
    string getMp3();

    void operator=(Song& toCopy);
    bool operator==(Song song);
    bool operator<(Song &song);
    bool operator<=(Song &song);
    bool operator>(Song &song);
    bool operator>=(Song &song);

    friend std::ostream& operator<<(std::ostream &out, Song &song);
};

Song::Song() {
    ranking = 0;
}

Song::Song(string atribs) {
    int i;
    i = 0;

    while(atribs.at(i) == ' ') {
        i++;
    }
    while(i < 30) {
        author += atribs.at(i);
        i++;
    }

    while(atribs.at(i) == ' ') {
        i++;
    }
    while(i < 60) {
        artist += atribs.at(i);
        i++;
    }

    while(atribs.at(i) == ' ') {
        i++;
    }
    while(i < 90) {
        name += atribs.at(i);
        i++;
    }

    ranking = (atribs.at(90)-'0')*100;
    ranking = (atribs.at(91)-'0')*10;
    ranking = (atribs.at(92)-'0');

    i = 93;
    while(atribs.at(i) == ' ') {
        i++;
    }
    while(i < 123) {
        mp3 += atribs.at(i);
        i++;
    }
}

Song::~Song() {

}

void Song::setAuthor(string author) {
    this->author = author;
}

string Song::getAuthor() {
    return author;
}

void Song::setArtist(string artist) {
    this->artist = artist;
}

string Song::getArtist() {
    return artist;
}

void Song::setName(string name) {
    this->name = name;
}

string Song::getName() {
    return name;
}

void Song::setRanking(int ranking) {
    this->ranking = ranking;
}

int Song::getRanking() {
    return ranking;
}

void Song::setMp3(string mp3) {
    this->mp3 = mp3;
}

string Song::getMp3() {
    return mp3;
}

void Song::operator=(Song& toCopy) {
    if(this != &toCopy) {
        author = toCopy.author;
        artist = toCopy.artist;
        name = toCopy.name;
        ranking = toCopy.ranking;
        mp3 = toCopy.mp3;
    }
}

bool Song::operator==(Song song) {
    return (artist.compare(song.artist) == 0 || name.compare(song.name) == 0);
}

bool Song::operator<(Song &song) {
    int aut = author.compare(song.author);
    if(aut == 0) {
        return name.compare(song.name) < 0;   
    } else {
        return aut < 0;
    }
}

bool Song::operator<=(Song &song) {
    int aut = author.compare(song.author);
    if(aut == 0) {
        return name.compare(song.name) <= 0;   
    } else {
        return aut <= 0;
    }
}

bool Song::operator>(Song &song) {
    int aut = author.compare(song.author);
    if(aut == 0) {
        return name.compare(song.name) > 0;   
    } else {
        return aut > 0;
    }
}

bool Song::operator>=(Song &song) {
    int aut = author.compare(song.author);
    if(aut == 0) {
        return name.compare(song.name) >= 0;   
    } else {
        return aut >= 0;
    }
}

std::ostream& operator<<(std::ostream &out, Song &song)
{
    out << setw(30) << setfill(' ');
    out << song.getAuthor();
    out << setw(30) << setfill(' ');
    out << song.getArtist();
    out << setw(30) << setfill(' ');
    out << song.getName();
    out << setw(3) << setfill(' ');
    out << song.getRanking();
    out << setw(30) << setfill(' ');
    out << song.getMp3();
    out << endl;
    return out;
}