#include <fstream>
#include "listException.h"
#include "node.h"

using namespace std;

template <class TYPE>
class List {
private:
    Node<TYPE> *list;
    void swap(Node<TYPE> *n1, Node<TYPE> *n2);
    int size_;
public:
    List();
    ~List();
    //operator=(List &toCopy); TODO
    /*Capacity*/
    bool isEmpty();
    int size();
    /*Element access*/
    TYPE front();
    TYPE back();
    /*Node<TYPE> access*/
    Node<TYPE>* begin();
    Node<TYPE>* end();
    Node<TYPE>* prev(Node<TYPE> *node);
    Node<TYPE>* get(int position);
    /*Modifiers*/
    void push_front(const TYPE &value);
    void pop_front();
    void push_back(const TYPE &value);
    void pop_back();
    void insert(Node<TYPE> *position, const TYPE &value);
    void insert(int position, const TYPE &value);
    void erase(Node<TYPE> *position);
    void clear();
    /*Operations*/
    void remove(const TYPE &value);
    void sort(int (*relation)(TYPE, TYPE));
    /*Miscellaneous*/
    void forEach(void (*function)(TYPE));
    void forEachEqual(TYPE &searching, void (*funcion)(TYPE));
    void writeToFile(const string &file);
    void readFromFile(const string &file);
};

template <class TYPE>
List<TYPE>::List() {
    list = nullptr;
    size_ = 0;
}

template <class TYPE>
List<TYPE>::~List() {
    clear();
}

template <class TYPE>
bool List<TYPE>::isEmpty() {
    return list == nullptr;
}

template <class TYPE>
int List<TYPE>::size() {
    return size_;
}

template <class TYPE>
TYPE List<TYPE>::front() {
    return list->data;
}

template <class TYPE>
TYPE List<TYPE>::back() {
    Node<TYPE> *aux;
    aux = list;
    while(aux->next != nullptr) {
        aux = aux->next;
    }
    return aux->data;
}

template <class TYPE>
Node<TYPE>* List<TYPE>::begin() {
    return list;
}

template <class TYPE>
Node<TYPE>* List<TYPE>::end() {
    Node<TYPE> *aux;
    aux = list;
    while(aux->next != nullptr) {
        aux = aux->next;
    }
    return aux;
}

template <class TYPE>
Node<TYPE>* List<TYPE>::prev(Node<TYPE> *position) {
    if(position == list) {
        return nullptr;
    }
    if(position != nullptr) {
        Node<TYPE> *aux;
        aux = list;
        while(aux != nullptr && aux->next != position) {
            aux = aux->next;
        }
        if(aux != nullptr) {
            return aux;
        } else {
            throw ListException("Not found position");
        }
    } else {
        throw ListException("Invalid access (nullptr)");
    }
}

template <class TYPE>
Node<TYPE>* List<TYPE>::get(int position) {
    if(position >= 0 && position < size()) {
        Node<TYPE> *aux;
        aux = list;
        for(int i = 0; i < position; i++) {
            aux = aux->next;
        }
        return aux;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
void List<TYPE>::push_front(const TYPE &value) {
    Node<TYPE> *node;
    node = new Node<TYPE>(value);
    node->next = list;
    list = node;
    size_++;
}

template <class TYPE>
void List<TYPE>::pop_front() {
    if(!isEmpty()) {
        Node<TYPE> *aux;
        aux = list;
        list = list->next;
        delete aux;
        size_--;
    } else {
        throw ListException("Empty list");
    }
}

template <class TYPE>
void List<TYPE>::push_back(const TYPE &value) {
    Node<TYPE> *node;
    node = new Node<TYPE>(value);
    if(isEmpty()) {
        list = node;
    } else {
        Node<TYPE> *aux;
        aux = list;
        while(aux->next != nullptr) {
            aux = aux->next;
        }
        aux->next = node;        
    }
    size_++;
}

template <class TYPE>
void List<TYPE>::pop_back() {
    if(!isEmpty()) {
        Node<TYPE> *aux;
        aux = list;
        while(aux->next != nullptr) {
            aux = aux->next;
        }
        prev(aux)->next = nullptr;
        if(list == aux) {
            list = list->next;    
        }
        delete aux;
        size_--;
    } else {
        throw ListException("Empty list");
    }
}

template <class TYPE>
void List<TYPE>::insert(Node<TYPE> *position, const TYPE &value) {
    if(position != nullptr) {    
        Node<TYPE> *node;
        node = new Node<TYPE>(value);
        if(position == list) {
            node->next = list;
            list = node;
        } else {
            Node<TYPE> *aux;
            aux = list;
            while(aux != nullptr && aux->next != position) {
                aux = aux->next;
            }
            if(aux != nullptr) {
                node->next = aux->next;
                aux->next = node;
            } else {
                throw ListException("Not found position");
            }
        }
        size_++;
    } else {
        throw ListException("Invalid access (nullptr)");
    }
}

template <class TYPE>
void List<TYPE>::insert(int position, const TYPE &value) {
    if(position >= 0 && position <= size()) {    
        Node<TYPE> *node;
        node = new Node<TYPE>(value);
        if(position == 0) {
            node->next = list;
            list = node;
        } else {
            Node<TYPE> *aux;
            aux = list;
            position--;
            for(int i = 0; i < position; i++) {
                aux = aux->next;
            }
            node->next = aux->next;
            aux->next = node;
        }
        size_++;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
void List<TYPE>::erase(Node<TYPE> *position) {
    if(position != nullptr) {
        Node<TYPE> *aux;
        aux = list;
        while(aux != nullptr && aux != position) {
            aux = aux->next;
        }
        if(aux != nullptr) {
            if(aux != list) {
                prev(aux)->next = aux->next;
            } else {
                list = list->next;
            }
            delete aux;
            size_--;
        } else {
            throw ListException("Not found position");
        }
    } else {
        throw ListException("Invalid access (nullptr)");
    }
}

template <class TYPE>
void List<TYPE>::clear() {
    Node<TYPE> *aux;
    aux = list;
    while(aux != nullptr) {
        list = list->next;
        delete aux;
        aux = list;
    }
    list = nullptr;
    size_ = 0;
}

template <class TYPE>
void List<TYPE>::remove(const TYPE &value) {
    Node<TYPE> *aux;
    Node<TYPE> *aux2;
    aux = list;
    while(aux != nullptr) {
        aux = aux->next;
        if(aux != nullptr && aux->data == value) {
            aux2 = prev(aux);
            aux2->next = aux->next;
            delete aux;
            aux = aux2;
            size_--;
        }
    }
    if(list->data == value) {
        aux = list;
        list = list->next;
        delete aux;
        size_--;
    }
}

template <class TYPE>
void List<TYPE>::sort(int (*relation)(TYPE, TYPE)) {
    Node<TYPE> *nj, *njm1;
    int s = size();
    for(int i = 1; i < s; i++) {
        for(int j = i; j > 0; j--) {
            nj =   get(j);
            njm1 = get(j-1);
            if((*relation)(nj->data, njm1->data)) {
                swap(nj, njm1);
            } else {
                break;
            }
        }
    }
}

template <class TYPE>
void List<TYPE>::forEach(void (*funcion)(TYPE)) {
    Node<TYPE> *aux;
    aux = list;
    while(aux != nullptr) {
        (*funcion)(aux->data);
        aux = aux->next;
    }
}

template <class TYPE>
void List<TYPE>::swap(Node<TYPE> *n1, Node<TYPE> *n2) {
    if(n1 == n2) {
        return;
    }

    Node<TYPE> *pn1, *nn1, *pn2, *nn2;
    bool b1, b2;
    b1 = b2 = false;

    if(n1 == list) {
        b1 = true;
    } else if(n2 == list) {
        b2 = true;
    }

    if(n1->next == n2) {
        if(!b1) {
            prev(n1)->next = n2;
        }
        n1->next = n2->next;
        n2->next = n1;
    } else if(n2->next == n1) {
        if(!b2) {
            prev(n2)->next = n1;
        }
        n2->next = n1->next;
        n1->next = n2;
    } else {
        if(!b1) {
            pn1 = prev(n1);
            pn1->next = n2;
        }
        nn1 = n1->next;
        if(!b2) {
            pn2 = prev(n2);
            pn2->next = n1;
        }
        nn2 = n2->next;
        n2->next = nn1;
        n1->next = nn2;
    }
    
    if(b1) {
        list = n2;
    } else if(b2) {
        list = n1;
    }
}

template <class TYPE>
void List<TYPE>::forEachEqual(TYPE &searching, void (*function)(TYPE)) {
    Node<TYPE> *aux;
    aux = list;
    while(aux != nullptr) {
        if((TYPE)(aux->data) == (TYPE)searching) {
            (*function)(aux->data);
        }
        aux = aux->next;
    }
}

template <class TYPE>
void List<TYPE>::writeToFile(const string &filePath) {
    ofstream file;
    string info;
    Node<TYPE> *aux;

    file.open(filePath, ios::binary);
    if(file.is_open()) { //zxc
        aux = list;
        while(aux != nullptr) {
            file << aux->data;
            aux = aux->next;
        }
        file.close();
    } else{
        throw ListException("Error, file not found");
    }
}

template <class TYPE>
void List<TYPE>::readFromFile(const string &filePath) {
    ifstream file;

    file.open(filePath, ios::in);
    if(file.is_open()) {
        clear();
        string line;
        while(getline(file, line)){
            TYPE t(line);
            push_back(t);
        }
        file.close();
    } else {
        throw ListException("Error, file not found");
    }
}