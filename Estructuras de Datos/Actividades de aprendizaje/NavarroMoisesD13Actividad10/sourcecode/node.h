template <class TYPE>
class Node {
private:
public:
    TYPE data;
    Node *next;

    Node();
    Node(TYPE data);
    ~Node();
};

template <class TYPE>    
Node<TYPE>::Node() {
    next = nullptr;
}

template <class TYPE> 
Node<TYPE>::Node(TYPE data) {
    next = nullptr;
    this->data = data;
}

template <class TYPE> 
Node<TYPE>::~Node() {

}
