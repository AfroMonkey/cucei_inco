#include <iostream>
#include <sstream>
#include <cstdlib>
#include "list.h"
#include "song.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

void fill(Song *song);void print(Song song);
void print(Song &song);
int relation(Song a, Song b);
bool equals(Song a, Song b);

int main() {
    List<Song> list;
    int opc;
    do {
        list.forEach(print);
        cout << "1) Insert" << endl;
        cout << "2) Remove" << endl;
        cout << "3) Sort" << endl;
        cout << "4) Linear search (name)" << endl;
        cout << "5) Linear search (artist)" << endl;
        cout << "6) Save in file" << endl;
        cout << "7) Read from file" << endl;
        cout << "0) Exit" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();
        system(CLEAR);
        switch(opc) {
            case 0: {
                cout << "Closing" << endl;
                break;
            }
            case 1: {
                Song song;
                fill(&song);
                list.push_back(song);
                break;
            }
            case 2: {
                list.forEach(print);
                int position;
                cout << "Position >";
                cin >> position;
                cin.ignore();
                try {
                    list.erase(list.get(position));
                    cout << "Removed" << endl;
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 3: {
                cout << "Sorting..." << endl;
                list.sort(relation);
                cout << "Sorted" << endl;
                break;
            }
            case 4: {
                Song song;
                string name;
                cout << "Name >";
                getline(cin, name);
                song.setName(name);
                try {
                    list.forEachEqual(song, print);
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 5: {
                Song song;
                string artist;
                cout << "Artist >";
                getline(cin, artist);
                song.setArtist(artist);
                try {
                    list.forEachEqual(song, print);
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 6: {
                string path;
                cout << "Path>";
                cin>>path;
                cin.ignore();
                try {
                    list.writeToFile(path);
                    cout << "File writted" << endl;
                } catch(ListException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 7: {
                string path;
                cout << "Path>";
                cin>>path;
                cin.ignore();
                try {
                    list.readFromFile(path);
                    cout << "File loaded" << endl;
                } catch(ListException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        cout << "Press enter to continue ..." << endl;
        cin.ignore();
        system(CLEAR);
    } while(opc != 0);

    return 0;
}

void fill(Song *song) {
    string author;
    string artist;
    string name;
    int ranking;
    string mp3;

    cout <<  "Author> ";
    getline(cin, author);
    song->setAuthor(author);

    cout <<  "Artist> ";
    getline(cin, artist);
    song->setArtist(artist);

    cout <<  "Name> ";
    getline(cin, name);
    song->setName(name);

    cout <<  "Ranking> ";
    cin >> ranking;
    cin.ignore();
    song->setRanking(ranking);

    cout <<  "Mp3> ";
    getline(cin, mp3);
    song->setMp3(mp3);
}

void print(Song song) {
    cout << "Author :" << song.getAuthor() << endl;
    cout << "Artist :" << song.getArtist() << endl;
    cout << "Name :" << song.getName() << endl;
    cout << "Ranking :" << song.getRanking() << endl;
    cout << "Mp3 :" << song.getMp3() << endl;
    cout << "---------------------------" << endl;
}

int relation(Song a, Song b) {
    return a < b;
}
