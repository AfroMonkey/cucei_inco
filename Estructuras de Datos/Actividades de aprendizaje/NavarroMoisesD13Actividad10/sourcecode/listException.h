class ListException{
public:
    ListException(const std::string& msg) : msg_(msg) {}
    ~ListException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};
