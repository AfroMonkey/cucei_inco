#include <iostream>
#include "staticList.h"

using namespace std;

void StaticList::init() {
	end = -1;
}

bool StaticList::isEmpty() {
	return end == -1;
}

bool StaticList::isFilled() {
	return end == MAX - 1;
}

int StaticList::first() {
	return isEmpty() ? -1 : 0;
}

int StaticList::last() {
	return isEmpty() ? -1 : end;
}

int StaticList::previous(int position) {
	if(isEmpty()) {
		return -1;
	}
	return (position > 0 && position <= end) ? position - 1 : -1;
}

int StaticList::next(int position) {
	if(isEmpty()) {
		return -1;
	}
	return (position < end) ? position + 1 : -1;
}

bool StaticList::insert(int position, Song song) {
	if(position >= 0 && (position <= end + 1) && !isFilled()) {
		for(int i = end; i >= position; i--) {
			array[i + 1] = array[i];
		}
		array[position] = song;
		end++;
		return true;
	} else {
		return false;
	}
}

bool StaticList::remove(int position) {
	if(position >= 0 && (position <= end + 1) && !isFilled()) {
		for(int i = position; i < end; i++) {
			array[i] = array[i + 1];
		}
		end--;
		return true;
	} else {
		return false;
	}
}

Song StaticList::get(int position) {
	return (position >= 0 && position <= end) ? array[position] : array[position];
}

int StaticList::searchByRanking(int ranking) {
	int position;
	if(isEmpty()) {
		return -1;
	}
	for(position = 0; position <= end; position++) {
		if(array[position].getRanking() == ranking) {
			return position;
		}
	}
	return -1;
}

int StaticList::binarySearchByRanking(int ranking) {
	if(isEmpty()) {
		return -1;
	}
	int start, end, middle;
	start = 0;
	end = last();
	do {
		middle = (start + end) / 2;
		if(array[middle].getRanking() == ranking) {
			return middle;
		} else if(array[middle].getRanking() > ranking) {
			end = middle - 1;
		} else {
			start = middle + 1;
		}
	} while(start <= end);
	return -1;
}

void StaticList::print() {
	if(isEmpty()) {
		cout << "Empty list" << endl;
		return;
	}
	cout << "# \tAuthor \t\tArtist \t\tName \t\tRanking"<< endl;
	for(int i = 0; i <= end; i++) {
		cout << i << "\t" << array[i].toString() << endl;
	}
	cout << endl;
}

void StaticList::clear() {
	init();
}

void StaticList::sort() {
	if(isEmpty()) {
		return;
	}
	Song aux;
	for(int i = 0; i < end; i++) {
		for(int j = 0; j < end - i; j++) {
			if(array[j].getRanking() > array[j+1].getRanking()) {
				aux = array[j];
				array[j] = array[j+1];
				array[j+1] = aux;
			}
		}
	}
}
