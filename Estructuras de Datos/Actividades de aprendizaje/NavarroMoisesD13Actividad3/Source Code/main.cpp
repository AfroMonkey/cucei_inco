/*
 * Author: Navarro Presas Moisés Alejandro
 * Number of practice: AA2
 * Name of practice: AA2
 * Date: 2015/09/09
 * Version 1
 */
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "staticList.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

int main() {
    StaticList list;
    Song song;
    int opc;
    list.init();
    do {
        list.print();
        cout << "1) Insert" << endl;
        cout << "2) Remove" << endl;
        cout << "3) Sort" << endl;
        cout << "4) Linear search" << endl;
        cout << "5) Binary search" << endl;
        cout << "-1) Exit" << endl;
        cin >> opc;
        system(CLEAR);
        switch(opc) {
            case -1: {
                cout << "Closing" << endl;
                break;
            }
            case 1: {
                song.fill();
                list.insert(list.last() + 1, song);
                break;
            }
            case 2: {
                list.print();
                int position;
                cout << "Position >";
                cin >> position;
                if(list.remove(position)) {
                    cout << "Removed" << endl;
                } else {
                    cout << "Cant remove it" << endl;
                }
                break;
            }
            case 3: {
                cout << "Sorting..." << endl;
                list.sort();
                cout << "Sorted" << endl;
                break;
            }
            case 4: {
                int ranking;
                cout << "Ranking >";
                cin >> ranking;
                int index = list.searchByRanking(ranking);
                if(index == -1) {
                    cout << "Not found" << endl;
                } else {
                    cout << "# \t Author \t Artist \t Name \t Ranking"<< endl;
                    cout << index << "\t" << list.get(index).toString() << endl;
                }
                break;
            }
            case 5: {
                char opcBS;
                cout << "Sort (y/n)>";
                cin >> opcBS;
                if(opcBS == 'y') {
                    list.sort();
                    int ranking;
                    cout << "Ranking >";
                    cin >> ranking;
                    int index = list.binarySearchByRanking(ranking);
                    if(index == -1) {
                        cout << "Not found" << endl;
                    } else {
                        cout << "# \t Author \t Artist \t Name \t Ranking"<< endl;
                        cout << index << "\t" << list.get(index).toString() << endl;
                    }
                } else {
                    cout << "Sort canceled" << endl;
                }
                break;                
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        getchar();
        cout << "Press enter to continue ..." << endl;
        getchar();
        system(CLEAR);
    } while(opc != -1);
	return 0;
}