#include "song.h"
#include <sstream>

void Song::setAuthor(string author) {
    this->author = author;
}

string Song::getAuthor() {
    return author;
}

void Song::setArtist(string artist) {
    this->artist = artist;
}

string Song::getArtist() {
    return artist;
}

void Song::setName(string name) {
    this->name = name;
}

string Song::getName() {
    return name;
}

void Song::setRanking(int ranking) {
    this->ranking = ranking;
}

int Song::getRanking() {
    return ranking;
}

void Song::fill() {
    cout << "Author >";
    cin >> author;
    cout << "Artist >";
    cin >> artist;
    cout << "Name >";
    cin >> name;
    cout << "Ranking >";
    cin >> ranking;
}

string Song::toString() {
    string s = "";
    ostringstream sRanking;
    s += author + "\t" + "\t";
    s += artist + "\t" + "\t";
    s += name + "\t" + "\t";
    sRanking<<ranking;
    s += sRanking.str() + "\t";
    return s;
}