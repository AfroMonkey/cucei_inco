#include "song.h"

#define MAX	50

class StaticList {
private:
    Song array[MAX];
    int end;
public:
    void init();
    bool isEmpty();
    bool isFilled();
    int first();
    int last();
    int previous(int position);
    int next(int position);
    bool insert(int position, Song song);
    bool remove(int position);
    Song get(int position);
    int searchByRanking(int ranking);
    int binarySearchByRanking(int ranking);
    void print();
    void clear();
    void sort();
};