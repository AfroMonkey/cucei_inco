#include <iostream>

using namespace std;

class Song {
private:
    string author;
    string artist;
    string name;
    int ranking;
public:
    void setAuthor(string author);
    string getAuthor();
    void setArtist(string artist);
    string getArtist();
    void setName(string name);
    string getName();
    void setRanking(int ranking);
    int getRanking();
    void fill();
    string toString();
};