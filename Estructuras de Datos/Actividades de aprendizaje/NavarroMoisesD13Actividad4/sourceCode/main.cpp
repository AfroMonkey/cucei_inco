/*
 * Autor: Navarro Presas Moisés Alejandro
 * Número de práctica: AA4
 * Nombre de práctica: AA4
 * Fecha: 2015/09/24
 * Versión: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "staticList.h"
#include "song.h"

#define SONG_MAX 3000

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

int Song.relation(Song a, Song b);
int relationByArtist(Song a, Song b);
string songToString(Song song);

int main() {
    StaticList<Song> list(SONG_MAX);
    Song song;
    int opc;
    do {
        cout << "Author \t Artist \t Name \t Ranking \t MP3"<< endl;
        cout << list.toString(songToString) << endl;
        cout << "1) Insert" << endl;
        cout << "2) Remove" << endl;
        cout << "3) Sort" << endl;
        cout << "4) Linear search (name)" << endl;
        cout << "5) Binary search (name)" << endl;
        cout << "6) Linear search (artist)" << endl;
        cout << "7) Binary search (artist)" << endl;
        cout << "0) Exit" << endl;
        cin >> opc;
        system(CLEAR);
        switch(opc) {
            case 0: {
                cout << "Closing" << endl;
                break;
            }
            case 1: {
                song.fill();
                try {
                    list.insert(list.last() + 1, song);
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 2: {
                list.toString(songToString);
                int position;
                cout << "Position >";
                cin >> position;
                try {
                    list.erase(list.get(position));
                    cout << "Removed" << endl;
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 3: {
                cout << "Sorting..." << endl;
                list.sort(Song.relation);
                cout << "Sorted" << endl;
                break;
            }
            case 4: {
                string name;
                cout << "Name >";
                cin >> name;
                song.setName(name);
                try {
                    int index = list.linearSearch(Song.relation, song);
                    if(index == -1) {
                        cout << "Not found" << endl;
                    } else {
                        cout << "# \t Author \t Artist \t Name \t Ranking \t MP3"<< endl;
                        cout << index << "\t" << list.get(index).toString() << endl;
                    }
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 5: {
                char opcBS;
                cout << "Sort (y/n)>";
                cin >> opcBS;
                if(opcBS == 'y') {
                    list.sort(Song.relation);
                    string name;
                    cout << "Name >";
                    cin >> name;
                    song.setName(name);
                    try {
                        int index = list.linearSearch(Song.relation, song);
                        if(index == -1) {
                            cout << "Not found" << endl;
                        } else {
                            cout << "# \t Author \t Artist \t Name \t Ranking \t MP3"<< endl;
                            cout << index << "\t" << list.get(index).toString() << endl;
                        }
                    } catch(ListException e) {
                        cout << e.what() << endl;
                    }
                } else {
                    cout << "Sort canceled" << endl;
                }
                break;                
            }
            case 6: {
                string artist;
                cout << "Artist >";
                cin >> artist;
                song.setArtist(artist);
                try {
                    int index = list.linearSearch(relationByArtist, song);
                    if(index == -1) {
                        cout << "Not found" << endl;
                    } else {
                        cout << "# \t Author \t Artist \t Name \t Ranking \t MP3"<< endl;
                        cout << index << "\t" << list.get(index).toString() << endl;
                    }
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 7: {
                char opcBS;
                cout << "Sort (y/n)>";
                cin >> opcBS;
                if(opcBS == 'y') {
                    list.sort(relationByArtist);
                    string artist;
                    cout << "Artist >";
                    cin >> artist;
                    song.setArtist(artist);
                    try {
                        int index = list.linearSearch(relationByArtist, song);
                        if(index == -1) {
                            cout << "Not found" << endl;
                        } else {
                            cout << "# \t Author \t Artist \t Name \t Ranking \t MP3"<< endl;
                            cout << index << "\t" << list.get(index).toString() << endl;
                        }
                    } catch(ListException e) {
                        cout << e.what() << endl;
                    }
                } else {
                    cout << "Sort canceled" << endl;
                }
                break;                
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        getchar();
        cout << "Press enter to continue ..." << endl;
        getchar();
        system(CLEAR);
    } while(opc != 0);
    return 0;
}

int Song.relation(Song a, Song b) {
    return a.getName().compare(b.getName());
}

int relationByArtist(Song a, Song b) {
    return a.getArtist().compare(b.getArtist());
}

string songToString(Song song) {
    return song.toString() + "\n";
}