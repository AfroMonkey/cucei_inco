#include <iostream>

using namespace std;

class Song {
private:
    string author;
    string artist;
    string name;
    int ranking;
    string mp3;
public:
    void setAuthor(string author);
    string getAuthor();
    void setArtist(string artist);
    string getArtist();
    void setName(string name);
    string getName();
    void setRanking(int ranking);
    int getRanking();
    void setMp3(string mp3);
    string getMp3();
    void fill();
    string toString();
};