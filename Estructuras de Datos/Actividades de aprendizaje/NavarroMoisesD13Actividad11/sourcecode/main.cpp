/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: 
 * Nombre de practica: 
 * Fecha: 2015/11/05
 * Version: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdlib>
#include "list.h"
#include "Employee.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_INSERT 1
#define OPC_ERASE 2
#define OPC_CLEAR 3
#define OPC_EXIT 0

void print(Employee employee);
void insert(List<Employee> &list);
void erase(List<Employee> &list);

int main() {
    List<Employee> list;
    int opc;

    do {
        list.forEach(print);
        cout << endl;
        cout << "1) Insert" << endl;
        cout << "2) Erase" << endl;
        cout << "3) Clear" << endl;
        cout << "0) Exit" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();

        switch(opc) {
            case OPC_INSERT:
                insert(list);
                break;
            case OPC_ERASE:
                erase(list);
                break;
            case OPC_CLEAR:
                list.clear();
                cout << "Cleared" << endl;
                break;
            case OPC_EXIT:
                cout << "Closing . . ." << endl;
                break;
            default:
                cout << "Invalid option" << endl;
                break;
        }
        cout << "Press enter to continue . . .";
        cin.ignore();
        system(CLEAR);
    } while(opc != OPC_EXIT);

    return 0;
}

void print(Employee employee) {
    cout << "name: " << employee.name << endl;
    cout << "address: " << employee.address << endl;
    cout << "telephone: " << employee.telephone << endl;
    cout << "birthdate: " << employee.birthdate << endl;
    cout << "rfc: " << employee.rfc << endl;
    cout << "-----" << endl;
}

void insert(List<Employee> &list) {
    Employee employee;

    cout << "name>";
    getline(cin, employee.name);
    cout << "address>";
    getline(cin, employee.address);
    cout << "telephone>";
    getline(cin, employee.telephone);
    cout << "birthdate>";
    getline(cin, employee.birthdate);
    cout << "rfc>";
    getline(cin, employee.rfc);

    if(list.empty()) {
        list.push_front(employee);
        cout << "Inserted" << endl;
    } else {
        int position;
        cout << "Position>";
        cin >> position;
        cin.ignore();
        try {
            list.insert(position, employee);
            cout << "Inserted" << endl;
        } catch(ListException &e) {
            cout << e.what() << endl;
        }
    }
}

void erase(List<Employee> &list) {
    int position;
    cout << "Position>";
    cin >>position;
    cin.ignore();
    try {
        list.erase(position);
        cout << "Erased" << endl;
    } catch(ListException &e) {
        cout << e.what() << endl;
    }
}
