#ifndef Employee_INCLUDED
#define Employee_INCLUDED

class Employee {
public:
    std::string name;
    std::string address;
    std::string telephone;
    std::string birthdate;
    std::string rfc;
};

#endif
