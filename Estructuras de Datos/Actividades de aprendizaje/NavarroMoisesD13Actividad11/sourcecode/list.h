#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "node.h"
/*------------------------------------------------------------------------------------------------*/
class ListException{
public:
    ListException(const std::string& msg) : msg_(msg) {}
    ~ListException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};
/*------------------------------------------------------------------------------------------------*/

template <class TYPE>
class List {
private:
    Node<TYPE> *list;
public:
    List();
    ~List();
    /*Capacity*/
    bool empty();
    int size();
    /*Element access*/
    TYPE& front();
    TYPE& back();
    TYPE& at(const int &position);
    /*Node<TYPE> access*/
    Node<TYPE>* begin();
    Node<TYPE>* end();
    Node<TYPE>* get(const int &position);
    Node<TYPE>* search(const TYPE &val);
    /*Modifiers*/
    void push_front(const TYPE &val);
    void pop_front();
    void push_back(const TYPE &val);
    void pop_back();
    void insert(Node<TYPE> *position, const TYPE &val);
    void insert(const int &position, const TYPE &val);
    void erase(Node<TYPE> *position);
    void erase(const int &position);
    void clear();
    /*Operations*/
    void remove(const TYPE &val);
    void sort(int (*relation)(TYPE, TYPE));
    /*Misc*/
    void forEach(void (*function)(TYPE));
};

template <class TYPE>
List<TYPE>::List() {
    list = nullptr;
}

template <class TYPE>
List<TYPE>::~List() {
    clear();
}

template <class TYPE>
bool List<TYPE>::empty() {
    return list == nullptr;
}

template <class TYPE>
int List<TYPE>::size() {
    int s;
    Node<TYPE> *aux;

    s = 0;
    aux = list;

    while(aux != nullptr) {
        s++;
        aux = aux->next;
    }

    return s;
}

template <class TYPE>
TYPE& List<TYPE>::front() {
    if(!empty()) {
        return list->data;
    } else {
        throw ListException("Empty");
    }
}

template <class TYPE>
TYPE& List<TYPE>::back() {
    if(!empty()) {
        Node<TYPE> *aux;

        aux = list;

        while(aux->next != nullptr) {
            aux = aux->next;
        }

        return aux->data;
    } else {
        throw ListException("Empty");
    }
}

template <class TYPE>
TYPE& List<TYPE>::at(const int &position) {
    return get(position)->data;
}

template <class TYPE>
Node<TYPE>* List<TYPE>::begin() {
    if(!empty()) {
        return list;
    } else {
        throw ListException("Empty");
    }
}

template <class TYPE>
Node<TYPE>* List<TYPE>::end() {
    if(!empty()) {
        Node<TYPE> *aux;

        aux = list;

        while(aux->next != nullptr) {
            aux = aux->next;
        }

        return aux;        
    } else {
        throw ListException("Empty");
    }
}

template <class TYPE>
Node<TYPE>* List<TYPE>::get(const int &position) {
    Node<TYPE> *node;
    int i;

    node = list;
    i = 0;

    while(node != nullptr && i < position) {
        node = node->next;
        i++;
    }

    if(node != nullptr) {
        return node;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
Node<TYPE>* List<TYPE>::search(const TYPE &val) {
    Node<TYPE> *node;

    node = list;
    while(node != nullptr && node->data != val) {
        node = node->next;
    }
    return node;
}

template <class TYPE>
void List<TYPE>::push_front(const TYPE &val) {
    Node<TYPE> *node;

    node = new Node<TYPE>(val);

    node->next = list;
    if(!empty()) {
        list->prev = node;
    }
    list = node;
}

template <class TYPE>
void List<TYPE>::pop_front() {
    if(!empty()) {
        Node<TYPE> *aux;

        aux = list;
        list = list->next;
        delete aux;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
void List<TYPE>::push_back(const TYPE &val) {
    Node<TYPE> *node;

    node = new Node<TYPE>(val);

    if(empty()) {
        list = node;
    } else {
        Node<TYPE> *aux;

        aux = list;
        while(aux->next != nullptr) {
            aux = aux->next;
        }
        aux->next = node;
        node->prev = aux;
    }
}

template <class TYPE>
void List<TYPE>::pop_back() {
    if(!empty()) {
        Node<TYPE> *aux;

        aux = list;

        while(aux->next != nullptr) {
            aux = aux->next;
        }

        if(aux == list) {
            list = nullptr;
        } else {
            aux->prev->next = nullptr;
        }
        delete aux;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
void List<TYPE>::insert(Node<TYPE> *position, const TYPE &val) {
    Node<TYPE> *node;

    node = new Node<TYPE>(val);


    if(position != list) {
        position->prev->next = node;
    } else {
        list = node;
    }
    position->prev = node;
    node->next = position;
}

template <class TYPE>
void List<TYPE>::insert(const int &position, const TYPE &val) {
    if(position == 0) {
        push_front(val);
    } else if(position == size()) {
        push_back(val);
    } else {
        insert(get(position), val);
    }
}

template <class TYPE>
void List<TYPE>::erase(Node<TYPE> *position) {
    if(position != end()) {
        position->next->prev = position->prev;
    }
    if(position != list) {
        position->prev->next = position->next;
    } else {
        list = list->next;
    }

    delete position;
}

template <class TYPE>
void List<TYPE>::erase(const int &position) {
    if(position == 0) {
        pop_front();
    } else if(position == size() - 1) {
        pop_back();
    } else {
        erase(get(position));
    }
}

template <class TYPE>
void List<TYPE>::clear() {
    Node<TYPE> *aux;

    aux = list;

    while(aux != nullptr) {
        list = list->next;
        delete aux;
        aux = list;
    }
}

template <class TYPE>
void List<TYPE>::remove(const TYPE &val) {
    Node<TYPE> *aux;
    Node<TYPE> *aux2;

    aux = list->next;

    while(aux != nullptr) {
        if(aux->data == val) {
            aux2 = aux->next;
            erase(aux);
            aux = aux2;
        } else {
            aux = aux->next;
        }
    }
}

template <class TYPE>
void List<TYPE>::sort(int (*relation)(TYPE, TYPE)) {
    //TODO
}

template <class TYPE>
void List<TYPE>::forEach(void (*function)(TYPE)) {
    Node<TYPE> *aux;

    aux = list;

    while(aux != nullptr) {
        (*function)(aux->data);
        aux = aux->next;
    }
}
#endif
