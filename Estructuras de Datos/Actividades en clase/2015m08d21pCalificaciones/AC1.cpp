/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 1
 * Name of practice: Calificaciones
 * Date: 2015/08/21
 * Version 1
 */
#define ALUMNOS 5
#define MATERIAS 3

#include <iostream>
#include <string>

using namespace std;

class alumno{
private:
	string nombre;
	int calificaciones[MATERIAS];
public:
	void setNombre(string n) {nombre = n;}
	string getNombre() {return nombre;}
	int getCalificacion(int i) {return 0 <= i && i < MATERIAS? calificaciones[i] : -1;}

	int setCalificacion(int i, int c) {
		if((0 <= i && i < MATERIAS) && (0 <= c && c <= 100)) {
			calificaciones[i] = c;
			return 1;
		} else {
			return 0;
		}
	}

	float getPromedio() {
		float promedio = 0;
		for(int i = 0; i < MATERIAS; i++) {
			promedio += calificaciones[i];
		}
		promedio /= MATERIAS;
		return promedio;
	}
};

//----------------------------------------------------------//
void setAlumnos(alumno alumnos[]);
void printAlumnos(alumno alumnos[]);

int main() {
	alumno alumnos[ALUMNOS];
	setAlumnos(alumnos);
	printAlumnos(alumnos);
	return 0;
}

void setAlumnos(alumno alumnos[]) {
	string nombre;
	int calificacion;
	for(int i = 0; i < ALUMNOS; i++) {
		cout<<"Ingrese el nombre del alumno "<<i<<" >";
		cin>>nombre;
		alumnos[i].setNombre(nombre);
		for(int j = 0; j < MATERIAS; j++) {
			do {
				cout<<"Ingrese la calificacion de la materia "<<j<<" del alumno "<<alumnos[i].getNombre()<<" >";
				cin>>calificacion;
			}while(alumnos[i].setCalificacion(j, calificacion) == 0);
		}
	}
}

void printAlumnos(alumno alumnos[]) {
	//Interfaz
	cout<<"#\t"<<"Nombre\t\t\t";
	for(int i = 0; i < MATERIAS; i++) {
		cout<<"Materia "<<i<<"\t";
	}
	cout<<"Promedio"<<endl;
	//-Interfaz	
	for(int i = 0; i < ALUMNOS; i++) {
		cout<<i<<"\t"<<alumnos[i].getNombre()<<"\t\t\t";
		for(int j = 0; j < MATERIAS; j++) {
			cout<<alumnos[i].getCalificacion(j)<<"\t\t";
		}
		cout<<alumnos[i].getPromedio()<<endl;
	}
}