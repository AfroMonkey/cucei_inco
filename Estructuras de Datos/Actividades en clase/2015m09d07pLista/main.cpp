/*
 * Author: Navarro Presas Moisés Alejandro
 * Number of practice: 
 * Name of practice: Static List
 * Date: 2015/09/07
 * Version 1
 */
#include <iostream>
#include <cstdlib>
#include "staticList.h"

using namespace std;

int main() {
	StaticList list;
	int opc, position, value;
	list.init();
	do {
		system("cls");
		cout << "01) isEmpty()" <<endl;
		cout << "02) isFilled()" <<endl;
		cout << "03) first()" <<endl;
		cout << "04) last()" <<endl;
		cout << "05) previous(int position)" <<endl;
		cout << "06) next(int position)" <<endl;
		cout << "07) insert(int position, int value)" <<endl;
		cout << "08) remove(int position)" <<endl;
		cout << "09) get(int position)" <<endl;
		cout << "10) find(int value)" <<endl;
		cout << "11) print()" <<endl;
		cout << "12) clear()" <<endl;
		cout << "13) sort()" <<endl;
		cout << "14) exit" <<endl;
		cin >> opc;
		cin.get();
		system("cls");
		switch(opc) {
			case 1: {
				cout << list.isEmpty() << endl;
				cin.get();
				break;
			}

			case 2: {
				cout << list.isFilled() << endl;
				cin.get();
				break;
			}

			case 3: {
				cout << list.first() << endl;
				cin.get();
				break;
			}

			case 4: {
				cout << list.last() << endl;				
				cin.get();
				break;
			}

			case 5: {
				cout << "position >";
				cin >> position;
				cin.get();
				cout << list.previous(position) << endl;
				cin.get();
				break;
			}

			case 6: {
				cout << "position >";
				cin >> position;
				cin.get();
				cout << list.next(position) << endl;
				cin.get();
				break;
			}

			case 7: {
				cout << "position >";
				cin >> position;
				cout << "value >";
				cin >> value;
				cin.get();
				cout <<list.insert(position, value) << endl;
				cin.get();
				break;
			}

			case 8: {
				cout << "position >";
				cin >> position;
				cin.get();
				cout << list.remove(position) << endl;
				cin.get();
				break;
			}

			case 9: {
				cout << "position >";
				cin >> position;
				cin.get();
				cout << list.get(position) << endl;
				cin.get();
				break;
			}

			case 10: {
				cout << "value >";
				cin >> value;
				cin.get();
				cout << list.find(value) << endl;
				cin.get();
				break;
			}

			case 11: {
				list.print();
				cin.get();
				break;
			}

			case 12: {
				list.clear();
				cin.get();
				break;
			}

			case 13: {
				list.sort();
				cin.get();
				break;
			}

			case 14: {
				cout << "Press enter to continue ...";
				cin.get();
				break;
			}

			default: {
				cout << "invalid option" << endl;
				cin.get();
				break;
			}
		}
	} while(opc != 14);
	return 0;
}