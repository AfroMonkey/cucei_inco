#include <iostream>
#include "staticList.h"

using namespace std;

void StaticList::init() {
	end = -1;
}

bool StaticList::isEmpty() {
	return (end == -1) ? true : false;
}

bool StaticList::isFilled() {
	return end == MAX;
}

int StaticList::first() {
	return isEmpty() ? -1 : 0;
}

int StaticList::last() {
	return isEmpty() ? -1 : end;
}

int StaticList::previous(int position) {
	if(isEmpty()) {
		return -1;
	}
	return (position > 0 && position <= end) ? position - 1 : -1;
}

int StaticList::next(int position) {
	if(isEmpty()) {
		return -1;
	}
	return (position < end) ? position + 1 : -1;
}

bool StaticList::insert(int position, int value) {
	if(position >= 0 && (position <= end + 1) && !isFilled()) {
		for(int i = end; i >= position; i--) {
			array[i + 1] = array[i];
		}
		array[position] = value;
		end++;
		return true;
	} else {
		return false;
	}
}

bool StaticList::remove(int position) {
	if(position >= 0 && (position <= end + 1) && !isFilled()) {
		for(int i = position; i < end; i++) {
			array[i] = array[i + 1];
		}
		end--;
		return true;
	} else {
		return false;
	}
}

int StaticList::get(int position) {
	//TODO check -1
	return (position >= 0 && position <= end) ? array[position] : -1;
}

int StaticList::find(int value) {
	int position;
	if(isEmpty()) {
		return -1;
	}
	for(position = 0; position <= end; position++) {
		if(array[position] == value) {
			return position;
		}
	}
	return -1;
}

void StaticList::print() {
	if(isEmpty()) {
		return;
	}
	for(int i = 0; i <= end; i++) {
		cout << array[i] << " ";
	}
	cout << endl;
}

void StaticList::clear() {
	init();
}

void StaticList::sort() {
	//TODO
}
