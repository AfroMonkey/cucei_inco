#define MAX	50

class StaticList {
private:
	int array[MAX];
	int end;
public:
	void init();
	bool isEmpty();
	bool isFilled();
	int first();
	int last();
	int previous(int position);
	int next(int position);
	bool insert(int position, int value);
	bool remove(int position);
	int get(int position);
	int find(int value);
	void print();
	void clear();
	void sort();
};