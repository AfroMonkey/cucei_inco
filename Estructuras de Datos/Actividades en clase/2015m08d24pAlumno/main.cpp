/*
 * Author: Navarro Presas Móisés Alejandro
 * Number of practice: 2
 * Name of practice: Student
 * Date: 2015/08/24
 * Version 1
 */
#include <iostream>
#include "student.h"

using namespace std;

int main() {
	Student student;
	student.fill();
	student.print();
	return 0;
}