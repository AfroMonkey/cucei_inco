#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include "node.h"

/*------------------------------------------------------------------------------------------------*/
class QueueException{
public:
    QueueException(const std::string& msg) : msg_(msg) {}
    ~QueueException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};
/*------------------------------------------------------------------------------------------------*/

template <typename TYPE>
class Queue {
private:    
    Node<TYPE> *queue_;
public:
    Queue();
    ~Queue();

    bool empty();
    unsigned int size();
    TYPE& front();
    TYPE& back();
    void enqueue(const TYPE& val);
    TYPE deque();
};

template <typename TYPE>
Queue<TYPE>::Queue() {
    queue_ = nullptr;
}

template <typename TYPE>
Queue<TYPE>::~Queue() {
    while(!empty()) {
        deque();
    }
}

template <typename TYPE>
bool Queue<TYPE>::empty() {
    return queue_ == nullptr;
}

template <typename TYPE>
unsigned int Queue<TYPE>::size() {
    unsigned int s;
    Node<TYPE> *aux;

    s = 0;
    aux = queue_;
    while(aux != nullptr) {
        s++;
        aux = aux->nexT;
    }

    return s;
}

template <typename TYPE>
TYPE& Queue<TYPE>::front() {
    if(!empty()) {
        return queue_->data;
    } else {
        throw QueueException("Empty");
    }
}

template <typename TYPE>
TYPE& Queue<TYPE>::back() {
    if(!empty()) {
        Node<TYPE> *aux;

        aux = queue_;
        while(aux->next != nullptr) {
            aux = aux->next;
        }

        return aux->data;
    } else {
        throw QueueException("Empty");
    }
}

template <typename TYPE>
void Queue<TYPE>::enqueue(const TYPE& val) {
    Node<TYPE> *node;
    node = new Node<TYPE>(val);

    if(empty()) {
        queue_ = node;
    } else {
        Node<TYPE> *aux;

        aux = queue_;
        while(aux->next != nullptr) {
            aux = aux->next;
        }
        aux->next = node;
    }
}

template <typename TYPE>
TYPE Queue<TYPE>::deque() {
    if(!empty()) {
        TYPE val;
        Node<TYPE> *aux;

        aux = queue_;
        val = aux->data;
        queue_ = queue_->next;
        delete aux;

        return val;
    } else {
        throw QueueException("Empty");
    }
}
#endif
