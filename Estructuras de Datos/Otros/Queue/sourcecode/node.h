#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED
template <typename TYPE>
class Node {
public:
    Node();
    Node(TYPE data);

    TYPE data;
    Node<TYPE> *next;
};

template <typename TYPE>
Node<TYPE>::Node() {
    next = nullptr;
}

template <typename TYPE>
Node<TYPE>::Node(TYPE data) {
    this->data = data;
    next = nullptr;
}
#endif
