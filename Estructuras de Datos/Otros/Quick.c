#include <stdio.h>

void print(int *list, int start, int end);
void spwa(int *a, int *b);
int partition(int *list, int start, int end);
void quick(int *list, int start, int end);

#define NUM_ELEMENS 43429 //43430 crash

int main() {
    int *list = new int[NUM_ELEMENS];

    for(int i = 0; i < NUM_ELEMENS; i++) {
        list[i] = i;
    }

    print(list, 0, NUM_ELEMENS);
    quick(list, 0, NUM_ELEMENS - 1);
    print(list, 0, NUM_ELEMENS);
    return 0;
}

void print(int *list, int start, int end) {
    return;
    for(int i = start; i < end; i++) {
        printf("%d ", list[i]);
    }
    printf("\n");
}

void swap(int *a, int *b) {
    int aux = *a;
    *a = *b;
    *b = aux;
}

int partition(int *list, int start, int end) {
    int aux;
    int pivot; //TYPE

    pivot = list[start];
    aux = start;

    while(aux < end) {
        while(list[end] > pivot) {
            end--;
        }
        while((aux < end) && list[aux] <= pivot) {
            aux++;
        }
        if(aux < end) {
            swap(&list[aux], &list[end]);
            print(list, 0, NUM_ELEMENS);
        }
    }
    swap(&list[end], &list[start]);
    print(list, 0, NUM_ELEMENS);
    return end;
}

void quick(int *list, int start, int end) {
    if(start < end) {
        int pivotPosition = partition(list, start, end);
        quick(list, start, pivotPosition-1);
        quick(list, pivotPosition+1, end);
    }
}
