/*
 * Autor: Navarro Presas Moisés Alejandro
 * Número de práctica: 
 * Nombre de práctica: 
 * Fecha: 
 * Versión: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "staticStack.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

using namespace std;

int main() {
    StaticStack<char> stack;
    int opc;
    do {
        cout << "1) Push" << endl;
        cout << "2) Pop" << endl;
        cout << "3) Get top" << endl;
        cout << "0) Exit" << endl;
        cin >> opc;
        switch(opc) {
            case 1: {
                char c;
                cout << "Char >";
                cin >> c;
                try {
                    cout << "Pushing ..." << endl;
                    stack.push(c);
                    cout << "Pushed" << endl;
                } catch(StackException &e) {
                    cout << e.what() << endl;    
                }
                break;
            }
            case 2: {
                try {
                    cout << "Poping ..." << endl;
                    stack.pop();
                    cout << "Poped" << endl;
                } catch(StackException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 3: {
                try {
                    cout << "Getting ..." << endl;
                    cout << stack.getTop() << endl;
                } catch (StackException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 0: {
                cout << "Closing ..." << endl;
                break;
            }
            default: {
                break;
            }
        }
        getchar();
        cout << "Press enter to continue . . . ";
        getchar();
        system(CLEAR);
    } while (opc != 0);
    return 0;
}