#include <iostream>

#define DEFAULT_SIZE 50

#ifndef StackException_h
#define StackException_h
class StackException{
public:
    StackException(const std::string& msg) : msg_(msg) {}
    ~StackException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};
#endif

#ifndef StaticStack_h
#define StaticStack_h
template <class TYPE>
class StaticStack {
public:
    StaticStack();
    StaticStack(int max);
    ~StaticStack() {}

    bool isEmpty();
    bool isFull();
    void push(TYPE data);
    TYPE pop();
    TYPE getTop();
private:
    int max_;
    TYPE* stack_;
    int top_;
};

template <class TYPE>
StaticStack<TYPE>::StaticStack() {
    max_ = DEFAULT_SIZE;
    stack_ = new TYPE[max_];
    top_ = -1;
}

template <class TYPE>
StaticStack<TYPE>::StaticStack(int max) {
    max_ = max;
    stack_ = new TYPE[max_];
    top_ = -1;
}

template <class TYPE>
bool StaticStack<TYPE>::isEmpty() {
    return top_ == -1;
}

template <class TYPE>
bool StaticStack<TYPE>::isFull() {
    return top_ == max_ - 1;
}

template <class TYPE>
void StaticStack<TYPE>::push(TYPE data) {
    if(!isFull()) {
        stack_[++top_] = data;
    } else {
        throw StackException("Stack full");
    }
}

template <class TYPE>
TYPE StaticStack<TYPE>::pop() {
    if(!isEmpty()) {
        return stack_[top_--];
    } else {
        throw StackException("Stack empty");
    }
}

template <class TYPE>
TYPE StaticStack<TYPE>::getTop() {
    if(!isEmpty()) {
        return stack_[top_];
    } else {
        throw StackException("Stack empty");
    }
}
#endif
