/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: 
 * Nombre de practica: 
 * Fecha: 2015/11/18
 * Version: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdlib>
#include "BinaryTree.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_EXIT 0
#define OPC_WEIGHT 1
#define OPC_HEIGHT 2 
#define OPC_ROOT 3
#define OPC_INSERT 5
#define OPC_REMOVE 6
#define OPC_TRIM 7
#define OPC_PREORDER 8
#define OPC_ORDER 9
#define OPC_POSTORDER 10

void print(int i);

int main() {
    BinaryTree<int> binaryTree;
    int opc;

    opc = 0;
    do {
        cout << OPC_EXIT << ") EXIT" << endl;
        cout << OPC_WEIGHT << ") WEIGHT" << endl;
        cout << OPC_HEIGHT  << ") HEIGHT" << endl;
        cout << OPC_ROOT << ") ROOT" << endl;
        cout << OPC_INSERT << ") INSERT" << endl;
        cout << OPC_REMOVE << ") REMOVE" << endl;
        cout << OPC_TRIM << ") TRIM" << endl;
        cout << OPC_PREORDER << ") PREORDER" << endl;
        cout << OPC_ORDER << ") ORDER" << endl;
        cout << OPC_POSTORDER << ") POSTORDER" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();

        switch(opc) {
            case OPC_EXIT: {
                cout << "Clossing . . ." << endl;
                break;
            }
            case OPC_WEIGHT: {
                cout << "Weight: " << binaryTree.weight() << endl;
                break;
            }
            case OPC_HEIGHT : {
                cout << "Height: " << binaryTree.height() << endl;
                break;
            }
            case OPC_ROOT: {
                if(!binaryTree.empty()) {
                    cout << "Root: " << binaryTree.root()->data << endl;
                } else {
                    cout << "Error, empty" << endl;
                }
                break;
            }
            case OPC_INSERT: {
                int data;
                cout << "Data >";
                cin >> data;
                cin.ignore();
                binaryTree.insert(data);
                cout << "Inserted" << endl; 
                break;
            }
            case OPC_REMOVE: {
                int data;
                cout << "Data >";
                cin >> data;
                cin.ignore();
                binaryTree.remove(binaryTree.get(data));
                cout << "Removed" << endl; 
                break;
            }
            case OPC_TRIM: {
                int data;
                cout << "Data >";
                cin >> data;
                cin.ignore();
                binaryTree.trim(binaryTree.get(data));
                cout << "Trimmed" << endl; 
                break;
            }
            case OPC_PREORDER: {
                binaryTree.preorder().forEach(print);
                cout << endl;
                break;
            }
            case OPC_ORDER: {
                binaryTree.order().forEach(print);
                cout << endl;
                break;
            }
            case OPC_POSTORDER : {
                binaryTree.postorder().forEach(print);
                cout << endl;
                break;
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        if(opc != OPC_EXIT) {
            cout << "Press enter to continue . . .";
            cin.ignore();
            system(CLEAR);
        }
    } while(opc != OPC_EXIT);

    return 0;
}

void print(int i) {
    cout << i << " ";
}
