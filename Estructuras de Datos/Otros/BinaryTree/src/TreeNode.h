#ifndef NODE_INCLUDED
#define NODE_INCLUDED

template <class TYPE>
class TreeNode {
public:
    TYPE data;
    TreeNode *left;
    TreeNode *right;
    
    TreeNode();
    TreeNode(TYPE data);
    bool isLeaf();
};

template <class TYPE>
TreeNode<TYPE>::TreeNode() {
    left = nullptr;
    right = nullptr;
}

template <class TYPE>
TreeNode<TYPE>::TreeNode(TYPE data) {
    left = nullptr;
    right = nullptr;
    this->data = data;
}

template <class TYPE>
bool TreeNode<TYPE>::isLeaf() {
    return left == nullptr && right == nullptr;
}

#endif
