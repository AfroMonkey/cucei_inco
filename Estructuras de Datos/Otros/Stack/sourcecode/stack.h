#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include "node.h"

/*------------------------------------------------------------------------------------------------*/
class StackException{
public:
    StackException(const std::string& msg) : msg_(msg) {}
    ~StackException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};
/*------------------------------------------------------------------------------------------------*/

template <typename TYPE>
class Stack {
private:    
    Node<TYPE> *stack_;
public:
    Stack();
    ~Stack();

    bool empty();
    unsigned int size();
    TYPE& top();
    void push(const TYPE& val);
    TYPE pop();
};

template <typename TYPE>
Stack<TYPE>::Stack() {
    stack_ = nullptr;
}

template <typename TYPE>
Stack<TYPE>::~Stack() {
    while(!empty()) {
        pop();
    }
}

template <typename TYPE>
bool Stack<TYPE>::empty() {
    return stack_ == nullptr;
}

template <typename TYPE>
unsigned int Stack<TYPE>::size() {
    unsigned int s;
    Node<TYPE> *aux;

    s = 0;
    aux = stack_;
    while(aux != nullptr) {
        s++;
        aux = aux->nexT;
    }

    return s;
}

template <typename TYPE>
TYPE& Stack<TYPE>::top() {
    if(!empty()) {
        return stack_->data;
    } else {
        throw StackException("Empty");
    }
}

template <typename TYPE>
void Stack<TYPE>::push(const TYPE& val) {
    Node<TYPE> *node;
    
    node = new Node<TYPE>(val);
    node->next = stack_;
    stack_ = node;
}

template <typename TYPE>
TYPE Stack<TYPE>::pop() {
    if(!empty()) {
        TYPE val;
        Node<TYPE> *aux;

        aux = stack_;
        val = aux->data;
        stack_ = stack_->next;
        delete aux;

        return val;
    } else {
        throw StackException("Empty");
    }
}
#endif
