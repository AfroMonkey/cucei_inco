/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica: 
 * Nombre de practica: Pila dinamica
 * Fecha: 2015/11/04
 * Version: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdlib>
#include "stack.h"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPC_PUSH 1
#define OPC_POP 2
#define OPC_SALIR 0

int main() {
    Stack<int> stack;
    int opc;

    opc = 0;
    do {
        try {
            cout << "Tope: " << stack.top() << endl;
        } catch(StackException &e) {
            cout << e.what() << endl;
        }
        cout << "1) Push" << endl;
        cout << "2) Pop" << endl;
        cout << "0) Salir" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();
        switch(opc) {
            case OPC_PUSH:
                int val;
                
                cout << "Val>";
                cin >> val;
                cin.ignore();
                stack.push(val);
                break;
            case OPC_POP:
                try {
                    stack.pop();
                } catch(StackException &e) {
                    cout << e.what() << endl;
                }
                break;
            case OPC_SALIR:
                cout << "Cerrando . . ." << endl;
                break;
            default:
                cout << "Opcion invalida" << endl;
                break;
        }
        cout << "Presione entrar para continuar . . .";
        cin.ignore();
        system(CLEAR);
    } while(opc != OPC_SALIR);

    return 0;
}
