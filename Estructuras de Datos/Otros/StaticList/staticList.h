#include <iostream>

#define DEFAULT_SIZE 50

class ListException{
public:
    ListException(const std::string& msg) : msg_(msg) {}
    ~ListException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};

template <class TYPE>
class StaticList {
public:
    StaticList();
    StaticList(int max);
    ~StaticList() {}

    bool isEmpty();
    bool isFilled();
    int first();
    int last();
    int previous(int position);
    int next(int position);
    void insert(int position, TYPE data);
    void remove(int position);
    TYPE get(int position);
    void sort(int (*relation)(TYPE, TYPE));
    int linearSearch(int (*relation)(TYPE, TYPE), TYPE searched);
    int binarySearch(int (*relation)(TYPE, TYPE), TYPE searched);
    std::string toString(std::string (*elementToString)(TYPE));
    void clear();
private:
    int max_;
    TYPE* list_;
    int end_;
};

template <class TYPE>
StaticList<TYPE>::StaticList() {
    max_ = DEFAULT_SIZE;
    list_ = new TYPE[max_];
    end_ = -1;
}

template <class TYPE>
StaticList<TYPE>::StaticList(int max) {
    max_ = max;
    list_ = new TYPE[max_];
    end_ = -1;
}

template <class TYPE>
bool StaticList<TYPE>::isEmpty() {
    return end_ == -1;
}

template <class TYPE>
bool StaticList<TYPE>::isFilled() {
    return end_ == max_ - 1;
}

template <class TYPE>
int StaticList<TYPE>::first() {
    if(!isEmpty()) {
        return 0;
    } else {
        return -1;
    }
}

template <class TYPE>
int StaticList<TYPE>::last() {
    return end_;
}

template <class TYPE>
int StaticList<TYPE>::previous(int position) {
    if(!isEmpty()) {
        if(position > 0 && position <= end_) {
            return position - 1;
        } else {
            throw ListException("Out of bounds");
        }
    } else {
        throw ListException("List empty");
    }    
}

template <class TYPE>
int StaticList<TYPE>::next(int position) {
    if(!isEmpty()) {
        if(position >= 0 && position < end_) {
            return position + 1;
        } else {
            throw ListException("Out of bounds");
        }
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
void StaticList<TYPE>::insert(int position, TYPE data) {
    if(!isFilled()) {
        if(position >= 0 && position <= end_ + 1) {
            for(int i = end_; i >= position; i--) {
                list_[i + 1] = list_[i];
            }
            list_[position] = data;
            end_++;
        } else {
            throw ListException("Out of bounds");            
        }
    } else {
        throw ListException("List filled");
    }
}

template <class TYPE>
void StaticList<TYPE>::remove(int position) {
    if(!isFilled()) {
        if(position >= 0 && position <= end_ + 1) {
            for(int i = position; i < end_; i++) {
                list_[i] = list_[i + 1];
            }
            end_--;
        } else {
            throw ListException("Out of bounds");            
        }
    } else {
        throw ListException("List filled");
    }
}

template <class TYPE>
TYPE StaticList<TYPE>::get(int position) {
    if(!isEmpty()) {
        if(position >= 0 && position <= end_) {
            return list_[position];
        } else {
            throw ListException("Out of bounds");            
        }
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
void StaticList<TYPE>::sort(int (*relation)(TYPE, TYPE)) {
    if(!isEmpty()) {
        TYPE aux;
        for(int i = 0; i < end_; i++) {
            for(int j = 0; j < end_ - i; j++) {
                if((*relation)(list_[j], list_[j+1]) > 0) {
                    aux = list_[j];
                    list_[j] = list_[j+1];
                    list_[j+1] = aux;
                }
            }
        }
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
int StaticList<TYPE>::linearSearch(int (*relation)(TYPE, TYPE), TYPE searched) {
    if(!isEmpty()) {
        for(int position = 0; position <= end_; position++) {
            if((*relation)(searched, list_[position]) == 0) {
                return position;
            }
        }
        return -1;
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
int StaticList<TYPE>::binarySearch(int (*relation)(TYPE, TYPE), TYPE searched) {
    if(!isEmpty()) {
        int start, end, middle;
        start = 0;
        end = last();
        do {
            middle = (start + end) / 2;
            if((*relation)(list_[middle], searched) == 0) {
                return middle;
            } else if((*relation)(list_[middle], searched) > 0) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
        } while(start <= end);
        return -1;
    } else {
        throw ListException("List empty");
    }
}

template <class TYPE>
std::string StaticList<TYPE>::toString(std::string (*elementToString)(TYPE)) {
    if(!isEmpty()) {
        std::string s = "";
        for(int position = 0; position <= end_; position++) {
            s += (*elementToString)(list_[position]);
        }
        return s;
    } else {
        return "";
    }
}

template <class TYPE>
void StaticList<TYPE>::clear() {
    end_ = -1;
}
