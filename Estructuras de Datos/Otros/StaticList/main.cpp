#include <sstream>
#include <iostream>
#include "staticList.h"

using namespace std;

bool search(int searched, int data);
int relation(int a, int b);
string elementToString(int element);

int main() {
    StaticList<int> list;
    int opc;
    do {
        cout << list.toString(elementToString) << endl;
        cout << "1) Insert" << endl;
        cout << "2) Remove" << endl;
        cout << "3) Sort" << endl;
        cout << "4) Linear search" << endl;
        cout << "5) Binary search" << endl;
        cout << "0) Exit" << endl;
        cin >> opc;
        switch(opc) {
            case 0: {
                cout << "Closing" << endl;
                break;
            }
            case 1: {
                int data;
                cin >> data;
                list.insert(list.last() + 1, data);
                break;
            }
            case 2: {
                int position;
                cin >> position;
                try {
                    list.remove(position);
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 3: {
                try {
                    list.sort(relation);
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 4: {
                int data;
                cin >> data;
                try {
                    cout << list.linearSearch(relation, data) << endl;
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 5: {
                int data;
                cin >> data;
                try {
                    list.sort(relation);
                    cout << list.binarySearch(relation, data) << endl;
                } catch(ListException e) {
                    cout << e.what() << endl;
                }
                break;                
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        cout << "X" << endl;
    } while(opc != -1);
    return 0;
}

int relation(int a, int b) {
    if(a > b) {
        return 1;
    } else if(a < b) {
        return -1;
    } else {
        return 0;
    }
}

string elementToString(int element) {
    ostringstream x;
    x << element;
    return x.str() + " ";
}