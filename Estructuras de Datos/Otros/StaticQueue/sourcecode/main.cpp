/*
 * Autor: Navarro Presas Moisés Alejandro
 * Número de práctica: 
 * Nombre de práctica: 
 * Fecha: 
 * Versión: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "staticQueue.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

using namespace std;

int main() {
    StaticQueue<char> queue(5);
    int opc;
    do {
        cout << "1) Enqueue" << endl;
        cout << "2) Deque" << endl;
        cout << "3) Get front" << endl;
        cout << "4) Get back" << endl;
        cout << "0) Exit" << endl;
        cin >> opc;
        switch(opc) {
            case 1: {
                char c;
                cout << "Char >";
                cin >> c;
                try {
                    cout << "Enqueue ..." << endl;
                    queue.enqueue(c);
                } catch(QueueException &e) {
                    cout << e.what() << endl;    
                }
                break;
            }
            case 2: {
                try {
                    cout << "Deque ..." << endl;
                    queue.deque();
                } catch(QueueException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 3: {
                try {
                    cout << "Getting front ..." << endl;
                    cout << queue.getFront() << endl;
                } catch (QueueException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 4: {
                try {
                    cout << "Getting back ..." << endl;
                    cout << queue.getBack() << endl;
                } catch (QueueException &e) {
                    cout << e.what() << endl;
                }
                break;
            }
            case 0: {
                cout << "Closing ..." << endl;
                break;
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        getchar();
        cout << "Press enter to continue . . . ";
        getchar();
        system(CLEAR);
    } while (opc != 0);
    return 0;
}