#include <stdio.h>

int main()
{
    char s[30];
    int i;
    printf(">");
    scanf("%s", s);
    for (i = 0; s[i] != '\x0'; i++)
    {
        s[i] += 7;
        if (s[i] > 'z')
        {
            s[i] -= ('z' - 'a');
            continue;
        }
        if (s[i] < 'a' && s[i] > 'Z')
            s[i] -= ('Z' - 'A');
    }
    printf("%s\n", s);
    return 0;
}


//Codigo Cesar = Jvkpnv Jlzhy
