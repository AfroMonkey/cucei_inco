#include <stdio.h>

int phi(int a);

int main() {
    int a;
    printf("a>");
    scanf("%d", &a);
    printf("phi(%d) = %d\n", a, phi(a));
    return 0;
}

int gcd(int a, int b) {
    int r = b;
    do {
        b = r;
        r = a % b;
        a = b;
    } while(r != 0);
    if(b < 0) {
        b*= -1;
    }
    return b;
}

int phi(int a) {
    int p = 0;
    int i;
    for(i = 1; i < a; i++) {
        if(gcd(a, i) == 1) {
            p++;
        }
    }
    return p;
}
