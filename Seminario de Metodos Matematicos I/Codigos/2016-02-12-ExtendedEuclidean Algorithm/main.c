#include <stdio.h>

int gcdExtended(int a, int b, int *x, int *y);

int main() {
    int a, b, x, y;

    printf("a>");
    scanf("%d", &a);
    printf("b>");
    scanf("%d", &b);

    int gcd = gcdExtended(a, b, &x, &y);
    printf("gcd(%d, %d) = %d = %d*%d + %d*%d\n", a, b, gcd, a, x, b, y);
    return 0;
}

int gcdExtended(int a, int b, int *x, int *y) {
    //Caso base, ya que si "a" == 0 significa que hemos llegado al final de algoritmo de Euclides
    if (a == 0) {
        *x = 0;
        *y = 1;
        return b;
    }

    int x1, y1; //Se utilizac como temporales
    int gcd = gcdExtended(b%a, a, &x1, &y1);

    *x = y1 - (b/a) * x1; //Se calcula el nuevo valor de x en base al paso anterior
    *y = x1; //y se convierte en x1

    return gcd;
}
