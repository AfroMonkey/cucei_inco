#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
int a, b, m, q, r, p0, p, tmp;
cout << "x: ";
cin >> b;
cout << "m: ";
cin >> m;
a = m;
p0 = 0;
p = 1;
if (b > a) {
b = b % m;
}
// leer valor a calcular inverso mod m
// leer m
// a inicia en m, se utiliza para mcd(a,b)
// valores de p en la iteracion 0 y 1, predefinidos
while (a % b)
 // mientras r > 0, algoritmo euclides no ha terminado
{
 // se va realizando el calculo de p, usando pi-2 (p0) y pi-1 (p)
q = a / b;
tmp = p0 - (p * q);
//cout << p0 << " - " << "(" << p << " * " << q << ")" << endl;
p0 = p;
p = tmp;
if (p >= 0) { // mantener p dentro del modulo
p = p % m;
}
else { // si el numero es negativo restar al modulo y aplicar modulo
p = m - (-p) % m;
}
r = a % b;
 // algoritmo de euclides
a = b;
b = r;
}
if (b == 1) {
// si mcd(a, b) = 1, entonces existe el inverso multiplicativo y esta encout << "p: " << p << endl;
cout << "p: " << p << endl;

}
else {
 // mcd(a, b) != 1, no existe el inverso multiplicativo
cout << "No existe el inverso multiplicativo para " << a << " modulo" << m << "." << endl;
}
}
