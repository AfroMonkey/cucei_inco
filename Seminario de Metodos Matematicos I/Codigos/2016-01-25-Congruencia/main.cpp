#include <iostream>

using namespace std;

int main() {
    int a, b, mod;

    cout << "a>";
    cin >> a;
    cout << "b>";
    cin >> b;
    cout << "mod>";
    cin >> mod;
    //"a" y "b" son los numeros a los cuales se les quiere comprobar la congruencia
    //"mod" es el modulo sobre el cual sera evaluada la congruencia

    if((a-b)%mod == 0) { //si la diferencia entre "a" y "b" es multiplo de "mod"
        //Significa que los numeros "a" y "b" son congruentes modulo "mod"
        cout << "Congruentes" << endl;
    }
    else {
        //De lo contrario no son congruentes
        cout << "No congruentes" << endl;
    }

    return 0;
}
