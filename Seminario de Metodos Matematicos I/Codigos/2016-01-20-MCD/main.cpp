/*
 * Author: Navarro Presas Moises Alejandro
 * Date: 2016-01-20
 * File: MCD
 */

#include <iostream>
#include <list>

using namespace std;

int main() {
    int a, b, r;

    cout << "a>";
    cin >> a;
    cout << "b>";
    cin >> b;
    //"a" y "b" son los numeros a lso cuales les queremos calcular su MCD
    r = b; //Esta asignacion es para poder opimizar el ciclo posterior
    do {
        b = r; //"b" es sustituido por el residuo
        r = a % b; //"r" es el residuo de dividir "a" entre "b"
        cout << a << "=" << b << "*" << (a/b) << "+" << r << endl; //Muetsra paso a paso el proceso
        a = b; //"a" se sustituye por "b"
    } while(r != 0); //Este proceso se repite hasta que no quede residuo alguo
    b = b>0 ? b : b*-1; //Si el resultado queda negativo; simplemente se le cambia de signo

    cout << "MCD=" << b << endl; //Al final se muestra el MCD de "a" y "b"

    return 0;
}
