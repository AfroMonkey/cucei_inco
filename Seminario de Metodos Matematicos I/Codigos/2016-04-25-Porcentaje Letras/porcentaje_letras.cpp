#include <iostream>
#include <fstream>

int main()
{
    std::string file;

    std::cout << "Ingrese el nombre del archivo a analizar>";
    std::cin >> file;
    std::ifstream database(file);
    if (!database) return 1;

    char c;
    int reps['Z'-'A'+1] = {0};
    int chars = 0;
    while (database.read((char*)&c, sizeof(char)))
    {
        if (c < 'A' || c > 'Z') continue;
        reps[c-'A']++;
        chars++;
    }

    for (int i = 0; i < 'Z'-'A'+1; i++)
    {
        std::cout << "Porcentaje de " << (char)('A'+i) << "'s: " << 100*reps[i]/(float)chars << std::endl;
    }
    
    return 0;
}
