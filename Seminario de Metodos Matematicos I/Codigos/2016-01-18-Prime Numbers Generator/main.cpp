/*
 * Author: Navarro Presas Moises Alejandro
 * Date: 2016-01-18
 * File: Prime Numbers Generator
 */

#include <iostream>
#include <list>

using namespace std;

int main() {
    unsigned int n;

    cin >> n;

    if(n > 0) {
        list<unsigned int> primes;
        list<unsigned int>::iterator it;
        unsigned int aux, nPrimes;
        bool isPrime;

        primes.push_back(2);
        aux = 3;
        nPrimes = 1;

        while(nPrimes < n) {
            isPrime = true;
            for(it = primes.begin(); it != primes.end(); ++it) {
                if(aux % *it == 0) {
                    isPrime = false;
                    break;
                }
                if(*it**it > aux) {
                    break;
                }
            }
            if(isPrime) {
                primes.push_back(aux);
                nPrimes++;
            }
            aux += 2;
        }

        for(it = primes.begin(); it != primes.end(); ++it) {
            cout << *it << endl;
        }
    }

    return 0;
}
