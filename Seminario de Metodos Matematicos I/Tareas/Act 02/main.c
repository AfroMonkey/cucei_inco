#include "stdio.h"

int main() {
    int a, b;
    int mod;
    while(1) {
        printf("a>");
        scanf("%d", &a);
        printf("b>");
        scanf("%d", &b);
        mod = a%b;
        if(mod < 0) {
            mod += b;
        }
        printf("a%b = %d\n", mod);
    }
}