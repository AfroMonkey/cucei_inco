clear all; close all hidden; clc;

num_frames = 59;
frame_rate = 15;

radio = 1;
escala = 1/25;
filtro_x = 5;
filtro_y = 5;

[imagen_lado_y, imagen_lado_x, profundidad] = size(im2double(aviread('video.avi', 1)));

centro_x = imagen_lado_x/2;
centro_y = imagen_lado_y/2;

%% Filtro Pasa Bajas
filtro = ones(filtro_x, filtro_y)*escala;

filtro_p_altas = [-1, -1, -1; -1, 8, -1; -1, -1, -1];


while true
  for i = 1 : num_frames;
    imagen = im2double(aviread('video.avi', i));
    imagen = (imagen(:, :, 1) + imagen(:, :, 2) + imagen(:, :, 3))/3;
    imagen_ruido = imagen + .1*randn(imagen_lado_y, imagen_lado_x);
    imagen_f_p_bajas = conv2(imagen_ruido, filtro, 'same');
    imagen_f_p_altas = conv2(imagen, filtro_p_altas, 'same');
%    pasa bajas con ruido
    figure(1);
    subplot(1, 2, 1);
    imshow(imagen_f_p_bajas);
    subplot(1, 2, 2);
    imshow(imagen + imagen_f_p_altas);
    pause(1/frame_rate);
  end
end