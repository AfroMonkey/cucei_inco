clear all;
close all;
clc;

pkg load video;

video_ruta = 'video.avi';
filtro_bajas_radio = 100;
filtro_altas_radio = 200;
ruido_porcentaje = 10;
contraste_porcentaje = 10;
frame_cantidad = 59;
frame_rate = 15;

video_original = aviread(video_ruta, 1);

[video_alto, video_ancho, profundidad] = size(video_original);

centro_x = video_ancho/2;
centro_y = video_alto/2;

filtro_p_bajas = zeros(video_alto, video_ancho);

for i = 0 : filtro_bajas_radio;
  for j = 0 : filtro_bajas_radio;
    d_x = (j - filtro_bajas_radio/2) / (filtro_bajas_radio/2);
    d_y = (i - filtro_bajas_radio/2) / (filtro_bajas_radio/2);
    filtro_p_bajas(i + (centro_y - filtro_bajas_radio/2), j + (centro_x - filtro_bajas_radio/2)) = cos(pi*d_x/2) * cos(pi*d_y/2);
  endfor
endfor

filtro_p_altas = ones(video_alto, video_ancho);

for i = 0 : filtro_altas_radio;
  for j = 0 : filtro_altas_radio;
    d_x = (j - filtro_altas_radio/2) / (filtro_altas_radio/2);
    d_y = (i - filtro_altas_radio/2) / (filtro_altas_radio/2);
    filtro_p_altas(i + (centro_y - filtro_altas_radio/2), j + (centro_x - filtro_altas_radio/2)) = 1 - (cos(pi*d_x/2) * cos(pi*d_y/2));
  endfor
endfor

filtro_p_bandas = filtro_p_bajas + filtro_p_altas;

ruido = ruido_porcentaje/100.0 * randn(video_alto, video_ancho);

figure(1);
mesh(filtro_p_bajas);
title('Filtro pasa bajas');

figure(2);
mesh(filtro_p_altas);
title('Filtro pasa altas');

figure(3);
mesh(filtro_p_bandas);
title('Filtro pasa bandas');

for i = 1 : 1;
  frame = im2double(aviread(video_ruta, i));

  frame_r = frame(:, :, 1);
  frame_g = frame(:, :, 2);
  frame_b = frame(:, :, 3);

  frame_ruido_r = frame_r + ruido;
  frame_ruido_g = frame_g + ruido;
  frame_ruido_b = frame_b + ruido;

  frame_r_frecuencia = fftshift(fft2(frame_r));
  frame_g_frecuencia = fftshift(fft2(frame_g));
  frame_b_frecuencia = fftshift(fft2(frame_b));

  frame_ruido_r_frecuencia = fftshift(fft2(frame_ruido_r));
  frame_ruido_g_frecuencia = fftshift(fft2(frame_ruido_g));
  frame_ruido_b_frecuencia = fftshift(fft2(frame_ruido_b));

  frame_r_f_p_bajas_frecuencia = frame_ruido_r_frecuencia.*filtro_p_bajas;
  frame_g_f_p_bajas_frecuencia = frame_ruido_g_frecuencia.*filtro_p_bajas;
  frame_b_f_p_bajas_frecuencia = frame_ruido_b_frecuencia.*filtro_p_bajas;

  frame_r_f_p_altas_frecuencia = frame_r_frecuencia.*filtro_p_altas;
  frame_g_f_p_altas_frecuencia = frame_g_frecuencia.*filtro_p_altas;
  frame_b_f_p_altas_frecuencia = frame_b_frecuencia.*filtro_p_altas;

  frame_r_f_p_bandas_frecuencia = frame_ruido_r_frecuencia.*filtro_p_bandas;
  frame_g_f_p_bandas_frecuencia = frame_ruido_g_frecuencia.*filtro_p_bandas;
  frame_b_f_p_bandas_frecuencia = frame_ruido_b_frecuencia.*filtro_p_bandas;

  frame_r_f_p_bajas = ifft2(ifftshift(frame_r_f_p_bajas_frecuencia));
  frame_g_f_p_bajas = ifft2(ifftshift(frame_g_f_p_bajas_frecuencia));
  frame_b_f_p_bajas = ifft2(ifftshift(frame_b_f_p_bajas_frecuencia));

  frame_r_f_p_altas = ifft2(ifftshift(frame_r_f_p_altas_frecuencia));
  frame_g_f_p_altas = ifft2(ifftshift(frame_g_f_p_altas_frecuencia));
  frame_b_f_p_altas = ifft2(ifftshift(frame_b_f_p_altas_frecuencia));

  frame_r_f_p_bandas = ifft2(ifftshift(frame_r_f_p_bandas_frecuencia));
  frame_g_f_p_bandas = ifft2(ifftshift(frame_g_f_p_bandas_frecuencia));
  frame_b_f_p_bandas = ifft2(ifftshift(frame_b_f_p_bandas_frecuencia));

  frame_f_p_bajas(:, :, 1) = frame_r_f_p_bajas;
  frame_f_p_bajas(:, :, 2) = frame_g_f_p_bajas;
  frame_f_p_bajas(:, :, 3) = frame_b_f_p_bajas;

  frame_f_p_altas(:, :, 1) = frame_r_f_p_altas;
  frame_f_p_altas(:, :, 2) = frame_g_f_p_altas;
  frame_f_p_altas(:, :, 3) = frame_b_f_p_altas;

  frame_f_p_bandas(:, :, 1) = frame_r_f_p_bandas;
  frame_f_p_bandas(:, :, 2) = frame_g_f_p_bandas;
  frame_f_p_bandas(:, :, 3) = frame_b_f_p_bandas;

  frame_ruido(:, :, 1) = frame_ruido_r;
  frame_ruido(:, :, 2) = frame_ruido_g;
  frame_ruido(:, :, 3) = frame_ruido_b;


  figure(4);
  imshow(frame);
  title('Frame original');

  figure(5);
  imshow(frame_ruido);
  title('Frame con ruido');

  figure(6);
  imshow(real(frame_f_p_bajas));
  title('Frame con ruido filtro pasa bajas');

  figure(7);
  imshow(real(frame + contraste_porcentaje/100.0*frame_f_p_altas));
  title('Frame original + filtro pasa altas');

  figure(8);
  imshow(real(frame + contraste_porcentaje/100.0*frame_f_p_bandas));
  title('Frame original + Frame con ruido filtro pasa bandas');

  pause(1/(15*frame_rate));
end
