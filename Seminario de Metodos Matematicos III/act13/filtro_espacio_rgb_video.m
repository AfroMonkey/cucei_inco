clear all;
close all;
clc;

pkg load video;

video_ruta = 'video.avi';
filtro_p_bajas = ones(3, 3)/9;
filtro_p_altas = [-1, -1, -1; -1, 8, -1; -1, -1, -1];
ruido_porcentaje = 10;
contraste_porcentaje = 10;
frame_cantidad = 59;
frame_rate = 15;

video_original = aviread(video_ruta, 1);

[video_alto, video_ancho, profundidad] = size(video_original);

ruido = ruido_porcentaje/100.0 * randn(video_alto, video_ancho);

for i = 1 : 1;
  frame = im2double(aviread(video_ruta, i));

  frame_r = frame(:, :, 1);
  frame_g = frame(:, :, 2);
  frame_b = frame(:, :, 3);

  frame_ruido_r = frame_r + ruido;
  frame_ruido_g = frame_g + ruido;
  frame_ruido_b = frame_b + ruido;

  frame_r_f_p_bajas = conv2(frame_ruido_r, filtro_p_bajas, 'same');
  frame_g_f_p_bajas = conv2(frame_ruido_g, filtro_p_bajas, 'same');
  frame_b_f_p_bajas = conv2(frame_ruido_b, filtro_p_bajas, 'same');

  frame_r_f_p_altas = conv2(frame_r, filtro_p_altas, 'same');
  frame_g_f_p_altas = conv2(frame_g, filtro_p_altas, 'same');
  frame_b_f_p_altas = conv2(frame_b, filtro_p_altas, 'same');

  frame_r_f_p_bandas = conv2(frame_r_f_p_bajas, filtro_p_altas, 'same');
  frame_g_f_p_bandas = conv2(frame_g_f_p_bajas, filtro_p_altas, 'same');
  frame_b_f_p_bandas = conv2(frame_b_f_p_bajas, filtro_p_altas, 'same');

  frame_f_p_bajas(:, :, 1) = frame_r_f_p_bajas;
  frame_f_p_bajas(:, :, 2) = frame_g_f_p_bajas;
  frame_f_p_bajas(:, :, 3) = frame_b_f_p_bajas;

  frame_f_p_altas(:, :, 1) = frame_r_f_p_altas;
  frame_f_p_altas(:, :, 2) = frame_g_f_p_altas;
  frame_f_p_altas(:, :, 3) = frame_b_f_p_altas;

  frame_f_p_bandas(:, :, 1) = frame_r_f_p_bandas;
  frame_f_p_bandas(:, :, 2) = frame_g_f_p_bandas;
  frame_f_p_bandas(:, :, 3) = frame_b_f_p_bandas;

  frame_ruido(:, :, 1) = frame_ruido_r;
  frame_ruido(:, :, 2) = frame_ruido_g;
  frame_ruido(:, :, 3) = frame_ruido_b;


  figure(1);
  imshow(frame);
  title('Frame original');

  figure(2);
  imshow(frame_ruido);
  title('Frame con ruido');

  figure(3);
  imshow(real(frame_f_p_bajas));
  title('Frame con ruido filtro pasa bajas');

  figure(4);
  imshow(real(frame + contraste_porcentaje/100.0*frame_f_p_altas));
  title('Frame original + filtro pasa altas');

  figure(5);
  imshow(real(frame + contraste_porcentaje/100.0*frame_f_p_bandas));
  title('Frame original + Frame con ruido filtro pasa bandas');

  pause(1/(15*frame_rate));
end
