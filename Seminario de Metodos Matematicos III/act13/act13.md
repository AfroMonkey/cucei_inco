# Actividad 12: Implementación de las estrategias de Fourier en Video
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-05-16


## Introducción
### Filtro
Un filtro es un sistema el cual pondera de acuerdo a su función de transferencia las diferentes componentes frecuenciales de una señal de entrada, pudiendo ası́ atenuar unas mientras deja pasar sin distorsión otras. Lo anterior puede ser expresado como:  
![](img/formula1)  
donde `X(f)` es la señal filtrada en el dominio de la frecuencia, `S(f)` es la transformada de Fourier de la señal de entrada y `H(f)` es la función de transferencia del filtro. En este sentido, se puede hacer una clasificación de los filtros de acuerdo a su función de transferencia siendo: filtros pasa baja, filtros pasa altas, filtros pasa banda y filtros rechaza banda.  
El filtro ideal pasa bajas se define como un sistema cuya función de transferencia esta dada por:  
![](img/formula2)  
donde `fc` es la frecuencia de corte del filtro.

### Convolución
En matemáticas y, en particular, análisis funcional, una convolución es un operador matemático que transforma dos funciones f y g en una tercera función que en cierto sentido representa la magnitud en la que se superponen f y una versión trasladada e invertida de g. Una convolución es un tipo muy general de media móvil, como se puede observar si una de las funciones se toma como la función característica de un intervalo.

La convolución de `f` y `g` se denota `f∗g`. Se define como la integral del producto de ambas funciones después de desplazar una de ellas una distancia `t`.  
![](img/conv)  

### Video
Un archivo de video no es más que una secuencia de imágenes junto con un audio.

### Imagen RGB
Un archivo de imagen RGB es una matriz tridimensional de 3 capas, donde cada capa representa un color (Rojo, Verde y Azul), cada una de estas capas es de la misma dimension (ancho y alto), y cada una de las celdas indica que tanto de ese color que hay en dicho pixel.

## Actividad
### Notas
*   En la actividad pasada se tuvo que comprimir el archivo PDF final debido a la cantidad de imágenes que incluía, por ello en esta actividad no se colocarán las imágenes del proceso intermedio (imagen en frecuencia o las distintas capas), puesto que al ojo humano no se logra apreciar nada de estas.

### Filtros en el dominio de la frecuencia.

#### Notas
*   La actividad está planeada para hacer uso de la cámara web, sin embargo no fue posible hacerlo así, por lo que se utilizará un video.
*   Las características del video (frame rate y total de frames fue obtenida mediante programas externos).

#### Variables iniciales
Lo primero es preparar el espacio de trabajo e inicializar unas variables de control, donde se establece la ruta del video a tratar, el radio de los filtros, el porcentaje de ruido que se le quiera agregar y un valor para el contraste en la suma de la imagen con el filtro. Además se requiere establecer la cantidad de frames que contiene el video y el frame rate del mismo.  
![](img/frec/iniciales)  

#### Cargar la imagen
Para cargar la imagen utilizamos el comando `aviread` indicándole el numero del frame que deseamos leer, el cual nos devuelve la imagen en formato de entero.  
![](img/frec/cargar)  

#### Obtener información de la imagen
Para procesos posteriores sera requerido saber el ancho y alto de la imagen, para ello podemos utilizar el comando `size`, el cual nos dará 3 componentes (alto, ancho y profundidad), aprovecharemos para calcular en centro de la imagen, el cual utilizaremos para crear el filtro.  
![](img/frec/info)  

.  
.  
#### Crear filtro
Para crear el filtro pasa bajas partimos de una matriz de ceros de las mismas dimensiones que la imagen, después, utilizando ciclos for, recorreremos cada celda perteneciente al radio del filtro, desde 0. Despues, en cada celda aplicaremos la formula ![](img/frec/ventana), donde ![](img/frec/donde) la cual creara un filtro más real y suavizado; se aplica el mismo procedimiento para el pasa altas, solo que inicialmente el filtro está compuesto de unos y a 1 le restamos el resultado para que ahora el filtro apunte hacia 0. Finalmente para el pasa bandas solo sumamos los filtros anteriores.  
![](img/frec/crear_filtro_p_bajas)  

![](img/frec/crear_filtro_p_altas)  

![](img/frec/crear_filtro_p_bandas)  

.  
#### Generar ruido
Para los filtros pasa bajas y pasa bandas es útil añadir ruido y así ver la diferencia, por lo que crearemos una matriz `ruido` la cual se le aplicará a todas las capas.  
![](img/frec/ruido)  

#### Mostrar filtros
Lo siguiente en el código es mostrar los filtro que se aplicarán, pero esto lo veremos en la sección de gráficas.  
![](img/frec/codigo_filtros)  

#### Leer frame
Como lo que se esta tratando es un video, es necesario filtrar frame e a frame, para ello utilizaremos un ciclo for que recorra cada uno de los frames del video utilizando la función `aviread`.  
![](img/frec/leer_frame)  

#### Separar capas RGB
Igual que en la practica anterior, es necesario separar las capas RGB de cada frame.  
![](img/frec/separar_rgb)  

#### Aplicar ruido
Ahora le añadiremos ruido a las capas.  
![](img/frec/agregar_ruido)  

#### Pasar la imagen al dominio de la frecuencia
Esto se realiza capa a capa, lo único que se requiere es utilizar el comando `fft2` (el 2 por que es una matriz bi-dimensional) y posteriormente re-ordenar el resultado con `fftshift`. Haremos lo mismo con la imagen con ruido.  
![](img/frec/fft2)  

![](img/frec/fft2_ruido)  


#### Aplicar el filtro
El mismo filtro se le aplica a las tres capas, para ello utilizamos el producto cruz matricial.  
![](img/frec/cruz_bajas)  

![](img/frec/cruz_altas)  

![](img/frec/cruz_bandas)  


.  
.  
.  
#### Regresar al dominio espacial
Ya que tenemos cada capa filtrada en frecuencia, hay que devolverla al dominio espacial, para ello utilizamos `ifftshift` el cual colocará las frecuencias en el orden en que Octave las maneja, ese resultado lo utilizamos con `ifft2`, el cual convierte del dominio frecuencial al espacial.  
![](img/frec/ifft2_bajas)  

![](img/frec/ifft2_altas)  

![](img/frec/ifft2_bandas)  



#### Unir las capas
Ya que tenemos cada capa filtrada de manera individual, es necesario volver a unir las tres capas en una sola matriz tridimensional.  
![](img/frec/unir_bajas)  

![](img/frec/unir_altas)  

![](img/frec/unir_bandas)  

.  
.  
.  
.  
#### Unir las capas con ruido
Ahora que tenemos las capas alteradas, generamos la imagen con ruido.  
![](img/frec/unir_ruido)  

#### Gráficar
Para gráficar utilizaremos la función `imshow`.  
![](img/frec/fig/codigos)  

Para que el video se reproduzca de manera adecuada, deberíamos de colocar una pausa equivalente al frame rate del video, sin embargo; debido a la cantidad de operaciones que realiza Octave, no es necesaria dicha pausa.

![](img/frec/pausa)  

.  
.  
.  
#### Gráficas
A continuación se muestran las gráficas de los filtros.  
![](img/frec/fig/1)  

![](img/frec/fig/2)  

![](img/frec/fig/3)  

Estos son los resultados

![](img/frec/fig/4)  

![](img/frec/fig/5)  

![](img/frec/fig/6)  

![](img/frec/fig/7)  

![](img/frec/fig/8)  


.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
### Filtros en el dominio espacial.
#### Notas
*   Para el filtro en espacio se utiliza la misma lógica que para el frecuencial, lo único que cambia es la creación de los filtros y como se aplican; por esto solo se mostrará dichas diferencias.

#### Variables iniciales
Además de lo visto los filtros frecuenciales, en esta ocasión se requiere establecer las matrices que harán de filtro. Recordar que para el pasa bandas es solamente aplicar el pasa bajas seguido del pasa altas.  
![](img/esp/filtros)  

#### Cargar la imagen
![](img/frec/cargar)  

#### Obtener información de la imagen
![](img/frec/info)  

#### Generar ruido
![](img/frec/ruido)  

.  
.  
#### Leer frame
![](img/frec/leer_frame)  

#### Separar capas RGB
![](img/frec/separar_rgb)  

#### Aplicar ruido
![](img/frec/agregar_ruido)  

#### Aplicar el filtro
El mismo filtro se le aplica a las tres capas, para ello utilizamos la técnica de convolución.  
![](img/esp/conv_bajas)  

![](img/esp/conv_altas)  

![](img/esp/conv_bandas)  


#### Unir las capas
![](img/frec/unir_bajas)  

![](img/frec/unir_altas)  

![](img/frec/unir_bandas)  


#### Unir las capas con ruido
![](img/frec/unir_ruido)  

#### Gráficar
![](img/frec/fig/codigos)  

Para que el video se reproduzca de manera adecuada, deberíamos de colocar una pausa equivalente al frame rate del video, sin embargo; debido a la cantidad de operaciones que realiza Octave, no es necesaria dicha pausa.

![](img/frec/pausa)  

.  
.  
.  
.  
#### Gráficas
Estos son los resultados

![](img/esp/fig/1)  

![](img/esp/fig/2)  

![](img/esp/fig/3)  

![](img/esp/fig/4)  

![](img/esp/fig/5)  


## Conclusión
El realizar un filtrado de cualquier tipo (altas, bajas, bandas) sobre un video no tiene mayor complicación que el hacerlo sobre una imagen, inclusive podríamos filtrar el audio del mismo. Lo único a tener en cuenta es que al tener que realizar multiples filtros en repetidas ocasiones, puede que nuestro equipo de computo no tenga la capacidad necesaria para que el resultado sea fluido, por lo que recomiendo que solo se realice un filtro a la vez e ir jugando con los parámetros de la función `pause` para lograr una semejanza con el archivo original.

## Referencias
*   Act12 Filtros ideales en imágenes.pdf
*   Act13 Archivos de video.pdf
*   Documentación interna de Octave
*   <http://mathworld.wolfram.com/Convolution.html>
*   <https://es.wikipedia.org/wiki/Convoluci%C3%B3n>
