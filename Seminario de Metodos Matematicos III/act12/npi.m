clear all;
close all;
clc;

imagen_ruta = "img.jpg";
radio = 1;
escala = 1/25;
filtro_x = 5;
filtro_y = 5;

imagen = imread(imagen_ruta);
imagen = im2double(imagen);
imagen = (imagen(:, :, 1) + imagen(:, :, 2) + imagen(:, :, 3))/3;
[imagen_lado_y, imagen_lado_x, profundidad] = size(imagen);
imagen_ruido = imagen + (10/100)*rand(imagen_lado_y, imagen_lado_x);

centro_x = imagen_lado_x/2;
centro_y = imagen_lado_y/2;

%% Filtro Pasa Bajas
filtro = ones(filtro_x, filtro_y)*escala;
imagen_f_p_bajas = conv2(imagen_ruido, filtro, 'same');

%% Filtro Pasa Altas
filtro_p_altas = [-1, -1, -1; -1, 8, -1; -1, -1, -1];
imagen_f_p_altas = conv2(imagen, filtro_p_altas, 'same');


%% Filtro Pasa Bandas
filtro_p_altas = [-1, -1, -1; -1, 9, -1; -1, -1, -1];
imagen_f_p_bandas = conv2(imagen, filtro, 'same');
imagen_f_p_bandas = conv2(imagen_f_p_bandas, filtro_p_altas, 'same');


figure(1)
mesh(filtro);
%imshow(filtro);

figure(2)
imshow(imagen);
%
%figure(3)
%imshow(imagen_f);
%
%figure(4)
%imshow(imagen_filtrada_f);
%
figure(5)
imshow(imagen_f_p_bajas);
%
figure(6)
imshow(imagen + 0.1*imagen_f_p_altas);
figure(7)
%
imshow(imagen_f_p_bandas);
%
%figure(6)
%imshow(imagen + imagen_filtrada);