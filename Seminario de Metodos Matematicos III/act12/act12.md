# Actividad 12: Implementación de las estrategias de Fourier en Imágenes
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-05-09


## Introducción
### Filtro
Un filtro es un sistema el cual pondera de acuerdo a su función de transferencia las diferentes componentes frecuenciales de una señal de entrada, pudiendo ası́ atenuar unas mientras deja pasar sin distorsión otras. Lo anterior puede ser expresado como:  
![](img/formula1)  
donde `X(f)` es la señal filtrada en el dominio de la frecuencia, `S(f)` es la transformada de Fourier de la señal de entrada y `H(f)` es la función de transferencia del filtro. En este sentido, se puede hacer una clasificación de los filtros de acuerdo a su función de transferencia siendo: filtros pasa baja, filtros pasa altas, filtros pasa banda y filtros rechaza banda.  
El filtro ideal pasa bajas se define como un sistema cuya función de transferencia esta dada por:  
![](img/formula2)  
donde `fc` es la frecuencia de corte del filtro.

### Convolución
En matemáticas y, en particular, análisis funcional, una convolución es un operador matemático que transforma dos funciones f y g en una tercera función que en cierto sentido representa la magnitud en la que se superponen f y una versión trasladada e invertida de g. Una convolución es un tipo muy general de media móvil, como se puede observar si una de las funciones se toma como la función característica de un intervalo.

La convolución de `f` y `g` se denota `f∗g`. Se define como la integral del producto de ambas funciones después de desplazar una de ellas una distancia `t`.  
![](img/conv)  

### Imagen RGB
Un archivo de imagen RGB es una matriz tridimensional de 3 capas, donde cada capa representa un color (Rojo, Verde y Azul), cada una de estas capas es de la misma dimension (ancho y alto), y cada una de las celdas indica que tanto de ese color que hay en dicho pixel.

## Actividad
### Filtros en el dominio de la frecuencia.
#### Notas
*   El código para generar los tres tipos de filtros (pasa bajas, altas y bandas) es el mismo, lo único que cambia es el radio de los círculos internos y externos.

#### Variables iniciales
Lo primero es preparar el espacio de trabajo e inicializar unas Variables de control, donde se establece la ruta de la imagen a tratar, el radio de la circunferencia interna y externa, el porcentaje de ruido que se le quiera agregar y un valor para el contraste en la suma de la imagen con el filtro.  
![](img/frec/iniciales)  

#### Cargar la imagen
Para cargar la imagen utilizamos el comando `imread`, el cual nos devuelve la imagen en formato de entero, con el comando `im2double` lo convertimos a valores decimales y así poder hacer más operaciones aritméticas.  
![](img/frec/cargar)  

#### Obtener información de la imagen
Para procesos posteriores sera requerido saber el ancho y alto de la imagen, para ello podemos utilizar el comando `size`, el cual nos dará 3 componentes (alto, ancho y profundidad), aprovecharemos para calcular en centro de la imagen, el cual utilizaremos para crear el filtro.  
![](img/frec/info)  

#### Separar las capas y añadir ruido
La imagen está compuesta por tres capas (roja, verde y azul), y los filtros solo pueden ser aplicados a una capa a la vez, así que procederemos a separa la imagen en sus componentes RGB. Aprovecharemos para añadir ruido a las capas.  
![](img/frec/separar)  

#### Unir las capas con ruido
Ahora que tenemos las capas alteradas, generamos la imagen con ruido.  
![](img/frec/unir_ruido)  

<!-- TODO No aplica para No Ideales -->
#### Crear filtro
Para crear el filtro partimos de una matriz de ceros de las mismas dimensiones que la imagen, después, utilizando ciclos for, recorremos cada celda de la matriz, si dicha celda se encuentra a una distancia mayor o igual al radio que deseamos para el circulo interior y además se encuentra a una distancia menor o igual al radio que se desea para el circulo externo, dicha celda se le establece el valor de 1.  
![](img/frec/crear_filtro)  

#### Pasar la imagen al dominio de la frecuencia
Esto se realiza capa a capa, lo único que se requiere es utilizar el comando `fft2` (el 2 por que es una matriz bi-dimensional) y posteriormente re-ordenar el resultado con `fftshift`.  
![](img/frec/fft2)  

#### Aplicar el filtro
El mismo filtro se le aplica a las tres capas, para ello utilizamos el producto cruz matricial.  
![](img/frec/cruz)  

#### Regresar al dominio espacial
Ya que tenemos cada capa filtrada en frecuencia, hay que devolverla al dominio espacial, para ello utilizamos `ifftshift` el cual colocará las frecuencias en el orden en que Octave las maneja, ese resultado lo utilizamos con `ifft2`, el cual convierte del dominio frecuencial al espacial.  
![](img/frec/ifft2)  

.  
#### Unir las capas
Ya que tenemos cada capa filtrada de manera individual, es necesario volver a unir las tres capas en una sola matriz tridimensional.  
![](img/frec/unir)  

#### Gráficar
Para gráficar utilizaremos la función `imshow` para la mayoría de los casos y `mesh` para mostrar el filtro.  
![](img/frec/codigo_graficas_genericas)  

![](img/frec/codigo_graficas_filtro)  

#### Gráficas
A continuación se muestran las gráficas que aplican para todos los filtros.  
![](img/frec/fig/1)  

![](img/frec/fig/2)  

![](img/frec/fig/4)  

.  
.  
.  
.  
##### Filtro Pasa Bajas (`radio_int=0, radio_ext=100, ruido_porcentaje=10`)
![](img/frec/fig/bajas/3)  

![](img/frec/fig/bajas/5)  

![](img/frec/fig/bajas/6)  

.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
##### Filtro Pasa Altas (`radio_int=100, radio_ext=10000, contraste_porcentaje=10`)
![](img/frec/fig/altas/3)  

![](img/frec/fig/altas/5)  

![](img/frec/fig/altas/6)  

![](img/frec/fig/altas/7)  

.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
##### Filtro Pasa Bandas (`radio_int=200, radio_ext=500, ruido_porcentaje=10, contraste_porcentaje=10`)
![](img/frec/fig/bandas/3)  

![](img/frec/fig/bandas/5)  

![](img/frec/fig/bandas/6)  

![](img/frec/fig/bandas/7)  

.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
### Filtros en el dominio espacial.
#### Notas
*   El código para generar los los filtros pasa bajas, altas es el mismo, lo único que cambia es filtro como tal, para generar el filtro pasa bandas lo que se hará será primero filtrar con pasa bajas (atenuar el ruido) y posteriormente utilizar el filtro pasa altas para acentuar los bordes.
*   Gran parte del código es el mismo que para el filtro en frecuencia, así que se obviara en algunas circunstancias.

#### Variables iniciales
Lo primero es preparar el espacio de trabajo e inicializar unas Variables de control, donde se establece la ruta de la imagen a tratar, las matrices para los filtros pasa bajas y pasa altas, el porcentaje de ruido que se desee añadir y el porcentaje de contraste.  
![](img/esp/iniciales)  

#### Cargar la imagen
Para cargar la imagen utilizamos el comando `imread`, el cual nos devuelve la imagen en formato de entero, con el comando `im2double` lo convertimos a valores decimales y así poder hacer más operaciones aritméticas.  
![](img/esp/cargar)  

#### Obtener información de la imagen
Para procesos posteriores sera requerido saber el ancho y alto de la imagen, para ello podemos utilizar el comando `size`, el cual nos dará 3 componentes (alto, ancho y profundidad).  
![](img/esp/info)  

#### Separar las capas
La imagen está compuesta por tres capas (roja, verde y azul), y los filtros solo pueden ser aplicados a una capa a la vez, así que procederemos a separa la imagen en sus componentes RGB.  
![](img/esp/separar)  

#### Generar imagen con ruido
A las capas originales les añadiremos ruido, ya que tenemos las capas con ruido, las unimos para generar nuevamente una imagen RGB.  
![](img/esp/unir_ruido)

#### Aplicar el filtro
Aplicamos la convolución en dos dimensiones a cada capa, y esto aplica para cada filtro, con la excepción de que para el filtro pasa bandas tomaremos como imagen de entrada la que provea el filtro pasa bajas y le aplicaremos a está el filtro pasa altas. Otra cosa a tener en cuenta es que para el filtro pasa bajas (y por ende el pasa bandas) la imagen de entrada tendrá ruido.  
![](img/esp/conv_bajas)  

![](img/esp/conv_altas)  

![](img/esp/conv_bandas)  

#### Unir las capas
Ya que tenemos cada capa filtrada de manera individual, es necesario volver a unir las tres capas en una sola matriz tridimensional.  
![](img/esp/unir_bajas)  

![](img/esp/unir_altas)  

![](img/esp/unir_bandas)  

.  
.  
.  
.  
.  
.  
.  
.  
#### Gráficar
Para gráficar utilizaremos la función `imshow` para la mayoría de los casos y `disp` para mostrar los filtros.  
![](img/esp/cg)  

![](img/esp/cg_bajas)  

![](img/esp/cg_altas)  

![](img/esp/cg_bandas)  

.  
.  
.  
.  
.  
.  
.  
.  
#### Gráficas
A continuación se muestran las gráficas que aplican para todos los filtros.  
![](img/esp/fig/1)  

![](img/esp/fig/2)  

![](img/esp/disp/1)  

![](img/esp/disp/2)  


.  
.  
.  
.  
.  
.  
##### Filtro Pasa Bajas (`radio_int=0, radio_ext=100, ruido_porcentaje=10`)
![](img/esp/fig/bajas/3)  

![](img/esp/fig/bajas/4)  

.  
.  
.  
.  
.  
.  
.  
##### Filtro Pasa Altas (`radio_int=100, radio_ext=10000, contraste_porcentaje=10`)
![](img/esp/fig/altas/5)  

![](img/esp/fig/altas/6)  

![](img/esp/fig/altas/7)  

.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
##### Filtro Pasa Bandas (`radio_int=200, radio_ext=500, ruido_porcentaje=10, contraste_porcentaje=10`)
![](img/esp/fig/bandas/8)  

![](img/esp/fig/bandas/9)  

![](img/esp/fig/bandas/10)  


## Conclusión
Como podemos ver en las gráficas, los filtros pasa bajas ayudan a "suavizar" los bordes de los objetos así como atenuar el ruido que pueda contener la imagen; por otro lado, los filtros pasa altas ayudan a crear contraste entre los distintos objetos de la imagen, pero si se hace esto sobre una imagen con ruido los resultados pueden no ser óptimos; y por ultimo, utilizando el filtro pasa bandas sobre una imagen con ruido podemos reducir el ruido para posteriormente realzar los bordes. Otra cosa que noté fue que, al menos en el dominio frecuencial, podemos hacer un algoritmo para filtro pasa bandas y dependiendo de los limites que establezcamos se comportará como cualquiera de los tres que hemos manejado.

## Referencias
*   Act12 Filtros ideales en imágenes.pdf
*   Documentación interna de Octave
*   <http://mathworld.wolfram.com/Convolution.html>
*   <https://es.wikipedia.org/wiki/Convoluci%C3%B3n>
