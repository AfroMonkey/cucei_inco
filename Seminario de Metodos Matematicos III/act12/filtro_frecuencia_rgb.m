clear all;
close all;
clc;

imagen_ruta = "img.jpg";
% NOTA: El radio interior debe ser menor que el exterior
filtro_radio_int = 100; % 0 = filtro pasa bajas
filtro_radio_ext = 10000; % Max radio en la imagen = filtro pasa altas
ruido_porcentaje = 10;
contraste_porcentaje = 10;

imagen_original = im2double(imread(imagen_ruta));

[imagen_alto, imagen_ancho, profundidad] = size(imagen_original);
centro_x = imagen_ancho/2;
centro_y = imagen_alto/2;

imagen_r = imagen_original(:, :, 1) + (ruido_porcentaje/100.0 * randn(imagen_alto, imagen_ancho));
imagen_g = imagen_original(:, :, 2) + (ruido_porcentaje/100.0 * randn(imagen_alto, imagen_ancho));
imagen_b = imagen_original(:, :, 3) + (ruido_porcentaje/100.0 * randn(imagen_alto, imagen_ancho));

imagen_ruido(:, :, 1) = imagen_r;
imagen_ruido(:, :, 2) = imagen_g;
imagen_ruido(:, :, 3) = imagen_b;

filtro_p_bandas = zeros(imagen_alto, imagen_ancho);

for i = 1 : imagen_alto;
  for j = 1 : imagen_ancho;
    if ((i - centro_y)^2 + (j - centro_x)^2 >= filtro_radio_int^2) && ((i - centro_y)^2 + (j - centro_x)^2 <= filtro_radio_ext^2);
      filtro_p_bandas(i, j) = 1;
    endif
  endfor
endfor

imagen_r_frecuencia = fftshift(fft2(imagen_r));
imagen_g_frecuencia = fftshift(fft2(imagen_g));
imagen_b_frecuencia = fftshift(fft2(imagen_b));

imagen_r_filtrada_frecuencia = imagen_r_frecuencia.*filtro_p_bandas;
imagen_g_filtrada_frecuencia = imagen_g_frecuencia.*filtro_p_bandas;
imagen_b_filtrada_frecuencia = imagen_b_frecuencia.*filtro_p_bandas;

imagen_r_filtrada = ifft2(ifftshift(imagen_r_filtrada_frecuencia));
imagen_g_filtrada = ifft2(ifftshift(imagen_g_filtrada_frecuencia));
imagen_b_filtrada = ifft2(ifftshift(imagen_b_filtrada_frecuencia));

imagen_filtrada(:, :, 1) = imagen_r_filtrada;
imagen_filtrada(:, :, 2) = imagen_g_filtrada;
imagen_filtrada(:, :, 3) = imagen_b_filtrada;

figure(1);
imshow(imagen_original);
title("Imagen Original");

figure(2);
imshow(real(imagen_ruido));
title("Imagen con Ruido");

figure(3);
mesh(filtro_p_bandas);
title(["Filtro desde ", int2str(filtro_radio_int), " hasta ", int2str(filtro_radio_ext)]);

figure(4);
subplot(2, 3, 1);
imshow(real(imagen_r_frecuencia));
title("Capa R en frecuencia (real)");

subplot(2, 3, 2);
imshow(real(imagen_g_frecuencia));
title("Capa G en frecuencia (real)");

subplot(2, 3, 3);
imshow(real(imagen_b_frecuencia));
title("Capa B en frecuencia (real)");

subplot(2, 3, 4);
imshow(imag(imagen_r_frecuencia));
title("Capa R en frecuencia (imaginario)");

subplot(2, 3, 5);
imshow(imag(imagen_g_frecuencia));
title("Capa G en frecuencia (imaginario)");

subplot(2, 3, 6);
imshow(imag(imagen_b_frecuencia));
title("Capa B en frecuencia (imaginario)");

figure(5);
subplot(1, 3, 1);
imshow(real(imagen_r_filtrada));
title("Capa R filtrada");

subplot(1, 3, 2);
imshow(real(imagen_g_filtrada));
title("Capa G filtrada");

subplot(1, 3, 3);
imshow(real(imagen_b_filtrada));
title("Capa B filtrada");

figure(6);
imshow(real(imagen_filtrada));
title("Imagen Filtrada");

figure(7);
imshow(real(imagen_original + (contraste_porcentaje/100.0)*imagen_filtrada));
title("Imagen Original + Imagen Filtrada");
