clear all;
close all;
clc;

imagen_ruta = 'img.jpg';
filtro_p_bajas = ones(5, 5)/25;
filtro_p_altas = [-1, -1, -1; -1, 8, -1; -1, -1, -1];
ruido_porcentaje = 10;
contraste_porcentaje = 10;

imagen_original = im2double(imread(imagen_ruta));

[imagen_alto, imagen_ancho, profundidad] = size(imagen_original);

imagen_r = imagen_original(:, :, 1);
imagen_g = imagen_original(:, :, 2);
imagen_b = imagen_original(:, :, 3);

imagen_ruido_r = imagen_r + (ruido_porcentaje/100.0 * randn(imagen_alto, imagen_ancho));
imagen_ruido_g = imagen_g + (ruido_porcentaje/100.0 * randn(imagen_alto, imagen_ancho));
imagen_ruido_b = imagen_b + (ruido_porcentaje/100.0 * randn(imagen_alto, imagen_ancho));

imagen_ruido(:, :, 1) = imagen_ruido_r;
imagen_ruido(:, :, 2) = imagen_ruido_g;
imagen_ruido(:, :, 3) = imagen_ruido_b;

imagen_r_f_p_bajas = conv2(imagen_ruido_r, filtro_p_bajas, 'same');
imagen_g_f_p_bajas = conv2(imagen_ruido_g, filtro_p_bajas, 'same');
imagen_b_f_p_bajas = conv2(imagen_ruido_b, filtro_p_bajas, 'same');

imagen_r_f_p_altas = conv2(imagen_r, filtro_p_altas, 'same');
imagen_g_f_p_altas = conv2(imagen_g, filtro_p_altas, 'same');
imagen_b_f_p_altas = conv2(imagen_b, filtro_p_altas, 'same');

imagen_r_f_p_bandas = conv2(imagen_r_f_p_bajas, filtro_p_altas, 'same');
imagen_g_f_p_bandas = conv2(imagen_g_f_p_bajas, filtro_p_altas, 'same');
imagen_b_f_p_bandas = conv2(imagen_b_f_p_bajas, filtro_p_altas, 'same');

imagen_f_p_bajas(:, :, 1) = imagen_r_f_p_bajas;
imagen_f_p_bajas(:, :, 2) = imagen_g_f_p_bajas;
imagen_f_p_bajas(:, :, 3) = imagen_b_f_p_bajas;

imagen_f_p_altas(:, :, 1) = imagen_r_f_p_altas;
imagen_f_p_altas(:, :, 2) = imagen_g_f_p_altas;
imagen_f_p_altas(:, :, 3) = imagen_b_f_p_altas;

imagen_f_p_bandas(:, :, 1) = imagen_r_f_p_bandas;
imagen_f_p_bandas(:, :, 2) = imagen_g_f_p_bandas;
imagen_f_p_bandas(:, :, 3) = imagen_b_f_p_bandas;

figure(1);
imshow(imagen_original);
title('Imagen Original');

figure(2);
imshow(imagen_ruido);
title('Imagen con Ruido');

disp('Filtro pasa bajas');
disp(filtro_p_bajas);

disp('Filtro pasa altas');
disp(filtro_p_altas);

figure(3);
subplot(1, 3, 1);
imshow(imagen_r_f_p_bajas);
title('Capa R filtrada con pasa bajas');

subplot(1, 3, 2);
imshow(imagen_g_f_p_bajas);
title('Capa G filtrada con pasa bajas');

subplot(1, 3, 3);
imshow(imagen_b_f_p_bajas);
title('Capa B filtrada con pasa bajas');

figure(4);
imshow(imagen_f_p_bajas);
title('Imagen Filtrada con pasa bajas');

figure(5);
subplot(1, 3, 1);
imshow(imagen_r_f_p_altas);
title('Capa R filtrada con pasa altas');

subplot(1, 3, 2);
imshow(imagen_g_f_p_altas);
title('Capa G filtrada con pasa altas');

subplot(1, 3, 3);
imshow(imagen_b_f_p_altas);
title('Capa B filtrada con pasa altas');

figure(6);
imshow(imagen_f_p_altas);
title('Imagen Filtrada con pasa altas');

figure(7);
imshow(imagen_original + (contraste_porcentaje/100.0)*imagen_f_p_altas);
title('Imagen Original + Imagen Filtrada con pasa altas');


figure(8);
subplot(1, 3, 1);
imshow(imagen_r_f_p_bandas);
title('Capa R filtrada con pasa bandas');

subplot(1, 3, 2);
imshow(imagen_g_f_p_bandas);
title('Capa G filtrada con pasa bandas');

subplot(1, 3, 3);
imshow(imagen_b_f_p_bandas);
title('Capa B filtrada con pasa bandas');

figure(9);
imshow(imagen_f_p_bandas);
title('Imagen Filtrada con pasa bandas');

figure(10);
imshow(imagen_original + (contraste_porcentaje/100.0)*imagen_f_p_bandas);
title('Imagen Original + Imagen Filtrada con pasa bandas');
