clear all;
close all;
clc;

imagen_ruta = "img.jpg";
filtro_p_bajas_radio = 100;
filtro_p_altas_radio = 100;
filtro_p_bandas_radio_int = 150;
filtro_p_bandas_radio_ext = 300;

imagen_enteros = imread(imagen_ruta);
imagen = im2double(imagen_enteros);
imagen_grises = (imagen(:, :, 1) + imagen(:, :, 2) + imagen(:, :, 3))/3;
[imagen_lado_y, imagen_lado_x, profundidad] = size(imagen);

centro_x = imagen_lado_x/2;
centro_y = imagen_lado_y/2;

filtro_p_bajas = zeros(imagen_lado_y, imagen_lado_x);

for i = 1 : imagen_lado_y;
  for j = 1 : imagen_lado_x;
    if (i - centro_y)^2 + (j - centro_x)^2 <= filtro_p_bajas_radio^2
      filtro_p_bajas(i, j) = 1;
    endif
  endfor
endfor

filtro_p_altas = ones(imagen_lado_y, imagen_lado_x);

for i = 1 : imagen_lado_y;
  for j = 1 : imagen_lado_x;
    if (i - centro_y)^2 + (j - centro_x)^2 > filtro_p_altas_radio^2;
      filtro_p_altas(i, j) = 0;
    endif
  endfor
endfor

filtro_p_bandas = zeros(imagen_lado_y, imagen_lado_x);

for i = 1 : imagen_lado_y;
  for j = 1 : imagen_lado_x;
    if (i - centro_y)^2 + (j - centro_x)^2 > filtro_p_bandas_radio_int^2 and (i - centro_y)^2 + (j - centro_x)^2 <= filtro_p_bandas_radio_ext^2;
      filtro_p_bandas(i, j) = 1;
    endif
  endfor
endfor

imagen_f = fftshift(fft2(imagen_grises));

imagen_f_p_bajas_f = imagen_f.*filtro_p_bajas;
imagen_f_p_bajas = ifft2(ifftshift(imagen_f_p_bajas_f));

imagen_f_p_altas_f = imagen_f.*filtro_p_altas;
imagen_f_p_altas = ifft2(ifftshift(imagen_f_p_altas_f));

imagen_f_p_bandas_f = imagen_f.*filtro_p_bandas;
imagen_f_p_bandas = ifft2(ifftshift(imagen_f_p_bandas_f));

figure(1);
imshow(imagen_grises);

figure(2);
imshow(imagen_f);

figure(3);
mesh(filtro_p_bajas);

figure(4);
imshow(imagen_f_p_bajas_f);

figure(5);
imshow(imagen_f_p_bajas);

figure(6);
mesh(filtro_p_altas);

figure(7);
imshow(imagen_f_p_altas_f);

figure(8);
imshow(imagen_f_p_altas);

figure(9);
imshow(imagen_grises + imagen_f_p_altas);

figure(10);
imshow(imagen_f_p_bandas_f);

figure(11);
imshow(imagen_f_p_bandas);
