clear all;
close all;
clc;

imagen_ruta = "img.jpg";
radio = 1;

imagen = imread(imagen_ruta);
imagen = im2double(imagen);
imagen = (imagen(:, :, 1) + imagen(:, :, 2) + imagen(:, :, 3))/3;
[imagen_lado_y, imagen_lado_x, profundidad] = size(imagen);

centro_x = imagen_lado_x/2;
centro_y = imagen_lado_y/2;

filtro = zeros(imagen_lado_y, imagen_lado_x);

for i = 1 : imagen_lado_y;
  for j = 1 : imagen_lado_x;
    if (i - centro_y)^2 + (j - centro_x)^2 > radio^2
      filtro(i, j) = 1;
    endif
  endfor
endfor

imagen_f = fftshift(fft2(imagen));
imagen_filtrada_f = imagen_f.*filtro;
imagen_filtrada = ifft2(ifftshift(imagen_filtrada_f));

figure(1);
mesh(filtro);

figure(2);
imshow(imagen);

figure(3);
imshow(imagen_f);

figure(4);
imshow(imagen_filtrada_f);

figure(5);
imshow(imagen_filtrada);

figure(6);
imshow(imagen + imagen_filtrada);