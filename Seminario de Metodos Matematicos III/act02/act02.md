# Actividad 01 Jugando con el Software
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-02-07

## Indice
*   [Introducción](#Introducción)
*   [Actividad](#Actividad)
*   [Sección 1](#1)
    *   [Ejercicio 1-a](#1a)
    *   [Ejercicio 1-b](#1b)
    *   [Ejercicio 1-c](#1c)
    *   [Ejercicio 1-d](#1d)
    *   [Ejercicio 1-e](#1e)
*   [Sección 2](#2)
    *   [Ejercicio 2-a](#2a)
    *   [Ejercicio 2-b](#2b)
    *   [Ejercicio 2-c](#2c)
    *   [Ejercicio 2-d](#2d)
    *   [Ejercicio 2-e](#2e)
*   [Sección 3](#3)
    *   [Ejercicio 3-a](#3a)
    *   [Ejercicio 3-b](#3b)
    *   [Ejercicio 3-c](#3c)
    *   [Ejercicio 3-d](#3d)
    *   [Ejercicio 3-e](#3e)
*   [Sección 4](#4)
    *   [Ejercicio 4-a](#4a)
    *   [Ejercicio 4-b](#4b)
    *   [Ejercicio 4-c](#4c)
    *   [Ejercicio 4-d](#4d)
    *   [Ejercicio 4-e](#4e)
*   [Conclusión](#Conclusión)
*   [Referencias](#Referencias)

## Introducción
Una transformación integral es una regla de asignación (mapeo) entre dosfunciones f (t) y F (s), dada por  
![](images/transformada)  
En donde K8s, t) es el kernel de la transformada que queramos hacer y f(t) es la función a transforma. Recordar que las transformadas se utilizan para llevar una función a otra dimensión en la cual podamos manipularla mas fácilmente, y generalmente se hace una transformada inversa para volver a llevar esa función a su plano original.
A continuación se muestra una tablas con los distintos kernel para distintas transformaciones  
![](images/kernels)  
Observar que en la transformada de Laplace de dos hojas se utiliza el infinito negativo y positivo como limites, sin embargo esto suele provocar que la transformada sea no convergente y es por eso que es común utilizar la transformada finita de Laplace.


## Actividad
### 1
Calcular la transformada de Laplace para cada función.
#### 1a
Utilizaremos la integral antes descrita en la introducción, donde sustituiremos k(s, t) por el kernel de la transformada de Laplace. En la función original se utiliza la función escalón (u), la cual recibe 't' como parámetro  y a partir de ese momento la función toma el valor de 1 y 0 para cualquier momento anterior a este. Como le estamos mandando como parámetro 't-4', al hacer el despeje tenemos que lo la función escalón "iniciará" en t=4, es decir, tomará el valor de 1 para todo momento mayor o igual a 4 y 0 para un tiempo menor a 4; por lo tanto podemos establecer el valor de la función a transformar como "1" y simplemente establecer los limites de integración desde 4 hasta infinito.  
![](images/1a)  
Podemos ver que al momento de ejecutar el comando nos pregunta si 's' es positiva, negativa o cero; a lo que respondemos que es positiva, esto para que al evaluarla en el kernel con una 't' cercana a infinito, el valor del mismo tienda a '0' y de está manera lograr la convergencia.

#### 1b
![](images/1b)  
Nuevamente nos pregunta la naturaleza de 's' , a lo que volvemos a responder que es positiva, por la misma explicación que se da en el punto anterior; esto sucederá en repetidas ocasiones.

#### 1c
Además de utilizar la forma general para las transformaciones, podemos hacer uso de una función nativa en Maxima para hacer transformadas de Laplace, pero haciéndolo de manera "manual" se pueden ajustar mas los parámetros  
![](images/1c)  

#### 1d
En este punto la función a transformar está compuesta por 3 funciones escalón, lo que se procede a hacer es calcular que valores toma esa función desde 't' mayor o igual a 0 (que es donde comienza atener un valor distinto de 0 en este caso). las funciones escalón dan como resultado 1.25 desde el tiempo 0 hasta 3, 0.25 desde 3 hasta 7 y 2.25 desde 7 hasta el infinito. Después implemente realizamos tres integrales (una por cada función) y finalmente las unimos utilizando las propiedades de las integrales (Podemos separar una gran integral en varias mas pequeñas)  
![](images/1d)  

#### 1e
![](images/1e)  

### 2
Obtener la transformada inversa de Laplace para cada función.
#### 2a
Para obtener la transformada inversa de Laplace podemos hacer uso de la función "ilt" (inverse Laplace transformation), la cual requiere la función a transformar, el dominio actual y el dominio deseado.  
![](images/2a)  

#### 2b
![](images/2b)  

#### 2c
![](images/2c)  

#### 2d
![](images/2d)  

#### 2e
En este caso, a Maxima no le es posible obtener la transformada inversa de Laplace.  
![](images/2e)  

### 3
Calcular X(z) para cada función.
#### 3a
La transformada Z se realiza en tiempo discreto, es por eso que en lugar de utilizar integrales utilizaremos sumatorias, pero funciona de manera muy similar a como lo hacen las transformadas integrales. Utilizaremos el kernel propio de la transformada Z.  
En este punto en concreto se utiliza la función delata o impulso ('d'), la cual genera un valor '1' en u momento en concreto (t=0), y 0 para cualquier otro momento. En este caso los valores de las 3 funciones que requerimos son: 11/10 para t=0, -1 en t=2 y 1/2 en t=4, así que procedemos a la creación de las funciones y establecer los limites de las mismas.  
![](images/3a)  

#### 3b
En este ejercicio originalmente se requiere de 3 tramos, sin embargo, utilizando la lógica podemos simplificarlo en 2 tramos. Además le indicaremos a Maxima que la sumatoria la simplifique, pues de otro modo nos arrojará un valor en términos de una sumatoria.  
![](images/3b)  

#### 3c
En esta ocasión surge algo similar al punto 3a, solo que en lugar de ser una funcion impulso es la función escalón.  
![](images/3c)  

#### 3d
Recordando lo mencionado en el punto 3b acerca de las sumatorias, Maxima puede realizar está simplificacción solo en un nivel, sin embargo la simplificación de esta requiere mas niveles de profundidad y es por eso que la muestra de manera simbólica.  
![](images/3d)  

#### 3e
![](images/3e)  

### 4
Calcular x(n) para cada función.
#### 4a
En este punto de la actividad simplemente simplificamos las ecuaciones dadas, ya que no hay una manera sencilla de realizar la transformada inversa Z; asi podríamos revisar tablas donde para obtener la anti-transformada.  
![](images/4a)  

#### 4b
![](images/4b)  

#### 4c
![](images/4c)  

#### 4d
![](images/4d)  

#### 4e
Posiblemente haya un error en la redacción de este punto, pues me parece curioso que los últimos 2 términos del denominador sean términos independientes.
![](images/4e)  

## Conclusión
La realización de transformadas apoyándonos en Maxima es muy sencillo, sin embargo habrá ocasiones en que el problema sea mas complejo de lo que Maxima puede manejar; en esos caso será necesario recurrir a otros medios. Otra cosa a tener en cuenta es las características de la función a transformar, pues de estás depende los limites que utilizaremos para que la integral sea convergente y nos sea útil el resultado.

## Referencias
*   Act02 Transformada de Laplace.pdf (provisto en clase)
*   Documentación interna de Maxima
