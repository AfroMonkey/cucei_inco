# Actividad 01 Jugando con el Software
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-02-07

## Introducción
En está actividad estaremos realizando distintos ejercicios para familizarizarnos con el software "Maxima" y "Matlab"/"Octave".

## Maxima
Maxima es una herramienta de software que ayuda en los procesos de resolución de problemas de álgebra.  

La creación de matrices en maxima se hace mediante la istrucción "matrix", la cual recibe las distintas filas cada una de ellas en forma de lista (con corchetes). A continuación creamos las 3 matrices apra el ejercicio A.  
![](images/ma)  
![](images/mb)  
![](images/mc)  

Para obtener el determinante de una matriz se utiliza el comando "determinant". Recordar que el determinante nos indica varias propiedades de la matriz, por ejemplo si es invertible o no (no lo es si el determinante es 0).  
![](images/1a)  

Para obtener la transpuesta (cambiar filas por columnas y viceversa) de una matriz se utiliza el comando "transpose".  
![](images/1b)  

Se pueden realizar operaciones como sustracción (-) y multiplicación por escalares (*) de una manera muy sencilla.  
![](images/1c)  

Para obtener el producto punto se utiliza el operador '.'.  
![](images/1d)  

Y para obtener la inversa de una matriz se usa el comando "invert". Recordar que no todas las matrices pueden ser invertidas.  
![](images/1e)  

Maxima nos puede ayudar también en la resolución de ecuaciones. Cuando tenemos multiples ecuaciones las ingresamos en forma de matriz y al obtener el determinante de esa matriz obtenemos una única ecuanción, la cual puede ser resuelta por el comando "solve". Para comprobar que los resultados arrojados son correctos utilizamos el comando "ev", el cual evaluá una ecuación sustituyendo la variable por el valor que le indiquemos.
![](images/2a)  
![](images/2b)  

También podemos comprobar los resultados de "solve" graficando las ecuaciones y comprobando que en los valores de x indicados por "solve", en la gráfica estén con y=0. Para visualizar una gráfica se usa el comando "wxplot2d" o "wxplot3d" dependiendo de la dimensión en la que estemos trabajando.  
![](images/2c)  

Cuando trabajamos con varias funciones podemos obtener su intersección en un plano más.
![](images/2d)  

En ocasiones contamos con mas incógnitas de las que podemos resolver (al menos con un valor numérico), en estos casos podemos dejar una variable en función de otras.  
![](images/2e)  

Cuando una ecuación es muy "complicada" podemos utilizar distintas técnicas para simplificarlas, una de ellas es el uso de fracciones parciales. Para utilizar esto se usa el comando "partfrac". Recordar que para poder realizar esto es necesario que el grado del polinomio del numerador no puede ser menor que el del denominador.  
![](images/3a)  

Otra de las técnicas para mostrar de manera distinta una misma ecuación es mediante la expansión de sus términos, podemos utilizar el comando "expand" para esto.  
![](images/3b)  
![](images/3c)  
![](images/3d)  
![](images/3e)  
![](images/3f)  
![](images/3g)  

Para obtener las raíces reales de una ecuación lo podemos hacer o bien con "solve" y descartar aquellas que contengan números imaginarios, o bien utilizando "realroots" el cual hace esto por nosotros.  
![](images/4)  

En el punto número 5 se solicita la representación gráfica de una serie de funciones, las cuales están compuestas por diversos tramos, para lograr realizarlo de manera sencilla se utilizan las sentencias "if" y "else" las cuales nos ayudaran a delimitar los distintos tramos y los comportamientos que debe tener la función en ellos.  
![](images/5a)  

También podemos gráficar listas de elementos que creamos "al vuelo", utilizando la función "create_list", la cual recibe como parámetros los valores de la columna izquierda (los evaluandos) y una función para generar los valores de la derecha, ademas de un rango.  
![](images/5b)  
![](images/5c)  

Otra de las facilidades que ofrece Maxima es el poder gráficar varias figuras en la misma imágen, esto es útil sobre todo al momento de realizar comparaciones.  
![](images/5d)  
![](images/5e)  
![](images/5f)  
![](images/5g)  

En algunas ocasiones, si no establecemos limites a lo que queremos que se gráfique, puede que el resultado no nos sea útil, para ello podemos establecer limites de manera manual y así asegurarnos de solo ver la zona que nos interesa. Solo hay que tener en cuenta que esto trunca la gráfica y puede que la perspectiva completa no sea lo que estamos viendo.  
![](images/5h)  

La siguiente imágen representa una cinta Moebius.  
![](images/5i)  

Y aquí vemos una figura tridimensional donde se puede apreciar un efecto similar a la cinta Moebius.  
![](images/5j)  


## Octave
Octave es similar a Matlab, con algunas pequeñas diferencias, sin embargo posee un poder equiparable en cuanto al manejo de matrices.  

En este ejercicio se trabajara con imágenes, en especifico con un modelo dental.  
![](ModeloDental.jpg).

Lo primero es cargar (imread) la imagen en una variable (matriz).  
![](images/6b)

Importante: La imagen debe estar en la misma ubicación que el directorio actual de octave.
![](images/dir)

Para mostrar la imagen se utiliza el comando "imshow".  
![](images/6c)

Y se desplegará una nueva ventana con la imágen.  
![](images/6c2)

En el workspace podemos ver las variables que tenemos en memoria, así como algunas de sus características. Por ejemplo, aquí vemos que la matriz es de 3 dimensiones, 2 para las coordenadas de los pixeles y una tercera para los distintos valores de rojo, verde y azul en los mismos.  
![](images/6d)

Lamentablemente Octave no tiene la misma cantidad de funciones "nativas" que tiene Matlab, sin embargo, con un poco de ingenio pueden solventarse estos problemas, como la falta de una función que convierta una imágen a escala de grises. Para ello podemos simplemente promediar sus valores en RGB y listo.
![](images/6e)

Y el resultado es este:  
![](images/6f)

También podemos realizar operaciones aritméticas directo sobre la "imagen", recordar que no es mas que una matriz, por ejemplo, si multiplicamos el valor de todos sus pixeles por 3, obtendremos una saturación, puesto que los valores no pueden sobre pasar los 255.  
![](images/6g)  
![](images/6g2)  

Y si la dividimos veremos que los colores se oscurecen.  
![](images/6h)  
![](images/6h2)  

Podemos modificar directamente el valor de algún pixel, aun que con la resolución de está imagen no se alcanza a apreciar.  
![](images/6i)

Y de manera muy intuitiva podemos obtener el valor de los pixeles. Vemos que nos devuelve un vector con los colores de RGB.  
![](images/6j)  
![](images/6j2)  

Otra de las funciones que no posee Octave es "imadjust" la cual aplica una especie de "filtros", sin embargo, volviendo a aplicar el ingenio podemos obtener la imagen con los colores inversos, solo que para lograrlo lo que haremos será manejar la imágen con números flotantes, en lugar de la estructura RGB, de está manera los valores de los pixeles van desde 0 hasta 1, así que para invertir simplemente tomamos el valor máximo (1) y se lo restamos al valor original.  
![](images/6k)  
![](images/6k2)  


## Conclusión
Aun que ya había usado Maxima en otros momentos, el tema de las gráficas era algo en lo que no tenia mucha práctica y en este ejerció desarrolle más eso, también recordé la importancia de conocer el orden los operadores para evitar o poner parentesís donde se requiere. En cuanto a Octave, es un software que no había utilizado, pero si tengo un poco de experiencia en Matlab, por lo que no me fue difícil acostumbrarme a Octave, aprendí que en este se pueden manejar las imágenes, algo que solo había hecho con lenguajes como C; sin embargo se aplica una lógica similar.

## Referencias
*   Act01 Jugando con el software.pdf (Documento provisto por el profesor)
*   Documentación interna de Maxima
*   Documentación interna de Octave
