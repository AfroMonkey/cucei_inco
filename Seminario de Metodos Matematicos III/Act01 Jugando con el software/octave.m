# 6b
I = imread('ModeloDental.jpg')

# 6c
imshow(I);

# 6d ver workspace%

# 6e
Id = im2double(I); # Conversión a flotante en lugar de rgb
G = [Id(:, :, 1) + Id(:, :, 2) + Id(:, :, 3)]/3; ## Promediando

# 6f
%imshow(G);

# 6g
imshow(I*3);

# 6h
imshow(I/3);

# 6i
G(700, 500) = 70;

# 6j
pixel = I(700, 500, :)

# 6k
imshow(1-Id);
