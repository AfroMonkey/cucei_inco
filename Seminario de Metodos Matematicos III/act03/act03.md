# Actividad 03 Transformada de Mellin y de Fourier
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-02-14


## Introducción
La transformada de Mellin  suele utilizarse en teoría de números y teorías de series asintóticas. Recordar que para aplicar la transformada lo que se debe realizar simplemente es la integral (o sumatoria) de la función a transformar multiplicada por el kernel de dicha transofmración, en este caso, el la formula para realizar transformadas de Mellin es:  
![](images/mellin)  
y para obtener la anti transformada se utiliza:  
![](images/antimellin)  

La transformada de Fourier es utilizada para transformar señales originalmente del dominio del tiempo al dominio de la frecuencia, esto se suele realizar para el tratamiento de señales físicas y el diseño de controladores en las mismas.
Para relaizar la transformada de Fourier se utiliza la siguiente formula:  
![](images/fourier)

## Actividad
1.  Apoyándose en el software, calcule la transformada Mellin para cada función  
![](images/1a)  
En este ejercicio nos pregunta la naturaleza de "a-1", a lo que respondemos que es positivo, esto para que en t=inf, al estar en el argumento de la exponencial se tienda a 0 y sea convergente, sucede algo similar en el caso de la 's'. Aun así, maxima no logro resolver la integral.  
![](images/1b)  
Para esta transformada no nos dieron los limites, así que asumimos que iría desde 0 hasta inf, pero como podemos observar, al evaluar la exponencial con t=inf esta se vuelve divergente.  
![](images/1c)  
Al realziar este ejercicio, Maxima nos arroja una función en términos de otra función (La función Gamma)
![](images/1d)  
![](images/1e)  
En esta ocasión, nuevamente no le es posible a Maxima realizar la intergal.

2.  Apoyándose en el software, calcule la anti transformada o la transformada de Fourier según sea el caso.  
![](images/2a)  
Nota: Utilizar el "parametro" '%i', puesto que en ocasiones cambia el resultado dependiendo de si Maxima lo interpreta o no como numero imaginario.
Para ver su dominio en el tiempo simplemente graficamos la función antes de la transformada  
![](images/2at)  
Y para observar su dominio en la frecuencia graficamos la función ya transformada. En ambos casos establecemos a=1.  
![](images/2af)  
![](images/2b)  
Lo complicado de este ejercicio es obtener los limites, para lo cual hay que despejar el argumento de la función escalón (u), en donde tenemos que 1 - abs(x)/1 > 0, de ahi nos queda que -abs(x)/a > -1, al multiplicar ambos lados de la desigualdad por 'a' nos queda: -abs(x) > -a, o lo que es lo mismo: abs(x) < a. Finalmente obtenemos los limites -a < x < a.  
Para graficar el dominio del tiempo asumí que hubo un error en la creación del documento, por lo que cambie el argumento 'x' por 't'.  
![](images/2bt)  
![](images/2c)  
![](images/2d)  
Como ya ha sucedido en otras ocasiones, nos encontramos con una integral divergente.
![](images/2e)  
![](images/2f)  
Maxima interpreta que la integral desde 'a' hasta 'a' es igual a 0, puesto que no hubo cambio en el eje, por lo que obtenemos el valor '0' como resultado, lo idela sería que nos diera algún resultado con términos de seno, pero no es posible mediante esta herramienta.
![](images/2g)  
![](images/2h)  
![](images/2i)  
![](images/2j)  

## Conclusión
El realizar transformadas, sobre todo de Fourier, nos puede ayudar a resolver algún problema que en su dominio original (por ejemplo tiempo), pueden ser más dificles de resolver, es por eso que les cambiamos su dominio para poder manipularlas, lamentablemente no siempre se puede realizar fácilmente este cambio, aun utilizado software matemático para ello; tal es el caso de Maxima al intentar resolver una integral cuyos limites superior e inferior son el mismo.

## Referencias
*   Act03 Transformadas de Mellin y Fourier.pdf
*   Documentación interna Maxima
*   <http://algo.inria.fr/flajolet/Publications/mellin-harm.pdf>
