# Actividad 05 Señales Electrofisiológicas
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-03-02


## Introducción
### Definiciones
#### Señal
Función matemática que depende de una o más variables independientes, normalmente a través del tiempo; y cuyo valor nos da información acerca de algún fenómeno.

#### Señal electrofisiológica
Señal obtenida al analizar las propiedades eléctricas de células y tejidos biológicos. Las variables comúnmente utilizadas son los cambios de voltaje o de corriente.

#### Electrocardiograma (ECG)
Es la representación gráfica de la actividad eléctrica del corazón. Se obtiene mediante la colocación de electrodos en puntos del cuerpo que faciliten el acceso a estas señales. El resultado es una señal donde el eje Y es la amplitud de la señal (en micro volts) y el eje X es el tiempo.

#### Electroencefalograma (EEG)
Es la representación gráfica de la actividad eléctrica del cerebro, en la cual podemos ver varios tipos de ondas como: Delta, Tetha, Alfa y Beta. El resultado son múltiples señales (una por cada electrodo colocado) donde el eje Y es la amplitud de la señal (en micro volts) y el eje X es el tiempo.

#### Electromiograma (EMG)
Es la representación gráfica de la actividad eléctrica del corazón. Se obtiene mediante la colocación de electrodos en 3 puntos, uno al inicio del musculo a analizar, otra en su parte central y un ultimo electrodo que sirve de referencia o "tierra". El resultado es una señal donde el eje Y es la amplitud de la señal (en micro volts) y el eje X es el tiempo.

#### Señal mono-dimensional
Señales que dependen de una sola variable

#### Señal mutli-dimensional
Señales que dependen de múltiples variables

### Actividad
#### 1. Generar una señal mono-dimensional “A” a partir de una función senoidal de frecuencia f (un vector).  
Lo primero que haremos es preparar nuestro espacio de trabajo (limpiar las variables y cerrar ventanas).
![](images/1_1)  
Para este ejercicio requerimos establecer la frecuencia de la señal (en este caso será de 1 Hercio), un vector 'tiempo' de 10 segundos (con 1001 momentos en los que tomaremos muestra de la señal) y ω (ω = 2πf).  
![](images/1_2)  
Posteriormente generamos la señal senoidal.  
![](images/1_3)  
Para graficar la señal usaremos el siguiente código, el cual incluye un titulo, etiquetas para los ejes X,Y y activa la cuadriculo. Este código se reutilizará para las siguientes gráficas, donde el vector del tiempo para las primeras 5 es el mismo (de 0 a 10 con pasos de 0.01), y para las siguientes gráficas se indicará explícitamente su vector de tiempo.  
![](images/graf)  
Y esta es la señal obtenida; una señal senoidal de amplitud 1 con una frecuencia de 1 Hercio en 10 segundos (por lo tanto 10 ciclos).  
![](images/1)  

#### 2. Multiplicar A por un escalar (aumentar y disminuir amplitud).  
La señal original la multiplicamos por un escalar (en este caso Ampl=10) que determinará la amplitud de la señal.  
![](images/2_1)  
![](images/2_2)  
La señal resultante es similar a la anterior, pero su amplitud ahora es de 10 unidades.  
![](images/2)  

#### 3. Desfasar la señal A.  
Para desfasar la señal le sumamos el desfase a la creación de la señal, en este caso la desfasaremos 1/4 de ciclo (θ = 2π/4), por lo que la señal comenzará en pico.  
![](images/3_1)  
![](images/3_2)  
Nota: Si el desfase es múltiplo de 2π, no se notará, pues es el ciclo completo.  
![](images/3)  

#### 4. Generar una señal de ruido “R” de la misma longitud que A, usando la función randn implementada en Matlab.  
Ahora generaremos una señal con valores "aleatorios", del mismo tamaño que la señal original.  
![](images/4_1)  
![](images/4)  
.  
#### 5. Obtener la señal “AR1” a partir de la suma de A y R.  
Ahora, a la señal A3 (la señal amplificada y desfasada) le sumaremos cada uno de los valores aleatorios, simulando el ruido en las señales físicas.  
![](images/5_1)  
Nota: El ruido debería tener una magnitud de máximo 10% con respecto a la señal portadora esto para poder fiarnos de la señal portadora.  
![](images/5)  

#### 6. Generar la señal “AR2” a partir de la concatenación de las señales A y R.  
Para este ejericio vamos a unir la señal AR1 con la señal R, pero lo haremos una detrás de la otra; por lo que requerimos un vector tiempo del doble de tamaño. Para concatenar los valores simplemente los ponemos en modo de lista (utilizando corchetes) y los separamos mediante una coma (,).  
![](images/6_2)  
![](images/6)  

#### 7. Simular una señal de ECG de 2s a partir de la función ecg implementada en Matlab.  
Nota: Matlab cuenta con una función que simula una señal generada por un ECG, sin embargo Octave no cuenta con ella de manera nativa, es por eso que se requiere obtenerla desde Matlab (exportando dicha función).  
Está función genera una señal similar al ECG con tantas muestras como le indiquemos, para esta actividad utilizaremos 500 muestras.  
Como se requiere que el tiempo de dicha señal sea de 2 segundos, lo que haremos será a un vector tiempo de 500 elementos (numero de muestras) y un paso de 1 segundo, lo multiplicaremos por 2/500 (tiempo deseado/muestras) para así hacer que cada uno de sus elementos sea 2/500 veces el valor original.  
![](images/7_1)  
![](images/7)  
 
#### 8. Concatenar 5 complejos de ECG.  
Lo ultimo que haremos será tomar la señal anterior y concatenarla 5 veces (utilizando la misma técnica que en el ejercicio 6). Deberemos generar un nuevo vector de tiempo que contenga 5 veces la cantidad de elementos que en el punto anterior, como no indica nada respecto al tiempo, también lo pondremos de 5 veces el tiempo anterior, es decir, 10 segundos.  
![](images/8_1)  
![](images/8)  

## Conclusión
El manejo de estas señales fue bastante sencillo, pues lo podemos ver todo como vectores, los cuales para ser graficados han de tener la misma cantidad de elementos que su respectivo vector de tiempo, la parte mas compleja fue adaptar la señal de ECG para que se ajuste a los 2 segundo requeridos, pero despues de analizarlo con detalle, no es complicado.

## Referencias
*  http://enrique.sanchez.webs.uvigo.es/PDFs/125_TemaI-Senales.pdf
*  https://es.wikipedia.org/wiki/Electrocardiograma
*  https://es.wikipedia.org/wiki/Electroencefalografía
*  https://es.wikipedia.org/wiki/Electromiografía
