% Limpieza de memoria y cierre de ventanas
clear all;
clear hidden;
clc;

% Declaracion de variables
f = 1;
t = 0:0.01:10;
omega = 2*pi*f;
Amp = 10;
theta = 2*pi/4;

A1 = sin(omega*t); % Señal original

A2 = Amp*sin(omega*t); % Señal amplificada

A3 = Amp*sin(omega*t + theta); % Señal desfasada

R = rand(1, length(A3)); % Señal aleatoria

AR1 = A3 + R; % Señal desfasada con ruido

AR2 = [A3, R]; % Señal desfasada concatenada con ruido
t2 = 0:0.01:20.01; % Vector tiempo de 2002 elementos

ecgm = 500; % Cantidad de muestras para ECG
ECG1 = ecg(ecgm); % Señal ECG de ecgm datos
t3 = (1:1:ecgm)*(2/ecgm); % Vector tiempo de ecgm elementos en 2 segundos

ECG2 = [ecg(ecgm), ecg(ecgm), ecg(ecgm), ecg(ecgm), ecg(ecgm)]; % 5 ECG1 concatenados
t4 = (1:1:ecgm*5)*(2/ecgm);  % Vector tiempo de ecgm*5 elementos en 10 segundos


% Transformadas de Fourier (FFT)
fs = 100; % Frecuencia de muestreo

A1_FFT = fftshift(fft(A1)); % FFT de señal A1
eje_f = -fs/2:fs/1000:fs/2; % Eje de frecuencia de 1001 elementos

A2_FFT = fftshift(fft(A2)); % FFT de señal A2

A3_FFT = fftshift(fft(A3)); % FFT de señal A3

R_FFT = fftshift(fft(R)); % FFT de señal R

AR1_FFT = fftshift(fft(AR1)); % FFT de señal AR1

AR2_FFT = fftshift(fft(AR2)); % FFT de señal AR2
eje2_f = -fs/2:fs/2001:fs/2; % Eje de frecuencia de 2002 elementos

ECG1_FFT = fftshift(fft(ECG1)); % FFT de señal ECG1
eje3_f = -fs/2:fs/499:fs/2; % Eje de frecuencia de 500 elementos

ECG2_FFT = fftshift(fft(ECG2)); % FFT de señal ECG2
eje4_f = -fs/2:fs/2499:fs/2; % Eje de frecuencia de 2500 elementos

% Graficas
figure(1);
subplot(3, 1, 1);
plot(t, A1);
title('A1: Señal Original');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje_f, real(A1_FFT));
title('A1 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje_f, imag(A1_FFT));
title('A1 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(2);
subplot(3, 1, 1);
plot(t, A2);
title('A2: Señal amplificada');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje_f, real(A2_FFT));
title('A2 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje_f, imag(A2_FFT));
title('A2 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(3);
subplot(3, 1, 1);
plot(t, A3);
title('A3: Señal desfasada');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje_f, real(A3_FFT));
title('A3 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje_f, imag(A3_FFT));
title('A3 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(4);
subplot(3, 1, 1);
plot(t, R);
title('R: Señal aleatoria (ruido)');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje_f, real(R_FFT));
title('R FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje_f, imag(R_FFT));
title('R FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(5);
subplot(3, 1, 1);
plot(t, AR1);
title('AR1: Señal A3 con ruido');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje_f, real(AR1_FFT));
title('AR1 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje_f, imag(AR1_FFT));
title('AR1 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(6);
subplot(3, 1, 1);
plot(t2, AR2);
title('AR2: Señal A3 con ruido concatenado');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje2_f, real(AR2_FFT));
title('AR2 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje2_f, imag(AR2_FFT));
title('AR2 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(7);
subplot(3, 1, 1);
plot(t3, ECG1);
title('ECG1: 1 ciclo de ECG');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje3_f, real(ECG1_FFT));
title('ECG1 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje3_f, imag(ECG1_FFT));
title('ECG1 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;

figure(8);
subplot(3, 1, 1);
plot(t4, ECG2);
title('ECG2: Señal 5 ciclos de ECG');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 2);
plot(eje4_f, real(ECG2_FFT));
title('ECG2 FFT: parte real');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
subplot(3, 1, 3);
plot(eje4_f, imag(ECG2_FFT));
title('ECG2 FFT: parte imaginaria');
xlabel('frecuencia');
ylabel('amplitud');
axis tight;
grid on;
