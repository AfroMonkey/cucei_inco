# Práctica 3 Señales Electrofisiológicas Simuladas
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-03-07


## Introducción
### Definiciones
#### Transformada de Fourier
La transformada de Fourier, es una transformación matemática empleada para transformar señales entre el dominio del tiempo y el dominio de la frecuencia y tiene muchas aplicaciones en la física y la ingeniería. Es reversible, siendo capaz de transformarse en cualquiera de los dominios al otro. El propio término se refiere tanto a la operación de transformación como a la función que produce.

En el caso de una función periódica en el tiempo, la transformada de Fourier se puede simplificar para el cálculo de un conjunto discreto de amplitudes complejas, llamado coeficientes de las series de Fourier. Ellos representan el espectro de frecuencia de la señal del dominio-tiempo original.

## Actividad
** Introducir la trasformada de Fourier en señales electrofisiológicas sintéticas.**  
Utilizaremos las 8 señales generadas en la practica pasada, para esto utilizaremos el siguiente código explicado anteriormente:  
![](img3/prev)  
*Esto generará las señales a las cuales les vamos a aplicar la transformada rápida de Fourier (FFT).*

Para realizar la transformada utilizaremos el comando ```fft```, el cual realiza justo eso; sin embargo Octave lo hace de tal modo que las frecuencias mas bajas quedan en los extremos, y para esta ocasion requerimos que estas frecuencias se encuentren en el centro; por lo que utilizaremos el comando ```fftshift```, el cual "intercambia" las mitades de la señal.

Para realizar la transformada de Fourier de la señal A1 utilizaremos el siguiente comando:  
![](img3/a1)  
utilizaremos exactamente la misma sintaxis para las demás trasnformaciones  
![](img3/a2)  
![](img3/a3)  
![](img3/r)  
![](img3/ar1)  
![](img3/ar2)  
![](img3/ecg1)  
![](img3/ecg2)  
Con esto hemos obtenido la FFT de cada una de las señales (de una manera muy sencilla).

Para realizar las graficas tendremos que generar distintos vectores de frecuencias (de la misma cantidad de elementos que la señal a graficar), además estableceremos una frecuencia ```fs = 100``` la cual indica la frecuencia de muestreo.  
![](img3/fs)  
Con base a esta frecuencia de muestreo generaremos 4 vectores de frecuencia (uno para cada conjunto de señales de distintos numeros de elementos).  
![](img3/e1)  
![](img3/e2)  
![](img3/e3)  
![](img3/e4)

Utilizando estos vectores generaremos las graficas de las señales.  
![](img3/ga1)  
En donde se mostrarán 3 graficas por figura, la primera es la señal original, la segunda es la parte real de transformada de dicha señal y la utima es la parte imaginaria de la FFT. Se repetirá esta estructura para las 8 señales.


### Graficas
<!-- #### A1 -->
![](img3/1)  
---
<!-- #### A2 -->
![](img3/2)  
---
<!-- #### A3 -->
![](img3/3)  
---
<!-- #### R -->
![](img3/4)  
---
<!-- #### AR1 -->
![](img3/5)  
---
<!-- #### AR2 -->
![](img3/6)  
---
<!-- #### ECG1 -->
![](img3/7)  
---
<!-- #### ECG2 -->
![](img3/8)  
---

## Conclusión
El realizar la transformada de Fouerier de manera "convencional" (es decir no FFT), puede darnos un poco más de fiabilidad, pero eso implica un gran esfuerzo para la computadora, y para fines practicos se utiliza la transformada rapida en sustitución; solo debemos recordar que dependiendo el algoritmo que se emeplee para esto, puede que nos de la señal de forma "invertida" (con las frecuencias bajas en los extremos).  
Una de las funcionalidades que le encuentro a realizar la transformada de una señal (sobre todo si es una señal con ruido), es que podemos identificar las frecuencias que este afectando a nuestra señal portadora.


## Referencias
*   Act04 Series de Fourier y FFT.pdf
* Act06 Senales electrofisiologicas simuladas.pdf
* https://es.wikipedia.org/wiki/Transformada_de_Fourier
