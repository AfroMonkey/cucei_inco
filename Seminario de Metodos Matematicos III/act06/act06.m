clear all;
clear hidden;
clc;

Amp = 10;
f = 1;
theta = pi*2;
t = 0:0.01:10;
omega = 2*pi*f;

A1 = sin(omega*t);

A2 = Amp*sin(omega*t);

A3 = Amp*sin(omega*t + theta);

R = rand(1, length(A1));

AR1 = A3 + R;

AR2 = [A3, R];
t2 = 0:0.01:20.01;

ecgm = 500;
ECG1 = ecg(ecgm);
t3 = (1:1:ecgm)/(ecgm/2);

ECG2 = [ecg(ecgm), ecg(ecgm), ecg(ecgm), ecg(ecgm), ecg(ecgm)];
t4 = (1:1:ecgm*5)/(ecgm/2);


figure(1); % TODO poner chida la grafica
subplot(4, 1, 1);
plot(t, A1);
subplot(4, 1, 2);
plot(t, A2);
subplot(4, 1, 3);
plot(t, A3);
subplot(4, 1, 4);
plot(t, R);

figure(2); % TODO poner chida la grafica
subplot(4, 1, 1);
plot(t, AR1);
subplot(4, 1, 2);
plot(t2, AR2);
subplot(4, 1, 3);
plot(t3, ECG1);
subplot(4, 1, 4);
plot(t4, ECG2);