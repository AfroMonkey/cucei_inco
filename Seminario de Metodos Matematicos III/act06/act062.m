% Limpieza de memoria y cierre de ventanas
clear all;
clear hidden;
clc;

% Declaracion de variables
f = 1;
t = 0:0.01:10;
omega = 2*pi*f;
Amp = 10;
theta = 2*pi/4;

A1 = sin(omega*t); % Señal original

A2 = Amp*sin(omega*t); % Señal amplificada

A3 = Amp*sin(omega*t + theta); % Señal desfasada

R = rand(1, length(A3)); % Señal aleatoria

AR1 = A3 + R; % Señal desfasada con ruido

AR2 = [A3, R]; % Señal desfasada concatenada con ruido
t2 = 0:0.01:20.01; % Vector tiempo de 2002 elementos

ecgm = 500; % Cantidad de muestras para ECG
ECG1 = ecg(ecgm); % Señal ECG de ecgm datos
t3 = (1:1:ecgm)*(2/ecgm); % Vector tiempo de ecgm elementos en 2 segundos

ECG2 = [ecg(ecgm), ecg(ecgm), ecg(ecgm), ecg(ecgm), ecg(ecgm)]; % 5 ECG1 concatenados
t4 = (1:1:ecgm*5)*(2/ecgm);  % Vector tiempo de ecgm*5 elementos en 10 segundos

% Graficas
figure(1);
plot(t, A1);
title('A1: Señal Original');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(2);
plot(t, A2);
title('A2: Señal amplificada');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(3);
plot(t, A3);
title('A3: Señal desfasada');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(4);
plot(t, R);
title('R: Señal aleatoria (ruido)');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(5);
plot(t, AR1);
title('AR1: Señal A3 con ruido');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(6);
plot(t2, AR2);
title('AR2: Señal A3 con ruido concatenado');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(7);
plot(t3, ECG1);
title('ECG1: 1 ciclo de ECG');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;

figure(8);
plot(t4, ECG2);
title('ECG2: Señal 5 ciclos de ECG');
xlabel('tiempo');
ylabel('amplitud');
axis tight;
grid on;