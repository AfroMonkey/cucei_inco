clear all;
clear all hidden;
clc;

[x, fs] = audioread('chido.wav');
fc = 1500;


x1 = x(:, 1);

%audio = audioplayer(x1, fs);
%play(audio);


t = (1 : length(x1))/fs;
eje_f = -fs/2 : fs/(length(x1) - 1) : fs/2;

tic;
fftx1 = fftshift(fft(x1));

tam_fc = round(fc*length(x1)/fs)*2 + mod(length(x1), 2);
tam_rest = (length(x1) - tam_fc)/2;

vect_1 = ones(tam_fc, 1);
vect_0 = zeros(tam_rest, 1);

fil = [vect_0; vect_1; vect_0];

Y = fftx1.*fil;
y = ifft(ifftshift(Y));
toc

tsinc = -1 : 1/fs : 1;
tic;
sinc_f = sinc(2*fc*tsinc);
%y2 = conv(x1, sinc_f, 'same');
toc

audiowrite('final.wav', y, fs);

f1 = 1500;
f2 = 5000;
fsinc = f2 - f1;
fcos = f1 + f2/2;
hpb = (2*fsinc*tsinc).*cos(2*pi*fcos*tsinc);
%ypb = conv(x1, hpb, 'same');


figure(1);
subplot(3, 1, 1);
plot(t, x1);
subplot(3, 1, 2);
plot(eje_f, real(fftx1));
subplot(3, 1, 3);
plot(eje_f, imag(fftx1));

figure(2);
subplot(4, 1, 1);
%plot(t, y2);
subplot(4, 1, 2);
plot(t, y);
subplot(4, 1, 3);
plot(eje_f, real(Y));
subplot(4, 1, 4);
plot(eje_f, imag(Y));