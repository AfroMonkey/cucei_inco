close all hidden;
clc;

%% Captura de audio

[x, fs] = audioread('chido.wav');
x1 = x(:,1);
t = (1:length(x))/fs;

%% Transformada

ejef = -fs/2:fs/(length(x)-1):fs/2;
X = fftshift(fft(x1));

%% Filtro PasaBajas Frecuencia

fc = 480;
tamfc = (round((fc*length(x)/fs))*2)+mod(length(x),2);
tamresto = (length(x) - tamfc)/2;
unos = ones(tamfc,1);
ceros = zeros(tamresto, 1);
filtro = [ceros; unos; ceros];
Y = X.*filtro;
y = ifft(ifftshift(Y));

%% Graficando Original y Frecuencia

figure(1);
subplot(3,1,1);
plot(t,x);
xlabel('Eje de tiempo');
ylabel('Amplitud de la se�al');
title('Audio original');
axis tight;

subplot(3,1,2);
plot(ejef,real(X));
xlabel('Eje de Frecuencia');
ylabel('Amplitud de la se�al');
title('Parte real de la transformada');
axis tight;

subplot(3,1,3);
plot(ejef,imag(X));
xlabel('Eje de Frecuencia');
ylabel('Amplitud de la se�al');
title('Parte imaginaria de la transformada');
axis tight;

%% Graficando PasaBajas Frecuencia

figure(2);
subplot(3,1,1);
plot(t,real(y));
xlabel('Eje de tiempo');
ylabel('Amplitud de la se�al');
title('Audio PasaBajas en Frecuencia');
axis tight;

subplot(3,1,2);
plot(ejef,real(Y));
xlabel('Eje de Frecuencia');
ylabel('Amplitud de la se�al');
title('Parte real de la transformada');
axis tight;

subplot(3,1,3);
plot(ejef,imag(Y));
xlabel('Eje de Frecuencia');
ylabel('Amplitud de la se�al');
title('Parte imaginaria de la transformada');
axis tight;


%% Filtro PasaBandas Frecuencia

fc = 480;
fc2 = 800;
tamfc = (round((fc*length(x)/fs))*2)+mod(length(x),2);
tamfc2 = round((fc2-fc)*length(x)/fs);
tamResto = (length(x) - (2*tamfc2) - tamfc)/2;

cerosCentro = zeros(tamfc, 1);
unos = ones(tamfc2, 1);
cerosOrilla = zeros(tamResto, 1);
filtro = [cerosOrilla; unos; cerosCentro; unos; cerosOrilla];
pasaBandasF = X.*filtro;
pasaBandasT = ifft(ifftshift(pasaBandasF));

%% Graficando PasaBandas Frecuencia

figure(3);
subplot(3,1,1);
plot(t,real(pasaBandasT));
xlabel('Eje de tiempo');
ylabel('Amplitud de la se�al');
title('Audio PasaBandas en Frecuencia');
axis tight;

subplot(3,1,2);
plot(ejef,real(pasaBandasF));
xlabel('Eje de Frecuencia');
ylabel('Amplitud de la se�al');
title('Parte real de la transformada');
axis tight;

subplot(3,1,3);
plot(ejef,imag(pasaBandasF));
xlabel('Eje de Frecuencia');
ylabel('Amplitud de la se�al');
title('Parte imaginaria de la transformada');
axis tight;

%% Filtro PasaBajas Tiempo

tsinc = -1:1/fs:1;
h = sinc(2*fc*tsinc);
pasaBajasTiempo = conv(x1,h,'same');

%% Graficando PasaBajas Tiempo

figure(4);
subplot(3,1,1);
plot(t,pasaBajasTiempo);
xlabel('Eje de tiempo');
ylabel('Amplitud de la se�al');
title('Audio PasaBajas en Tiempo');
axis tight;

pbtfft = fftshift(fft(pasaBajasTiempo));
subplot(3,1,2);
plot(ejef,real(pbtfft))
xlabel('Eje de frecuencia');
ylabel('Amplitud de la se�al');
title('Parte real de la transformada');
axis tight;

subplot(3,1,3);
plot(ejef,imag(pbtfft))
xlabel('Eje de frecuencia');
ylabel('Amplitud de la se�al');
title('Parte imaginaria de la transformada');
axis tight;


%% Filtro PasaBandas Tiempo

fsinc = fc2-fc;
fcos = fc+(fsinc/2);
hpb = sinc(2*fsinc*tsinc) .* cos(2*pi*fcos*tsinc);
pasaBandasTiempo = conv(x1, hpb, 'same');


%% Graficando PasaBandas Tiempo

figure(5);
subplot(3,1,1);
plot(t,pasaBandasTiempo);
xlabel('Eje de tiempo');
ylabel('Amplitud de la se�al');
title('Audio PasaBandas en Tiempo');
axis tight;

pbtfft2 = fftshift(fft(pasaBandasTiempo));
subplot(3,1,2);
plot(ejef,real(pbtfft2))
xlabel('Eje de frecuencia');
ylabel('Amplitud de la se�al');
title('Parte real de la transformada');
axis tight;

subplot(3,1,3);
plot(ejef,imag(pbtfft2))
xlabel('Eje de frecuencia');
ylabel('Amplitud de la se�al');
title('Parte imaginaria de la transformada');
axis tight;


%% audio

% audioFiltrado = audioplayer(pasaBandasTiempo, fs);
% play(audioFiltrado);