close all hidden;
clc;

%% Variables de entrada

archivo_audio = 'chido.wav';
frecuencia_corte_p_bajas = 1500;
frecuencia_corte_p_bandas = 1500;
frecuencia_corte_p_bandas2 = 4000;

%% Lectura de audio

[audio, frecuencia_muestreo] = audioread(archivo_audio);
canal1 = audio(:, 1);
eje_tiempo = (1:length(audio))/frecuencia_muestreo;

%% Transformada de Fourier

eje_frecuencia = -frecuencia_muestreo/2 : frecuencia_muestreo/(length(canal1) - 1) : frecuencia_muestreo/2;
canal1_f = fftshift(fft(canal1));

%% Filtro pasa-bajas Frecuencia

tam_fc_p_bajas = (round((frecuencia_corte_p_bajas*length(canal1)/frecuencia_muestreo))*2) + mod(length(canal1), 2);
tam_resto_p_bajas = (length(canal1) - tam_fc_p_bajas)/2;
unos_p_bajas = ones(tam_fc_p_bajas, 1);
ceros_p_bajas = zeros(tam_resto_p_bajas, 1);
filtro = [ceros_p_bajas; unos_p_bajas; ceros_p_bajas];
sen_filtrada_f_p_bajas_f = canal1_f.*filtro;
sen_filtrada_f_p_bajas_t = ifft(ifftshift(sen_filtrada_f_p_bajas_f));

%% Filtro pasa-bandas Frecuencia

tam_fc_p_bandas = (round((frecuencia_corte_p_bandas*length(canal1)/frecuencia_muestreo))*2) + mod(length(canal1), 2);
tam_fc_p_bandas2 = round((frecuencia_corte_p_bandas2-frecuencia_corte_p_bandas)*length(canal1)/frecuencia_muestreo);
tam_resto_p_bandas = (length(canal1) - (2*tam_fc_p_bandas2) - tam_fc_p_bandas)/2;

ceros_centro_p_bandas = zeros(tam_fc_p_bandas, 1);
unos_p_bandas = ones(tam_fc_p_bandas2, 1);
ceros_orilla_p_bandas = zeros(tam_resto_p_bandas, 1);
filtro = [ceros_orilla_p_bandas; unos_p_bandas; ceros_centro_p_bandas; unos_p_bandas; ceros_orilla_p_bandas];
sen_filtrada_f_p_bandas_f = canal1_f.*filtro;
sen_filtrada_f_p_bandas_t = ifft(ifftshift(sen_filtrada_f_p_bandas_f));

%% Filtro pasa-bajas Tiempo

eje_t_sinc = -1 : 1/frecuencia_muestreo : 1;
sen_sinc_p_bajas = sinc(2*frecuencia_corte_p_bajas*eje_t_sinc);
sen_filtrada_t_p_bajas = conv(canal1, sen_sinc_p_bajas, 'same');
sen_filtrada_t_p_bajas = sen_filtrada_t_p_bajas*max(canal1)/max(sen_filtrada_t_p_bajas);
sen_filtrada_t_p_bajas_f = fftshift(fft(sen_filtrada_t_p_bajas));

%% Filtro pasa-bandas Tiempo

frecencia_sinc = frecuencia_corte_p_bandas2-frecuencia_corte_p_bandas;
frecuencia_cos = frecuencia_corte_p_bandas+(frecencia_sinc/2);
sen_sinc_p_bandas = sinc(frecencia_sinc*eje_t_sinc).*cos(2*pi*frecuencia_cos*eje_t_sinc);
sen_filtrada_t_p_bandas = conv(canal1, sen_sinc_p_bandas, 'same');
sen_filtrada_t_p_bandas = sen_filtrada_t_p_bandas*max(canal1)/max(sen_filtrada_t_p_bandas);
sen_filtrada_t_p_bandas_f = fftshift(fft(sen_filtrada_t_p_bandas));

%% Graficas

figure(1);
subplot(3, 1, 1);
plot(eje_tiempo, canal1);
title('Pista 1');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_f));
title('Transformada de Pista 1 (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_f));
title('Transformada de Pista 1 (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(2);
subplot(3, 1, 1);
plot(eje_tiempo, sen_filtrada_f_p_bajas_t);
title('Pista 1 filtrada en frecuencia con pasa-bajas');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(sen_filtrada_f_p_bajas_f));
title('FFT de Pista 1 filtrada en frecuencia con pasa-bajas (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(sen_filtrada_f_p_bajas_f));
title('FFT de Pista 1 filtrada en frecuencia con pasa-bajas (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(3);
subplot(3, 1, 1);
plot(eje_tiempo, sen_filtrada_f_p_bandas_t);
title('Pista 1 filtrada en frecuencia con pasa-bandas');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(sen_filtrada_f_p_bandas_f));
title('Parte real de la transformada');
title('FFT de Pista 1 filtrada en frecuencia con pasa-bandas (real)');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(sen_filtrada_f_p_bandas_f));
title('FFT de Pista 1 filtrada en frecuencia con pasa-bandas (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(4);
subplot(3, 1, 1);
plot(eje_tiempo, sen_filtrada_t_p_bajas);
title('Pista 1 filtrada en tiempo con pasa-bajas');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(sen_filtrada_t_p_bajas_f));
title('FFT de Pista 1 filtrada en tiempo con pasa-bajas (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(sen_filtrada_t_p_bajas_f));
title('FFT de Pista 1 filtrada en tiempo con pasa-bajas (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(5);
subplot(3, 1, 1);
plot(eje_tiempo, sen_filtrada_t_p_bandas);
title('Pista 1 filtrada en tiempo con pasa-bandas');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(sen_filtrada_t_p_bandas_f));
title('FFT de Pista 1 filtrada en tiempo con pasa-bandas (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(sen_filtrada_t_p_bandas_f));
title('FFT de Pista 1 filtrada en tiempo con pasa-bandas (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

% audioFiltrado = audioplayer(sen_filtrada_t_p_bandas, frecuencia_muestreo);
% play(audioFiltrado);
