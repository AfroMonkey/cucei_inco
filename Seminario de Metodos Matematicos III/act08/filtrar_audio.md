# Actividad 08: Filtros Ideales
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-04-04


## Introducción
### Filtro
Un filtro es un sistema el cual pondera de acuerdo a su función de transferencia las diferentes componentes frecuenciales de una señal de entrada, pudiendo ası́ atenuar unas mientras deja pasar sin distorsión otras. Lo anterior puede ser expresado como:  
![](img/f1)  
donde `X(f)` es la señal filtrada en el dominio de la frecuencia, `S(f)` es la transformada de Fourier de la señal de entrada y `H(f)` es la función de transferencia del filtro. En este sentido, se puede hacer una clasificación de los filtros de acuerdo a su función de transferencia siendo: filtros pasa baja, filtros pasa altas, filtros pasa banda y filtros rechaza banda.  
El filtro ideal pasa bajas se define como un sistema cuya función de transferencia esta dada por:  
![](img/f2)  
donde `fc` es la frecuencia de corte del filtro.

### Convolución
En matemáticas y, en particular, análisis funcional, una convolución es un operador matemático que transforma dos funciones f y g en una tercera función que en cierto sentido representa la magnitud en la que se superponen f y una versión trasladada e invertida de g. Una convolución es un tipo muy general de media móvil, como se puede observar si una de las funciones se toma como la función característica de un intervalo.

La convolución de `f` y `g` se denota `f∗g`. Se define como la integral del producto de ambas funciones después de desplazar una de ellas una distancia `t`.  
![](img/conv)  


## Actividad
### Variables iniciales
Al inicio del script preparamos nuestro espacio de trabajo y definimos unas variables que nos ayudarán a indicar el nombre del archivo a analizar, la frecuencia de corte para el filtro pasa bajas y las frecuencias de corte para el filtro pasa bandas.  
![](img/iniciales)  

### 1. Grabar un mensaje de 30 segundos en el equipo de computo y guardarlo en formato WAV.
![](img/audacity)  
### 2. Cargar en el programa de procesamiento el mensaje grabado.
Para cargar el archivo de audio utilizaremos la función `audioread`. Esta función nos entrega todos los canales del archivo en cuestión, así como su frecuencia de muestreo; es por eso que lo almacenamos en un par de variables.
Posteriormente obtenemos el canal que analizaremos del audio completo. Aprovecharemos para crear el eje del tiempo que se requerirá para las gráficas en dicho dominio.  
![](img/audioread)  

### 3. Obtener la transformada rápida de Fourier de uno de los canales de audio del mensaje cargado.
Obtendremos la FFT de la misma forma que en ocasiones anteriores, así como la generación de su respectivo eje de frecuencia.  
![](img/fft)  

### 4. Graficar el espectro de magnitud obtenido en el punto anterior.
Se verá en la sección de gráficas.

### 5. Genera la función de transferencia de un filtro pasa bajas ideal con frecuencia de corte igual a 1500Hz.
Nuevamente, este proceso ya lo hemos realizado en anteriores ocasiones. Solo tenemos que definir nuestros vectores de unos y ceros en base a la frecuencia de corte y la frecuencia de muestreo.  
![](img/fpbajas)  

### 6. Filtrar la señal de audio con el filtro pasa-bajas. Realizar esta tarea en el dominio de la frecuencia.
Ahora aplicamos el filtro creado en el paso anterior y aprovecharemos apra obtener el resultado de la señal filtrada pero en el dominio del tiempo. (Ojo, el filtro se hizo en el dominio de la frecuencia).  
![](img/afpbajas)  

### 7. Realizar la convolución de la señal de audio con la respuesta al impulso del filtro. Obtener la transformada de Fourier de la señal obtenida.
Al realizar la convolución nos evitamos el tener que manejar la señal en el espacio de la frecuencia; y, en lineas de código, es más sencillo este proceso, sin embargo, la convolución implica una gran cantidad de operaciones, pues tiene que trabajar con dos señales y "mezclaras", en una algoritmo de complejidad `O(n, m) = n*m`, donde `n` y `m` son la cantidad de elementos de las señales a mezclar.

Para realizar este filtrado se requiere de un nuevo vector el cual nos indicará la cantidad de "muestras" para hacer la convolución. Además requerimos usar la función `sinc`, la devuelve `sin(x)/x`, esto para lograr generar el filtro (En la matemática general especificar dentro del argumento la constante `pi` para su uso acertado; sin embargo en las herramientas digitales se omite esa constante pues ya esta de manera interna en la función).

Aprovecharemos para generar la FFT de la señal filtrada.  
![](img/ftpbajas)  
Utilizamos el argumento 'same' para que la señal resultante tenga la misma cantidad de elementos que la original, además fue necesario ajustar la amplitud de la señal, pues la señal resultante de manera natural de la convolución es mucho más amplia que la original; para ello utilizamos una regla de tres haciendo que la amplitud maxima de la señal filtrada sea la misma que la original.

### 10. Repita los pasos 6 - 8 ahora con el filtro pasa banda.
Primero realizamos el filtrado en el dominio de la frecuencia.
![](img/fpbandas)  

Posteriormente relizamos el mismo filtrado pero en el dominio del tiempo.

Nota: En la función `sinc` no se  requiere multiplicar la frecuencia por 2 puesto que ahora no hay parte central que deba ser duplicada.  
![](img/ftpbandas)  
.
### Graficas
![](img/g1)  

![](img/g2)  

![](img/g3)  

![](img/g4)  

![](img/g5)  

Como podemos observar, las gráficas de las señales filtradas en tiempo con su correspondiente en frecuencia son prácticamente las mismas, quizá con un poco de diferencia debido al tamaño de la muestra.

Para fines prácticos se reprodujeron los audios resultantes de cada filtro y, para el odio humano, las diferencias entre los filtros realizados en tiempo con su análogo en frecuencia resultaron indistinguibles.

## Conclusión
El filtrado en frecuencia es algo más complejo que el filtrado en tiempo en cuanto a proceso "manual" que se requiere, puesto que hay que crear las secciones de 0's y 1's que se desea; sin embargo, a mi parecer, vale la pena este "sacrificio" para evitar hacer la convolución, ya que es un proceso que toma demasiado tiempo (minutos en una computadora con un gran poder de procesamiento). Además, el filtrado en tiempo, de manera "natural", entrega la señal "amplificada", por lo que hay que realizar un ajuste manual de la misma, perdiendo sus "puntos" por sencillez.

## Referencias
* Act08 Filtros ideales.pdf
* Documentación interna de Octave
* http://mathworld.wolfram.com/Convolution.html
* https://es.wikipedia.org/wiki/Convoluci%C3%B3n
* https://www.mathworks.com/help/signal/ref/sinc.html
* https://es.wikipedia.org/wiki/Funci%C3%B3n_sinc
