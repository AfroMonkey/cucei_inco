% Limpieza de memoria y cierre de ventanas
clear all;
close all hidden;
clc;


% Declaracion de variables
f = 1; % Frecuencia
j = csvread('datos01.csv', 1, 0); % Lectura de datos, excluyendo cabecera
t = j(:, 1); % Vector tiempo
y = j(:, 2); % Amplitud


% Coeficientes de Fourier
k = length(j); % Tamaño de la muestra
w_0 = 2*pi*f;

mul_y_cos = y(1:k).*cos(w_0*t(1:k)); % Multiplicaciones funcion*cos
sum_y_cos = sum(mul_y_cos); % Sumatoria
a = (2 / k)*sum_y_cos; % Coeficiente a

mul_y_sin = y(1:k).*sin(w_0*t(1:k)); % Multiplicaciones funcion*sen
sum_y_sin = sum(mul_y_sin); % Sumatoria
b = (2 / k)*sum_y_sin; % Coeficiente b

c = mean(y(1:k)); % Coeficiente c


% Resultados
disp(['a = ', num2str(a)]);
disp(['b = ', num2str(b)]);
disp(['c = ', num2str(c)]);

% Graficas
figure(1);
plot(t, y);
title('Señal de CSV');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
