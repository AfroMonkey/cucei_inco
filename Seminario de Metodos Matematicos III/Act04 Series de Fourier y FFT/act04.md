# Actividad 04 Series de Fourier y FFT
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-02-28


## Introducción
#### Serie de Fourier
Serie infinita que converge puntualmente a una función periódica y continua a trozos, es decir, una expansión de una función periódica en términos de una suma infinita de senos y cosenos.  
La transformada rápida de Fourier (FFT, Fast Fourier Transform) es un algoritmo
eficiente para calcular la transformada de Fourier discreta (DFT) y su inversa. 

Si una función `f(t)` tiene un número finito de discontinuidades en `-a < t < a` y tiene un número finito de máximos y mínimos en dicho intervalo, entonces puede ser representada por la serie compleja de Fourier:  
![](images/fourier)  
en donde los coeficientes están dados por  
![](images/coef)  
Esta representación corresponde a un función periódica, de periodo 2a en el intervalo −a < t < a.  
En forma equivalente, la serie de Fourier de una función periódica f (x), de periodo T , se define como  
![](images/t)  
en donde ω0 = 2π/T se conoce como frecuencia fundamental y  
![](images/at)  
![](images/bt)  
y una suma parcial está dada por  
![](images/p)  

#### Serie cortas de Fourier y FFT
La transformada rápida de Fourier (FFT) es una manera más rápida de realizar el cálculo, llevando la complejidad computacional de O(N²) a O(N log N).  
Un escenario equivalente para funciones peródicas en tiempo discreto, con periodo 2a, se puede representar mediante  
![](images/fft)  
en donde ∼ indica que la función f (t) y la suma coinciden en los puntos muestrales, es decir, f (xj ) = p(xj ) por lo que, la teorı́a que soporta los cálculos se mueve en dirección de modelos de ajuste de curvas.

### Actividad
1. Genere una muestra de tamaño 500 de la función  
![](images/f1)  
para `f = [0.02, 0.2, 0.4]`, agregue ruido aleatorio con la función randn y suponiendo que todos son valores desconocidos, estime frecuencias y coeficientes de Fourier.  
  * Iniciamos preparando Octave  
![](images/clear)  
  * Declaramos las variables "globales" a utilizar  
![](images/dec)  
Donde `f` es la frecuencia, `t` es un vector que indica los puntos en el tiempo a graficar (con un tamaño de 1x500), `x` es la función original, y `y` es la misma funcion pero añadiendole ruido aleatorio.
  * Posteriormente definimos el tamaño de la muestra, así como w_0, para agilizar el proceso.  
  ![](images/k500)  
  * Ahora calculamos los elementos correspondientes al coseno  
  ![](images/an)  
  ![](images/ycos)  
  Para realizar esto utilizamos la multiplicación de elemento a elemento (.*  ) provista por Octave.  
  * Luego sumamos estos términos (utilizando la función "sum")  
  ![](images/scos)  
  * Para obtener el coeficiente `a` simplemente debemos multiplicar la sumatoria por  `(2/T)`  
  ![](images/a)
  * Realizamos un proceso similar para obtener el coeficiente `b`, pero cambiamos cosenos por senos.  
  ![](images/b)
  * Para obtener `c` simplemente obtenemos la media de los valores de `y`  
  ![](images/c)
  * Mostramos los resultados en pantalla  
  ![](images/resultados_form)  
  ![](images/resultados02)  
  Como vemos, los resultados son muy cercanos a -2, 7 y 5; que son los coeficientes originales de la función.
  * Finalmente graficamos la función original y la alterada.  
  ![](images/graf02)  
  * Realizamos el mismo proceso pero para `f=0.2`  
  ![](images/resultados2)  
  ![](images/graf2)  
  * Realizamos el mismo proceso pero para `f=0.4`  
  ![](images/resultados4)  
  ![](images/graf4)  
2. Los datos que se presentan en el archivo datos01.csv tienen una muestra de una señal en tiempo discreto. estime frecuencias y coeficientes de Fourier.
  * Esta actividad es prácticamente igual que la anterior, lo único que cambia es la obtención de los datos y la frecuencia.
  ![](images/exc2)  
  Para leer los datos desde el archivo utilizamos la función "csvread", en la cual indicamos la ruta del archivo, así como la fila y columna desde la cual comenzara a leer (para evitar leer la cabecera del archivo). Dicho archivo contiene 2 columnas, en la primera viene el tiempo en el cual fue tomada la muestra, en la segunda está la magnitud de la misma.  
  A continuación los coeficientes y gráficas de este ejercicio.  
  ![](images/res2)  
  ![](images/graf22)  

3. Los registros mensuales de manchas solares observadas desde 1749 a 2015 está disponible en http://solarscience.msfc.nasa.gov/greenwch/spot_num.txt. Obtenga una copia de los datos y obtenga sus coeficientes de Fourier.
  * Nuevamente los pasos serán los mismos, pero hay un par de detalles a tener en cuenta, por ejemplo: 
    * El archivo fue ligeramente modificado para poder ser leído como csv 
    * El tiempo está dado en 2 columnas, una indicando el año y otra el mes.
    * Contiene 2 columnas para las amplitudes, dando a entender que son 2 señales distintas (SSN: Smoothed Sunspot Number y DEV)  
  ![](images/31)  
  * Como los datos de tiempo están divididos en 2 columnas, lo que hice fue convertir los meses a años (dividiendo entre 12) y sumarlo al año, de esta forma queda todo en términos de años.  
  ![](images/32)  
  * Despues obtenemos los coeficientes para SSN  
  ![](images/33)  
  * Despues obtenemos los coeficientes para DEV  
  ![](images/rt3)  
  ![](images/rp3)  
  * Obtenemos los coeficientes de Fourier para SSN y DEV  
  ![](images/gt3)  
  ![](images/graf3)  
  * Finalmente mostramos las gráficas de estos dos fenómenos.

## Conclusión
Como se pudo observar, es posible obtener (al menos de manera muy aproximada) los coeficientes de Fourier de una función solamente con tener los datos de amplitud y momento de dicha amplitud de la función, esto nos puede ayudar a obtener la función original y hacer predicciones mas acertadas que utilizando simplemente los valores crudos, así como establecer una función que se adapte a un fenómeno físico, el cual podría parecer que no sigue alguna periodicidad.

## Referencias
*   Act04 Series de Fourier y FFT.pdf
*   http://solarscience.msfc.nasa.gov/greenwch/spot_num.txt
*   http://hfradio.org/propagation_page3.html
