% Limpieza de memoria y cierre de ventanas
clear all;
close all hidden;
clc;


% Declaracion de variables
f = 1; % Frecuencia
j = csvread('spot_num.csv', 1, 0); % Lectura de datos, excluyendo cabecera
t = j(: ,1) + j(:, 2)/12; % Conversion  de tiempo a años
ssn = j(:, 3); % Valores de SSN
dev = j(:, 4); % Valores de DEV


% Coeficientes de Fourier
k = length(j); % Tamaño de la muestra
w_0 = 2*pi*f;

mul_ssn_cos = ssn(1:k).*cos(w_0*t(1:k)); % Multiplicaciones funcion*cos
sum_ssn_cos = sum(mul_ssn_cos); % Sumatoria
a_ssn = (2 / k)*sum_ssn_cos; % Coeficiente a_ssn

mul_ssn_sin = ssn(1:k).*sin(w_0*t(1:k)); % Multiplicaciones funcion*sen
sum_ssn_sin = sum(mul_ssn_sin); % Sumatoria
b_ssn = (2 / k)*sum_ssn_sin; % Coeficiente b_ssn

c_ssn = mean(ssn(1:k)); % Coeficiente c_ssn


mul_dev_cos = dev(1:k).*cos(w_0*t(1:k)); % Multiplicaciones funcion*cos
sum_dev_cos = sum(mul_dev_cos); % Sumatoria
a_dev = (2 / k)*sum_dev_cos; % Coeficiente a_dev

mul_dev_sin = dev(1:k).*sin(w_0*t(1:k)); % Multiplicaciones funcion*sen
sum_dev_sin = sum(mul_dev_sin); % Sumatoria
b_dev = (2 / k)*sum_dev_sin; % Coeficiente b_dev

c_dev = mean(dev(1:k)); % Coeficiente c_dev


% Resultados
disp(['a_ssn = ', num2str(a_ssn)]);
disp(['b_ssn = ', num2str(b_ssn)]);
disp(['c_ssn = ', num2str(c_ssn)]);
disp(['a_dev = ', num2str(a_dev)]);
disp(['b_dev = ', num2str(b_dev)]);
disp(['c_dev = ', num2str(c_dev)]);

% Graficas
figure(1);
subplot(2, 1, 1);
plot(t, ssn);
title('Señal SSN');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
subplot(2, 1, 2);
plot(t, dev);
title('Señal DEV');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;