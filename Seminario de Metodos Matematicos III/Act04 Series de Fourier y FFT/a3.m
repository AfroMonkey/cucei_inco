clear all;
close all hidden;
clc;


%TODO que es SSN y ssn

f = 1;
j = csvread('spot_num.csv', 1, 0);
t = j(: ,1) + j(:, 2)/12;
ssn = j(:, 3);
dev = j(:, 4);


% Coeficientes de Fourier
k = length(j);
w_0 = 2*pi*f;

mul_ssn_cos = ssn(1:k).*cos(w_0*t(1:k));
sum_ssn_cos = sum(mul_ssn_cos);
a = (2 / k)*sum_ssn_cos;

mul_ssn_sin = ssn(1:k).*sin(w_0*t(1:k));
sum_ssn_sin = sum(mul_ssn_sin);
b = (2 / k)*sum_ssn_sin;

c = mean(ssn(1:k));


disp(['a = ', num2str(a)]);
disp(['b = ', num2str(b)]);
disp(['c = ', num2str(c)]);

% Coeficientes de Fourier

mul_dev_cos = dev(1:k).*cos(w_0*t(1:k));
sum_dev_cos = sum(mul_dev_cos);
a2 = (2 / (k - 1))*sum_dev_cos;

mul_dev_sin = dev(1:k).*sin(w_0*t(1:k));
sum_dev_sin = sum(mul_dev_sin);
b2 = (2 / (k - 1))*sum_dev_sin;

c2 = mean(dev(1:k));


disp(['a2 = ', num2str(a2)]);
disp(['b2 = ', num2str(b2)]);
disp(['c2 = ', num2str(c2)]);


figure(1);
subplot(2, 1, 1);
plot(t, ssn);
title('Señal SSN');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
subplot(2, 1, 2);
plot(t, dev);
title('Señal DEV');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
