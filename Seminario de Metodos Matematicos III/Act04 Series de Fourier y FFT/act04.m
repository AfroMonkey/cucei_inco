% Limpieza de memoria y cierre de ventanas
clear all;
close all hidden;
clc;


% Declaracion de variables
f = 0.4; % Frecuencia
t = 1:1:500; % Vector tiempo
x = 5 - 2*cos(2*pi*f*t) + 7*sin(2*pi*f*t); % Funcion original
y = x + randn(1, 500); % Funcion con ruido


% Coeficientes de Fourier
k = 500; % Tamaño de la muestra
w_0 = 2*pi*f;

mul_y_cos = y(1:k).*cos(w_0*t(1:k)); % Multiplicaciones funcion*cos
sum_y_cos = sum(mul_y_cos); % Sumatoria
a = (2 / k)*sum_y_cos; % Coeficiente a

mul_y_sin = y(1:k).*sin(w_0*t(1:k)); % Multiplicaciones funcion*sen
sum_y_sin = sum(mul_y_sin); % Sumatoria
b = (2 / k)*sum_y_sin; % Coeficiente b

c = mean(y(1:k)); % Coeficiente c


% Resultados
disp(['a = ', num2str(a)]);
disp(['b = ', num2str(b)]);
disp(['c = ', num2str(c)]);

% Graficas
figure(1);
subplot(2, 1, 1);
plot(t, x);
title('Señal Original');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
subplot(2, 1, 2);
plot(t, y);
title('Señal con Ruido');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
