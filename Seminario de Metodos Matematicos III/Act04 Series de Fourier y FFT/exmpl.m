clear all;
close all hidden;
clc;

t = -1 : 0.01 : 1;
%f = sin(2*pi*t*1) + cos(2*pi*t*1);
f = randn(1, length(t));

% Descomposicion par e impar
f_ref = f(end : -1 : 1);
f_par = f/2 + f_ref/2;
f_imp = f/2 - f_ref/2;

figure(1);
subplot(3, 1, 1);
plot(t, f);
subplot(3, 1, 2);
plot(t, f_par);
subplot(3, 1, 3);
plot(t, f_imp);
