clear all;
close all hidden;
clc;


f = 1;
j = csvread('datos01.csv', 1, 0);
t = j(: ,1);
y = j(:, 2);


% Coeficientes de Fourier
k = length(j);
w_0 = 2*pi*f;

mul_y_cos = y(1:k).*cos(w_0*t(1:k));
sum_y_cos = sum(mul_y_cos);
a = (2 / k)*sum_y_cos;

mul_y_sin = y(1:k).*sin(w_0*t(1:k));
sum_y_sin = sum(mul_y_sin);
b = (2 / k)*sum_y_sin;

c = mean(y(1:k));


disp(['a = ', num2str(a)]);
disp(['b = ', num2str(b)]);
disp(['c = ', num2str(c)]);


figure(1);
plot(t, y);
title('Señal de CSV');
xlabel('Eje de tiempo');
ylabel('Amplitud de la señal');
axis tight;
grid on;
