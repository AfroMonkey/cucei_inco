# Actividad 10: Modulación por Ancho de Pulso (PWM)
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-04-11


## Introducción
### PWM
La modulación por ancho de pulso (PWM) es una técnica de modulación en la cual el ciclo de trabajo de un tren de pulso cuadrados se modifica en función de la señal modulante. De la definición anterior, se entiende por ciclo de trabajo el periodo de tiempo en la cual el pulso cuadrado permanece en un valor distinto de cero. Matemáticamente, este tren de pulsos cuadrados puede ser expresado como:  
![](img/p1)   
donde `x(t)` es la señal PWM generada, A es la amplitud del pulso cuadrado, `0 ≤ ν ≤ 1` es el ciclo de trabajo, `T` es el periodo del tren de pulso cuadrados. En la figura 2 se muestra el tren de pulsos cuadrados con todos los parámetros que lo representa. De manera directa, se puede observar que un que un ciclo de trabajo de cero nos generarı́a una señal con amplitud promedio igual a cero, en el otro extremo un ciclo de trabajo igual a uno producirı́a una señal con amplitud promedio igual a `A`. Ası́, un ciclo de trabajo entre estos dos valores extremos conducirı́a a que la amplitud promedio de la señal generada sea igual `νA`. De manera genérica, la señal PWM generada resulta de modular el ciclo de trabajo en función de una señal modulante, considerando esto, (1) se transforma en:  
![](img/p2)  
donde `ν(t)` es el ciclo de trabajo variante en el tiempo que es función de la señal modulante. De esta manera, las aplicaciones de las señales PWM consisten en el control de la energı́a entregada utilizando dispositivos de conmutación de dos estados (estado de conducción, no conducción). Tales aplicaciones van desde el control de velocidad de motores, control de intensidad lumı́nica, amplificadores de audio, etc.  

![](img/p3)  

### Amplificación de audio
La amplificación de audio por medio de señales PWM se logra por medio de los amplificador clase
D. En estos la señal PWM, la cual es función de la señal de audio, controla a los elementos de
conmutación, permitiendo ası́ el flujo de energı́a hacia la etapa de salida compuesta por un filtro pasa
bajas (que sirve para eliminar las componentes armónicas de alta frecuencia) y la bocina. En la figura
2.1 se muestra el diagrama a bloques funcional de una amplificador clase D básico de medio puente.
La manera mas sencilla de modular el tren de pulsos cuadrados es mediante la comparación de la
señal de audio con una señal triangular periódica, de tal manera de que cuando la señal de triangular
sea menor que la de audio se genera la parte del pulso con amplitud diferente a cero y cuando la
señal triangular es mayor que la señal de audio se genera la parte del pulso cuadro igual a cero tal y
como se muestras en la figura 2.1. De acuerdo a [JH15] la frecuencia tı́pica de la señal triangular para
aplicaciones de audio es entre 250 KHz y 1.5MHz.



# Actividad
## Variables iniciales
Al inicio del script preparamos nuestro espacio de trabajo y definimos unas variables que nos ayudarán a indicar el nombre del archivo a analizar, la frecuencia de corte para el filtro y la frecuencia para la señal triangular que utilizaremos.  
![](img/iniciales)  

## a. Grabar un mensaje de 30 segundos en el equipo de computo y guardarlo en formato WAV.
![](img/audacity)  


## b. Cargar en el programa de procesamiento el mensaje grabado.
Para cargar el archivo de audio utilizaremos la función `audioread`. Esta función nos entrega todos los canales del archivo en cuestión, así como su frecuencia de muestreo; es por eso que lo almacenamos en un par de variables.
Posteriormente obtenemos el canal que analizaremos del audio completo.  
Aprovecharemos para crear el eje del tiempo que se requerirá para las gráficas en dicho dominio.  
![](img/audioread)  

## c. Obtener la transformada rápida de Fourier de uno de los canales de audio del mensaje cargado.
Obtendremos la FFT de la misma forma que en ocasiones anteriores, así como la generación de su respectivo eje de frecuencia.  
![](img/canal1_fft)  

## d. Graficar el espectro de magnitud obtenido en el punto anterior.
Se muestra en la sección de Gráficas.

## e. Generar una señal triangular con frecuencia igual a 2.5KHz
Para generar la señal triangulare utilizaremos una función llamada 'sawtooth' (En octave,  al menos en el que utilicé, fue necesario instalar el paquete 'octave-signal' para hacer uso de esta función).  
Esta función recibe dos parámetros: el primero hace referencia a la frecuencia de la señal y el segundo indica el la ubicación del punto máximo de la señal (siendo 0.5 el valor indicado para que sea una señal triangular).  
Aprovecharemos para crear su equivalente en frecuencia.  
![](img/triangular)  


## f. Obtener la señal PWM a partir de la señal de audio y la señal triangular generada
Para obtener PWM utilizamos el operador `>` el cual hace que los valores mayor a 0 tomen el valor de 1 y los menores a este se vuelvan 0.  
![](img/pwm)  

## g. Obtener la transformada rápida de Fourier de la señal PWM y graficar el espectro de frecuencia de esta.
Para ello simplemente aplicamos la transformada rápida de Fourier a la señal anterior.  
![](img/pwm_fft)  


## h. A la transformada de Fourier de la señal PWM hacer todas las componentes frecuenciales mayores a 1 KHz igual a cero.
Para esto crearemos un filtro pasa bajas con la frecuencia estipulada al inicio del script.  
![](img/filtro)  
Posteriormente aplicaremos este filtro a la señal original.  
![](img/filtrado)  

## Adecuación de la señal resultante.
Para que la señal sea correcta, es necesario que su media sea igual a 0, por lo que a la señal resultante del filtro le restamos su media actual; además se requiere que la amplitud máxima de la misma sea de 1, por lo que dividimos toda la señal entre su amplitud máxima.  
![](img/ajustado)  
.  
.  
.  
.  
## i. Del punto anterior, obtener la transformada inversa, graficar y escuchar en las bocinas de la computadora esta nueva señal.
Para volver al espacio del tiempo simplemente aplicamos la IFFT a la señal anterior.  
![](img/filtrado_t)
Ya para reproducir el audio requerimos de la función `audioplayer`.  
![](img/audios)  
Ya que tenemos estas variables, podemos utilizar la ventana de comandos para reproducir tanto el audio original como los audios resultantes utilizando la función `play`.  
![](img/play)  


.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
## Graficas
### Códigos
![](img/cf1)  
Código para generar la Figura 1  

![](img/cf2)  
Código para generar la Figura 2  

![](img/cf3)  
Código para generar la Figura 3  

![](img/cf4)  
Código para generar la Figura 4  

![](img/cf5)  
Código para generar la Figura 5  

.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
.  
### Figuras
#### Figura 1
![](img/f1)  
En esta gráfica podemos ver la señal de audio original, tanto en tiempo como en frecuencia.  

#### Figura 2
![](img/f2)  
Aquí se observa una pequeña sección de la señal triangular, las imperfecciones en la misma se debe a un problema de muestreo.  


#### Figura 3
![](img/f3)  
En esta figura podemos observar una sección de la señal PWM, la encargada de modular la señal "final".  

#### Figura 4
![](img/f4)  
Aqui podemos ver una comparación entre la señal Original (azul) contra la triangular (rojo) y contra la PWM (magenta); como se puede ver, la señal PWM solo está "activa" cuando la señal triangular tiene un valor mayor a 0.  


#### Figura 5
![](img/f5)  
En esta figura se muestra la señal ya filtrada y, como se puede ver en la 2da gráfica, ha perdido gran parte de sus frecuencias en la parte real.

## Conclusión
El audio resultante del filtro tiene una calidad muy pobre, incluso con las adecuaciones que hicimos posteriormente, quizá se deba a las frecuencias que seleccionamos para realizar los cortes, puesto que la "técnica" de modulación por ancho de pulso se suele utilizar en el mundo real y no presenta tales perdidas de fidelidad. Una de las ventajas de este tipo de modulación es que reduce drásticamente la cantidad de frecuencias que se envían a través del medio, lo cual reduce el ancho de banda consumido permitiendo enviar más señales por el mismo.

## Referencias
* Act10 Modulacion por ancho de pulso.pdf
* <https://www.mathworks.com/matlabcentral/fileexchange/9366-pwm-signal-from-soundcard>
