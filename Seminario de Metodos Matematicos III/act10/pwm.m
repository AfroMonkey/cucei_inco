close all hidden;
clc;

%% Variables de entrada

archivo_audio = 'chido.wav';
frecuencia_corte_p_bajas = 1500;
frecuencia_corte_p_bandas = 1500;
frecuencia_corte_p_bandas2 = 4000;

%% Lectura de audio

[audio, frecuencia_muestreo] = audioread(archivo_audio);
canal1 = audio(:, 1);
eje_tiempo = (1:length(audio))/frecuencia_muestreo;

%% Transformada de Fourier

eje_frecuencia = -frecuencia_muestreo/2 : frecuencia_muestreo/(length(canal1) - 1) : frecuencia_muestreo/2;
canal1_f = fftshift(fft(canal1));

%% Sawtooth
f = 2500; % 250000
fTriangular = sawtooth(2*pi*f*eje_tiempo, 0.5);
fTriangularF = fftshift(fft(fTriangular));

%% PWM
pwm = fTriangular > canal1;
pwmF = fftshift(fft(pwm));

%% Graficas

figure(1);
subplot(3, 1, 1);
plot(eje_tiempo, canal1);
title('Pista 1');

xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_f));
title('Transformada de Pista 1 (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_f));
title('Transformada de Pista 1 (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(2);
subplot(3, 1, 1);
plot(eje_tiempo, fTriangular);
title('Pista 1');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(fTriangularF));
title('Transformada de Pista 1 (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(fTriangularF));
title('Transformada de Pista 1 (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(3);
subplot(3, 1, 1);
plot(eje_tiempo, pwm);
title('Pista 1');
xlabel('Tiempo');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(pwmF));
title('Transformada de Pista 1 (real)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(pwmF));
title('Transformada de Pista 1 (imaginaria)');
xlabel('Frecuencia');
ylabel('Amplitud');
axis tight;


figure(4);
plot(eje_tiempo, canal1);
hold on;
plot(eje_tiempo, fTriangular, 'r');
stem(eje_tiempo, pwm, 'm');
xlim ([10, 10.003]);