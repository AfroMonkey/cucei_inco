close all hidden;
clc;
pkg load signal;

%% Lectura de audio
archivo_ruta = 'chido.wav';
frecuencia_corte = 1000;
triangular_frecuencia = 2500;

[archivo, Fs] = audioread(archivo_ruta);
canal1 = archivo(:, 1);
eje_tiempo = (1 : length(canal1))/Fs;

%% FFT audio
canal1_fft = fftshift(fft(canal1));
eje_frecuencia = -Fs/2 : Fs/(length(canal1) - 1) : Fs/2;

%% Señal triangular
triangular = sawtooth(2*pi*triangular_frecuencia*eje_tiempo, 0.5)';

canal1_fft_triangular = fftshift(fft(triangular));

%% Comparacion
pwm = triangular > canal1;
canal1_fft_pwm = fftshift(fft(pwm));

%% Filtro pasa bajas
fc_tam = (round((frecuencia_corte*length(canal1))/Fs) * 2) + mod(length(canal1),2);
resto_tam = (length(canal1) - fc_tam)/2;

unos = ones(fc_tam, 1);
ceros = zeros(resto_tam, 1);

filtro = [ceros; unos; ceros;];

canal1_filtrado_f = filtro.*canal1_fft_pwm;
canal1_filtrado_t = ifft(ifftshift(canal1_filtrado_f));

canal1_media_cero = canal1_filtrado_t - mean(canal1_filtrado_t);
canal1_amplitud_uno = canal1_media_cero/max(canal1_media_cero);

%% Audio
audio_original = audioplayer(canal1, Fs);
audio_filtrado = audioplayer(canal1_filtrado_t, Fs);
audio_filtrado_ajustado = audioplayer(canal1_amplitud_uno, Fs);

%% Graficas
figure(1);
subplot(3, 1, 1);
plot(eje_tiempo, canal1);
title('Señal Original');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
axis tight;

subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_fft));
title('FFT de la señal original (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_fft));
title('FFT de la señal original (imag)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;


figure(2);
subplot(3, 1, 1);
plot(eje_tiempo, triangular);
title('Señal Triangular');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
xlim([1, 1.005]);

subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_fft_triangular));
title('FFT de la señal Triangular (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_fft_triangular));
title('FFT de la señal Triangular (imag)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;


figure(3);
subplot(3, 1, 1);
stem(eje_tiempo, pwm);
title('Señal PWM');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
xlim([1, 1.005]);


subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_fft_pwm));
title('FFT de la señal PWM (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_fft_pwm));
title('FFT de la señal PWM (imag)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;


figure(4);
plot(eje_tiempo, canal1);
title('Señal Original vs Señal Triangular vs Señal PWM');
hold on;
plot(eje_tiempo, triangular, 'r');
xlabel('Tiempo (s)');
ylabel('Amplitud');
stem(eje_tiempo, pwm, 'm')
xlim([1, 1.005]);


figure(5);
subplot(3, 1, 1);
stem(eje_tiempo, canal1_filtrado_t);
title('Señal filtrada');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
xlim([1, 1.005]);

subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_filtrado_f));
title('FFT de la señal Filtrada (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_filtrado_f));
title('FFT de la señal Filtrada (imag)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;
