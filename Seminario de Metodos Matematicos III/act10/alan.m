clear all;
close all;
clc;
%% LECTURA DE AUDIO
% ACT 09 -> PARA ESTE JUEVES
% ACT 10 antes del martes
% fs => frecuencia de muestreo :OOO
[x, fs] = audioread('chido.wav');
canalIzq = x(:, 1);

t = 1/fs : 1/fs : length(canalIzq)/fs;
%% TRANSFORMADAS DE FOURIER
f_fourier = fftshift(fft(canalIzq));
ejeF = -fs/2 : fs/(length(canalIzq) - 1) : fs/2;
%% FUNCION TRIANGULAR
f = 2500; % ORIGINAL = 250000, dejar en 2500
fTriangular = sawtooth(2*pi*f*t, 0.5);

f_fourier_TRIANG = fftshift(fft(fTriangular));
%% COMPARACION
pwm = fTriangular > canalIzq'; % PICOS MAYORES A 0 valen 1, picos menores a 0
% valen 0 (DE la se�al de sierra)
% TRANSFORMADAS DE FOURIER PWM
f_fourier_PWM = fftshift(fft(pwm));

%% CONSTRUYENDO FILTRO PASA BAJAS
fc = 1000; % ORIGINAL => 20Khz
tamfc = (round( ( fc*length(canalIzq) ) /fs ) * 2) + mod( length(canalIzq),2);
tamresto = (length(canalIzq) - tamfc) / 2;

unos = ones(tamfc, 1);
ceros = zeros(tamresto, 1);

filtro = [ceros; unos; ceros;];

Y_pwm = filtro .* f_fourier_PWM';
y_pwm = ifft(ifftshift(Y_pwm));

y1 = y_pwm - mean(y_pwm);
y2 = y1 / max(y1);
%% CREANDO AUDIO
audio_pwm_filtrada = audioplayer(y_pwm, fs);
audio_original = audioplayer(canalIzq, fs);
audio_pwm_filtrada2 = audioplayer(y2, fs);
%% GRAFICACION
figure(1);
subplot(3, 1, 1);
plot(t, canalIzq);
title('Audio');
axis tight;
xlim([10, 10.005]);

subplot(3, 1, 2);
plot(ejeF, real(f_fourier));
axis tight;

subplot(3, 1, 3);
plot(ejeF, imag(f_fourier));
axis tight;

%% GRAFICACION DE FUNCION TRIANGULAR
figure(2);
subplot(3, 1, 1);
plot(t, fTriangular);
title('Funcion Triangular');
axis tight;
xlim([10, 10.005]);

subplot(3, 1, 2);
plot(ejeF, real(f_fourier_TRIANG));
axis tight;

subplot(3, 1, 3);
plot(ejeF, imag(f_fourier_TRIANG));
axis tight;

%% PWM
figure(3);
subplot(3, 1, 1);
stem(t, pwm);
title('PWM');
axis tight;
% xlim([10, 10.005]);

subplot(3, 1, 2);
plot(ejeF, real(f_fourier_PWM));
axis tight;

subplot(3, 1, 3);
plot(ejeF, imag(f_fourier_PWM));
axis tight;
%% GRAFICACION DE TODO
figure(4);
plot(t, canalIzq);
hold on;
plot(t, fTriangular, 'r');
stem(t, pwm, 'm'); % para graficar datos que no son continuos (discretos)
xlim([10, 10.003]); % limitar la grafica

%% GRAFICA PWM FILTRADO
figure(5);
subplot(3, 1, 1);
stem(t, y_pwm);
title('PWM Filtrada');
axis tight;
% xlim([10, 10.005]);

subplot(3, 1, 2);
plot(ejeF, real(Y_pwm));
axis tight;

subplot(3, 1, 3);
plot(ejeF, imag(Y_pwm));
axis tight;
