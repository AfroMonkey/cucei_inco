clear all
clear all hidden
clc

m = load('data/data/ECG2.mat');
x = m.data;
Fs = 1000; % 1 KHz
x_muestra = x(30*Fs : 50*Fs);
t = (1 : 1 : length(x))/Fs;
t_muestra = ((1 : 1 : length(x_muestra))/Fs) + 30;

fft1 = fftshift(fft(x_muestra));
eje_f = -Fs/2 : Fs/(length(x_muestra)-1) : Fs/2;


figure(1);
subplot(3, 1, 1);
plot(t_muestra, x_muestra);
subplot(3, 1, 2);
plot(eje_f,  real(fft1));
subplot(3, 1, 3);
plot(eje_f, imag(fft1));