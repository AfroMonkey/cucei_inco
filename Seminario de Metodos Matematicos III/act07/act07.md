# Actividad 07 Práctica 1
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-02-28

## Introducción
### Señal
Función matemática que depende de una o más variables independientes, normalmente a través del tiempo; y cuyo valor nos da información acerca de algún fenómeno.

### Señal electrofisiológica
Señal obtenida al analizar las propiedades eléctricas de células y tejidos biológicos. Las variables comúnmente utilizadas son los cambios de voltaje o de corriente.

### Electrocardiograma (ECG)
Es la representación gráfica de la actividad eléctrica del corazón. Se obtiene mediante la colocación de electrodos en puntos del cuerpo que faciliten el acceso a estas señales. El resultado es una señal donde el eje Y es la amplitud de la señal (en micro volts) y el eje X es el tiempo.

### Electroencefalograma (EEG)
Es la representación gráfica de la actividad eléctrica del cerebro, en la cual podemos ver varios tipos de ondas como: Delta, Tetha, Alfa y Beta. El resultado son múltiples señales (una por cada electrodo colocado) donde el eje Y es la amplitud de la señal (en micro volts) y el eje X es el tiempo.

### Electromiograma (EMG)
Es la representación gráfica de la actividad eléctrica del corazón. Se obtiene mediante la colocación de electrodos en 3 puntos, uno al inicio del musculo a analizar, otra en su parte central y un ultimo electrodo que sirve de referencia o "tierra". El resultado es una señal donde el eje Y es la amplitud de la señal (en micro volts) y el eje X es el tiempo.

## Conclusión
Este tipo de señales aportan información útil sobre la condición de algún paciente, sin embargo, puesto que su magnitud es muy pequeña (en el orden de los micros), es muy fácil que se vea contaminada por ruido externo, lo que le resta fiabilidad a la señal y puede provocar interpretaciones erróneas.

## Referencias
*  http://enrique.sanchez.webs.uvigo.es/PDFs/125_TemaI-Senales.pdf
*  https://es.wikipedia.org/wiki/Electrocardiograma
*  https://es.wikipedia.org/wiki/Electroencefalografía
*  https://es.wikipedia.org/wiki/Electromiografía
