# Actividad 09: Moidulación AM
Navarro Presas Moisés Alejandro - 215861509  
SSP de Métodos Matemáticos III - D04  
2017-04-06


## Introducción
Modulación es el método de modificar las caracterı́sticas de una señal portadora en función de una señal modulante. En comunicaciones, uno de los beneficios de llevar a cabo la modulación de una señal es el de lograr transmisiones mas eficientes (menor dimensión de las antenas, inmunidad al ruido, asignación del espectro radio eléctrico). Las técnicas de modulación mas utilizadas son la modulación de amplitud (AM), modulación de frecuencia (FM), modulación de fase (PM) o combinaciones de estas. Suponiendo que la señal portadora sea una señal coseno, la señal modulada se expresa como:  
![](img/f1)  
donde `x(t)` es la señal modulada, `s(t)` es la amplitud de la portadora en función del tiempo (amplitud modulada), `fc(t)` es la frecuencia de la portadora en función del tiempo (frecuencia modulada) y `θ(t)` es la fase de la portadora en función del tiempo (modulación de fase).
### Amplitud Modulada
Es una de las técnicas de modulación mas sencillas y es ampliamente utilizada. En este tipo de modulación se puede observar un desplazamiento del espectro de frecuencia de la señal modulante en la vecindad de la frecuencia portadora. Lo anterior se puede verificar fácilmente al observar la propiedad de translación de la frecuencia en la transformada de Fourier. En la radio comercial se utiliza la modulación AM de portadora completa y doble banda lateral (DSBFC) la cual queda expresada matemáticamente como:  
![](img/f2)  
donde `xAM(t)` es la señal modulada en AM, `s(t)` es la señal modulante, `fc` es la frecuencia de la
portadora y `k` es el coeficiente de modulación. Se puede observar que la señal modulada contiene
potencia tanto de la portadora como de la señal modulante y la proporción de cada una de ellas esta
en función del coeficiente de modulación.
Por otra parte, las transmisiones de AM comercial se realizan en la banda frecuencial que va desde
los 535 KHz hasta los 1600 KHz, donde se fija el ancho de banda de la señal modulante a un máximo
de 10 KHz. Ası́, las posibles frecuencias de portadoras van desde los 540 KHz hasta los 1600 KHz en
incrementos de 10KHz.




# Actividad
## Variables iniciales
Al inicio del script preparamos nuestro espacio de trabajo y definimos unas variables que nos ayudarán a indicar el nombre del archivo a analizar, la frecuencia de corte para el filtro, la frecuencia para la señal coseno que requeriremos maás delante y el coeficiente de modulación (`k`).  
![](img/iniciales)  

## a. Grabar un mensaje de 30 segundos en el equipo de computo y guardarlo en formato WAV.
![](img/audacity)  

## b. Cargar en el programa de procesamiento el mensaje grabado.
Para cargar el archivo de audio utilizaremos la función `audioread`. Esta función nos entrega todos los canales del archivo en cuestión, así como su frecuencia de muestreo; es por eso que lo almacenamos en un par de variables.
Posteriormente obtenemos el canal que analizaremos del audio completo.  
Aprovecharemos para crear el eje del tiempo que se requerirá para las gráficas en dicho dominio.  
![](img/audioread)  

## c. Obtener la transformada rápida de Fourier de uno de los canales de audio del mensaje cargado.
Obtendremos la FFT de la misma forma que en ocasiones anteriores, así como la generación de su respectivo eje de frecuencia.  
![](img/canal1_fft)  

## d. Graficar el espectro de magnitud obtenido en el punto anterior.
Se muestra en la sección de Gráficas.

## e. Generar una seal coseno de frecuencia 540 KHz y amplitud 1.
![](img/portadora)  

## f. Modular la señal coseno con la señal de audio.
Para modular la señal, se requiere aplicar la formula `x_{AM} (t) = k(1 + s(t)) cos(2πf_ct)` para `|s(t)| ≤ 1` por lo que se requeriría discriminar los valores mayores a 1 de la señal original, sin embargo le valor máximo de la misma no llega 1, por lo que no es necesaria ninguna adecuación.  
Aprovecharemos para generar la FFT de la misma señal.  
![](img/modulada)  


## g. Obtener gráfica en el dominio del tiempo y en el dominio de la frecuencia de la señal modulada.
Se muestra en la sección de Gráficas.


## Demodulación
Para obtener la señal de-modulada se realiza el mismo proceso que para la modulación, ya que al volver a multiplicar la señal por el mismo coseno hace que esta vuelva "posicionarse" en su estado original.  
![](img/demodulada)  

## Filtro Pasa Bajas
Ahora generaremos un filtro pasa-bajas de la misma forma que lo hemos hecho en actividades previas; es decir, generando un conjunto de unos y ceros para eliminar las frecuencias que no deseamos.  
![](img/filtro)  
## Graficas
### Códigos
![](img/cg1)  
![](img/cg2)  
![](img/cg3)  
![](img/cg4)   
### Figuras
![](img/g1)  
En esta gráfica podemos ver la señal de audio original.  

![](img/g2)  
La señal portadora tiene una frecuencia muy alta (540Khz), por lo que solo estamos mostrando una parte reducida de la misma para lograr ver la onda (debería ser un coseno perfecto, sin embargo debido a la frecuencia de muestreo no se aprecia correctamente).  

![](img/g3)  
Aqui podemos ver como la señal original está montada en la señal portadora (el coseno de gran frecuencia) dando como resultado la señal modulada en amplitud.  

![](img/g4)  
Esta es la señal ya de-modulada, la cual tiene un gran parecido con la señal original; pero como se puede comprobar en las partes real e imaginaria de la FFT den ambas señales, hay frecuencias que se pierden en el proceso de modulación y filtrado.

## Conclusión
La modulación de señales permite que podamos transmitir las mismas más fácilmente; y el proceso para realizar eso (al menos con Octave) es muy sencillo. Cabe mencionar que aun que la modulación en amplitud trae sus ventajas, también genera inconvenientes, como la perdida de fidelidad de la señal.

## Referencias
* Act09 Modulacion AM.pdf
* http://www.radio-electronics.com/info/rf-technology-design/am-amplitude-modulation/what-is-am-tutorial.php
