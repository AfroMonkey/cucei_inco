close all hidden;
clc;

archivo_ruta = 'chido.wav';
frecuencia_corte = 1500;
coseno_frecuencia = 540000;
k = 0.8;

[archivo, Fs] = audioread(archivo_ruta);
canal1 = archivo(:, 1);
eje_tiempo = (1 : length(canal1))/Fs;

% FFT audio
canal1_fft = fftshift(fft(canal1));
eje_frecuencia = -Fs/2 : Fs/(length(canal1) - 1) : Fs/2;

% Señal portadora
portadora = transpose(cos(2*pi*coseno_frecuencia*eje_tiempo));
portadora_fft = fftshift(fft(transpose(portadora)));

% Señal modulada
modulada = k*(1 + canal1).*portadora;
modulada_fft = fftshift(fft(modulada));

% Señal demodulada
demodulada = modulada.*portadora;
demodulada_fft = fftshift(fft(demodulada));

% Filtro pasa bajas
fc_tam = 2*round((frecuencia_corte*length(canal1))/Fs) + mod(length(canal1), 2);
resto_tam = (length(canal1) - fc_tam)/2;
ceros_vector = zeros(resto_tam, 1);
unos_vector = ones(fc_tam, 1);
filtro = [ceros_vector; unos_vector; ceros_vector];
canal1_filtrada_f = demodulada_fft.*filtro;
canal1_filtrada_t = ifft(ifftshift(canal1_filtrada_f));

figure(1);
subplot(3, 1, 1);
plot(eje_tiempo, canal1);
title('Señal original');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_fft));
title('FFT de la señal original (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_fft));
title('FFT de la señal original (imaginaria)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

figure(2);
subplot(3, 1, 1);
plot(eje_tiempo, transpose(portadora));
title('Señal portadora');
xlabel('Tiempo (s)');
ylabel('Amplitud');
xlim([0, 0.001]);
grid on;
subplot(3, 1, 2);
plot(eje_frecuencia, real(portadora_fft));
title('FFT de la señal portadora (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 3);
plot(eje_frecuencia, imag(portadora_fft));
title('FFT de la señal portadora (imaginaria)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

figure(3);
subplot(3, 1, 1);
plot(eje_tiempo, modulada);
title('Señal modulada en amplitud');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 2);
plot(eje_frecuencia, real(modulada_fft));
title('FFT de la señal modulada en amplitud (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 3);
plot(eje_frecuencia, imag(modulada_fft));
title('FFT de la señal modulada en amplitud (imaginaria)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;

figure(4);
subplot(3, 1, 1);
plot(eje_tiempo, canal1_filtrada_t);
title('Señal de-modulada');
xlabel('Tiempo (s)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 2);
plot(eje_frecuencia, real(canal1_filtrada_f));
title('FFT de la señal de-modulada (real)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;
subplot(3, 1, 3);
plot(eje_frecuencia, imag(canal1_filtrada_f));
title('FFT de la señal de-modulada (imaginaria)');
xlabel('Frecuencia (Hz)');
ylabel('Amplitud');
grid on;
axis tight;
