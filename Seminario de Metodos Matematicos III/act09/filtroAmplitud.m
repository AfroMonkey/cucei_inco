clear all;
clear all hidden;
clc;

[file, fs] = audioread('chido.wav');
fc = 540*1000;
k = 0.8;

signal = file(:, 1);

vector_t = (1 : length(signal))/fs;
vector_f = -fs/2 : fs/(length(signal) - 1) : fs/2;

fft_signal = fftshift(fft(signal));

cos_signal = transpose(cos(2*pi*fc*vector_t));

modulated = signal.*cos_signal;

for i = 1:length(modulated)
  if abs(modulated(i)) > 1
    modulated(i) = 0;
  endif
endfor

modulate_am = k*(1 + signal).*cos_signal;
modulated_f = fftshift(fft(modulate_am));
max_fc = max(real(modulated_f));

tam_fc = round(max_fc*length(modulated_f)/fc)*2 + mod(length(modulated_f), 2);
tam_rest = (length(modulated_f) - tam_fc)/2;

vect_1 = ones(tam_fc, 1);
vect_0 = zeros(tam_rest, 1);

fil = [vect_0; vect_1; vect_0];
demodulated = ifft(ifftshift((modulate_am.*cos_signal).*fil));

figure(1);
subplot(3, 1, 1);
plot(vector_t, signal);
subplot(3, 1, 2);
plot(vector_f, real(fft_signal));
subplot(3, 1, 3);
plot(vector_f, imag(fft_signal));

figure(2);
subplot(1, 1, 1);
plot(vector_t, cos_signal);
xlim([0, .001]);

figure(3);
subplot(3, 1, 1);
plot(vector_t, modulate_am);
subplot(3, 1, 2);
plot(vector_f, real(modulated_f));
subplot(3, 1, 3);
plot(vector_f, imag(modulated_f));


figure(4);
subplot(1, 1, 1);
plot(vector_t, demodulated);
