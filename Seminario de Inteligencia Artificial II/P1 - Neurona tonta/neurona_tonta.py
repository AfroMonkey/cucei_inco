from random import uniform

MIN = -1
MAX = 1


class Neuron:
    def __init__(self, n_inputs):
        self.ws = list(range(n_inputs + 1))
        self.reinit()
        self.z = lambda inputs: sum(self.ws[i] * inputs[i] for i in range(len(self.ws)))
        self.f = lambda z: int(z > 0)

    def reinit(self):
        self.ws = [uniform(MIN, MAX) for _ in range(len(self.ws))]

    def evaluate(self, inputs):
        return self.f(self.z(inputs + [1]))


tests = [
    ([0, 0], 0),
    ([0, 1], 0),
    ([1, 0], 0),
    ([1, 1], 1),
]

n = Neuron(2)
print("Evaluando w0 = {}, w1 = {}, b = {}".format(*n.ws))


current = 0
success = 0
while success != len(tests):
    e = n.evaluate(tests[current][0])
    print("{} \t\t {} \t\t\t {}".format(tests[current][0], tests[current][1], e))
    if e == tests[current][1]:
        success += 1
    else:
        n.reinit()
        print("")
        print("Evaluando w0 = {}, w1 = {}, b = {}".format(*n.ws))
        print("Entradas \t Salida esperada \t Salida obtenida")
        success = 0
    current += 1
    current %= len(tests)
