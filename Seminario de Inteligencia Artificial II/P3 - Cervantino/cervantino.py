import numpy as np


class Neuron:
    def __init__(self, n_inputs):
        self.weights = None
        self.reinit(n_inputs)
        self.z = lambda inputs: self.weights.T @ np.array(inputs)
        self.f = lambda z: int(z > 0)

    def reinit(self, n_inputs=None):
        if n_inputs is None:
            if self.weights is not None:
                n_inputs = len(self.weights) - 1
            else:
                raise Exception('Cant reinit')
        self.weights = np.random.rand(n_inputs + 1) * 2 - 1

    def evaluate(self, inputs):
        return self.f(self.z(inputs + [1]))


class Perceptron:
    def __init__(self, arch):
        self.neuron = Neuron(arch['n_inputs'])

    def fit(self, inputs, error):
        self.neuron.weights = self.neuron.weights + (error * np.append(inputs, [1]))

    def train(self, tests):
        print("Evaluando w0 = {}, w1 = {}, b = {}".format(*self.neuron.weights))
        current = 0
        success = 0
        while success != len(tests):
            actual = self.neuron.evaluate(tests[current][0])
            print("{} \t\t {} \t\t\t {}".format(tests[current][0], tests[current][1], actual))
            error = tests[current][1] - actual
            if not error:
                success += 1
            else:
                self.fit(tests[current][0], error)
                print("")
                print("Evaluando w0 = {}, w1 = {}, b = {}".format(*self.neuron.weights))
                print("Entradas \t Salida esperada \t Salida obtenida")
                success = 0
            current += 1
            current %= len(tests)


tests = [
    ([0, 0, 0], 0),
    ([0, 0, 1], 0),
    ([0, 1, 0], 0),
    ([0, 1, 1], 1),
    ([1, 0, 0], 0),
    ([1, 0, 1], 0),
    ([1, 1, 0], 1),
    ([1, 1, 1], 1),
]

p = Perceptron({'n_inputs': len(tests[0][0])})
p.train(tests)
