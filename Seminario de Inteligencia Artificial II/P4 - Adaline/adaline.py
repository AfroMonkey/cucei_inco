import numpy as np


class Neuron:
    def __init__(self, n_inputs):
        self.weights = None
        self.reinit(n_inputs)
        self.z = lambda inputs: self.weights.T @ inputs
        self.f = lambda z: z

    def reinit(self, n_inputs=None):
        if n_inputs is None:
            if self.weights is not None:
                n_inputs = len(self.weights)
            else:
                raise Exception('Cant reinit')
        self.weights = np.random.rand(n_inputs) * 2 - 1

    def eval(self, inputs):
        return self.f(self.z(inputs))


class Adaline:
    def __init__(self, arch):
        self.neuron = Neuron(arch['n_inputs'])
        self.eta = arch.get('eta', 0.1)
        self.max_error = arch.get('max_error', 0.001)
        self.max_epochs = arch.get('max_epochs', 1000)

    def fit(self, tests):
        global_error = 0
        for test in tests:
            error = test[1] - self.neuron.eval(test[0])
            self.neuron.weights += test[0] * error * self.eta
            global_error += error * error / 2
        print(self.neuron.weights)
        return global_error

    def train(self, tests):
        epochs = 0
        while epochs < self.max_epochs:
            epochs += 1
            if self.fit(tests) < self.max_error:
                break
        return epochs


tests = [
    [np.array([2.0, 1, 40]), 0],
    [np.array([3.0, 1, 50]), 1],
    [np.array([3.0, 50, 50]), 1],
    [np.array([8.0, 90, 60]), 1],
    [np.array([9, 100, 70]), 0],
]

for test in tests:
    test[0] = np.append(test[0], 1)

adaline = Adaline({'n_inputs': len(tests[0][0])})
print(adaline.train(tests))
while True:
    point = []
    for d in range(len(tests[0][0]) - 1):
        point.append(float(input(">")))
    point = np.append(point, 1)
    print(adaline.neuron.eval(point))
