#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#define SIZE 35

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

private slots:
    void update();

    void on_actionExit_triggered();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QTimer* timer;

    char products[SIZE];
    unsigned ptrProducer;
    unsigned ptrConsumer;
    unsigned time;
    unsigned nextAwakenProducer;
    unsigned nextAwakenConsumer;
    unsigned toProduce;
    unsigned toConsume;
    bool activeProducer;
    bool activeConsumer;

    void init();
    void updateUI();
    void newProducer();
    void newConsumer();
    void produce();
    void consume();
};

#endif // MAINWINDOW_H
