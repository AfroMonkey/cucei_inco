#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    init();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::init() {
    time = 0;
    ptrProducer = ptrConsumer = 0;
    nextAwakenConsumer = nextAwakenProducer = 0;

    ui->tableWidgetProducts->insertRow(0);
    ui->tableWidgetProducts->insertRow(0);
    ui->tableWidgetProducts->setColumnCount(SIZE);
    ui->tableWidgetProducts->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    for (int i = 0; i < 35; ++i) {
        products[i] = '-';
    }

    newProducer();
    newConsumer();
}

void MainWindow::newProducer() {
    activeProducer = false;
    nextAwakenProducer = time + rand() % 10 + 1;
    toProduce = rand() % 10 + 1;
}

void MainWindow::newConsumer() {
    activeConsumer = false;
    nextAwakenConsumer = time + rand() % 10 + 1;
    toConsume = rand() % 10 + 1;
}

void MainWindow::produce() {
    if (products[ptrProducer] == '-') {
        products[ptrProducer] = 'O';
        ++ptrProducer;
        ptrProducer %= SIZE;
        --toProduce;
    } else {
        toProduce = 0;
    }
    if (not toProduce) {
        newProducer();
    }
}

void MainWindow::consume() {
    if (products[ptrConsumer] == 'O') {
        products[ptrConsumer] = '-';
        ++ptrConsumer;
        ptrConsumer %= SIZE;
        --toConsume;
    } else {
        toConsume = 0;
    }
    if (not toConsume) {
        newConsumer();
    }
}

void MainWindow::update() {
    updateUI();

    if (time >= nextAwakenProducer) {
        if (not activeConsumer) {
            activeProducer = true;
            produce();
        } else {
            ui->labelProducerStatus->setText(QString::fromStdString("Bussy"));
            newProducer();
        }
    }
    if (time >= nextAwakenConsumer) {
        if (not activeProducer) {
            activeConsumer = true;
            consume();
        } else {
            ui->labelConsumerStatus->setText(QString::fromStdString("Bussy"));
            newConsumer();
        }
    }

    ++time;
}

void MainWindow::updateUI() {
    for (int i = 0; i < 35; ++i) {
        ui->tableWidgetProducts->setItem(0, i, new QTableWidgetItem(QString::fromStdString(std::string(1, products[i]))));
        ui->tableWidgetProducts->setItem(1, i, new QTableWidgetItem(QString::fromStdString("-")));
        ui->tableWidgetProducts->item(0, i)->setTextAlignment(Qt::AlignCenter);
        ui->tableWidgetProducts->item(1, i)->setTextAlignment(Qt::AlignCenter);
    }

    if (ptrProducer == ptrConsumer) {
        ui->tableWidgetProducts->setItem(1, ptrProducer, new QTableWidgetItem(QString::fromStdString("PC")));
    } else {
        ui->tableWidgetProducts->setItem(1, ptrProducer, new QTableWidgetItem(QString::fromStdString("P")));
        ui->tableWidgetProducts->setItem(1, ptrConsumer, new QTableWidgetItem(QString::fromStdString("C")));
    }
    ui->tableWidgetProducts->item(1, ptrProducer)->setTextAlignment(Qt::AlignCenter);
    ui->tableWidgetProducts->item(1, ptrConsumer)->setTextAlignment(Qt::AlignCenter);

    if (activeProducer) {
        ui->labelProducerStatus->setText(QString::fromStdString("Working"));
        ui->labelProducerAwake->setText(QString::fromStdString("NA"));
    } else {
        ui->labelProducerStatus->setText(QString::fromStdString("Sleeping"));
        ui->labelProducerAwake->setText(QString::number(nextAwakenProducer - time));
    }

    if (activeConsumer) {
        ui->labelConsumerStatus->setText(QString::fromStdString("Working"));
        ui->labelConsumerAwake->setText(QString::fromStdString("NA"));
    } else {
        ui->labelConsumerStatus->setText(QString::fromStdString("Sleeping"));
        ui->labelConsumerAwake->setText(QString::number(nextAwakenConsumer - time));
    }

    ui->labelProduce->setText(QString::number(toProduce));
    ui->labelConsume->setText(QString::number(toConsume));
}

void MainWindow::on_actionExit_triggered()
{
    close();
}
