∫ f(x) dx = F(x) + C

f(x) => Es la derivada de F(x) con respecto de x
∫ => Símbolo de integral
∫ f(x) dx => INtegral indefinida
dx => Indica la variable que se va a integrar
C => Constante de integración
