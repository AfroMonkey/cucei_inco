#include <fstream>
#include <iostream>
#include <map>

int main(int argc, char *argv[]) {
  if (argc != 4) {
    std::cout << "Usage:\n";
    std::cout << argv[0] << " input_file key_file output_file"
              << "\n";
    return 1;
  }

  std::ifstream input(argv[1]);
  if (!input) {
    std::cout << "Cant open the file \"" << argv[1] << "\""
              << "\n";
    return 2;
  }
  std::ifstream keys_file(argv[2]);
  if (!keys_file) {
    std::cout << "Cant open the file \"" << argv[2] << "\""
              << "\n";
    return 3;
  }
  std::ofstream output(argv[3], std::ios::trunc | std::ios::binary);
  if (!output) {
    std::cout << "Cant open the file \"" << argv[3] << "\""
              << "\n";
    return 4;
  }

  /* Dictionary */
  std::map<std::string, char> keys;
  char c;
  std::string k;
  while (true) {
    keys_file.read((char *)&c, sizeof(char));
    if (keys_file.eof())
      break;
    getline(keys_file, k);
    keys[k] = c;
  }
  keys_file.close();

  /* Input */
  std::string bits;
  while (true) {
    input.read((char *)&c, sizeof(char));
    if (input.eof())
      break;
    for (int i = 7; i >= 0; --i) {
      bits += (c & 1 << i) ? '1' : '0';
    }
  }
  input.close();

  /* Output */
  int i;
  std::string may_c;
  for (i = 0; i < (int)bits.length(); ++i) {
    may_c += bits[i];
    if (keys.find(may_c) != keys.end()) {
      output.write((char *)&keys[may_c], sizeof(char));
      may_c = "";
    }
  }

  if (keys.find(may_c) == keys.end()) {
    while (keys.find(may_c) == keys.end() && may_c != "") {
      may_c = may_c.substr(0, may_c.size() - 1);
    }
    if (may_c != "")
      output.write((char *)&keys[may_c], sizeof(char));
  }

  output.close();

  return 0;
}
