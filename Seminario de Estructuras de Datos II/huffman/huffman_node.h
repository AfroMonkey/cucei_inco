#ifndef HUFFMAN_NODE
#define HUFFMAN_NODE

#include <cstdlib> /*NULL*/
#include <fstream>

class HuffmanNode
{
public:
    int weight_;
    char c_;
    HuffmanNode* left_;
    HuffmanNode* right_;

    HuffmanNode(int weight = -1, char c = '\x0', HuffmanNode *left = NULL, HuffmanNode *right = NULL) : weight_(weight), c_(c), left_(left), right_(right) {}
    void free();
    char get_least();
    static bool cmp(HuffmanNode *a, HuffmanNode *b);
    void get_dictionary(std::ofstream &output, std::string id = "");
};

void HuffmanNode::free()
{
    if (left_) left_->free();
    if (right_) right_->free();
    if (left_) delete left_;
    if (right_) delete right_;
}

char HuffmanNode::get_least()
{
    if (c_ != '\x0') return c_;
    return left_->get_least();
}

bool HuffmanNode::cmp(HuffmanNode *a, HuffmanNode *b)
{
    if (a->weight_ < b->weight_) return true;
    if (a->weight_ == b->weight_)
    {
        if (a->c_ != '\x0' && b->c_ != '\x0') return a->c_ < b->c_;
        if (a->c_ == '\x0' && b->c_ != '\x0') return true;
        if (a->c_ != '\x0' && b->c_ == '\x0') return false;
        /*at this point we know that both a and b are not characters*/
        return a->get_least() < b->get_least();
    }
    return false;
}

void HuffmanNode::get_dictionary(std::ofstream &output, std::string id) //TODO [IMP] Change to return a HashMap an use a binary number as parameter
{
    if (left_) left_->get_dictionary(output, id + "0");
    if (right_) right_->get_dictionary(output, id + "1");
    if (c_ != '\x0')
    {
        output.write((char*)&c_, sizeof(char));
        output << id << std::endl;
    }
}

#endif
