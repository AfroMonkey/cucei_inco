#include <cstring>
#include <fstream>
#include <iostream>

#define INPUT 1
#define KEY 2
#define OUTPUT 3

#define OPTIONS "file key [-d]"

bool check_argc();
bool check_argv();
bool check_file();
void vigenere();

int argc_g;
char **argv_g;
bool decrypt;
std::fstream file;

int main(int argc, char *argv[]) {
  argc_g = argc;
  argv_g = argv;
  decrypt = false;

  if (!check_argc())
    return 1;

  if (!check_argv())
    return 2;

  if (!check_file())
    return 3;

  vigenere();

  return 0;
}

bool check_argc() {
  if (argc_g < 3 || argc_g > 4) {
    std::cout << "Usage: " << argv_g[0] << " " << OPTIONS << '\n';
    std::cout << "d: Decrypt" << '\n';
    return false;
  }
  return true;
}

bool check_argv() {
  if (argc_g == 4) {
    if (!std::strcmp(argv_g[3], "-d")) {
      decrypt = true;
    } else {
      return false;
    }
  }
  return true;
}

bool check_file() {
  file.open(argv_g[1], std::ios::in | std::ios::out);
  return file.is_open();
}

void vigenere() {
  char *key = argv_g[2];
  char c;
  while (true) {
    file.read(&c, sizeof(char));
    if (file.eof())
      break;
    if (!*key)
      key = argv_g[2];
    c += decrypt ? -*key : *key;
    file.seekp(-1, std::ios::cur);
    file.write(&c, sizeof(c));
    ++key;
  }
  file.close();
}
