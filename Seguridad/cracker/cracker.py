# -*- coding: utf-8 -*-
from itertools import product
from subprocess import Popen, PIPE

def attack(program):
    pass_len = 0
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 '
    chars = list(chars)
    while (True):
        pass_len = pass_len + 1
        for password in product(chars, repeat=pass_len):
            password = ''.join(password)
            a = Popen(program + password, shell=True, stdout=PIPE)
            a.communicate()[0]
            if not a.returncode:
                return password
        


if __name__ == '__main__':
    print(attack("./vulnerable "))
