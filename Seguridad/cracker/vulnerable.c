#include <stdio.h>
#include <string.h>

#define PASS "abc"

int main(int argc, char* argv[]) {
	if (argc != 2) {
		printf("Usage: %s PASSWORD\n", argv[0]);
		return -1;
	}
	if (!strcmp(argv[1], PASS)) {
		printf("Loged\n");
		return 0;
	} else {
		printf("Incorrect pass\n");
		return -2;
	}
}
