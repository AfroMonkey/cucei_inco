# Email
**subject: Alerta de Seguridad**

**from: security@service_help.com**

**content:**

Hola email@domain.com!

Hemos detectado varios intentos fallidos de ingreso a tu cuenta de Service. Si no reconoces estos mismos te recomendamos revisar tus tus opciones de seguridad y revisar que no haya cuentas activas sospechosas, para realizar esto haz clic AQUÍ.

Atentamente el equipo de seguridad de Service.

Administrar notificaciones · Ayuda

# Web
Pagina idéntica al inicio se sesión de Service, pero registra la contraseña del usuario en un server.
Posterior al intento de inicio de sesión, re-dirige a la pagina original (en caso de no tener la cuenta ya abierta, volverá a solicitar el inicio de sesión).
