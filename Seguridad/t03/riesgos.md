* El propio dinero del banco y clientes (banca electrónica) (activo disponible)
    * Recibir dinero falso
    * Baja de las acciones adquiridas
    * Robo del dinero


* Datos personales de los clientes (que serían activos realizables)
    * Suplanatación de identidad
    * Robo de información
    * Manipulación de datos


* Historial de movimientos de los clientes (¿realizable?)
    * Manipulación no autorizada de datos


* Información de la organización (organigramas, puestos, sueldos, contratos l
    aborales, legal, etc) (¿intangible?)
    * Omisión de las normas (?)
    * Sanciones legales


* Sistemas de software (activos intangibles)
    * In-disponibilidad del sistema


* Servidores (activos materiales)
    * In-disponibilidad del sistema
    * Ataques externos


* Equipos de comunicación (activos materiales)
    * Intercepción de los mensajes


* Equipos de cómputo (activos materiales)
    * Infección con software malicioso


* Redes de comunicación (activos materiales e intangibles)
    * Infiltración en las redes laborales

* Instalaciones dedicadas al almacenamiento de información (sites, racks, redundancia, etc) (acts materiales)
    * Perdida de información
    * Extracción no autorizada de información


* Personal relacionado
