#include <stdio.h>

int main(int argc, char const *argv[]) {
    FILE *input, *output;
    unsigned char b;
    unsigned char pair;

    if (argc != 3) {
        return 1;
    }

    input = fopen(argv[1], "r");
    output = fopen(argv[2], "w");

    if (input == NULL) {
        perror("Input error");
        return 2;
    }
    if (output == NULL) {
        perror("Output error");
        return 3;
    }

    pair = 0x00;
    while (1) {
        b = fgetc(input);
        if (feof(input)) {
            break;
        }
        b ^= 0xFF;
        b += pair;
        pair = 1 - pair;
        fputc(b, output);
    }

    fclose(input);
    fclose(output);
    return 0;
}
