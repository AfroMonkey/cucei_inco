# Actividad 07
Alumno: Celada Rodriguez Diego Antonio  
Código: 215861517  
Alumno: Navarro Presas Moisés Alejandro  
Código: 215861509  
Carrera: Ingeniería en Computación  
Asignatura: Bases de Datos  
Profesor: Román Godinez Israel  
Seccion: D03  
Ciclo: 2017A  
Act: 02  

## Supuestos
* Cada RFC es único para cada abogado administrador.
* El nombre de las empresas es único.
* Las personas se identifican mediante su CURP.
* Todas las empresas tiene al menos una persona de contacto.
* Un inquilino solo puede habitar una propiedad.
* La sepración de la propiedad en sus sub tipos la realiza un usuario.
* Solo se puede presidir en la comunidad que se habita.
* Una comunidad puede no tener vocales.
* Una cuenta bancaria no puede ser utilizada por varias comunidades.
* Los administradores tienen teléfono de contacto.
* Un recibo se identifica mediante su numero de recibo y su cuenta bancaria.

## ER
![](er.png)
