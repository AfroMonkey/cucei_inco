# Ejercicios entidad-relación

## Sistema de seguimiento de votaciones

#### Suposiciones
* Se puede repetir el nombre de un distrito en diferentes estados.
* Se puede repetir el nombre de un municipio en diferentes distritos.
* Un municipio sólo puede ser representado por un diputado y un diputdo sólo representa un municipio.
* Todos los diputados deben votar en un proyecto de ley.
* Todos los proyectos de ley deben ser promovidos por al menos un diputado.

#### Notas
* Se agregó un identificador a la entidad `diputado`.
* Se agregó un identificador a la entidad `proyecto de ley`.
