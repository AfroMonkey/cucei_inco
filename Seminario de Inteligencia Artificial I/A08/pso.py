from math import e, pi, sqrt, cos
from random import random

SEARCH_RANGE = 10
POPULATION_SIZE = 10
DIMENSIONS = 2
NEIGHBORHOOD_SIZE = 5
PHI1_MAX = 1
PHI2_MAX = 1
MAX_SPEED = SEARCH_RANGE / 2
MAX_ERROR = 0.01
TARGET = 0

particles = []


def ackley(x: float, y: float) -> float:
    return -20 * e ** (-0.2 * sqrt(0.5 * (x ** 2 + y ** 2))) - e ** (0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20


class Particle:
    def __init__(self) -> None:
        self.pos = []
        self.speed = []
        self.best_pos = []
        for d in range(DIMENSIONS):
            self.pos.append(random() * SEARCH_RANGE - SEARCH_RANGE / 2)
            self.speed.append(0)
        self.best_pos = self.pos[:]

    def fitness(self) -> float:
        return ackley(self.pos[0], self.pos[1])

    def best_fitness(self) -> float:
        return ackley(self.best_pos[0], self.best_pos[1])


for p in range(POPULATION_SIZE):
    particles.append(Particle())

particles.sort(key=lambda p: p.fitness())

generation = 1

while (particles[0].fitness() > TARGET + MAX_ERROR):
    print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, particles[0].pos[0], particles[0].pos[1], particles[0].fitness()))
    for i in range(POPULATION_SIZE):
        left_neighbors = NEIGHBORHOOD_SIZE - NEIGHBORHOOD_SIZE // 2
        right_neighbors = NEIGHBORHOOD_SIZE - left_neighbors
        aux = i - left_neighbors
        best_neighbor = particles[aux]
        for _ in range(i + right_neighbors - aux):
            if aux == POPULATION_SIZE:
                aux = 0
            if particles[aux].best_fitness() < best_neighbor.best_fitness():
                best_neighbor = particles[aux]
            aux += 1
        phi1 = random() * PHI1_MAX
        phi2 = random() * PHI2_MAX
        for j in range(len(particles[i].speed)):
            particles[i].speed[j] += phi1 * (particles[i].best_pos[j] - particles[i].pos[j]) + phi2 * (best_neighbor.best_pos[j] - particles[i].pos[j])
            if abs(particles[i].speed[j]) > MAX_SPEED:
                particles[i].speed[j] = MAX_SPEED * (particles[i].speed[j] / abs(particles[i].speed[j]))
            particles[i].pos[j] += particles[i].speed[j]
        if particles[i].fitness() < particles[i].best_fitness():
            particles[i].best_pos = particles[i].pos[:]
    particles.sort(key=lambda p: p.fitness())
    generation += 1
print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, particles[0].pos[0], particles[0].pos[1], particles[0].fitness()))
