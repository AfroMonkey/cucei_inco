from math import e, pi, sqrt, cos
from random import random


def ackley(x: float, y: float) -> float:
    return -20 * e ** (-0.2 * sqrt(0.5 * (x ** 2 + y ** 2))) - e ** (0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20


class Individual:
    def __init__(self):
        self.x = random() * 20 - 10
        self.y = random() * 20 - 10

    def from_parents(self, p1, p2):
        self.x = p1.x / 2 + p2.x / 2
        self.y = p1.y / 2 + p2.y / 2

    def fitness(self):
        return ackley(self.x, self.y)


def get_parents(poblation):
    fitnesses = list(map(lambda i: i.fitness(), poblation))
    max_fitness = max(fitnesses)
    fitnesses = list(map(lambda i: max_fitness - i, fitnesses))
    total_fitnes = sum(fitnesses)
    fp1 = 0
    rp1 = random() * total_fitnes
    p1 = 0
    while fp1 < rp1 and p1 < len(poblation) - 1:
        fp1 += (max_fitness - poblation[p1].fitness())
        p1 += 1
    fp2 = 0
    rp2 = random() * total_fitnes
    p2 = 0
    while fp2 < rp2 and p2 < len(poblation) - 1:
        fp2 += (max_fitness - poblation[p2].fitness())
        p2 += 1
    return (poblation[p1], poblation[p2])


MUTATION_RATE = 0.02
INITIAL_POBLATION = 1000
STOP_CRITERIA = 0.01
ELITE = 2

poblation = []

for i in range(INITIAL_POBLATION):
    poblation.append(Individual())

poblation.sort(key=lambda i: i.fitness())

generation = 1

while (poblation[0].fitness() > STOP_CRITERIA):
    print('Best of generation {} is x={:0.3f}, y={:0.3f}. Fitness={:0.3f}'.format(generation, poblation[0].x, poblation[0].y, poblation[0].fitness()))
    # print('{} {:0.3f} {:0.3f}'.format(generation, poblation[0].fitness(), sum(list(map(lambda i: i.fitness(), poblation))) / len(poblation)))
    childrens = []
    while len(childrens) < len(poblation) - ELITE:
        children = Individual()
        children.from_parents(*get_parents(poblation))
        childrens.append(children)
    for children in childrens:
        if random() <= MUTATION_RATE:
            if random() < 0.5:
                children.x = random() * 20 - 10
            else:
                children.y = random() * 20 - 10
    poblation = sorted(poblation[:ELITE] + childrens, key=lambda i: i.fitness())
    generation += 1
# print('{} {:0.3f} {:0.3f}'.format(generation, poblation[0].fitness(), sum(list(map(lambda i: i.fitness(), poblation))) / len(poblation)))
print('Best after {} generations is x={}, y={}. Fitness={}'.format(generation, poblation[0].x, poblation[0].y, poblation[0].fitness()))
