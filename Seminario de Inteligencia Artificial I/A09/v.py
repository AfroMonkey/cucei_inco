from random import random, sample
from math import e, pi, sqrt, cos
from numpy import array

DIMENSIONS = 2
POPULATION_SIZE = 10
SEARCH_RANGE = 10
TARGET = 0
MAX_ERROR = 0.01
MUTATION_RATE = 0.5
CROSSOVER_RATE = 0.8


def ackley(x, y):
    return -20 * e ** (-0.2 * sqrt(0.5 * (x ** 2 + y ** 2))) - e ** (0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20


class Vector():
    def __init__(self, function, args):
        self.function = function
        self.args = args
        self.pos = array([random() * SEARCH_RANGE - SEARCH_RANGE / 2 for _ in range(DIMENSIONS)])

    def fitness(self):
        return self.function(*[self.pos[a] for a in self.args])

    def get_mutation(self, v2, v3):
        mutant_vector = Vector(self.function, self.args)
        mutant_vector.pos = self.pos + MUTATION_RATE * (v2.pos - v3.pos)
        return mutant_vector

    def cross(self, mutant):
        cross = Vector(self.function, self.args)
        for d in range(DIMENSIONS):
            if random() < CROSSOVER_RATE:
                cross.pos[d] = mutant.pos[d]
            else:
                cross.pos[d] = self.pos[d]
        return cross


vectors = [Vector(ackley, [0, 1]) for _ in range(POPULATION_SIZE)]
vectors.sort(key=lambda v: v.fitness())

generation = 1

while abs(vectors[0].fitness()) > abs(TARGET + MAX_ERROR):
    print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, vectors[0].pos[0], vectors[0].pos[1], vectors[0].fitness()))
    next_generation = []
    for vector in vectors:
        selected_vectors = sample(range(POPULATION_SIZE), 3)
        mutant_vector = vectors[selected_vectors[0]].get_mutation(vectors[selected_vectors[1]], vectors[selected_vectors[2]])
        next_generation.append(vector.cross(mutant_vector))
    for i in range(POPULATION_SIZE):
        if next_generation[i].fitness() < vectors[i].fitness():
            vectors[i] = next_generation[i]
    vectors.sort(key=lambda v: v.fitness())
    generation += 1
print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, vectors[0].pos[0], vectors[0].pos[1], vectors[0].fitness()))
