from random import random, randint, choice
from math import e, pi, sqrt, cos


def ackley(x, y):
    return -20 * e ** (-0.2 * sqrt(0.5 * (x ** 2 + y ** 2))) - e ** (0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20


class Bee():
    def __init__(self):
        self.args = [random() * SEARCH_RANGE - SEARCH_RANGE / 2 for _ in range(n)]

    def fitness(self):
        return ackley(*self.args)


TARGET = 0
MAX_ERROR = 0.01
SEARCH_RANGE = 10

n = 2
N = 100
Pf = int(N / 2)
Po = N - Pf
L = int(N * n / 2)
bees = [Bee() for _ in range(N)]
T = [0 for _ in range(Pf)]


def foragers():
    for i in range(Pf):
        k = i
        while k == i:
            k = randint(0, N - 1)
        s = randint(0, n - 1)
        r = choice([-1, 1]) * random()
        v = Bee()
        v.args[s] = bees[i].args[s] + r * (bees[i].args[s] - bees[k].args[s])
        if v.fitness() < bees[i].fitness():
            bees[i] = v
            T[i] = 0
        else:
            T[i] += 1


def onlookers():
    for i in range(Pf, Pf + Po):
        j = randint(0, Pf - 1)
        k = j
        while k == j:
            k = randint(0, Pf - 1)
        s = randint(0, n - 1)
        r = choice([-1, 1]) * random()
        bees[i].args[s] = bees[j].args[s] + r * (bees[j].args[s] - bees[k].args[s])
        if bees[i].fitness() < bees[j].fitness():
            bees[j] = bees[i]
            T[j] = 0
        else:
            T[j] += 1


def scouts():
    for i in range(Pf):
        if T[i] > L:
            bees[i] = Bee()
            T[i] = 0


generation = 1
while True:
    best = min(bees, key=lambda b: b.fitness())
    if abs(best.fitness()) <= abs(TARGET + MAX_ERROR):
        break
    print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, best.args[0], best.args[1], best.fitness()))
    foragers()
    onlookers()
    scouts()
    generation += 1
