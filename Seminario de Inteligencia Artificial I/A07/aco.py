from math import e, pi, sqrt, cos
from random import random

RANGE = 10
MUTATION_RATE = 0.02
INITIAL_ANTS = 1000
MAX_ERROR = 0.01
TARGET = 0
ELITE = 2
COLONY_RADIUS = 1

ants = []


def ackley(x: float, y: float) -> float:
    return -20 * e ** (-0.2 * sqrt(0.5 * (x ** 2 + y ** 2))) - e ** (0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20


class Ant:
    def __init__(self, queen=None):
        if queen is None:
            self.x = random() * RANGE * 2 - RANGE
            self.y = random() * RANGE * 2 - RANGE
        else:
            self.x = queen.x + random() * COLONY_RADIUS * 2 - COLONY_RADIUS
            self.y = queen.y + random() * COLONY_RADIUS * 2 - COLONY_RADIUS

    def fitness(self) -> float:
        return ackley(self.x, self.y)


for i in range(INITIAL_ANTS):
    ants.append(Ant())

ants.sort(key=lambda i: i.fitness())

generation = 1

while (ants[0].fitness() > TARGET + MAX_ERROR):
    news = []
    while len(news) < len(ants) - ELITE:
        news.append(Ant(ants[0]))
    ants = sorted(ants[:ELITE] + news, key=lambda i: i.fitness())
    generation += 1
print('Best after {} generations is x={}, y={}. Fitness={}'.format(generation, ants[0].x, ants[0].y, ants[0].fitness()))
