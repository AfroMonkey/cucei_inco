from random import random, choice
from math import e, pi, sqrt, cos
from copy import deepcopy

DIMENSIONS = 2
SEARCH_RANGE = 10
TARGET = 0
MAX_ERROR = 0.01
POPULATION_SIZE = 50
RH_SIZE = 8


def ackley(x, y):
    return -20 * e ** (-0.2 * sqrt(0.5 * (x ** 2 + y ** 2))) - e ** (0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20


class Receptor():
    def __init__(self, function):
        self.args = [random() * SEARCH_RANGE - SEARCH_RANGE / 2 for _ in range(DIMENSIONS)]
        self.function = function

    def fitness(self):
        return self.function(*self.args)

    def mutate(self):
        for a in range(len(self.args)):
            self.args[a] += choice([1, -1]) * random() * self.fitness()

    def n_clones(self):
        return int(1 / self.fitness())


receptors = [Receptor(ackley) for _ in range(POPULATION_SIZE)]
receptors.sort(key=lambda v: v.fitness())

generation = 1

while abs(receptors[0].fitness()) > abs(TARGET + MAX_ERROR):
    print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, receptors[0].args[0], receptors[0].args[1], receptors[0].fitness()))
    rh = receptors[:RH_SIZE]
    rm = [deepcopy(r) for r in rh for c in range(r.n_clones())]
    for r in rm:
        r.mutate()
    rr = [Receptor(ackley) for _ in range(POPULATION_SIZE)]
    receptors = sorted(rh + rm + rr, key=lambda v: v.fitness())[:POPULATION_SIZE]
    generation += 1
print("Best solution for generation {} is x={:.4f}, y={:.4f} Fitness={:.6f}".format(generation, receptors[0].args[0], receptors[0].args[1], receptors[0].fitness()))
