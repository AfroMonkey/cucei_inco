org 100h
include 'emu8086.inc'
DEFINE_GET_STRING

.data
i db 0x00h                  ; Utilizado como contador
input db 8 dup(?)           ; Arreglo de 8 bytes

.code
main:
    ; Lectura
    mov di, offset input    ; Inicio del arreglo donde leremos
    mov dx, 0x09h           ; Cantidad de caracteres a leer
    call get_string         ; Llamada a la funcion para leer por teclado
    ; ASCII to HEX
    mov ax, 0x0000h         ; Inicializacion de AX
    mov bx, 0x0000h         ; Inicializacion de BX
    mov i, 0x04h            ; Inicializacion de <i> (utilizado como contador)
ascii2hex:
    sub [di], 0x30h         ; Se restan 30h a cada byte leido
    cmp [di], 0x09h         ; En caso de ser una letra
    jbe next_char
    sub [di], 0x07h         ; se le restan otros 7h
next_char:
    shl ax, 0x04h           ; Se desplaza AX 4 posiciones a la izquierda
    mov cx, [di]            ; Movemos a CX el valor hex del digito leido
    and cx, 0x00FFh         ; Aplicamos una mascara
    add ax, cx              ; AX = AX + CX
    inc di                  ; Incremento del apuntador
    dec i                   ; Decremento del contador
    jnz ascii2hex           ; Si aun no terminamos, vamos por el siguiente

    mov i, 0x04h            ; Inicializacion de <i>
ascii2hex2:                 ; Se aplica el mismo proceso que arriba\
    sub [di], 0x30h         ; pero utilizando el registro BX en lugar de AX
    cmp [di], 0x09h
    jbe next_char2
    sub [di], 0x07h
next_char2:
    shl bx, 0x04h
    mov cx, [di]
    and cx, 0x00FFh
    add bx, cx
    inc di
    dec i
    jnz ascii2hex2

    mov i, 0x09h            ; Contador = 9 (obtiene los ultimos 9 digitos)
next_div:
    mov cl, 0x0Ah           ; Divisor = 10
    call div32_16           ; AX:BX/CX
    push dx                 ; Digitos resultantes de la division
    dec i                   ; Decremento de contador
    jnz next_div            ; Mientras no terminen las divisiones

    push cx                 ; Ultimo digito de la conversion
    mov i, 0x09h            ; Obtiene los primero 9 digitos
    putc 61                 ; Imprime un '='

next_dig:
    pop ax                  ; Recupera los digitos en orden inverso
    add al, 0x30h           ; Se le añade 30h para convertirlo a ascii2hex2
    putc al                 ; Se imprimen en pantall
    dec i                   ; Decremento del contador
    jnz next_dig            ; Mientras haya digitos

    pop ax                  ; Obtiene el ultimo digito
    add al, 0x30h
    putc al

    ret

div32_16 proc
    mov dx, 0x0000h         ; Inicializacionde DX
    div cx                  ; AX = DX:AX/CX     DX = Residuo
    push ax                 ; Almacenamos AX

    mov ax, bx              ; AX = BX
    shr ax, 0x04h           ; AX hacia la derecha 4 posicones
    shl dx, 0x0Ch           ; AX hacia la izquierda Ch posicones
    add ax, dx              ; AX = AX + DX

    mov dx, 0x0000h         ; Mismo proceso que al inicio
    div cx
    shl ax, 0x04h
    push ax

    shl dx, 0x04h
    mov ax, bx
    and ax, 0x000F          ; Mascarfa para los ultimos 4 bits
    add ax, dx

    mov dx, 0x0000h
    div cx
    mov cx, ax
    pop ax
    add ax, cx
    mov bx, ax
    pop ax
    ret
endp
