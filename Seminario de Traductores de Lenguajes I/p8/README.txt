Nota 1: El programa fue probado con Python 3.6.3, no existe un compilado como tal ni mucho menos uno 100% ejecutable; es necesario interpretar en tiempo real.
Nota 2: El programa fue probado con el archivo "hex2dec.asm" y la ejecución fue exitosa
Nota 3: El uso del programa requiere pasar la ruta del archivo a analiza de la siguiente manera:
  ./analyzer.py file
O bien
  python analyzer.py file
