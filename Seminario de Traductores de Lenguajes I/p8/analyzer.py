#!/usr/bin/python

# Navarro Presas Moises Alejandro - 215861509

from itertools import product
import sys

registers = {
    'AH': 'rb',
    'AL': 'rb',
    'AX': 'rw',
    'BH': 'rb',
    'BL': 'rb',
    'BP': 'rs',
    'BX': 'rw',
    'CH': 'rb',
    'CL': 'rb',
    'CS': 'rs',
    'CX': 'rw',
    'DH': 'rb',
    'DI': 'rs',
    'DL': 'rb',
    'DS': 'rs',
    'DX': 'rw',
    'ES': 'rs',
    'IP': 'rs',
    'SI': 'rs',
    'SP': 'rs',
    'SS': 'rs',
}


mnemonics = {
    'AAA': {
        'na': {
            'na': 1,
        },
    },
    'AAD': {
        'na': {
            'na': 2,
        },
    },
    'AAM': {
        'na': {
            'na': 2,
        },
    },
    'AAS': {
        'na': {
            'na': 1,
        },
    },
    'CBW': {
        'na': {
            'na': 1,
        },
    },
    'CLC': {
        'na': {
            'na': 1,
        },
    },
    'CLD': {
        'na': {
            'na': 1,
        },
    },
    'CLI': {
        'na': {
            'na': 1,
        },
    },
    'CMC': {
        'na': {
            'na': 1,
        },
    },
    'CMPSB': {
        'na': {
            'na': 1,
        },
    },
    'CMPSW': {
        'na': {
            'na': 1,
        },
    },
    'CWD': {
        'na': {
            'na': 1,
        },
    },
    'DAA': {
        'na': {
            'na': 1,
        },
    },
    'DAS': {
        'na': {
            'na': 1,
        },
    },
    'HLT': {
        'na': {
            'na': 1,
        },
    },
    'INTO': {
        'na': {
            'na': 1,
        },
    },
    'IRET': {
        'na': {
            'na': 1,
        },
    },
    'LAHF': {
        'na': {
            'na': 1,
        },
    },
    'LODSB': {
        'na': {
            'na': 1,
        },
    },
    'LODSW': {
        'na': {
            'na': 1,
        },
    },
    'MOVSB': {
        'na': {
            'na': 1,
        },
    },
    'MOVSW': {
        'na': {
            'na': 1,
        },
    },
    'NOP': {
        'na': {
            'na': 1,
        },
    },
    'POPA': {
        'na': {
            'na': 1,
        },
    },
    'POPF': {
        'na': {
            'na': 1,
        },
    },
    'PUSHA': {
        'na': {
            'na': 1,
        },
    },
    'PUSHF': {
        'na': {
            'na': 1,
        },
    },
    'RET': {
        'na': {
            'na': 1,
        },
    },
    'RETF': {
        'na': {
            'na': 1,
        },
    },
    'SAHF': {
        'na': {
            'na': 1,
        },
    },
    'SCASB': {
        'na': {
            'na': 1,
        },
    },
    'SCASW': {
        'na': {
            'na': 1,
        },
    },
    'STC': {
        'na': {
            'na': 1,
        },
    },
    'STD': {
        'na': {
            'na': 1,
        },
    },
    'STI': {
        'na': {
            'na': 1,
        },
    },
    'STOSB': {
        'na': {
            'na': 1,
        },
    },
    'STOSW': {
        'na': {
            'na': 1,
        },
    },
    'XLATB': {
        'na': {
            'na': 1,
        },
    },
    'CALL': {
        'np': {
            'na': 3,
        },
        'rw': {
            'na': 2,
        },
    },
    'DEC': {
        'AX': {
            'na': 1,
        },
        'BP': {
            'na': 1,
        },
        'BX': {
            'na': 1,
        },
        'CX': {
            'na': 1,
        },
        'DI': {
            'na': 1,
        },
        'DX': {
            'na': 1,
        },
        'rb': {
            'na': 2,
        },
        'mb': {
            'na': 4,
        },
        'rw': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
        'SI': {
            'na': 1,
        },
        'SP': {
            'na': 1,
        },
    },
    'IDIV': {
        'rb': {
            'na': 2,
        },
        'mb': {
            'na': 4,
        },
        'rw': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
    },
    'IMUL': {
        'mw': {
            'na': 4,
        },
        'rb': {
            'na': 4,
        },
        'rd': {
            'ib': 3,
            'id': 6,
        },
        'rw': {
            'ib': 4,
            'na': 4,
            'iw': 4,
        },
    },
    'INC': {
        'AX': {
            'na': 1,
        },
        'CX': {
            'na': 1,
        },
        'DX': {
            'na': 1,
        },
        'BX': {
            'na': 1,
        },
        'SP': {
            'na': 1,
        },
        'BP': {
            'na': 1,
        },
        'SI': {
            'na': 1,
        },
        'DI': {
            'na': 1,
        },
        'rb': {
            'na': 2,
        },
        'mb': {
            'na': 4,
        },
        'rw': {
            'na': 2,
        },
        'mw': {
            'na': 4,
        },
    },
    'INT': {
        '3': {
            'na': 1,
        },
        'ib': {
            'na': 2,
        },
    },
    'JB': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JBE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JC': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JCXZ': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JG': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JGE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JL': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JLE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JMP': {
        'sl': {
            'na': 2,
        },
        'll': {
            'na': 4,
        },
        'np': {
            'na': 3,
        },
        'rw': {
            'na': 4,
        },
        'iw': {
            'na': 4,
        },
        'fp': {
            'na': 5,
        },
    },
    'JNA': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNAE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNB': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNBE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNC': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNG': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNGE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNL': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNLE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNO': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNP': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNS': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JNZ': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JO': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JP': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JPE': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JPO': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JS': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'JZ': {
        'll': {
            'na': 4,
        },
        'sl': {
            'na': 2,
        },
    },
    'LOOPE': {
        'sl': {
            'na': 2,
        },
    },
    'LOOPNE': {
        'sl': {
            'na': 2,
        },
    },
    'LOOPNZ': {
        'sl': {
            'na': 2,
        },
    },
    'LOOPZ': {
        'sl': {
            'na': 2,
        },
    },
    'MUL': {
        'rb': {
            'na': 2,
        },
        'rw': {
            'na': 2,
        },
        'ib': {
            'na': 3,
        },
        'iw': {
            'na': 4,
        },
    },
    'NEG': {
        'rb': {
            'na': 2,
        },
        'rw': {
            'na': 2,
        },
        'mb': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
    },
    'NOT': {
        'rb': {
            'na': 2,
        },
        'rw': {
            'na': 2,
        },
        'mb': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
    },
    'POP': {
        'rb': {
            'na': 1,
        },
        'rw': {
            'na': 1,
        },
        'mb': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
    },
    'PUSH': {
        'rb': {
            'na': 1,
        },
        'rw': {
            'na': 1,
        },
        'mb': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
    },
    'ADC': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
    'ADD': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
    'AND': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
    'CMP': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
    'DIV': {
        'rb': {
            'na': 2,
        },
        'mb': {
            'na': 4,
        },
        'rw': {
            'na': 4,
        },
        'mw': {
            'na': 4,
        },
    },
    'IN': {

        'AL': {
            'ib': 2,
            'DX': 1
        },
        'AX': {
            'ib': 2,
            'DX': 1
        }
    },
    'LDS': {
        'rw': {
            'md': 4,
        },
    },
    'LEA': {
        'rw': {
            'mw': 3,
        },
    },
    'LES': {
        'rw': {
            'md': 4,
        },
    },
    'MOV': {
        'AL': {
            'rb': 3,
            'mb': 3,
            'ib': 2,
        },
        'AX': {
            'rw': 3,
            'mw': 3,
            'iw': 3,
        },
        'AH': {
            'ib': 2,
        },
        'CL': {
            'ib': 2,
        },
        'CH': {
            'ib': 2,
        },
        'CX': {
            'iw': 3,
        },
        'DL': {
            'ib': 2,
        },
        'DH': {
            'ib': 2,
        },
        'DX': {
            'iw': 3,
        },
        'BL': {
            'ib': 2,
        },
        'BH': {
            'ib': 2,
        },
        'BX': {
            'iw': 3,
        },
        'SP': {
            'iw': 3,
        },
        'BP': {
            'iw': 3,
        },
        'SI': {
            'iw': 3,
        },
        'DI': {
            'iw': 3,
        },
        'rb': {
            'rb': 2,
            'mb': 4,
            'rb': 2,
            'AL': 3,
            'ib': 3,
        },
        'mb': {
            'rb': 4,
            'AL': 3,
            'ib': 5,
        },
        'rw': {
            'AX': 3,
            'iw': 4,
            'rw': 2,
            'rw': 2,
            'mw': 4,
            'sr': 2,
        },
        'mw': {
            'AX': 3,
            'iw': 6,
            'rw': 4,
            'sr': 4,
        },
        'sr': {
            'rw': 2,
            'mw': 4,
        },
    },
    'OR': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
    'OUT': {
        'dx': {
            'AX': 1,
        },
        'ib': {
            'AX': 2,
        },
    },
    'RCL': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'RCR': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'ROL': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'ROR': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'SAL': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'SAR': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'SBB': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'SHL': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'SHR': {
        'rb': {
            'ib': 2,
            'CL': 2,
        },
        'rw': {
            'ib': 2,
            'CL': 2,
        },
        'mb': {
            'ib': 4,
            'CL': 4,
        },
        'mw': {
            'ib': 4,
            'CL': 4,
        },
    },
    'SUB': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
    'TEST': {
        'AL': {
            'ib': 2,
        },
        'AX': {
            'iw': 3,
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4,
        },
        'mb': {
            'ib': 5,
            'rb': 4,
            'mb': 4,
        },
        'rw': {
            'iw': 4,
            'rw': 2,
            'mb': 4,
        },
        'mw': {
            'iw': 6,
            'rw': 4,
            'mw': 4,
        },
    },
    'XCHG': {
        'rb': {
            'rb': 2,
            'mb': 4,
            'mw': 4,
        },
        'rw': {
            'rw': 2,
            'mb': 4,
            'mw': 4,
        },
        'mb': {
            'rb': 4,
            'rw': 4,
        },
        'mw': {
            'rb': 4,
            'rw': 4,
        },
    },
    'XOR': {
        'al': {
            'ib': 2,
            'rb': 2,
            'mb': 4
        },
        'ax': {
            'ib': 3,
            'iw': 3,
            'rw': 2,
            'mw': 4
        },
        'rb': {
            'ib': 3,
            'rb': 2,
            'mb': 4
        },
        'rw': {
            'ib': 3,
            'iw': 4,
            'rw': 2,
            'mw': 4
        },
        'mb': {
            'ib': 5,
            'rb': 4
        },
        'mw': {
            'ib': 5,
            'iw': 6,
            'rw': 4
        }
    },
}


def tokenize(line):
    # Separa los elementos de la cadena en sus tokens
    tokens = line.replace(',', '').split(' ')
    mne = tokens[0]  # El primer token es el mnemonico
    arg1 = 'na'  # Se asigna un valor por defecto
    arg2 = 'na'  # Se asigna un valor por defecto
    if len(tokens) > 1:  # Obtiene los demas argumentos
        arg1 = tokens[1]
        if len(tokens) > 2:
            arg2 = tokens[2]
    return mne, arg1, arg2  # Retorna una tupla con los tokens


def get_arg_type(arg):
    if arg not in registers and arg != 'na':
        if '0x' in arg:  # Si es numerico
            if '[' in arg:
                arg = 'm' + arg[1:-1]
            else:
                arg = 'i' + arg
            if len(arg) == 5:  # Si la longitud es de cuatro es tomado como byte
                arg = arg[0] + 'b'
            else:  # De lo contrario son dos bytes
                arg = arg[0] + 'w'
        else:
            arg = 'll'
    return arg


def analyze(line):
    (mne, arg1, arg2) = tokenize(line)
    arg1 = get_arg_type(arg1)
    arg2 = get_arg_type(arg2)
    p_arg1 = []
    p_arg2 = []
    if arg1 in registers:
        p_arg1.append(arg1)
        p_arg1.append(registers[arg1])
    else:
        if arg1[0] == 'm':
            p_arg1.append('mb')
            p_arg1.append('mw')
        else:
            p_arg1.append(arg1)
    if arg2 in registers:
        p_arg2.append(arg2)
        p_arg2.append(registers[arg2])
    else:
        if arg2[0] == 'm':
            p_arg2.append('mb')
            p_arg2.append('mw')
        else:
            p_arg2.append(arg2)
    for p_args in product(p_arg1, p_arg2):
        if p_args[0] in mnemonics[mne] and p_args[1] in mnemonics[mne][p_args[0]]:
            return mnemonics[mne][p_args[0]][p_args[1]]


if __name__ == '__main__':
    if len(sys.argv) == 2:
        input_file = sys.argv[1]  # Nombre de la archivo a analizar
        with open(sys.argv[1], 'r') as assembler_file:
            with open(sys.argv[1] + '.list', 'w') as list_file:
                total_size = 0
                actual_dir = 0
                for (num, line) in enumerate(assembler_file, 1):  # Para cada linea del archivo
                    if ':' in line:  # Si contiene ':' es considerado etiqueta
                        list_file.write(
                            '[' + format(num, '5') + '] ' + ' ' * 12 + line)
                    elif line.split(' ')[0] == 'ORG':
                        # Convierte el hexadecimal a decimal
                        actual_dir = int(line.split(' ')[1], 16)
                        list_file.write(
                            '[' + format(num, '5') + '] ' + ' ' * 12 + line)
                    else:  # Si no es etiqueta
                        # Se analiza la linea y se obtiene su tamaño
                        size = analyze(line[:-1])
                        # Se escrbe la linea indicando el tamaño
                        total_size += size  # Se aumenta el contador de tamaño
                        list_file.write('[' + format(num, '5') + '] ' + format(actual_dir, '08X') +
                                        ': ' + str(size) + ' ' + line)
                        actual_dir += size  # Se aumenta el contador de tamaño
                # Escribe el numero de bytes empleados
                list_file.write('Total:    ' + str(total_size) + '\n')
    else:
        print('Usage: ' + sys.argv[0] + ' file')
