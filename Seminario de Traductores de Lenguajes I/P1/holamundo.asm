name "holamundo"

org 100h
    mov al, 01h ; Asigna el valor 01h al registro al
    mov bh, 00h ; Asigna el valor 00h al registro bh
    mov bl, 9Fh ; Indica el formato del texto (Texto blanco sobre fondo azul claro)
    mov cx, msg2 - offset msg1 ; Indica la cantidad de caracteres del primer mensaje
    mov dl, 07h ; La cantidad de columnas antes de que comience el primer mensaje
    mov dh, 0Bh ; Indica la cantidad de filas antes de que comience el primer mensaje
    push cs ; No se puede asignar directamente CS a ES, se usa el Stack como auxiliar
    pop es ; Aqui se recupera el valor de CS
    mov bp, offset msg1 ; Indica la direccion del primer mensaje
    mov ah, 13h ; Indica que hacer a la siguiente interrupcion
    int 10h ; Se ejecuta la interrupcion 10h con el "argumento" 13h
    mov cx, msgend - offset msg2
    mov dl, 24h
    mov dh, 0Dh
    mov bp, offset msg2
    mov ah, 13h
    int 10h
    jmp msgend ; El curso del programa brinca a la seccion msgend

msg1    db "Hola, seminario de solucion de problemas de traductores de lenguaje 1" ; Primer mensaje
msg2    db "Seccion D05" ; Contenido del segundo mensaje

msgend:
    mov ah, 00h ; Con este valor el sistema espera una entrada de teclado
    int 16h ; Se ejecuta la interrupcion 16h con el "argumento" 00h
    int 20h ; Finaliza el programa
