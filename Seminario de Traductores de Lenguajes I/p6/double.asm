org 0x100
.data
i db ?						; Contador
buffer db 7 dup(?)			; Buffer para entrada y salida de datos
n1 dw 2 dup(?)				; Primer numero a leer
n2 dw 2 dup(?)				; Segundo numero a leer

.code
main:
	mov bx, offset buffer 	; Posicionamiento de BX al inicio del buffer
	mov [bx], 0x05			; Indica la cantidad de elementos a leer
	call get_string			; Llamada a <get_string>
	call print_string		; Llamada a <print_string>
	call string_to_num		; Llamada a <string_to_num>
	mov n1, ax				; La parte menos signf se coloca en <n1>
	mov n1 + 2, dx			; La parte mas signf se coloca en <n1> + 2

	mov bx, offset buffer 	; Mismo procedimieto, pero para <n2>
	mov [bx], 0x05			;
	call get_string			;
	call print_string		;
	call string_to_num		;
	mov n2, ax				;
	mov n2 + 2, dx			;

	add ax, n1				; Suma las partes menos signf
	jnc no_carry			; Si hay carry &
	inc dx					;  se incrementa en una unidad a DX (mas signf)
no_carry:
	add dx, n1 + 2			; Suma las partes mas signf

	mov si, 0x0006			; Indica la cantidad de elementos a limipiar
clear_buffer:
	mov [buffer + si], '0'	; Coloca '0' en todo el buffer
	dec si					; Decrementa SI
	jnz clear_buffer		; Mientras no haya finalizado

	mov si, 0x06			; Indica la cantidad de elementos en el buffer
	mov cx, 0x000A			; Divide entre 10 para convertir a decimal
hex2dec:					; Coloca el resultado de la suma en el buffer
	div cx					; AX = AX / CX & DL = AX mod CX
	add dl, 0x30			; Covierte a su equivalente en ASCII
	mov [buffer + si], dl 	; Coloca el digito en el buffer
	dec si					; Decrementa SI
	mov dx, 0x0000			; Limpiar DX
	cmp ax, 0x0000			; Mientras haya AX
	jnz hex2dec

	mov bx, offset buffer 	; Mueve BX al inicio del buffer
	mov [bx], 0x06			; Indica la cantidad de elementos a imprimir
	call print_string		; Llamada a <print_string>

	ret

string_to_num proc			; Convierte el buffer en su equivalente hex DX:AX
	push bx					; Resguardo de BX
	push cx					; Resguardo de CX
	push bp					; Resguardo de BP

	mov ax, 0x0000			; Inicializa AX
	mov dx, 0x0000			; Inicializa DX
	mov cl, [bx]			; Indica la cantidad de caracteres en buffer
	dec cl					; Decrementa CL
	inc bx					; Coloca BX al inicio de la cadena
	mov bp, 0x000A
next_digit:					;
	add al, [bx]			; Lee el digito mas significativo
	sub al, 0x30			; Convierte a su equivalente en numero
	mul bp					; Multiplica por 10
	inc bx					; Mueve BX a la siguiente posicion de la cadena
	dec cl					; Decrementa CL
	jnz next_digit			; Mientras haya digitos
	add al, [bx]			; Repite el proceso para el ultimo digito
	sub al, 0x30

	pop bp					; Recupera BP
	pop cx					; Recupera CX
	pop bx					; Recupera BX
	ret
endp

print_string proc			; Imprime una cadena en pantalla
	push ax					; Resguardo de AX
	push bx					; Resguardo de BX
	push cx					; Resguardo de CX
	push dx					; Resguardo de DX
	push bp					; Resguardo de BP

	mov al, 0x01			; Argumento para la interrupcion
	mov dx, 0x0000			; Fila 0 & Columna 0
	mov cx, [bx]			; Coloca en CX la cantidad de caracteres
	and cx, 0x00FF			; Le aplica a CX una mascara
	mov bp, bx				; BP = BX = inicio del buffer
	inc bp					; Coloca BP al inicio de la cadena
	mov bh, 0x00			; BH = 0
	mov bl, 0x0F			; Texto blanco sobre fondo negro
	mov ah, 13h				; Argumento para la interrupcion
	int 10h					; Interrupcion 10

	pop bp					; Recupera BP
	pop dx					; Recupera DX
	pop cx					; Recupera CX
	pop bx					; Recupera BX
	pop ax					; Recupera AX

	ret
endp

get_string proc				; Obtiene una cadena del teclado
	push ax					; Resguardo de AX
	push bx					; Resguardo de BX
	push cx					; Resguardo de CX
	mov cx, [bx]			; Indica la cantidad de caracteres a leer
next_char:
	inc bx					; Coloca BX al inicio de la cadena
	mov ah, 0x00			; Limpia AH
	int 0x16				; Interrupcion 0x16
	mov [bx], al			; Mueve el caracter a una posicion del buffer
	dec cl					; Decrementa DX
	jnz next_char			; Mientras haya caracter por leer

	pop cx					; Recupera CX
	pop bx					; Recupera BX
	pop ax					; Recupera AX
	ret
endp
