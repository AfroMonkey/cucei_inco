org 100h

.data
base dw 0000h
exp db 00h
res dw 0000h

.code
main:
    mov bx, [base]
    mov cl, [exp]
    call pow
    mov res, ax
    ret

pow proc
    mov ax, 0001h  
    cmp cl, 0h
    jz pow_end
pow_start:
    mul bx
    dec cl
    jnz pow_start
pow_end: 
    ret
endp