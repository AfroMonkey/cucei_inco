org 100h
.data
i db 0x09h

.code
main:
    mov ax, 0x0000h
    mov bx, 0xffffh     
next_div:
    mov cl, 0x0Ah
    call div32_16
    push dx
    dec [i]
    jnz next_div
    
    push cx 
    mov [i], 0x09h
next_dig:
    pop dx
    dec [i]
    jnz next_dig
    
    pop dx
    ret

div32_16 proc
    mov dx, 0x0000h
    div cx
    push ax
    
    mov ax, bx
    shr ax, 0x04h
    shl dx, 0x0Ch
    add ax, dx
    
    mov dx, 0x0000h
    div cx         
    shl ax, 0x04h
    push ax
    
    
    shl dx, 0x04h
    mov ax, bx
    and ax, 00000000000001111b
    add ax, dx
    
    mov dx, 0x0000h
    div cx
    mov cx, ax
    pop ax
    add ax, cx
    mov bx, ax
    pop ax
    ret
endp