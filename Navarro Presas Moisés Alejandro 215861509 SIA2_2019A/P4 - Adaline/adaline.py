import numpy as np


class Neuron:
    def __init__(self, n_inputs):
        self.weights = None
        self.reinit(n_inputs)
        self.z = lambda inputs: self.weights.T @ inputs
        self.f = lambda z: z

    def reinit(self, n_inputs=None):
        if n_inputs is None:
            if self.weights is not None:
                n_inputs = len(self.weights)
            else:
                raise Exception('Cant reinit')
        self.weights = np.random.rand(n_inputs) * 2 - 1

    def eval(self, inputs):
        return self.f(self.z(inputs))


class Perceptron:
    def __init__(self, arch):
        self.neuron = Neuron(arch['n_inputs'])
        self.eta = arch.get('eta', 0.1)
        self.max_error = arch.get('max_error', 0.001)
        self.max_epochs = arch.get('max_epochs', 1000)

    def fit(self, tests):
        global_error = 0
        for test in tests:
            error = test[1] - self.neuron.eval(test[0])
            self.neuron.weights += test[0] * error * self.eta
            global_error += error * error / 2
        print(self.neuron.weights)
        return global_error

    def train(self, tests):
        epochs = 0
        while epochs < self.max_epochs:
            epochs += 1
            if self.fit(tests) < self.max_error:
                break
        return epochs


tests = [
    [np.array([0, 2, 0, 2]),  10],
    [np.array([1, 0, 0, 0]),  8],
    [np.array([0, 0, 1, 1]),  3],
    [np.array([0, 0, 1, 0]),  2],
    [np.array([0, 0, 0, 0]),  0],
    [np.array([0, 0, 0, 1]),  1],
    [np.array([0, 0, 0, 2]),  2],
    [np.array([0, 1, 1, 1]),  7],
    [np.array([0, 1, 2, 1]),  9],
    [np.array([1, 0, 0, 2]),  10],
]

for test in tests:
    test[0] = np.append(test[0], 1)

p = Perceptron({'n_inputs': len(tests[0][0])})
print(p.train(tests))
while True:
    point = [int(c) for c in input('>') + '1']
    print(p.neuron.eval(point))
