import numpy as np


class Neuron:
    def __init__(self, n_inputs):
        self.weights = None
        self.reinit(n_inputs)
        self.z = lambda inputs: self.weights.T @ np.array(inputs)
        self.f = lambda z: int(z > 0)

    def reinit(self, n_inputs=None):
        if n_inputs is None:
            if self.weights is not None:
                n_inputs = len(self.weights) - 1
            else:
                raise Exception('Cant reinit')
        self.weights = np.random.rand(n_inputs + 1) * 2 - 1

    def evaluate(self, inputs):
        return self.f(self.z(inputs + [1]))


class Perceptron:
    def __init__(self, arch):
        self.neuron = Neuron(arch['n_inputs'])

    def fit(self, inputs, error):
        self.neuron.weights = self.neuron.weights + (error * np.append(inputs, [1]))

    def train(self, tests):
        current = 0
        success = 0
        while success != len(tests):
            actual = self.neuron.evaluate(tests[current][0])
            error = tests[current][1] - actual
            if not error:
                success += 1
            else:
                self.fit(tests[current][0], error)
                success = 0
            current += 1
            current %= len(tests)


tests = [
    #   ([P, R, F], M)
    # ([150, 20, 40], 0),
    # ([225, 30, 70], 0),
    # ([300, 40, 100], 0),
    # ([150, 50, 80], 0),
    # ([175, 75, 85], 0),
    # ([200, 100, 90], 0),
    ([2.000, 0.80, 0.40], 0),
    ([2.500, 0.85, 0.45], 0),
    ([3.000, 0.90, 0.50], 1),
    ([5.500, 0.95, 0.55], 1),
    ([8.000, 1.00, 0.60], 1),
    ([9.000, 1.00, 0.60], 0),
    # ([8.500, 1.05, 0.65], 0),
    # ([9.000, 1.10, 0.70], 0),
]

p = Perceptron({'n_inputs': len(tests[0][0])})
print("Training")
p.train(tests)
print("Trained")
while True:
    point = []
    for d in range(len(tests[0][0])):
        point.append(float(input(">")))
    print(p.neuron.evaluate(point))
