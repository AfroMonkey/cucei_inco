# Sistema Operativo de Red
Es aquel SO que mantiene a dos o más computadoras unidos a travez de algún medio de comunicación con el betivo de compartir los diferentes recursos y a información del sistema. En este entorno, cada ordenador mantiene su propio SO y su propio sistema de archivos local.

Se dividen en dos grupos:
* Modelo Cliente/Servidor
* Modelo entre iguales (P2P)

Se encarga de gestionar a los usuarios.

Características:
* Gestión de Usuarios
* Gestión de la Red
* Bloqueo de Archivos y Registros
* Distribución de Espacio en Discos Duros
* Compartición de recursos

# Servidor
Computadora que permite compartir sus recursos con otras computadoras conectadas con el.

Tipos:
* Servidor proxy
* Servidor de archivos
* Servidor de impresión
* Servidor de comunicaciones
* Servidor de correo electrónico
* Servidor web
* Servidor FTP

## Modelo de referencia OSI
Desarrollado en 1980 por la ISO L. es una normativa formada por 7 capas que define las diferentes fases por las que deben pasar los datos para viajar de un dispositivo a otro sobre un red de comunicaciones

### Capa física
Capa mas baja del modelo OSI. Es la que se encarga del medio físico y de la forma en que se transmiten los datos.

### Capa de Enlace de Datos
Se ocupa del direccionamiento físico, del acceso al medio, de la detección de errores, de la distribución ordenada de tramas y del control del flujo.

### Capa de Red
Se encarga de identificar el enrutamiento existente entre una o mas redes. Las unidades de datos se denominan paquetes, se pueden clasificar en protocolos enrutables y protocolos de enrutamiento.

### Capa de transporte
Encargada de efectuar el transporte de los datos de la maquina origen a la de destino, independientemente del tipo de red física que se este utilizando.

### Capa de Sesión
Se encarga de mantener y controlar el enlace establecido entre dos computadoras que estan intercambiando datos.

### Capa de presentación
Su objetivo es encargarse de la representación de la información de manera que, aun que distintos equipos puedan tener diferentes representaciones internas de los caracteres, los datos lleguen de manera reconocible.

### Capa de Aplicación
Ofrece a las aplicaciones la posibilidad de acceder a los servicios de las demas capas y define los protocolos que utilizan las aplicaciones para intercambiar datos.

## Capas de modelo de internet
### TODO
daniel.landa@academicos.udg.mx
SOR Act1 Navarro Moises
