
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <queue>

#include "trie.h"

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif


int main()
{
    Trie trie;
    std::string s;
    const unsigned int MAX_WORD_SIZE = 50;
    char holder[MAX_WORD_SIZE];

    std::fstream dictionary;
    dictionary.open("spanish.lex", std::ios::in);

    system(CLEAR);

    if (!dictionary.is_open())
    {
        std::cout << " Error al cargar el diccionario." << std::endl;
        return -1;
    }
    while (dictionary.getline(holder, 50, '\n'))
    {
        s.assign(holder);
        trie.add(s);
    }

    std::cout << " Hay " << trie.words() << " palabras en el diccionario." << std::endl;
    std::cout << " Presiona enter para continuar..." << std::endl;
    std::cin.get();


    s = "";
    while (true)
    {
        system(CLEAR);
        std::cout << " Diccionario en español usando estructura trie" << std::endl;
        std::cout << " (Ingresa 'exit' para salir)" << std::endl << std::endl;

        std::cout << " Palabra: ";
        std::getline(std::cin, s);
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);

        if (s == "exit") break;

        if (trie.check(s))
        {
            std::cout << " La palabra '" << s << "' se encuentra en el diccionario." << std::endl << std::endl;
        }
        else
        {
            std::cout << " La palabra  '" << s << "' no esta en el diccionario." << std::endl << std::endl;
        }


        std::string temp = s;
        if (s.size() >= 8)
        {
             s.erase(s.end() - 4, s.end());
        }
        else if (s.size() > 4)
        {
            s.erase(s.end() - 1);
        }

        std::cout << " Otras palabras similares en el diccionario:" << std::endl;

        auto cmp = [](WORD_TIMES a, WORD_TIMES b) { return a.second < b.second;};
        std::priority_queue<WORD_TIMES, std::vector<WORD_TIMES>, decltype(cmp)> q(cmp);
        for (auto e : trie.possibleWords(s))
        {
            // if (temp == e.first) continue;
            // std::cout << " - " << e.first << std::endl;
            q.push(e);
        }
        for (int i = 0; i < 5 && !q.empty(); ++i)
        {
            std::cout << " - " << q.top().first << std::endl;
            q.pop();
        }


        std::cout << std::endl;
        std::cout << " Presiona enter para continuar..." << std::endl;
        std::cin.get();
    }

    std::cout << " Hasta la vista!" << std::endl << std::endl;
    return 0;
}
