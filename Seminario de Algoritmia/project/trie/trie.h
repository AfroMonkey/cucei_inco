
#ifndef TRIE_H
#define TRIE_H

#include <list>
#include <string>
#include <utility>

#define WORD_TIMES std::pair<std::string, unsigned>

// english alphabet
const unsigned int ALPHABET_SIZE = 26;

class TrieNode
{
public:
    TrieNode();
    ~TrieNode();
    unsigned terminator_;
    TrieNode** next_;
};

TrieNode::TrieNode()
{
    terminator_ = 0;
    next_ = new TrieNode*[ALPHABET_SIZE];
    for (unsigned int i = 0; i < ALPHABET_SIZE; ++i) next_[i] = nullptr;
}

TrieNode::~TrieNode()
{
    delete[] next_;
}

class Trie
{
public:
    Trie();
    ~Trie();
    std::size_t words();
    bool add(std::string s);
    bool check(std::string s);
    std::list<WORD_TIMES> possibleWords(std::string s);
    void trim();
private:
    void trim(TrieNode* node);
    std::list<unsigned int> char_indices(std::string s);
    TrieNode* tail(std::string s);
    void push_words(TrieNode* start, std::string s, std::list<WORD_TIMES>& l);
    std::size_t words_;
    TrieNode* root_;
};

Trie::Trie()
{
    root_ = new TrieNode;
    words_ = 0;
}

Trie::~Trie()
{
    trim();
}

std::size_t Trie::words()
{
    return words_;
}

bool Trie::add(std::string s)
{
    // string is empty, nothing to add
    if (s.empty()) return false;

    std::list<unsigned int> indices = char_indices(s);

    // not every character in s was a valid character
    if (indices.size() != s.size()) return false;

    TrieNode* curr_char = root_;
    for (auto idx : indices)
    {
        if (!curr_char->next_[idx])
        {
            curr_char->next_[idx] = new TrieNode;
        }
        else
        {
        }
        curr_char = curr_char->next_[idx];
    }
    curr_char->terminator_ = 1;
    words_++;
    return true;
}

bool Trie::check(std::string s)
{
    TrieNode* tl = tail(s);
    if (tl) return tl->terminator_++;
    return false;

}

std::list<WORD_TIMES> Trie::possibleWords(std::string s)
{
    std::list<WORD_TIMES> possible_words;
    TrieNode* tl = tail(s);
    if (tl)
    {
        push_words(tl, s, possible_words);
    }
    return possible_words;
}

void Trie::trim()
{
    trim(root_);
}

void Trie::trim(TrieNode* node)
{
    if (!node) return;
    for (unsigned int i = 0; i < ALPHABET_SIZE; ++i) trim(node->next_[i]);
    delete node;
}

std::list<unsigned int> Trie::char_indices(std::string s)
{
    std::list<unsigned int> indices;

    // for each character in string, check that it is a valid
    // character in the english alphabet, if so add it's index
    // value (a = 0, z = 25)
    for (char c : s)
    {
        if (c < 'a' || c > 'z') break;
        indices.push_back(c - 'a');
    }
    return indices;
}

TrieNode* Trie::tail(std::string s)
{
    if (s.empty()) return nullptr;

    std::list<unsigned int> indices = char_indices(s);

    // not every character in s was a valid character
    if (indices.size() != s.size()) return nullptr;

    TrieNode* curr_char = root_;
    for (auto idx : indices)
    {
        if (!curr_char->next_[idx])
        {
            return nullptr;
        }
        curr_char = curr_char->next_[idx];
    }

    return curr_char;
}

void Trie::push_words(TrieNode* start, std::string s, std::list<WORD_TIMES>& l)
{
    if (!start) return;
    if (l.size() >= 10) return;
    if (start->terminator_ > 0) l.push_back(WORD_TIMES(s, start->terminator_));
    for (unsigned i = 0; i < 25; ++i)
    {
        if (start->next_[i])
        {
            push_words(start->next_[i], s + (char)('a' + i), l);
        }
    }
}


#endif // TRIE_H
