/*
 * Autor: Navarro Presas Moises Alejandro
 * Numero de practica:
 * Nombre de practica:
 * Fecha: 2015/11/18
 * Version: 1
 * Tiempo: :
 */

#include <iostream>
#include <cstdlib>
#include "avl.hpp"
#include "dog.hpp"

using namespace std;

#ifdef _WIN32
#define CLEAR "cls"
#else
#define CLEAR "clear"
#endif

#define OPT_EXIT 0
#define OPT_ROOT 1
#define OPT_INSERT 2
#define OPT_REMOVE 3
#define OPT_PREORDER 4
#define OPT_ORDER 5
#define OPT_POSTORDER 6
#define OPT_SEARCH 7

void print(Dog data);

int main() {
    Avl<Dog> avl;
    int opc;

    opc = 0;
    do {
        cout << OPT_EXIT << ") EXIT" << endl;
        cout << OPT_ROOT << ") ROOT" << endl;
        cout << OPT_INSERT << ") INSERT" << endl;
        cout << OPT_REMOVE << ") REMOVE" << endl;
        cout << OPT_PREORDER << ") PREORDER" << endl;
        cout << OPT_ORDER << ") ORDER" << endl;
        cout << OPT_POSTORDER << ") POSTORDER" << endl;
        cout << OPT_SEARCH << ") SEARCH" << endl;
        cout << ">";
        cin >> opc;
        cin.ignore();

        switch(opc) {
            case OPT_EXIT: {
                cout << "Clossing . . ." << endl;
                break;
            }
            case OPT_ROOT: {
                if(!avl.empty()) {
                    cout << "Root: ";
                    print(avl.root()->data);
                    std::cout << std::endl;
                } else {
                    cout << "Error, empty" << endl;
                }
                break;
            }
            case OPT_INSERT: {
                Dog data;
                cout << "Data >";
                cin >> data.name;
                cin.ignore();
                avl.insert(data);
                cout << "Inserted" << endl;
                break;
            }
            case OPT_REMOVE: {
                Dog data;
                cout << "Data >";
                cin >> data.name;
                cin.ignore();
                avl.remove(avl.get(data));
                cout << "Removed" << endl;
                break;
            }
            case OPT_PREORDER: {
                avl.preorder(avl.root());
                cout << endl;
                break;
            }
            case OPT_ORDER: {
                avl.inorder(avl.root());
                cout << endl;
                break;
            }
            case OPT_POSTORDER : {
                avl.postorder(avl.root());
                cout << endl;
                break;
            }
            case OPT_SEARCH : {
                Dog data;
                cout << "Data >";
                cin >> data.name;
                cin.ignore();
                if (avl.get(data) != nullptr)
                {
                    cout << avl.get(data) << endl;
                }
                else
                {
                    cout << "NOT FOUND" << endl;
                }
                break;
            }
            default: {
                cout << "Invalid option" << endl;
                break;
            }
        }
        if(opc != OPT_EXIT) {
            cout << "Press enter to continue . . .";
            cin.ignore();
            system(CLEAR);
        }
    } while(opc != OPT_EXIT);

    return 0;
}

void print(Dog data) {
    cout << data.name << " ";
}
