#ifndef AVL_NODE_HPP
#define AVL_NODE_HPP

template <typename T>
class AvlNode
{
public:
    T data;
    AvlNode* parent;
    AvlNode* left;
    AvlNode* right;

    AvlNode();
    AvlNode(T data);
    bool is_leaf();
};

template <typename T>
AvlNode<T>::AvlNode()
{
    parent = nullptr;
    left = nullptr;
    right = nullptr;
}

template <typename T>
AvlNode<T>::AvlNode(T data)
{
    AvlNode();
    this->data = data;
}

template <typename T>
bool AvlNode<T>::is_leaf()
{
    return left == nullptr && right == nullptr;
}


#endif
