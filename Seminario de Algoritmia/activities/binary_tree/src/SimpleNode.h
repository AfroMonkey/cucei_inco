#ifndef SIMPLE_NODE_INCLUDED
#define SIMPLE_NODE_INCLUDED

template <class TYPE>
class SimpleNode {
private:
public:
    TYPE data;
    SimpleNode *next;

    SimpleNode();
    SimpleNode(TYPE data);
    ~SimpleNode();
};

template <class TYPE>    
SimpleNode<TYPE>::SimpleNode() {
    next = nullptr;
}

template <class TYPE> 
SimpleNode<TYPE>::SimpleNode(TYPE data) {
    next = nullptr;
    this->data = data;
}

template <class TYPE> 
SimpleNode<TYPE>::~SimpleNode() {

}

#endif
