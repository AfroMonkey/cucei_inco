#ifndef BINARY_TREE_INCLUDED
#define BINARY_TREE_INCLUDED

#include "TreeNode.h"
#include "List.h"


template <class TYPE>
class BinaryTree {
private:
    /*Attributes ---------------------------------------------------------------------------------*/
    TreeNode<TYPE> *tree_;
    /*Methods ------------------------------------------------------------------------------------*/
    /*Capacity*/
    int weight_(const TreeNode<TYPE> *subtree);
    int height_(const TreeNode<TYPE> *subtree, int h);
    /*TreeNode access*/
    TreeNode<TYPE>* get_(TreeNode<TYPE> *subtree, const TYPE &data);
    TreeNode<TYPE>* father_(TreeNode<TYPE> *subtree, const TreeNode<TYPE> *node);
    TreeNode<TYPE>* min_(TreeNode<TYPE> *subtree);
    TreeNode<TYPE>* max_(TreeNode<TYPE> *subtree);
    /*Modifiers*/
    void insert_(TreeNode<TYPE> *subtree, TreeNode<TYPE> *node);
    void trim_(TreeNode<TYPE> *subtree);
    /*Tours*/
    void preorder_(const TreeNode<TYPE> *subtree, List<TYPE> *list);
    void order_(const TreeNode<TYPE> *subtree, List<TYPE> *list);
    void postorder_(const TreeNode<TYPE> *subtree, List<TYPE> *list);
public:
    BinaryTree();
    ~BinaryTree();
    /*Capacity*/
    bool empty();
    int weight();
    int height();
    /*TreeNode access*/
    TreeNode<TYPE>* root();
    TreeNode<TYPE>* get(const TYPE &data);
    TreeNode<TYPE>* father(TreeNode<TYPE> *node);
    /*Modifiers*/
    void insert(const TYPE &data);
    void remove(TreeNode<TYPE> *node);
    void trim(TreeNode<TYPE> *subtree);
    /*Tours*/
    List<TYPE> preorder();
    List<TYPE> order();
    List<TYPE> postorder();
};

template <class TYPE>
BinaryTree<TYPE>::BinaryTree() {
    tree_ = nullptr;
}

template <class TYPE>
BinaryTree<TYPE>::~BinaryTree() {
    trim(tree_);
}

template <class TYPE>
bool BinaryTree<TYPE>::empty() {
    return tree_ == nullptr;
}

template <class TYPE>
int BinaryTree<TYPE>::weight_(const TreeNode<TYPE> *subtree) {
    if(subtree == nullptr) {
        return 0;
    } else {
        if(subtree->left == nullptr && subtree->right == nullptr) {
            return 1;
        } else {
            return weight_(subtree->left) + weight_(subtree->right);
        }
    }
}

template <class TYPE>
int BinaryTree<TYPE>::weight() {
    if(!empty()) {
        if(!tree_->isLeaf()) {
            int w;

            w = weight_(tree_->left) + weight_(tree_->right);

            return w;
        } else {
            return 1;
        }
    } else {
        return 0;
    }
}

template <class TYPE>
int BinaryTree<TYPE>::height_(const TreeNode<TYPE> *subtree, int h) {
    if(subtree != nullptr) {
        int hl, hr;

        if(subtree->left != nullptr) {
            hl = height_(subtree->left, h+1);
        } else {
            hl = h;
        }
        if(subtree->right != nullptr) {
            hr = height_(subtree->right, h+1);
        } else {
            hr = h;
        }
        return (hl > hr)?hl:hr;
    } else {
        return h;
    }
}

template <class TYPE>
int BinaryTree<TYPE>::height() {
    if(!empty()) {
        int h;

        h = height_(tree_, 1);

        return h;
    } else {
        return 0;
    }
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::root() {
    return tree_;
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::get_(TreeNode<TYPE> *subtree, const TYPE &data) {
    if(subtree == nullptr) {
        return nullptr;
    } else {
        if(data < subtree->data) {
            return get_(subtree->left, data);
        } else if(data == subtree->data) {
            return subtree;
        } else {
            return get_(subtree->right, data);
        }
    }
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::get(const TYPE &data) {
    return get_(tree_, data);
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::father_(TreeNode<TYPE> *subtree, const TreeNode<TYPE> *node) {
    if(subtree == nullptr) {
        return nullptr;
    } else {
        if(node->data < subtree->data) {
            if(node == subtree->left) {
                return subtree;
            } else {
                return father_(subtree->left, node);
            }
        } else {
            if(node == subtree->right) {
                return subtree;
            } else {
                return father_(subtree->right, node);
            }
        }
    }
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::father(TreeNode<TYPE> *node) {
    if(node != nullptr && node != tree_) {
        return father_(tree_, node);
    } else {
        return nullptr;
    }
}

template <class TYPE>
void BinaryTree<TYPE>::insert_(TreeNode<TYPE> *subtree, TreeNode<TYPE> *node) {
    if(node->data < subtree->data) {
        if(subtree->left == nullptr) {
            subtree->left = node;
        } else {
            insert_(subtree->left, node);
        }
    } else {
        if(subtree->right == nullptr) {
            subtree->right = node;
        } else {
            insert_(subtree->right, node);
        }
    }
}

template <class TYPE>
void BinaryTree<TYPE>::insert(const TYPE &data) {
    TreeNode<TYPE> *node;

    node = new TreeNode<TYPE>(data);
    if(empty()) {
        tree_ = node;
    } else {
        insert_(tree_, node);
    }
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::min_(TreeNode<TYPE> *subtree) {
    TreeNode<TYPE> *aux;

    aux = subtree;
    while(aux != nullptr && aux->left != nullptr) {
        aux = aux->left;
    }

    return aux;
}

template <class TYPE>
TreeNode<TYPE>* BinaryTree<TYPE>::max_(TreeNode<TYPE> *subtree) {
    TreeNode<TYPE> *aux;

    aux = subtree;
    while(aux != nullptr && aux->right != nullptr) {
        aux = aux->right;
    }

    return aux;
}

template <class TYPE>
void BinaryTree<TYPE>::remove(TreeNode<TYPE> *node) {
    TreeNode<TYPE> *aux, *f, *f2;
    bool fatherLeft, min;

    f = father(node);
    if(f != nullptr) {
        fatherLeft = (f->left == node);
    }
    if(node->left != nullptr) {
        aux = max_(node->left);
        min = false;
    } else {
        if(node->right != nullptr) {
            aux = min_(node->right);
            min = true;
        } else { //Leaf
            aux = nullptr;
        }
    }
    f2 = father(aux);
    if(f2 != nullptr) {
        if(f2->left == aux) {
            if(min) {
                f2->left = aux->right;
            } else {
                f2->left = aux->left;
            }
        } else {
            if(min) {
                f2->right = aux->right;
            } else {
                f2->right = aux->left;
            }
        }
    }
    if(f != nullptr) {
        if(fatherLeft) {
            f->left = aux;
        } else {
            f->right = aux;
        }
    }
    if(aux != nullptr) {
        aux->left = node->left;
        aux->right = node->right;
    }
    if(node == tree_) {
        tree_ = aux;
    }
    delete node;
}

template <class TYPE>
void BinaryTree<TYPE>::trim_(TreeNode<TYPE> *subtree) {
    if(subtree != nullptr) {
        trim_(subtree->left);
        subtree->left = nullptr;
        trim_(subtree->right);
        subtree->right = nullptr;
        delete subtree;
        subtree = nullptr;
    }
}

template <class TYPE>
void BinaryTree<TYPE>::trim(TreeNode<TYPE> *subtree) {
    TreeNode<TYPE> *f;
    f = father(subtree);
    if(f != nullptr) {
        if(f->left == subtree) {
            f->left = nullptr;
        } else {
            f->right = nullptr;
        }
    }
    if(subtree == tree_) {
        tree_ = nullptr;
    }
    trim_(subtree);
}

template <class TYPE>
void BinaryTree<TYPE>::preorder_(const TreeNode<TYPE> *subtree, List<TYPE> *list) {
    if(subtree != nullptr) {
        list->push_back(subtree->data);
        preorder_(subtree->left, list);
        preorder_(subtree->right, list);
    }
}

template <class TYPE>
List<TYPE> BinaryTree<TYPE>::preorder() {
    List<TYPE> *list;

    list = new List<TYPE>();
    preorder_(tree_, list);

    return *list;
}

template <class TYPE>
void BinaryTree<TYPE>::order_(const TreeNode<TYPE> *subtree, List<TYPE> *list) {
    if(subtree != nullptr) {
        order_(subtree->left, list);
        list->push_back(subtree->data);
        order_(subtree->right, list);
    }
}

template <class TYPE>
List<TYPE> BinaryTree<TYPE>::order() {
    List<TYPE> *list;

    list = new List<TYPE>();
    order_(tree_, list);

    return *list;
}

template <class TYPE>
void BinaryTree<TYPE>::postorder_(const TreeNode<TYPE> *subtree, List<TYPE> *list) {
    if(subtree != nullptr) {
        postorder_(subtree->left, list);
        postorder_(subtree->right, list);
        list->push_back(subtree->data);
    }
}

template <class TYPE>
List<TYPE> BinaryTree<TYPE>::postorder() {
    List<TYPE> *list;

    list = new List<TYPE>();
    postorder_(tree_, list);

    return *list;
}

#endif
