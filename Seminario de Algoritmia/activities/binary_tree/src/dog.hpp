#ifndef DOG_HPP
#define DOG_HPP

#include <iostream>
#include <cstring> // memcpy

class Dog
{
public:
    char name[10];

    Dog() {name[9] = '\x0';};
    Dog(std::string name) {memcpy(this->name, name.c_str(), 10);}

    bool operator<(Dog& dog) const { return strcmp(name, dog.name) < 0; }
    bool operator==(Dog& dog) const { return strcmp(name, dog.name) == 0; }
};

#endif
