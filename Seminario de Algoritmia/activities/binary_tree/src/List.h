#ifndef LIST_INCLUDED
#define LIST_INCLUDED

#include "SimpleNode.h"

using namespace std;

class ListException{
public:
    ListException(const std::string& msg) : msg_(msg) {}
    ~ListException() {}

    std::string what() {return msg_;}
private:
    std::string msg_;
};

template <class TYPE>
class List {
private:
    SimpleNode<TYPE> *list;
    void swap(SimpleNode<TYPE> *n1, SimpleNode<TYPE> *n2);
    int size_;
public:
    List();
    ~List();
    //operator=(List &toCopy); TODO
    /*Capacity*/
    bool isEmpty();
    int size();
    /*Element access*/
    TYPE front();
    TYPE back();
    /*SimpleNode<TYPE> access*/
    SimpleNode<TYPE>* begin();
    SimpleNode<TYPE>* end();
    SimpleNode<TYPE>* prev(SimpleNode<TYPE> *node);
    SimpleNode<TYPE>* get(int position);
    /*Modifiers*/
    void push_front(const TYPE &value);
    void pop_front();
    void push_back(const TYPE &value);
    void pop_back();
    void insert(SimpleNode<TYPE> *position, const TYPE &value);
    void insert(int position, const TYPE &value);
    void erase(SimpleNode<TYPE> *position);
    void clear();
    /*Operations*/
    void remove(const TYPE &value);
    void sort(int (*relation)(TYPE, TYPE));
    /*Miscellaneous*/
    void forEach(void (*function)(TYPE));
    void forEachEqual(TYPE &searching, void (*funcion)(TYPE));
};

template <class TYPE>
List<TYPE>::List() {
    list = nullptr;
    size_ = 0;
}

template <class TYPE>
List<TYPE>::~List() {
    clear();
}

template <class TYPE>
bool List<TYPE>::isEmpty() {
    return list == nullptr;
}

template <class TYPE>
int List<TYPE>::size() {
    return size_;
}

template <class TYPE>
TYPE List<TYPE>::front() {
    return list->data;
}

template <class TYPE>
TYPE List<TYPE>::back() {
    SimpleNode<TYPE> *aux;
    aux = list;
    while(aux->next != nullptr) {
        aux = aux->next;
    }
    return aux->data;
}

template <class TYPE>
SimpleNode<TYPE>* List<TYPE>::begin() {
    return list;
}

template <class TYPE>
SimpleNode<TYPE>* List<TYPE>::end() {
    SimpleNode<TYPE> *aux;
    aux = list;
    while(aux->next != nullptr) {
        aux = aux->next;
    }
    return aux;
}

template <class TYPE>
SimpleNode<TYPE>* List<TYPE>::prev(SimpleNode<TYPE> *position) {
    if(position == list) {
        return nullptr;
    }
    if(position != nullptr) {
        SimpleNode<TYPE> *aux;
        aux = list;
        while(aux != nullptr && aux->next != position) {
            aux = aux->next;
        }
        if(aux != nullptr) {
            return aux;
        } else {
            throw ListException("Not found position");
        }
    } else {
        throw ListException("Invalid access (nullptr)");
    }
}

template <class TYPE>
SimpleNode<TYPE>* List<TYPE>::get(int position) {
    if(position >= 0 && position < size()) {
        SimpleNode<TYPE> *aux;
        aux = list;
        for(int i = 0; i < position; i++) {
            aux = aux->next;
        }
        return aux;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
void List<TYPE>::push_front(const TYPE &value) {
    SimpleNode<TYPE> *node;
    node = new SimpleNode<TYPE>(value);
    node->next = list;
    list = node;
    size_++;
}

template <class TYPE>
void List<TYPE>::pop_front() {
    if(!isEmpty()) {
        SimpleNode<TYPE> *aux;
        aux = list;
        list = list->next;
        delete aux;
        size_--;
    } else {
        throw ListException("Empty list");
    }
}

template <class TYPE>
void List<TYPE>::push_back(const TYPE &value) {
    SimpleNode<TYPE> *node;
    node = new SimpleNode<TYPE>(value);
    if(isEmpty()) {
        list = node;
    } else {
        SimpleNode<TYPE> *aux;
        aux = list;
        while(aux->next != nullptr) {
            aux = aux->next;
        }
        aux->next = node;
    }
    size_++;
}

template <class TYPE>
void List<TYPE>::pop_back() {
    if(!isEmpty()) {
        SimpleNode<TYPE> *aux;
        aux = list;
        while(aux->next != nullptr) {
            aux = aux->next;
        }
        prev(aux)->next = nullptr;
        if(list == aux) {
            list = list->next;
        }
        delete aux;
        size_--;
    } else {
        throw ListException("Empty list");
    }
}

template <class TYPE>
void List<TYPE>::insert(SimpleNode<TYPE> *position, const TYPE &value) {
    if(position != nullptr) {
        SimpleNode<TYPE> *node;
        node = new SimpleNode<TYPE>(value);
        if(position == list) {
            node->next = list;
            list = node;
        } else {
            SimpleNode<TYPE> *aux;
            aux = list;
            while(aux != nullptr && aux->next != position) {
                aux = aux->next;
            }
            if(aux != nullptr) {
                node->next = aux->next;
                aux->next = node;
            } else {
                throw ListException("Not found position");
            }
        }
        size_++;
    } else {
        throw ListException("Invalid access (nullptr)");
    }
}

template <class TYPE>
void List<TYPE>::insert(int position, const TYPE &value) {
    if(position >= 0 && position <= size()) {
        SimpleNode<TYPE> *node;
        node = new SimpleNode<TYPE>(value);
        if(position == 0) {
            node->next = list;
            list = node;
        } else {
            SimpleNode<TYPE> *aux;
            aux = list;
            position--;
            for(int i = 0; i < position; i++) {
                aux = aux->next;
            }
            node->next = aux->next;
            aux->next = node;
        }
        size_++;
    } else {
        throw ListException("Out of bounds");
    }
}

template <class TYPE>
void List<TYPE>::erase(SimpleNode<TYPE> *position) {
    if(position != nullptr) {
        SimpleNode<TYPE> *aux;
        aux = list;
        while(aux != nullptr && aux != position) {
            aux = aux->next;
        }
        if(aux != nullptr) {
            if(aux != list) {
                prev(aux)->next = aux->next;
            } else {
                list = list->next;
            }
            delete aux;
            size_--;
        } else {
            throw ListException("Not found position");
        }
    } else {
        throw ListException("Invalid access (nullptr)");
    }
}

template <class TYPE>
void List<TYPE>::clear() {
    SimpleNode<TYPE> *aux;
    aux = list;
    while(aux != nullptr) {
        list = list->next;
        delete aux;
        aux = list;
    }
    list = nullptr;
    size_ = 0;
}

template <class TYPE>
void List<TYPE>::remove(const TYPE &value) {
    SimpleNode<TYPE> *aux;
    SimpleNode<TYPE> *aux2;
    aux = list;
    while(aux != nullptr) {
        aux = aux->next;
        if(aux != nullptr && aux->data == value) {
            aux2 = prev(aux);
            aux2->next = aux->next;
            delete aux;
            aux = aux2;
            size_--;
        }
    }
    if(list->data == value) {
        aux = list;
        list = list->next;
        delete aux;
        size_--;
    }
}

template <class TYPE>
void List<TYPE>::sort(int (*relation)(TYPE, TYPE)) {
    SimpleNode<TYPE> *nj, *njm1;
    int s = size();
    for(int i = 1; i < s; i++) {
        for(int j = i; j > 0; j--) {
            nj =   get(j);
            njm1 = get(j-1);
            if((*relation)(nj->data, njm1->data)) {
                swap(nj, njm1);
            } else {
                break;
            }
        }
    }
}

template <class TYPE>
void List<TYPE>::forEach(void (*funcion)(TYPE)) {
    SimpleNode<TYPE> *aux;
    aux = list;
    while(aux != nullptr) {
        (*funcion)(aux->data);
        aux = aux->next;
    }
}

template <class TYPE>
void List<TYPE>::swap(SimpleNode<TYPE> *n1, SimpleNode<TYPE> *n2) {
    if(n1 == n2) {
        return;
    }

    SimpleNode<TYPE> *pn1, *nn1, *pn2, *nn2;
    bool b1, b2;
    b1 = b2 = false;

    if(n1 == list) {
        b1 = true;
    } else if(n2 == list) {
        b2 = true;
    }

    if(n1->next == n2) {
        if(!b1) {
            prev(n1)->next = n2;
        }
        n1->next = n2->next;
        n2->next = n1;
    } else if(n2->next == n1) {
        if(!b2) {
            prev(n2)->next = n1;
        }
        n2->next = n1->next;
        n1->next = n2;
    } else {
        if(!b1) {
            pn1 = prev(n1);
            pn1->next = n2;
        }
        nn1 = n1->next;
        if(!b2) {
            pn2 = prev(n2);
            pn2->next = n1;
        }
        nn2 = n2->next;
        n2->next = nn1;
        n1->next = nn2;
    }

    if(b1) {
        list = n2;
    } else if(b2) {
        list = n1;
    }
}

template <class TYPE>
void List<TYPE>::forEachEqual(TYPE &searching, void (*function)(TYPE)) {
    SimpleNode<TYPE> *aux;
    aux = list;
    while(aux != nullptr) {
        if((TYPE)(aux->data) == (TYPE)searching) {
            (*function)(aux->data);
        }
        aux = aux->next;
    }
}

#endif
