#include <iostream>

#include "quick_sort.hpp"

int compare(const void* a, const void* b)
{
    return *(int*)a - *(int*)b;
}

int main(int argc, char const *argv[])
{
    int n;
    std::cin >> n;
    int array[n];
    for (int i = 0; i < n; i++)
    {
        std::cin >> array[i];
    }
    afro_qsort(array, n, sizeof(int), compare);
    n--;
    for (int i = 0; i < n;)
    {
        if (array[i] > array[++i])
        {
            std::cout << "Error" << std::endl;
            break;
        }
    }
    return 0;
}
