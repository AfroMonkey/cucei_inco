#include <iostream>
#include <ctime>

#include "merge_sort.hpp"

int compare(const void* a, const void* b)
{
    return *(int*)a - *(int*)b;
}

int main(int argc, char const *argv[])
{
    int n;
    std::cin >> n;
    int array[n];
    for (int i = 0; i < n; i++)
    {
        std::cin >> array[i];
    }
    std::clock_t t = std::clock();
    t = std::clock() - t;
    std::cout << "*Start*" << std::endl;
    afro_merge(array, n, sizeof(int), compare);
    std::cout << "*End*" << std::endl;
    std::cout << "CPU time used: " << ((float) t / CLOCKS_PER_SEC) * 1000.00 << " ms" << std::endl;
    n--;
    for (int i = 0; i < n;)
    {
        if (array[i] > array[++i])
        {
            std::cout << "Error" << std::endl;
            break;
        }
    }
    return 0;
}
