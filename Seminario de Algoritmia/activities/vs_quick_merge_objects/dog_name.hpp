#ifndef DOG_NAME_HPP
#define DOG_NAME_HPP

#include <iostream>
#include <cstring> // memcpy


class DogName
{
public:
    char name[10];
    DogName(std::string name) {memcpy(this->name, name.c_str(), 10);}
};

#endif
