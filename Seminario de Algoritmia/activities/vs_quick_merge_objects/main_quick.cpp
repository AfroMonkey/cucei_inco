#include <iostream>
#include <cstring>
#include <iostream>

#include "quick_sort.hpp"
#include "dog_name.hpp"

int compare(const void* a, const void* b)
{
    return strcmp((char*)a, (char*)b);
}

int main(int argc, char const *argv[])
{
    DogName array[] =
    {
        DogName("Alger"),
        DogName("Nath"),
        DogName("Ulisse"),
        DogName("Evan"),
        DogName("Gabone"),
        DogName("Auro"),
        DogName("Kassio"),
        DogName("Bruc"),
        DogName("Maxime"),
        DogName("Tyrrell"),
        DogName("Benne"),
        DogName("Indio"),
        DogName("Gasper"),
        DogName("Lambert"),
        DogName("Figo"),
        DogName("Volton"),
        DogName("Bord"),
        DogName("Tino"),
        DogName("Crunch"),
        DogName("Monty"),
        DogName("Anouk"),
        DogName("Zaimon"),
        DogName("Hali"),
        DogName("Duran"),
        DogName("Davant"),
        DogName("Dingo"),
        DogName("Heinek"),
        DogName("Joale"),
        DogName("Man"),
        DogName("Tyssen"),
        DogName("Lamar"),
        DogName("Dents"),
        DogName("Pezzo"),
        DogName("Cooper"),
        DogName("Thin"),
        DogName("Venite"),
        DogName("Curro"),
        DogName("Karim"),
        DogName("Jalba"),
        DogName("Sukkar"),
        DogName("Kurdo"),
        DogName("Iker"),
        DogName("Maco"),
        DogName("Axic"),
        DogName("Bilal"),
        DogName("Vivien"),
        DogName("Kyle"),
        DogName("Enzzo"),
        DogName("Dion"),
        DogName("Remi"),
        DogName("Sirio"),
        DogName("Amir"),
        DogName("Hobbo"),
        DogName("Milo"),
        DogName("Galbi"),
        DogName("Noah"),
        DogName("Patch"),
        DogName("Tahel"),
        DogName("Cru"),
        DogName("Franc"),
        DogName("Thaysson"),
        DogName("Beix"),
        DogName("Dasel"),
        DogName("Benif"),
        DogName("Neo"),
        DogName("Chester"),
        DogName("Trust"),
        DogName("Frezzio"),
        DogName("Lorik"),
        DogName("Aman"),
        DogName("Morgan"),
        DogName("Crestin"),
        DogName("Bru"),
        DogName("Idale"),
        DogName("Loras"),
        DogName("Rossi"),
        DogName("Jayson"),
        DogName("Krende"),
        DogName("Larry"),
        DogName("Vito"),
        DogName("Antón"),
        DogName("Frany"),
        DogName("Bixo"),
        DogName("Bali"),
        DogName("Pinto"),
        DogName("Kurt"),
        DogName("Trigo"),
        DogName("Bonne"),
        DogName("Kannuck"),
        DogName("Arcadi"),
        DogName("Gianni"),
        DogName("Newman"),
        DogName("Valan"),
        DogName("Noete"),
        DogName("Vinni"),
        DogName("Este"),
        DogName("Aureli"),
        DogName("Libio"),
        DogName("Zick"),
        DogName("Cromee")
    };
    std::cout << "*Start*" << std::endl;
    afro_qsort(array, 100, sizeof(DogName), compare);
    for (int i = 0; i < 100; i++)
    {
        std::cout << array[i].name << std::endl;
    }
    std::cout << "*End*" << std::endl;
    return 0;
}
