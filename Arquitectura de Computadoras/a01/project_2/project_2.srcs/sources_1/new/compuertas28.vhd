library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity compuertas28 is
	port(x, y : in  std_logic;
		 z    : out std_logic_vector(0 to 7));
end compuertas28;

architecture hola of compuertas28 is
	begin
		z(0) <= not(x);
		z(1) <= not(y);
		z(2) <= x and y;
		z(3) <= x nand y;
		z(4) <= x or y;
		z(5) <= x nor y; 
		z(6) <= x xor y;
		z(7) <= not(x xor y);
	end hola;