LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY compuertas IS
    PORT ( a : in STD_LOGIC;
           b : in STD_LOGIC;
           pand : out STD_LOGIC;
           pnand : out STD_LOGIC;
           por : out STD_LOGIC;
           pnor : out STD_LOGIC;
           pxor : out STD_LOGIC;
           pxnor : out STD_LOGIC;
           pnot : out STD_LOGIC);
END compuertas;

ARCHITECTURE main OF compuertas IS
BEGIN
    pand <= a and b;
    pnand <= a nand b;
    por <= a or b;
    pnor <= a nor b;
    pxor <= a xor b;
    pxnor <= not(a xor b);
    pnot <= not a;
END main;

