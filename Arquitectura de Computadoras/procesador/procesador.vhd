library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
ENTITY procesador IS
	PORT(
		clk: IN std_logic;
		rst: IN std_logic;
		r_w: OUT std_logic;
		dir: OUT std_logic_vector(7 DOWNTO 0);
		dat: INOUT std_logic_vector(7 DOWNTO 0));
END procesador;

ARCHITECTURE descripcion OF procesador IS
	TYPE estado IS (inicial,busqueda,ejec,ldxxa,ldaxx,anda,adda,suba,xora,nanda,ora);
	SIGNAL a,pc,ir: std_logic_vector(7 DOWNTO 0);
	SIGNAL rdat_in,dat_in,dat_out: std_logic_vector(7 DOWNTO 0);
	SIGNAL rwaux,seldir: std_logic;
	SIGNAL presente: estado:=inicial;

	BEGIN fsm:PROCESS(clk)
		BEGIN
			IF clk='1' THEN
				CASE presente IS
					WHEN inicial =>
						seldir<='1'; -- dir<=pc
						pc<=(OTHERS=>'0');
						rwaux<='1';
						ir<=(OTHERS=>'0');
						a<=(OTHERS=>'0');
						presente<=busqueda;
					WHEN busqueda=>
						ir<=dat_in;
						pc<=pc+1;
						IF dat_in(2 DOWNTO 0)="1111" THEN
							presente<=busqueda;
						ELSE
							presente<=ejec;
						END IF;
					WHEN ejec =>
						seldir<='0'; -- dir<=dat_in
						pc<=pc+1;
						presente<=busqueda;
						CASE ir(3 DOWNTO 0) IS
							WHEN "0000" =>
								presente<=ldaxx;
							WHEN "0001" =>
								dat_out<=a;
								rwaux<='0'; -- Escribir
								presente<=ldxxa;
							WHEN "0010" =>
								presente<=anda;
							WHEN "0011" =>
								presente<=adda;
							WHEN "0100" =>
								presente<=suba;
							WHEN "0101" =>
								seldir<='1';
								IF a=0 THEN
									pc<=dat_in;
								END IF;
							WHEN "0110" =>
								seldir<='1';
								pc<=dat_in;
							WHEN "0111" =>
								presente<=xora;
							WHEN "1000" =>
								presente<=nanda;
							WHEN "1001" =>
								presente<=ora;
							WHEN OTHERS => NULL;
						END CASE;
					WHEN ldaxx =>
						a<=dat_in;
						seldir<='1';
						presente<=busqueda;
					WHEN ldxxa =>
						rwaux<='1';
						seldir<='1';
						presente<=busqueda;
					WHEN anda =>
						a<=a AND dat_in;
						seldir<='1';
						presente<=busqueda;
					WHEN adda =>
						a<=a+dat_in;
						seldir<='1';
						presente<=busqueda;
					WHEN suba =>
						a<=a-dat_in;
						seldir<='1';
						presente<=busqueda;
					WHEN xora =>
						a<=a XOR dat_in;
						seldir<='1';
						presente<=busqueda;
					WHEN nanda =>
						a<=a NAND dat_in;
						seldir<='1';
						presente<=busqueda;
					WHEN ora =>
						a<=a or dat_in;
						seldir<='1';
						presente<=busqueda;
				END CASE;
				IF rst='1' THEN
					presente<=inicial;
				END IF;
			END IF;
	END PROCESS fsm;

	latch_in: PROCESS(clk)

	BEGIN
		IF clk='1' THEN
			rdat_in<=dat_in;
		END IF;
	END PROCESS latch_in;
	dir<=pc WHEN seldir='1' ELSE rdat_in; -- Multiplexor de las direcciones
	r_w<=rwaux;
	dat<=dat_out WHEN rwaux='0' ELSE (OTHERS=>'Z'); -- Buffer de Salida
	dat_in<=dat;
END descripcion;
