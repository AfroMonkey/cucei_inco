LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY sumador_restador IS
    PORT (
        x : in STD_LOGIC_VECTOR(3 downto 0);
        y : in STD_LOGIC_VECTOR(3 downto 0);
        w : in STD_LOGIC;
        s : out STD_LOGIC_VECTOR(3 downto 0);
        cout : out STD_LOGIC
    );
END sumador_restador;

ARCHITECTURE main OF sumador_restador IS
    signal y_xor : STD_LOGIC_VECTOR(3 downto 0);
    signal c : STD_LOGIC_VECTOR(4 downto 0);
BEGIN
    y_xor(0) <= y(0) xor w;
    y_xor(1) <= y(1) xor w;
    y_xor(2) <= y(2) xor w;
    y_xor(3) <= y(3) xor w;
    c(0) <= w;
    s(0) <= c(0) xor x(0) xor y_xor(0);
    c(1) <= (x(0) and y_xor(0)) or (c(0) and (x(0) xor y_xor(0)));
    s(1) <= c(1) xor x(1) xor y_xor(1);
    c(2) <= (x(1) and y_xor(1)) or (c(1) and (x(1) xor y_xor(1)));
    s(2) <= c(2) xor x(2) xor y_xor(2);
    c(3) <= (x(2) and y_xor(2)) or (c(2) and (x(2) xor y_xor(2)));
    s(3) <= c(3) xor x(3) xor y_xor(3);
    c(4) <= (x(3) and y_xor(3)) or (c(3) and (x(3) xor y_xor(3))) ;
    cout <= c(4);
END main;
