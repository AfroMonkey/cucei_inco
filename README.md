# CUCEI - INCO
En este repositorio se encuentra todo el material digital generado y obtenido durante la carrera de Ingeniería en Computación del Centro Universitario de Ciencias Exactas e Ingenierías. 2015-2019.

Si encuentras información que no debería estar aquí (Información personal, archivos con copyright, etc), favor de hacérmelo saber.

Si deseas contribuir de alguna forma (añadir archivos, modificar los existentes, organizar, etc), no dudes en contactarme.
